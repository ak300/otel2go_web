@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
			<li>@lang('messages.Amenity')</li>
		</ul>
		<h4>@lang('messages.Amenity')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->
<div class="contentpanel">
@if(has_staff_permission('vendor/amenities/create'))	
<div class="buttons_block pull-right">
<div class="btn-group mr5">
<a class="btn btn-primary tip" href="{{ URL::to('vendor/amenities/create') }}" title="Add New">@lang('messages.Add New')</a>
</div>
</div>
@endif
@if (Session::has('message'))
		<div class="admin_sucess_common">
	<div class="admin_sucess">
    <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div>
    </div></div>
@endif

@if (Session::has('no-amenities'))
        <div class="admin_sucess_common">
    <div class="admin_sucess">
    <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('no-amenities') }}</div>
    </div></div>
@endif

 <table id="city-table" class="table table-striped table-bordered responsive">
    <thead>
        <tr class="headings">
			
            <th>@lang('messages.S.no')</th> 
            <th>@lang('messages.Amenity Name')</th> 
            <th>@lang('messages.Amenities Type')</th>     
            <th>@lang('messages.Created date')</th> 
            <th>@lang('messages.Status')</th>
            <?php if(has_staff_permission('vendor/amenities/edit')) { ?>
            <th>@lang('messages.Actions')</th>
            <?php } ?> 
        </tr>
    </thead>
<tbody>
<tr>
<td class="empty-text" colspan="7" style="background-color: #fff!important;">
<div class="list-empty-text"> @lang('messages.No records found.') </div>
</td>
</tr>
</tbody>

</table>
<?php /** echo $cities->render(); */ ?>
</div>

<script>
$(function() {
    $('#city-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('ajaxamenities.data') !!}',
        "order": [],
		"columnDefs": [{
		  "targets"  : 'no-sort',
		  "orderable": false,
		  
		}],
        columns: [
			{ data: 'cid', name: 'amenities.cid',orderable: false },
			{ data: 'amenity_name', name: 'amenity_name' },
            { data: 'amenity_type', name: 'amenity_type' },
            { data: 'created_date', name: 'created_date' },
            { data: 'default_status', name: 'default_status' },
            <?php if(has_staff_permission('vendor/amenities/edit')) { ?>
            { data: 'action', name: 'action', orderable: false, searchable: false}
            <?php } ?>
        ],
    });
});
</script>
@endsection
