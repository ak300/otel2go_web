@extends('layouts.vendors')
@section('content')
<!-- Nav tabs -->
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-home"></i>
		</div>
		<div class="media-body">
			<ul class="breadcrumb">
				<li><a href="{{ url('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
				<li>@lang('messages.Outlets')</li>
			</ul>
			<h4>@lang('messages.View Outlet Details')  - {{ ucfirst($data[0]->outlet_name) }}</h4>
		</div>
	</div><!-- media -->
</div><!-- pageheader -->
<div class="contentpanel">
  <ul class="nav nav-tabs"></ul>
		    <div class="tab-content mb30">
		        <div class="tab-pane active" id="home3">
			<div class="buttons_block pull-right">
				<div class="btn-group mr5">
					<a class="btn btn-primary tip" href="{{ URL::to('vendor/edit_outlet/'.$data[0]->id . '') }}" title="Edit" >@lang('messages.Edit')</a>
				</div>
			</div>
            <legend>@lang('messages.Outlet Information')</legend>
				<div class="form-group">
					<label class="col-sm-5 control-label">@lang('messages.Outlet Name')</label>
					<div class="col-sm-7">{!! $data[0]->outlet_name !!}</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">@lang('messages.Vendor Name')</label>
					<div class="col-sm-7">{!! $data[0]->vendor_name !!}</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">@lang('messages.Country')</label>
					<div class="col-sm-7">
					@foreach($countries as $list)
						<?php echo ($data[0]->country_id==$list->id)?ucfirst($list->country_name):'';?>
					@endforeach
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">@lang('messages.City')</label>
					<div class="col-sm-7">
						<?php $city = getCityList($data[0]->country_id); ?>
						@foreach($city as $list)
							<?php echo ($data[0]->city_id==$list->id)?ucfirst($list->city_name):'';?>
						@endforeach
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">@lang('messages.Zone')</label>
					<div class="col-sm-7">
						<?php $location = getLocationList($data[0]->country_id,$data[0]->city_id); ?>
						@foreach($location as $list)
							<?php echo ($data[0]->location_id==$list->id)?ucfirst($list->zone_name):'';?>
						@endforeach
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">@lang('messages.Contact Phone')</label>
					<div class="col-sm-7">{!! $data[0]->contact_phone !!}</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">@lang('messages.Contact Email')</label>
					<div class="col-sm-7">{!! $data[0]->contact_email !!}</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">@lang('messages.Contact Address')</label>
					<div class="col-sm-7">{!! ucfirst($data[0]->contact_address) !!}</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-5 control-label">@lang('messages.Status')</label>
					<div class="col-sm-7"><?php if($data[0]->active_status==1){ ?> @lang('messages.Active') <?php } else { ?> @lang('messages.Inactive'); <?php } ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-5 control-label">@lang('messages.Property Image')</label>
						<div class="col-sm-7"><img src="<?php echo url('/assets/admin/base/images/vendors/property_image/'.$data[0]->outlet_image); ?>" class="thumbnail img-responsive" alt="No image"></div>
				</div>	

			<legend>@lang('messages.Hotel Information')</legend>
				<div class="form-group">
					<label  class="col-sm-5 control-label">@lang('messages.Amenities')</label>
					<div class="col-sm-7">
						<?php $amty = getAllAmenityTypes();
							  $current_amty = isset($data[0]->amenity_type)?$data[0]->amenity_type:array();
							  $current_amty = explode(',',$current_amty);	
						?>
						@foreach($amty as $value)
							@if(in_array($value->id,$current_amty))
								{{ $value->amenity_name }}</br>
							@endif
						@endforeach
					</div>
				</div>
					<div class="form-group">
					<label  class="col-sm-5 control-label">@lang('messages.Facilities')</label>
					<div class="col-sm-7">{!! $data[0]->facilities !!}</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-5 control-label">@lang('messages.Highlights')</label>
					<div class="col-sm-7">{!! $data[0]->highlights !!}</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-5 control-label">@lang('messages.Hotel Rules')</label>
					<div class="col-sm-7">{!! $data[0]->hotel_rules !!}</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-5 control-label">@lang('messages.Near By')</label>
					<div class="col-sm-7">{!! $data[0]->near_by !!}</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-5 control-label">@lang('messages.Cancellation Policy')</label>
					<div class="col-sm-7">{!! $data[0]->cancellation_policy !!}</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-5 control-label">@lang('messages.Directions')</label>
					<div class="col-sm-7">{!! $data[0]->directions !!}</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-5 control-label">@lang('messages.Landmark')</label>
					<div class="col-sm-7">{!! $data[0]->landmark !!}</div>
				</div>
			</div>	
			<legend>@lang('messages.Place of Interest')</legend>
				@if(isset($data[0]->places))
					@foreach($data[0]->places as $key => $val) &nbsp
						<div class="form-group">
							<div class="col-md-3"><p>&nbsp;{!! ucfirst($val->place_name) !!}</p></div>
							<?php /* {!! ucfirst($val->place_name) !!} */ ?>
							@if( $val->place == 1 )
							 <div class="col-md-3"><p>&nbsp;{!!'' .ucfirst('traveller') !!}</p></div>
							@elseif( $val->place == 2)
							<div class="col-md-3"><p>&nbsp;{!!''.ucfirst('restaurants') !!}</p></div>
							@elseif( $val->place == 3)
							<div class="col-md-3"><p>&nbsp;{!!''.ucfirst('markets') !!}</p></div>
							@endif
							<div class="col-md-3"><p>&nbsp;
								@if(is_numeric($val->distance))
									{!! $val->distance !!} KM
								@else
									{!! $val->distance !!}
								@endif
							</p></div>
							<?php /* {!! $val->distance !!} */ ?>
						</div>
					@endforeach
				@endif
		</div>
    </div>

@endsection
