@extends('layouts.vendors')
@section('content')
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-timepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/select2.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" /> 

<style>

.remove_div
{
	margin-top: 10px;
}


</style>
<!-- Nav tabs -->
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-home"></i>
		</div>
		<div class="media-body">
			<ul class="breadcrumb">
				<li><a href="{{ url('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
				<li>@lang('messages.Outlets')</li>
			</ul>
			<h4>@lang('messages.Add Property')</h4>
		</div>
	</div><!-- media -->
</div><!-- pageheader -->


<div class="contentpanel">
<div class="col-md-12">
<div class="row panel panel-default">
<div class="grid simple">
	@if (count($errors) > 0)
	<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
		<ul>
			@foreach ($errors->all() as $error)
				<li><?php echo trans('messages.'.$error); ?> </li>
			@endforeach
		</ul>
	</div>
	@endif
	<ul class="nav nav-justified nav-wizard nav-pills">
		<li @if(old('tab_info')=='login_info' || old('tab_info')=='') class="active" @endif><a href="#login_info" class="login_info" data-toggle="tab"><strong>@lang('messages.Property Information')</strong></a></li>
		<li @if(old('tab_info')=='vendor_info') class="active" @endif ><a href="#vendor_info" class="vendor_info" data-toggle="tab"><strong>@lang('messages.Hotel Description')</strong></a></li>		
		<li @if(old('tab_info')=='delivery_info') class="active" @endif ><a href="#delivery_info" class="delivery_info" data-toggle="tab"><strong>@lang('messages.Contact Information')</strong></a></li>
		<li @if(old('tab_info')=='contact_info') class="active" @endif ><a href="#contact_info" class="contact_info" data-toggle="tab"><strong>@lang('messages.Place of Interest')</strong></a></li>
	</ul>
	{!!Form::open(array('url' => 'vendor/outlet_create', 'method' => 'post','class'=>'panel-wizard','id'=>'outlet_form','files' => true));!!}
		<div class="tab-content tab-content-simple mb30 no-padding" >
			<div class="tab-pane active" id="login_info">
				<legend>@lang('messages.Property Information')</legend>
				<div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Outlet Name') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<?php /*<input type="text" name="outlet_name" value="{!! old('outlet_name') !!}"  maxlength="255" placeholder="@lang('messages.Outlet Name')"  class="form-control" />*/?>
						<?php $i = 0; foreach($languages as $langid => $language):?>
							<div class="input-group translatable_field language-<?php echo $language->id;?>" <?php if($i > 0):?>style="display: none;"<?php endif;?>>
								<input type="text" name="outlet_name[<?php echo $language->id;?>]" id="suffix_<?php echo $language->id;?>"  placeholder="<?php echo trans('messages.Outlet Name').trans('messages.'.'('.$language->name.')');?>" class="form-control" value="{!! Input::old('outlet_name.'.$language->id) !!}" maxlength="50" />
								<div class="input-group-btn">
									<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><?php echo $language->name;?> <span class="caret"></span></button>
									<ul class="dropdown-menu pull-right">
										<?php foreach($languages as $sublangid => $sublanguage):?>
											<li><a href="javascript:YL.Language.fieldchange(<?php echo $sublanguage->id;?>)"> <?php echo trans('messages.'.$sublanguage->name);?></a></li>
										<?php endforeach;?>
									</ul>
								</div><!-- input-group-btn -->
							</div>
						<?php $i++; endforeach;?>
					</div>
				</div>
<?php /*				
				<div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Country') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<select name="country" id="country_id" class="form-control" >
							<option value="">@lang('messages.Select Country')</option>
							@foreach($countries as $list)
								<option value="{{$list->id}}" <?php if(!empty(old('country'))){ echo (old('country')==$list->id)?'selected="selected"':''; } */	?> <?php /* >{{$list->country_name}}</option>
							@endforeach
						</select>
					</div>
			   </div>
*/ ?>			   
				<div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.City') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<select name="city" id="city_id" class="form-control" >
							<option value="">@lang('messages.Select City')</option>
							<?php /*
								if(!empty(old('country'))){ 
								$city = getCityList(old('country'));
									*/
							?>
								@foreach(getCityList() as $list)
									<option name="city" value="{{$list->id}}" <?php echo (old('city')==$list->id)?'selected="selected"':''; ?> >{{$list->city_name}}</option>
								@endforeach
							<?php /* } */ ?>
						</select>
					</div>
			   </div>
				<div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Zone') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<select name="location" id="location_id" class="form-control">
							<option value="">@lang('messages.Select Zone')</option>
							<?php
								if(!empty(old('city'))){
								$location = getLocationList(1,old('city'));
							?>
								@foreach($location as $list)
									<option value="{{$list->id}}" <?php echo (old('location')==$list->id)?'selected="selected"':''; ?> >{{$list->zone_name}}</option>
								@endforeach
							<?php } ?>
						</select>
					</div>
			    </div>

			    <div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Price Category') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<?php  $old =old('pricecategories'); $cate=array(); if($old){  $cate=$old; } ?>
						<select id="pricecategories"  name="pricecategories[]"  data-placeholder="@lang('messages.Select Category')" multiple class="width300">
						@foreach ($pricecategories as $val)
							<option value="{{ $val->id }}" <?php echo in_array($val->id,$cate)?'selected="selected"':'';?> >{{  ucfirst($val->category_name) }}</option>	
						@endforeach
					</select>
					</div>
			    </div>

			    <div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Place Category') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<?php  $old =old('placecategories'); $cate=array(); if($old){  $cate=$old; } ?>
						<select id="placecategories"  name="placecategories[]"  data-placeholder="@lang('messages.Select Category')" multiple class="width300">
						@foreach ($placecategories as $val)
							<option value="{{ $val->id }}" <?php echo in_array($val->id,$cate)?'selected="selected"':'';?> >{{  ucfirst($val->category_name) }}</option>	
						@endforeach
						</select>
					</div>
			    </div>

			    <div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Accomodation Type') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<?php  $old =old('accomodation_type'); $cate=array(); if($old){  $cate=$old; } ?>
						<select id="accomodation_type"  name="accomodation_type[]"  data-placeholder="@lang('messages.Select Accomodation Type')" multiple class="width300">
						@foreach ($accmodation_type as $val)
							<option value="{{ $val->id }}" <?php echo in_array($val->id,$cate)?'selected="selected"':'';?> >{{  ucfirst($val->accomodation_type_name) }}</option>	
						@endforeach
					</select>
					</div>
			    </div>			    
				<div class="form-group">
						<label class="col-sm-3 control-label">@lang('messages.Contact Address') <span class="asterisk">*</span></label>
						<div class="col-sm-7">
							<?php $i = 0; foreach($languages as $langid => $language):?>
								<div class="input-group translatable_field language-<?php echo $language->id;?>" <?php if($i > 0):?>style="display: none;"<?php endif;?>>
									<input id="target" type="text" onKeyPress="return disableEnterKey(event)" name="contact_address[<?php echo $language->id;?>]" id="suffix_<?php echo $language->id;?>"  placeholder="<?php echo trans('messages.Search your place and press enter').trans('messages.'.'('.$language->name.')');?>" class="form-control"  value="{!! Input::old('contact_address.'.$language->id) !!}" />
									<div class="input-group-btn">
										<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><?php echo $language->name;?> <span class="caret"></span></button>
										<ul class="dropdown-menu pull-right">
											<?php foreach($languages as $sublangid => $sublanguage):?>
												<li><a href="javascript:YL.Language.fieldchange(<?php echo $sublanguage->id;?>)"> <?php echo trans('messages.'.$sublanguage->name);?></a></li>
											<?php endforeach;?>
										</ul>
									</div>
									<!-- input-group-btn -->
								</div>
							<?php $i++; endforeach;?>  
							<div id="map" style=" width: 565px; height:400px;margin-top:10px;float:left;"></div>
							<div class="gllpMap"></div>
							<input type="hidden" class="gllpZoom" value="18"/>
							<input type="hidden" class="gllpUpdateButton" value="update map">
						</div>
					</div>
					<input type="hidden" name="latitude" class="gllpLatitude form-control" id="lat" value="" >
					<input type="hidden" name="longitude" class="gllpLongitude form-control" id="lng" value="" >
					<?php /*
					<div class="form-group">
						<label  class="col-sm-3 control-label">@lang('messages.Latitude') <span class="asterisk">*</span></label>
						<div class="col-sm-7">
							<input type="text" name="latitude" class="gllpLatitude form-control" id="lat" value="" >
						</div>
					</div>
					<div class="form-group">
						<label  class="col-sm-3 control-label">@lang('messages.Longitude') <span class="asterisk">*</span></label>
						<div class="col-sm-7">
							<input type="text" name="longitude" class="gllpLongitude form-control" id="lng" value="" >
						</div>
					</div> 
					*/ ?>
					<div class="form-group">
						<label  class="col-sm-3 control-label">@lang('messages.Status') </label>
						<div class="col-sm-7">	
							<?php $checked1 = ""; ?>
							<input type="checkbox" class="toggle" name="active_status" data-size="small" <?php echo $checked1;?> data-on-text="@lang('messages.Yes')" data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
						</div>
					</div>

					<div class="form-group">
						<label  class="col-sm-3 control-label">@lang('messages.Online Payment') </label>
						<div class="col-sm-7">	
							<?php $checked1 = ""; ?>
							<input type="checkbox" class="toggle" name="online_payment" data-size="small" <?php echo $checked1;?> data-on-text="@lang('messages.Yes')" data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
						</div>
					</div>					
			</div>
			<div class="tab-pane" id="vendor_info">
				<legend>@lang('messages.Hotel Description')</legend>
				<div class="form-group">
					<label  class="col-sm-3 control-label">@lang('messages.Amenities') <span class="asterisk">*</span></label>
					<div class="col-sm-9">
						<div class="checkbox" style="display: inline-block;">
							<?php $old = old('amenity_type');  $amtiy = array(); if ($old){	$amtiy = $old;	}	?>	
							<table>	
							@if(count($amty) > 0)
								<?php $i=1; ?>
								@if($i%3 == 1)
									<tr>
								@endif
								@foreach($amty as $value)
							  		<td><label class="checkbox-inline"><input type="checkbox" name="amenity_type[]"  value="{{ $value->id }}" <?php echo in_array($value->id, $amtiy)? "checked" : ""; ?>>
							  			<span class="mr10">{{	$value->amenity_name }}</span></label></td>
							  		@if($i%3 == 0 || $i == count($amty))
							  			</tr>
							  		@endif
							  	<?php $i++; ?>
							  	@endforeach
							@endif
						</table>
						</div>	
					</div>
				</div>				
			    <div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Facilities') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<?php $i = 0; foreach($languages as $langid => $language):?>
						<div class="input-group translatable_field language-<?php echo $language->id;?>" <?php if($i > 0):?>style="display: none;"<?php endif;?>>
							  <textarea name="facilities[<?php echo $language->id;?>]" id="suffix_<?php echo $language->id;?>"  placeholder="<?php echo trans('messages.Facilities').trans('messages.'.'('.$language->name.')');?>" class="form-control" rows="5">{!! Input::old('facilities.'.$language->id) !!}</textarea>
						 
							<div class="input-group-btn">
								<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><?php echo $language->name;?> <span class="caret"></span></button>
								<ul class="dropdown-menu pull-right">
									<?php foreach($languages as $sublangid => $sublanguage):?>
										<li><a href="javascript:YL.Language.fieldchange(<?php echo $sublanguage->id;?>)"> <?php echo trans('messages.'.$sublanguage->name);?></a></li>
									<?php endforeach;?>
								</ul>
							</div><!-- input-group-btn -->
						</div>
						<?php $i++; endforeach;?>
					</div>
			    </div>
		
			    <div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Highlights') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<?php $i = 0; foreach($languages as $langid => $language):?>
						<div class="input-group translatable_field language-<?php echo $language->id;?>" <?php if($i > 0):?>style="display: none;"<?php endif;?>>
							  <textarea name="highlights[<?php echo $language->id;?>]" id="suffix_<?php echo $language->id;?>"  placeholder="<?php echo trans('messages.Highlights').trans('messages.'.'('.$language->name.')');?>" class="form-control" rows="5">{!! Input::old('highlights.'.$language->id) !!}</textarea>
						 
							<div class="input-group-btn">
								<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><?php echo $language->name;?> <span class="caret"></span></button>
								<ul class="dropdown-menu pull-right">
									<?php foreach($languages as $sublangid => $sublanguage):?>
										<li><a href="javascript:YL.Language.fieldchange(<?php echo $sublanguage->id;?>)"> <?php echo trans('messages.'.$sublanguage->name);?></a></li>
									<?php endforeach;?>
								</ul>
							</div><!-- input-group-btn -->
						</div>
						<?php $i++; endforeach;?>
					</div>
			    </div>	

			    <div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Hotel Rules') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<?php $i = 0; foreach($languages as $langid => $language):?>
						<div class="input-group translatable_field language-<?php echo $language->id;?>" <?php if($i > 0):?>style="display: none;"<?php endif;?>>
							  <textarea name="hotel_rules[<?php echo $language->id;?>]" id="suffix_<?php echo $language->id;?>"  placeholder="<?php echo trans('messages.Hotel Rules').trans('messages.'.'('.$language->name.')');?>" class="form-control" rows="5">{!! Input::old('hotel_rules.'.$language->id) !!}</textarea>
						 
							<div class="input-group-btn">
								<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><?php echo $language->name;?> <span class="caret"></span></button>
								<ul class="dropdown-menu pull-right">
									<?php foreach($languages as $sublangid => $sublanguage):?>
										<li><a href="javascript:YL.Language.fieldchange(<?php echo $sublanguage->id;?>)"> <?php echo trans('messages.'.$sublanguage->name);?></a></li>
									<?php endforeach;?>
								</ul>
							</div><!-- input-group-btn -->
						</div>
						<?php $i++; endforeach;?>
					</div>
			    </div>

			    <div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Near By') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<?php $i = 0; foreach($languages as $langid => $language):?>
						<div class="input-group translatable_field language-<?php echo $language->id;?>" <?php if($i > 0):?>style="display: none;"<?php endif;?>>
							  <textarea name="near_by[<?php echo $language->id;?>]" id="suffix_<?php echo $language->id;?>"  placeholder="<?php echo trans('messages.Near By').trans('messages.'.'('.$language->name.')');?>" class="form-control" rows="5">{!! Input::old('near_by.'.$language->id) !!}</textarea>
						 
							<div class="input-group-btn">
								<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><?php echo $language->name;?> <span class="caret"></span></button>
								<ul class="dropdown-menu pull-right">
									<?php foreach($languages as $sublangid => $sublanguage):?>
										<li><a href="javascript:YL.Language.fieldchange(<?php echo $sublanguage->id;?>)"> <?php echo trans('messages.'.$sublanguage->name);?></a></li>
									<?php endforeach;?>
								</ul>
							</div><!-- input-group-btn -->
						</div>
						<?php $i++; endforeach;?>
					</div>
			    </div>	

			    <div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Property Image') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<input type="file" name="property_image"/>
						<span class="help-text">@lang('messages.Please upload 263X127 images for better quality')</span>
					</div>
				</div>

			    <div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Cancellation Policy') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<?php $i = 0; foreach($languages as $langid => $language):?>
						<div class="input-group translatable_field language-<?php echo $language->id;?>" <?php if($i > 0):?>style="display: none;"<?php endif;?>>
							  <textarea name="cancellation_policy[<?php echo $language->id;?>]" id="suffix_<?php echo $language->id;?>"  placeholder="<?php echo trans('messages.Cancellation Policy').trans('messages.'.'('.$language->name.')');?>" class="form-control" rows="5">{!! Input::old('cancellation_policy.'.$language->id) !!}</textarea>
						 
							<div class="input-group-btn">
								<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><?php echo $language->name;?> <span class="caret"></span></button>
								<ul class="dropdown-menu pull-right">
									<?php foreach($languages as $sublangid => $sublanguage):?>
										<li><a href="javascript:YL.Language.fieldchange(<?php echo $sublanguage->id;?>)"> <?php echo trans('messages.'.$sublanguage->name);?></a></li>
									<?php endforeach;?>
								</ul>
							</div><!-- input-group-btn -->
						</div>
						<?php $i++; endforeach;?>
					</div>
			    </div>
				<div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Directions') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
					  <input type="text" name="directions" value="{!! old('directions') !!}" placeholder="@lang('messages.Directions')"  class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Landmark') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
					  <input type="text" name="landmark" value="{!! old('landmark') !!}" placeholder="@lang('messages.Landmark')"  class="form-control" />
					</div>
				</div>				    
				    						
			</div>
			<div class="tab-pane" id="contact_info">
				<div class="form-group">
					<label class="col-sm-2 control-label">@lang('messages.Place of Interest') <span class="asterisk">*</span></label>
					<div class="col-sm-10">
						<div class="row" id="append_div">
							<div class="col-sm-3">
								<input type="text" name="place_name[]" class="form-control" placeholder="Place Name" value="{!! old('place_name[0]') !!}">
							</div>
							<div class="col-sm-3">
								<select name="places[]" id="places" class="form-control" >
									<option value="">@lang('messages.Select Place')</option>
										@if (count(getPlaceTypes()) > 0)
											@foreach (getPlaceTypes() as $key => $type)
												<option value="{{ $key }}" <?php echo (old('places')==$key)?'selected="selected"':''; ?> ><?php echo trans('messages.'.$type); ?></option>
											@endforeach
										@endif
								</select>								
							</div>
							<div class="col-sm-3">
								<input type="text" name="distance[]" class="form-control" placeholder="Distance" value="{!! old('distance[0]') !!}">
							</div>							
							<div class="col-sm-3">
			 			    	<a class="btn btn-primary" id="add" name="add">Add More <i class="fa fa-plus"></i></a>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" value="1" id="total_count" name="total_count">				
			</div>
			<div class="tab-pane" id="delivery_info"> 
				<legend>@lang('messages.Contact Information')</legend>
				<div class="form-group">
					<label class="col-sm-3 control-label">@lang('messages.Contact Phone') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
					  <input type="text" name="contact_phone_number" value="{!! old('contact_phone_number') !!}" placeholder="@lang('messages.Contact Phone')"  class="form-control" />
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label">@lang('messages.Contact Email') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<input type="text" name="contact_email" value="{!! old('contact_email') !!}"  maxlength="100" placeholder="@lang('messages.Contact Email')"  class="form-control"  />
					</div>
				</div>	
<?php /*				
				<div class="form-group">
					<label  class="col-sm-3 control-label">@lang('messages.Delivery Time') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<input type="text" name="delivery_time" value="{!! old('delivery_time') !!}"  maxlength="4" placeholder="@lang('messages.Delivery Time')"  class="form-control" />
						<span class="help-block">@lang('messages.Delivery Time')@lang('messages.Like 30 mins,1 hour')</span>
					</div>
				</div>
					<div class="form-group">
					<label  class="col-sm-3 control-label">@lang('messages.Pickup Time') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<input type="text" name="pickup_time" value="{!! old('pickup_time') !!}"  maxlength="4" placeholder="@lang('messages.Pickup Time')"  class="form-control" />
						<span class="help-block">@lang('messages.Pickup Time')@lang('messages.Like 30 mins,1 hour')</span>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-3 control-label">@lang('messages.Cancel Time') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<input type="text" name="cancel_time" value="{!! old('cancel_time') !!}"  maxlength="4" placeholder="@lang('messages.Cancel Time')"  class="form-control" />
						<span class="help-block">@lang('messages.Cancel Time')@lang('messages.Like 30 mins,1 hour')</span>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-3 control-label">@lang('messages.Return Time') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<input type="text" name="return_time" value="{!! old('return_time') !!}"  maxlength="4" placeholder="@lang('messages.Return Time')"  class="form-control" />
						<span class="help-block">@lang('messages.Return Time')@lang('messages.Like 1 or 2 days')</span>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-3 control-label">@lang('messages.Delivery Charges Fixed') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<input type="text" name="delivery_charges_fixed" value="{!! old('delivery_charges_fixed') !!}"  maxlength="5" placeholder="@lang('messages.Delivery Charges Fixed')"  class="form-control" />
						<span class="help-block">@lang('messages.Delivery Charges Fixed')@lang('messages.Fixed for 5km')</span>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-3 control-label">@lang('messages.Delivery Cost Variation') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<input type="text" name="delivery_cost_variation" value="{!! old('delivery_cost_variation') !!}"  maxlength="5" placeholder="@lang('messages.Delivery Cost Variation')"  class="form-control" />
						<span class="help-block">@lang('messages.Delivery Cost Variation')@lang('messages.After 5km every km')</span>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-3 control-label">@lang('messages.Service Tax') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<input type="text" name="service_tax" value="{!! old('service_tax') !!}"  maxlength="5" placeholder="@lang('messages.Service Tax')"  class="form-control"  />
						<span class="help-block">@lang('messages.Service Tax')@lang('messages.Like 5 or 10 percentage')</span>
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-3 control-label">@lang('messages.Minimum Order Amount') <span class="asterisk">*</span></label>
					<div class="col-sm-7">
						<input type="text" name="minimum_order_amount" value="{!! old('minimum_order_amount') !!}"  maxlength="5" placeholder="@lang('messages.Minimum Order Amount')"  class="form-control"  />
						<span class="help-block">@lang('messages.Minimum Order Amount')@lang('messages.Min amount 5')</span>
					</div>
				</div>
*/?>				
			</div>	
			<div class="form-group Loading_Img" style="display:none;">
				<div class="col-sm-4">
					<i class="fa fa-spinner fa-spin fa-3x"></i><strong style="margin-left: 3px;">@lang('messages.Processing...')</strong>
				</div>
			</div>		
				</div><!-- panel-default -->
			</div>
			<div class="panel-footer Submit_button">
				<input type="hidden" name="tab_info" class="tab_info" value="">
				<button type="submit" onclick="HideButton('Submit_button','Loading_Img');" onsubmit="HideButton('Submit_button','Loading_Img');" class="btn btn-primary mr5" title="Save">@lang('messages.Save')</button>
				<button type="reset" title="Cancel" class="btn btn-default" onclick="window.location='{{ url('vendor/outlets') }}'">@lang('messages.Cancel')</button>
			</div><!-- panel-footer -->
			{!!Form::close();!!} 
		</div>
	</div>
</div>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-timepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>
<script type="text/javascript">
	


	$("#add").click(function() {
		var cnt = $("#total_count").val();
		cnt ++;
		$("#total_count").val(cnt);
		//alert(cnt);
		

		var html = "";
		html += '<div class="col-sm-12 remove_div" id="remove_div'+cnt+'">';
		html += '<div class="row">';
		html += '<div class="col-sm-3">';
		html += '<input type="text" name="place_name[]" class="form-control" placeholder="Place Name">';
		html += '</div>';
		html += '<div class="col-sm-3">';
		html += '<select name="places[]" id="places" class="form-control" >';
		html += '<option value="">@lang('messages.Select Place')</option>';
		html += '@if (count(getPlaceTypes()) > 0)';
		html += '@foreach (getPlaceTypes() as $key => $type)';
		html += '<option value="{{ $key }}" <?php echo (old('places')==$key)?'selected="selected"':''; ?> ><?php echo trans('messages.'.$type); ?></option>';
		html += '@endforeach';
		html += '@endif';
		html += '</select>';
		html += '</div>';
		html += '<div class="col-sm-3">';
		html += '<input type="text" name="distance[]" class="form-control" placeholder="Distance">';
		html += '</div>';	
		html += '<div class="col-sm-3">';
	 	html += '<a class="btn btn-danger btn_remove" id="'+cnt+'" name="remove"><i class="fa fa-minus"></i></a>';
		html += '</div>';
		html += '</div>';
		html += '</div>';




		$("#append_div").append(html);
	
		//	$("#append_div").append('<input type="text" name="amenities_name[]" class="form-control">');

	});


	$(document).on('click', '.btn_remove' , function(){

		//	alert('Ak');

		//	$(this).closest('<br>').remove();

		var btn_id = $(this).attr("id");

		//	alert(btn_id);

		$('#remove_div'+btn_id+'').remove();

		--cnt;

	});


</script>
<script type="text/javascript">
$( document ).ready(function() {
	$('#delivery_areas').select2();
	$('#placecategories').select2();
	$('#pricecategories').select2();
	$('#accomodation_type').select2();
	// Time Picker
	$('.timepicker').timepicker({defaultTIme: false});
	@if(old('tab_info')=='vendor_info')
		$('.tab_info').val('vendor_info');
		$('#login_info').hide();
		$('#delivery_info').hide();
		$('#contact_info').hide();
		$('#vendor_info').show();
	@elseif(old('tab_info')=='login_info')
		$('.tab_info').val('login_info');
		$('#login_info').show();
		$('#delivery_info').hide();
		$('#contact_info').hide();
		$('#vendor_info').hide();
	@elseif(old('tab_info')=='delivery_info')
		$('.tab_info').val('delivery_info');
		$('#login_info').hide();
		$('#delivery_info').show();
		$('#contact_info').hide();
		$('#vendor_info').hide();
	@elseif(old('tab_info')=='contact_info')
		$('.tab_info').val('contact_info');
		$('#login_info').hide();
		$('#delivery_info').hide();
		$('#contact_info').show();
		$('#vendor_info').hide();
	@endif
});
$(".login_info").on("click", function(){
	$('.tab_info').val('login_info');
	$('#delivery_info').hide();
	$('#vendor_info').hide();
	$('#login_info').show();
	$('#contact_info').hide();
});
$(".vendor_info").on("click", function(){
	$('.tab_info').val('vendor_info');
	$('#delivery_info').hide();
	$('#vendor_info').show();
	$('#login_info').hide();
	$('#contact_info').hide();
});
$(".delivery_info").on("click", function(){
	$('.tab_info').val('delivery_info');
	$('#delivery_info').show();
	$('#vendor_info').hide();
	$('#login_info').hide();
	$('#contact_info').hide();
});
$(".contact_info").on("click", function(){
	$('.tab_info').val('contact_info');
	$('#delivery_info').hide();
	$('#vendor_info').hide();
	$('#login_info').hide();
	$('#contact_info').show();
});
$(window).load(function(){
	$('form').preventDoubleSubmission();	
});
$('#country_id').change(function(){
	var cid, token, url, data;
	token = $('input[name=_token]').val();
	cid = $('#country_id').val();
	url = '{{url('list/CityList')}}';
	data = {cid: cid};
	$.ajax({
		url: url,
		headers: {'X-CSRF-TOKEN': token},
		data: data,
		type: 'POST',
		datatype: 'JSON',
		success: function (resp) {
			//console.log('in--'+resp.data);
			$('#city_id').empty();
			if(resp.data==''){
				$('#city_id').append($("<option></option>").attr("value","").text('No data there..')); 
			} else {
				$.each(resp.data, function(key, value) {
					//console.log(value['id']+'=='+value['city_name']);
					$('#city_id').append($("<option></option>").attr("value",value['id']).text(value['city_name'])); 
			   });
			}
		}
	});
});
$('#city_id').change(function(){
	var city_id, country_id, token, url, data;
	token = $('input[name=_token]').val();
	country_id = 1; /*$('#country_id').val(); */
	city_id = $('#city_id').val();
	url = '{{url('list/LocationList')}}';
	console.log(city_id+'--'+country_id);
	data = {city_id: city_id,country_id:country_id};
	$.ajax({
		url: url,
		headers: {'X-CSRF-TOKEN': token},
		data: data,
		type: 'POST',
		datatype: 'JSON',
		success: function (resp) {
			//console.log('in--'+resp.data);
			$('#location_id,#delivery_areas').empty();
			$('#s2id_delivery_areas .select2-choices .select2-search-choice').remove();
			if(resp.data==''){
				$('#location_id,#delivery_areas').append($("<option></option>").attr("value","").text('No data there..')); 
			} else {
				$.each(resp.data, function(key, value) {
					//console.log(value['id']+'=='+value['city_name']);
					$('#location_id,#delivery_areas').append($("<option></option>").attr("value",value['id']).text(value['zone_name'])); 
			   });
			}
		}
	});
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo env('GOOGLE_MAP_API_KEY'); ?>"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-gmaps-latlon-picker.js') }}"></script>
<script type="text/javascript">
	var geocoder = new google.maps.Geocoder();
    function geocodePosition(pos) {
		geocoder.geocode({
			latLng: pos
		}, function(responses) {
			if (responses && responses.length > 0) {
			} else {

			}
		});
    }
    function updateMarkerStatus(str) {
    }
    function updateMarkerPosition(latLng) {
		document.getElementById('lat').value=latLng.lat(); 
		document.getElementById('lng').value=latLng.lng();
    }
    function initialize() {
		var latLng = new google.maps.LatLng(29.776255,45.9416983);
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 10,
			center: latLng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		google.maps.event.trigger(map, "resize");
		var marker = new google.maps.Marker({
			position: latLng,
			title: 'Drag this Marker',
			map: map,
			draggable: true
		});
		var input = /** @type {HTMLInputElement}*/(document.getElementById('target'));
		var searchBox = new google.maps.places.SearchBox(input);
		var markers = [];
		google.maps.event.addListener(searchBox, 'places_changed', function() {
			var places = searchBox.getPlaces();
			for (var i = 0, marker; marker = markers[i]; i++) {
			marker.setMap(null);
			}
			markers = [];
			var bounds = new google.maps.LatLngBounds();
			for (var i = 0, place; place = places[i]; i++) {
			var marker = new google.maps.Marker({
				map: map, 
				title: place.name,
				position: place.geometry.location,
				draggable: true
			});
			markers.push(marker);
			dragmarker(marker);
			updateMarkerPosition(place.geometry.location);
			geocodePosition(place.geometry.location);
			bounds.extend(place.geometry.location);
			}
			map.fitBounds(bounds);
		});
		  
		// Update current position info.
		updateMarkerPosition(latLng);
		geocodePosition(latLng);
		dragmarker(marker); 
    }
    function dragmarker(marker)
    {
		google.maps.event.addListener(marker, 'dragstart', function() {
			updateMarkerAddress('Searching...');
		});
		google.maps.event.addListener(marker, 'drag', function() {
			updateMarkerStatus('Dragging...');
			updateMarkerPosition(marker.getPosition());
		});
		google.maps.event.addListener(marker, 'dragend', function() {
			updateMarkerStatus('Drag ended');
			geocodePosition(marker.getPosition());
		});
    }
    // Onload handler to fire off the app.
    google.maps.event.addDomListener(window, 'load', initialize); 
    function disableEnterKey(e)
    {
		var key;      
		if(window.event)
			key = window.event.keyCode; //IE
		else
			key = e.which; //firefox
		return (key != 13);
    } 
</script>
@endsection
