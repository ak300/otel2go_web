@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/buttons.html5.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
			<li>@lang('messages.Housekeeping Staffs')</li>
		</ul>
		<h4>@lang('messages.Housekeeping Staffs')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->
	<!-- will be used to show any messages -->
	@if (Session::has('message'))
		<div class="admin_sucess_common">
	<div class="admin_sucess">
		<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div></div></div>
	@endif
<div class="contentpanel">
@if(has_staff_permission('vendor/house_keeping/create'))
<div class="buttons_block pull-right">
<div class="btn-group mr5">
<a class="btn btn-primary tip" href="{{ URL::to('vendor/house_keeping/create') }}" title="Add New">@lang('messages.Add New')</a>
</div>
</div>
@endif
 <table id="vendor-table" class="table table-striped table-bordered responsive">
    <thead>
        <tr class="headings">
			
            <th>@lang('messages.S.no')</th>
            <th>@lang('messages.Housekeeping Staff Name')</th> 
			<th>@lang('messages.Outlet Name')</th> 
            <th>@lang('messages.Mobile')</th> 
            <th>@lang('messages.Created date')</th>
			<th>@lang('messages.Updated Date')</th>
			<th>@lang('messages.Status')</th>
			<?php if(has_staff_permission('vendor/house_keeping/edit')) { ?> 
            <th>@lang('messages.Actions')</th>
            <?php } ?>
        </tr>
    </thead>
</table>
</div>

<script>
$(function() {
    $('#vendor-table').DataTable({
		dom: 'Blfrtip',
		buttons: [
		],
        processing: true,
        serverSide: false,
		responsive: true,
		autoWidth:false,
        ajax: '{!! route('anyajaxhousekeeping.data') !!}',
        "order": [],
		"columnDefs": [ {
		  "targets"  : 'no-sort',
		  "orderable": false,
		}],
        columns: [
			{ data: 'id', name: 'id',orderable: false },
			{ data: 'firstname', name: 'firstname',searchable:true },
			{ data: 'outlet_name', name: 'outlet_name',searchable:true },
			{ data: 'mobile_number', name: 'mobile',searchable:true  },
            { data: 'created_at', name: 'created_at' },
			{ data: 'updated_at', name: 'updated_at' },
			{ data: 'status', name: 'status' },
			<?php if(has_staff_permission('vendor/house_keeping/edit')) { ?>
            { data: 'action', name: 'action', orderable: false, searchable: false}
            <?php } ?>
        ],
    });
});
</script>
@endsection
