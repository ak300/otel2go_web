@extends('layouts.vendors')
@section('content')
<div class="row">	
	<div class="col-md-12 ">
		<!-- Nav tabs -->
		<div class="pageheader">
			<div class="media">
				<div class="pageicon pull-left">
					<i class="fa fa-home"></i>
				</div>
				<div class="media-body">
					<ul class="breadcrumb">
						<li><a href="#"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
						<li>@lang('messages.Housekeeping Staffs')</li>
					</ul>
					<h4>@lang('messages.View Housekeeping Staffs')</h4>
				</div>
			</div><!-- media -->
		</div><!-- pageheader -->

		<div class="contentpanel">
			<ul class="nav nav-tabs"></ul>
			<div class="tab-content mb30">
				<div class="tab-pane active" id="home3">
					<!-- Task Name -->
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Task Name') :</label>
						<div class="col-sm-3">
							{{ $data[0]->taskname }}
						</div>
					</div>
					 <div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Housekeeping Staff Name') :</label>
						<div class="col-sm-3">
							{{ $data[0]->firstname }} {{ $data[0]->lastname }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Room Name') :</label>
						<div class="col-sm-3">
							{{ $data[0]->room_name }}
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Date') :</label>
						<div class="col-sm-6">
							{{ $data[0]->task_date }}
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Time') :</label>
						<div class="col-sm-6">
							{{ $data[0]->task_time }}
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Created Date') :</label>
						<div class="col-sm-6">
							{{ $data[0]->created_at }}
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Updated Date') :</label>
						<div class="col-sm-6">
							{{ $data[0]->updated_at }}
						</div>
					</div> 
					 <div class="panel-footer">
					<button type="button"  onclick="window.location='{{ url('vendor/housekeeping_tasks') }}'" class="btn btn-primary mr5">@lang('messages.Cancel')</button>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


