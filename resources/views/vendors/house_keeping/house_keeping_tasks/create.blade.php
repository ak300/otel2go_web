@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script> 
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-timepicker.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-timepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" /> 
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>

<div class="row">
    <div class="col-md-12 ">
        <!-- Nav tabs -->
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-home"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
                        <li>@lang('messages.Housekeeping Tasks')</li>
                    </ul>
                    <h4>@lang('messages.Add Housekeeping Tasks')</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><?php echo trans('messages.'.$error); ?> </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <ul class="nav nav-tabs"></ul>
            {!!Form::open(array('url' => 'create_housekeeping_task', 'method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'create_housekeeping_task_form', 'files' => true));!!} 
                <div class="tab-content mb30">
                    <div class="tab-pane active" id="home3">
                        
                       <?php /* <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Outlet Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select name="outlet_name" id="outlet_name" class="form-control"  >
                                    <option value="">@lang("messages.Select Outlet")</option>
                                    <?php $outlet_list = get_outlet_list(Session::get('vendor_id')); ?>
                                    @if(count($outlet_list) > 0)
                                        @foreach($outlet_list as $ot)
                                            <option value="{{$ot->id}}" @if(old('outlet_name') == $ot->id) {{'selected'}} @endif >{{ ucfirst($ot->outlet_name) }}</option>
                                        @endforeach
                                    @else
                                        <option value="">@lang("messages.No Outlet Found")</option>
                                    @endif
                                </select>
                            </div>
                        </div> */?>
                        <input type="hidden" name="outlet_name" value="{{ Session::get('property_id') }}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Room Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select name="room_name" id="room_name" class="form-control"  >
                                    <option value="">@lang("messages.Select the Room")</option>
                                    @if(count($room_details) > 0)
                                        @foreach($room_details as $key => $value)
                                                <option value="<?php echo $value->id;?>" <?php echo ($value->id == old('room_name'))?"selected":""; ?>><?php  echo $value->room_name; ?></option>
                                         @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Housekeeper Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select name="housekeeper_name" id="housekeeper_name" class="form-control"  >
                                    <option value="">@lang("messages.Select the Housekeeper")</option>
                                    @if(count($housekeeper_details) > 0)
                                        @foreach($housekeeper_details as $key => $value)
                                                <option value="<?php echo $value->id;?>" <?php echo ($value->id == old('housekeeper_name'))?"selected":""; ?>><?php  echo $value->firstname.' '.$value->lastname; ?></option>
                                         @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Task Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="task_name" maxlength="56" placeholder="@lang('messages.Task Name')" class="form-control" value="{!! old('task_name') !!}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Date')<span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="date" autocomplete="off" value="{!! old('date') !!}" placeholder="mm/dd/yyyy" id="datepicker" onkeydown="return false">
                                    <span class="input-group-addon datepicker-trigger"><i class="glyphicon glyphicon-calendar" id="dob"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="time">
                            <label class="col-sm-2 control-label">@lang('messages.Time') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                     <div class="bootstrap-timepicker">
                                        <input type="text" name="time" class="timepicker form-control"  value="{{ old('time') }}">
                                        <div class="bootstrap-timepicker-widget dropdown-menu"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary mr5" title="Save">@lang('messages.Save')</button>
                        <button type="reset" title="Cancel" class="btn btn-default" onclick="window.location='{{ url('vendor/housekeeping_tasks') }}'">@lang('messages.Cancel')</button>
                    </div>
                </div>
            {!!Form::close();!!} 
        </div>
    </div>
</div>

<script> 
    $(window).load(function(){
        $('input.timepicker').timepicker({ timeFormat: 'h:mm:ss p' });
        $('input.timepicker').val('');
        $('.timepicker').timepicker({
            defaultTime: false, 
            showSeconds:true, 
            showMeridian:false
        });
        $('#datepicker').datepicker({
            yearRange: '<?php echo date("Y") - 100; ?>:<?php echo date("Y"); ?>',
            minDate: new Date(),
            format: 'YYYY-MM-DD',
            changeMonth: true,
            changeYear: true
        }); 
        $(".datepicker-trigger").on("click", function() {
           // $("#datepicker").datepicker("show");
        });
    });

    $(document).ready(function(){
        /*
        $('#outlet_name').change(function(){
            var outlet_id = $(this).val();
            $.ajax({
            type: "POST",
                dataType: "json",
                url: '{{ url('RoomsListing') }}',
                data: { _token: "{{csrf_token()}}",property_id:outlet_id},
                success: function(response){
                    var options = '';
                    options +='<option value="">Select Room</option>'
                    $.each(response.data, function (key, val) {
                        options +='<option value="'+val.id+'">'+val.room_name+'</option>'
                    });
                    $('#room_name').html(options);
                },
            });

            $.ajax({
            type: "POST",
                dataType: "json",
                url: '{{ url('HousekeepersList') }}',
                data: { _token: "{{csrf_token()}}",property_id:outlet_id},
                success: function(response){
                    var options = '';
                    options +='<option value="">Select Housekeeper</option>'
                    $.each(response.data, function (key, val) {
                        options +='<option value="'+val.id+'">'+val.firstname+' '+val.lastname+'</option>'
                    });
                    $('#housekeeper_name').html(options);
                },
            });
        });*/
    });
</script>
@endsection
