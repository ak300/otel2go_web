@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
			<li>@lang('messages.Room Type')</li>
		</ul>
		<h4>@lang('messages.Room Type')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->
<div class="contentpanel">
@if(has_staff_permission('vendor/room_type/create'))	
<div class="buttons_block pull-right">
<div class="btn-group mr5">
<a class="btn btn-primary tip" href="{{ URL::to('vendor/room_type/create') }}" title="Add New">@lang('messages.Add New')</a>
</div>
</div>
@endif
@if (Session::has('message'))
		<div class="admin_sucess_common">
	<div class="admin_sucess">
    <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div>
    </div></div>
@endif

 <table id="city-table" class="table table-striped table-bordered responsive">
    <thead>
        <tr class="headings">
			
            <th>@lang('messages.S.no')</th> 
            <th>@lang('messages.Room Name')</th> 
            <th>@lang('messages.Normal Price')</th>    
            <th>@lang('messages.Discount Price')</th> 
            <th>@lang('messages.Adult Count')</th> 
            <th>@lang('messages.Child Count')</th> 
            <th>@lang('messages.Created date')</th> 
            <th>@lang('messages.Status')</th>
            <?php if(has_staff_permission('vendor/room_type/edit')) { ?>
            <th>@lang('messages.Actions')</th> 
            <?php } ?>
        </tr>
    </thead>
<tbody>
<tr>
<td class="empty-text" colspan="7" style="background-color: #fff!important;">
<div class="list-empty-text"> @lang('messages.No records found.') </div>
</td>
</tr>
</tbody>
</table>
</div>

<script>
$(function() {
    $('#city-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('ajaxroomtype.data') !!}',
        "order": [],
		"columnDefs": [ {
		  "targets"  : 'no-sort',
		  "orderable": false,
		  
		}],
        columns: [
			{ data: 'cid', name: 'room_type.cid',orderable: false },
			{ data: 'room_type', name: 'room_type' },
            { data: 'normal_price', name: 'normal_price' },
            { data: 'discount_price', name: 'discount_price' },
            { data: 'adult_count', name: 'adult_count' },
            { data: 'child_count', name: 'child_count' },
            { data: 'created_date', name: 'created_date' },
            { data: 'default_status', name: 'default_status' },
            <?php if(has_staff_permission('vendor/room_type/edit')) { ?>
            { data: 'action', name: 'action', orderable: false, searchable: false}
            <?php } ?>
        ],
    });
});
</script>
@endsection
