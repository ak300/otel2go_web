@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" /> 
<div class="row">	
	<div class="col-md-12 ">
<!-- Nav tabs -->
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
			<li>@lang('messages.Room Type')</li>
		</ul>
		<h4>@lang('messages.Add Room Type')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
			<ul>
				@foreach ($errors->all() as $error)
					<li><?php echo trans('messages.'.$error); ?> </li>
				@endforeach
			</ul>
		</div>
		@endif
<ul class="nav nav-tabs"></ul>
       {!!Form::open(array('url' => 'createroom_type', 'method' => 'post','class'=>'tab-form attribute_form','id'=>'amenity_form','files' => true));!!}
	<div class="tab-content mb30">
	<div class="tab-pane active" id="home3">		
		<div class="form-group">
                <label class="col-sm-2 control-label">@lang('messages.Room Type Name') <span class="asterisk">*</span></label>
                <div class="col-sm-10">
                    <?php $i = 0; foreach($languages as $langid => $language):?>
                    <div class="input-group translatable_field language-<?php echo $language->id;?>" <?php if($i > 0):?>style="display: none;"<?php endif;?>>
                          <input type="text" name="room_type[<?php echo $language->id;?>]" id="suffix_<?php echo $language->id;?>"  placeholder="<?php echo trans('messages.Room Type').trans('messages.'.'('.$language->name.')');?>" class="form-control" value="{!! Input::old('room_type.'.$language->id) !!}" maxlength="32" />
                     
                        <div class="input-group-btn">
                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><?php echo $language->name;?> <span class="caret"></span></button>
                            <ul class="dropdown-menu pull-right">
                                <?php foreach($languages as $sublangid => $sublanguage):?>
                                    <li><a href="javascript:YL.Language.fieldchange(<?php echo $sublanguage->id;?>)"> <?php echo trans('messages.'.$sublanguage->name);?></a></li>
                                <?php endforeach;?>
                            </ul>
                        </div><!-- input-group-btn -->
                    </div>
                    <?php $i++; endforeach;?>
                </div>
        </div>

		<div class="form-group">
			<label  class="col-sm-2 control-label">@lang('messages.Normal Price') <span class="asterisk">*</span></label>
			<div class="col-sm-10">
				<input type="text" id="normal_price" name="normal_price" value="{!! old('normal_price') !!}"  maxlength="8" placeholder="@lang('messages.Normal Price')"  class="form-control" />
			<?php /*	<span class="help-block">@lang('messages.Normal Price')@lang('messages.Like 30 mins,1 hour')</span> */?>
			</div>
		</div>

		<div class="form-group">
				<label class="col-sm-2 control-label">@lang('messages.Discount Price') <span class="asterisk">*</span></label>
				<div class="col-sm-10">
				  <input type="text" id="discount_price" name="discount_price" value="{!! old('discount_price') !!}"   maxlength="8" placeholder="@lang('messages.Discount Price')"  class="form-control"  />
				 <?php /*  <span class="help-block">@lang('messages.An unique numeric code for the city eg:(356)')</span>*/?>
				 <input type="hidden" name="discount_perc" value="" id="discount_perc">
				</div>
		</div>

		<div class="form-group">
			<label  class="col-sm-2 control-label">@lang('messages.Adult Count') <span class="asterisk">*</span></label>
			<div class="col-sm-10">
				<input type="text" name="adult_count" value="{!! old('adult_count') !!}"  maxlength="4" placeholder="@lang('messages.Adult Count')"  class="form-control" />
		<?php /* 	<span class="help-block">@lang('messages.Normal Price')@lang('messages.Like 30 mins,1 hour')</span> */?>
			</div>
		</div>

		<div class="form-group">
				<label class="col-sm-2 control-label">@lang('messages.Child Count') <span class="asterisk">*</span></label>
				<div class="col-sm-10">
				  <input type="text" name="child_count" value="{!! old('child_count') !!}"   maxlength="3" placeholder="@lang('messages.Child Count')"  class="form-control"  />
			<?php /*    <span class="help-block">@lang('messages.An unique numeric code for the city eg:(356)')</span> */?>
				</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">@lang('messages.Description') <span class="asterisk">*</span></label>
			<div class="col-sm-10">
				<?php $i = 0; foreach($languages as $langid => $language):?>
				<div class="input-group translatable_field language-<?php echo $language->id;?>" <?php if($i > 0):?>style="display: none;"<?php endif;?>>
					  <textarea name="description[<?php echo $language->id;?>]" id="suffix_<?php echo $language->id;?>"  placeholder="<?php echo trans('messages.Description').trans('messages.'.'('.$language->name.')');?>" class="form-control" rows="5">{!! Input::old('description.'.$language->id) !!}</textarea>
				 
					<div class="input-group-btn">
						<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><?php echo $language->name;?> <span class="caret"></span></button>
						<ul class="dropdown-menu pull-right">
							<?php foreach($languages as $sublangid => $sublanguage):?>
								<li><a href="javascript:YL.Language.fieldchange(<?php echo $sublanguage->id;?>)"> <?php echo trans('messages.'.$sublanguage->name);?></a></li>
							<?php endforeach;?>
						</ul>
					</div><!-- input-group-btn -->
				</div>
				<?php $i++; endforeach;?>
			</div>
	   </div>
		<?php 
		/*		
				<div class="form-group ">
					<label class="col-sm-2 control-label">Country <span class="asterisk">*</span></label>
					<div class="col-sm-10">
						<select class="form-control" name="country">
							<option value="">Select Country</option>
							<?php foreach(getCountryLists() as $value) { */ ?>
		<?php /*					
								<option value="{!! $value->id !!}"  @if (Input::old('country') == $value->id) selected @endif >{!! $value->country_name !!}</option>
							<?php } */ ?>
		<?php /*					
						</select>
					</div>
				</div>
		*/
		?>	
		<div class="form-group">
			<label class="col-sm-2 control-label">@lang('messages.Image') <span class="asterisk">*</span></label>
			<div class="col-sm-10">
				<input type="file" name="image"/>
				<span class="help-text">@lang('messages.Please upload 263X127 images for better quality')</span>
			</div>
		</div>
<?php
/*
		<div class="form-group">
			<label class="col-sm-2 control-label">@lang('messages.Extra Amenities') <span class="asterisk">*</span></label>
			<div class="col-sm-10">
				<div class="row" id="append_div">
					<div class="col-sm-3">
						<input type="text" name="amenities_name[]" class="form-control" placeholder="Amenities Name" value="{!! old('amenities_name[0]') !!}">
					</div>
					<div class="col-sm-3">
						<input type="file" name="amenities_image[]"/>
					</div>
					<div class="col-sm-3">
	 			    	<a class="btn btn-primary" id="add" name="add">Add More <i class="fa fa-plus"></i></a>			
					</div>
				</div>
			</div>
		</div>
*/
	?>
		<div class="form-group">
		  <label  class="col-sm-2 control-label">@lang('messages.Status')</label>
			<div class="col-sm-10">
            <?php $checked = ""; ?>
            <?php if(old('status')) { $checked = "checked"; } ?>
            <input type="checkbox" class="toggle" name="status" data-size="small" <?php echo $checked;?> data-on-text="@lang('messages.Yes')" data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
			</div>
	   </div>
					
       </div>
		<div class="panel-footer">
			<input type="hidden" value="1" id="total_count" name="total_count">
		<button class="btn btn-primary mr5" title="Save">@lang('messages.Save')</button>
		<button type="reset" title="Cancel" class="btn btn-default" onclick="window.location='{{ url('vendor/room_type') }}'">@lang('messages.Cancel')</button>
		</div>
        </div>
      
 {!!Form::close();!!} 
</div></div></div>
<script type="text/javascript">
	
/*	

	$("#add").click(function() {
		var cnt = $("#total_count").val();
		cnt ++;
		$("#total_count").val(cnt);
		

		var html = "";
		html += '<div class="col-sm-12" id="remove_div'+cnt+'">';
		html += '<div class="row">';
		html += '<div class="col-sm-3">';
		html += '<input type="text" name="amenities_name[]" class="form-control" placeholder="Amenities Name">';
		html += '</div>';
		html += '<div class="col-sm-3">';
		html += '<input type="file" name="amenities_image[]"/>';
		html += '</div>';
		html += '<div class="col-sm-3">';
	 	html += '<a class="btn btn-danger btn_remove" id="'+cnt+'" name="remove"><i class="fa fa-minus"></i></a>';
		html += '</div>';
		html += '</div>';
		html += '</div>';


		$("#append_div").append(html);
	
		//	$("#append_div").append('<input type="text" name="amenities_name[]" class="form-control">');

	});


	$(document).on('click', '.btn_remove' , function(){

		//	alert('Ak');

		//	$(this).closest('<br>').remove();

		var btn_id = $(this).attr("id");

		alert(btn_id);

		$('#remove_div'+btn_id+'').remove();

		--cnt;

	});
*/


$(function(){
    
    $('#normal_price').on('input', function() {
      calculate();
    });
    $('#discount_price').on('input', function() {
     calculate();
    });
    function calculate(){
        var pPos = parseInt($('#normal_price').val()); 
        var pEarned = parseInt($('#discount_price').val());
        var perc="";
        if(isNaN(pPos) || isNaN(pEarned)){
            perc=" ";
           }else{
           perc = (100 - (pEarned/pPos) * 100).toFixed(2);
           //	perc = (100 - perc)
           }
        
        $('#discount_perc').val(perc);
    }

});
</script>
<script>
$(window).load(function(){	
	$('form').preventDoubleSubmission();	
});
</script>
@endsection
