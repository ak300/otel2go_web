@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/buttons.html5.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/toastr.css') }}" rel="stylesheet" />
<script src="{{ URL::asset('assets/admin/base/js/toastr.min.js') }}"></script>
 <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-home"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
                        <li>@lang('messages.Billing')</li>
                    </ul>
                    <h4>@lang('messages.Billing')</h4>
                </div>
            </div>
            <!-- media -->
        </div>
<div class="contentpanel">
	@if(Session::has('message'))
		<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<span>{!!Session::get('message');!!}</span>
		</div>
	@endif
		<?php  if(count($current_paln)>0){ ?>
        <div class="top_billing_common"> 
			<div class="form-horizontal"> 
				<div class="col-md-4">
					<div class="row">
						<div class="form-group">
							<div class="col-sm-12"><h4>Name of the plan | <a href='javascript:;' class="get_plans">Change plan</a></h4> </div>
							<div class="col-sm-12"><p><?php echo $current_paln[0]->subscription_plan_name;?></p></div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row">
						<div class="form-group">
							<div class="col-sm-12"><h4>Next billing date : </h4></div>
								<div class="col-sm-12"><p><?php echo $plan_endate; ?></p></div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row">
						<div class="form-group">
							<div class="col-sm-12"><h4>Account : </h4></div>
							<div class="col-sm-12"><p><?php echo $user->first_name;?></p></div>
						</div>
					</div>
				</div>
			</div>
        </div>
	
		<?php  } else { /*  ?>
		<div class="top_apply_info">
		<div class="col-sm-12 padding_left0"><p>Currently you have not subscribed for any plan please<a href='javascript:;' class="get_plans"> Choose plan here</a></p></div> 
		</div> 
		<?php */ }?>
		
		<?php /*<table>
			@foreach ($invoices as $invoice)
				<tr>
					<td>{{ $invoice->date()->toFormattedDateString() }}</td>
					<td>{{ $invoice->total() }}</td>
					<td><a href="/user/invoice/{{ $invoice->id }}">Download</a></td>
				</tr>
			@endforeach
		</table> */ ?>
		<div class="top_bil_sec" style="margin-top:10px">
		<h4>Payment history</h4>
		<table id="ingredientTable" class="table table-striped table-bordered responsive display nowrap">
			<thead>
				<tr>
					<th>@lang('messages.Transaction id')</th>
					<th>@lang('messages.Plan name')</th>
					<th>@lang('messages.Amount')</th>
					<th>@lang('messages.Payment type')</th>
					<th>@lang('messages.Created Date')</th>
					<th>@lang('messages.Payment status')</th>
					<th>@lang('messages.Invoice')</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="empty-text" colspan="7" style="background-color: #fff!important;">
						<div class="list-empty-text"> @lang('messages.No records found.') </div>
					</td>
				</tr>
			</tbody>
		</table>
   </div>
			
    </div>		

<script>

$(function() {
    $('#ingredientTable').DataTable({
        processing: true,
        serverSide: false,
        responsive: true,
        autoWidth:false,
        ajax: '{!! route('listbillingAjaxadmin.data') !!}',
        "order": [],
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
            "searchable":true,
            "pagingType": "full",
            'exportable':false
        }],
        columns: [
            { data: 'transaction_id', name: 'transaction_id'},
            { data: 'subscription_plan_name', name: 'subscription_plan_name'},
            { data: 'total_amount', name: 'total_amount' },
            { data: 'payment_type', name: 'payment_type' },
            { data: 'created_date', name: 'created_date' },
            { data: 'payment_status', name: 'payment_status' },
            { data: 'invoice', name: 'invoice' },
            
        ],
    });
});



</script>
@endsection




















