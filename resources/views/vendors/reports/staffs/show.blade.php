@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
<script src="{{ URL::asset('/assets/admin/base/js/toastr.min.js') }}"></script> 
<link href="{{ URL::asset('/assets/admin/base/css/toastr.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/buttons.html5.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<?php /*
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-datetimepicker.min.js') }}"></script>
<link href="{{ URL::asset('/assets/admin/base/css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<style>
  	.appointment_date{z-index:1080 !important;}	
.clsDatePicker {
    z-index: 100000;
}
</style>
*/ ?>
<div class="row">	
	<div class="col-md-12 ">
<!-- Nav tabs -->
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="#"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
			<li>@lang('messages.Daily Report')</li>
		</ul>
		<h4>@lang('messages.Daily Report')  - <?php  echo $date;  ?></h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
<div class="buttons_block pull-right">
<div class="btn-group mr5">
	<?php	/*
<a class="btn btn-primary tip" href="{{ URL::to('vendor/editbooking/' . $data->bid . '') }}" title="Edit" >@lang('messages.Edit')</a>

*/	?>
</div>
</div>
<div>
    <h3>List of Bookings</h3>
    <table id="manager-table" class="table table-striped table-bordered responsive">
        <thead>
            <tr class="headings">
                
                <?php  /*  <th>@lang('messages.S.no')</th> */  ?>
                <th>@lang('messages.Room')</th> 
                <th>@lang('messages.User Name')</th>  
                <th>@lang('messages.Charge Type')</th>  
                <th>@lang('messages.Check-in date')</th>              
                <th>@lang('messages.Check-out date')</th>
                <th>@lang('messages.Created date')</th>
                <?php /*    <th>@lang('messages.Actions')</th>  */  ?> 
                <th>@lang('messages.No of Days')</th>
            </tr>
        </thead>
             
        <tbody>
            @foreach($booking_details as $bd)
            <tr>
                <?php   /*  <td>{{  $charges->bc_id         }}</td>    */  ?>
                <td>{{  $bd->room_id     }}</td>
                <td>{{  $bd->firstname     }}</td>
                    @if($bd->booking_status == 1)
                        <td style="color:<?php echo $bd->color_code; ?>">{{ $bd->name }}</td>
                    @elseif($bd->booking_status == 2)
                        <td style="color:<?php echo $bd->color_code; ?>">{{ $bd->name }}</td>
                    @elseif($bd->booking_status == 3)
                        <td style="color:<?php echo $bd->color_code; ?>">{{ $bd->name }}</td>
                    @elseif($bd->booking_status == 4)
                        <td style="color:<?php echo $bd->color_code; ?>">{{ $bd->name }}</td>
                    @elseif($bd->booking_status == 5)
                        <td style="color:<?php echo $bd->color_code; ?>">{{ $bd->name }}</td>
                    @elseif($bd->booking_status == 6)
                        <td style="color:<?php echo $bd->color_code; ?>">{{ $bd->name }}</td>      
                    @endif                              
                
                <?php   /*  <td>{{  $bd->room_type     }}</td> */  ?>
                <td>{{  $bd->check_in_date  }}</td>             
                <td>{{  $bd->check_out_date }}</td>
                <td>{{  $bd->created_date }}</td>
                <td>{{  $bd->no_of_days     }}</td>              
                
            <tr>
            @endforeach
        </tbody>
    </table>
<hr>
    <div><h4> Total : {{    $booking_charges->booking_charge  }}</h4></div>
<hr>



	<h3>List of Charges</h3>
	<table id="manager-table" class="table table-striped table-bordered responsive">
	    <thead>
	        <tr class="headings">
				
	            <?php  /*  <th>@lang('messages.S.no')</th> */  ?>
                <th>@lang('messages.Booking ID')</th> 
	            <th>@lang('messages.User Name')</th>  
	            <th>@lang('messages.Amount')</th>
	            <th>@lang('messages.Charge Type')</th>  
	         	<th>@lang('messages.Description')</th>	            
	            <th>@lang('messages.Created date')</th>
				<?php /*	<th>@lang('messages.Status')</th>	*/	?>
	         	<?php /*	<th>@lang('messages.Actions')</th>  */	?> 
	         	<th>@lang('messages.Status')</th>
	        </tr>
	    </thead>
	         
		<tbody>
			@foreach($charges_list as $charges)
			<tr>
				<?php   /*  <td>{{	$charges->bc_id			}}</td>    */  ?>
                <td>{{  $charges->booking_random_id     }}</td>
				<td>{{	$charges->firstname 	}}</td>
				<td>{{	$charges->price 		}}</td>
					@if($charges->charge_type == 1)
						<td><?php echo 'Room Charge';?></td>
					@elseif($charges->charge_type == 2)
						<td><?php echo 'Cleaning Charge';?></td>
					@elseif($charges->charge_type == 3)
						<td><?php echo 'Food Charge';?></td>
					@elseif($charges->charge_type == 4)
						<td><?php echo 'Extra Charges';?></td>
					@endif				
				
				<?php	/*	<td>{{	$charges->room_type		}}</td>	*/	?>
				<td>{{	ucfirst($charges->bs_notes)}}</td>				
				<td>{{	$charges->created_date  }}</td>
				<td>
                    @if($charges->booking_status==0)
                        <span class="label label-warning">Inactive</span>
                    @elseif($charges->booking_status==1)
                        <span class="label label-success">Active</span>
                    @elseif($charges->booking_status==2)
                        <span class="label label-danger">Delete</span>
                    @endif			
                </td>				
				
			<tr>
			@endforeach
		</tbody>
	</table>
<hr>
	<div><h4> Total : {{	$booking_charges->booking_charge  }}</h4></div>
<hr>

	<h3>List of Payments</h3>
	<table id="manager-table" class="table table-striped table-bordered responsive">
	    <thead>
	        <tr class="headings">
				
	            <?php  /*  <th>@lang('messages.S.no')</th> */  ?>
                <th>@lang('messages.Booking ID')</th>  
	            <th>@lang('messages.User Name')</th>
	            <th>@lang('messages.Amount')</th>
	            <th>@lang('messages.Payment Method')</th>  
				<?php	/*	<th>@lang('messages.Email')</th> */	?>
	            <th>@lang('messages.Description')</th> 
	            <th>@lang('messages.Created date')</th>
				<th>@lang('messages.Status')</th>
	         	<?php /*	<th>@lang('messages.Actions')</th> */	?>
	        </tr>
	    </thead>
	         
		<tbody>
			@foreach($payment_list as $pay)
			<tr>
				<?php   /*  <td>{{	$pay->payment_id	}}</td> */  ?>
                <td>{{  $pay->booking_random_id       }}</td>
				<td>{{	$pay->firstname		}}</td>
				<td>{{	$pay->paid_amount	}}</td>
					@if($pay->paid_type == 1)
						<td><?php echo 'Cash';?></td>
					@endif
				<?php	/*	<td>{{	$pay->email			}}</td>	*/	?>
				
				<td>{{	ucfirst($pay->description)	}}</td>
				<td>{{	$pay->created_date	}}</td>
				<td>
                    @if($pay->payment_status==0)
                        <span class="label label-warning">Inactive</span>
                    @elseif($pay->payment_status==1)
                        <span class="label label-success">Active</span>
                    @elseif($pay->payment_status==2)
                        <span class="label label-danger">Delete</span>
                    @endif			
                </td>
			<tr>
			@endforeach
		</tbody>
	</table>


<hr>
<div><h4>Payment Total :  {{	$payments->paid_amount  }}</h4></div>
<hr>
<div><h3> <?php /* Due Balance: {{	$booking_charges->booking_charge - $payments->paid_amount	}} */ ?>

     @if($booking_charges->booking_charge > $payments->paid_amount)
        Due Balance :{{  $booking_charges->booking_charge - $payments->paid_amount }}

     @elseif($booking_charges->booking_charge > $payments->paid_amount)
        Due Balance :{{ "0" }}

     @elseif($booking_charges->booking_charge < $payments->paid_amount)

        Extra Balance :{{ $payments->paid_amount - $booking_charges->booking_charge }}
        
     @endif

</h3></div>
</div>		
</div>
@endsection


