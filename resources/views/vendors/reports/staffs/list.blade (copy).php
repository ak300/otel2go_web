@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/datatables2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-datetimepicker.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/datatables2.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-datetimepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>
<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-home"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
                <li>@lang('messages.Reports')</li>
            </ul>
            <h4>@lang('messages.Transaction Reports')</h4>
        </div>
    </div><!-- media -->
</div><!-- pageheader -->
<div class="contentpanel">
    @if (Session::has('message'))
        <div class="admin_sucess_common">
            <div class="admin_sucess">
                <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div>
            </div>
        </div>
    @endif

    {!!Form::open(array('method' => 'POST','class'=>'tab-form attribute_form','id'=>'reports_order_form','files' => true));!!}
        <div class="form-group">
            <div class="col-md-6 padding0">
                <label class="col-sm-3 control-label padding_left0">@lang('messages.Date Start')</label>
                <div class="col-sm-9">
                    <input type="text"  name="from" value="{{   $date_data->min_date }}" autocomplete="off" id="datepicker" placeholder="mm/dd/yyyy"  class="form-control"  />
                </div>
            </div>
            <div class="col-md-6 padding0">
                <label class="col-sm-3 control-label">@lang('messages.Date End')</label>
                <div class="col-sm-9">
                    <input type="text"  name="to" value="{{   $date_data->max_date }}"  autocomplete="off" id="datepicker1"  placeholder="mm/dd/yyyy"  class="form-control"  />
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary mr5" title="@lang('messages.Save')">@lang('messages.Search')</button>
        </div>
    {!!Form::close();!!}
<div class="contentpanel">

<div class="col-md-12">
    {{ csrf_field() }}
    <div style="display: none;" id="container" style="height: 400px; min-width: 600px"></div>

    <script src="http://code.highcharts.com/stock/highstock.js"></script>
    <script src="http://code.highcharts.com/stock/modules/exporting.js"></script>
</div><!-- row -->

<script type="text/javascript">
    
<?php  if(count(json_decode($charges_amount_encoded,1))> 0){ ?>

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Account'
    },
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            day: '%b %e'
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Charges & Payments'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr>' +
            '<td style="padding:0"><b>{point.y} MYR</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            //  pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    {
        name: 'Charges',
        data: <?php  echo $charges_amount_encoded; ?>

    },
    {
        name: 'Payments',
        data: <?php  echo $payment_amount_encoded; ?>

    },    
    ]
});

<?php   } else  {   ?>

     $('#container').hide();

<?php   }   ?>

</script>

</div>
    <div class="vender_scroll_sec">
        <table id="ReportsOrdersTable" class="table table-striped table-bordered responsive">
            <thead>
                <tr class="headings">
                    <th>@lang('messages.Date')</th>
                    <th>@lang('messages.No. Bookings')</th>
                    <th>@lang('messages.Charges')</th> 
                    <th>@lang('messages.Payments')</th>
                </tr>
            </thead>
            <tbody>
                @foreach($gs as $charges)
                    <tr>
                        <td>{{  $charges['date']             }}</td>
                        <td>{{  $charges['booking_count']     }}</td>
                        <td>{{  $charges['booking_charges']   }}</td>              
                        <td>{{  $charges['payments']          }}</td>            
                    <tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script> 
    $(window).load(function(){
        $('#order_status').select2();
        $('#group_by').select2();
        $('#datepicker').datetimepicker();
        $('#datepicker1').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datepicker").on("dp.change", function (e) {
            $('#datepicker1').data("DateTimePicker").minDate(e.date);
        });
        $("#datepicker1").on("dp.change", function (e) {
            $('#datepicker').data("DateTimePicker").maxDate(e.date);
        });
        /*
        $('#datepicker').datetimepicker({
            maxDate: new Date(),
            sideBySide: true,
            useCurrent: false
        });
        $('#datepicker1').datetimepicker({
            maxDate: new Date(),
            sideBySide: true,
            useCurrent: false
        });*/
    });
/*    
    $(function()
    {
        var oTable = $('#ReportsOrdersTable').DataTable({
        bFilter: false,
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'collection',
                text: 'Export',
                title: 'vendor_orders_reports',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ],
        processing: true,
        serverSide: true,
        responsive: true,
        autoWidth:true,
        ajax: {
            url: '{{ URL::to('vendors/report_orders_list') }}',
            type: 'POST',
            data: function (d) {
                d.from         = $('input[name=from]').val();
                d.to           = $('input[name=to]').val();
                //  d.group_by     = $('#group_by').val();
                //  d.order_status = $('#order_status').val();
            },
            headers:{
                'X-CSRF-TOKEN': $('input[name=_token]').val()
            }
        },
        order: [],
           columnDefs: [ {
               targets  : 'no-sort',
               orderable: false,
           }],
        "order": [[ 0, "desc" ]],
        columns: [
                { data: 'booking_random_id', name:'booking_random_id'},
                { data: 'firstname', name: 'firstname' },
                { data: 'date_start', name: 'date_start'},
                { data: 'date_end', name: 'date_end'},
                //  { data: 'orders_count', name: 'orders_count' },
                //  { data: 'quantity_count', name: 'quantity_count' },
                //  { data: 'tax_total', name: 'tax_total' },
                { data: 'total_charges', name: 'total_charges' },
                { data: 'total_payments', name: 'total_payments' },
                { data: 'due_amount', name: 'due_amount'},
            ],
        
        });
        $('#reports_order_form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    });
*/    
	$('.dropdown-toggle').dropdown();
</script>

@endsection

@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/datatables2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-datetimepicker.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/datatables2.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-datetimepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>
<script src="{{ URL::asset('/assets/admin/base/js/toastr.min.js') }}"></script> 

<link href="{{ URL::asset('/assets/admin/base/css/toastr.css') }}" media="all" rel="stylesheet" type="text/css" />

<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-home"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
                <li>@lang('messages.Reports')</li>
            </ul>
            <h4>@lang('messages.Transaction Reports')</h4>
        </div>
    </div><!-- media -->
</div><!-- pageheader -->
<div class="contentpanel">
    @if (Session::has('message'))
        <div class="admin_sucess_common">
            <div class="admin_sucess">
                <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div>
            </div>
        </div>
    @endif

    {!!Form::open(array('method' => 'POST','class'=>'tab-form attribute_form','id'=>'reports_order_form','files' => true));!!}
        <div class="form-group">
            <div class="col-md-6 padding0">
                <label class="col-sm-3 control-label padding_left0">@lang('messages.Date Start')</label>
                <div class="col-sm-9">
                    <input type="text"  name="from" value="{{   $date_data->min_date }}" autocomplete="off" id="datepicker" placeholder="mm/dd/yyyy"  class="form-control"  />
                </div>
            </div>
            <div class="col-md-6 padding0">
                <label class="col-sm-3 control-label">@lang('messages.Date End')</label>
                <div class="col-sm-9">
                    <input type="text"  name="to" value="{{   $date_data->max_date }}"  autocomplete="off" id="datepicker1"  placeholder="mm/dd/yyyy"  class="form-control"  />
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary mr5" title="@lang('messages.Save')">@lang('messages.Search')</button>
        </div>
    {!!Form::close();!!}
<div class="contentpanel">

<div class="col-md-12">
    {{ csrf_field() }}
    <div style="display: none;" id="container" style="height: 400px; min-width: 600px"></div>

    <script src="http://code.highcharts.com/stock/highstock.js"></script>
    <script src="http://code.highcharts.com/stock/modules/exporting.js"></script>
</div><!-- row -->

<script type="text/javascript">
    
<?php  if(count(json_decode($charges_amount_encoded,1))> 0){ ?>

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Account'
    },
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            day: '%b %e'
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Charges & Payments'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr>' +
            '<td style="padding:0"><b>{point.y} MYR</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            //  pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    {
        name: 'Charges',
        data: <?php  echo $charges_amount_encoded; ?>

    },
    {
        name: 'Payments',
        data: <?php  echo $payment_amount_encoded; ?>

    },    
    ]
});

<?php   } else  {   ?>

     $('#container').hide();

<?php   }   ?>

</script>

</div>
    <div class="vender_scroll_sec">
        <table id="ReportsOrdersTable" class="table table-striped table-bordered responsive">
            <thead>
                <tr class="headings">
                    <th>@lang('messages.Date')</th>
                    <th>@lang('messages.No. Bookings')</th>
                    <th>@lang('messages.Charges')</th> 
                    <th>@lang('messages.Payments')</th>
                </tr>
            </thead>
            <tbody class="dats">
                @foreach($gs as $charges)
                    <tr>
                        <td>{{  $charges['date']             }}</td>
                        <td>{{  $charges['booking_count']     }}</td>
                        <td>{{  $charges['booking_charges']   }}</td>              
                        <td>{{  $charges['payments']          }}</td>            
                    <tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script> 
    $(window).load(function(){
        $('#order_status').select2();
        $('#group_by').select2();
        $('#datepicker').datetimepicker();
        $('#datepicker1').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datepicker").on("dp.change", function (e) {
            $('#datepicker1').data("DateTimePicker").minDate(e.date);
        });
        $("#datepicker1").on("dp.change", function (e) {
            $('#datepicker').data("DateTimePicker").maxDate(e.date);
            roomShowcout();
        });
        
        $('#datepicker').datetimepicker({
            maxDate: new Date(),
            sideBySide: true,
            useCurrent: false
        });
        $('#datepicker1').datetimepicker({
            maxDate: new Date(),
            sideBySide: true,
            useCurrent: false
        });
    });
   
    $('.dropdown-toggle').dropdown();

function roomShowcout()
{
    if($("#from").val() =="")
    {
        toastr.error('Select From Date First');
    }
    else
    {      
        alert('ak');
        //  checkin();
    }
}    

$(document).ready(function(){

    $("#reports_order_form").on("submit",function(){

        alert('Ak');

        checkin();
    });

});

function checkin()
{

    var from = $('input[name=from]').val();
    var to   = $('input[name=to]').val();

    if(from == "")
    {
        toastr.remove();
        toastr.error('Adult Count should Not Empty');
        return false();
    }  
    else if(to == "")
    {
        toastr.remove();
        toastr.error('Child Count should Not Empty');        
        return false();
    }  

      event.preventDefault();

    var token,url;

    token = $("input[name=_token]").val();
    url = '{{   url('summary_check')    }}';

    data = {from:from,to:to};

    $.ajax({

        url : url,
        headers: {'X-CSRF-TOKEN': token},
        data: data,
        type: 'POST',
        datatype: 'JSON',
        success: function (resp) {

            alert('output got');
                        //  $('#room').select2('data', id);
                        if(resp.data!='')
                        { 
                            //  $('#price').val(resp.data.discount_price);
                            $.each(resp.data, function(key, value)
                            {
                                  console.log(value['date']+' '+value['booking_count']);
                                  console.log(value['booking_charges']+'=='+value['payments']);
                                // $('#room_type').append($("<option></option>").attr("value",value['room_type_id']).text(value['room_type']));
                                //  $('#room').append($("<option></option>").attr("value",value['id']).text(value['room_name']));

                                //  $('#price').val(discount_price);
                            });
                        }
                        else {
                            $('#room').text('No Matches Found');
                            toastr.remove();
                            toastr.warning('No Rooms found on Selected Dates');
                            $("#price").val("");
                        }
                    }
    });

}    
</script>

@endsection

