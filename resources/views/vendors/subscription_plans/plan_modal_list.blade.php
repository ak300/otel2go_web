
<div class="row">
	<div class="col-right-border col-sm-12">
		<div class="row">
			<div class="col-sm-12">
				{!!Form::open(array('url' => ['subscription_payment', $data[0]->id], 'method' => 'post','class'=>'tab-form attribute_form','id'=>'subscription_payment','name'=>'subscription_payment','files' => true));!!}
				
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label"><h5>Plan Name</h5></label>
					</div>
					<div class="col-sm-6">
						<label><h5>{{ $data[0]->subscription_plan_name }}</h5></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label"><h5>Price</h5></label>
					</div>
					<div class="col-sm-6">
						<label><h5>
							@if(getCurrencyPosition()->currency_side == 1)
		                    
									{{ getCurrency()." ".$data[0]->subscription_plan_price }}
							@else
									{{ $data[0]->subscription_plan_price." ".getCurrency() }}

							@endif
						</h5></label>
						<input type="hidden" name="plan_price" value="{{ $data[0]->subscription_plan_price }}">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label"><h5>Discount Price</h5></label>
					</div>
					<div class="col-sm-6">
						<label><h5>
							@if(getCurrencyPosition()->currency_side == 1)
		                    
									{{ getCurrency()." ".$data[0]->subscription_discount_price }}
							@else
									{{ $data[0]->subscription_discount_price." ".getCurrency() }}

							@endif
						</h5></label>
						<input type="hidden" name="plan_discount" value="{{ $data[0]->subscription_discount_price }}">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label"><h5>Duration</h5></label>
					</div>
					<div class="col-sm-6">
						<label><h5>{{ $data[0]->subscription_plan_duration }} Days</h5></label>
						<input type="hidden" name="plan_duration" value="{{ $data[0]->subscription_plan_duration }}">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label"><h5>Module Packages</h5></label>
					</div>
					<div class="col-sm-6">
						<?php $my_packages = getSubscription_package($data[0]->id);?>
                        @foreach($my_packages as $my_key => $my_val)
                            <p>
                            @foreach($allpackages as $all_key => $all_val)
                                @if($all_val->id == $my_val->package_id)
                                    {{ $all_val->package_name }}
                                @endif
                            @endforeach
                            </p>    
                        @endforeach
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label"><h5>Payment Method</h5></label>
					</div>
					<div class="col-sm-6">
						<?php $payments = getPaymentlist();	?>
						 @if (count($payments) > 0)
							@foreach($payments as $pay)
								<input  value="{{ $pay->id }}" name="payment_type" id="payment_type" type="radio" {{old('payment_type') == $pay->id ? 'checked' : '' }} required>{{	$pay->name	}}
							@endforeach
						@endif
                    </div>
				</div>	
				<div class="row">
					<div class="col-sm-6">
						<button class="btn btn-primary mr10" type="submit">@lang('messages.Proceed')</button>
					</div>
				</div>
			{!!Form::close();!!}
			</div>
		</div>
	</div>
</div>	
