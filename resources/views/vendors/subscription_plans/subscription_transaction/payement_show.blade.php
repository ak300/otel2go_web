@extends('layouts.vendors')
@section('content')
<div class="row">	
	<div class="col-md-12 ">
		<!-- Nav tabs -->
		<div class="pageheader">
			<div class="media">
				<div class="pageicon pull-left">
					<i class="fa fa-home"></i>
				</div>
				<div class="media-body">
					<ul class="breadcrumb">
						<li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
						<li>@lang('messages.Subscription Transactions')</li>
					</ul>
					<h4>@lang('messages.View Subscription Transaction')</h4>
				</div>
			</div><!-- media -->
		</div><!-- pageheader -->

		<div class="contentpanel">
			<ul class="nav nav-tabs"></ul>
			<div class="tab-content mb30">
				<div class="tab-pane active" id="home3">
					<!-- Task Name -->
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Vendor Name') :</label>
						<div class="col-sm-3">
							{{ Session::get('user_name') }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Plan Name') :</label>
						<div class="col-sm-3">
							{{ $plan_name[0]->subscription_plan_name }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Price') :</label>
						<div class="col-sm-3">
							{{ $plan_discount_price }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Duration') :</label>
						<div class="col-sm-3">
							{{ $plan_duration }} @lang('messages.Days')
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Payment Status') :</label>
						<div class="col-sm-6" style="color:red;">
							 {{ "Payment Fail - ".$reason_code }} 
						</div>
					</div>
					 <div class="panel-footer">
					<button type="button"  onclick="window.location='{{ url('vendors/subscription_transactions') }}'" class="btn btn-primary mr5">@lang('messages.Back')</button>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


