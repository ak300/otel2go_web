@extends('layouts.front')
@section('content')

<script src="{{ URL::asset('/assets/admin/base/js/toastr.min.js') }}"></script> 

<link href="{{ URL::asset('/assets/admin/base/css/toastr.css') }}" media="all" rel="stylesheet" type="text/css" />

        <section class="check_out_bg">
            <div class="container">

                @if (Session::has('reorder-message'))
                <?php /*
                    <div class="admin_sucess_common">
                        <div class="admin_sucess">
                            <div style="background: #f44066;color:black" class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('reorder-message') }}</div>
                        </div>
                    </div>
                */ ?>
                    <script type="text/javascript">
                        toastr.success('Checkout has been updated successfully!');
                    </script>
                @endif                

                <div class="payhotel Guest_Details">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 guest_left padding_15 padding_right0">
                            <div class="gust_detial_left">

                              @if (count($errors) > 0)
                                    @foreach ($errors->all() as $error)
                                            <script type="text/javascript">
                                                  toastr.error("{{ $error }}");
                                            </script>
                                    @endforeach
                              @endif                                

                              <?php 

                                //  IF SESSION CLEARED MEANS IT WILL SET DEFAULT VALUES
/*
                                    $old_date = date('01/01/1970');

                                    if(Session::get('start_date') == "")
                                    {
                                        
                                        $sd = date('m/d/Y');
                                        Session::put('start_date', $sd);
                                        //  print_r('1');



                                    }
                                    elseif(Session::get('start_date') == $old_date)
                                    {
                                        
                                        $sd = date('m/d/Y');
                                        Session::put('start_date',   $sd);
                                        //  print_r('2');
                                        //  dd($sd);
                                    }                                    
                                    else
                                    {
                                        
                                        $sd = Session::get('start_date');
                                        //  print_r('3  ');
                                        //  dd($sd);
                                    
                                    }

                                        $ss = Session::get('start_date');
                                       
                                
                                    if(Session::get('end_date') == "")
                                    {
                                        $ed = date('m/d/Y', strtotime(' +1 day'));
                                        Session::put('end_date',   $ed);

                                        //  SMALL DATE MONTH ISSUE CREATED - IT WAS RECTIFIED
                                    }
                                    elseif(Session::get('end_date') == $old_date)
                                    {
                                        $ed = date('m/d/Y', strtotime(' +1 day'));
                                        Session::put('end_date',   $ed);
                                    }
                                    else
                                    {
                                        $ed = Session::get('end_date');
                                    }  

                                    //  dd($ed);


                                    $sd = Session::get('start_date');
                                    $ed = Session::get('end_date');

                                    //  dd($sd);

                                    //  print_r($sd);print_r($ed);exit;

   

                                // DATE CALCULATION 

                                      //    dd(Session()->all());

                                    $start_date = Session::get('start_date');

                                    $end_date = Session::get('end_date');

                                      //    print_r($start_date);print_r($end_date);exit;

                                    $d1 = str_replace('/', '-', $start_date);
                                    $d2 = str_replace('/', '-', $end_date);



                                     print_r($d1);print_r($d2);exit;

                                    // $d1 = date('m-d-Y',strtotime($d1));

                                    // $d2 = date('m-d-Y',strtotime($d2));

                                    // print_r($d1);print_r($d2);exit;

                                     $s = date_create($d1);
                                     $e = date_create($d2);

                                     print_r($s);print_r($e);exit;

                                     // dd($sd);

                                     $c = date_diff($e,$s);

*/

        //  dd($startDate);

                                    if(Session::get('guest_count') == "")
                                    {
                                        $gc = 1;
                                        Session::put('guest_count',$gc);
                                    }
                                    else
                                    {
                                        $gc = Session::get('guest_count');
                                    }            
                                    if(Session::get('room_count') == "")
                                    {
                                        $rc = 1;
                                        Session::put('room_count', $rc);
                                    }
                                    else
                                    {
                                        $rc = Session::get('room_count');
                                    }  

                               
                                            $disc = $room_type->discount_price;
                                            //  $days = $c->days;

                                            $total = $rc * $disc * $days;


                               ?>      
                              {!!Form::open(array( 'url' => 'payment_details', 'method' => 'post','class'=>'tab-form attribute_form','id'=>'guest_signup','files' => true));!!}    
                                                         
                                <h4>Guest Details</h4>
                                <hr>
                                <?php   /*  $s = Session()->all(); dd($s); */  ?>
                                <label>Name</label>
                                <div class="input-group">
                                    <?php $old_name = old('user_name'); $un = Session::get('user_name');
                                              if($un) { $old_name = $un; } 
                                    ?> 
                                    <input type="text" class="form-control form" required placeholder="Name " 
                                    id="user_name" value="{{ $old_name }}" name="user_name">
                                    <input type="hidden" name="user_id" id="user_id" value="{{ Session::get('user_id') }}">
                                </div>
                                <label>Mobile Number</label>
                                <div class="input-group">
                                    <?php $old_mobile = old('mobile'); $um = Session::get('user_mobile');
                                              if($um) { $old_mobile = $um; }
                                    ?>                                     
                                    <input type="text" id="phone" class="form-control form" required placeholder="Mobile Number" value="{{ $old_mobile }}" name="mobile">
                                    <input type="hidden" name="country_short" id="country_short" 
                                value="{{ Session::get('country_code') }}">
                                    <input id="phone_hidden" type="hidden" name="country_code" value="0">
                                </div>
                                <label>Email Id</label>
                                <div class="input-group">
                                    <?php $old_email = old('email'); $user_email = Session::get('user_email');
                                              if($user_email) { $old_email = $user_email; }
                                    ?>                                          
                                    <input type="text" id="user_email" class="form-control form" required placeholder="Email" value="{{ $old_email }}" name="email">
                                </div>
                                <h1 class="have_you_cop">Have discount Coupon?</h1>
                                <div class="coupon_modules">
                                    <div class="input-group mb-3">

                                        <input type="text" id="promo_code" class="form-control" placeholder="Enter your Coupon">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button" id="coupon_submit">Apply</button>
                                            <button class="btn btn-outline-secondary" type="button" id="coupon_remove" style="display: none;">Remove</button>
                                        </div>
                                    </div>
                                </div>

                            <input type="hidden" name="user_id" value="{{ Session::get('user_id') }}">
<?php /*                             
                            <input type="hidden" name="new_user_id" value="" id="new_user_id">
                            <input type="hidden" name="verify_otp_id" id="verify_otp_id" value="1">
*/  ?>                            
                            <input type="hidden" name="outlet_id" id="outlet_id" value="{{ $properties->id }}">
                            <input type="hidden" name="vendor_id" value="{{ $room_type->created_by }}">
                            <input type="hidden" name="room_type_id" value="{{ $room_type->id }}">
                            <input type="hidden" name="room_count" value="{{ Session::get('room_count') }}">
                            <input type="hidden" name="guest_count" value="{{ Session::get('guest_count') }}">
                            <input type="hidden" name="check_in" value="{{ Session::get('start_date') }}">
                            <input type="hidden" name="check_out" value="{{ Session::get('end_date') }}">
                            <input type="hidden" name="duration" value="{{ Session::get('duration') }}">
                            <input type="hidden" name="no_of_nights" value="{{ $days }}">

                            <!-- COUPON DISCOUNT   -->

                            <input type="hidden" name="coupon_id" id="coupon_id" value="0">
                            <input type="hidden" name="coupon_amount" id="coupon_amount" value="0">
                            <input type="hidden" name="coupon_type" id="coupon_type" value="0">

                            <!-- COST -->

                            <input type="hidden" name="total_cost" id="total_cost" value="{{ $total }}">
                            <input type="hidden" name="original_price" id="original_price" value="{{ $total }}">
                            @if($coupons !="")
                                <div class="booking_info">

                                    @if($coupons->offer_type == 1)
                                        <h6>{{ $coupons->offer_amount." ".getCurrency() }} discount on Booking</h6>
                                    @elseif($coupons->offer_type == 2)
                                        <h6>{{ $coupons->offer_percentage }}% discount on Booking</h6>
                                    @endif
                                    <span> "{{ $coupons->coupon_code }}" coupon code applied</span>
                                </div>
                            @endif
                                <div class="pay_now_section">
                                    <div class="row">

                                        <div class="col">
                                            <button type="submit" class="btn btn-primary btn-lg-12 paynow_btn" id="pay_now">Pay Now </button>
                                        </div>
                                        <div class="col">
                                            <button type="button" class="btn btn-primary btn-lg-12 select_btn" id="pay_at_hotel">Pay at Hotel </button>

                                        </div>
                                    </div>
                                </div>

                                {!!Form::close();!!}

                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 guest_left">
                            <div class="gust_detial_left">
                                <div class="hotel_detail">
                                    <div class="row">
                                        <div class="col-md-3">
                <?php $image = $properties->outlet_image;?>
                <?php $file_path = url('/assets/admin/base/images/vendors/property_image/list/'.$image); ?>
                
                @if (!empty($image) && File::exists(public_path('/assets/admin/base/images/vendors/property_image/list/'.$image)))
                    <img src="<?php echo $file_path;  ?>" alt="" class="hotel_image">
                @else 
                    <img src="<?php echo url('assets/front/otel2go/images/no_hotel.jpg');  ?>" class="hotel_image">
                @endif  

                                        </div>
                                        <div class="col">
                                            <h1>{{  ucfirst($properties->outlet_name)  }}</h1>
                                            <p><i class="glyph-icon flaticon-facebook-placeholder-for-locate-places-on-maps"></i>
                                              {{ $properties->contact_address }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="check_inout check">
                                    <div class="check_info_list">
                                        <div class="row">

                                  <?php 

                                    // $sd = date("d M Y",strtotime($sd));

                                    // $ed = date("d M Y",strtotime($ed));                                  
/*
                                    //  IF SESSION VALUES RESET - IT WILL REDIRECT TO VIEW PROPERTY PAGE

                                    if((Session::get('start_date') == "")){

                                   */ ?><?php /*

                                      <script type="text/javascript">
                                        
                                                  redirect = '{{ url('view-hotel/'.$properties->url_index) }}';    
                                                  window.location.replace(redirect);

                                                  toastr.warning('Please Select Check-in Date & Check-out Date');
                                      </script>
                                    <?php
                                          }
                                          */
                                    ?>                                          
                                            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                <i class="glyph-icon flaticon-left-arrow guest_gyp"></i><span> Check in</span>
                                            </div>
                                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 checkin">
                                                <?php $sdr = date('d M Y', strtotime($startDate)); ?>
                                                <p>{{ $sdr }} <span class="dividers">|</span> 08.00 PM</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                <i class="glyph-icon flaticon-right-arrow guest_gyp"></i><span> Check out</span>
                                            </div>
                                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 checkin">

                                                <?php $edr = date('d M Y', strtotime($endDate)); ?>
                                                <p>{{ $edr }} <span class="dividers">|</span> 08.00 PM</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                <i class="glyph-icon flaticon-double-king-size-bed guest_gyp"></i><span>Room Type</span>
                                            </div>
                                            <div class="col-6 col-sm-6 col-md-6 col-lg-6 checkin">
                                                <p>{{ $room_type->room_type  }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="cust_hr">

                              
                                    <div class="edit_rooms">
                                        <div class="row">
                                            <div class="col-md-8">

                                                <div class="room_info"><i class="glyph-icon flaticon-social"></i> {{ "0".$rc }} Room, {{ "0".$gc }} Guest, {{ $days }} Night</div>

                                                <input type="hidden" name="outlet_id" id="outlet_id" 
                                                value="{{ $properties->id }}">

                                            </div>


                                            <div class="col edit_btn check_btn">
                                                <a href="{{ URL::to('view-hotel/'.$properties->url_index) }}" class="btn btn-primary btn-lg book">Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="cust_hr1">
                                    <div class="payment_details">
                                        <h1 class="pay_titles">Payment details</h1>
                                        <hr class="cust_hr">
                                        <div class="common_booking_info">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <p>Booking Amount</p>
                                                </div>

                                                @if(getCurrencyPosition()->currency_side == 1)
                                                    <?php $disc = getCurrency()." ".$disc;  ?>
                                                @else 
                                                    <?php $disc = $disc." ".getCurrency();    ?>
                                                @endif
                    
                                                <div class="col-md-5"><span>{{ $disc }} x {{ $rc }} Room x {{ $days }} Night</span></div>
                                                <div class="col text-align">

                                                <?php $total_amt = $total; ?>
                                                @if(getCurrencyPosition()->currency_side == 1)
                                                    <?php $total = getCurrency()." ".$total;  ?>
                                                @else 
                                                    <?php $total = $total." ".getCurrency();    ?>
                                                @endif 

                                                    <p id="total">&nbsp;{{ $total  }}</p>
                                                </div>
                                            </div>
                                            <div class="row" style="display: none;" id="coupon_details">
                                                <div class="col-md-4">
                                                    <p>Coupon Discount</p>
                                                </div>
                                                <div class="col-md-5">
                                                    <p></p>
                                                </div>
                                                <div class="col-md text-align">
                                                    <p id="coupon_cost"></p>
                                                </div>

                                            </div>
                                        </div>
                                        <hr>
                                        <div class="pay_now_buttons">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <h1> Payable Amount</h1>
                                                    <p>(Inclusive of taxes)</p>

                                                </div>                                              
                                                <div class="col-md"><b id="total_pay">{{ $total  }}</b></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="space"></div>
            </div>
      </section>

<!--  BOOKING VERIFICATION MODAL  -->

          <div class="modal fade in book_verifycode"  data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="singup_info">
                  <div class="sign_up book_verifycode">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                      <div class="signup_inner_containt">
                        <div class="signup_image">
                          <img src="<?php echo url('/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.Session::get("general")->logo.'?'.time()); ?>"  title="<?php echo ucfirst(Session::get("general")->site_name);?>" alt="logo">
                          <h4>BOOKING VERIFICATION CODE</h4>
                          <i class="signup_line"></i>
                        </div>
                        {!!Form::open(array('method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'booking_verify_user'));!!}
                          <p>We send verification code to your email & mobile number.
                            Please enter the code.
                          </p>
                          <div class="input-group verify_input">
                            <input type="text" class="form-control" placeholder="OTP" aria-label="Recipient's username" aria-describedby="basic-addon2" id="otp_number" name="otp_number">
                            <div class="input-group-append">

                            <!-- USER ID -->
                            <input type="hidden" name="user_id" value="{{ Session::get('user_id') }}">
                            <input type="hidden" name="new_user_id" value="" id="new_user_id">
                            
                            <input type="hidden" name="verify_otp_id" id="verify_otp_id" value="1">
                            <input type="hidden" name="outlet_id" id="outlet_id2" value="{{ $properties->id }}">
                            <input type="hidden" name="vendor_id" value="{{ $room_type->created_by }}">
                            <input type="hidden" name="room_type_id" value="{{ $room_type->id }}">
                            <input type="hidden" name="room_count" value="{{ Session::get('room_count') }}">
                            <input type="hidden" name="guest_count" value="{{ Session::get('guest_count') }}">
                            <input type="hidden" name="check_in" value="{{ Session::get('start_date') }}">
                            <input type="hidden" name="check_out" value="{{ Session::get('end_date') }}">
                            <input type="hidden" name="duration" value="{{ Session::get('duration') }}">
                            <input type="hidden" name="no_of_nights" value="{{ $days }}">

                            <!-- COUPON DISCOUNT   -->

                            <input type="hidden" name="coupon_id" id="coupon_id2" value="0">
                            <input type="hidden" name="coupon_amount" id="coupon_amount2" value="0">
                            <input type="hidden" name="coupon_type" id="coupon_type2" value="0">

                            <!-- COST -->

                            <input type="hidden" name="total_cost" id="total_cost2" value="{{ $total_amt }}">
                            <input type="hidden" name="original_price" id="original_price2" value="{{ $total_amt }}">                            


                              <button class="btn btn-outline-secondary confirm" type="button" id="confirm_otp">Confirm</button>
                            </div>
                          </div>
                          <a class="resend" onclick ="resendotp();" href="#" >Resend OTP</a>
                        {!!Form::close();!!}
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>    
          
<!--  BOOKING VERIFICATION MODAL  -->  


    <script>
      $('.listing_header').show();
    </script>
    <script type="text/javascript">
      $(document).ready(function(){

          $('#pay_at_hotel').click(function(){

              //  alert('Ak');
              
              if($("#user_name").val() == ""){
                toastr.remove();
                toastr.error('Enter Guest Name');
                return false;
              }
              if($("#phone").val() == ""){
                toastr.remove();
                toastr.error('Enter Mobile Number');
                return false;
              }
              if($("#user_email").val() == ""){
                toastr.remove();
                toastr.error('Enter Email ID');
                return false;
              }              


                guestsignup();

          });
/*
          $('#pay_now').click(function(){

              //  alert('Ak');            

                guestsignupnow();

                //  alert('Ak');

          });          
*/
          $('#confirm_otp').click(function(){

              //  alert('Ak');
              if($("#otp_number").val() == ""){
                toastr.remove();
                toastr.error('Enter OTP');
                return false;
              }

              bookingotpverify();
          });

          $('#coupon_remove').on('click', function() 
          {
              //  $(".offer_amount").hide();
              
              var total = {{  $total_amt  }};
              var total_pay = parseFloat(total).toFixed(1);

              $("#total_pay").text(total_pay +' {{ getCurrency() }}');
              $("#coupon_remove").hide();
              $("#coupon_submit").show();                  
              $("#coupon_details").hide();
              $("#coupon_id").val('');
              $("#coupon_amount").val('');
              $("#coupon_id2").val('');
              $("#coupon_amount2").val('');              
              //  $("#promo_code").val('');   //  COUPON CODE VALUE MAKES EMPTY
              $('#promo_code').attr('readonly', false);
              toastr.remove();
              toastr.warning("Coupon Removed Successfully");
          });

          $("#coupon_submit").on('click',function(){
              //  alert('Ak');

              var promo_code = $('#promo_code').val();
              var outlet_id  = $('#outlet_id').val();
              var total  = {{ $total_amt }};

              //  alert(total);

              if(promo_code == "")
              {
                  toastr.remove();
                  toastr.error('Enter Promo Code');
                  return false;
              }

            var c_url = '/update-promcode';
            token = $('input[name=_token]').val();
            $.ajax({
                url: c_url,
                headers: {'X-CSRF-TOKEN': token},
                data: {promo_code:promo_code,outlet_id:outlet_id,total:total},
                type: 'POST',
                datatype: 'JSON',
                success: function (resp) 
                {

                  
                    //  $(this).button('reset');
                    //  $("#fadpage").hide();
                    var total = {{ $total_amt }}; //  $("#total").text();

                    //  toastr.success(total);
                    //  var sub_total= $("#sub_total").text();
                    var total_pay = "";
                    if(resp.httpCode == 400)
                    {
                        toastr.error(resp.Message);
                        return false;
                    }

                      //  toastr.success('Ak');

                    if(parseInt(resp.coupon_details.offer_type) == 1)
                    {

                         total_pay = parseFloat((total - resp.coupon_details.offer_amount)).toFixed(1);
                    }
                    else
                    {
                        total_pay = parseFloat((total - (total * resp.coupon_details.offer_percentage)/100)).toFixed(1);
                    }
/*                    
                    console.log(resp.coupon_details.offer_percentage);
                    console.log(total_pay);
                    console.log(resp.coupon_details.coupon_type);
*/
                      //  toastr.error(total_pay +' {{ getCurrency() }}');
                    var cc = parseFloat((total - total_pay)).toFixed(1);

                    //  alert(cc);

                    @if(getCurrencyPosition()->currency_side == 1)
                        $("#coupon_cost").text('{{ getCurrency()." " }}' + cc);    
                    @else 
                        $("#coupon_cost").text(cc +' {{ " ".getCurrency() }}');
                    @endif                       
                    
                    @if(getCurrencyPosition()->currency_side == 1)
                        $("#total_pay").text('{{ getCurrency()." " }}' + total_pay);
                    @else
                        $("#total_pay").text(total_pay +' {{ getCurrency() }}');
                    @endif 

                    $("#total_cost").val(total_pay);
                    $("#total_cost2").val(total_pay);
                    $("#coupon_details").show();
                    $("#coupon_remove").show();
                    $("#coupon_submit").hide();
                    $(".total_pay").show();
                    if(parseInt(resp.coupon_details.offer_type) == 1){

                    $("#offer_amount").text(parseFloat(resp.coupon_details.offer_amount).toFixed(2));

                   //alert(parseFloat(resp.coupon_details.offer_amount).toFixed(2));
                    $("#coupon_amount").val(parseFloat(resp.coupon_details.offer_amount).toFixed(2));
                    }else{
                         //alert(parseFloat((sub_total*resp.coupon_details.offer_percentage)/100));
                    $("#offer_amount").text(parseFloat((total * resp.coupon_details.offer_percentage)/100).toFixed(2));
                    
                    $("#coupon_amount").val(parseFloat((total * resp.coupon_details.offer_percentage)/100).toFixed(2));

                    }
                    $("#coupon_id").val(resp.coupon_details.coupon_id);
                    $("#coupon_amount").val(cc);
                    $("#coupon_type").val(resp.coupon_details.coupon_type);
                    $("#coupon_id2").val(resp.coupon_details.coupon_id);
                    $("#coupon_amount2").val(cc);
                    $("#coupon_type2").val(resp.coupon_details.coupon_type);                    
                    $('#promo_code').attr('readonly', true);
                    $("#apply_promocode").hide();
                    $("#remove_promocode").show();
                    toastr.remove();
                    toastr.success("Coupon Applied Successfully");
                    return false;
                },
                error:function(resp)
                {
                    console.log('out--'+data); 
                    return false;
                }
            });
            return false;             

        
          });

      });  

    </script> 
    <script type="text/javascript">
      
//  PAY AT HOTEL

      function guestsignup(){   
          //To set country code in hidden input 
          var countryData = $("#phone").intlTelInput("getSelectedCountryData");
          $("#phone_hidden").val('+'+countryData.dialCode);

          data = $("#guest_signup").serializeArray();
          data.push({ name: "login_type", value: "1" });
          var c_url = '/signup_guest';
          token = $('input[name=_token]').val();
          $.ajax({
              url: c_url,
              headers: {'X-CSRF-TOKEN': token},
              data: data,
              type: 'POST',
              datatype: 'JSON',
              success: function (resp)
              {
                  //  $this.button('reset');
                  data = resp;
                  if(data.httpCode == 200)
                  {  
                      //  $('.signup').modal('hide');
                      toastr.success(data.Message);

                      $('#new_user_id').val(data.last_id);
                      //location.reload(true);
                      //toastr.options.onShown = function() { location.reload(true); }
                      //  $('#verify_otp_id').val(data.last_id);
                      //  $('#verifycode').modal('show');
                      //  $('#sign_up').trigger('reset');
                      //  $this.html('Sign Up');
                      $('.book_verifycode').modal('show');                  
                  }
                  else
                  {
                      if(data.httpCode == 400)
                      {
                          $.each(data.Message,function(key,val){
                              toastr.warning(val);
                          });
                      }
                      else if(resp.httpCode == 700)
                        {
                            //  toastr.warning('Ak');
                            toastr.success(resp.Message);
                            //  $('.signup').modal('hide');
                            $('#verifycode').modal('show');
                            //  $('#sign_up').trigger('reset');
                            $('#verify_otp_id').val(resp.user_id);
                            $('#verify_user_id').val(resp.user_id);
                            //  $this.html('Sign Up');                  
                        }                      
                       else {
                          toastr.warning(data.Message);
                      }
                      //    $this.html('Sign Up');
                  }
              },
              error:function(resp)
              {
                
                  //    $this.button('reset');
                  //    $("#ajaxloading").hide();
                  console.log('out--'+data); 
                  return false;
              }
          });
          return false;
      }

  //   PAY NOW
      function guestsignupnow(){   

          //To set country code in hidden input 
          var countryData = $("#phone").intlTelInput("getSelectedCountryData");
          $("#phone_hidden").val('+'+countryData.dialCode);
          data = $("#guest_signup").serializeArray();
          data.push({ name: "login_type", value: "1" });
          var c_url = '/signup_guest_now';
          token = $('input[name=_token]').val();
          $.ajax({
              url: c_url,
              headers: {'X-CSRF-TOKEN': token},
              data: data,
              type: 'POST',
              datatype: 'JSON',
              success: function (resp)
              {
                  //  $this.button('reset');
                  data = resp;
                  if(data.httpCode == 200)
                  {  
                      //  toastr.success(data.Message);
                      $('#new_user_id').val(data.last_id);
                      booking_page();          
                  }
                  else
                  {
                      if(data.httpCode == 400)
                      {
                          $.each(data.Message,function(key,val){
                              toastr.warning(val);
                          });
                      } else {
                          toastr.warning(data.Message);
                      }
                     //  $this.html('Sign Up');
                  }
              },
              error:function(resp)
              {
                
                 //  $this.button('reset');
                  //$("#ajaxloading").hide();
                  console.log('out--'+data); 
                  return false;
              }
          });
          return false;
      }

/*
      function booking_page(){

          var c_url = '/payment_details_page';
          data = $("#booking_verify_user").serializeArray();
          token = $('input[name=_token]').val();
          $.ajax({
              url: c_url,
              headers: {'X-CSRF-TOKEN': token},
              data: data,
              type: 'POST',
              datatype: 'JSON',
              success: function (resp)
              {
                  data = resp;
                  if(data.httpCode == 200)
                  {  
                  
                    toastr.success(data.Message);
                    redirect = '{{ url('thankyou/') }}'+'/'+data.booking_id;
                    window.location.replace(redirect);                          
                 
                  }
                  else
                  {
                      toastr.warning(data.Message);
                      //  $this.html('Confirm');
                  }
              },
              error:function(resp)
              {
                  $this.button('reset');
                  console.log('out--'+data); 
                  return false;
              }
          });
          return false;
      }
*/
      function bookingotpverify(){

          $this = $(".btn-outline-secondary");
          //    $this.html('Processing....');
          $("#confirm_otp").html('Processing....');

          var c_url = '/verify_guest_otp';
          data = $("#booking_verify_user").serializeArray();
          token = $('input[name=_token]').val();
          $.ajax({
              url: c_url,
              headers: {'X-CSRF-TOKEN': token},
              data: data,
              type: 'POST',
              datatype: 'JSON',
              success: function (resp)
              {
                  data = resp;
                  if(data.httpCode == 200)
                  {  
                    $('.book_verifycode').modal('hide');
                    toastr.success(data.Message);
                    $('#booking_verify_user').trigger('reset');
                    redirect = '{{ url('thankyou/') }}'+'/'+data.booking_id;
                    window.location.replace(redirect);                          
                  }
                  else
                  {
                    if(data.httpCode == 600)
                    {
                      toastr.warning(data.Message);
                      //    toastr.error('AK');
                      //    $this.html('Confirm');

                      redirect = '{{ url('view-hotel/') }}'+'/'+data.outlet_url_index;
                      window.location.replace(redirect);                      
                      
                    }
                    else
                    {
                      toastr.warning(data.Message);
                     // $this.html('Confirm');
                      $("#confirm_otp").html('Confirm');
                      //    $('#booking_verify_user').trigger('reset');                     
                    }
                  }
              },
              error:function(resp)
              {
                  $this.button('reset');
                  console.log('out--'+data); 
                  return false;
              }
          });
          return false;
      } 

    function resendotp(){
      var data = $("#booking_verify_user").serializeArray();
      var c_url = '/resend_otp';
         $.ajax({
            url: c_url,
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            type: 'POST',
            datatype: 'JSON',
            success: function (resp)
            {
                data = resp;
                if(data.httpCode == 200)
                {  
                    toastr.success(data.Message);
                }
                else
                {
                    toastr.warning(data.Message);
                }
            },
            error:function(resp)
            {
                $this.button('reset');
                //$("#ajaxloading").hide();
                console.log('out--'+data); 
                return false;
            }
        });
    }          

    </script>     
    <script src="{{ URL::asset('assets/front/otel2go/js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('assets/front/otel2go/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/front/otel2go/js/intlTelInput.min.js') }}"></script>
    <script>
$(document).ready(function(){


          var countryData = $("#phone").intlTelInput("getSelectedCountryData");
          $("#phone_hidden").val('+'+countryData.dialCode);

          $("#phone").intlTelInput({
              setCountry: country_short,
              initialCountry: "in",
              nationalMode: false,
              separateDialCode: true,
              allowDropdown: true,            
              utilsScript: "{{ URL::asset('assets/front/otel2go/js/utils.js') }}"
          });          
});


    </script>
@endsection