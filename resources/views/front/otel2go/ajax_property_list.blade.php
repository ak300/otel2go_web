<script src="{{url('assets/front/'.Session::get('general')->theme.'/plugins/rateit/src/jquery.rateit.js')}}"></script>
<link href="{{url('assets/front/'.Session::get('general')->theme.'/plugins/rateit/src/rateit.css')}}" rel="stylesheet">

@if(count($properties) > 0)
  @foreach($properties as $pr_key => $pr_val)
    @if(!empty($pr_val->outlet_name) || !empty($pr_val->outlet_image) || !empty($pr_val->contact_address))
      <div class="col-12 col-sm-6 col-md-6 col-lg-6">
        <div class="all_items">
          <a href="{{ URL::to('view-hotel/'.$pr_val->url_index) }}">  
          <div class="hotel">
              <div class="proparty_img">
                  <?php $image = $pr_val->outlet_image;?>
                  <?php $file_path = url('/assets/admin/base/images/vendors/property_image/list/'.$image); ?>
                  
                  @if (!empty($image) && File::exists(public_path('/assets/admin/base/images/vendors/property_image/list/'.$image)))
                      <img src="<?php echo $file_path;  ?>" alt="">
                  @else 
                      <img src="<?php echo url('assets/front/otel2go/images/no_hotel.jpg');  ?>">
                  @endif  
              </div>            

            <div class="price_list">

                <?php 

                    $outlet_id = $pr_val->id;

                    $sd = date('Y-m-d');

                    $ed = date('Y-m-d', strtotime(' +1 day'));

                   ?>

                <?php $disc = getDiscountPrice($outlet_id,$sd,$ed);  /* dd($disc); */?>

                @if(getCurrencyPosition()->currency_side == 1)        
                    <label>{{ getCurrency()." ".$disc[0]->discount_price  }}</label>
                @else
                    <label>{{ $disc[0]->discount_price." ".getCurrency() }}</label>
                @endif
                    
                @if(getCurrencyPosition()->currency_side == 1)        
                    <del>{{ getCurrency()." ".$disc[0]->normal_price  }}</del>
                @else
                    <del>{{ $disc[0]->normal_price." ".getCurrency() }}</del>
                @endif      

                @if($disc[0]->discount_price != "")
                    <b>{{ round(100 - ($disc[0]->discount_price / $disc[0]->normal_price) * 100) }}% OFF</b>
                @else
                    <b>0% OFF</b>
                @endif         
            </div>
          </div>
        </a>
          <div class="product_info">
            <a href="">
              <a href="{{ URL::to('view-hotel/'.$pr_val->url_index) }}">  
              <h5>{!! ucfirst($pr_val->outlet_name) !!}
              </h5>
              <p>{{ $pr_val->zone_name.", ".$pr_val->city_name }}
              </p>
            </a>
            <div class="rating_icons">
              <label>

                <?php $outlet_id = $pr_val->id; ?>

                <?php $rate = getRatingCount($outlet_id);
                 $rate_count = isset($rate[0]->avg_count)?$rate[0]->avg_count:"0"; ?>
                    <div class="rateit" data-rateit-mode="font" data-rateit-value="{{  $rate[0]->avg_rate }}" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                    {{ number_format((round($rate_count))) }} reviews
              </label>
            </div>
          </div>
        </div>
      </div>
    @endif
  @endforeach
  <div class="pagination">
    {{ $properties->links() }} 
    </div>
  @else
  <div class="no_data_found">
  <div class="no_data_found_inner">
    <img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/no_hotels_found.png'); ?>" alt="playstore">
<p>No hotels available.</p>
</div>
</div>
@endif