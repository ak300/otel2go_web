  <!-- signup verfication -->
  <div class="modal fade verifycode" id="verifycode" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="singup_info">
        <div class="sign_up verifycode">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
          <div class="signup_inner_containt">
            <div class="signup_image">
              <img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/otel2go1.png'); ?>"  alt="signup_verification">
              <h4>SIGNUP VERIFICATION CODE</h4>
              <i class="signup_line"></i>
            </div>
            {!!Form::open(array('method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'verify_user' ,'onsubmit'=>'return otpverify()'));!!}
              <p>We send verification code to your email & mobile number.
                Please enter the code.
              </p>
              <div class="input-group verify_input">
                <input type="text" name="otp_number" class="form-control" required placeholder="OTP" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <button class="btn btn-outline-secondary confirm" type="submit">Confirm</button>
                </div>
                <input type="hidden" name="verify_otp_id" id="verify_otp_id" value="1">
                <input type="hidden" name="verify_user_id" id="verify_user_id" value="1">
              </div>
              <a class="resend" onclick ="resendotp();" href="#" >Resend OTP</a>
            {!!Form::close();!!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- signup verfication -->


<script type="text/javascript">
function resendotp(){
  var data = $("#verify_user").serializeArray();
  var c_url = '/resend_otp';
     $.ajax({
        url: c_url,
        headers: {'X-CSRF-TOKEN': token},
        data: data,
        type: 'POST',
        datatype: 'JSON',
        success: function (resp)
        {
            data = resp;
            if(data.httpCode == 200)
            {  
                toastr.success(data.Message);
            }
            else
            {
                toastr.warning(data.Message);
            }
        },
        error:function(resp)
        {
            //  $this.button('reset');
            //$("#ajaxloading").hide();
            console.log('out--'+data); 
            return false;
        }
    });
}


function otpverify(){

    $this = $(".btn-outline-secondary");
    $this.html('Processing....');

    var c_url = '/verify_user';
    data = $("#verify_user").serializeArray();
    token = $('input[name=_token]').val();
    $.ajax({
        url: c_url,
        headers: {'X-CSRF-TOKEN': token},
        data: data,
        type: 'POST',
        datatype: 'JSON',
        success: function (resp)
        {
            data = resp;
            if(data.httpCode == 200)
            {  
                $this.html('Confirm');
                toastr.success(data.Message);
                $('#verifycode').modal('hide');
                $('#verify_user').trigger('reset');

                $('#user_input_id').val(data.last_id);
                $('.userdetails').modal('show');     
            }
            else
            {
                toastr.warning(data.Message);
                $this.html('Confirm');
            }
        },
        error:function(resp)
        {
            $this.button('reset');
            console.log('out--'+data); 
            return false;
        }
    });
    return false;
}

</script>
