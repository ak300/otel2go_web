@extends('layouts.front')
@section('content')
   <!-- <section class="banner_sections_inner blog">
<div class="container">
<div class="inner_banner_info">
<h2>@lang('messages.Blog')</h2>
<p>@lang('messages.Where everything on demand is discussed')</p>
</div>
</div>
    </section>-->
    <section class="error_sections">
<div class="container">
<div class="bolg_listing">
<div class="blog_title"><h1>Blog</h1></div>
<div class="row">
	@if (count($blog) > 0 )
		@foreach($blog as $key => $value)
				<div class="col-md-4 col-sm-4 col-xs-6">
				
				<div class="blog_list_img">
				<a title="{{ ucfirst($value->title) }}" href="{{ URL::to('/blog/info/' . $value->url_index . '') }}">
                        <?php  if(file_exists(base_path().'/public/assets/admin/base/images/blog/list/'.$value->image)) { ?>
								<img   alt="{{ ucfirst($value->title) }}"  src="<?php echo url('/assets/admin/base/images/blog/list/'.$value->image.''); ?>" >
							<?php } else{  ?>
									<img src="{{ URL::asset('assets/admin/base/images/blog/blog.png') }}" alt="{{ ucfirst($value->title) }}">
							<?php } ?>
                        </a>
				</div>
				<div class="blog_list_in">
				<h2><a title="{{ ucfirst($value->title) }}" href="{{ URL::to('/blog/info/' . $value->url_index . '') }}">{{ str_limit($value->title.',', 50) }}</a></h2>
				<p>{{ str_limit($value->short_notes , 250) }}</p>
				<a href="{{ URL::to('/blog/info/' . $value->url_index . '') }}" title="@lang('messages.Continue Reading')" class="continue_butt"> <span>→</span> @lang('messages.Continue Reading')</a>
				</div>
				</div>
				
				
		@endforeach
	@else
	<div class="blog_no_img">
	<img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/blog.png');?>" alt="">
	@lang('<p>No data found.</p>')

	</div>
	@endif
</div>
<div class="space"></div>
</div>
</div>
<script>
      $('.listing_header').show();
</script>
@endsection
