@extends('layouts.front')
@section('content')
  <meta property="og:image" content="https://static.wixstatic.com/media/56a444_9273e80a60684dc8b38e56025059f356%7Emv2_d_3200_1800_s_2.png" />
	<section class="ref_bg_sections">
	 <img src="{{ URL::asset('assets/front/otel2go/images/ref_bg.jpg') }}" alt="">
	</section>
	
	
	<div class="container_outer">
	<div class="container">
	<div class="inner_referral"> 
	<div class="inner_referral_midd">
		<h1 style="display:none;">Referral Code : {{ $data->customer_unique_id }}</h1>
		<h2>Hey {{ Session::get('user_name') }}!</h2>

		<p>We'd love it if you tod your friends about us. Whilie you're at it, earn {{ getAppConfig()->referral_commision }}% of <br/>your friend's purchase for each refrral.</p>
		<p>Your friens also get <span>{{ getAppConfig()->referral_commision }}%</span> off their 1st purchase </p>
			
			<div class="forward_link">
			<h3>Forward this link to your friends!</h3>
			<button type="button" class="btn btn-warning"> <?php echo env("APP_URL")."/"."referral_signup/".$data->customer_unique_id; ?></button>
			</div>
			<label>Or share it online</label>
				<div class="fb-share-button" data-href="<?php echo env("APP_URL")."/"."referral_signup/".$data->customer_unique_id; ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.otel2go.com/book-now?referral_id=rt4567" class="fb-xfbml-parse-ignore">Share on Facebook</a></div>
				<a href="http://twitter.com/share?url=https://192.168.1.19:1006/referral_signup/{{ $data->customer_unique_id }};size=l&amp;count=none" target="_blank">Tweet</a>
				<div class="fb-share-button" data-href="https://www.otel2go.com/book-now?referral_id={{ $data->customer_unique_id }}" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.otel2go.com/book-now?referral_id=rt4567" class="fb-xfbml-parse-ignore">Share on Facebook</a></div>
		
			<?php /* 
			<a target="_blank" title="Share on LinkedIn"
			     href="http://www.linkedin.com/shareArticle?mini=true&url=otel2go.com">Click LinkedIn
			</a>
			<button class="fb_click btn">FB</ > */ ?>
		
	</div>
	</div>
	</div>
	</div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0';
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
</script> 
<script>
        $('.listing_header').show();
        $(document).ready(function(){
			var descript = 'My Description';
			var mainUrl = 'https://192.168.1.19:1006';
			var imgUrl = "<?php 
							echo '192.168.1.19:1006/assets/front/otel2go/images/logo/159_81/logo.png'; 
						  ?>";
			var referral_id = "<?php echo $data->customer_unique_id;?>";
			var shareLink = mainUrl + encodeURIComponent('/book-now?referral_id='+referral_id);
			var fbShareLink = shareLink + '&picture=' + imgUrl + '&description=' + descript;
			var twShareLink = 'text=' + descript + '&url=' + shareLink;
		
			$(".my-btn .facebook").off("tap click").on("tap click",function(){
			  var fbpopup = window.open("https://www.facebook.com/sharer/sharer.php?u=" + fbShareLink, "pop", "width=600, height=400, scrollbars=no");
			  return false;
			});
			/*$('.fb_click').on('click',function(){
				 var fbpopup = window.open("https://www.facebook.com/sharer/sharer.php?u=" + fbShareLink, "pop", "width=600, height=400, scrollbars=no");
			  	return false;
			}); */
			$(".my-btn .twitter").off("tap click").on("tap click",function(){
			  var twpopup = window.open("http://twitter.com/intent/tweet?" + twShareLink , "pop", "width=600, height=400, scrollbars=no");
			  return false;
			});  
		});
</script>	
<?php /*
<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
<script type="IN/Share" data-url="www.otel2go.com/referral_signup/<?php echo $data->customer_unique_id;?>"></script> */ ?>
@endsection
