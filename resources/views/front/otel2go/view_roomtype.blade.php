      <div class="listing_of_rommon" id="roomt">
      @if(count($room_type) > 0)    
        <?php $total_room_type = count($room_type);

        //    dd($room_type);
          $k = 1;
        ?>
          @foreach($room_type as $rt_val)  
          <div class="mainsection <?php echo ($total_room_type == $k)?'border_btm':''?>" id="retrive_rooms">
            <div class="row">
              <div class="col-sm col-md col-lg-7 col-xs">
                <div class="row">
                  <div class="col-sm-4 col-md-4 col-lg-4 col-xs-4">
                      <?php $image = $rt_val->room_image;?>
                      <?php $file_path = url('/assets/admin/base/images/room_type/'.$image); ?>
                      
                      @if (!empty($image) && File::exists(public_path('assets/admin/base/images/room_type/'.$image)))
                          <img src="<?php echo $file_path;  ?>" alt="">
                      @else 
                          <img src="<?php echo url('assets/admin/base/images/room_type/default.jpeg');  ?>">
                      @endif                      
                  </div>
                  <div class="col-sm col-md col-lg col-xs padding_left0">
                    <h3 id="roomtyep">{{ ucfirst($rt_val->room_type) }}</h3>
                    <small>Max. guests {{ $rt_val->adult_count }} Adult(s) {{ $rt_val->child_count }} child(children)</small>
                    <h6>Cancellation Policy Applies</h6>

                    <?php $amty = getroomtypeAmenities($rt_val->id,$vendor_id); ?>
                      @if(count($amty) > 0)
                        @foreach($amty as $value)
                            <span><i class="{{ $value->amenity_icon }}"></i>&nbsp;{{ $value->amenity_name }}</span>
                        @endforeach
                      @endif
                  </div>
                </div>
              </div>
              <div class="col-sm col-md col-lg-5 col-xs ">
                <div class="book_now">
                  <div class="row">
                    <div class="col-sm col-md col-lg col-xs">
                      <div class="book_now_inner">
                        <p> {{ $rt_val->normal_price }} MYR / room / night(s)</p>
                        <?php $actual_price = $rt_val->discount_price;?>
                        <h5>{{ $actual_price }} MYR</h5>
                        <p>Price inclusive of tax</p>
                      </div>
                    </div>
                    <div class="col-sm col-md col-lg col-xs-4">
                      <input type="hidden" value="{{  $rt_val->id  }}" name="room_type_id">
                      <a href="{{  url('checkout/'.$outlets[0]->url_index.'/'.$rt_val->url_index) }}" class="btn btn-primary btn-lg book">Book Now</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php $k++; ?>
          @endforeach
        @else
        <div class="no_data_found">
          <div class="no_data_found_inner">
            <img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/no_hotels_found.png'); ?>"  alt="playstore">
            <p>No Rooms available.</p>
        </div>
        </div>
        @endif
      </div>

