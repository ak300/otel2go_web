@extends('layouts.front')
@section('content')

<section class="business_banner">
	<div class="container">
	<div class="row">
	<div class="aligin_center">
	<h1>Best hotel management system - Launching soon!</h1>
	<p>The best online platform to showcase your hotels and facilities that you offer to your potential travelers.
Give your hotel a brand recognition with our digital platform.</p>

<img src="{{ URL::asset('assets/front/otel2go/images/dasbord_bg.png') }}"/>
	</div>
	</div>
	</div>
	</section>
	
	
  <section class="home_infomations_business">
    <div class="container">
      <div class="Intrro_section">
        <h2> 
        Everything you need for your hotel - Simple and Flexible.
        </h2>
        <p>Get all information that you need to know about the hotel, before you book your stay. We offer the best
and assured hotels in our listing. Book your hotel rooms at the best price from us.
        </p>
      </div>
      <div class="row">
        <div class="col-sm col-lg col-xs">
          <div class="list_works">
         <span> <img src="{{ URL::asset('assets/front/otel2go/images/how_it1.png') }}" alt="Mate Rewards "/></span>
            <h3>Guest reservation<br/>
mangement</h3>
			<p>We never miss your bookings or duplicate it. Pre-cleaned and ready to check-in rooms are offered, matching exactly with the ones showcased. We offer efficient guest reservation management system, with customizations.</p>
          </div>
        </div>
        <div class="col-sm col-lg col-xs">
          <div class="list_works">
         <span> <img src="{{ URL::asset('assets/front/otel2go/images/how_it2.png') }}" alt="Mate Rewards "/></span>
            <h3>Channel manager<br/>
integration</h3>
			<p>The channel manager integration helps the hotels to sychronize and update their vacancies in the rooms and in return we help them get bookings for the vacant rooms, thus enhancing the revenue generation.</p>
          </div>
        </div>
        <div class="col-sm col-lg col-xs">
          <div class="list_works">
         <span> <img src="{{ URL::asset('assets/front/otel2go/images/how_it3.png') }}" alt="Mate Rewards "/></span>
            <h3>Payment processing
(PCI-DSS compliant)</h3>
			<p>We help in secure completion of the payments on hotel bookings, protecting the privacy of information of
the cardholders. This allows the cardholders to complete their transactions safely and successfully.</p>
          </div>
        </div>
      </div>
   
    </div>
  </section>
  
    <section class="home_infomations_hellp">
    <div class="container">
    <div class="Intrro_section">
        <h2> 
        Everything you need for your hotel - Simple and Flexible.
        </h2>
        <p>Get all information that you need to know about the hotel, before you book your stay. We offer the best
and assured hotels in our listing. Book your hotel rooms at the best price from us.
        </p>
      </div>
	  <div class="check_marks_sections">
	  <div class="row">
	   <div class="col-sm-8 col-lg-8 col-xs-12">

	   <img src="{{ URL::asset('assets/front/otel2go/images/how_hotel_hellp.jpg') }}"/>

	   </div>
	   <div class="col-sm-4 col-lg-4 col-xs-12">
	   <div class="list_of_check">
	   <ul>
	   <li>Manage reservations</li>
	   <li>Invoicing</li>
	   <li>Online book keeping and reporting</li>
	   <li>In house report</li>
	   <li>Activate staffs</li>
	   <li>Track housekeeping</li>
	   <li>24/7 Support</li>
	   </ul>
	   </div>
	   </div>
	  </div>
	  </div>
  
    </div>
  </section>
    <section class="home_infomations_engine">
    <div class="container">
	<div class="booking_engine">
	   <div class="Intrro_section">
        <h2> 
       Online booking engine
        </h2>
        <p>Our online hotel booking system displays the best and authentic hotels at the best price and encourages
customers with attractive offers and discounts.
        </p>
      </div>
	    
	    <img src="{{ URL::asset('assets/front/otel2go/images/room_booking.jpg') }}"/>
	</div>
	
    </div>
  </section>
  
      <section class="payment_sucess">
    <div class="container">
    <div class="Intrro_section">
        <h2> 
       View our attractive plans
        </h2>
        <p>Subscribe for the plan that suits your hotel and business best and make use of it to boost your revenue.
        </p>
      </div>
	  <div class="price_sections">
	  <div class="row">
	  <div class="col-md-4 col-xs-4 col-sm-12">
	  <div class="price_list_busness">
  <div class="prgrass_bar">	
	<h3>Essential plan</h3>
	  <b><span class="myr">MYR</span>499<span class="mon_siz">/ mo</span></b>
	  </div>
	  <div class="prgrass_bar">
	  <p>50 rooms</p>
	  
	  <img src="{{ URL::asset('assets/front/otel2go/images/pograss.png') }}"/>
	  </div>
	   <div class="prgrass_bar">
	   <ul>
	   <li>Room Report</li>
	   <li>Sales Report</li>
	   <li>Secure Security</li>
	   <li>Free Listing Low Commission Fees</li>
	   </ul>
	    <div class="sigun_up_butt">
	   <a href="{{ URL('/vendor-signup')}}" class="btn btn-default">Sign up</a>
	   </div>
	   </div>
	  </div>
	  </div>
	    <div class="col-md-4 col-xs-4 col-sm-12">
	  <div class="price_list_busness">
  <div class="prgrass_bar">	
	<h3>Premium plan</h3>
	  <b><span class="myr">MYR</span>999<span class="mon_siz">/ mo</span></b>
	  </div>
	  <div class="prgrass_bar">
	  <p>50 - 100 rooms</p>
	  <img src="{{ URL::asset('assets/front/otel2go/images/pograss.png') }}"/>
	  </div>
	   <div class="prgrass_bar">
	   <ul>
	   <li>Room Report</li>
	   <li>Sales Report</li>
	   <li>Analytical Report</li>
	   <li>Secure Security</li>
	   <li>Free Listing Low Commission Fees</li>
	   </ul>
	    <div class="sigun_up_butt">
	   <a href="{{ URL('/vendor-signup')}}" class="btn btn-default">Sign up</a>
	   </div>
	   </div>
	  </div>
	  </div>
	     <div class="col-md-4 col-xs-4 col-sm-12">
	  <div class="price_list_busness">
  <div class="prgrass_bar">	
	<h3>Affiliate plan</h3>
		<b><span class="myr">MYR</span>1499<span class="mon_siz">/ mo</span></b>
	  </div>
	  <div class="prgrass_bar">
		  <p>200 rooms</p>
		  <img src="{{ URL::asset('assets/front/otel2go/images/pograss.png') }}"/>
	  </div>
	   <div class="prgrass_bar">
	   <ul>
	   <li>Room Report</li>
	   <li>Sales Report</li>
	   <li>Analytical Report</li>
	   <li>Secure Security</li>
	   <li>Free Listing Low Commission Fees</li>
	   </ul>
	   </div>
	   <div class="sigun_up_butt">
	    <a style="display: none;" href="{{ URL('/contact-us')}}" class="btn btn-default">Contact us</a>
	    <a href="{{ URL('/vendor-signup')}}" class="btn btn-default">Sign up</a>
	   </div>
	  </div>
	  </div>
	  </div>
   </div>
  </section>
   <section class="home_infomations_signup_free">
    <div class="container">
	<h2>Try us. You will love the Otel2GO experience.</h2>
	<h3>Ready to give your hotel the much needed boost and business? Join hands with us an experience the
change</h3>

<a href="{{ URL('/vendor-signup')}}" class="btn btn-default">Sign up for free</a>
     </div>
  </section>
  <div class="space">
      </div>
    <script>

    $('.listing_header').show();
    
</script>
@endsection
