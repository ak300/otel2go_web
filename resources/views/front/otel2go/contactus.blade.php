@extends('layouts.front')
@section('content')
<link href="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/css/intlTelInput.css');?>" media="all" rel="stylesheet" type="text/css" />
<!-- container start -->
<section class="inner_page_common">
    <div class="container">
        <div class="sucess_messgaes">
            <div class="sucess_messgaes_absolut animatedParent"  data-sequence="500">
                <div class="sucess_messgaes_inner animated growIn slower go" data-id="1"></div>
            </div>
        </div>
        <div class="contact_us_inner row">
            <div class="col-md-12 col-xs-12">
            <div class="location">
            <h1 style="display: none;">Contact us</h1>
            <?php /* dd($settings->contact_address); */ ?>
			<div class="map_section">
                <div class="map-responsive col-lg">


                <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&amp;q=<?php echo ($settings->contact_address); ?>"></iframe>
                </div>
                </div>
            </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="contact_det">
                    <br/>
                    <h3>@lang('messages.Contact details')</h3>
                    <address>
                        <h4><i class="glyph-icon flaticon-facebook-placeholder-for-locate-places-on-maps"></i><?php echo $settings_infos->site_name; ?><br><p><?php echo ($settings->contact_address); ?></p></h4>
                        <?php if(isset($settings->email) && ($settings->email !="")) { ?>
                        <a href="mailto:<?php echo $settings->email; ?>" title="<?php echo $settings->email; ?>"><i class="glyph-icon flaticon-close-envelope"></i><?php echo $settings->email; ?></a>
                        <?php } 
                         if(isset($settings->mobile_number) && ($settings->mobile_number !="")) { ?>
                        
                        ?>
                        <label><i class="glyph-icon flaticon-phone-receiver"></i><?php echo $settings->mobile_number; ?></label>
                        <?php } ?>
                    </address>
                    <h3>@lang('messages.Be social')</h3>
                    <div class="social_share contact_us_social">

                        <ul>
                            <a class="facebook" href="<?php echo Session::get("social")->facebook_page; ?>" target="_blank"><i class="glyph-icon flaticon-facebook-logo-button"></i></a>
                            <a class="twitter" href="<?php echo Session::get("social")->twitter_page; ?>" target="_blank"><i class="glyph-icon flaticon-twitter-logo-button"></i> </a>
                            <a class="insta" href="<?php echo Session::get("social")->instagram_page; ?>" target="_blank"><i class="glyph-icon flaticon-instagram-logo"></i> </a>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="right_form">
                     {!!Form::open(array('url' => 'postcontactus', 'method' => 'post','class'=>'col s12','id'=>'contactus_form','files' => true));!!}
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input maxlength="20" type="text" class="form-control" value="{!! old('name') !!}" name="name" id="name" placeholder="@lang('messages.Name')" required>
                                <span class="error">@if ($errors->has('name')){{ $errors->first('name', ':message') }}@endif</span>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                    <input type="mobile number" name="mobile" class="form-control" id="mobile" placeholder="" value="{!! old('mobile') !!}" required>
                                    <input id="phone_hidden" type="hidden" name="country_code" value="0">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input id="email" maxlength="100" type="text" value="{!! old('email') !!}" name="email" class="form-control" placeholder="@lang('messages.Email')" required>
                                <span class="error"> @if ($errors->has('email')){{ $errors->first('email', ':message') }}@endif</span>
                            </div>
                        </div>
                    <?php /*   
                     <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-control" style=" padding:0px;">
                                    <select id = "city" class="" required style="width:100%;" name="city" >
                                        <option value="">@lang('messages.Select City')</option>
                                            @foreach(getCityList(getAppConfig()->default_country) as $city)
                                                <option value="{{ $city->id }}" <?php if($city->id==Input::get('city')){  echo "selected";   } */ ?> <?php /* > {{ $city->city_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('city')){{ $errors->first('city', ':message') }}@endif
                                    </div>
                                </div>
                            </div>
                         
                            <div class="col-md-6 col-sm-12 col-xs-12 border_none_new">
                                <div class="form-group">
                                    <div class="form-control" style=" padding:0px;">
                                        <select id="enquiry_type" class="" required="true" style="width:100%;" name="enquery_type">
                                            <option value="">@lang('messages.Enquiry type')</option>
                                            <option value="1" @if(old('enquery_type')==1) {{ "selected" }} @endif >General</option>
                                            <option value="2" @if(old('enquery_type')==2) {{ "selected" }} @endif >Salon </option>
                                            <option value="3" @if(old('enquery_type')==3) {{ "selected" }} @endif>Service</option>
                                            <option value="4" @if(old('enquery_type')==4) {{ "selected" }} @endif>Payment</option>
                                        </select>
                                        @if ($errors->has('enquery_type')){{ $errors->first('enquery_type', ':message') }}@endif
                                    </div>
                                </div>
                            </div>
                        */ ?>      
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" id="comment" name="message"  placeholder="@lang('messages.Description')" required>{!! old('message') !!}</textarea>
                                   <?php /* @if ($errors->has('enquery_type')){{ $errors->first('enquery_type', ':message') }}@endif */ ?>
                                </div>
                            </div>
                            <?php /* echo $settings->contact_address; */ ?>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="sign_bot_sub">
                                        <?php if(!Session::has('user_id')) { ?>
                                        <button type="button" class="btn btn-primary" onclick="window.location='{{ url('/') }}'" title="@lang('messages.Cancel')">@lang('messages.Cancel')</button>
                                        <?php } else { ?>
                                        <button type="button" class="btn btn-primary" onclick="window.location='{{ url('/book-now') }}'" title="@lang('messages.Cancel')">@lang('messages.Cancel')</button>
                                        <?php } ?>                                        
                                        <button type="submit" id="contactsubmit" class="btn btn-default" title="@lang('messages.Send')">@lang('messages.Send')</button>
                                        <div class="col-sm-4 loader-coms" id="payajaxloading" style="display:none;">
                                            <i class="fa fa-spinner fa-spin fa-3x "></i>
                                            <strong style="margin-left: 3px;">@lang('messages.Processing...')</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {!!Form::close();!!}
                </div>
            </div>
        </div>
    </div>
</section>
<script>
  $('.listing_header').show();
</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo env('GOOGLE_MAP_API_KEY'); ?>"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-gmaps-latlon-picker.js') }}"></script>
<script type="text/javascript">


    $('#contactus_form').submit(function(evt) {
        evt.preventDefault();
        var c_url    = '/postcontactus';
        var token    = $('input[name=_token]').val();
        var countryData = $("#mobile").intlTelInput("getSelectedCountryData"); // get country data as obj 
        var countryCode = countryData.dialCode; // using updated doc, code has been replaced with dialCode
        countryCode = "+" + countryCode;
        $('#phone_hidden').val(countryCode);
        var formData = new FormData(this)
        $.ajax({
            url: c_url,
            headers: {'X-CSRF-TOKEN': token},
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (resp) {
                // alert('resp');
                // console.log(resp);return false;

                //  data = JSON.parse(resp);
                if(resp.httpCode == 200) {
                   // $('#contactus_form').reset();
                     toastr.success(resp.Message);

                   // alert(resp.Message);
                   $('#contactus_form').trigger("reset");

                } 
                else
                {
                    if(resp.httpCode == 400)
                    {
                        $.each(resp.Message,function(key,val){
                            toastr.warning(val);
                        });
                    } else {
                        toastr.warning(resp.Message);
                    }
                    //  $this.html('Sign Up');
                }

            }, error:function(data) {
                console.log('out--'+data); 
                return false;
            }
        });
        return false;
    });
    
    var address= "<?php  echo ($settings->contact_address);  ?>";
    var latlng = new google.maps.LatLng(<?php echo Session::get("general")->geocode; ?>);
    var myOptions = {
        zoom: 8,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        navigationControl: true,
        mapTypeControl: true,
        scaleControl: true
    };
    var map = new google.maps.Map(document.getElementById("googleMap"), myOptions);
    var marker = new google.maps.Marker({
        position: latlng,
        animation: google.maps.Animation.BOUNCE
    });
    marker.setMap(map);
    var contentString = '<div id="content">'+'<div id="siteNotice">'+'</div>'+'<h3 id="firstHeading" class="firstHeading">'+address+'</h3>'+'</div>';
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });
    infowindow.open(map,marker);
    function ContactSubmit()
    {
        $("#contactajaxloading").show();
        $("#contactsubmit").hide();
    }    
    $( document ).ready(function() {
        // alert('vcxvvxc');
        $('.BDC_CaptchaImageDiv a').addClass('test');
        $('.test').css('visibility', 'hidden');
    });
    
</script>
@endsection
