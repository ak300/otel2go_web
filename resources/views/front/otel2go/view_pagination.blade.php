      
        <div class="pag_section_comon">
          @if(count($reviews) > 0)
            @foreach($reviews as $rev)
              <ul>
                <li>
                  <div class="row" id="paginate1">
                    <div class="col-3 col-sm-3 col-md-1 col-lg-1" id="paginate1">
                      <div class="user_no">{{ number_format(round($rev->ratings)),1 }} </div>
                    </div>
                    <div class="col-9 col-sm-9 col-md-11 col-lg-11" id="paginate1">
                      <h4> {{ ucfirst($rev->firstname) }} </h4>
                      <small> {{ date("M d,Y", strtotime($rev->created_date)) }} <?php /* - Family vacation */ ?></small>
                      <p>{{ ucfirst($rev->title) }}</p>
                    </div>
                  </div>
                </li>
              </ul>
            @endforeach
          @endif
        </div>
    