<link href="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/css/intlTelInput.css');?>" media="all" rel="stylesheet" type="text/css" />

<!-- Modal for signup -->
    <div class="modal fade signup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="singup_info">
                    <div class="sign_up">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('#sign_up').trigger('reset');">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="signup_inner_containt ">
                            <div class="signup_image">
                                <img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/otel2go1.png'); ?>">
                                <h4>SIGN UP</h4>
                                <i class="signup_line"></i>
                            </div>
                            {!!Form::open(array('method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'sign_up' ,'onsubmit'=>'return signup()'));!!}
                                <div class="form-group">
                                    <label>Email</label>
                                    <!-- <input type="email" name="email" required class="form-control" placeholder=""> -->
                                     <input type="email" name="email" required maxlength="250" class="form-control"  id="exampleInputEmail" placeholder="@lang('messages.Email') (@lang('messages.Required'))">
                                </div>
                                <div class="form-group">
                                    <label>Mobile number</label>
                                    <input type="mobile number" required name="mobile" class="form-control" id="mobile" placeholder="">
                                    <input id="phone_hidden" type="hidden" name="country_code" value="0">
                                </div>
                                <div class="form-group">
                                    <label >Password</label>
                                    <input type="password" required name="password" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label class="checkmark_button">
                                        <input type="checkbox" required name="terms_condition" value="1">I agree the<span> <a href="{{ 'cms/terms-and-conditions' }}" onclick="$('#sign_up').trigger('reset');">Terms &amp; Conditions</a> </span> of Otel2go
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="button_signup">
                                    <div class="row">
                                        <div class="col-sm-6 col padding_right">
                                            <button type="button" class="btn sign-in-modal signup_btn cancel_btn" onclick="$('#sign_up').trigger('reset');">Cancel</button>
                                        </div>
                                        <div class="col-sm-6 col">
                                            <button type="submit" id="signup_btn_new" class="btn signup_btn">Sign Up</button>
                                        </div>
                                    </div>
                                </div>
                                <p class="account">Already have Account?   <a href="javascript:;" 
                                    class="sign-in-modal" title="@lang('messages.Login')" data-toggle="modal" data-target=".login" onclick="$('#sign_up').trigger('reset');">@lang('messages.Login')</a> </p>
                                <div class="connect_divider">
                                    <div class="connect_with">
                                        <h6>Or Connect with</h6>
                                    </div>
                                </div>
                              <div class="connect_btn">
                <div class="row">
                  <div class="col-sm-6 col padding_right">
                    <div class="facebook_sign">
                      <a href="{{ URL::to('auth/facebook') }}" title="@lang('messages.Signup  with  facebook?')"> <i class="glyph-icon flaticon-facebook-letter-logo"></i></a>
                      <!-- <i class="glyph-icon flaticon-facebook-letter-logo"></i> -->
                    </div>
                  </div>
                  <div class="col-sm-6 col">
                    <div class="google_plus_sign">
                            <a href="{{ URL::to('auth/googleplus') }}" class="google_plus_but" title="@lang('messages.Signup  with  facebook?')"><i class="glyph-icon flaticon-google-plus-logo"></i></a>
                    </div>
                  </div>
                </div>
              </div>
                             {!!Form::close();!!} 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Modal for signup -->

<!-- country code js-->
<script type="text/javascript" src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/js/intlTelInput.min.js'); ?>"></script>
<!-- Modal for signup end -->
<script type="text/javascript">
 $('.sign-in-modal').click(function(){
      $('.signup').modal('hide');
    });
function signup(){   
    //To set country code in hidden input 
    var countryData = $("#mobile").intlTelInput("getSelectedCountryData");
    $("#phone_hidden").val('+'+countryData.dialCode);

    $this = $("#signup_btn");
    $this.html('Loading....');
    data = $("#sign_up").serializeArray();
    data.push({ name: "login_type", value: "1" });
    var c_url = '/signup_user';
    token = $('input[name=_token]').val();
    $.ajax({
        url: c_url,
        headers: {'X-CSRF-TOKEN': token},
        data: data,
        type: 'POST',
        datatype: 'JSON',
        success: function (resp)
        {
            // toastr.success(resp.Message);
            //  $this.button('reset');
            if(resp.httpCode == 200)
            {  
                // alert(resp.Message);
                $('.signup').modal('hide');
                toastr.success(resp.Message);
                //location.reload(true);
                //toastr.options.onShown = function() { location.reload(true); }
                $('#verify_otp_id').val(resp.last_id);
                $('#verifycode').modal('show');
                $('#sign_up').trigger('reset');
                $this.html('Sign Up');
            }
            else
            {
                if(resp.httpCode == 400)
                {
                    $.each(resp.Message,function(key,val){
                        toastr.warning(val);
                    });
                }
                else if(resp.httpCode == 700)
                {
                    //  toastr.warning('Ak');
                    toastr.success(resp.Message);
                    $('.signup').modal('hide');
                    $('#verifycode').modal('show');
                    $('#sign_up').trigger('reset');
                    $('#verify_otp_id').val(resp.user_id);
                    $this.html('Sign Up');                  
                }
                else {
                    toastr.warning(resp.Message);
                }
                $this.html('Sign Up');
            }
        },
        error:function(resp)
        {
            $this.button('reset');
            //$("#ajaxloading").hide();
            console.log('out--'+resp); 
            return false;
        }
    });
    return false;
}

$( document ).ready(function() {
    $("#mobile").intlTelInput("setCountry",'my');
    setTimeout(function() {
            $('.alert').fadeOut('fast');
    }, 7500);
    
    $('.BDC_CaptchaImageDiv a').addClass('test');
    $('.test').css('visibility', 'hidden');
    $('#ContactCaptcha_SoundIcon').css('visibility', 'hidden');
             
});

$("#mobile").intlTelInput({
    initialCountry: "auto",
    separateDialCode: true,
    allowDropdown: true,
    onlyCountries: ['in','my'],
    utilsScript: "<?php echo URL::asset('/assets/front/'.Session::get("general")->theme.'/js/utils.js'); ?>" // just for formatting/placeholders etc
});
</script>
