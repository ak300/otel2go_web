  <!-- user details -->
    <div class="modal fade userdetails" id="userdetails" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="singup_info">
            <div class="sign_up verifycode">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
                <div class="signup_inner_containt">
                  <div class="signup_image">
                    <img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/otel2go1.png'); ?>"  alt="user_details">
                      <h4>USER DETAILS</h4>
                    <i class="signup_line"></i>
                  </div>
                   {!!Form::open(array('method' => 'post','class' => 'tab-form attribute_form', 'id' => 'update_user' ,'onsubmit'=>'return update_user()'));!!}
                  <div class="form-group">
                    <label>First name</label>
                    <input type="text" name="firstname" required maxlength="250" class="form-control"  id="firstname" placeholder="@lang('messages.First name') (@lang('messages.Required'))">
                  </div>
                  <div class="form-group">
                    <label>Last name</label>
                    <input type="text" name="lastname" required maxlength="250" class="form-control"  id="lastname" placeholder="@lang('messages.Last name') (@lang('messages.Required'))">
                  </div>
                  <div class="form-group">
                    <label>Gender</label>
                        <select name="gender" id="gender" class="form-control" width="80%">
                            <option value="">Select Gender</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                  </div>
                  <input type="hidden" name="user_input_id" id="user_input_id" value="1">
                  <div class="buttons">
                  <div class="row">
                  <div class="col-sm col-md padding_right">
                  <button type="button" class="btn cancel_btn signup_btn">Cancel</button>
                  <?php /*  <a href="{{ url('/book-now') }}" class="btn cancel_btn signup_btn">Cancel</a> */ ?>
                  </div>
                  <div class="col-sm col-md">
                  <button type="submit" id="signup_btn" class="btn signup_btn">Update</button>
                  </div>
                  </div>
                  </div>
                  {!!Form::close();!!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <!-- user details -->


<!-- Modal for signup end -->
<script type="text/javascript">
function update_user(){
  var data = $("#update_user").serializeArray();
  var c_url = '/update_user';
  token = $('input[name=_token]').val();
     $.ajax({
        url: c_url,
        headers: {'X-CSRF-TOKEN': token},
        data: data,
        type: 'POST',
        datatype: 'JSON',
        success: function (resp)
        {
            data = resp;
            if(data.httpCode == 200)
            {  
                toastr.success(data.Message);
                $('#update_user').trigger('reset');
                $('.userdetails').modal('hide');
                location.reload();
<?php /*                
                url = '{{url("book-now")}}';
                $(window).attr("location",url);
*/ ?>                
            }
            else
            {
                if(data.httpCode == 400)
                {
                    $.each(data.Message,function(key,val){
                        toastr.warning(val);
                    });
                } 
            }
        },
        error:function(resp)
        {
            //  alert('here3');
            //  $this.button('reset');
            console.log('out--'+data); 
            return false;
        }
    });
    return false;
}

</script>
