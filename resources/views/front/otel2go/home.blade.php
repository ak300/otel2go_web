@extends('layouts.front')
@section('content')

<style type="text/css">
    .ui-autocomplete-category{
  
    font-size: 20px;

    }

    body .ui-autocomplete {
  /* font-family to all */
    background-color: white;
    z-index: 10000;
    position: relative;
    top: -1821.94px;
    left: 643px;
    width: 416px;
}



body .ui-menu .ui-menu-item:hover,.ui-menu .ui-menu-item-focus, .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus {
    background-color: ;
    font-weight: bold;
}

</style>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/front/otel2go/css/daterangepicker.css') }}"/>

<section>
    <div class="banner_section">
      <div id="doc" class="hidden_responsive">
        <div id="content">
          <!-- blueberry -->
          <div class="blueberry">
            <ul class="slides">

              @if(count($banners) > 0)

                  @if(count($banners) == 1)

                        <li>
                          <img src="{{ URL::asset('assets/admin/base/images/banner/'.$banners[0]->banner_image) }}"/>
                        </li>

                  @else

                    @foreach($banners as $banner)

                        <li>
                          <img src="{{ URL::asset('assets/admin/base/images/banner/'.$banner->banner_image) }}"/>
                        </li>

                    @endforeach

                  @endif

              @else

                    <li>
                      <img  src="{{ URL::asset('assets/front/otel2go/images/slide.jpg') }}"/>
                    </li>
                    <li>
                      <img src="{{ URL::asset('assets/front/otel2go/images/slide1.jpg') }}" />
                    </li>

              @endif
            </ul>
          </div>
          <!-- blueberry -->
        </div>
      </div>

      @if(Session::has('logout-message'))
          <script type="text/javascript">
              toastr.success("{{ Session::get('logout-message') }}");
          </script>
      @endif

      @if (Session::has('message-failure'))
        <script type="text/javascript">
            toastr.warning("{{ Session::get('message-failure') }}");
        </script>
      @endif

      @if(Session::has('message-success'))
          <script type="text/javascript">
            toastr.success("{{ Session::get('message-success') }}");
          </script>
      @endif 

      <div class="search_informations">
        <div class="container">
          <h2>Get the best deals for rooms in Malaysia
          </h2>
          <div class="search_inners">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="search_list">
                    <div class="sign_upcooper">
                      <label>Destination / Hotel name
                      </label>
                      <div class="search_fellid">
                        <div class="form-group">
                        <input class=" search-query form-control" placeholder="City, hotel, place to go" type="text" name="place_city" id="map" value="">
                        <input type="hidden" name="latitude" class="gllpLatitude form-control" id="lat" value="">
                        <input type="hidden" name="longitude" class="gllpLongitude form-control" id="lng" value="">
                          <div class="search_buttons">
                            <button class="btn btn-danger" type="button">
                              <i class="glyph-icon flaticon-search">
                              </i>
                            </button>
                          </div>
                        </div>
                      </div>

                      <div class="search_fellid well configurator">
                        <div class="row">
                          <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group check_in">
                              <label for="startDate">Check-in:
                              </label>
                              <input type="text" class="form-control" id="startDate" value="{!! Session::get('start_date') !!}" name="startDate">
                            </div>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12 padding0">
                            <div class="duraction">
                              <label for="duration">Duration
                              </label>
                              <select name="duration" id="duration" class="js-example-disabled-results duration_hide" style="width:100%;">
                                <option value="0">@lang('messages.Select Duration')</option>
                                @if (count(getDuration()) > 0)
                                    @foreach (getDuration() as $key => $type)
                                        <?php $duration = Session::get('duration'); ?>
                                        <option value="{{ $key }}" <?php echo ($duration==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                          <div style="display: none;" id="dura">
                            <label for="duration">Duration</label>
                            <input type="text" id="dur" class="form-control" readonly="">
                          </div>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group check_out">
                              <label for="endDate">Check-out:
                              </label>
                              <?php 
                              //   STORED SESSION DATE FORMAT IS D-M-Y , SO IT SHOWS WRONG DATE , FIXED - AK
                                $ed = Session::get('end_date');  
                                $ed = date('m-d-Y', strtotime($ed));
                              ?>
                              <input type="text" class="form-control" id="endDate" value="{!! $ed !!}" name="endDate">
                              <div>
                                <p id="cout" style="display: none;" value=""></p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="select_roomes">
                    <label>No. of Guest(s) &amp; Room(s)
                    </label>
                    <div class="row">
                      <div class="col col-sm col-lg">
                        <div class="gust_common">
                          <div class="icon_bg">
                            <i class="glyph-icon flaticon-guest">
                            </i>
                          </div>
                          <div class="gust_qt">
                            <select class="js-example-disabled-results" name="guest_count" id="guest_count">
                                @if (count(getGuestcount()) > 0)
                                    @foreach (getGuestcount() as $key => $type)
                                        <option value="{{ $key }}" <?php echo (Session::get('guest_count')==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                                    @endforeach
                                @endif
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col col-sm col-lg">
                        <div class="gust_common">
                          <div class="icon_bg">
                            <i class="glyph-icon flaticon-double-king-size-bed">
                            </i>
                          </div>
                          <div class="gust_qt">
                            <select class="js-example-disabled-results" name="room_count" id="room_count">
                                @if (count(getRoomcount()) > 0)
                                    @foreach (getRoomcount() as $key => $type)
                                        <option value="{{ $key }}" <?php echo (Session::get('room_count')==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                                    @endforeach
                                @endif                              
                            </select>
                          </div>
                        </div>
                      </div>
                      
                      <input type="hidden" name="user_latitude" id="user_lat" value="">
                      <input type="hidden" name="user_longitude" id="user_lng" value="">

                      <div class="search_by_rooms">
                        <div class="col">
                          <button class="btn btn-primary" type="button" id="search_hotels">Search hotels
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
               
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="home_infomations">
    <div class="container">
      <div class="Intrro_section">
        <h2> 
          <span>
            <img src="{{ URL::asset('assets/front/otel2go/images/display.png') }}" alt="">
          </span>Join Otel2GO &amp; Be Smartfun Traveler
        </h2>
        <p>Otel2go transforms your travel into a memorable experience, that you will cherish for your lifetime.
          <br/>
            Experience a wider range of hotels at the best deals with us. 
        </p>
      </div>
      <div class="row">
        <div class="col-sm col-lg col-xs">
          <div class="list_works">
            <img src="{{ URL::asset('assets/front/otel2go/images/rewards.png') }}" alt="Mate Rewards ">
            <label>Mate Rewards 
            </label>
          </div>
        </div>
        <div class="col-sm col-lg col-xs">
          <div class="list_works">
            <img src="{{ URL::asset('assets/front/otel2go/images/rating.png') }}" alt="Mate Review and Rating">
            <label>Mate Review and Rating 
            </label>
          </div>
        </div>
        <div class="col-sm col-lg col-xs">
          <div class="list_works">
            <img src="{{ URL::asset('assets/front/otel2go/images/quality.png') }}" alt="Quality Assurance">
            <label>Quality Assurance
            </label>
          </div>
        </div>
        <div class="explore_title">
          <div class="col">
            <h3>Explore Destinations
            </h3>
          </div>
        </div>
        @if($city_details)
          @foreach($city_details as $c_key => $c_val)
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="list_of_explore">
                <a href="{{ URL::to('list-hotels/city/'.$c_val->url_index) }}">
                  <img src="{{ URL::asset('assets/admin/base/images/city/'.$c_val->city_image) }}" alt="">
                  <div class="city_names">
                    <h2>{{ $c_val->city_name}}
                    </h2>
                    <h4>{{ $c_val->outlet_count }} Hotels
                    </h4>
                  </div>
                </a>
              </div>
            </div>
          @endforeach
        @endif
        <div class="explore_title">
          <div class="col">
            <h3>Inspired Travelers
            </h3>
          </div>
        </div>
        @if($categories)
          @foreach($categories as $cat_key => $cat_val)
            <div class="col-md-4 col-sm-6 col-xs-12">
              <div class="list_of_explore">
                <a href="#">
                <a href="{{ URL::to('list-hotels/category/'.$cat_val->url_key) }}">  
                  <img src="{{ URL::asset('assets/admin/base/images/category/'.$cat_val->image) }}" alt="">
                  <div class="city_names">
                    <h2>{{ $cat_val->category_name }}
                    </h2>
                  </div>
                </a>
              </div>
            </div>
          @endforeach
        @endif
      </div>
      <div class="space">
      </div>
    </div>
  </section>
<script type="text/javascript">
  

    $(document).ready(function(){

    //  IF DURATION DATE CHANEGS MEANS END-DATE AUTOMATICALLY CALCUTATE

      $("#duration").on('change',function(){      

          
          var dur = $("#duration").val();

          //  alert(dur);

          if(dur == 0)
          {

            $('#endDate').daterangepicker({
              singleDatePicker: true,
              minDate: moment().add(1, 'days'),
              startDate: moment().add(1, 'days')
            });    
            $('#cout').hide();              
            $("#endDate" ).show();
            $("#dura").hide();
          }
          else
          {
         
            $('#endDate').daterangepicker({
              singleDatePicker: true,
              startDate: moment().add(dur, 'days')
            });     

            var ed = $("#endDate").val();       

            $("#endDate" ).hide();
            $("#dura").hide();
            $('#cout').text(ed).show();
            
          }
      });

          $("#endDate").on('change',function(){

            var From_date = new Date($("#startDate").val());
            var To_date = new Date($("#endDate").val());
            var diff_date =  To_date - From_date;

            var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);

            var dur = $("#duration").val();

            //  alert(dur);

            
            $("#dur").val(days+" Days");

            if(days > 1 && dur == 0)
            {
                $(".duraction").hide();
                $("#dura").show();
            }          
            else
            {
                $(".duraction").show();
                $("#dura").hide();  
            }
          });

    });

</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo env('GOOGLE_MAP_API_KEY'); ?>"></script>

<script type="text/javascript">

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            //  console.log(position);
            // console.log(position.coords.latitude);
            // console.log(position.coords.longitude);

          $("#user_lat").val(position.coords.latitude);
          $("#user_lng").val(position.coords.longitude);
        });
    }

</script>

<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-gmaps-latlon-picker.js') }}"></script>
<script type="text/javascript">
  var geocoder = new google.maps.Geocoder();
    function geocodePosition(pos) {
    geocoder.geocode({
      latLng: pos
    }, 
    function(responses) {
      if (responses && responses.length > 0) {
      } else {

      }
    });
    }
    function updateMarkerStatus(str) {
    }
    function updateMarkerPosition(latLng) {
    document.getElementById('lat').value=latLng.lat(); 
    document.getElementById('lng').value=latLng.lng();
    }
    function initialize() {
    var latLng = new google.maps.LatLng(29.776255,45.9416983);
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: latLng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    google.maps.event.trigger(map, "resize");
    var marker = new google.maps.Marker({
      position: latLng,
      title: 'Drag this Marker',
      map: map,
      draggable: true
    });
    var input = /** @type {HTMLInputElement}*/(document.getElementById('map'));
    var searchBox = new google.maps.places.SearchBox(input);
    var markers = [];
    google.maps.event.addListener(searchBox, 'places_changed', function() {
      var places = searchBox.getPlaces();
      for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
      }
      markers = [];
      var bounds = new google.maps.LatLngBounds();
      for (var i = 0, place; place = places[i]; i++) {
      var marker = new google.maps.Marker({
        map: map, 
        title: place.name,
        position: place.geometry.location,
        draggable: true
      });
      markers.push(marker);
      dragmarker(marker);
      updateMarkerPosition(place.geometry.location);
      geocodePosition(place.geometry.location);
      bounds.extend(place.geometry.location);
      }
      map.fitBounds(bounds);
    });
      
    // Update current position info.
    updateMarkerPosition(latLng);
    geocodePosition(latLng);
    dragmarker(marker); 
    }
    function dragmarker(marker)
    {
    google.maps.event.addListener(marker, 'dragstart', function() {
      updateMarkerAddress('Searching...');
    });
    google.maps.event.addListener(marker, 'drag', function() {
      updateMarkerStatus('Dragging...');
      updateMarkerPosition(marker.getPosition());
    });
    google.maps.event.addListener(marker, 'dragend', function() {
      updateMarkerStatus('Drag ended');
      geocodePosition(marker.getPosition());
    });
    }
    // Onload handler to fire off the app.
    google.maps.event.addDomListener(window, 'load', initialize); 
    function disableEnterKey(e)
    {
    var key;      
    if(window.event)
      key = window.event.keyCode; //IE
    else
      key = e.which; //firefox
    return (key != 13);
    } 
</script>
  <script>
    $('.front_header').show();
    $(window).load(function() {
      $('.blueberry').blueberry();
      $('select').select2();
      
      $('#config-text').keyup(function() {
        eval($(this).val());
      });
      $('.configurator input, .configurator select').change(function() {
        updateConfig();
      });
      $('.demo i').click(function() {
        $(this).parent().find('input').click();
      });
      $('#startDate').daterangepicker({
        singleDatePicker: true,
        minDate: new Date(),
        startDate: moment()
      });
      $('#endDate').daterangepicker({
        singleDatePicker: true,
        minDate: moment().add(1, 'days')
        //  startDate: moment().add(1, 'days')
      });
      updateConfig();
      function updateConfig() {
        var options = {
        };
        if ($('#singleDatePicker').is(':checked'))
          options.singleDatePicker = true;
        if ($('#showDropdowns').is(':checked'))
          options.showDropdowns = true;
        if ($('#showWeekNumbers').is(':checked'))
          options.showWeekNumbers = true;
        if ($('#showISOWeekNumbers').is(':checked'))
          options.showISOWeekNumbers = true;
        if ($('#timePicker').is(':checked'))
          options.timePicker = true;
        if ($('#timePicker24Hour').is(':checked'))
          options.timePicker24Hour = true;
        if ($('#timePickerIncrement').val() != 1)
          options.timePickerIncrement = parseInt($('#timePickerIncrement').val(), 10);
        if ($('#timePickerSeconds').is(':checked'))
          options.timePickerSeconds = true;
        if ($('#autoApply').is(':checked'))
          options.autoApply = true;
        if ($('#dateLimit').is(':checked'))
          options.dateLimit = {
            days: 7 };
        if ($('#ranges').is(':checked')) {
          options.ranges = {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          };
        }
        if ($('#locale').is(':checked')) {
          $('#rtl-wrap').show();
          options.locale = {
            direction: $('#rtl').is(':checked') ? 'rtl' : 'ltr',
            format: 'MM/DD/YYYY HH:mm',
            separator: ' - ',
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          };
        }
        else {
          $('#rtl-wrap').hide();
        }
        if (!$('#linkedCalendars').is(':checked'))
          options.linkedCalendars = false;
        if (!$('#autoUpdateInput').is(':checked'))
          options.autoUpdateInput = false;
        if (!$('#showCustomRangeLabel').is(':checked'))
          options.showCustomRangeLabel = false;
        if ($('#alwaysShowCalendars').is(':checked'))
          options.alwaysShowCalendars = true;
        if ($('#parentEl').val())
          options.parentEl = $('#parentEl').val();
        if ($('#startDate').val()) 
          options.startDate = $('#startDate').val();
        if ($('#endDate').val())
          options.endDate = $('#endDate').val();
        if ($('#minDate').val())
          options.minDate = $('#minDate').val();
        if ($('#maxDate').val())
          options.maxDate = $('#maxDate').val();
        if ($('#opens').val() && $('#opens').val() != 'right')
          options.opens = $('#opens').val();
        if ($('#drops').val() && $('#drops').val() != 'down')
          options.drops = $('#drops').val();
        if ($('#buttonClasses').val() && $('#buttonClasses').val() != 'btn btn-sm')
          options.buttonClasses = $('#buttonClasses').val();
        if ($('#applyClass').val() && $('#applyClass').val() != 'btn-success')
          options.applyClass = $('#applyClass').val();
        if ($('#cancelClass').val() && $('#cancelClass').val() != 'btn-default')
          options.cancelClass = $('#cancelClass').val();
        $('#config-text').val("$('#demo').daterangepicker(" + JSON.stringify(options, null, '    ') + ", function(start, end, label) {\n  console.log(\"New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')\");\n});");
        $('#config-demo').daterangepicker(options, function(start, end, label) {
          console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        }
                                         );
      }
    }
                  );
  </script>
  <script type="text/javascript">
    

    $('#search_hotels').on('click', function() {
        // 
        //  var city = $("#city_id").val();
        //  var category = $(this).attr("data-url");

        var lat = $("#lat").val();
        var lng = $("#lng").val();
        var location = $(".search-query").val();
        var startDate = $("#startDate").val();
        var endDate = $("#endDate").val();
        var guest_count = $("#guest_count").val();
        var room_count = $("#room_count").val();
        var user_lat = $("#user_lat").val();
        var user_lng = $("#user_lng").val();

        if(user_lat == "")
        {
           var user_lat = lat; 
        }
        if(user_lng == "")
        {
           var user_lng = lng; 
        }
        //  alert(startDate);

        var sd = moment(startDate).format("YYYY-MM-DD");

        var ed = moment(endDate).format("YYYY-MM-DD");
      
        //  alert(sd);

        if(location == "")
        {
            toastr.remove();
            toastr.error('Please Enter Destination');
            return false;
        }

        
        //var url = '<?php echo url('store'); ?>?location='+location+'&category_ids='+category;
/*        
        var url = '{{url("hotels/")}}/'+location+'/'+lat+'/'+lng+'/'+startDate+'/'+endDate+'/'+guest_count+'/'+room_count;
*/        
        //  sdChange();
        var url = '{{url("hotels/")}}/'+location+'/'+lat+'/'+lng+'/'+sd+'/'+ed+'/'+guest_count+'/'+room_count+'/'+user_lat+'/'+user_lng;
        window.location = url;
    });

  </script>  

  <script type="text/javascript">
  
  /*
    function sdChange()
      {
          //  alert('Ak');

        var sd,ed,duration,room_count,adult_count;


        token = $("input[name=_token]").val();
        url = '{{   url('days_check')    }}';

        sd = $("#startDate").val();
        ed = $("#endDate").val();
        duration = $("#duration").val();
        room_count = $("#room_count").val();
        adult_count = $("#guest_count").val();

        data = {sd:sd,ed:ed,duration:duration,room_count:room_count,adult_count:adult_count};

        $.ajax({

            url : url,
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            type: 'POST',
            datatype: 'JSON',
            success: function (resp) {

                            if(resp.data!='')
                            {
                                //  toastr.error('success');

                                //  location.reload();

                                $("#duration").val({{  Session::get('duration') }});
                            }
                            else
                            {
                                //  toastr.error('No data');
                            }
                        }
        });               
      }  

*/
</script>

   <!-- jQuery first, then Tether, then Bootstrap JS. -->
  <script src="{{ URL::asset('assets/front/otel2go/js/jquery.blueberry.js') }}">  </script>
  <script type="text/javascript" src="{{ URL::asset('assets/front/otel2go/js/moment.js') }}">  </script>
  <script type="text/javascript" src="{{ URL::asset('assets/front/otel2go/js/daterangepicker.js') }}">  </script>
  @endsection