<div class="modal fade forgot-password" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="singup_info">
        <div class="sign_up">
			 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
          <div class="signup_inner_containt ">
            <div class="signup_image">
               <img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/otel2go1.png'); ?>"  alt="forgot-password">
              <h4>FORGOT PASSWORD</h4>
              <i class="signup_line"></i>
            </div>
            {!!Form::open(array('method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'forgot_password' ,'onsubmit'=>'return forgot_password()'));!!}
              <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" class="form-control" placeholder="Email" required>
              </div>
              <div class="buttons forgot_btn">
                <div class="row">
                  <div class="col-sm col-md padding_right">
                    <button type="button" class="btn cancel_btn signup_btn" id="cancel_btn">Cancel</button>
                  </div>
                  <div class="col-sm col-md">
                    <button type="submit" class="btn signup_btn send-email">Send Email</button>
                  </div>
                </div>
              </div>
            {!!Form::close();!!} 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#cancel_btn').click(function(){
        $('.forgot-password').modal('hide');
      });
  });   
    function forgot_password(){  
        $this = $(".send-email");
        $this.html('Sending....');
        data = $("#forgot_password").serializeArray();
        var c_url = '/forgot_password';
        token = $('input[name=_token]').val();
        $.ajax({
            url: c_url,
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            type: 'POST',
            datatype: 'JSON',
            success: function (resp)
            {
                //  $this.button('reset');
                $(".send-email").html('Send Email');
                data = resp;
                if(data.httpCode == 200)
                {  
                    $('.forgot-password').modal('hide');
                    toastr.success(data.Message);
                    $('#forgot_password').trigger('reset');
                    $this.html('Send Email');
                }
                else
                {
                    
                    toastr.warning(data.Message);
                    $this.html('Send Email');
                }
            },
            error:function(resp)
            {
                //  $this.button('reset');
                console.log('out--'+data); 
                return false;
            }
        });
        return false;
    }
</script>