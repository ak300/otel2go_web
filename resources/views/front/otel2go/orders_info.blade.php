@extends('layouts.front')
@section('content')
<?php $general = Session::get("general");$currency_side = getCurrencyPosition()->currency_side;$currency_symbol = getCurrency(); $language = getCurrentLang();

//print_r($delivery_details[0]->service_tax);exit;
?>


<script src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/plugins/rateit/src/jquery.rateit.js');?>"></script>
<link href="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/plugins/rateit/src/rateit.css');?>" rel="stylesheet">
<section class="store_item_list">
    <div class="container">
       
            <div class="my_account_section">
                <div id="parentHorizontalTab">
                 <div class="row">
                    <div class="col-md-3">
                        @include('front.'.Session::get("general")->theme.'.profile_sidebar')
                    </div>
                    <div class="col-md-9 order_info_text_right">
                        <div class="right_descript">
                            <div class="resp-tabs-container hor_1">
                                <div class="profile_sections">
                                    <h2 class="pay_title">@lang('messages.Order Summary')</h2>
                                    <?php if($booking_details[0]->booking_status == 3){  ?>
                                      <div class="col-md-12">
                                            <div class="reviewblock order_payments_reorder">
                                                <?php $booking_id = encrypt($booking_details[0]->bid); ?>
                                                <a class="btn btn-info" data-toggle="modal" data-target="#myModal3" title="@lang('messages.Review')">@lang('messages.Review')</a>
                                            </div>
                                        </div>
                                        
                                    <?php  } ?>

                                    <?php /*
                                    echo "<pre>";
                                    print_r($outlets);die; */ ?>
                                    <div class="stores_det_info">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="store_det_in">
                                                <img width="161px" height="107px" alt="{{ ucfirst($outlets[0]->outlet_name) }}" src="{{ URL::asset('assets/admin/base/images/vendors/property_image/list/'.$outlets[0]->outlet_image) }}" >
                                                
                                                <p>{{ucfirst($outlets[0]->outlet_name)}}</p>
                                                <p>@lang('messages.Phone number'): {{$outlets[0]->contact_phone}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-5 store_mg">
                                            <div class="info_store">
                                            <div class="row">
                                                <div class="col col-ms-6 col-xs-6">
                                                    <h4>@lang('messages.Order Id')</h4>
                                                </div>
                                                <div class="col col-ms-6 col-xs-6">
                                                    <h5>{{ $booking_details[0]->booking_random_id  }}</h5>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="info_store">
                                            <div class="row">
                                                <div class="col col-ms-6 col-xs-6">
                                                    <h4>@lang('messages.Date')</h4>
                                                </div>
                                                <div class="col col-ms-6 col-xs-6">
                                                    <h5>{{ date('d M, Y', strtotime($outlets[0]->created_date)) }}</h5>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="info_store">
                                            <div class="row">
                                                <div class="col col-ms-6 col-xs-6">
                                                    <h4>@lang('messages.Booking Status')</h4>
                                                </div>
                                                <div class="col col-ms-6 col-xs-6">
                                                    <h5><?php echo $booking_details[0]->name; ?></h5>
                                                </div>
                                            </div>
                                            </div>
                                            <?php /* if($vendor_info->order_status==17 && (count($return_orders_result)>0)){?>
                                                <div class="info_store">
                                                    <div class="col-md-6 col-ms-6 col-xs-6">
                                                        <h4>@lang('messages.Return Status')</h4>
                                                    </div>
                                                    <div class="col-md-6 col-ms-6 col-xs-6">
                                                        <h5><?php echo $return_orders_result->return_action_name; ?></h5>
                                                    </div>
                                                </div>
                                                <div class="info_store">
                                                    <div class="col-md-6 col-ms-6 col-xs-6">
                                                        <h4>@lang('messages.Return Action')</h4>
                                                    </div>
                                                    <div class="col-md-6 col-ms-6 col-xs-6">
                                                        <h5><?php echo $return_orders_result->return_status_name; ?></h5>
                                                    </div>
                                                </div>
                                            <?php } */ ?>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="my_account_sections">
                                        <div class="table-responsive">
                                            <div class="cart_sections_tables">
                                                <div class="table-responsive">
                                                    <div class="responsive_scroll">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="2" style="text-align:left;">@lang('messages.Bill details')</th>
                                                                    <th></th>
                                                                </tr>
                                                                <tr>
                                                                    <th style="text-align:left;">@lang('messages.Rooms')</th>
                                                                   
                                                                    <th>@lang('messages.Nights')</th>
                                                                    <th>@lang('messages.Price')</th>
                                                                 
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $sub_total = 0; foreach($rooms_count as $items){ ?>
                                                                    <tr>
                                                                        <td style="text-align:left;">
                                                                            {{ wordwrap(ucfirst(strtolower($items->room_name)),64,'<br />') }}
                                                                        </td>
                                                                       
                                                                        <td>
                                                                            {{$booking_details[0]->no_of_days}}
                                                                        </td>
                                                                       
                                                                        <td valign="middle">
                                                                            <?php if($currency_side == 1) { ?>
                                                                                {{ $currency_symbol." ".($booking_details[0]->room_type_cost*$booking_details[0]->no_of_days) }}
                                                                            <?php } else { ?>
                                                                                {{ ($booking_details[0]->room_type_cost*$booking_details[0]->no_of_days)." ".$currency_symbol }}
                                                                            <?php } ?>
                                                                        </td>
																		
                                                                    </tr>
                                                                    <?php /* $sub_total += $booking_details[0]->original_price*$booking_details[0]->no_of_days; */ ?>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="price_setions">
                                                            <div class="price_setions_list">
                                                            <div class="row">
                                                                <div class="col col-sm-7 dis_none"></div>
                                                                <div class="col col-sm-3 col-xs-6"><label>@lang('messages.Subtotal')</label></div>
                                                                <div class="col col-sm-2 col-xs-6">
                                                                    <p>
                                                                        <?php if($currency_side == 1) { ?>
                                                                            {{ $currency_symbol." ".$booking_details[0]->original_price}}
                                                                        <?php } else { ?>
                                                                            {{ $booking_details[0]->original_price." ".$currency_symbol}}
                                                                        <?php } ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            </div>
															
                                            
                                                            <?php /* if($delivery_details[0]->service_tax >0) { */ ?><?php /*
                                                                <div class="price_setions_list">
                                                                    <div class="col-md-7 dis_none"></div>
                                                                    <div class="col-md-3 col-sm-6 col-xs-6"><label>@lang('messages.tax')</label></div>
                                                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                                                        <p>
                                                                            <?php 
                                                                            
                                                                            $service_tax = isset($delivery_details[0]->service_tax)?$delivery_details[0]->service_tax:0;
                                                                            
                                                                            if($currency_side == 1) {  */ ?> <?php /*
                                                                                {{$currency_symbol.$service_tax}}
                                                                            <?php } else { 
                                                                            
                                                                            */ ?><?php /*
                                                                                {{$service_tax.$currency_symbol}}
                                                                            <?php } */ ?><?php /*
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            <?php } */ ?>
                                                            <?php if($booking_details[0]->coupon_amount != 0) { ?>
                                                                <div class="price_setions_list">
                                                                <div class="row">
                                                                    <div class="col col-sm-7 dis_none"></div>
                                                                    <div class="col col-sm-3 col-xs-6"><label>@lang('messages.Promo code discount')</label></div>
                                                                    <div class="col col-sm-2 col-xs-6">
                                                                        <p>
                                                                            <?php if($currency_side == 1) { ?>
                                                                                {{$currency_symbol." ".$booking_details[0]->coupon_amount}}
                                                                            <?php } else { ?>
                                                                                {{$booking_details[0]->coupon_amount." ".$currency_symbol}}
                                                                            <?php } ?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                            <?php } ?>
                                                            
                                                            <?php /* if($delivery_details[0]->tips_percentage_amount != 0) { */ ?><?php /*
                                                                <div class="price_setions_list">
                                                                <div class="col-md-7 dis_none"></div>
                                                                <div class="col-md-3 col-sm-6 col-xs-6"><label>@lang('messages.Tips Amount')</label></div>
                                                                <div class="col-md-2 col-sm-6 col-xs-6"><p><?php if($currency_side == 1) { echo $currency_symbol.round($delivery_details[0]->tips_percentage_amount,2);} else { echo round($delivery_details[0]->tips_percentage_amount,2).$currency_symbol;} */ ?><?php /*</p></div>
                                                                </div>
                                                            <?php } */ ?>
                                                            
                                                            
                                                            <div class="price_setions_list_total">
                                                            <div class="row">
                                                                <div class="col col-sm-7 dis_none"></div>
                                                                <div class="col col-sm-3 col-xs-6"><label>@lang('messages.Total')</label></div>
                                                                <div class="col col-sm-2col-xs-6">
                                                                    <p>
                                                                        <?php if($currency_side == 1) { 
                                                                            echo $currency_symbol." ".round($booking_details[0]->bc_charges,2);
                                                                         } else { 
                                                                             echo round($booking_details[0]->bc_charges,2)." ".$currency_symbol;
                                                                         } ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            </div>
                                                        </div>

                                    <!--  NOW COMMENTED DUE TO PAYMENT GATEWAY IMPLEMENTATION ISSUE  - AK -->

                                                        <div class="delivery_det">
                                                            <?php /* payment_gateway_nameif($delivery_details[0]->login_type == 1){ */ ?><?php /*
                                                            <div class="deli_list">
                                                                <div class="col-md-3 col-sm-6 col-xs-6">
                                                                    <h4>@lang('messages.Service address')</h4>
                                                                </div>
                                                                <div class="col-md-9 col-sm-6 col-xs-6 padding_right0">
                                                                    <h4>@if($delivery_details[0]->user_contact_address){{$delivery_details[0]->user_contact_address}} @else  &nbsp; @endif</h4>
                                                                </div>
                                                            </div>	
                                                            <div class="deli_list">
                                                                <div class="col-md-3 col-sm-6 col-xs-6">
                                                                    <h4>@lang('messages.Payment mode')</h4>
                                                                </div>
                                                                <div class="col-md-9 col-sm-6 col-xs-6 padding_right0">
                                                                    <h4>{{ $vendor_info->payment_gateway_namepayment_gateway_name  }}</h4>
                                                                </div>
                                                            </div>
                                                            <?php */ ?>
                                                        <?php /* } else */ ?><?php { ?>
                                                            <div class="deli_list">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-6 col-xs-6">
                                                                    <h4>@lang('messages.Hotel Address')</h4>
                                                                </div>
                                                                <div class="col-md-9 col-sm-6 col-xs-6 padding_right0">
                                                                    <h4>{{$outlets[0]->contact_address}}</h4>
                                                                </div>
                                                              </div>
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                                                        <h4>@lang('messages.Payment Method')</h4>
                                                                    </div>
                                                                   <div class="col-md-9 col-sm-6 col-xs-6 padding_right0">
                                                               <?php $payment_type = $booking_details[0]->payment_type; ?>
                                                                                    
                                                                    @if($payment_type == 1)

                                                                        <h4>Card Payment</h4>
                                                                    @else

                                                                        <h4>Pay at Hotel</h4> 
                                                                    @endif                                                              
                                                            </div>                                                                 
                                                            </div>                                                                 
                                                                 <?php /* <div class="col-md-9 col-sm-6 col-xs-6 padding_right0">
                                                                        <h4>{{ucfirst(strtolower($vendor_info->payment_gateway_name))  }}</h4>
                                                                    </div>
                                                                    */ ?>
                                                               
                                                               
                                                           
                                                            </div>
                                                            
                                                        <?php } ?>
                                                        <?php /*
                                                           <div class="deli_list">
                                                                <div class="col-md-3 col-sm-6 col-xs-6">
                                                                    <h4>@lang('messages.Professional')</h4>
                                                                </div>
                                                                <div class="col-md-9 col-sm-6 col-xs-6 padding_right0">
                                                                    <h4>{{ucfirst($booking_details[0]->firstname." ".$booking_details[0]->lastname)}}</h4>
                                                                </div>
                                                            </div>
                                                          */ ?>
                                                            <div class="deli_list">
                                                            <div class="row">
                                                            <div class="col-md-3 col-sm-6 col-xs-6 padding_right0">
                                                                <h4>@lang('messages.Check-in Date')</h4>
                                                            </div>
                                                            <div class="col-md-9 col-sm-6 col-xs-6 padding_right0">
                                                                <?php $delivery_date = date("d F, l Y", strtotime($booking_details[0]->check_in_date)); 
                                                                ?>
                                                                <h4>{{ $delivery_date }}</h4>
                                                            </div>
                                                            <div class="col-md-3 col-sm-6 col-xs-6 padding_right0">
                                                                <h4>@lang('messages.Check-out Date')</h4>
                                                            </div>
                                                            <div class="col-md-9 col-sm-6 col-xs-6 padding_right0">
                                                                <?php $delivery_date = date("d F, l Y", strtotime($booking_details[0]->check_out_date)); 
                                                                ?>
                                                                <h4>{{ $delivery_date }}</h4>
                                                            </div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <?php 
                                                    /*  $order_id = encrypt($vendor_info->order_id); */ ?>
                                                        

                                                        
                                                        
                    <div class="order_payments">
                        <div class="border_top_reor">
                            <div class="order_payments_reorder">
                            <?php /* $order_date=$vendor_info->created_date; */ ?>
                            
                            <?php /* $cancel_time=$vendor_info->cancel_time.'mins'; */ ?>
                            
                            <?php $currentime = Carbon\Carbon::now(); ?>
                            <?php $date = date_create($booking_details[0]->created_date);?>
                            <?php $cancel_time = date_format($date, 'Y-m-d H:i:s');?>

                <?php $booking_id = encrypt($booking_details[0]->bid); ?>
                            
                <?php if(($booking_details[0]->booking_status == 5) || $booking_details[0]->booking_status == 1) { 
                             //   befor ($vendor_info->order_status != 2) && ($vendor_info->order_status != 3)
                                  ?>
                        <a class="btn btn-primary" href="{{ URL::to('/cancel_order/'.$booking_id) }}" title="@lang('messages.Cancel')">@lang('messages.Cancel')</a> 
                <?php  }  ?>

                    <!--    -->
                        <a class="btn btn-primary reorder-btn" href="{{URL::to('/re-book-order/'.$booking_id)}}" title="@lang('messages.Re Book')">@lang('messages.Re Book')</a>

                                <?php $outlet_id = $outlet_id; ?>
                                                            
                                <?php if($cancel_time > $currentime->toDateTimeString()){  ?>
                                <a class="btn btn-primary" href="{{URL::to('/cancel-order/'.$outlet_id)}}" title="@lang('messages.Cancel')">@lang('messages.Cancel')</a>
                        <?php }  ?>
                                       <?php /*  if($booking_details[0]->booking_status==12){  */ ?><?php /*
                                            <button class="btn btn-primary return_order"  title="Return" type="button">@lang('messages.Return')</button>
                                        <?php } */ ?>

                                        <?php if($booking_details[0]->booking_status == 3 ) { ?>
                                        <a class="btn btn-primary" target="_blank" href="{{URL::to('/invoice-order/'.$outlet_id)}}" title="@lang('messages.Invoice')">@lang('messages.Invoice')</a>
                                        <?php } ?>
                                        <?php /* $order_id = encrypt($booking_details[0]->bid); */ ?>
                                        <?php /*
                                        <a class="btn btn-info" href="{{URL::to('/re-order/'.$order_id)}}" title="@lang('messages.Reorder')">@lang('messages.Reorder')</a>
                                        */ ?>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>


<!-- Modal for membership signIn -->
<div class="modal fade model_for_signup membership_login" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="logo_popup">
                    <img src="<?php echo url('/assets/front/'.Session::get("general")->theme.'/images/'.strtolower(Session::get("general")->theme).'.png');  ?>" title="{{ Session::get('general')->site_name }}" alt="{{ Session::get('general')->site_name }}">
                </span>
            </div>
            <div class="modal-body">
                <div class="sign_up_inner">
                    <h2>@lang('messages.Lets rate the store')<br><span class="bottom_border"></span></h2>
                    {!!Form::open(array('url' => 'rating', 'method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'rating' ,'onsubmit'=>'return rating()'));!!} 
                        <div class="membership_inner">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">


                                    <label>@lang('messages.Star rating') : </label>
                                    <div class="rateit" data-rateit-value="1" id="rateit"  style="cursor:pointer" > </div>
                                    <input type="hidden" name="starrating" value="1" class="form-control rating_value">
                                    <span class="error"> @if ($errors->has('starrating')){{ $errors->first('starrating', ':message') }}@endif</span>

                                    <div class="strss_over"><span class="clr4 value5"></span><span class="hover5"></span></div>
                                    <?php $star_descritption=array(trans('messages.Very Poor'),trans('messages.Poor'),trans('messages.Average'),trans('messages.Good'),trans('messages.Very Good'));
                                    $description= (array)$star_descritption;  ?>
                                    <script type="text/javascript">
                                        var tooltipvalues = <?php echo json_encode(array_values($description)); ?>;
                                        $("#rateit").bind('rated', function (event, value) { $('.rating_value').val(value); $('.value5').text('You\'ve rated it: ' + value +' '+$('#rateit').attr("title")); });
                                        $("#rateit").bind('reset', function () { $('.value5').text('Rating reset'); });
                                        $("#rateit").bind('over', function (event, value) {
                                            if(value) { var val= value }else { val='';}  
                                            $(this).attr('title', tooltipvalues[val-1]);
                                            $('.value5').text(val +' '+$('#rateit').attr("title")); 
                                        });
                                        $("#rateit").rateit({ max: 5, step: 1 ,resetable: false});
                                    </script>

                                    <label>@lang('messages.Location') : </label>
                                    <div class="rateit" data-rateit-value="1" id="location"  style="cursor:pointer" > </div>
                                    <input type="hidden" name="location" value="1" class="form-control location">
                                    <span class="error"> @if ($errors->has('location')){{ $errors->first('starrating', ':message') }}@endif</span>

                                    <div class="strss_over"><span class="clr4 value4"></span><span class="hover5"></span></div>
                                    <?php $star_descritption=array(trans('messages.Very Poor'),trans('messages.Poor'),trans('messages.Average'),trans('messages.Good'),trans('messages.Very Good'));
                                    $description= (array)$star_descritption;  ?>
                                    <script type="text/javascript">
                                        var tooltipvalues = <?php echo json_encode(array_values($description)); ?>;
                                        $("#location").bind('rated', function (event, value) { $('.location').val(value); $('.value4').text('You\'ve rated it: ' + value +' '+$('#location').attr("title")); });
                                        $("#location").bind('reset', function () { $('.value4').text('Rating reset'); });
                                        $("#location").bind('over', function (event, value) {
                                            if(value) { var val= value }else { val='';}  
                                            $(this).attr('title', tooltipvalues[val-1]);
                                            $('.value4').text(val +' '+$('#location').attr("title")); 
                                        });
                                        $("#location").rateit({ max: 5, step: 1 ,resetable: false});
                                    </script>


                                    <label>@lang('messages.Comfort') : </label>
                                    <div class="rateit" data-rateit-value="1" id="comfort"  style="cursor:pointer" > </div>
                                    <input type="hidden" name="comfort" value="1" class="form-control comfort">
                                    <span class="error"> @if ($errors->has('comfort')){{ $errors->first('starrating', ':message') }}@endif</span>

                                    <div class="strss_over"><span class="clr4 value3"></span><span class="hover5"></span></div>
                                    <?php $star_descritption=array(trans('messages.Very Poor'),trans('messages.Poor'),trans('messages.Average'),trans('messages.Good'),trans('messages.Very Good'));
                                    $description= (array)$star_descritption;  ?>
                                    <script type="text/javascript">
                                        var tooltipvalues = <?php echo json_encode(array_values($description)); ?>;
                                        $("#comfort").bind('rated', function (event, value) { $('.comfort').val(value); $('.value3').text('You\'ve rated it: ' + value +' '+$('#comfort').attr("title")); });
                                        $("#comfort").bind('reset', function () { $('.value3').text('Rating reset'); });
                                        $("#comfort").bind('over', function (event, value) {
                                            if(value) { var val= value }else { val='';}  
                                            $(this).attr('title', tooltipvalues[val-1]);
                                            $('.value3').text(val +' '+$('#comfort').attr("title")); 
                                        });
                                        $("#comfort").rateit({ max: 5, step: 1 ,resetable: false});
                                    </script>       


                                    <label>@lang('messages.Sleep Quality') : </label>
                                    <div class="rateit" data-rateit-value="1" id="sleep_quality"  style="cursor:pointer" > </div>
                                    <input type="hidden" name="sleep_quality" value="1" class="form-control sleep_quality">
                                    <span class="error"> @if ($errors->has('sleep_quality')){{ $errors->first('starrating', ':message') }}@endif</span>

                                    <div class="strss_over"><span class="clr4 value2"></span><span class="hover5"></span></div>
                                    <?php $star_descritption=array(trans('messages.Very Poor'),trans('messages.Poor'),trans('messages.Average'),trans('messages.Good'),trans('messages.Very Good'));
                                    $description= (array)$star_descritption;  ?>
                                    <script type="text/javascript">
                                        var tooltipvalues = <?php echo json_encode(array_values($description)); ?>;
                                        $("#sleep_quality").bind('rated', function (event, value) { $('.sleep_quality').val(value); $('.value2').text('You\'ve rated it: ' + value +' '+$('#sleep_quality').attr("title")); });
                                        $("#sleep_quality").bind('reset', function () { $('.value2').text('Rating reset'); });
                                        $("#sleep_quality").bind('over', function (event, value) {
                                            if(value) { var val= value }else { val='';}  
                                            $(this).attr('title', tooltipvalues[val-1]);
                                            $('.value2').text(val +' '+$('#sleep_quality').attr("title")); 
                                        });
                                        $("#sleep_quality").rateit({ max: 5, step: 1 ,resetable: false});
                                    </script> 


                                    <label>@lang('messages.Rooms') : </label>
                                    <div class="rateit" data-rateit-value="1" id="rooms"  style="cursor:pointer" > </div>
                                    <input type="hidden" name="rooms" value="1" class="form-control rooms">
                                    <span class="error"> @if ($errors->has('rooms')){{ $errors->first('starrating', ':message') }}@endif</span>

                                    <div class="strss_over"><span class="clr4 value1"></span><span class="hover5"></span></div>
                                    <?php $star_descritption=array(trans('messages.Very Poor'),trans('messages.Poor'),trans('messages.Average'),trans('messages.Good'),trans('messages.Very Good'));
                                    $description= (array)$star_descritption;  ?>
                                    <script type="text/javascript">
                                        var tooltipvalues = <?php echo json_encode(array_values($description)); ?>;
                                        $("#rooms").bind('rated', function (event, value) { $('.rooms').val(value); $('.value1').text('You\'ve rated it: ' + value +' '+$('#rooms').attr("title")); });
                                        $("#rooms").bind('reset', function () { $('.value1').text('Rating reset'); });
                                        $("#rooms").bind('over', function (event, value) {
                                            if(value) { var val= value }else { val='';}  
                                            $(this).attr('title', tooltipvalues[val-1]);
                                            $('.value1').text(val +' '+$('#rooms').attr("title")); 
                                        });
                                        $("#rooms").rateit({ max: 5, step: 1 ,resetable: false});
                                    </script> 


                                    <label>@lang('messages.Cleanliness') : </label>
                                    <div class="rateit" data-rateit-value="1" id="cleanliness"  style="cursor:pointer" > </div>
                                    <input type="hidden" name="cleanliness" value="1" class="form-control cleanliness">
                                    <span class="error"> @if ($errors->has('cleanliness')){{ $errors->first('starrating', ':message') }}@endif</span>

                                    <div class="strss_over"><span class="clr4 value0"></span><span class="hover5"></span></div>
                                    <?php $star_descritption=array(trans('messages.Very Poor'),trans('messages.Poor'),trans('messages.Average'),trans('messages.Good'),trans('messages.Very Good'));
                                    $description= (array)$star_descritption;  ?>
                                    <script type="text/javascript">
                                        var tooltipvalues = <?php echo json_encode(array_values($description)); ?>;
                                        $("#cleanliness").bind('rated', function (event, value) { $('.cleanliness').val(value); $('.value0').text('You\'ve rated it: ' + value +' '+$('#cleanliness').attr("title")); });
                                        $("#cleanliness").bind('reset', function () { $('.value0').text('Rating reset'); });
                                        $("#cleanliness").bind('over', function (event, value) {
                                            if(value) { var val= value }else { val='';}  
                                            $(this).attr('title', tooltipvalues[val-1]);
                                            $('.value0').text(val +' '+$('#cleanliness').attr("title")); 
                                        });
                                        $("#cleanliness").rateit({ max: 5, step: 1 ,resetable: false});
                                    </script>                                                                                                                                     

                                    <input type="hidden" name="outlet_id" value="{{ $outlet_id }}" class="form-control">
                                    <input type="hidden" name="user_id" value="{{ Session::get('user_id') }}" class="form-control">
                                    <input type="hidden" name="vendor_id" value="{{ $booking_details[0]->vendor_id }}" class="form-control">
                                    <input type="hidden" name="order_id" value="{{ $booking_details[0]->bid }}" class="form-control">
                                    <input type="hidden" name="language" value="{{$language}}" class="form-control">

                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label> @lang('messages.Comments') : </label>
                                    <textarea name="comments" required class="form-control comments" placeholder="@lang('messages.Comments')" rows="6" cols="50"> <?php echo old('comments'); ?></textarea>
                                    <span class="error"> @if ($errors->has('comments')){{ $errors->first('comments', ':message') }}@endif</span>
                                </div>
                            </div>                          
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="sign_bot_sub">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" title="@lang('messages.Cancel')">@lang('messages.Cancel')</button>
                                        <button type="submit" class="btn btn-default membership_submit" title="@lang('messages.Submit')">@lang('messages.Submit')</button>
                                        <div class="ajaxloading" style="display:none;">
                                            <div class="loader-coms">
                                                <div class="loder_gif">
                                                    <img src="<?php echo url('assets/front/'.Session::get("general")->theme.'/images/ajax-loader.gif'); ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {!!Form::close();!!}
                </div>
            </div>
        </div>
    </div>
</div>


<?php /*
<div class="modal fade model_for_signup membership_login" id="return_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="logo_popup">
                    <img src="<?php echo url('/assets/front/'.Session::get("general")->theme.'/images/'.strtolower(Session::get("general")->theme).'.png'); */ ?><?php /*" title="{{ Session::get('general')->site_name }}" alt="{{ Session::get('general')->site_name }}">
                </span>
            </div>
            <div class="modal-body">
                <div class="tab-content mb30">
                    <div class="sign_up_inner">
                        <h2>@lang('messages.Return order')<br><span class="bottom_border"></span></h2>
                        {!!Form::open(array('url' => 'retun-order', 'method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'return_order' ,'onsubmit'=>'return return_order()'));!!} 
                            <div class="membership_inner">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>@lang('messages.Return order') : </label>
                                        <div class="returen_order">
                                            <input type="hidden" name="order_id" value="{{ $vendor_info->order_id }}">
                                            <input type="hidden" name="vendor_name" value="{{ $vendor_info->vendor_name }}">
                                            {{ Form::select('return_reason', $return_reasons,null, ['class' => 'form-control cooprative_select select_dropdown js-example-disabled-results'])}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label> @lang('messages.Comments') : </label>
                                        <textarea name="comments" required class="form-control comments" placeholder="@lang('messages.Comments')" > <?php echo old('comments'); */?>
                                        <?php /* </textarea>
                                        <span class="error"> </span>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="sign_bot_sub">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal" title="@lang('messages.Cancel')">@lang('messages.Cancel')</button>
                                            <button type="submit" class="btn btn-default membership_submit" title="@lang('messages.Submit')">@lang('messages.Submit')</button>
                                            <div class="ajaxloading" style="display:none;">
                                                <div class="loader-coms">
                                                    <div class="loder_gif">
                                                        <img src="<?php echo url('assets/front/'.Session::get("general")->theme.'/images/ajax-loader.gif'); */ ?><?php /* " />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {!!Form::close();!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


*/  ?>
<script type="text/javascript">
    //  $('select').select2();
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
    function rating()
    {
        $( '#success_message_signup' ).show().html("");
        $(".membership_submit").hide();
        $(".ajaxloading").show();
        data = $("#rating").serializeArray();
        var c_url = '/rating';
        token = $('input[name=_token]').val();
        $.ajax({
            url: c_url,
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            type: 'POST',
            datatype: 'JSON',
            success: function (resp)
            {
                $(".ajaxloading").hide();
                data = resp;
                console.log(data.httpCode);
                if(data.httpCode == 200)
                {
                    toastr.success(data.Message);
                    $('#myModal3').modal('hide');
                    $('.comments').val('');
                    $('.title').val('');
                    $('.ajaxloading').hide();
                    $('.reviewblock').hide();
                    $('.membership_submit').show();
                    //location.reload(true);
                    return false;
                }
                else
                {
                    toastr.warning(data.Message);
                    $('.ajaxloading').hide();
                    $('.membership_submit').show();
                    return false;
                }
            }, 
            error:function(resp)
            {
            }
        });
        return false;
    }
    $(document).ready(function()
    {
        $('[data-dismiss="modal"]').click(function(e) {
            $('.comments').val('');
            $('.title').val('');
            $('.rating_value').val(1);
            $('.ajaxloading').hide();
            $('.membership_submit').show();
        });
        $(".return_order").on("click",function()
        {
            $('#return_model').modal('show');
        });
        
        $('.order-step').on("mouseover",function()
        {
            $('.granular-info-box').hide();
            $('.arrow').hide();
            var step = $(this).data("row");
            $('.steps'+step).show();
            $('.arrow'+step).show();
            
        });

/*

        $(".reorder-btn").on("click",function()
        {
             event.preventDefault();
            
         
            if(cart_update==0){

            // confirm("Appoinment cannot be booked from multiple saloons at same time.Proceeding will empty the cart and add new services!!!"); 

            // }
            var r = confirm("Appoinment cannot be booked from multiple saloons at same time.Proceeding will empty the cart and add new services!!!");
            if (r == true) {
                 window.location = $(this).attr('href');
                return true;
            } else {
               
                return false;
            }

            }else{
                 window.location = $(this).attr('href');
            return true;

            } 

        });

*/
      //  $('.reorder-btn').

    });
    
    function return_order()
    {
        $(".ajaxloading").show();
        data = $("#return_order").serializeArray();
        var c_url = '/return_order';
        token = $('input[name=_token]').val();
        $.ajax({
            url: c_url,
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            type: 'POST',
            datatype: 'JSON',
            success: function (resp)
            {
                $(".ajaxloading").hide();
                data = resp;
                console.log(data.httpCode);
                if(data.httpCode == 200)
                {
                    toastr.success(data.Message);
                    $('#return_model').modal('hide');
                    $('.ajaxloading').hide();
                    location.reload(true);
                    return false;
                }
                else
                {
                    toastr.warning(data.Message);
                    $('.ajaxloading').hide();
                    $('.membership_submit').show();
                    return false;
                }
            }, 
            error:function(resp)
            {
            }
        });
        return false;
    }


</script>
@endsection
