    @extends('layouts.front')
	@section('content')
	    
        <section class="store_item_list">
            <div class="container">
            <div class="cms_pages">
            <div class="cms_titl">
            <h1>{{ ucfirst($cmsinfo[0]->title) }}</h1>
            </div>
          
            <?php echo $cmsinfo[0]->content; ?>
            </div>
            </div>
                
        </section>
        <script type="text/javascript">
            $('.listing_header').show();
        </script>
     <!-- content end -->
     @endsection
