@extends('layouts.front')
@section('content')
<script src="{{url('assets/front/'.Session::get('general')->theme.'/plugins/rateit/src/jquery.rateit.js')}}"></script>
<link href="{{url('assets/front/'.Session::get('general')->theme.'/plugins/rateit/src/rateit.css')}}" rel="stylesheet">

<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/moment.min.js') }}"></script>

<link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/daterangepicker.css') }}">
<section class="list_info_common">
    <div class="container">
        <div class="top_section">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/book-now')}}" title="@lang('messages.Home')">@lang('messages.Home')</a></li>
                            <li class="breadcrumb-item"><a href="{{ URL::to('list-hotels/city/'.$outlets[0]->city_url_index) }}" title="@lang('messages.City')">
                                {{ $outlets[0]->city_name }}
                                </a>
                            </li>
                            <li class="breadcrumb-item active">{{ucfirst($outlets[0]->outlet_name)}}</li>
                        </ol>
                    </nav>
                </div>
                @if (Session::has('reorder-message'))
                <?php /*
                    <div class="admin_sucess_common">
                        <div class="admin_sucess">
                            <div style="background: #f44066;color:black" class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('reorder-message') }}</div>
                        </div>
                    </div>
                */ ?>
                    <script type="text/javascript">
                        toastr.error('Rooms are Not Available for this Date!');
                    </script>
                @endif                
                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="hotel_address">
                        <h1>{{ isset($outlets[0]->outlet_name)?ucfirst($outlets[0]->outlet_name):"" }}</h1>
                        <p><i class="glyph-icon flaticon-facebook-placeholder-for-locate-places-on-maps"></i>
                            {{ isset($outlets[0]->contact_address)?$outlets[0]->contact_address:"" }}
                        </p>
                        <div class="icons">
                            <div class="rateit" data-rateit-mode="font" data-rateit-value="{{ number_format((round($avg_outlet_reviews)),1) }}" data-rateit-ispreset="true" data-rateit-readonly="true"> </div>
                            <?php /*
                                <i class="glyph-icon flaticon-favorite"></i>
                                <i class="glyph-icon flaticon-favorite"></i>
                                <i class="glyph-icon flaticon-favorite"></i>
                                <i class="glyph-icon flaticon-favorite"></i>
                                <i class="glyph-icon flaticon-favorite"></i>
                                */ ?>
                            <span> &nbsp; {{ number_format((round($avg_outlet_reviews)),1) }} </span>
                            <p>{{$total_rating}} reviews</p>
                        </div>
                    </div>
                    <input type="hidden" value="{{ $property_id }}" id="prop_id">
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="select_room">
                        <h2>
                            @if(getCurrencyPosition()->currency_side == 1)        
                            <label>{{ getCurrency()." ".$price->disc_price  }}</label>
                            @else
                            <label>{{ $price->disc_price." ".getCurrency() }}</label>
                            @endif
                            onwards
                        </h2>
                        <a href="#roomtypes" id="scroll_room" class="btn btn-primary btn-lg select_btn">Select room</button>
                        <a href="#hotelrules" id="scroll_hotel"><i class="glyph-icon flaticon-shopping-list"></i><span>&nbsp; READ HOTEL RULES</span></a>
                    </div>
                </div>
            </div>
        </div>
        <!--carousel_section-->
        <div class="carousel_section">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <?php 
                        if($property_id != ""){
              $destinationPath = base_path() .'/public/assets/admin/base/images/vendors/property/'.$property_id.'/';
              $fileurl = url('/assets/admin/base/images/vendors/property/'.$property_id.'/');
              $k = 0;
              foreach(glob($destinationPath.'*') as $filename)
              {
                if(basename($filename) !="thumb")
                {
                  if(!file_exists($fileurl.'/'.basename($filename)))
                  {
                    if (getimagesize($fileurl.'/'.basename($filename)) !== false) { ?>
                      <div class="carousel-item <?php echo ($k == 0)?"active":""; ?>">
                        <img class="d-block w-100" src="<?php echo $fileurl.'/'.basename($filename);?>" />
                      </div><?php 
                      $k++;
                    }
                  }
                }
              }?>
              @if($k == 0)
                <div class="carousel-item <?php echo ($k == 0)?"active":""; ?>">
                  <img class="d-block w-100" src="{{ URL::asset('assets/admin/base/images/vendors/property_image/'.$outlets[0]->outlet_image) }}" alt="">
                </div>
              @endif
            <?php  } ?>
          </div>
          @if($k > 0)
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="glyph-icon flaticon-arrows-1" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="glyph-icon flaticon-arrows-2" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          @else
          @endif
        </div>
      </div>
      <div class="property">
        <h4>Highlights of the property</h4>
        <p>{{ isset($outlets[0]->highlights)?$outlets[0]->highlights:"" }}</p>
      </div>
      <div class="roomtype search_fellid well configurator" id="roomtypes">
        <h2>Room type</h2>
        <div class="room_type_list">
          <div class="row">
            
      <div class="col-12 col-sm-8 col-md-8 col-lg-8">
      <div class="row">
      <div class="col-sm col-md col-lg col-xl">
              <div class="form-group check_in">
                <label for="startDate">Check-in:
                </label>
                <input type="text" class="form-control" id="startDate" value="{{  Session::get('start_date')  }}" onchange="roomsavailablitycheck()">
              </div>
            </div>
   <div class="col-sm col-md col-lg col-xl">
              <div class="form-group check_out">
                <label for="endDate">Check-out:</label>
                <?php  if((Session::get('end_date')) == "")?>
                  <input type="text" class="form-control" id="endDate" value="{{  Session::get('end_date')  }}" onchange="roomsavailablitycheck()">

                  <p id="cout" style="display: none;" value=""></p>
              </div>
            </div>
  <div class="col-sm col-md col-lg col-xl">
              <div class="duraction">
                <label for="startDate">Duration</label>
                <select class="js-example-disabled-results" id="duration" name="duration" required onchange="roomsavailablitycheck()">
                  <option value="">@lang('messages.Select Duration')</option>
                  @if (count(getDuration()) > 0)
                    <?php /* $d = Session::get('duration');  */ ?>
                    @foreach (getDuration() as $key => $type)
                      <option value="{{ $key }}" <?php /*  echo ($d==$key)?'selected="selected"':''; */ ?> ><?php echo $type; ?></option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div style="display: none;" id="dura">
                <label for="duration">Duration</label>
                <input type="text" id="dur" class="form-control" readonly="">
              </div>              
            </div>
      </div>
      </div>
      <div class="col-12 col-sm-4 col-md-4 col-lg-4">
      <div class="row">
      <div class="col-sm col-md col-lg col-xl">
              <label>Guests</label>
              <div class="gust_common">
                <div class="icon_bg">
                  <i class="glyph-icon flaticon-guest">
                  </i>
                </div>
                <div class="gust_qt">
                  <select class="js-example-disabled-results" id="adult_count" name="adult_count" required onchange="roomsavailablitycheck()">
                    @if (count(getGuestcount()) > 0)
                    <?php   $gc = Session::get('guest_count');  ?>
                    @foreach (getGuestcount() as $key => $type)
                    <option value="{{ $key }}" <?php echo ($gc==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                    @endforeach
                    @endif
                  </select>
                </div>
              </div>
            </div>
  <div class="col-sm col-md col-lg col-xl">
              <label>Rooms</label>
              <div class="gust_common">
                <div class="icon_bg">
                  <i class="glyph-icon flaticon-double-king-size-bed"></i>
                </div>
                <div class="gust_qt">
                  <select class="js-example-disabled-results" id="room_count" name="room_count" onchange="roomsavailablitycheck()">
                    <?php /* $room_c = min($rooc);
                    $min = min($rooc);
                    $max = max($rooc);//dd($max);
                    @if ($room_c <= 4)
                      <?php   $rc = Session::get('room_count');  
                      $less5 = getRoomscount($room_c);
                      @foreach ($less5 as $key => $type)
                        <option value="{{ $key }}" <?php echo ($rc==$key)?'selected="selected"':''; ><?php echo $type; </option>
                      @endforeach */ ?>                                    
                    @if (count(getRoomcount()) > 0)
                      <?php $rc = Session::get('room_count');  ?>
                      @foreach (getRoomcount() as $key => $type)
                        <option value="{{ $key }}" <?php echo ($rc==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                      @endforeach
                    @endif
                  </select>
                </div>
              </div>
            </div>
      
      </div>
      </div>
      
            
          </div>
        </div>
      </div>
      <input type="hidden" name="outlet_id" id="outlet_id" value="{{ $property_id }}">
      <div class="append_properties">
        @include('front.otel2go.view_roomtype')
      </div>
      <?php $r1 = count($room_type);
      $r2 = count($roomtypecount);  ?>
      <?php if($r1 < $r2) { ?>
        <div class="loade_more">
          <a href="#" title="" id="display_all_rooms">DISPLAY ALL ROOMS <i class="glyph-icon flaticon-down-arrow"></i></a>
          <a href="#" title="" style="display: none;" id="hide_all_rooms">DISPLAY ALL ROOMS<i class="glyph-icon flaticon-down-arrow"></i></a>
        </div>
      <?php }?>


      <div class="display informations_sec">
        <div class="row">
          <div class="col-12 col-sm-12 col-md-7 col-lg-7">
            <div class="list_infor_left">
              
<?php /*              
              <div class="row">
                <div class="col-md">
                  <p><i class="glyph-icon flaticon-people"></i>&nbsp;Swimming Pool</p>
                </div>
                <div class="col-md">
                  <p><i class="glyph-icon flaticon-technology-1"></i>&nbsp;Mini Fridge</p>
                </div>
                <div class="col-md">
                  <p><i class="glyph-icon flaticon-interface-1"></i>&nbsp;Banquet Hall</p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <p><i class="glyph-icon flaticon-people-1"></i>&nbsp;Gym</p>
                </div>
                <div class="col-md-4">
                  <p><i class="glyph-icon flaticon-business"></i>&nbsp;Conference Room</p>
                </div>
                <div class="col-md-4" id="amenities_extras" style="display: none;">
                  <p><i class="glyph-icon flaticon-video"></i>&nbsp;CCTV Cameras</p>
                </div>
              </div>
              <div class="row" id="amenities_extra" style="display: none;">
                <div class="col-md-4">
                  <p><i class="glyph-icon flaticon-transport"></i>&nbsp;Parking Facility </p>
                </div>
                <div class="col-md-4">
                  <p><i class="glyph-icon flaticon-technology"></i>&nbsp;Power backup</p>
                </div>
                <div class="col-md-4">
                  <p><i class="glyph-icon flaticon-connection"></i>&nbsp;Free Wifi </p>
                </div>
              </div>
*/  ?>         


<!--  GET ONLY SOME AMENITIES -->  


                <?php $amty = getsomeAmenities($property_id); ?>

                @if($amty)

                <h1>Amenities</h1>
                
                <div class="row" id="less_amenity">
                <table> 
                @if(count($amty) > 0)
                  <?php $i=1;   ?>
                @if($i%3 == 1)
                
                  <tr>
                  
                @endif
                @foreach($amty as $value)
                  
                    <td><div class="col-md"><p><i class="{{ $value->amenity_icon }}"></i>&nbsp;{{ $value->amenity_name }}</p></div></td>
                  
                  @if($i%3 == 0 || $i == count($amty))
                  </tr>
                  @endif
                  <?php $i++;   ?>
                @endforeach
                @endif
                </table>
                </div>


          <!--  GET ALL AMENITIES -->  

                <?php $amty = getAmenities($property_id); ?>
                <div class="row" id="all_amenity" style="display: none;">
                <table> 
                @if(count($amty) > 0)
                  <?php $i=1;   ?>
                @if($i%3 == 1)
                
                  <tr>
                  
                @endif
                @foreach($amty as $value)
                  
                    <td><div class="col-md"><p><i class="{{ $value->amenity_icon }}"></i>&nbsp;{{ $value->amenity_name }}</p></div></td>
                  
                  @if($i%3 == 0 || $i == count($amty))
                  </tr>
                  @endif
                  <?php $i++;   ?>
                @endforeach
                @endif
                </table>
                </div>
                
              <div class="add_more">
                <a href="#" class="amenties_more">+ More</a>
                <a href="#" class="amenties_less" style="display: none;">- Less</a>
              </div>

              <!--  -->

              @endif
              <div class="rules" id="hotelrules">
                <h1>Hotel Rules</h1>
                <ul class="list-inline">
                  <li>
                    <span>-</span>{{ isset($outlets[0]->hotel_rules)?$outlets[0]->hotel_rules:"" }}
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-5 col-lg-5">
            <div class="dist_calculator">
              <h1>Distance Calculator</h1>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroup-sizing-default"><i class="glyph-icon flaticon-facebook-placeholder-for-locate-places-on-maps"></i></span>
                </div>
                <input type="text" class="form-control distance" placeholder="Search nearby places from hotel" aria-label="Default" aria-describedby="inputGroup-sizing-default">
              </div>
              <h1>Places of Interest</h1>
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item col-sm-4 padding0">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Travel</a>
                </li>
                <li class="nav-item col-sm-4 padding0">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Restaurants</a>
                </li>
                <li class="nav-item col-sm-4 padding0">
                  <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Markets</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  @if($p1)
                    @foreach($p1 as $pl_key => $pl_val)
                      <div class="row">
                        <div class="col-sm-3 col-md col-lg">
                          <p>{{ $pl_val->place_name }}</p>
                        </div>
                        <div class="col-sm col-md col-lg">
                          <label>{{ $pl_val->distance }}</label>
                        </div>
                      </div>
                    @endforeach
                  @else
                    <p>No places found</p>
                  @endif
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  @if($p2)
                    @foreach($p2 as $pl_key => $pl_val)
                      <div class="row">
                        <div class="col-sm-3 col-md col-lg">
                          <p>{{ $pl_val->place_name }}</p>
                        </div>
                        <div class="col-sm col-md col-lg">
                          <?php if(is_numeric($pl_val->distance)) {
                            ?>
                          <label>{{ $pl_val->distance." KM" }}</label>
                        <?php } else { ?>
                          <label>{{ $pl_val->distance }}</label>
                        <?php } ?>
                        </div>
                      </div>
                    @endforeach
                  @else
                    <p>No places found</p>
                  @endif            
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                  @if($p3)
                    @foreach($p3 as $pl_key => $pl_val)
                      <div class="row">
                        <div class="col-sm-3 col-md col-lg">
                          <p>{{ $pl_val->place_name }}</p>
                        </div>
                        <div class="col-sm col-md col-lg">
                          <label>{{ $pl_val->distance }}</label>
                        </div>
                      </div>
                    @endforeach
                  @else
                    <p>No places found</p>
                  @endif 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="location">
        <h1>Location</h1>
        <div class="map-responsive col-lg">
          <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&amp;q=<?php echo isset($outlets[0]->contact_address)?$outlets[0]->contact_address:""; ?>"></iframe>
        </div>
      </div>
      <div class="ratings">
        <div class="row">
          <div class="col-12 col-sm-6 col-md-6 col-lg-6 padding_right0">
            <div class="over_all_rating">
              <h5>OVERALL RATINGS</h5>
              <div class="row">
                <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                  <b>{{ number_format(($avg_outlet_reviews),1) }}/5</b>
                  <div class="rateit" data-rateit-mode="font" data-rateit-value="{{$avg_outlet_reviews}}" data-rateit-ispreset="true" data-rateit-readonly="true"> 
                  </div>
                </div>
                <div class="col-12 col-sm-6 col-md-8 col-lg-8">
                  <div class="row">
                    <div class="col-md-1">
                      <h3>5</h3>
                    </div>
                    <div class="col-md">
                      <div class="progress">
                        <div class="progress-bar" role="   gressbar" style="width: {{($avg_ratings5)}}%" aria-valuenow="{{($avg_ratings5)}}" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                    <div class="col-md-5">
                      <h3>{{$count_ratings5}} Reviews</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1">
                      <h3>4</h3>
                    </div>
                    <div class="col-md">
                      <div class="progress">
                        <div class="progress-bar" role="   gressbar" style="width: {{($avg_ratings4) }}%" aria-valuenow="{{($avg_ratings4)}}" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                    <div class="col-md-5">
                      <h3>{{$count_ratings4}} Reviews</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1">
                      <h3>3</h3>
                    </div>
                    <div class="col-md">
                      <div class="progress">
                        <div class="progress-bar" role="   gressbar" style="width: {{($count_ratings3/5)*100}}%" aria-valuenow="{{($count_ratings3/5)*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                    <div class="col-md-5">
                      <h3>{{$count_ratings3}} Reviews</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1">
                      <h3>2</h3>
                    </div>
                    <div class="col-md">
                      <div class="progress">
                        <div class="progress-bar" role="   gressbar" style="width: {{($count_ratings2/5)*100}}%" aria-valuenow="{{($count_ratings2/5)*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                    <div class="col-md-5">
                      <h3>{{$count_ratings2}} Reviews</h3>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1">
                      <h3>1</h3>
                    </div>
                    <div class="col-md">
                      <div class="progress">
                        <div class="progress-bar" role="   gressbar" style="width: {{($count_ratings1/5)*100}}%" aria-valuenow="{{($count_ratings1/5)*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                    <div class="col-md-5">
                      <h3>{{$count_ratings1}} Reviews</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-6 col-md-6 col-lg-6 padding_left0">
            <div class="over_all_rating_right">
              <h5>AMENITIES RATINGS</h5>
              <div class="row">
                <div class="col-md-4">
                  <p> Location</p>
                </div>
                <div class="col-md-8">
                  <div class="col-md star">
                    <div class="rateit" data-rateit-mode="font" data-rateit-value="{{$avg_outlet_reviews_location}}" data-rateit-ispreset="true" data-rateit-readonly="true"> 
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <p> Comfort</p>
                </div>
                <div class="col-md-8">
                  <div class="col-md star">
                    <div class="rateit" data-rateit-mode="font" data-rateit-value="{{$avg_outlet_reviews_comfort}}" data-rateit-ispreset="true" data-rateit-readonly="true"> </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <p> Sleep Quality</p>
                </div>
                <div class="col-md-8">
                  <div class="col-md star">
                    <div class="rateit" data-rateit-mode="font" data-rateit-value="{{$avg_outlet_reviews_sleep_quality}}" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <p> Rooms</p>
                </div>
                <div class="col-md-8">
                  <div class="col-md star">
                    <div class="rateit" data-rateit-mode="font" data-rateit-value="{{$avg_outlet_reviews_rooms}}" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <p>Cleanliness</p>
                </div>
                <div class="col-md-8">
                  <div class="col-md star">
                    <div class="rateit" data-rateit-mode="font" data-rateit-value="{{$avg_outlet_reviews_cleanliness}}" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--  RATINGS DONE HERE--> 
<div class="pagination_section">
      <div class="append_pagination">
            @include('front.otel2go.view_pagination')
      </div>
      <div class="paginate"> {{ $reviews->render("pagination::bootstrap-4") }} </div>
</div>
    <!--  PAGINATION DONE HERE--> 

        <div class="description">
          <h5>Description</h5>
          <h4>Facilities</h4>
          <ul class="list-inline">
            <li>
              {{ isset($outlets[0]->facilities)?$outlets[0]->facilities:"" }}
            </li>
          </ul>
          <h4>What's Nearby</h4>
          <p>{{ isset($outlets[0]->near_by)?$outlets[0]->near_by:"" }}</p>
        </div>
        <div class="others">
          <h2 class="rec_otel">Other Recommended Otel2go</h2>
          <div class="row">
            @foreach($recommended_properties as $rec_key => $rec_val)  
              <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                <div class="all_items">
                  <a href="{{ URL::to('view-hotel/'.$rec_val->url_index) }}">
                    <div class="hotel">
                      <div class="proparty_img">
                        <?php $image = $rec_val->outlet_image;?>
                        <?php $file_path = url('/assets/admin/base/images/vendors/property_image/list/'.$image); ?>
                        @if (!empty($image) && File::exists(public_path('/assets/admin/base/images/vendors/property_image/list/'.$image)))
                          <img src="<?php echo $file_path;  ?>" alt="">
                        @else 
                          <img src="<?php echo url('assets/front/otel2go/images/no_hotel.jpg');  ?>">
                        @endif  
                      </div>
                      <div class="price_list">
                        <label>                
                          @if(getCurrencyPosition()->currency_side == 1)        
                            {{ getCurrency()." ".$rec_val->discount_price  }}
                          @else
                            {{ $rec_val->discount_price." ".getCurrency() }}
                          @endif
                        </label>
                        <del>
                          @if(getCurrencyPosition()->currency_side == 1)        
                            {{ getCurrency()." ".$rec_val->normal_price  }}
                          @else
                            {{ $rec_val->normal_price." ".getCurrency() }}
                          @endif
                        </del>
                        <b>{{ round(100 - ($rec_val->discount_price / $rec_val->normal_price) * 100)}}% OFF</b>
                      </div>
                    </div>
                  </a>
                  <div class="product_info">
                    <a href="{{ URL::to('view-hotel/'.$rec_val->url_index) }}">
                      <h5>{{ $rec_val->outlet_name }}</h5>
                      <p>{{ $rec_val->zone_name.", ".$rec_val->city_name }}</p>
                    </a>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  
    $(document).ready(function(){

    //  IF DURATION DATE CHANEGS MEANS END-DATE AUTOMATICALLY CALCUTATE

      $("#duration").on('change',function(){      

          
          var dur = $("#duration").val();

          //  alert(dur);

          if(dur == 0)
          {

            $('#endDate').daterangepicker({
              singleDatePicker: true,
              minDate: moment().add(1, 'days'),
              startDate: moment().add(1, 'days')
            });    
            $('#cout').hide();              
            $("#endDate" ).show();
            $("#dura").hide();
          }
          else
          {
         
            $('#endDate').daterangepicker({
              singleDatePicker: true,
              startDate: moment().add(dur, 'days')
            });     

            var ed = $("#endDate").val();       

            $("#endDate" ).hide();
            $("#dura").hide();
            $('#cout').text(ed).show();
            
          }
      });

          $("#endDate").on('change',function(){

            //  alert('Akr');
/*
            var From_date = new Date($("#startDate").val());
            var To_date = new Date($("#endDate").val());
            var diff_date =  To_date - From_date;

            var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);
*/

            var From_date = $("#startDate").val();

            //  alert(From_date);
            var To_date = $("#endDate").val();
            //  alert(To_date);

            var a = moment(From_date, "MM/DD/YYYY");
            var b = moment(To_date, "MM/DD/YYYY");

            //  alert(a);alert(b);

            var days = b.diff(a,'days');

            //  alert(days);
            var dur = $("#duration").val();

            //  alert(dur);

            
            $("#dur").val(days+" Days");

            if(days > 1 && dur == 0)
            {
                $(".duraction").hide();
                $("#dura").show();
            }          
            else
            {
                $(".duraction").show();
                $("#dura").hide();  
            }
          });

    });

</script>

<script>
$(document).ready(function(){
 $("#scroll_hotel").on('click', function(event) {
   if (this.hash !== "") {
     event.preventDefault();
     var hash = this.hash;
     $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 800, function(){
       window.location.hash = hash;
     });
   }
 });


 $("#scroll_room").on('click', function(event) {
   if (this.hash !== "") {
     event.preventDefault();
     var hash = this.hash;
     $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 800, function(){
       window.location.hash = hash;
     });
   }
 }); 
});


</script>


<script type="text/javascript" src="{{ URL::asset('assets/front/otel2go/js/moment.js') }}">  </script>  
<script type="text/javascript" src="{{ URL::asset('assets/front/otel2go/js/daterangepicker.js') }}">  </script>  
<script>
    $(window).load(function() {
    
       $('select').select2();
    
    
       $('#startDate').daterangepicker({
         singleDatePicker: true,
         //  startDate: moment()
         minDate: new Date(),
       });
       $('#endDate').daterangepicker({
         singleDatePicker: true,
         minDate: moment().add(1, 'days')
           //  startDate: moment().add(1, 'days')
       });
    
       //  sdChange();
    
    });
    $('.listing_header').show();

    var html ="";

    html += '<p class="page-link">»</p>';


    $('.pagination li:last').html('');
    $(".pagination li:last").append(html);
    
</script>
<script type="text/javascript">
    $(document).ready(function(){
    
        var maxLength = 1;
        var showHideBtns = '<li><span class="show-button"><i class="glyphicon glyphicon-plus"></i> + More</span><span class="show-button" style="display:none"><i class="glyphicon glyphicon-minus"></i>  - Less</span></li>';      
    
        $('ul.a').each(function() {
          if($(this).children('li').length > maxLength) {
            $(this).children('li:gt(' + (maxLength - 1) + ')').addClass('toggleable').hide();
            $(this).append(showHideBtns);
          }
        }); 
    
        $('ul.b').each(function() {
          if($(this).children('li').length > maxLength) {
            $(this).children('li:gt(' + (maxLength - 1) + ')').addClass('toggleable').hide();
            
          }
        });
    
        $('ul.c').each(function() {
          if($(this).children('li').length > maxLength) {
            $(this).children('li:gt(' + (maxLength - 1) + ')').addClass('toggleable').hide();
            
          }
        });                 
    
        $('.show-button').click(function() {
    
            $(this).closest('div#ak').find('li').show();
            //  $(this).closest('div#ak').find('li').hide();
            $(this).parent('li').children('.show-button').toggle();
            $(this).parent('ul.a').siblings('.toggleable').toggle();
            
        });
    
    });
</script>
<script type="text/javascript">
    $(".amenties_more").on('click',function(){
        //  alert('Ak');      
        event.preventDefault();
    
        $("#all_amenity").show();
        $(this).hide();
        $(".amenties_less").show();
        $("#less_amenity").hide();
        
    });
  
    
    $(".amenties_less").on('click',function(){
        //  alert('Ak');      
        event.preventDefault();
        $("#all_amenity").hide();
        $(this).hide();
        $(".amenties_more").show();
        $("#less_amenity").show();
        
    });  
    
    $("#display_all_rooms").on('click',function(){
        //  alert('Ak');
    
        var btn_val = 1;
        event.preventDefault();
        roomshow(btn_val);
    });
    
    $("#hide_all_rooms").on('click',function(){
        //  alert('Ak');
    
        var btn_val = 2;
    
        //  alert(btn_val);
        event.preventDefault();
        roomshow(btn_val);
    });
    
    // $("#startDate").on('change',function(){
    
    //     //  alert('Ak');
    
    //     roomsavailablitycheck();
    
    // });
    
    // $("#endDate").on('change',function(){
    
    //     //  alert('Ak');
    //     roomsavailablitycheck();
    
    // });
    
    // $("#adult_count").on('change',function(){
    
    //     //  alert('Ak');
    //     roomsavailablitycheck();
    
    // });
    
    // $("#room_count").on('change',function(){
    
    //     //  alert('Ak');
    //     roomsavailablitycheck();
    // });
    
      function roomshow(btn_val)
        {
            //  alert(btn_val);
    
          
          var outlet_id,sd,ed,room_count,adult_count,c_url;
    
          token = $("input[name=_token]").val();
          c_url = '/roomshow';
    
          outlet_id = $("#outlet_id").val();
          sd = $("#startDate").val();
          ed = $("#endDate").val();
          room_count = $("#room_count").val();
          adult_count = $("#adult_count").val();        
    
          //  alert(outlet_id);
    
          data = {sd:sd,ed:ed,outlet_id:outlet_id,room_count:room_count,adult_count:adult_count,btn_val:btn_val};
    
          $.ajax({
    
              url : c_url,
              headers: {'X-CSRF-TOKEN': token},
              data: data,
              type: 'POST',
              datatype: 'JSON',
              success: function (resp) {
                    $('.append_properties').html(resp);
    
                        if(btn_val == 1)
                        {
                            $('#display_all_rooms').hide();
                            $('#hide_all_rooms').show();
                        }
                        else if(btn_val == 2)
                        {
                            $('#display_all_rooms').show();
                            $('#hide_all_rooms').hide();                    
                        }
    
              }
          });
                        
      }
    
      function roomsavailablitycheck()
      {
            //  alert('Kumar');
          var outlet_id,sd,ed,room_count,adult_count,c_url;
    
          token = $("input[name=_token]").val();
          c_url = '/roomsavailablitycheck';
    
          outlet_id = $("#outlet_id").val();
          sd = $("#startDate").val();
          ed = $("#endDate").val();
          room_count = $("#room_count").val();
          adult_count = $("#adult_count").val(); 
    
          data = {sd:sd,ed:ed,room_count:room_count,adult_count:adult_count,outlet_id:outlet_id};
    
          $.ajax({
    
              url : c_url,
              headers: {'X-CSRF-TOKEN': token},
              data: data,
              type: 'POST',
              datatype: 'JSON',
              success: function (resp) {
                   $('.append_properties').html(resp);
              }
          });              
                        
      } 
    
    /*
    
    
      function sdChange()
        {
            //  alert('Ak');
    
          var sd,ed,duration,room_count,adult_count;
    
    
          token = $("input[name=_token]").val();
          url = '{{   url('days_check')    }}';
    
          sd = $("#startDate").val();
          ed = $("#endDate").val();
          duration = $("#duration").val();
          room_count = $("#room_count").val();
          adult_count = $("#adult_count").val();
    
          data = {sd:sd,ed:ed,duration:duration,room_count:room_count,adult_count:adult_count};
    
          $.ajax({
    
              url : url,
              headers: {'X-CSRF-TOKEN': token},
              data: data,
              type: 'POST',
              datatype: 'JSON',
              success: function (resp) {
    
                              if(resp.data!='')
                              {
                                  //  toastr.error('success');
    
                                  //  location.reload();
    
                                  $("#duration").val({{  Session::get('duration') }});
                              }
                              else
                              {
                                  //  toastr.error('No data');
                              }
                          }
          });               
        }  
    */

<?php /*    
          $('.pagination li a').click( function(e) {
          e.preventDefault();
    
          var str_url = $(this).attr('href');
          $(this).off("click").attr('href', "javascript: void(0);");
          var split_url = str_url.split('=');
          var page = split_url[1];
          $('#load a').css('color', '#dfecf6');
          $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');
    
          var url = '{{ url('view-hotel/review_comments') }}';
          var outlet_id = $('#prop_id').val();
          token = $('input[name=_token]').val();
          // alert(outlet_id);
          $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            data: { outlet_id:outlet_id,page_id:page },
            type:'POST',
            datatype: 'JSON',
            success: function (resp) {
              data = resp
                  if(data.httpCode == 200)
                  {
    
            // console.log(data.reviews.data)
            for(let count = 0 ; count < data.reviews.data.length ; count++)
            {
              // console.log( data.reviews.data[count].created_date);
                 var html = ''           // $('.active').show();
                 html += '<ul>'
                 html += '<li>' 
                 html += '<div class="row">'
                 html += '<div class="col-md-1">'
                 html += '<span>'+data.reviews.data[count].ratings+'</span>'
                 html += '</div>'
                 html += '<div class="col-md">'
                 html += '<h4> '+data.reviews.data[count].firstname+' </h4>'
                 html += '<small> '+ moment(data.reviews.data[count].created_date).format('MMM DD,YYYY') +'</small>'
                 html += '<p>' + data.reviews.data[count].title + '</p>'
                 html += '</div>'
                 html += '</div>'
                 html += '</li>'
                 html += '</ul>'
                  $(".pag_section_comon").empty();
                  $(".pag_section_comon").append(html); 
                  // $('.pag_section_comon').removeclass('active);
                  // $(this).addclass('active);
            }
                  // console.log(this)
                      //  toastr.success(data.Message);
                      return true;
                  }
                  else
                  {
                      toastr.warning(data.Message);
                      return false;
                  }
    
              }, 
    
          });
        });

*/ ?>      

// $('.pagination li a:last').click( function(e) {


//     //  alert('Akfht');
//           e.preventDefault();

//            var page = $(".pagination li.page-item.active").text();

//            alert(page);

//            page = parseInt(page)+1;


//           alert(page);

//           $(".page-item").removeClass('active');

//            $("li:nth-child("+page+")").addClass('active');
//           //  $(".page-item").removeClass('disabled');

//           // $(this).parent(".pagination li").addClass('active');
//           // $(this).parent(".pagination li").addClass('disabled');

          



      

//           $(".page-item").removeClass('disabled');
    
//           var str_url = $(this).attr('href');
//           //  $(this).off("click").attr('href', "javascript: void(0);");
//           var split_url = str_url.split('=');
//           var page = split_url[1];

//           //  alert(page);
//           // $('#load a').css('color', '#dfecf6');
//           // $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');
    
//           var url = '{{ url('view-hotel/review_comments') }}';
//           var outlet_id = $('#prop_id').val();
//           token = $('input[name=_token]').val();
//           // alert(outlet_id);
//           $.ajax({
//             url: url,
//             headers: {'X-CSRF-TOKEN': token},
//             data: { outlet_id:outlet_id,page_id:page },
//             type:'POST',
//             datatype: 'JSON',
//             success: function (resp) {
//             data = resp

//                 $('.append_pagination').html(resp);   

//               }, 
    
//           });
// });   

$('.pagination li a').click( function(e) {


  //  alert('Ak');
          e.preventDefault();

          //  var page = $(".pagination li.page-item.active").text();

           // alert(page);

           // $("li:last").addClass('disabled');

           var page = $(".pagination li.page-item.active").text();

            //  alert(page);

           var nextpage =  $(this).html();
           var last_bef = $(".pagination li:last").prev("li").text();

           alert(nextpage);alert(last_bef);

           if(nextpage = last_bef)
           {
              //  alert('aj');
              $(".pagination li:last").addClass('disabled'); 
           }            

           if(page < nextpage)
           {
              nextpage = parseInt(nextpage)+1;
           }
           else if(page > nextpage)
           {
              nextpage = parseInt(nextpage)+1;
           }

           // alert(page);

           



          $(".page-item").removeClass('active');
          //  $(".page-item").removeClass('disabled');

          //  var last = $(".pagination li:last a").html();

          //  alert(last);

          $("li:nth-child("+nextpage+")").addClass('active'); 

          $(".pagination li:last-child").addClass('disabled');    



          //  var page =  $(".page-item").('active').html();


          $(".page-item").removeClass('disabled');
    
          var str_url = $(this).attr('href');

          //  alert(str_url);
          //  $(this).off("click").attr('href', "javascript: void(0);");
          var split_url = str_url.split('=');
          var page = split_url[1];

          //  alert(page);
          // $('#load a').css('color', '#dfecf6');
          // $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');
    
          var url = '{{ url('view-hotel/review_comments') }}';
          var outlet_id = $('#prop_id').val();
          token = $('input[name=_token]').val();
          // alert(outlet_id);
          $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            data: { outlet_id:outlet_id,page_id:page },
            type:'POST',
            datatype: 'JSON',
            success: function (resp) {
            data = resp

                $('.append_pagination').html(resp);   

              }, 
    
          });
});  


$('.pagination li span').click( function(e) {

    //  alert('Ak');
          e.preventDefault();

           var page = $(".pagination li.page-item.active").text();

             alert(page);

           var nextpage =  $(this).html();

            //  alert(nextpage);

           if(page < nextpage)
           {
              nextpage = parseInt(nextpage)+1;
           }
           else if(page > nextpage)
           {
              nextpage = parseInt(nextpage)+1;
           }           


          $(".page-item").removeClass('active');
          $(".page-item").removeClass('disabled');

          //  $(this).parent(".pagination li").addClass('active');
          //  $(this).parent(".pagination li").addClass('disabled');


          if(page == 2 || page == 1)
          {
              $(".pagination li:first-child").addClass('disabled');
              //  $(".pagination li:first-child").removeClass('active');
              $("li:nth-child(2)").addClass('active');
          }
          else
          {
              $("li:nth-child("+nextpage+")").addClass('active');  
          }

          // if(nextpage == 3)
          // {
          //   $("li:last").addClass('disabled');
          // }


          
          //  $(".pagination li.page-item.a::eq(1)").addClass('active');
          

    
          // var str_url = $(this).attr('href');
          // //  $(this).off("click").attr('href', "javascript: void(0);");
          // var split_url = str_url.split('=');
          // var page = split_url[1];

          // alert(page);

          var page =  $(this).html();

          //  alert(page);
          // $('#load a').css('color', '#dfecf6');
          // $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');
    
          var url = '{{ url('view-hotel/review_comments') }}';
          var outlet_id = $('#prop_id').val();
          token = $('input[name=_token]').val();
          // alert(outlet_id);
          $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            data: { outlet_id:outlet_id,page_id:page },
            type:'POST',
            datatype: 'JSON',
            success: function (resp) {
            data = resp

                $('.append_pagination').html(resp);   

              }, 
    
          });
});

$('.pagination li p').click( function(e) {

    //  alert('Ak');
          e.preventDefault();

           var page = $(".pagination li.page-item.active").text();

             // alert(page);

            page = parseInt(page) + 1;

            //  alert(page);


      

          $(".page-item").removeClass('active');
          $(".page-item").removeClass('disabled');

          //  $(this).parent(".pagination li").addClass('active');
          //  $(this).parent(".pagination li").addClass('disabled');

          var nextpage = parseInt(page) + 1;

          //  alert(nextpage);

              $("li:nth-child("+nextpage+")").addClass('active');                      
          

          
          //  $(".pagination li.page-item.a::eq(1)").addClass('active');
          

          var page =  $(this).html();

    
          var url = '{{ url('view-hotel/review_comments') }}';
          var outlet_id = $('#prop_id').val();
          token = $('input[name=_token]').val();
          // alert(outlet_id);
          $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            data: { outlet_id:outlet_id,page_id:page },
            type:'POST',
            datatype: 'JSON',
            success: function (resp) {
            data = resp

                $('.append_pagination').html(resp);

              }, 
    
          });
         
});  


    
</script>
@endsection
