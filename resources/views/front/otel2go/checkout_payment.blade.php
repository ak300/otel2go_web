@extends('layouts.front')
@section('content')

<script src="{{ URL::asset('/assets/admin/base/js/toastr.min.js') }}"></script> 

<link href="{{ URL::asset('/assets/admin/base/css/toastr.css') }}" media="all" rel="stylesheet" type="text/css" />

    <section class="check_out_payment_bg">
        <div class="container">
            <div class="payhotel">
                <div class="row">
                    <div class="col-md">
                        <div class="check_out_right_sec">
                            <div class="hotel_detail">
                                <div class="row">
                                    <div class="col-md-2">
                                         <img class="hotel_image" src="{{ URL::asset('assets/admin/base/images/vendors/property_image/list/'.$properties->outlet_image) }}" alt="">
                                    </div>
                                    <div class="col">
                                        <h1>{{  $properties->outlet_name  }}</h1>
                                        <p><i class="glyph-icon flaticon-facebook-placeholder-for-locate-places-on-maps"></i>{{ $properties->contact_address }}
                                        </p>
                                    </div>
                                </div>
                                <hr>
                            </div>
                                <div class="check_inout">
                                <?php
                                    $sd = date("d M Y",strtotime($data['check_in']));
                                    $ed = date("d M Y",strtotime($data['check_out']));
                                ?>
								
								<div class="payment_info_sections">
								<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-12">
								 <label>Check in :  <p>Date & Time : {{ $sd }} | 08.00 PM</p></label>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
								  <label>Check out :  <p>Date & Time : {{ $ed }} | 08.00 PM</p></label>
								</div>
								</div>
								</div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-8">
                                        <label>{{ "0".Session::get('room_count') }} Room, {{ "0".Session::get('room_count') }} Guest, {{ $data['no_of_nights'] }} Night</label>
                                        <p><label>Room Type :</label><span>{{ $room_type->room_type  }}</span></p>
                                    </div>
                                    <div class="col edit_btn">
                                         <a href="{{ URL::to('view-hotel/'.$properties->url_index) }}" class="btn btn-primary btn-lg book">Edit</a>
                                    </div>
                                </div>
                                <hr>
                                <div class="payment_details">
                                    <h1>Payment details</h1>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p>Booking Amount</p>
                                        </div>
                                        <div class="col-md-5"><span>
                                            @if(getCurrencyPosition()->currency_side == 1)
                                                {{ getCurrency()." ".$room_type->discount_price  }}       
                                            @else 
                                                {{ $room_type->discount_price." ".getCurrency()  }}
                                            @endif                                            
                                            x {{ Session::get('room_count') }} Room x {{ $data['no_of_nights'] }} Nights</span></div>
                                        <div class="col">
                                            <p class="text-align-right">
                                                    @if(getCurrencyPosition()->currency_side == 1)
                                                        {{ getCurrency()." ".$data['total_cost']  }}       
                                                    @else 
                                                        {{ $data['total_cost']." ".getCurrency()  }}
                                                    @endif
                                            </p>
                                        </div>
                                    </div>
                                    @if($data['coupon_amount'] > 0)
                                        <div class="row">
                                            <div class="col-md-4">
                                                <p>Coupon Discount</p>
                                            </div>
                                            <div class="col-md-5">
                                                <p></p>
                                            </div>
                                            <div class="col-md">
                                                <p class="text-align-right">
                                                    @if(getCurrencyPosition()->currency_side == 1)
                                                        {{ getCurrency()." ".$data['coupon_amount']  }}       
                                                    @else 
                                                        {{ $data['coupon_amount']." ".getCurrency()  }}
                                                    @endif                                                    
                                                </p>
                                            </div>
                                        </div>
                                    @endif
                                    <hr>
                                    <div class="row">
                                        <div class="col">
                                            <h2> Payable Amount</h2>
                                            <p>(Inclusive of taxes)</p>
                                        </div>
                                        <div class="col label_right">
                                            <label>
                                                    @if(getCurrencyPosition()->currency_side == 1)
                                                        {{ getCurrency()." ".$data['total_cost']  }}       
                                                    @else 
                                                        {{ $data['total_cost']." ".getCurrency()  }}
                                                    @endif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
<?php 


            $response_url = env("APP_URL")."/api/booking_payment_response";
            $backend_url = env('APP_URL').'/api/response_booking_payment';
            //  dd($currency);
?>                            
                            <div class="edit_btn">
                                    {!!Form::open(array('url'=> 'https://www.mobile88.com/ePayment/entry.asp','method' => 'post','class'=>'tab-form attribute_form','id'=>'booking_payment','name'=>'booking_payment','files' => true));!!}               
                                       <input type="hidden" name="MerchantCode" value="{{ $merchant_code }}">
                                       <input type="hidden" name="RefNo" value="{{ $reference_no }}">
                                       <input type="hidden" name="Amount" value="{{ $amount }}">
                                       <input type="hidden" name="Currency" value="{{ $currency }}">
                                       <input type="hidden" name="Lang" value="UTF-8">
                                       <input type="hidden" name="SignatureType" value="SHA256">
                                       <input type="hidden" name="Signature" value="{{ $signature }}">
                                       <input type="hidden" name="ProdDesc" value="{{ $payment_title }}">
                                       <input type="hidden" name="UserName" value="{{ $user_name }}">
                                       <input type="hidden" name="UserEmail" value="{{ $user_email }}">
                                       <input type="hidden" name="UserContact" value="{{ $user_mobile }}">
                                       <input type="hidden" name="Remark" value="{{ $user_id }}">
                                       <input type="hidden" name="ResponseURL" value="{{ $response_url }}">
                                       <input type="hidden" name="BackendURL" value="{{ $backend_url }}">
                                       <button type="submit" class="btn paynow_btn">Pay now</button>

                                    {!!Form::close();!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="space"></div>
    </section>

    <script>
      $('.listing_header').show();
    </script>
    <script src="{{ URL::asset('assets/front/otel2go/js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('assets/front/otel2go/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/front/otel2go/js/intlTelInput.min.js') }}"></script>
 
<script type="text/javascript">
  
<?php /*  

    $(document).ready(function(){
        $('#booking_payment').on('submit',function(){
            
            event.preventDefault();
            var token,url,data;

            token = $('input[name=_token]').val();
            url = '{{   url('payment_store')    }}';
            data = $("#booking_payment").serialize();

            $.ajax({
                url:url,
                data: data,
                type: 'POST',
                datatype: 'JSON',

                success:function(resp){

                    if(resp == 1)
                    {
                        toastr.success('Payment Inserted Successfully');
                        //  $("#modal_id").modal('hide');
                        //  $('#payment_form')[0].reset();
                        //  location.reload();
                    }
                    else if(resp == -1)
                    {
                        toastr.error('Failed to Insert');
                    }
                }

            });


        });
    });

*/ ?>     

</script>    
@endsection