@extends('layouts.front')
@section('content')

    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
<section>
      <div class="home_listing">
        <div class="container">
          <div class="container_common">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <nav aria-label="breadcrumb">
                      <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="{{url('/book-now')}}" title="@lang('messages.Home')">@lang('messages.Home')</a></li> 
                          <li class="breadcrumb-item active">
                                  @lang('messages.Popular Hotels')
                          </li>
                      </ol>
                   </nav> 
                </div>              
              <div class="col-12 col-sm-8 col-md-8 col-lg-8 search_info">
                
                <h2>Popular Hotels in Malaysia</h2>
                
                <label>

                  <?php

                          $st = date("M d,Y", strtotime($sd));
                          $ed = date("M d,Y", strtotime($ed));
                  
                   ?>
                   
                 {{  $st.' - '.$ed }}</label>
              </div>
              <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                <div class="search_info">
                  <a href="{{  url('/book-now')  }}" class="btn btn-primary" style="color: white;">Change search
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
           <?php /*  <ol class="breadcrumb">
                 <li><a href="{{url('/')}}" title="@lang('messages.Home')">@lang('messages.Home')</a></li> >
                <?php  if($category_index == 'city') { 
                    <li><a href="{{url('/')}}" title="@lang('messages.City')">
                    {{ $cities[0]->city_name }}
                  </a></li> >  
                <?php   
                <?php if($category_index == 'category') { 
                   <li><a href="{{url('/')}}" title="@lang('messages.City')">
                                {{ $properties[0]->category_name }}
                  </a></li> > 
                  <?php } 
                <li><a href="{{url('/')}}" title="@lang('messages.Edit')">Edit</li>
       </ol>*/ ?> 
    </section>
    <section>
      <div class="home_listing">
        <div class="container">
          <div class="listing_common">
            <div class="row">
			 <div class="respnsive_cat">
			  <a id="click_cat" href="#">Click to refine your findings</a>
			  </div>
              <div class="col col-sm-4 col-lg-4 cat_100" id="open_cat">
                <div class="aside">
                  <div class="sort_by">
                    <h3 class="aside_title">Sort by
                    </h3>
                    <ul>
                      <li>
                        <label class="radio_button">Popularity
                          <input type="radio" checked="checked" name="radio_price" id="radio_popular" value="1">
                          <span class="checkmark">
                          </span>
                        </label>
                      </li>
                      <li>
                        <label class="radio_button">Distance
                          <input type="radio" name="radio_price" id="radio_dist" value="2">
                          <span class="checkmark">
                          </span>
                        </label>
                      </li>

                      <input type="hidden" name="user_latitude" id="user_lat" value="">
                      <input type="hidden" name="user_longitude" id="user_lng" value="">

                      <li>
                        <label class="radio_button">Price
                          <input type="radio" name="radio_price" id="radio_price" value="3">
                          <span class="checkmark">
                          </span>
                        </label>
                      </li>
                      </li>
                    <li>
                      <label class="radio_button">Rating
                          <input type="radio" name="radio_price" id="radio_rating" value="4">
                        <span class="checkmark">
                        </span>
                      </label>
                    </li>
                    </ul>
                </div>

                <div class="popular_by">
                  <h3 class="aside_title">Popular Locations in Malaysia
                  </h3>
                  <ul class="catloc">
                    <li>
                      <label class="checkmark_button">All of Malaysia
                        <input type="checkbox" id="check-cities">
                        <span class="checkmark">
                        </span>
                      </label>
                    </li>
                    @foreach($city_details as $city_key => $city_val)
                      <li>
                        <label class="checkmark_button" value="">{{ $city_val->city_name}}
                          <input type="checkbox" id="cities" name="cities[]" class="select-filter cities" value="{{ $city_val->id }}">
                          <span class="checkmark">
                          </span>
                        </label>
                      </li>
                    @endforeach
                  </ul>
                </div>             
                
                <div class="popular_by">
                  <h3 class="aside_title">Price
                  </h3>
                  <div class="slider">
                  </div>
                </div>

                <input type="hidden" id="slid">

                 <div class="popular_by">
                  <h3 class="aside_title">Star Rating
                  </h3>
                  <ul>
                    <?php $rate = getRateFilters(); ?>
                    @foreach($rate as $key => $type)
                    <li>
                      <label class="checkmark_button">{!! $type !!}
                        <input type="checkbox" id="rating" name="rating[]" class="select-filter rating" value="{!! $key !!}">
                        <span class="checkmark">
                        </span>
                      </label>
                    </li>
                    @endforeach  
                  </ul>
                </div>
                <div class="popular_by">
                  <h3 class="aside_title">Category
                  </h3>
                  <ul>
                    <li>
                      <label class="checkmark_button">All
                        <input type="checkbox" id="check-pricing">
                        <span class="checkmark">
                        </span>
                      </label>
                    </li>
                    @foreach($pricing as $pr_key => $pr_val)
                    <li>
                      <label class="checkmark_button">{!! $pr_val->category_name !!}
                        <input type="checkbox" id="pricing" name="pricing[]" class="select-filter pricing" value="{!! $pr_val->id !!}">
                        <span class="checkmark">
                        </span>
                      </label>
                    </li>
                    @endforeach

                  </ul>
                </div>
                <?php /* echo "<pre>";print_r($accomodation); */ ?>
                <div class="popular_by">
                  <h3 class="aside_title">Accommodation Types
                  </h3>
                  <ul>
                     <li>
                      <label class="checkmark_button">All
                        <input type="checkbox" id="check-accomodation">
                        <span class="checkmark">
                        </span>
                      </label>
                    </li>
                    @foreach($accomodation as $ac_key => $ac_val)
                    <li>
                      <label class="checkmark_button">{!! $ac_val->accomodation_type_name !!}
                        <input type="checkbox" id="accomodation" name="accomodation[]" class="select-filter accomodation" value="{!! $ac_val->id !!}">
                        <span class="checkmark">
                        </span>
                      </label>
                    </li>
                    @endforeach
                  </ul>
                </div>
                <div class="popular_by">
                  <h3 class="aside_title">Amenities
                  </h3>
                  <ul class="allcats">
                    <li>
                      <label class="checkmark_button">All
                        <input type="checkbox" id="check-amenities" value="">
                        <span class="checkmark">
                        </span>
                      </label>
                    </li>
                    @foreach($amenities as $am_key => $am_val)
                      <li>
                        <label class="checkmark_button">{!! $am_val->amenity_name !!}
                          <input type="checkbox" id="amenities" name="amenities[]" class="select-filter amenities" value="{!! $am_val->id !!}">
                          <span class="checkmark">
                          </span>
                        </label>
                      </li>
                   @endforeach
                  </ul>
                </div>
              </div>
            </div>
            <div class="col col-sm-8 col-lg-8 width_100">
              <div class="aside_right">
                <ul class="tabs">
                  <li class="tab-link col col-sm-3 col-lg-3 current click-pricing add-current" data-tab="tab-1" value="0">
                     All 
                  </li>
                  @foreach($pricing as $pr_key => $pr_val)
                      <li class="tab-link col col-sm-3 col-lg-3 click-pricing" data-tab="tab-1" value="{{ $pr_val->id }}">
                          {!! $pr_val->category_name !!}
                      </li> 
                  @endforeach
                </ul>
                <div class="tab_listing_section">
                  <div id="tab-1" class="tab-content current">
                    <div class="all_listing">
                      <div class="row append_properties">
                        @include('front.otel2go.list_ajax_property')
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="space">
        </div>
        
      </div>
      </div>
    </section>
    <script>
      $('.listing_header').show();
    </script>
     <!-- plus a jQuery UI theme, here I use "flick" -->
  <script>
    $(document).ready(function(){
 //responsive cat menu
   $("#click_cat").click(function(){
        $("#open_cat").slideToggle();
    });
 //
      $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');
        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');
        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
      });

      $("#click_menus").click(function(){
        $("#hiddle_menus").toggle();
      });

      var maxLength = 4;
      var showHideBtns = '<li><span class="show-button"><i class="glyphicon glyphicon-plus"></i> + More</span><span class="show-button" style="display:none"><i class="glyphicon glyphicon-minus"></i>  - Less</span></li>';      

      $('ul.allcats').each(function() {
        if($(this).children('li').length > maxLength) {
          $(this).children('li:gt(' + (maxLength - 1) + ')').addClass('toggleable').hide();
          $(this).append(showHideBtns);
        }
      });      

      $('.show-button').click(function() {
          $(this).parent('li').siblings('.toggleable').toggle();
          $(this).parent('li').children('.show-button').toggle();
      });

      var showciloc = '<li><span class="ciloc-button"><i class="glyphicon glyphicon-plus"></i> + More</span><span class="ciloc-button" style="display:none"><i class="glyphicon glyphicon-minus"></i>  - Less</span></li>';      
      var cimaxLength = 3;

      $('ul.ciloc').each(function() {
        if($(this).children('li').length > cimaxLength) {
          $(this).children('li:gt(' + (cimaxLength - 1) + ')').addClass('toggleable').hide();
          $(this).append(showciloc);
        }
      });      

      $('.ciloc-button').click(function() {
          $(this).parent('li').siblings('.toggleable').toggle();
          $(this).parent('li').children('.ciloc-button').toggle();
      });   
      
      var showcatloc = '<li><span class="show_more"><i class="glyphicon glyphicon-plus"></i> + More</span><span class="show_more" style="display:none"><i class="glyphicon glyphicon-minus"></i>  - Less</span></li>';      
      var citymax = 10;
      $('ul.catloc').each(function() {
        if($(this).children('li').length > citymax) {
          $(this).children('li:gt(' + (citymax - 1) + ')').addClass('toggleable').hide();
          $(this).append(showcatloc);
        }
      });      

      $('.show_more').click(function() {
          $(this).parent('li').siblings('.toggleable').toggle();
          $(this).parent('li').children('.show_more').toggle();
      });             
            
    });

      $(".slider").slider({

        <?php 
              //  FOR MAX LIMIT

              if($max_value == "")
              {
                $max = 1000;
              }
              elseif($max_value < 1000){

                $max = 1000;
              }
              else
              {
                $max = $max_value;
              }

              //  FOR MIN VALUE IN SLIDER

              if($min_value < $max_value)
              {
                  $min_value = $min_value;
              }
              else
              {
                 $min_value = 0;
              }

        ?>

        min: 0, 
        max: {{ $max }}, 
        range: true, 
        values: [0, {{ $max_value }} ],
      }).slider("pips", {
        rest: "label",
        prefix: "$",
        suffix: ".00¢"
      }).slider("float");

      $('.slider').slider({
            change: function(event, ui) { 
                  //  alert(ui.values);

                  $("#slid").val(ui.values);

                  filterproperty(url);
            } 
      });



    var url = $(this).attr('href');
    //ajax page listing
    $('#check-areas').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      if($(this).prop('checked') == true){
          $(".areas").prop( "checked", true );
      } else {
          $(".areas").prop( "checked", false );
      }
      filterproperty(url);
    });

    $('#check-cities').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      if($(this).prop('checked') == true){
          $(".cities").prop( "checked", true );
      } else {
          $(".cities").prop( "checked", false );
      }
      filterproperty(url);
    });

    $('#check-pricing').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      if($(this).prop('checked') == true){
          $(".pricing").prop( "checked", true );
      } else {
          $(".pricing").prop( "checked", false );
      }
     filterproperty(url);
    });

    $('#check-accomodation').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      if($(this).prop('checked') == true){
          $(".accomodation").prop( "checked", true );
      } else {
          $(".accomodation").prop( "checked", false );
      }
     filterproperty();
    });

    $('#check-amenities').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      if($(this).prop('checked') == true){
          $(".amenities").prop( "checked", true );
      } else {
          $(".amenities").prop( "checked", false );
      }
     filterproperty(url);
    });

//  SORT BY PRICE
    $('#radio_price').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      if($(this).prop('checked') == true){
          $("#radio_price").prop( "checked", true );
      } else {
          $("#radio_price").prop( "checked", false );
      }
     filterproperty(url);
    });

//  SORT BY POPULAR
    $('#radio_popular').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      if($(this).prop('checked') == true){
          $("#radio_popular").prop( "checked", true );
      } else {
          $("#radio_popular").prop( "checked", false );
      }
     filterproperty(url);
    });

//  SORT BY DISTANCE
    $('#radio_dist').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      if($(this).prop('checked') == true){
          $("#radio_dist").prop( "checked", true );
      } else {
          $("#radio_dist").prop( "checked", false );
      }

      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function (position) {
              console.log(position);
              console.log(position.coords.latitude);
              console.log(position.coords.longitude);

            $("#user_lat").val(position.coords.latitude);
            $("#user_lng").val(position.coords.longitude);
          });
      }

     filterproperty(url);
    });

//  SORT BY RATING
    $('#radio_rating').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      if($(this).prop('checked') == true){
          $("#radio_rating").prop( "checked", true );
      } else {
          $("#radio_rating").prop( "checked", false );
      }
     filterproperty(url);
    });        

//  RATING FILTER
    $('#rating').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      if($(this).prop('checked') == true){
          $("#rating").prop( "checked", true );
      } else {
          $("#rating").prop( "checked", false );
      }
     filterproperty(url);
    });   


    $('.click-pricing').click(function(){
      var price_val = $(this).val();
      if(price_val == 0){
        $("#check-pricing").prop( "checked", true );
        $(".pricing").prop( "checked", true );
      } else {
        $(".pricing").prop( "checked", false );
        $("input[id='pricing'][value='" + price_val + "']").prop('checked', true);
      }
      filterproperty(url);
    });
    
    $('.select-filter').change(function(){
      $('.click-pricing').removeClass('current');
      $('.add-current').addClass('current');
      filterproperty(url);
    });

    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');
        $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');

         var url = $(this).attr('href');

        // var url = '{{ url('hotel_list') }}';

         
        filterproperty(url);
        window.history.pushState("", "", url);
    });

    function filterproperty(url)
    {
      var html = "";
      html += '<div class="col-sm-3 loader_img">';
      html += '<img src="{{ URL::asset('/assets/front/otel2go/images/loader.apng') }}" alt="loader">';
      html += '</div>';

      $('.append_properties').html('');
      $(".append_properties").append(html);

        var city = "";
        $.each($("#cities:checked"),function(e){
            city += $(this).val()+',';
        });
        cities = city.substring(0,city.length-1);
        var price = "";
        $.each($("#pricing:checked"),function(e){
            price += $(this).val()+',';
        });
        pricing = price.substring(0,price.length-1);
        var amenity = "";
        $.each($("#amenities:checked"),function(e){
            amenity += $(this).val()+',';
        });
        amenities = amenity.substring(0,amenity.length-1);   
        var accomodation = "";
        $.each($("#accomodation:checked"),function(e){
            accomodation += $(this).val()+',';
        });
        accomodation_type = accomodation.substring(0,accomodation.length-1);

        var rating = "";
        $.each($("#rating:checked"),function(e){
            rating += $(this).val()+',';
        });
        rating = rating.substring(0,rating.length-1);

        //  alert(rating);

        var radio_price = $("#radio_price:checked").val();

        var radio_popular = $("#radio_popular:checked").val();

        var radio_dist = $("#radio_dist:checked").val();

        var radio_rating = $("#radio_rating:checked").val();

        var user_lat = $("#user_lat").val();

        var user_lng = $("#user_lng").val();

        // alert(user_lat);

        // alert(user_lng);



        var area = "";
        $.each($("#areas:checked"),function(e){
            area += $(this).val()+',';
        });
        areas = area.substring(0,area.length-1);

        var slider_val = $("#slid").val();
            //  alert(slider_val);

        var data = {};
        data["cities"] = cities;
        data["pricing"] = pricing;
        data["amenities"] = amenities;
        data["accomodation"] = accomodation_type;
        data["rating"] = rating;
        data["areas"] = areas;
        data["radio_price"] = radio_price;
        data["radio_popular"] = radio_popular;
        data["radio_dist"] = radio_dist;
        data["radio_rating"] = radio_rating;
        data["slider_val"] = slider_val;
        data["user_lat"] = user_lat;
        data["user_lng"] = user_lng;        
        token = $('input[name=_token]').val();

            
        $.ajax({
          url: url,
          headers: {'X-CSRF-TOKEN': token},
          data: { data },
          datatype: 'JSON',
          success: function (resp) {
            $('.append_properties').html(resp);
          }, 
          error:function(err) {
            //  alert('Ak');
            //  $('.append_properties').html('');

            var html = "";
            html += '<div class="no_data_found">';
            html += '<div class="no_data_found_inner">';
            html += '<img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/no_hotels_found.png'); ?>">';
            html += '<p>No hotels available.</p>';
            html += '</div>';
            html += '</div>';

            $('.append_properties').html('');
            $(".append_properties").append(html);            
            //  $('.append_properties').html('No Properties Found.');
            //  console.log('here'); 
            return false;
          }
        });
    } 
  </script>
@endsection