  @extends('layouts.front')
	@section('content')


<script type="text/javascript">
      $(window).load(function() {

          $('.listing_header').show();

      });
</script>

<link href="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/css/animations.css');?>" rel="stylesheet" />
    <!-- content start -->
	<section class="store_list">
        <div class="container">
          <div class="four_not_error">
          <img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/404_error.png');?>" alt="404">
          <h1>@lang('messages.Page not found')</h1>
          <h3>@lang('messages.The page your looking for is temporarily unavailable or has') <br> @lang('messages.been removed.')</h3>
          <a class="btn btn-success" class="hvr-ripple-out" href="{{ URL::to('/book-now') }}" title="Home">@lang('messages.Go to Home')</a>
		  
          </div>
        </div>
  </section>
<script type="text/javascript" src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/js/css3-animate-it.js');?>"></script>

    @endsection


