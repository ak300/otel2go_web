@extends('layouts.managers')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/buttons.html5.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('managers/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Managers')</a></li>
			<li>@lang('messages.Housekeeping Tasks')</li>
		</ul>
		<h4>@lang('messages.Housekeeping Tasks')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->
	<!-- will be used to show any messages -->
	@if (Session::has('message'))
		<div class="admin_sucess_common">
	<div class="admin_sucess">
		<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div></div></div>
	@endif
<div class="contentpanel">

<div class="buttons_block pull-right">
<div class="btn-group mr5">
<a class="btn btn-primary tip" href="{{ URL::to('managers/housekeeping_tasks/create') }}" title="Add New">@lang('messages.Add New')</a>
</div>
</div>

 <table id="housekeeping_tasks" class="table table-striped table-bordered responsive">
    <thead>
        <tr class="headings">
			
            <th>@lang('messages.S.no')</th>
            <th>@lang('messages.Task Name')</th> 
            <th>@lang('messages.Housekeeping Staff Name')</th>
			<th>@lang('messages.Outlet Name')</th>
			<th>@lang('messages.Room Name')</th> 
			<th>@lang('messages.Task Date')</th>
			<th>@lang('messages.Task Time')</th>
            <th>@lang('messages.Created date')</th>
			<th>@lang('messages.Room Clean Status')</th>
			<th>@lang('messages.Action')</th>
        </tr>
    </thead>
</table>
</div>

<script>
$(function() {
    $('#housekeeping_tasks').DataTable({
		dom: 'Blfrtip',
		buttons: [
		],
        processing: true,
        serverSide: false,
		responsive: true,
		autoWidth:false,
        ajax: '{!! route('anyajaxmanagerhousekeepingtasks.data') !!}',
        "order": [],
		"columnDefs": [ {
		  "targets"  : 'no-sort',
		  "orderable": false,
		}],
        columns: [
			{ data: 'id', name: 'id',orderable: false },
			{ data: 'taskname', name: 'taskname',searchable:true },
			{ data: 'firstname', name: 'firstname',searchable:true },
			{ data: 'outlet_name', name: 'outlet_name',searchable:true },
			{ data: 'room_name', name: 'room_name',searchable:true  },
			{ data: 'task_date', name: 'task_date' },
			{ data: 'task_time', name: 'task_time' },
            { data: 'created_at', name: 'created_at' },
			{ data: 'room_cleaning_status', name: 'room_cleaning_status',orderable: false },
			{ data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });
});


$(document).ready(function(){
	$(document).on('change','#clean_status',function(){
		var status = $(this).val();
		var task_id = $(this).find(':selected').attr('data-id');
		$.ajax({
	    		type: "POST",
	    		dataType: "json",
	    		url: '{{ url('managersManageTasks') }}',
	    		data: { _token: "{{csrf_token()}}",status:status,task_id:task_id},
	    		success: function(response){
	    				location.reload();
	    			
	    		},
	    	});
	});
});
</script>
@endsection
