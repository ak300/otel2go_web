@extends('layouts.managers')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script> 
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-timepicker.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-timepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" /> 
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>

<div class="row">
    <div class="col-md-12 ">
        <!-- Nav tabs -->
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-home"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ URL::to('managers/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Managers')</a></li>
                        <li>@lang('messages.Housekeeping Staffs')</li>
                    </ul>
                    <h4>@lang('messages.Edit Housekeeping Staffs')</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><?php echo trans('messages.'.$error); ?> </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <ul class="nav nav-tabs"></ul>
            {!!Form::open(array('url' => ['managersupdate_housekeeper', $data->id], 'method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'create_housekeeper_form', 'files' => true));!!} 
                <div class="tab-content mb30">
                    <div class="tab-pane active" id="home3">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.First Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="first_name" maxlength="50" placeholder="@lang('messages.First Name')" class="form-control" 
                                value="{!! $data->firstname !!}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Last Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="last_name" maxlength="50" placeholder="@lang('messages.Last Name')" class="form-control" 
                                value="{!! $data->lastname !!}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Mobile')<span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                              <input type="text" name="mobile" maxlength="12" placeholder="@lang('messages.Mobile')" class="form-control" value="{!! $data->mobile_number !!}" />
                              <span class="help-block">@lang('messages.Add Phone number(s) in comma seperated. <br>For example: 9750550341,9791239324')</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Date of birth')<span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="date_of_birth" autocomplete="off" value="{!! $data->date_of_birth !!}" placeholder="mm/dd/yyyy" id="datepicker">
                                    <span class="input-group-addon datepicker-trigger"><i class="glyphicon glyphicon-calendar" id="dob"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Gender') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select name="gender" class="form-control">
                                    <option value="" >@lang('messages.Select Gender')</option>
                                    <option value="M" <?php if("M"==$data->gender){ echo "selected=selected"; } ?> >@lang('messages.Male')</option>
                                    <option value="F" <?php if("F"==$data->gender){ echo "selected=selected"; } ?>>@lang('messages.Female')</option>
                                </select>
                            </div>
                        </div>

                        <?php /* <div class="form-group" id="outlet_head">
                            <label class="col-sm-2 control-label">@lang('messages.Outlet Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select name="outlet_name" id="outlet_name" class="form-control"  >
                                    <option value="">@lang("messages.Select Outlet")</option>
                                    <?php $outlet_list = get_outlet_list(Session::get('vendor_id')); ?>
                                    @if(count($outlet_list) > 0)
                                        @foreach($outlet_list as $ot)
                                            <option value="{{$ot->id}}" @if($data->outlet_id == $ot->id) {{'selected'}} @endif >{{ ucfirst($ot->outlet_name) }}</option>
                                        @endforeach
                                    @else
                                        <option value="">@lang("messages.No Outlet Found")</option>
                                    @endif
                                </select>
                            </div>
                        </div> */ ?>
                        <input type="hidden" name="outlet_name" value="{{ Session::get('property_id') }}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Address') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <textarea name="address"  placeholder="@lang('messages.Address')" class="form-control" maxlength="255"/>{{ $data->address }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Image')</label>
                            <div class="col-sm-6">
                                @if(isset($data['image']) && $data['image'] !="")
                                <img src="<?php echo url('/assets/admin/base/images/house_keepers/'.$data['image'].'');?>" name="image" class="img-thumbnail">
                                @endif
                                <input type="file" name="image" />
                            </div>
                        </div>
                        <div class="form-group">
                                <label  class="col-sm-2 control-label">@lang('messages.Status')<span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                                    <?php $checked = ""; 
                                          if($data->status==1){
                                                $checked ="checked=checked";
                                          }
                                    ?>
                                    <input type="checkbox" class="toggle" name="status" data-size="small" <?php echo $checked;?> data-on-text="@lang('messages.Yes')" data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
                                </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary mr5" title="Save">@lang('messages.Save')</button>
                        <button type="reset" title="Cancel" class="btn btn-default" onclick="window.location='{{ url('managers/house_keeping') }}'">@lang('messages.Cancel')</button>
                    </div>
                </div>
            {!!Form::close();!!} 
        </div>
    </div>
</div>
<script> 
    $(window).load(function(){
        $('#datepicker').datepicker({
            yearRange: '<?php echo date("Y") - 100; ?>:<?php echo date("Y"); ?>',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true
        });
        $(".datepicker-trigger").on("click", function() {
            $("#datepicker").datepicker("show");
        });
    });
</script>
@endsection
