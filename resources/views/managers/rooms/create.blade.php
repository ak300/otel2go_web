@extends('layouts.managers')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" /> 
<div class="row">	
	<div class="col-md-12 ">
<!-- Nav tabs -->
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('managers/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Managers')</a></li>
			<li>@lang('messages.Rooms')</li>
		</ul>
		<h4>@lang('messages.Add Rooms')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
			<ul>
				@foreach ($errors->all() as $error)
					<li><?php echo trans('messages.'.$error); ?> </li>
				@endforeach
			</ul>
		</div>
		@endif
<ul class="nav nav-tabs"></ul>
       {!!Form::open(array('url' => 'managerscreaterooms', 'method' => 'post','class'=>'tab-form attribute_form','id'=>'room_form','files' => true));!!}
	<div class="tab-content mb30">
	<div class="tab-pane active" id="home3">

				<?php if(Session::get('manager_outlet')!=0){?>
				
				 <div class="form-group">
				 	<label class="col-sm-2 control-label">Room Name<span class="asterisk">*</span></label>
					<div class="col-sm-10">
                          <input type="text" name="room_name"  placeholder="<?php echo trans('messages.Rooms Name');?>" class="form-control" value="{{ old('room_name') }}"  maxlength="32" />
                    </div>
                </div>

				<div class="form-group ">
					<label class="col-sm-2 control-label">@lang('messages.Room Types')<span class="asterisk">*</span></label>
					<div class="col-sm-10">
						<select class="form-control" name="room_type_id">
							<option value="">Select Room</option>
							<?php foreach($room_type as $value) {  ?>
							
								<option value="{!! $value->id !!}" <?php echo (old('room_type_id')==$value->id)?'selected="selected"':''; ?> >{!! $value->room_type !!}</option>
							<?php }  ?>
				
						</select>
					</div>
				</div>



				<div class="form-group">
				 	<label class="col-sm-2 control-label">@lang('messages.Room Floor')<span class="asterisk">*</span></label>
					<div class="col-sm-10">
                          <input type="text" name="room_floor"  placeholder="<?php echo trans('messages.Rooms Floor');?>" class="form-control" value="{!! Input::old('room_floor') !!}" maxlength="32" />  
                    </div><!-- input-group-btn -->
                </div>

				<div class="form-group">
					<label  class="col-sm-2 control-label">@lang('messages.Property Name')</label>
					<div class="col-sm-10">
						<?php $property_name = Session::get('outlet_name');	?>
						<h4><?php echo $property_name; ?></h4>
					</div>
				</div>

                <?php } else { ?>
                
                <div class="list-empty-text"> @lang('messages.No properties added yet.') </div>
                <?php } ?>
			<?php /*			
		<div class="form-group">
			<label class="col-sm-2 control-label">@lang('messages.Amenity Image') <span class="asterisk">*</span></label>
			<div class="col-sm-10">
				<input type="file" name="amenty_image"/>
				<span class="help-text">@lang('messages.Please upload 263X127 images for better quality')</span>
			</div>
		</div>
			*/
		?>
			
		<div class="form-group">
		  <label  class="col-sm-2 control-label">@lang('messages.Status')</label>
			<div class="col-sm-10">
            <?php $checked = ""; ?>
            <?php if(old('status')) { $checked = "checked"; }?>
            <input type="checkbox" class="toggle" name="status" data-size="small" <?php echo $checked;?> data-on-text="@lang('messages.Yes')" data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
			</div>
	   </div>
					
       </div>
		<div class="panel-footer">
		<button class="btn btn-primary mr5" title="Save">@lang('messages.Save')</button>
		<button type="reset" title="Cancel" class="btn btn-default" onclick="window.location='{{ url('managers/rooms') }}'">@lang('messages.Cancel')</button>
		</div>
        </div>
      
 {!!Form::close();!!} 
</div></div></div>
<script>
$(window).load(function(){	
	$('form').preventDoubleSubmission();	
});
</script>
@endsection
