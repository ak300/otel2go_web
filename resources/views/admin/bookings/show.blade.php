@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>
 <link href="{{ URL::asset('assets/admin/base/css/select2.css') }}" media="all" rel="stylesheet" type="text/css" />
 <link href="{{ URL::asset('assets/admin/base/css/toastr.min.css') }}" rel="stylesheet" />
 
 <script src="{{ URL::asset('assets/admin/base/js/toastr.min.js') }}"></script>
<!-- Nav tabs -->
<div class="pageheader">
<div class="media">
    <div class="pageicon pull-left">
        <i class="fa fa-home"></i>
    </div>
    <div class="media-body">
        <ul class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
            <li>@lang('messages.Orders')</li>
        </ul>
		<h4>@lang('messages.View Booking Details')   <?php  /*   echo $data->booking_random_id;  */  ?></h4>
    </div>
</div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">
<div class="col-md-12">
<div class="row panel panel-default">
    <div class="grid simple">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li><?php echo trans('messages.'.$error); ?> </li>
                @endforeach
            </ul>
        </div>
        @endif
        <ul class="nav nav-justified nav-wizard nav-pills">
            <li @if(old('tab_info')=='login_info' || old('tab_info')=='') class="active" @endif><a href="#login_info" class="login_info" data-toggle="tab"><strong>@lang('messages.Order Information')</strong></a></li>
            <?php   /* <li @if(old('tab_info')=='vendor_info') class="active" @endif ><a href="#vendor_info" class="vendor_info" data-toggle="tab"><strong>@lang('messages.Payment Information')</strong></a></li>
            <?php   /* <li @if(old('tab_info')=='contact_info') class="active" @endif ><a href="#contact_info" class="contact_info" data-toggle="tab"><strong>@lang('messages.Products')</strong></a></li>  */  ?> 
            <li @if(old('tab_info')=='history') class="active" @endif ><a href="#history" class="history" data-toggle="tab"><strong>@lang('messages.History')</strong></a></li>
        </ul>
        
        <div class="tab-content tab-content-simple mb30 no-padding" >
            <div class="tab-pane active" id="login_info">
                    <legend>@lang('messages.Order Information')</legend>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">@lang('messages.Booking id')</label>
                        <div class="col-sm-3">
                            {{  "#".$data->booking_random_id  }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label"> @lang('messages.Room Number') :</label>

                        <div class="col-sm-3">
                                {{  $data->room_id  }}
                        </div>
                    </div>            
                    
                    <div class="form-group">
                        <label for="index" class="col-sm-3 control-label"> @lang('messages.Duration') :</label>

                        <div class="col-sm-6">
                                {{  $data->check_in_date ." - ". $data->check_out_date}}
                        </div>
                    </div>
                    @if($data->created_date !="")
                    <div class="form-group">
                        <label for="content" class="col-sm-3 control-label"> @lang('messages.Booked Date') :</label>
                        <div class="col-sm-6">
                                {{  $data->created_date }}
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="content" class="col-sm-3 control-label"> @lang('messages.No of Adults') :</label>
                        <div class="col-sm-6">
                                {{  $data->adult_count }}
                        </div>
                    </div>
@if($data->child_count !="")
                    <div class="form-group">
                        <label for="content" class="col-sm-3 control-label"> @lang('messages.No of Children') :</label>
                        <div class="col-sm-6">
                                {{  $data->child_count }}
                        </div>
                    </div> 
@endif 

                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label"> @lang('messages.Vendor Name') :</label>

                        <div class="col-sm-3">
                                {{  ucfirst($vendor_details->first_name)." ".$vendor_details->last_name  }}
                        </div>
                    </div>   

                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label"> @lang('messages.Property Name') :</label>

                        <div class="col-sm-3">
                                {{  ucfirst($vendor_details->outlet_name) }}
                        </div>
                    </div>   

                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label"> @lang('messages.Customer Name') :</label>

                        <div class="col-sm-3">
                                {{  ucfirst($data->firstname)." ".$data->lastname  }}
                        </div>
                    </div>            
                    <div class="form-group">
                        <label class="col-sm-3 control-label">@lang('messages.Status')</label>
                        <div class="col-sm-3">
                          <span style="color:<?php echo $data->color_code; ?>">
                          {!! $data->name !!}</span>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label"> @lang('messages.Email') :</label>

                        <div class="col-sm-3">
                                {{  $data->email  }}
                        </div>
                    </div>   
                    @if($data->address !="")
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label"> @lang('messages.Address') :</label>

                        <div class="col-sm-3">
                                {{  ucfirst($data->address)  }}
                        </div>
                    </div>
                    @endif                                                
                    <div class="form-group">
                        <label for="title" class="col-sm-3 control-label"> @lang('messages.Mobile Number') :</label>

                        <div class="col-sm-3">
                                @if($data->country_code){{  "(".$data->country_code . ") " }} @endif {{ $data->mobile_number  }}
                        </div>
                    </div>  
                                    
                               
            </div>
            <!--   BOOKING INFORMATION DONE -->
<?php   /*
            <div class="tab-pane" id="vendor_info">
                <legend>@lang('messages.Payment Information')</legend>
                <div class="form-group">
                    <label class="col-sm-4 control-label">@lang('messages.Payment mode')</label>
                    <div class="col-sm-8">
                      <label class="col-sm-4 control-label"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">@lang('messages.Currency')</label>
                    <div class="col-sm-8">
                      <label class="col-sm-4 control-label"></label>
                    </div>
                </div>
            </div>

            <!--        -->
            <div class="tab-pane" id="delivery_info">
                <legend>Delivery Information</legend>

            </div>
*/  ?>            
            <div class="tab-pane" id="vendor_info">
                <legend>@lang('messages.Payment Information')</legend>
                <div class="pull-right">
                   <button type="button" class="btn btn-primary" id="charges_popup">Add Charges</button><br><br>
                </div>
                <h3>Charges</h3>


            @if(count($charges_list)>0)
                
                <table id="manager-table" class="table table-striped table-bordered responsive">
                    <thead>
                        <tr class="headings">
                            
                            <th>@lang('messages.S.no')</th>
                            <th>@lang('messages.User Name')</th>  
                            <th>@lang('messages.Amount')</th>
                            <th>@lang('messages.Charge Type')</th>  
                            <th>@lang('messages.Room')</th>
                            <th>@lang('messages.Description')</th>              
                            <th>@lang('messages.Created date')</th>
                            <?php /*    <th>@lang('messages.Status')</th>   */  ?>
                            <?php /*    <th>@lang('messages.Actions')</th>  
                            <th>@lang('messages.Status')</th>*/  ?> 
                        </tr>
                    </thead>
                         
                    <tbody>
                        @foreach($charges_list as $charges)
                        <tr>
                            <td>{{  $charges->bc_id         }}</td>
                            <td>{{  $charges->firstname     }}</td>
                            <td>{{  $charges->price         }}</td>
                                @if($charges->charge_type == 1)
                                    <td><?php echo 'Room Charge';?></td>
                                @elseif($charges->charge_type == 2)
                                    <td><?php echo 'Cleaning Charge';?></td>
                                @elseif($charges->charge_type == 3)
                                    <td><?php echo 'Food Charge';?></td>
                                @elseif($charges->charge_type == 4)
                                    <td><?php echo 'Extra Charges';?></td>
                                @endif              
                            <td>{{  $charges->room_name     }}</td>
                            <?php   /*  <td>{{  $charges->room_type     }}</td> */  ?>
                            <td>{{  ucfirst($charges->bc_notes) }}</td>             
                            <td>{{  $charges->created_date  }}</td>
                            <?php /*    
                            <td>
                                @if($charges->booking_status==0)
                                    <span class="label label-warning">Inactive</span>
                                @elseif($charges->booking_status==1)
                                    <span class="label label-success">Active</span>
                                @elseif($charges->booking_status==2)
                                    <span class="label label-danger">Delete</span>
                                @endif          
                            </td>    
                            */  ?>           
                            
                        <tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            <hr>
                <div><h4> Total : {{    $charges_count->price  }}</h4></div>
            <hr>
                
                <div class="pull-right">
                   <button type="button" class="btn btn-primary" id="payment_popup">Add Payment</button><br><br>
                </div>

                <h3>Payments</h3>
            @if(count($payment)>0)
                <table id="manager-table" class="table table-striped table-bordered responsive">
                    <thead>
                        <tr class="headings">
                            
                            <th>@lang('messages.S.no')</th>
                            <th>@lang('messages.User Name')</th>  
                            <th>@lang('messages.Amount')</th>
                            <th>@lang('messages.Payment Method')</th>  
                            <?php   /*  <th>@lang('messages.Email')</th> */ ?>
                            <th>@lang('messages.Room')</th>
                            <th>@lang('messages.Description')</th> 
                            <th>@lang('messages.Created date')</th>
                            <?php /*    <th>@lang('messages.Status')</th>
                            <th>@lang('messages.Actions')</th> */   ?>
                        </tr>
                    </thead>
                         
                    <tbody>
                        @foreach($payment_list as $pay)
                        <tr>
                            <td>{{  $pay->payment_id    }}</td>
                            <td>{{  $pay->firstname     }}</td>
                            <td>{{  $pay->paid_amount   }}</td>
                                @if($pay->paid_type == 1)
                                    <td><?php echo 'Cash';?></td>
                                @endif
                            <?php   /*  <td>{{  $pay->email         }}</td> */  ?>
                            <td>{{  $data->room_id        }}</td>
                            <td>{{  ucfirst($pay->description)  }}</td>
                            <td>{{  $pay->created_date  }}</td>
                            <?php /*    
                            <td>
                                @if($pay->payment_status==0)
                                    <span class="label label-warning">Inactive</span>
                                @elseif($pay->payment_status==1)
                                    <span class="label label-success">Active</span>
                                @elseif($pay->payment_status==2)
                                    <span class="label label-danger">Delete</span>
                                @endif          
                            </td>
                            */  ?>
                        <tr>
                        @endforeach
                    </tbody>
                </table>

            @endif

            @if(count($payment)>0)
            <hr>
            <div><h4>Payment Total :  {{    $payment_count->paid_amount  }}</h4></div>
            <hr>
            <div>
                <h3>Due Balance: @if($charges_count->price > $payment_count->paid_amount)
                                {{  $charges_count->price - $payment_count->paid_amount }}
                                 @endif
                </h3>
            </div>
            @endif
            <div class="pull-right"></div>

                        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_charge">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">

                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
                                
                                <h5 class="modal-title" id="gridSystemModalLabel">Add Charges</h5>
                              </div>
                              <div class="modal-body">
                                {!!Form::open(array('url' => 'booking/charges', 'method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'charges_form', 'files' => true));!!} 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">@lang('messages.Amount') <span class="asterisk">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" id="charges_amount" name="charges_amount" maxlength="50" placeholder="@lang('messages.Amount')" class="form-control" value="{!! old('charges_amount') !!}" />
                                        </div>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">@lang('messages.Select Paying Customer') <span class="asterisk">*</span></label>
                                        <div class="col-sm-8">
                                          <select id="customer_name" name="customer_name_charges" class="form-control" >
                                            <?php   /*  <option value="">@lang('messages.Select Customer Type')</option>    */  ?>
                                            <?php /*@if (count(getAllCustomerList()) > 0)
                                                @foreach (getAllCustomerList() as $key) */ ?>
                                                    <option value="{{ $data->cust_id }}" <?php echo (old('customer_name_charges')==$data->cust_id)?'selected="selected"':''; ?> ><?php echo $data->firstname; ?></option>
                                                <?php /*@endforeach
                                            @endif
                                            */  ?>
                                          </select>
                                        </div>
                                    </div> 
                                    <input type="hidden" name="booking_id" value="{{    $data->bid  }}" id="booking_id">
                                    <input type="hidden" name="room_id" value="{{   $data->room_id  }}" id="room_id">
                                    <input type="hidden" name="room_type_id" value="{{ $data->room_type }}" id="room_type_id">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">@lang('messages.Charge Type') <span class="asterisk">*</span></label>
                                        <div class="col-sm-8">
                                          <select id="charge_type" name="charge_type" class="form-control" >
                                            <?php   /*  <option value="">@lang('messages.Select Customer Type')</option>    */  ?>
                                            @if (count(getChargeTypes()) > 0)
                                                @foreach (getChargeTypes() as $key => $type)
                                                    <option value="{{ $key }}" <?php echo (old('charge_type')==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                                                @endforeach
                                            @endif
                                          </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">@lang('messages.Description') <span class="asterisk">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="notes" id="notes" maxlength="100" placeholder="@lang('messages.Description')" class="form-control" value="{!! old('notes') !!}" id="address"/>
                                        </div>
                                    </div>

                                     

                                    
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                              </div>
                              {!!Form::close();!!}
                            </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->      



                        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_id">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">

                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>   
                                
                                <h5 class="modal-title" id="gridSystemModalLabel">Add Payment</h5>
                              </div>
                              <div class="modal-body">
                                {!!Form::open(array('url' => 'booking/payment', 'method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'payment_form', 'files' => true));!!} 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">@lang('messages.Amount') <span class="asterisk">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" id="amount" name="amount" maxlength="50" placeholder="@lang('messages.Amount')" class="form-control" value="{{   $charges_count->price - $payment_count->paid_amount  }}" />
                                        </div>
                                    </div>
            <?php /*                        
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">@lang('messages.Date of birth')<span class="asterisk">*</span></label>
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <input type="text" class="form-control clsDatePicker" name="date_of_birth" autocomplete="off" value="{!! old('date_of_birth') !!}" placeholder="mm/dd/yyyy" id="datepicker">
                                                <span class="input-group-addon datepicker-trigger"><i class="glyphicon glyphicon-calendar" id="dob"></i></span>
                                            </div>
                                        </div>
                                    </div>   
                                    <div class="form-group">
                                        <input type="text" autocomplete="off" value="" id="appointment_date" class="appointment_date form-control" placeholder="@lang('messages.date')" />
                                    </div>
            */  ?>                        
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">@lang('messages.Select Paying Customer') <span class="asterisk">*</span></label>
                                        <div class="col-sm-8">
                                          <select id="customer_name" name="customer_name" class="form-control" >
                                            <?php   /*  <option value="">@lang('messages.Select Customer Type')</option>    */  ?>
                                            <?php /*@if (count(getAllCustomerList()) > 0)
                                                @foreach (getAllCustomerList() as $key) */ ?>
                                                    <option value="{{ $data->cust_id }}" <?php echo (old('customer_name')==$data->cust_id)?'selected="selected"':''; ?> ><?php echo $data->firstname; ?></option>
                                                <?php /*@endforeach
                                            @endif
                                            */  ?>
                                          </select>
                                        </div>
                                    </div> 
                                    
                                    <input type="hidden" name="booking_id" value="{{    $data->bid  }}" id="booking_id">
                                    <input type="hidden" name="room_id" value="{{   $data->room_id  }}" id="room_id">
                                    <input type="hidden" name="room_type_id" value="{{ $data->room_type }}" id="room_type_id">                                              
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">@lang('messages.Payment Method') <span class="asterisk">*</span></label>
                                        <div class="col-sm-8">
                                          <select id="paid_type" name="paid_type" class="form-control" >
                                            <?php   /*  <option value="">@lang('messages.Select Customer Type')</option>    */  ?>
                                            @if (count(getPaymentTypes()) > 0)
                                                @foreach (getPaymentTypes() as $key => $type)
                                                    <option value="{{ $key }}" <?php echo (old('paid_type')==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                                                @endforeach
                                            @endif
                                          </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">@lang('messages.Description') <span class="asterisk">*</span></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="description" id="description" maxlength="100" placeholder="@lang('messages.Description')" class="form-control" value="{!! old('description') !!}" id="address"/>
                                        </div>
                                    </div>

                                     

                                    
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                              </div>
                              {!!Form::close();!!}
                            </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->      

            </div>
            <!--   Paid Information DONE -->
            <div class="tab-pane" id="history">
                <legend>@lang('messages.History')</legend>
                <div id="history-data">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                              <th class="text-left">@lang('messages.Date')</th>
                              <th class="text-right">@lang('messages.Comment')</th>
                              <th class="text-right">@lang('messages.Status')</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $subtotal = "";
                            foreach($order_history as $history) { ?>
                                <tr>
                                    <td class="text-left"><?php echo date('M j Y g:i A', strtotime($history->log_time)); ?> </td>
                                    <td class="text-right">{{$history->order_comments}}</td>
                                    <td class="text-right"><span style="color:<?php echo $history->color_code; ?>">
                                        {!! $history->status_name !!}</span></td>
                                </tr>
                            <?php } ?>                            
                        </tbody>
                    </table>
                </div>
                <fieldset>
                    <legend>@lang('messages.Add Booking History')</legend>
                    {!!Form::open(array('url' => ['update_delivery_status',$data->bid], 'method' => 'post','class'=>'form-horizontal','id'=>'update_delivery_status','files' => true));!!}
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-order-status">@lang('messages.Booking Status')</label>
                                <div class="col-sm-10">
                                    <select name="order_status_id" id="input-order-status" class="form-control">
                                        <option value="">@lang('messages.Select Status')</option>
                                        <?php
                                            //  $order_status = getOrderStatus();
                                            foreach($order_status as $status) { ?>
                                                <option value="{{$status->id}}">{{$status->name}}</option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <input type="hidden" name="order_id" value="{{ $data->bid }}">
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-comment">Messages</label>
                                <div class="col-sm-10">
                                    <textarea maxlength="300" name="comment" rows="8" id="input-comment" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-comment"></label>
                                <div class="col-sm-10">
                                    <div class="error_data error"></div>
                                </div>
                            </div>
                        {!!Form::close();!!}
                        <div class="text-right">
                            <input type="hidden" name="tab_info" class="tab_info" value="">
                            <button type="button" id="button-history" data-loading-text="Loading..." class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;@lang('messages.Add History')</button>
                        </div>
                </fieldset>
            </div>
        </div>
        <div class="form-group Loading_Img" style="display:none;">
            <div class="col-sm-4">
                <i class="fa fa-spinner fa-spin fa-3x"></i><strong style="margin-left: 3px;">@lang('messages.Processing...')</strong>
            </div>
        </div>
    </div>
</div><!-- panel-default -->
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#charges_form').on('submit',function(){
    
    event.preventDefault();

    var charges = $("#charges_amount").val();

    var notes = $("#notes").val();
    //  alert(ac);

            if(charges == "")
            {
                toastr.remove();
                toastr.warning('The Amount Field is Required');
                return false;
            }    
            else if(notes == "")
            {
                toastr.remove();
                toastr.warning('The Description Field is Required');
                return false;
            }  


            var token,url,data;

            token = $('input[name=_token]').val();
            url = '{{   url('booking/charges')    }}';
            data = $("#charges_form").serialize();

            $.ajax({
                url:url,
                data: data,
                type: 'POST',
                datatype: 'JSON',

                success:function(resp){

                    if(resp == 1)
                    {
                        toastr.success('Charges Inserted Successfully');
                        $("#modal_charge").modal('hide');
                        $('#charges_form')[0].reset();
                        location.reload();
                    }
                    else
                    {
                        toastr.error('Failed to Insert');
                    }
                }

            });


        });
    });     
        $('#datepicker').datepicker({
            yearRange: '<?php echo date("Y") - 100; ?>:<?php echo date("Y"); ?>',
            minDate: new Date(),
            changeMonth: true,
            changeYear: true
        });
        $(".datepicker-trigger").on("click", function() {
            $("#datepicker").datepicker("show");
        });


    $("#payment_popup").on('click',function(){
        $("#modal_id").modal('show');
    })

    $("#charges_popup").on('click',function(){
        $("#modal_charge").modal('show');
    });

    $(document).ready(function(){
        $('#payment_form').on('submit',function(){
            
            event.preventDefault();
 
            var charges = $("#amount").val();

            var notes = $("#description").val();
            //  alert(ac);

            if(charges == "")
            {
                toastr.remove();
                toastr.warning('The Amount Field is Required');
                return false;
            }    
            else if(notes == "")
            {
                toastr.remove();
                toastr.warning('The Description Field is Required');
                return false;
            }  


            

            var token,url,data;

            token = $('input[name=_token]').val();
            url = '{{   url('booking/payment')    }}';
            data = $("#payment_form").serialize();

            $.ajax({
                url:url,
                data: data,
                type: 'POST',
                datatype: 'JSON',

                success:function(resp){

                    if(resp == 1)
                    {
                        toastr.success('Payment Inserted Successfully');
                        $("#modal_id").modal('hide');
                        $('#payment_form')[0].reset();
                        location.reload();
                    }
                    else if(resp == -1)
                    {
                        toastr.error('Failed to Insert');
                    }
                }

            });


        });
    });

$( document ).ready(function() {
    $('.error_data').hide();
    $('#categories').select2();
    @if(old('tab_info')=='vendor_info')
        $('.tab_info').val('vendor_info');
        $('#login_info').hide();
        $('#delivery_info').hide();
        $('#contact_info').hide();
        $('#vendor_info').show();
    @elseif(old('tab_info')=='login_info')
        $('.tab_info').val('login_info');
        $('#login_info').show();
        $('#delivery_info').hide();
        $('#contact_info').hide();
        $('#vendor_info').hide();
    @elseif(old('tab_info')=='delivery_info')
        $('.tab_info').val('delivery_info');
        $('#login_info').hide();
        $('#delivery_info').show();
        $('#contact_info').hide();
        $('#vendor_info').hide();
    @elseif(old('tab_info')=='contact_info')
        $('.tab_info').val('contact_info');
        $('#login_info').hide();
        $('#delivery_info').hide();
        $('#contact_info').show();
        $('#vendor_info').hide();
    @elseif(old('tab_info')=='history')
        $('.tab_info').val('history');
        $('#login_info').hide();
        $('#delivery_info').hide();
        $('#history').show();
        $('#contact_info').hide();
        $('#vendor_info').hide();   
    @endif
    /*function addOrderInfo()
    {
        var status_id = $('select[name="order_status_id"]').val();
        var token = $('input[name=_token]').val();
        var url = '{{url('order/update-status')}}';
          $.ajax({
            url: url,
            type: 'post',
            dataType: 'html',
            data: $("#update_delivery_status").serialize()
          });
    } */
    $('#button-history').on('click', function() 
    {
        if($("#input-order-status").val() == "")
        {
            //alert("Please select order status");
            $('.error_data').show().html("Please select order status");
            return false;
        }
        $('.error_data').hide().html("");
        token = $('input[name=_token]').val();
        var url = '{{url('admin/orders/update-status')}}';
        $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            data: $("#update_delivery_status").serialize(),
            type: 'POST',
            datatype: 'JSON',
            beforeSend: function() {
            $('#button-history').html('Loading');
            $("#button-history").attr("readonly",true);
            },
            complete: function() {
                  $('#button-history').html('Add History');
                  $("#button-history").attr("readonly",false);
            },
            success: function(json) 
            {
                
                toastr.success('History updated successfully')
                 $('#update_delivery_status')[0].reset();
                 $('#button-history').html('Add History');  
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) 
            {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
});
    

$(".login_info").on("click", function(){
    $('.tab_info').val('login_info');
    $('#delivery_info').hide();
    $('#vendor_info').hide();
    $('#login_info').show();
    $('#contact_info').hide();
    $('#history').hide();
});
$(".vendor_info").on("click", function(){
    $('.tab_info').val('vendor_info');
    $('#delivery_info').hide();
    $('#vendor_info').show();
    $('#login_info').hide();
    $('#contact_info').hide();
    $('#history').hide();
    //initialize();
});
$(".delivery_info").on("click", function(){
    $('.tab_info').val('delivery_info');
    $('#delivery_info').show();
    $('#vendor_info').hide();
    $('#contact_info').hide();
    $('#login_info').hide();
    $('#history').hide();
});
$(".contact_info").on("click", function(){
    $('.tab_info').val('contact_info');
    $('#delivery_info').hide();
    $('#vendor_info').hide();
    $('#login_info').hide();
    $('#contact_info').show();
    $('#history').hide();
});
$(".history").on("click", function(){
    $('.tab_info').val('contact_info');
    $('#delivery_info').hide();
    $('#vendor_info').hide();
    $('#login_info').hide();
    $('#contact_info').hide();
    $('#history').show();
});
$(window).load(function(){  
    $('form').preventDoubleSubmission();
});
</script>

@endsection
