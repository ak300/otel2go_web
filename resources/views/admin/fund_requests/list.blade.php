@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/buttons.html5.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />

<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-home"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
                <li>@lang('messages.Fund Requests')</li>
            </ul>
            <h4>@lang('messages.Fund Requests')</h4>
        </div>
    </div><!-- media -->
</div><!-- pageheader -->
<div class="contentpanel"> 

    @if (Session::has('message'))
        <div class="admin_sucess_common">
            <div class="admin_sucess">
                <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div>
            </div>
        </div>
    @endif

    <table id="adminFundRequest" class="table table-striped table-bordered responsive">
        <thead>
            <tr class="headings">
                <th>@lang('messages.S.No')</th> 
                <th>@lang('messages.Vendor Name')</th> 
                <th>@lang('messages.Reference Number')</th> 
                <th>@lang('messages.Request Amount')</th>
                <th>@lang('messages.Created Date')</th> 
                <th>@lang('messages.Request Status')</th>
            </tr>
        </thead>
		<tbody>
			<tr>
				<td class="empty-text" colspan="8" style="background-color: #fff!important;">
					<div class="list-empty-text"> @lang('messages.No records found.') </div>
				</td>
			</tr>
		</tbody>
    </table>
</div>
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">@lang('messages.Fund Request Details')</h4>
            </div>
            <div class="modal-body load_modal">

            </div>          
        </div>
    </div>
</div>
<script>
    $(function() {
        $('#adminFundRequest').DataTable({
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Export',
                    title: 'coupons',
                    footer: false,
                    exportOptions: {
                        columns: [0,1,2,3,4,5,6]
                    }
                }
            ],
            processing: true,
            serverSide: true,
            responsive: true,
            autoWidth:false,
            ajax: '{!! route('ajaxadminfundrequest.data') !!}',
            "order": [],
            "columnDefs": [ {
                "targets"  : 'no-sort',
                "orderable": false,
            }],
            columns: [
                { data: 'id', name: 'id',orderable: true },
                { data: 'vendor_name', name: 'vendor_name',searchable:true },
                { data: 'reference_no', name: 'reference_no',searchable:true },
                { data: 'request_amount', name: 'request_amount',searchable:true },
                { data: 'created_at', name: 'created_at' },
                { data: 'request_status', name: 'request_status' },
            ],
        });
    });
</script>
<script type="text/javascript">
    
     $(document).on('click','.approve_request',function(){
        var fund_id = $(this).val();
        if(fund_id !=""){
                $.ajax({
                    url: '{{ url("update_fund_requests") }}',
                    type:'POST',
                    data: { _token: "{{csrf_token()}}",fund_id:fund_id},
                   
                    success:function(data){
                        $('#myModal').modal();
                        $('#myModal').on('shown.bs.modal', function(){
                            $('#myModal .load_modal').html(data);
                        });
                        $('#myModal').on('hidden.bs.modal', function(){
                            $('#myModal .modal-body').data('');
                        });
                   }
                });
            }
        });

    $(document).on('click','.reject_request',function(){
        var reject_id = $(this).val();
        if(reject_id !=""){
                $.ajax({
                    url: '{{ url("reject_fund_requests") }}',
                    type:'POST',
                    data: { _token: "{{csrf_token()}}",reject_id:reject_id},
                   
                    success:function(response){
                        var result =eval('('+response+')');
                        if(result.success == 'true')
                        { 
                            location.reload();
                        }
                   }
                });
            }
    });
</script>
@endsection
