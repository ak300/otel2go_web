@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/highcharts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/funnel.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/exporting.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/data.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/drilldown.js') }}"></script>

@if (Session::has('message'))
	<div class="admin_sucess_common">
		<div class="admin_sucess">
			<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>{{ Session::get('message') }}
			</div>
		</div>
	</div>
@endif

<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-home"></i>
		</div>
		<div class="media-body">
			<ul class="breadcrumb">
				<li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
				<li>@lang('messages.Dashboard')</li>
			</ul>
			<h4>@lang('messages.Dashboard')</h4>
		</div>
	</div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">
	<?php if(has_permission('admin/users/index')) { ?>
		<a href="<?php echo URL::to("admin/users/index");?>">
			<div class="col-md-3">
				<div class="panel panel-primary  noborder">
					<div class="panel-heading noborder">
						<div class="panel-btns">
						</div><!-- panel-btns -->
						<div class="panel-icon"><i class="fa fa-users"></i></div>
						<div class="media-body">
							<h5 class="md-title nomargin">@lang('messages.Users')</h5>
							<h1 class="mt5"><?php echo count(getUserList(1)); ?></h1>
						</div><!-- media-body -->
					</div><!-- panel-body -->
				</div><!-- panel -->
			</div><!-- col-md-4 -->
		</a>
	<?php } ?>

	<?php if(has_permission('vendors/vendors')) { ?>
		<a href="<?php echo URL::to("vendors/vendors");?>">
			<div class="col-md-3">
				<div class="panel panel-dark panel_orange noborder">
					<div class="panel-heading noborder">
						<div class="panel-btns">
						</div><!-- panel-btns -->
						<div class="panel-icon"><i class="fa fa-rocket"></i></div>
						<div class="media-body">
							<h5 class="md-title nomargin">@lang('messages.Vendors')</h5>
							<h1 class="mt5"><?php echo $vendors_period_count[0]->total_count; ?></h1>
						</div><!-- media-body -->
					</div><!-- panel-body -->
				</div><!-- panel -->
			</div><!-- col-md-4 -->
		</a>
	<?php } ?>

	<?php	/* if(has_permission('vendors/outlets')) { */	?>
	<?php 	/*	<a href="<?php echo URL::to("vendors/outlets"); */	?><?php		/*	">
			<div class="col-md-3">
				<div class="panel panel-dark panel_orange noborder">
					<div class="panel-heading noborder">
						<div class="panel-btns">
						</div><!-- panel-btns -->
						<div class="panel-icon"><i class="fa fa-building"></i></div>
						<div class="media-body">
							<h5 class="md-title nomargin">@lang('messages.Outlets')</h5>
							<h1 class="mt5"><?php echo count($outlets); */	?><?php	/*	</h1>
						</div><!-- media-body -->
					</div><!-- panel-body -->
				</div><!-- panel -->
			</div><!-- col-md-4 -->
		</a>
		*/?>
	<?php	/* } */	?>

	<?php if(has_permission('admin/coupons')) { ?>
		<a href="<?php echo URL::to("admin/coupons");?>">
			<div class="col-md-3">
				<div class="panel panel-dark panel_orange noborder">
					<div class="panel-heading noborder">
						<div class="panel-btns">
						</div><!-- panel-btns -->
						<div class="panel-icon"><i class="fa fa-ticket"></i></div>
						<div class="media-body">
							<h5 class="md-title nomargin">@lang('messages.Coupons')</h5>
							<h1 class="mt5"><?php echo count($coupons); ?></h1>
						</div><!-- media-body -->
					</div><!-- panel-body -->
				</div><!-- panel -->
			</div><!-- col-md-4 -->
		</a>
	<?php } ?>

	<?php if(has_permission('admin/subscribers')) { ?>
		<a href="<?php echo URL::to("admin/subscribers");?>">
			<div class="col-md-3">
				<div class="panel panel-dark panel_orange noborder">
					<div class="panel-heading noborder">
						<div class="panel-btns">
						</div><!-- panel-btns -->
						<div class="panel-icon"><i class="fa fa-user"></i></div>
						<div class="media-body">
							<h5 class="md-title nomargin">@lang('messages.Subscribers')</h5>
							<h1 class="mt5"><?php echo count($newsletter_subscribers); ?></h1>
						</div><!-- media-body -->
					</div><!-- panel-body -->
				</div><!-- panel -->
			</div><!-- col-md-4 -->
		</a>
	<?php } ?>
	<div class="admin_dasbord_home">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>@lang('messages.Detail')</th>
					<th>@lang('messages.Today')</th>
					<th>@lang('messages.This week')</th>
					<th>@lang('messages.This month')</th>
					<th>@lang('messages.This Year')</th>
					<th>@lang('messages.Total')</th>
				</tr>
			</thead>
			<tbody>
				<?php if(has_permission('admin/users/index')) { ?>
					<tr>
						<td>@lang('messages.Users')</td>
						<td><h3><?php echo $users_period_count[0]->day_count; ?></h3></td>
						<td><h3><?php echo $users_period_count[0]->week_count; ?></h3></td>
						<td><h3><?php echo $users_period_count[0]->month_count; ?></h3></td>
						<td><h3><?php echo $users_period_count[0]->year_count; ?></h3></td>
						<td><h3><?php echo $users_period_count[0]->total_count; ?></h3></td>
					</tr>
				<?php } ?>
				<?php if(has_permission('vendors/vendors')) { ?>
					<tr>
						<td>@lang('messages.Vendors')</td>
						<td><h3><?php echo $vendors_period_count[0]->day_count; ?></h3></td>
						<td><h3><?php echo $vendors_period_count[0]->week_count; ?></h3></td>
						<td><h3><?php echo $vendors_period_count[0]->month_count; ?></h3></td>
						<td><h3><?php echo $vendors_period_count[0]->year_count; ?></h3></td>
						<td><h3><?php echo $vendors_period_count[0]->total_count; ?></h3></td>
					</tr>
				<?php } ?>
				<?php if(has_permission('admin/newsletter')) { ?>
					<tr>
						<td>@lang('messages.Newsletter')</td>
						<td><h3><?php echo $newsletter_subscribers_period_count[0]->day_count; ?></h3></td>
						<td><h3><?php echo $newsletter_subscribers_period_count[0]->week_count; ?></h3></td>
						<td><h3><?php echo $newsletter_subscribers_period_count[0]->month_count; ?></h3></td>
						<td><h3><?php echo $newsletter_subscribers_period_count[0]->year_count; ?></h3></td>
						<td><h3><?php echo $newsletter_subscribers_period_count[0]->total_count; ?></h3></td>
					</tr>
				<?php } ?>
				<?php if(has_permission('admin/blog')) { ?>
					<tr>
						<td>@lang('messages.Blogs')</td>
						<td><h3><?php echo $blogs_count[0]->day_count; ?></h3></td>
						<td><h3><?php echo $blogs_count[0]->week_count; ?></h3></td>
						<td><h3><?php echo $blogs_count[0]->month_count; ?></h3></td>
						<td><h3><?php echo $blogs_count[0]->year_count; ?></h3></td>
						<td><h3><?php echo $blogs_count[0]->total_count; ?></h3></td>
					</tr>
				<?php } ?>
				<?php if(has_permission('admin/reviews')) { ?>
					<tr>
						<td>@lang('messages.Reviews')</td>
						<td><h3><?php echo $outlet_reviews_query[0]->day_count; ?></h3></td>
						<td><h3><?php echo $outlet_reviews_query[0]->week_count; ?></h3></td>
						<td><h3><?php echo $outlet_reviews_query[0]->month_count; ?></h3></td>
						<td><h3><?php echo $outlet_reviews_query[0]->year_count; ?></h3></td>
						<td><h3><?php echo $outlet_reviews_query[0]->total_count; ?></h3></td>
					</tr>
				<?php } ?>
			</tbody> 
		</table>
	</div>
</div><!-- row -->
@endsection
