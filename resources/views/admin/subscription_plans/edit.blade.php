@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script> 
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-timepicker.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-timepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" /> 
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>

<div class="row">
    <div class="col-md-12 ">
        <!-- Nav tabs -->
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-home"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
                        <li>@lang('messages.Subscription Packages')</li>
                    </ul>
                    <h4>@lang('messages.Edit Subscription Packages')</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><?php echo trans('messages.'.$error); ?> </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <ul class="nav nav-tabs"></ul>
            {!!Form::open(array('url' => ['update_subscription_package',$data[0]->id], 'method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'subscription_package', 'files' => true));!!} 
                <div class="tab-content mb30">
                    <div class="tab-pane active" id="home3">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Plan Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="plan_name"  placeholder="@lang('messages.Plan Name')" class="form-control" value="{!! $data[0]->subscription_plan_name !!}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Description') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                            	<textarea name="description" maxlength="56" placeholder="@lang('messages.Description')" class="form-control">{!! $data[0]->description !!}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Price')<span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="price"  value="{!! $data[0]->subscription_plan_price !!}" placeholder="@lang('messages.Price')">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Total Properties')<span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="properties"  value="{!! $data[0]->total_properties !!}" placeholder="@lang('messages.Total Properties')">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Rooms Per Property')<span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="rooms"  value="{!! $data[0]->total_rooms !!}" placeholder="@lang('messages.Rooms Per Property')">
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.User Limit')<span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="user_limit"  value="{!! $data[0]->total_users !!}" placeholder="@lang('messages.User Limit')">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Discount Price') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="discount_price" class="form-control"  value="{{ $data[0]->subscription_discount_price }}" placeholder="@lang('messages.Discount Price')">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Duration(In Days)') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="duration" class="form-control"  value="{{ $data[0]->subscription_plan_duration }}" placeholder="@lang('messages.Duration')">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Packages') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                            		<?php 
                                 		foreach($packages as $value){ 
                                 			if(!in_array($value->id,$old_packages)){
                                 			 ?>
	                                        	<input type="checkbox" id="packages" name="packages[]" value="{{ $value->id}}" placeholder="@lang('messages.Duration')">
	                                        	<span class="package_check">{{ $value->package_name }}</span></br>
                                      <?php } else { ?>
                                      			<input type="checkbox" id="packages" name="packages[]" value="{{ $value->id}}" checked placeholder="@lang('messages.Duration')">
	                                        	<span class="package_check">{{ $value->package_name }}</span></br>
                                      	<?php }}
                                     ?>  
                            </div>
                        </div>
                        <div class="form-group">
                                <label  class="col-sm-2 control-label">@lang('messages.Status')<span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                                    <?php $checked = ""; 
                                          if($data[0]->subscription_plan_status==1){
                                                $checked ="checked=checked";
                                          }
                                    ?>
                                    <input type="checkbox" class="toggle" name="status" data-size="small" <?php echo $checked;?> data-on-text="@lang('messages.Yes')" data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
                                </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary mr5" title="Update">@lang('messages.Update')</button>
                        <button type="reset" title="Cancel" class="btn btn-default" onclick="window.location='{{ url('admin/subscription_packages') }}'">@lang('messages.Cancel')</button>
                    </div>
                </div>
            {!!Form::close();!!} 
        </div>
    </div>
</div>

<script> 
    $(document).ready(function(){
      	$('#subscription_package').on('submit',function(e){
      		//e.preventDefault();
      		var length = $("[name='packages[]']:checked").length;
      		if(length > 0){
      			$(this).form().submit();
      		}else{

      		}
      	});
    });
</script>
@endsection
