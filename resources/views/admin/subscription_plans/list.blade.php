@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/buttons.html5.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
			<li>@lang('messages.Subscription Packages')</li>
		</ul>
		<h4>@lang('messages.Subscription Packages')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->
	<!-- will be used to show any messages -->
	@if (Session::has('message'))
		<div class="admin_sucess_common">
	<div class="admin_sucess">
		<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div></div></div>
	@endif
<div class="contentpanel">

 <table id="vendor-table" class="table table-striped table-bordered responsive">
    <thead>
        <tr class="headings">
			
            <th>@lang('messages.S.no')</th>
            <th>@lang('messages.Plan Name')</th> 
			<th>@lang('messages.Price')</th> 
			<th>@lang('messages.Properties')</th> 
			<th>@lang('messages.Rooms Per Property')</th>
			<th>@lang('messages.User Limit')</th> 
            <th>@lang('messages.Duration')</th> 
            <th>@lang('messages.Created date')</th>
			<th>@lang('messages.Status')</th> 
            <th>@lang('messages.Actions')</th>
        </tr>
    </thead>
</table>
</div>

<script>
$(function() {
    $('#vendor-table').DataTable({
		dom: 'Blfrtip',
		buttons: [
		],
        processing: true,
        serverSide: false,
		responsive: true,
		autoWidth:false,
        ajax: '{!! route('anyajaxsubscriptionplans.data') !!}',
        "order": [],
		"columnDefs": [ {
		  "targets"  : 'no-sort',
		  "orderable": false,
		}],
        columns: [
			{ data: 'id', name: 'id',orderable: false },
			{ data: 'subscription_plan_name', name: 'subscription_plan_name',searchable:true },
			{ data: 'subscription_plan_price', name: 'subscription_plan_price',searchable:true },
			{ data: 'total_properties', name: 'total_properties',searchable:true },
			{ data: 'total_rooms', name: 'total_rooms',searchable:true },
			{ data: 'total_users', name: 'total_users',searchable:true },
			{ data: 'subscription_plan_duration', name: 'subscription_plan_duration',orderable:false  },
            { data: 'created_at', name: 'created_at' },
			{ data: 'subscription_plan_status', name: 'subscription_plan_status' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });
});
</script>
@endsection
