@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script> 
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-timepicker.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-timepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" /> 
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>
<!-- country code css -->
<link href="{{ URL::asset('/assets/front/otel2go/js/intlTelInput.css') }}" media="all" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-md-12 ">
        <!-- Nav tabs -->
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-home"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
                        <li>@lang('messages.Customers')</li>
                    </ul>
                    <h4>@lang('messages.Edit Customer')</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><?php echo trans('messages.'.$error); ?> </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <ul class="nav nav-tabs"></ul>
            {!!Form::open(array('url' => ['update_customers',$data->id], 'method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'update_customer_form', 'files' => true));!!} 
                <div class="tab-content mb30">
                    <div class="tab-pane active" id="home3">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.First Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="first_name"  placeholder="@lang('messages.First Name')" class="form-control" value="{!! $data->firstname !!}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Last Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="last_name"  placeholder="@lang('messages.Last Name')" class="form-control" value="{!! $data->lastname !!}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Email') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="email"  placeholder="@lang('messages.Email')" class="form-control" value="{!! $data->email !!}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Password') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="password" name="password" maxlength="50" placeholder="@lang('messages.Password')" class="form-control" value="" />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                </div>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Date of birth')<span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="date_of_birth" autocomplete="off" value="{!! $data->date_of_birth !!}" placeholder="mm/dd/yyyy" id="datepicker">
                                    <span class="input-group-addon datepicker-trigger"><i class="glyphicon glyphicon-calendar" id="dob"></i></span>
                                </div>
                            </div>
                        </div>
                       <div class="form-group">
                                <label class="col-sm-2 control-label">Mobile Number<span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" name="mobile" id ="mobile" value="" placeholder="Mobile Number" class="form-control">
                                    <input id="phone_hidden" type="hidden" name="country_code" value="0">
                                </div>
                                    <input type="hidden" name="country_short" id="country_short" 
                                value="{{ $data->country_code }}{{ $data->mobile_number }}">
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Gender') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select name="gender" class="form-control">
                                    <option value="" >@lang('messages.Select Gender')</option>
                                    <option value="M" <?php if("M"==$data->gender){ echo "selected=selected"; } ?> >@lang('messages.Male')</option>
                                    <option value="F" <?php if("F"==$data->gender){ echo "selected=selected"; } ?>>@lang('messages.Female')</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Login type') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select name="login_type" class="form-control">
                                    <option value="" >@lang('messages.Select Login type')</option>
                                    <option value="1" <?php if("1"==$data->login_type){ echo "selected=selected"; } ?> >@lang('messages.Web')</option>
                                    <option value="2" <?php if("2"==$data->login_type){ echo "selected=selected"; } ?>>@lang('messages.Mobile')</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Address') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <textarea name="address"  placeholder="@lang('messages.Address')" class="form-control" maxlength="255"/>{{ $data->address }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Image')</label>
                            <div class="col-sm-6">
                                    @if(isset($data->image) && $data->image !="")
                                      <img src="<?php echo url('/assets/admin/base/images/customers/'.$data->image.'');?>" name="image" class="img-thumbnail">
                                    @endif
                                <input type="file" name="image" />
                            </div>
                        </div>
                        <div class="form-group">
                                <label  class="col-sm-2 control-label">@lang('messages.Status')<span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                                    <?php $checked = ""; 
                                          if($data->status==1){
                                                $checked ="checked=checked";
                                          }
                                    ?>
                                    <input type="checkbox" class="toggle" name="status" data-size="small" <?php echo $checked;?> data-on-text="@lang('messages.Yes')" data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
                                </div>
                        </div>
                        <div class="form-group">
                                <label  class="col-sm-2 control-label">@lang('messages.Is Verified')<span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                                    <?php $checked = ""; 
                                          if($data->is_verified == 1){
                                                $checked ="checked=checked";
                                          }
                                    ?>
                                    <input type="checkbox" class="toggle" name="is_verified" data-size="small" <?php echo $checked;?> data-on-text="@lang('messages.Yes')" data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
                                </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary mr5" title="Save">@lang('messages.Update')</button>
                        <button type="reset" title="Cancel" class="btn btn-default" onclick="window.location='{{ url('admin/customers') }}'">@lang('messages.Cancel')</button>
                    </div>
                </div>
            {!!Form::close();!!} 
        </div>
    </div>
</div>
<!-- country code js-->
<script type="text/javascript" src="{{ URL::asset('assets/front/otel2go/js/intlTelInput.min.js') }}"></script>
<script> 
    $(window).load(function(){
        $('#datepicker').datepicker({
            yearRange: '<?php echo date("Y") - 100; ?>:<?php echo date("Y"); ?>',
            maxDate: new Date(),
            changeMonth: true,
            changeYear: true
        });
        $(".datepicker-trigger").on("click", function() {
            $("#datepicker").datepicker("show");
        });
    });

    $("#update_customer_form").submit(function() {
           var countryData = $("#mobile").intlTelInput("getSelectedCountryData");
           $("#phone_hidden").val('+'+countryData.dialCode);
    });

     $("#mobile").intlTelInput({
           initialCountry: "my",
           nationalMode: false,
           separateDialCode: true,
           allowDropdown: true,
           utilsScript: '{{ URL::asset("/assets/admin/base/js/utils.js") }}' 
           // just for formatting/placeholders etc
       });

    $( document ).ready(function() {
         var country_short = $('#country_short').val();
         $("#mobile").intlTelInput("setNumber", country_short);
           setTimeout(function() {
                   $('.alert').fadeOut('fast');
           }, 7500);
           
           $('.BDC_CaptchaImageDiv a').addClass('test');
           $('.test').css('visibility', 'hidden');
           $('#ContactCaptcha_SoundIcon').css('visibility', 'hidden');
       }); 
</script>
@endsection
