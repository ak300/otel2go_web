@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}">
</script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/buttons.html5.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
			<li>@lang('messages.Customer Referrals')</li>
		</ul>
		<h4>@lang('messages.Customer Referrals')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->
	<!-- will be used to show any messages -->
	@if (Session::has('message'))
		<div class="admin_sucess_common">
	<div class="admin_sucess">
		<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div></div></div>
	@endif
<div class="contentpanel">
 <table id="customer_referrals" class="table table-striped table-bordered responsive">
    <thead>
        <tr class="headings">
            <th>@lang('messages.S.no')</th>
            <th>@lang('messages.Customer Name')</th> 
            <th>@lang('messages.Referrer Name')</th> 
            <th>@lang('messages.Referral Amount')</th>
            <th>@lang('messages.Referral Code')</th>
            <th>@lang('messages.Created date')</th>
        </tr>
    </thead>
</table>
</div>

<script>
$(function() {
    $('#customer_referrals').DataTable({
		dom: 'Blfrtip',
		buttons: [
		],
        processing: true,
        serverSide: false,
		responsive: true,
		autoWidth:false,
        ajax: '{!! route('anyajaxreferralcustomers.data') !!}',
        "order": [],
		"columnDefs": [ {
		  "targets"  : 'no-sort',
		  "orderable": false,
		}],
        columns: [
			{ data: 'id', name: 'id',orderable: false },
			{ data: 'firstname', name: 'firstname',searchable:true },
			{ data: 'referrer_name', name: 'referrer_name',searchable:true },
			{ data: 'referral_amount', name: 'referral_amount',searchable:true },
			{ data: 'referral_code', name: 'referral_code',searchable:true },
            { data: 'created_at', name: 'created_at' },
        ],
    });
});
</script>
@endsection
