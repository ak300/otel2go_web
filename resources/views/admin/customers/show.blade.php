@extends('layouts.admin')
@section('content')
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/jquery-ui-1.10.3.css') }}" media="all" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script> 
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" />
	<script  src="<?php echo URL::asset('assets/admin/base/plugins/rateit/src/jquery.rateit.js');?>"></script>
<link href="<?php echo URL::asset('assets/admin/base/plugins/rateit/src/rateit.css');?>" rel="stylesheet"> 
<div class="row">	
	<div class="col-md-12 ">
		<!-- Nav tabs -->
		<div class="pageheader">
			<div class="media">
				<div class="pageicon pull-left">
					<i class="fa fa-home"></i>
				</div>
				<div class="media-body">
					<ul class="breadcrumb">
						<li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
						<li>@lang('messages.Customers')</li>
					</ul>
					<h4>@lang('messages.View Customer Details')</h4>
				</div>
			</div><!-- media -->
		</div><!-- pageheader -->
		<div class="contentpanel">
		    <div class="row">
		        <div class="col-md-12">
		            <div class="grid simple">
		                    <ul class="nav nav-justified nav-wizard nav-pills">
		                        <li class="active"><a href="#customer_info" class="customer_info" data-toggle="tab" aria-expanded="false"><strong>Customer Details</strong></a></li>
		                        <li class=""><a href="#payments_info" class="payments_info" data-toggle="tab" aria-expanded="true"><strong>Booking Details</strong></a></li>
		                    </ul>
		                    
		                    <div class="tab-content tab-content-simple mb30 no-padding">
		                        <div class="tab-pane active" id="customer_info" style="display: block;">
		                            <legend>Customer Details</legend>
		                            <div class="form-group">
										<label for="title" class="col-sm-3 control-label"> @lang('messages.Customer Name') :</label>
										<div class="col-sm-3">
											{{ $data['firstname'] }} {{ $data['lastname'] }}
										</div>
									</div>
									<div class="form-group">
										<label for="title" class="col-sm-3 control-label"> @lang('messages.Gender') :</label>
										<div class="col-sm-3">
											@if($data['gender'] == 'M')
												@lang('messages.Male')
											@else
												@lang('messages.FeMale')
											@endif
										</div>
									</div>
									<div class="form-group">
										<label for="title" class="col-sm-3 control-label"> @lang('messages.Date of birth') :</label>
										<div class="col-sm-3">
											{{ $data['date_of_birth'] }}
										</div>
									</div>
									<div class="form-group">
										<label for="content" class="col-sm-3 control-label"> @lang('messages.Mobile Number') :</label>
										<div class="col-sm-6">
											{{ $data['country_code'] }}-{{ $data['mobile_number'] }}
										</div>
									</div>
									<div class="form-group">
										<label for="title" class="col-sm-3 control-label"> @lang('messages.Email') :</label>
										<div class="col-sm-3">
											{{ $data['email'] }}
										</div>
									</div>
									<div class="form-group">
										<label for="content" class="col-sm-3 control-label"> @lang('messages.Address') :</label>
										<div class="col-sm-6">
											{{ $data['address'] }}
										</div>
									</div>
									<div class="form-group">
										<label for="content" class="col-sm-3 control-label"> @lang('messages.Created Date') :</label>
										<div class="col-sm-6">
											{{ $data['created_at'] }}
										</div>
									</div>
									<div class="form-group">
										<label for="content" class="col-sm-3 control-label"> @lang('messages.Updated Date') :</label>
										<div class="col-sm-6">
											{{ $data['updated_at'] }}
										</div>
									</div>
									<div class="form-group">
										<label for="content" class="col-sm-3 control-label"> @lang('messages.Active Status') :</label>
										<div class="col-sm-6">
											<?php
												$checked ="";
												if($data['status'] == 1){
													$checked = "checked=checked";
												}
											?>
											<input type="checkbox" class="toggle" name="status" data-size="small" data-on-text="@lang('messages.Yes')" readonly <?php echo $checked;?>data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
										</div>
									</div>
									<div class="form-group">
										<label for="content" class="col-sm-3 control-label"> @lang('messages.Verified Status') :</label>
										<div class="col-sm-6">
											<?php
												$checked ="";
												if($data['is_verified'] == 1){
													$checked = "checked=checked";
												}
											?>
											<input type="checkbox" class="toggle" name="status" data-size="small" data-on-text="@lang('messages.Yes')" readonly <?php echo $checked;?>data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
										</div>
									</div>
									@if($data['image'] != "")
										<div class="form-group">
											<label class="col-sm-3 control-label">@lang('messages.Image') :</label>
											<div class="col-sm-6">
												<?php if($data['image']){ ?>
												<img src="<?php echo url('/assets/admin/base/images/customers/'.$data['image'].'');?>" name="image" class="img-thumbnail">
												<?php } ?>
											</div>	
										</div>
									@endif
		                        </div>
		                        <div class="tab-pane" id="payments_info" style="display: none;">
		                            <legend>Booking Details</legend>
		                            @if (count($bookings) > 0 )
		                            <table id="basicTable" class="table table-striped table-bordered responsive">
		                                <thead>
		                                    <tr>
		                                        <th>@lang('messages.S.no')</th> 
		                                        <th>@lang('messages.Customer Name')</th>
		                                        <th>@lang('messages.Property Name')</th>
		                                        <th>@lang('messages.Paid Amount')</th>
		                                        <th>@lang('messages.Payment Type')</th>
		                                        <th>@lang('messages.Description')</th>
		                                        <th>@lang('messages.Created Date')</th>            
		                                    </tr>
		                                </thead>                            
		                                <tbody>
		                                @foreach($bookings as $i => $book)
		                                <tr>
		                                    <td>{{ $i+1 }}</td>
		                                    <td>  {!! $book->firstname !!} {!! $book->lastname !!}</td>
		                                    <td>{!! $book->outlet_name !!}</td>
		                                    <td>{!! $book->paid_amount !!}</td>
		                                    <td> 
		                                    	@if($book->paid_type == 1)
		                                    		@lang('messages.Cash')
		                                    	@endif
		                                    </td>
		                                    <td>{!! $book->description !!}</td>
		                                    <td>{!! $book->created_date !!}</td>
		                                </tr>
		                                <?php $i++; ?>
		                                        @endforeach
		                                    </tbody>
		                            </table>
		                            @else
		                                <div class="no_data">@lang('messages.No records found.')</div>
		                            @endif 
		                         </div>
		                        <div class="panel-footer">
		                            <button type="reset" class="btn btn-default" onclick="window.location='{{ url('admin/customers') }}'" title="Cancel">Back</button>
		                        </div>
		                        <div class="form-group Loading_Img" style="display:none;">
		                            <div class="col-sm-4">
		                                <i class="fa fa-spinner fa-spin fa-3x"></i><strong style="margin-left: 3px;">Processing...</strong>
		                            </div>
		                        </div>
		                </div>
		            </div>
		        </div>
		    </div>  
		</div>
	</div>
</div>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script>
<script>
   $(document).ready(function(){
        $(".customer_info").on("click", function(){
            $('.tab_info').val('customer_info');
            $('#customer_info').show();
            $('#payments_info').hide();
        });
        $(".payments_info").on("click", function(){
            $('.tab_info').val('payments_info');
            $('#payments_info').show();            
            $('#customer_info').hide();
        });   
    }); 
</script>
@endsection


