<div class="leftpanel">
	<div class="media profile-left">
		<?php $vendor_id = Session::get('vendor_image'); ?>
		<a class="pull-left profile-thumb">
			<img src="<?php echo url('assets/admin/base/images/vendors/logos/'.$vendor_id.'?'.time()); ?>" class="img-circle"> 
		</a>
		<div class="media-body">
			<h4 class="media-heading"><?php echo ucfirst(Session::get('user_name'));?></h4>
			<small class="text-muted"> <a href="{{ url('vendors/editprofile') }}" title="Edit Profile">@lang('messages.Edit Profile')</a> </small>
		</div>
	</div><!-- media -->
	<ul class="nav nav-pills nav-stacked">
		
		<?php if(has_staff_permission('vendors/orders/index')) { ?>
		<li class="parent {{ Request::is('vendors/orders/index*') ? 'active' : '' || Request::is('vendors/dashboard*') ? 'active' : '' || Request::is('vendor/showbooking*') ? 'active' : ''}}"><a  href="javascript:;"><i class="fa fa-home"></i> <span>@lang('messages.Booking Management')</span></a>
				<ul class="children">
					<?php if(has_staff_permission('vendors/orders/index')) { ?>
					<li class="{{ Request::is('vendors/orders/index*') ? 'active' : '' }}" ><a  href="{{ URL::to('vendors/orders/index') }}">@lang('messages.Booking Management')</a></li>
					<?php } ?>
					<li class="{{ Request::is('vendors/dashboard*') ? 'active' : '' || Request::is('vendor/showbooking*') ? 'active' : '' }}" ><a href="{{ URL::to('vendors/dashboard') }}"><span>@lang('messages.Booking Calendar')</span></a></li>					
				</ul>
		</li>
		<?php } ?>

		
		<li class="parent {{ Request::is('vendor/amenities*') ? 'active' : '' || Request::is('vendor/accomodation*') ? 'active' : ''}}"><a href="{{ URL::to('vendors/staffs') }}"><i class="fa  fa-users"></i> <span>@lang('messages.Common')</span></a>
		
			<ul class="children">	
				
				<?php if(has_staff_permission('vendor/amenities')) { ?>
					<li class="{{ Request::is('vendor/amenities*') ? 'active' : '' || Request::is('vendor/amenities/create') ? 'active' : '' || Request::is('vendor/amenities/edit/*') ? 'active' : '' }}"><a href="{{ URL::to('vendor/amenities') }}"><span>@lang('messages.Amenities')</span></a></li>
				<?php } ?>
				
				<?php if(has_staff_permission('vendor/accomodation')) { ?>
					<li class="{{ Request::is('vendor/accomodation*') ? 'active' : '' || Request::is('vendor/accomodation/create') ? 'active' : '' || Request::is('vendor/accomodation/edit/*') ? 'active' : '' }}"><a href="{{ URL::to('vendor/accomodation') }}"><span>@lang('messages.Accomodation')</span></a></li>
				<?php } ?>
			</ul>
		</li>	
		

	

        <?php if(has_staff_permission('reports/order') || has_staff_permission('vendors/reports_analysis') || has_staff_permission('reports/user') || has_staff_permission('reports/vendors')){ ?>
            <li class="parent {{ (Request::is('reports/*')) ? 'active' : '' || Request::is('vendors/reports_analysis') ? 'active' : '' || Request::is('vendor/reports/order') ? 'active' : '' || Request::is('vendor/room/reports') ? 'active' : '' || Request::is('vendor/reports/room') ? 'active' : '' || Request::is('vendor/outlet/reports') ? 'active' : '' || Request::is('vendor/staffs/reports') ? 'active' : '' }}"><a  href="javascript:;"><i class="fa fa-bar-chart-o"></i> <span>@lang('messages.Reports & Analytics')</span></a>
                <ul class="children">

					<?php if(has_staff_permission('vendors/reports_analysis')) { ?>
						<li class="{{ Request::is('vendors/reports_analysis*') ? 'active' : '' }}"><a href="{{ URL::to('vendors/reports_analysis') }}"><span>@lang('messages.Reports & Analysis')</span></a></li>
					<?php } ?>               	
                    <?php if(has_staff_permission('vendor/reports/order')){ ?>
                        <li class="{{ Request::is('vendor/reports/order') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/reports/order') }}">@lang('messages.Transactions')</a></li>
                    <?php } ?>
                    <?php if(has_staff_permission('vendor/reports/room')){ ?>
                        <li style="display: none;" class="{{ Request::is('vendor/reports/room') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/reports/room') }}">@lang('messages.Sales')</a></li>	
                    <?php } ?>
                    <?php if(has_staff_permission('vendor/room/reports')){ ?>
                        <li class="{{ Request::is('vendor/room/reports') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/room/reports') }}">@lang('messages.Rooms')</a></li>	
                    <?php } ?> 
                    <?php if(has_staff_permission('vendor/staffs/reports')){ ?>
                        <li class="{{ Request::is('vendor/staffs/reports') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/staffs/reports') }}">@lang('messages.Staffs')</a></li>	
                    <?php } ?>                       
                    <?php if(has_staff_permission('reports/coupons')){ ?>
                        <li style="display: none;" class="{{ Request::is('reports/coupons') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/coupons') }}">@lang('messages.Coupons')</a></li>
                    <?php } ?>
                    <?php if(has_staff_permission('vendor/outlet/reports')){ ?>
                        <li class="{{ Request::is('vendor/outlet/reports') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/outlet/reports') }}">@lang('messages.Property')</a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

		
		<?php if(has_staff_permission('vendor/outlets'))  { ?>
			<li class="parent {{ Request::is('vendor/outlets*') ? 'active' : '' || Request::is('vendor/create_outlet') ? 'active' : '' || Request::is('vendor/edit_outlet/*') ? 'active' : '' || Request::is('vendors/outlets_gallery*') ? 'active' : '' || Request::is('vendor/outlet_details*') ? 'active' : '' }}"><a href="{{ URL::to('vendor/outlets') }}"><i class="fa fa-building"></i> <span>@lang('messages.Outlets')</span></a>

				<ul class="children">
		 			<?php if(has_staff_permission('vendor/outlets')) { ?>
						<li class="{{ Request::is('vendor/outlets*') ? 'active' : '' || Request::is('vendor/edit_outlet*') ? 'active' : '' || Request::is('vendor/create_outlet*') ? 'active' : '' || Request::is('vendor/outlet_details*') ? 'active' : '' }}" ><a href="{{ URL::to('vendor/outlets') }}">@lang('messages.Property')</a></li>
					<?php } ?>	
		 			<?php if(has_staff_permission('vendors/outlets_gallery')) { ?>
			 			<li class="{{ Request::is('vendors/outlets_gallery*') ? 'active' : '' }}" ><a  href="{{ URL::to('vendors/outlets_gallery') }}">@lang('messages.Gallery')</a></li>
					<?php } ?>	 
				</ul>
			</li>
		<?php } ?>

		<?php if(has_staff_permission('vendor/coupons')) { ?>
		<li class="{{ Request::is('vendor/coupons*') ? 'active' : '' || Request::is('vendor/coupons/create') ? 'active' : '' || Request::is('vendor/coupons/edit') ? 'active' : '' }}"><a href="{{ URL::to('vendor/coupons') }}"><i class="fa  fa-tags"></i> <span>@lang('messages.Coupons')</span></a></li>
		<?php } ?>

		<?php if(has_staff_permission('vendor/outletmanagers')) { ?>
		<li class="{{ Request::is('vendor/outletmanagers*') ? 'active' : '' || Request::is('vendor/create_outlet_managers') ? 'active' : '' || Request::is('vendor/edit_outlet_manager*') ? 'active' : '' }}"><a href="{{ URL::to('vendor/outletmanagers') }}"><i class="fa  fa-users"></i> <span>@lang('messages.Staffs')</span></a></li>
		<?php } ?>

		<?php if(has_staff_permission('vendor/room_type')) { ?>
		<li class="parent {{ Request::is('vendor/room_type*') ? 'active' : '' || Request::is('vendor/rooms*') ? 'active' : '' }}"><a  href="javascript:;"><i class="fa fa-building"></i> <span>@lang('messages.Rooms Management')</span></a>
				<ul class="children">
					<?php if(has_staff_permission('vendor/room_type')) { ?>
					<li class="{{ Request::is('vendor/room_type*') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/room_type') }}">@lang('messages.Room Types')</a></li>
					<?php } ?>

					<?php if(has_staff_permission('vendor/rooms')) { ?>
					<li class="{{ Request::is('vendor/rooms*') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/rooms') }}">@lang('messages.Rooms')</a></li>
					<?php } ?>
				</ul>
		</li>
		<?php } ?>

		<?php /*<li class="{{ Request::is('vendor/permission*') ? 'active' : '' }}" ><a href="{{ URL::to('vendor/permission') }}"><i class="fa fa-list-alt"></i> <span>@lang('messages.Roles')</span></a></li> */?>

		<?php if(has_staff_permission('vendor/house_keeping')) { ?>
		<li class="parent {{ Request::is('vendor/house_keeping*') ? 'active' : '' || Request::is('vendor/housekeeping_tasks*') ? 'active' : ''}}"><a  href="javascript:;"><i class="fa fa-users"></i> <span>@lang('messages.Housekeeping Staffs')</span></a>
				<ul class="children">
					<?php if(has_staff_permission('vendor/house_keeping')) { ?>
					<li class="{{ Request::is('vendor/house_keeping*') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/house_keeping') }}">@lang('messages.Housekeeping Staffs')</a></li>
					<?php } ?>
					<?php if(has_staff_permission('vendor/housekeeping_tasks')) { ?>
					<li class="{{ Request::is('vendor/housekeeping_tasks*') ? 'active' : ''}}" ><a  href="{{ URL::to('vendor/housekeeping_tasks') }}">@lang('messages.Housekeeping Tasks')</a></li>
					<?php } ?>
				</ul>
		</li>
		<?php } ?>
	
		<?php if(has_staff_permission('vendors/staffs')) { ?>
				<li class="{{ Request::is('vendors/staffs*') ? 'active' : '' || Request::is('vendors/edit_staffs*') ? 'active' : '' || Request::is('vendors/create_staffs*') ? 'active' : ''  }}" ><a  href="{{ URL::to('vendors/staffs') }}"><i class="fa fa-user"></i> @lang('messages.Users')</a></li>
		<?php } ?>

		<?php if(has_staff_permission('vendor/permission')) { ?>
					<li class="parent {{ Request::is('vendor/permission*') ? 'active' : '' || Request::is('vendors/permission*') ? 'active' : '' }}"><a  href="javascript:;"><i class="fa fa-desktop"></i> <span>@lang('messages.Permission')</span></a>
						<ul class="children">
							<?php if(has_staff_permission('vendor/permission')) { ?>
							<li class="{{ Request::is('vendor/permission*') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/permission') }}">@lang('messages.Roles')</a></li>
							<?php } ?>
							<?php if(has_staff_permission('vendors/permission')) { ?>
							<li class="{{ Request::is('vendors/permission*') ? 'active' : ''}}" ><a  href="{{ URL::to('vendors/permission/users') }}">@lang('messages.Role User')</a></li>
							<?php } ?>
						</ul>
					</li>
		<?php } ?>

		<li class="parent {{ Request::is('vendors/reviews*') ? 'active' : '' || Request::is('vendors/review/services') ? 'active' : '' }}"><a href="{{ URL::to('vendor/reviews') }}"><i class="glyphicon  glyphicon-star"></i> </i> <span>@lang('messages.Reviews Management')</span></a>

			<ul class="children">
				<li class="{{ Request::is('vendors/reviews*') ? 'active' : '' || Request::is('vendors/reviews/view') ? 'active' : '' }}" ><a href="{{ URL::to('vendors/reviews') }}"><span>@lang('messages.Property Reviews')</span></a></li>

				<li class="{{ Request::is('vendors/review/services') ? 'active' : '' || Request::is('vendors/reviews/services/view') ? 'active' : '' }}"><a href="{{ URL::to('vendors/review/services') }}"><span>@lang('messages.Service Reviews')</span></a></li>
			</ul>
		</li>

		<?php if(has_staff_permission('vendors/customer_feedback')) { ?>
		<li class="{{ Request::is('vendors/customer_feedback*') ? 'active' : '' }}"><a href="{{ URL::to('vendors/customer_feedback') }}"><i class="fa fa-tags"></i> <span>@lang('messages.Customer Feedback')</span></a></li>
		<?php } ?>		
		
		<li class="parent {{ Request::is('vendor/expenses*') ? 'active' : '' || Request::is('vendor/accounts') ? 'active' : '' }}"><a href="{{ URL::to('vendor/expenses') }}"><i class="fa fa-building"></i> <span>@lang('messages.Accounts Management')</span></a>

			<ul class="children">
				<li class="{{ Request::is('vendor/expenses*') ? 'active' : '' }}"><a href="{{ URL::to('vendor/expenses') }}"><span>@lang('messages.Expenses')</span></a></li>	
	 			
		 		<li class="{{ Request::is('vendor/accounts*') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/accounts') }}">@lang('messages.Accounts')</a></li>
				 
			</ul>
		</li>

		<li class="{{ Request::is('vendors/subscription_transactions*') ? 'active' : '' }}"><a href="{{ URL::to('vendors/subscription_transactions') }}"><i class="fa  fa-bell"></i> <span>@lang('messages.Subscription Transactions')</span></a></li>
		
		<li class="{{ Request::is('vendors/fund_requests*') ? 'active' : '' }}" ><a href="{{ URL::to('vendors/fund_requests') }}"><i class="fa fa-comment"> </i> <span>@lang('messages.Fund Requests')</span></a></li>
		<?php /*
		
	<?php	/*<li class="{{ Request::is('vendors/notifications*') ? 'active' : '' }}" ><a href="{{ URL::to('vendors/notifications') }}"><i class="fa fa-bell"></i> <span>@lang('messages.Notifications')</span></a></li>
		<li class="{{ Request::is('vendors/billing*') ? 'active' : '' }}" ><a href="{{ URL::to('vendors/billing') }}"><i class="fa fa-credit-card"> </i> <span>@lang('messages.Subscription')</span></a></li>*/?>
	</ul>
	<footer>
        @include('includes.vendors.footer')
    </footer>
</div><!-- leftpanel -->
