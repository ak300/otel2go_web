 <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> 
    <?php /* <title>Otel2go</title> */ ?>
   
	<link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/font.css') }}">
	 <link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/mobile_nenu.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/font/font/flaticon.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/toastr.min.css') }}"  />
    <link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/jquery-ui-slider-pips.css')}}">
    <link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/jquery-ui.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700" rel="stylesheet"> 
    <link rel="stylesheet" href="{{ URL::asset('assets/front/otel2go/css/responsive.css') }}">
    
    <!-- JS Files -->
    
    <script src= "{{ URL::asset('assets/front/otel2go/js/jquery.min.js') }}">  </script>
    <script src="{{ URL::asset('assets/front/otel2go/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ URL::asset('assets/front/otel2go/js/bootstrap.min.js') }}">  </script>
    <script src="{{ URL::asset('assets/front/otel2go/js/toastr.min.js') }}"></script>
    <script src="{{ URL::asset('assets/front/otel2go/js/jquery-ui.js') }}"></script>
    <script src="{{ URL::asset('assets/front/otel2go/js/jquery-ui-slider-pips.js') }}"></script>
    <script src="{{ URL::asset('assets/front/otel2go/js/scripts.js.js') }}"></script>
  </head>
<link rel="shortcut icon" href="<?php echo url('/assets/front/'.Session::get("general")->theme.'/images/favicon/'.Session::get("general")->favicon.''); ?>">  
  <script type="text/javascript">
    $(document).ready( function() {
        //toastr.success('Have fun storming the castle!', 'Miracle Max Says')
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    });
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119532109-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-119532109-1');
</script>