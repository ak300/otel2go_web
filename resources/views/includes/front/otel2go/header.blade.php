<?php if( Request::is('/') || Request::is('vendor-signup')) {  ?>
      <div class="header_ourt header_bussiness">
<?php  } else if(Request::is('book-now')) { ?>
      <div class="header_ourt front_header">
<?php  } else { ?>
      <div class="header_ourt header_options listing_header" style="display:none;">
<?php  }  ?>  
  <div class="container">
    <div class="row">
      <div class="col col-sm-6 col-lg col-xl">
        <div class="brand_logo">

          <!--  LANDING PAGE -->
          <?php if( Request::is('/') || Request::is('vendor-signup')) {  ?>

              <a href="{{  url('/') }}" title="Otel2go">
                   <img src="{{ URL::asset('assets/front/otel2go/images/business_logo.png') }}" title="{{ ucfirst(getAppConfig()->site_name) }}" alt="logo"/>
              </a>

          <!-- Front Book-Now Page -->

          <?php  } else if(Request::is('book-now')) { ?>

              <a href="{{  url('/book-now') }}">
              <img src="{{ URL::asset('assets/front/otel2go/images/otel2go.png') }}" alt="" title="{{ ucfirst(getAppConfig()->site_name) }}">
              </a>              

          <!-- Inner Pages -->

          <?php  } else { ?>

              <a href="{{  url('/book-now') }}" title="Otel2go">
                   <img src="<?php echo url('/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.Session::get("general")->front_logo.'?'.time()); ?>"  title="{{ ucfirst(getAppConfig()->site_name) }}" alt="front_logo">
              </a>

          <?php  }  ?>


        </div>
      </div>
      <div class="col col-sm-6 col-lg col-xl">
        <div class="navbar_right">
          <nav class="navbar navbar-expand-sm bg-light">
            <!-- Links -->
            <ul class="navbar-nav disply_hidden">

              <!--  LANDING PAGE -->
            <?php if( Request::is('/') || Request::is('vendor-signup')) {  ?>
              <li>
                <a href="{{ url('vendors/login') }}" title="@lang('messages.Login')" target="_blank">@lang('messages.Login')</a>
              </li>
              <li>
                <a href="{{ url('vendor-signup') }}"  title="@lang('messages.Signup')">@lang('messages.Signup')</a>
              </li>

              <li>
                <a href="{{ url('book-now') }}" class="book_now_bussines" title="@lang('messages.Book Now')">@lang('messages.Book Now')</a>
              </li>                              

            <?php  } else { ?>

              <?php if(Session::has('user_id')) { ?>
              <li><a title="{{Session::get('user_name')}} " id="open_drop_hed" href="{{ URL::to('/profile') }}" title="@lang('messages.Login')"><i class="glyph-icon flaticon-social"></i><?php echo ucfirst(str_limit(Session::get('user_name'), 10)); ?></a>
                <div class="after_login_drop">
                      <ul>
                          <li class="{{ Request::is('bookings*') ? 'active' : '' }}"><a href="{{url('/orders')}}" title="@lang('messages.My bookings')">@lang('messages.My bookings')</a></li>    
                          <li class="{{ Request::is('wallet') ? 'active' : '' }}"><a href="{{url('/wallet')}}" title="@lang('messages.My Wallet')">@lang('messages.My Wallet')</a></li>
                          <li class="{{ Request::is('refer_friends*') ? 'active' : '' }}"><a href="{{url('refer_friends')}}" title="@lang('messages.Refer Friends')">@lang('messages.Refer Friends')</a></li>                           
                          <li class="{{ Request::is('logout*') ? 'active' : '' }}"><a href="{{url('logout')}}" title="@lang('messages.Logout')">@lang('messages.Logout')</a></li>
                      </ul>
                </div>    
                </li>
                <?php } ?>
              <li>
                <a  href="{{ URL::to('/cms/help') }}" title="@lang('messages.Help')">Help</a>
              </li>
              <?php 
              if(!Session::has('user_id')) {?>
               <li>
                <a href="javascript:;"  title="@lang('messages.Login')" data-toggle="modal" data-target=".login">@lang('messages.Login')</a>
              </li>
              <li>
                <a href="javascript:;"  title="@lang('messages.Signup')" data-toggle="modal" data-target=".signup">@lang('messages.Signup')</a>
              </li>
              <?php } }?>
            </ul>
          </nav>
		  <div id="button-box" class="button-box">
          <button type="button" id="toggle-nav" class="toggle-nav menu_btn">MENU<span>&#9776;</span></button>
        </div>

        <div class="fad_responsive" style="display: none;"></div>

        </div>
      </div>
    </div>
	
<!-- RESPONSIVE -->	
	
	<div id="navigation" class="navigation">
          <nav id="nav">
          <ul class="navbar-nav">
              <?php if(Session::has('user_id')) { ?>
              <li><a title="{{Session::get('user_name')}} " id="open_drop_hed" href="{{ URL::to('/profile') }}" title="@lang('messages.Login')"><i class="glyph-icon flaticon-social"></i><?php echo ucfirst(str_limit(Session::get('user_name'), 10)); ?></a>
               
                      <ul>
                          <li class="{{ Request::is('bookings*') ? 'active' : '' }}"><a href="{{url('/orders')}}" title="@lang('messages.My bookings')">@lang('messages.My bookings')</a></li>    
                          <li class="{{ Request::is('wallet') ? 'active' : '' }}"><a href="{{url('/wallet')}}" title="@lang('messages.My Wallet')">@lang('messages.My Wallet')</a></li>
                          <li class="{{ Request::is('refer_friends*') ? 'active' : '' }}"><a href="{{url('refer_friends')}}" title="@lang('messages.Refer Friends')">@lang('messages.Refer Friends')</a></li>                           
                          <li class="{{ Request::is('logout*') ? 'active' : '' }}"><a href="{{url('logout')}}" title="@lang('messages.Logout')">@lang('messages.Logout')</a></li>
                      </ul>
                 
                </li>
                <?php } ?>
              <li>
                <a class="nav-link" href="{{ URL::to('/cms/help') }}" title="@lang('messages.Help')">Help</a>
              </li>
              <?php 
              if(!Session::has('user_id')) {?>
               <li>
                <a href="javascript:;" class="nav-link" title="@lang('messages.Login')" data-toggle="modal" data-target=".login">@lang('messages.Login')</a>
              </li>
              <li>
                <a href="javascript:;" class="nav-link" title="@lang('messages.Signup')" data-toggle="modal" data-target=".signup">@lang('messages.Signup')</a>
              </li>
              <li>
                <a  href="{{ URL::to('/book-now') }}" title="@lang('messages.Book Now')">Book Now</a>                
              </li>
              <?php } ?>
            </ul>
          </nav>
  </div>
        
	
	
  </div>
</div>

<script type="text/javascript">

$(document).ready(function(){


  $(".menu_btn").click(function(){

      $(".fad_responsive").toggle();

        //  html = "";
        //  html += '<div class="fad_responsive"></div>';

        //  $(".navbar_right").append(html);
      
  });

});



</script>