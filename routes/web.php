<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// Baseback
Route::group(['middleware' => ['web']], function (){
	Route::auth();
	Route::get('admin/login', 'Admin@index');
	Route::get('admin/logout', 'Admin@adminlogout');
	Route::get('admin/dashboard', 'Admin@dashboard');
	Route::get('admin/changepassword','Admin@change_password');
	Route::post('admin/password_data','Admin@change_details');
	Route::get('admin/editprofile/{id}', 'Admin@edit_profile');
	Route::post('admin/updateprofile/{id}', 'Admin@update_profile');
	Route::post('admin/otp_changepassword','Admin@otp_changepassword');
	
	//Vendors Authentication & Login
	Route::get('vendors/login','Store@login');
	Route::post('vendors/signin','Store@signin');
	Route::get('vendors/dashboard','Store@booking');
	Route::get('vendors/reports_analysis','Store@home');	
	Route::get('vendors/reset','Store@forgot');
	Route::post('vendors/forgot_mail','Store@forgot_details');
	Route::get('vendors/changepassword','Store@change_password');
	Route::post('vendors/password_data','Store@change_details');
	Route::get('vendors/signout','Store@logout');
	Route::get('vendors/editprofile','Store@edit_profile');
	Route::post('vendors/updateprofile/{id}', 'Store@update_profile');
	Route::post('vendors/verify_otp_password', 'Store@verify_otp_password');
		
	//Restaurant Managers Authentication & Login
    Route::get('managers/login', 'Manager@login');
    Route::post('managers/signin', 'Manager@signin');
    Route::get('managers/reports_analysis', 'Manager@home');
    Route::get('managers/dashboard', 'Manager@booking');
    Route::get('managers/reset', 'Manager@forgot');
    Route::post('managers/forgot_mail', 'Manager@forgot_details');
    Route::get('managers/changepassword', 'Manager@change_password');
    Route::post('managers/password_data', 'Manager@change_details');
    Route::get('managers/signout', 'Manager@logout');
    Route::get('managers/editprofile', 'Manager@edit_profile');
    Route::post('managers/updateprofile/{id}', 'Manager@update_profile');



	// Admin User //
	Route::get('admin/user/viewprofile/{id}', 'User@view_profile');

	//admin settings//
	Route::get('admin/settings/general', 'Admin@general_settings');
	Route::post('admin/settings/updategeneral/{id}', 'Admin@update_general_settings');
	Route::get('admin/settings/email', 'Admin@email_settings');
	Route::post('admin/settings/updateemail/{id}', 'Admin@update_email_settings');
	Route::get('admin/settings/socialmedia', 'Admin@social_media_settings');
	Route::post('admin/settings/updatemedia/{id}', 'Admin@update_media_settings');
	Route::get('admin/settings/sms', 'Admin@sms_settings');
	Route::post('admin/settings/updatesms/{id}', 'Admin@update_sms_settings');
	Route::get('admin/settings/image', 'Admin@image_settings');
	Route::post('admin/settings/updateimagesettings/{id}', 'Admin@update_image_settings');
	
	//admin local//
	Route::get('admin/settings/local', 'Admin@local');
	Route::post('admin/settings/updatelocal/{id}', 'Admin@update_local');
	
		
	//admin Localisation country//
	Route::get('admin/localisation/country', 'Localisation@country');
	Route::get('admin/country/create', 'Localisation@country_create');
	Route::post('createcountry', 'Localisation@country_store');
	Route::get('admin/country/edit/{id}', 'Localisation@country_edit');
	Route::post('admin/updatecountry/{id}', 'Localisation@country_update');
	Route::get('admin/country/delete/{id}', 'Localisation@country_destroy');
	
	//admin Localisation zones//
	Route::get('admin/localisation/zones', 'Localisation@zones');
	Route::get('admin/zones/create', 'Localisation@zones_create');
	Route::post('createzone', 'Localisation@zone_store');
	Route::get('admin/zones/edit/{id}', 'Localisation@zone_edit');
	Route::post('admin/updatezone/{id}', 'Localisation@zone_update');
	Route::get('admin/zones/delete/{id}', 'Localisation@zone_destroy');
	
	//admin Localisation city//
	Route::get('admin/localisation/city', 'Localisation@city');
	Route::get('admin/city/create', 'Localisation@city_create');
	Route::post('createcity', 'Localisation@city_store');
	Route::get('admin/city/edit/{id}', 'Localisation@city_edit');
	Route::post('admin/updatecity/{id}', 'Localisation@city_update');
	Route::get('admin/city/delete/{id}', 'Localisation@city_destroy');

	//Admin Booking Status //
	Route::get('admin/booking_status', 'Booking_Statuses@index');
	Route::get('admin/booking_status/create', 'Booking_Statuses@booking_create');
	Route::post('createbooking_status', 'Booking_Statuses@booking_store');
	Route::get('admin/booking_status/edit/{id}', 'Booking_Statuses@booking_status_edit');
	Route::post('updatebooking_status/{id}', 'Booking_Statuses@booking_status_update');
	Route::get('admin/booking_status/delete/{id}', 'Booking_Statuses@booking_status_destroy');

	//Vendors Amenities //
	Route::get('vendor/amenities', 'Amenity@index');
	Route::get('vendor/amenities/create', 'Amenity@amenity_create');
	Route::post('createamenities', 'Amenity@amenity_store');
	Route::get('vendor/amenities/edit/{id}', 'Amenity@amenity_edit');
	Route::post('vendor/updateamenities/{id}', 'Amenity@amenity_update');
	Route::get('vendor/amenities/delete/{id}', 'Amenity@amenity_destroy');

	//Vendors Room Type //
	Route::get('vendor/room_type', 'Room@index');
	Route::get('vendor/room_type/create', 'Room@roomtype_create');
	Route::post('createroom_type', 'Room@roomtype_store');
	Route::get('vendor/room_type/edit/{id}', 'Room@roomtype_edit');
	Route::post('vendor/updateroom_type/{id}', 'Room@roomtype_update');
	Route::get('vendor/room_type/delete/{id}', 'Room@roomtype_destroy');

	//Vendors Rooms //
	Route::get('vendor/rooms', 'Room@rooms');
	Route::get('vendor/rooms/create', 'Room@rooms_create');
	Route::post('/createrooms', 'Room@rooms_store');
	Route::get('vendor/rooms/edit/{id}', 'Room@rooms_edit');
	Route::post('vendor/updaterooms/{id}', 'Room@rooms_update');
	Route::get('vendor/rooms/delete/{id}', 'Room@rooms_destroy');

	//	Managers Room Type //
	Route::get('managers/room_type', 'Managers_Room@index');
	Route::get('managers/room_type/create', 'Managers_Room@roomtype_create');
	Route::post('managerscreateroom_type', 'Managers_Room@roomtype_store');
	Route::get('managers/room_type/edit/{id}', 'Managers_Room@roomtype_edit');
	Route::post('managers/updateroom_type/{id}', 'Managers_Room@roomtype_update');
	Route::get('managers/room_type/delete/{id}', 'Managers_Room@roomtype_destroy');

	//	Managers Rooms //
	Route::get('managers/rooms', 'Managers_Room@rooms');
	Route::get('managers/rooms/create', 'Managers_Room@rooms_create');
	Route::post('managerscreaterooms', 'Managers_Room@rooms_store');
	Route::get('managers/rooms/edit/{id}', 'Managers_Room@rooms_edit');
	Route::post('managers/updaterooms/{id}', 'Managers_Room@rooms_update');
	Route::get('managers/rooms/delete/{id}', 'Managers_Room@rooms_destroy');

	//	VENDOR BOOKING 	//	

	Route::get('vendor/booking', 'Booking@new_booking');
	Route::post('booking/booking_user','Booking@booking_user');
	Route::get('vendor/showbooking/{id}', 'Booking@showBooking');
	Route::get('vendor/editbooking/{id}', 'Booking@editBooking');
	Route::post('booking/payment','Booking@storePayment');
	Route::post('booking/charges','Booking@storeCharges');

	Route::get('booking/deletePayment/{id}', 'Booking@payment_destroy');
	//	Route::get('payment/list/{bid}','Booking@anyAjaxPaymentmanager');

	Route::get('autocomplete',array('as'=>'autocomplete','uses'=>'Booking@autocomplete'));
	Route::post('booking/new_user','Booking@newUser');


	//	MANAGERS BOOKING 

	Route::get('managers/booking', 'Managers_Booking@new_booking');
	Route::post('manager_booking/booking_user','Managers_Booking@booking_user');
	Route::get('managers/showbooking/{id}', 'Managers_Booking@showBooking');
	Route::get('managers/editbooking/{id}', 'Managers_Booking@editBooking');
	Route::post('manager_booking/payment','Managers_Booking@storePayment');
	Route::post('manager_booking/charges','Managers_Booking@storeCharges');
	Route::post('/manager_booking/new_user','Managers_Booking@newUser');


	//	Admin Reports

	Route::get('admin/reports','Reports@index');
	Route::get('reports/order', 'Reports@order');
    Route::post('reports/report-order-list', 'Reports@anyAjaxReportOrderList');
    Route::get('reports/user','Reports@customer_report');
	Route::post('reports/report_customer_order_list', 'Reports@anyAjaxCustomerReportList');
    Route::get('reports/vendor','Reports@vendor_report');
	Route::post('reports/report_vendor_order_list', 'Reports@anyAjaxVendorReportList'); 
    Route::get('reports/outlets','Reports@outlet_report');
	Route::post('reports/report_outlets_order_list', 'Reports@anyAjaxOutletReportList'); 	   
    Route::get('reports/transactions','Reports@transaction_report');
	Route::post('reports/report_transaction_list', 'Reports@anyAjaxTransactionReportList'); 
    Route::get('reports/statistics','Reports@statistics_report');
	Route::post('reports/statistics','Reports@statistics_report');


    //	Vendor Reports

    Route::get('vendor/reports/order', 'Reports@vendors_order');
    Route::post('vendor/reports/order', 'Reports@vendors_order');
    Route::get('vendor/date_reports/{date}', 'Reports@date_reports');
    Route::post('vendors/report_orders_list','Reports@anyAjaxVendorReportOrderList');
    Route::get('vendor/reports/room', 'Reports@room_report');
    Route::post('vendors/report_room_list','Reports@anyAjaxRoomReportList');
    Route::get('vendor/room/reports', 'Reports@booked_room_report');
    Route::post('vendors/report_bookedroom_list','Reports@anyAjaxBookedRoomReportList');
    //	Route::get('vendor/staffs/reports', 'Reports@staff_report');
    //	Route::post('vendors/staffs_list','Reports@anyAjaxStaffsReportList');
    Route::get('vendor/staffs/reports', 'Reports@vendors_staff');
    Route::post('vendor/staffs/reports', 'Reports@vendors_staff');
    Route::get('vendor/staff_date_reports/{date}/{id}', 'Reports@staff_date_reports');  
    Route::get('vendor/outlet/reports', 'Reports@outlets_report');
    Route::post('vendors/outlet_list','Reports@anyAjaxOutletsReportList');       


    //	Managers Reports

    Route::get('manager/reports/order', 'Managers_Reports@manager_trasaction_report');
    Route::post('manager/reports/order', 'Managers_Reports@manager_trasaction_report');
    Route::get('manager/date_reports/{date}', 'Managers_Reports@date_reports');
    Route::post('managers/report_orders_list','Reports@anyAjaxManagerReportOrderList');
    Route::get('manager/reports/room', 'Managers_Reports@booked_room_report');
    // Route::post('managers/report_room_list','Managers_Reports@anyAjaxManagersRoomReportList');
    // Route::get('manager/room/reports', 'Reports@managers_booked_room_report');
    Route::post('managers/report_bookedroom_list','Managers_Reports@anyAjaxBookedRoomReportList'); 

	//	Vendor Expenses

    Route::get('vendor/expenses','Expenses@index');
	Route::get('vendor/expenses/create', 'Expenses@expenses_create');
	Route::post('createexpenses', 'Expenses@expenses_store');
	Route::get('vendor/expenses/edit/{id}', 'Expenses@expenses_edit');
	Route::post('vendor/updateexpenses/{id}', 'Expenses@expenses_update');
	Route::get('vendor/expenses/delete/{id}', 'Expenses@expenses_destroy');

	//	Vendor Accounts

    Route::get('vendor/accounts','Expenses@accounts');
    Route::post('accounts/booking_list', 'Expenses@anyAjaxAccountList'); 


	//	Managers Expenses

    Route::get('managers/expenses','Managers_Expenses@index');
	Route::get('managers/expenses/create', 'Managers_Expenses@expenses_create');
	Route::post('managerscreateexpenses', 'Managers_Expenses@expenses_store');
	Route::get('managers/expenses/edit/{id}', 'Managers_Expenses@expenses_edit');
	Route::post('managers/updateexpenses/{id}', 'Managers_Expenses@expenses_update');
	Route::get('managers/expenses/delete/{id}', 'Managers_Expenses@expenses_destroy');

	//	Managers Accounts

    Route::get('managers/accounts','Managers_Expenses@accounts');
    Route::post('managers/accounts/booking_list', 'Managers_Expenses@anyAjaxAccountList');     

	//Vendors Accomodation //
	Route::get('vendor/accomodation', 'Accomodation@index');
	Route::get('vendor/accomodation/create', 'Accomodation@accomodation_create');
	Route::post('createaccomodation', 'Accomodation@accomodation_store');
	Route::get('vendor/accomodation/edit/{id}', 'Accomodation@accomodation_edit');
	Route::post('vendor/updateaccomodation/{id}', 'Accomodation@accomodation_update');
	Route::get('vendor/accomodation/delete/{id}', 'Accomodation@accomodation_destroy');
	
	//Managers Accomodation //
	Route::get('managers/accomodation', 'Accomodation@manager_index');
	Route::get('managers/accomodation/create', 'Accomodation@manager_accomodation_create');
	Route::post('createmanagersaccomodation', 'Accomodation@manager_accomodation_store');
	Route::get('managers/accomodation/edit/{id}', 'Accomodation@manager_accomodation_edit');
	Route::post('managers/updateaccomodation/{id}', 'Accomodation@manager_accomodation_update');
	Route::get('managers/accomodation/delete/{id}', 'Accomodation@manager_accomodation_destroy');

	//admin Localisation Language//
	Route::get('admin/localisation/language', 'Localisation@language');
	Route::get('admin/language/create', 'Localisation@language_create');
	Route::post('createlanguage', 'Localisation@language_store');
	Route::get('admin/language/edit/{id}', 'Localisation@language_edit');
	Route::post('admin/language/update/{id}', 'Localisation@language_update');
	Route::get('admin/language/delete/{id}', 'Localisation@language_destroy');
	
	//admin Localisation Currency//
	Route::get('admin/localisation/currency', 'Localisation@currency');
	Route::get('admin/currency/create', 'Localisation@currency_create');
	Route::post('createcurrency', 'Localisation@currency_store');
	Route::get('admin/currency/edit/{id}', 'Localisation@currency_edit');
	Route::post('admin/currency/update/{id}', 'Localisation@currency_update');
	Route::get('admin/currency/delete/{id}', 'Localisation@currency_destroy');

	//Payment Settings 
	Route::get('admin/payment/settings', 'Payment@payment_settings');
	Route::get('admin/payment/gatewaycreate', 'Payment@payment_gateway_create');
	Route::post('create_payment_gateway', 'Payment@payment_gateway_store');
	Route::get('admin/payment/gatewayedit/{id}', 'Payment@payment_gateway_edit');
	Route::post('update_payment_gateway/{id}', 'Payment@payment_gateway_update');
	Route::get('admin/payment/deletegateway/{id}', 'Payment@payment_gateway_destroy');

	//Vendors
	Route::get('vendors/vendors', 'Vendor@vendors');
	Route::get('vendors/create_vendor', 'Vendor@vendor_create');
	Route::post('vendor_create', 'Vendor@vendor_store');
	Route::get('vendors/edit_vendor/{id}', 'Vendor@vendor_edit');
	Route::post('update_vendor/{id}', 'Vendor@vendor_update');
	Route::get('vendors/delete_vendor/{id}', 'Vendor@vendor_destroy');
	Route::get('vendors/vendor_details/{id}','Vendor@vendor_show');

	//Admin Outlets
	Route::get('vendors/outlets', 'Vendor@branches');
	Route::get('vendors/create_outlet', 'Vendor@branch_create');
	Route::post('outlet_create', 'Vendor@branch_store');
	Route::get('vendors/edit_outlet/{id}', 'Vendor@branch_edit');
	Route::post('update_outlet/{id}', 'Vendor@branch_update');
	Route::get('vendors/delete_outlet/{id}', 'Vendor@branch_destroy');
	Route::get('vendors/outlet_details/{id}','Vendor@branch_show');

	//Vendors created staffs
	Route::get('vendors/staffs', 'Staffs@staffs');
	Route::get('vendors/create_staffs', 'Staffs@staffs_create');
	Route::post('vendors/store_staffs', 'Staffs@staffs_store');
	Route::get('vendors/edit_staffs/{id}', 'Staffs@staffs_edit');
	Route::post('update_users_staff/{id}', 'Staffs@staffs_update');
	Route::get('vendors/delete_staffs/{id}', 'Staffs@staffs_destroy');
	Route::get('vendors/staffs_details/{id}','Staffs@staffs_show');
	
	//Vendor Outlets
	Route::get('vendor/outlets', 'Store@branches');
	Route::get('vendor/create_outlet', 'Store@branch_create');
	Route::post('vendor/outlet_create', 'Store@branch_store');
	Route::get('vendor/edit_outlet/{id}', 'Store@branch_edit');
	Route::post('vendor/update_outlet/{id}', 'Store@branch_update');
	Route::get('vendor/delete_outlet/{id}', 'Store@branch_destroy');
	Route::get('vendor/outlet_details/{id}','Store@branch_show');

	Route::post('c_list1/coupon_outlet_list', 'Store@getAllVendorOutletList');
	Route::post('c_list1/coupon_product_list', 'Store@getAllOutletProductList');
	Route::post('admin/read_notifications', 'Store@notifications_read');

	//Gallery for Properties
	Route::get('vendors/outlets_gallery', 'Gallery@outlets_gallery_index');
	Route::post('vendor/gallery-upload', 'Gallery@gallery_upload');
	Route::post('vendor/image-deletes', 'Gallery@delete_image');

	//managers in Admin
	Route::get('vendors/outlet_managers', 'Vendor@outlet_managers');
	Route::get('vendors/create_outlet_managers', 'Vendor@outlet_managers_create');
	Route::post('create_manager', 'Vendor@outlet_managers_store');
	Route::get('vendors/edit_outlet_manager/{id}', 'Vendor@outlet_managers_edit');
	Route::post('admin/managers/update/{id}', 'Vendor@outlet_managers_update');
	Route::get('vendors/delete_outlet_managers/{id}', 'Vendor@outlet_managers_destroy');

	//Managers OR USER
	Route::get('vendor/outletmanagers', 'Store@outlet_managers');
	Route::get('vendor/create_outlet_managers', 'Store@outlet_managers_create');
	Route::post('vendor/create_manager', 'Store@outlet_managers_store');
	Route::get('vendor/edit_outlet_manager/{id}', 'Store@outlet_managers_edit');
	Route::post('vendor/managers/update/{id}', 'Store@outlet_managers_update');
	Route::get('vendor/delete_outlet_managers/{id}', 'Store@outlet_managers_destroy');
	Route::get('/managers/confirmation', 'Store@signup_confirmation_manager');

	//categories//
	Route::get('admin/category', 'Category@index');
	Route::get('admin/category/create', 'Category@create');
	Route::post('createcategory', 'Category@store');
	Route::get('categorycreate', 'Category@create');
	Route::get('admin/category/edit/{id}', 'Category@edit');
	Route::get('admin/category/view/{id}', 'Category@show');
	Route::post('updatecategory/{id}', 'Category@update');
	Route::get('admin/category/delete/{id}', 'Category@destroy');

	//Banners//
	Route::get('admin/banners', 'Banner@index');
	Route::get('admin/banner/create', 'Banner@create');	
	Route::post('createbanner', 'Banner@store');
	Route::get('admin/banner/edit/{id}', 'Banner@edit');
	Route::post('admin/banner/update/{id}', 'Banner@update');
	Route::get('admin/banner/delete/{banner_setting_id}', 'Banner@destroy');
	
	//Users//
	Route::get('admin/users/index', 'User@user_index');
	Route::get('admin/users/create', 'User@user_create');
	Route::post('createuser', 'User@user_store');
	Route::get('admin/users/delete/{id}', 'User@user_delete');
	Route::get('admin/users/edit/{id}', 'User@user_edit');
	Route::post('update_users/{id}', 'User@user_update');
	
	//User Groups//
	Route::get('admin/users/groups', 'User@group_index');
	Route::get('admin/groups/create', 'User@group_create');
	Route::post('creategroup', 'User@group_store');
	Route::get('admin/groups/edit/{id}', 'User@group_edit');
	Route::get('admin/groups/delete/{id}', 'User@group_delete');
	Route::post('update_group/{id}', 'User@group_update');

	//User Address Types//
	/*
	Route::get('admin/users/addresstype', 'User@address_index');
    Route::get('admin/addresstype/create', 'User@address_create');
    Route::post('createaddresstype', 'User@address_store');
    Route::get('admin/addresstype/edit/{id}', 'User@address_edit');
    Route::post('update_addresstype/{id}', 'User@address_update');
    Route::get('admin/addresstype/delete/{id}', 'User@address_delete');
    */

	//Notification Templates//
	Route::get('admin/templates/email', 'Template@index');
	Route::get('admin/templates/create', 'Template@create');
	Route::post('createtemplate', 'Template@store');
	Route::get('admin/templates/delete/{id}', 'Template@destroy');
	Route::get('admin/templates/edit/{id}', 'Template@edit');
	Route::post('admin/template/update/{id}', 'Template@update');
	Route::get('admin/templates/view/{id}', 'Template@view');

	//Admin Newsletter 
	Route::get('admin/newsletter', 'Admin@newsletter');
	Route::post('send_newsletter', 'Admin@send_newsletter');
	Route::post('list/all_customers', 'Admin@getAllCustomersData');
	Route::post('list/newsletter_subscribers', 'Admin@getAllSubscribersData');
	Route::post('list/customers_group', 'Admin@getAllCustomersGroupData');
		
	//Admin Coupon Management
	Route::get('admin/coupons', 'Coupon@index');
	/*Route::get('admin/coupons/create', 'Coupon@create');
	Route::post('create_coupon', 'Coupon@store');
	Route::get('admin/coupons/edit/{id}', 'Coupon@edit');
	Route::post('admin/coupons/update/{id}', 'Coupon@update'); */
	Route::get('admin/coupons/view/{id}', 'Coupon@show');
	Route::get('admin/coupons/delete/{id}', 'Coupon@delete');
	Route::post('c_list/coupon_outlet_list', 'Coupon@getAllVendorOutletList');
	Route::post('c_list/coupon_product_list', 'Coupon@getAllOutletProductList');

	// Admin Subscribers Management
	Route::get('admin/subscribers', 'Subscribers@index');
	Route::get('admin/subscribers/delete/{id}', 'Subscribers@delete');
	Route::get('admin/subscribers/updateStatus/{id}/{status}', 'Subscribers@UpdateStatus');

	//blogs//
	Route::get('admin/blog', 'Blog@index');
	Route::get('admin/blog/create', 'Blog@create');
	Route::post('createblog', 'Blog@store');
	Route::get('admin/blog/edit/{id}', 'Blog@edit');
	Route::get('admin/blog/view/{id}', 'Blog@show');
	Route::post('updateblog/{id}', 'Blog@update');
	Route::get('admin/blog/delete/{id}', 'Blog@destroy');

	//Permission Roles//
	Route::get('system/permission', 'Roles@index');
	Route::get('system/permission/create', 'Roles@create');
	Route::post('system/rolecreate', 'Roles@store');
	Route::get('system/permission/edit/{id}', 'Roles@edit');
	Route::post('update_role/{id}', 'Roles@update');
	Route::get('system/permission/delete/{id}', 'Roles@destroy');

	//Permission Roles for vendor//
    Route::get('vendor/permission', 'Vendorroles@index');
    Route::get('vendor/permission/create', 'Vendorroles@create');
    Route::post('vendor/rolecreate', 'Vendorroles@store');
    Route::get('vendor/permission/edit/{id}', 'Vendorroles@edit');
    Route::post('vendor/update_role/{id}', 'Vendorroles@update');
    Route::get('vendor/permission/delete/{id}', 'Vendorroles@destroy');	

	//Permission Users//
	Route::get('permission/users', 'Roles@users');
	Route::get('permission/usercreate', 'Roles@user_create');
	Route::post('permission/userstore', 'Roles@user_store');
	Route::get('permission/users/edit/{id}', 'Roles@user_edit');
	Route::post('usersupdate/{id}', 'Roles@user_update');
	Route::get('permission/users/delete/{id}', 'Roles@users_destroy');

	//Permission Users for vendors//
	Route::get('vendors/permission/users', 'Vendorroles@users');
	Route::get('vendors/permission/usercreate', 'Vendorroles@user_create');
	Route::post('vendors/permission/userstore', 'Vendorroles@user_store');
	Route::get('vendors/permission/users/edit/{id}', 'Vendorroles@user_edit');
	Route::post('vendors/usersupdate/{id}', 'Vendorroles@user_update');
	Route::get('vendors/permission/users/delete/{id}', 'Vendorroles@users_destroy');

	//cms//
	Route::post('createcms', 'Acms@store');
    Route::get('admin/cms/create', 'Acms@create');
    Route::get('admin/cms/custom-filter-data', 'Acms@getCustomFilterData');
    Route::get('admin/cms/edit/{id}', 'Acms@edit');
    Route::get('admin/cms/view/{id}', 'Acms@show');
    Route::post('updatecms/{id}', 'Acms@update');
    Route::get('admin/cms/delete/{id}', 'Acms@destory');
    Route::get('admin/cms', 'Acms@index');
	
	
	//front end cms//
	Route::get('stores/{city}', 'Frontstore@store_list');
	//Route::get('stores', 'Frontstore@store_list');
	Route::get('stores/{city}/{location}/{category}', 'Frontstore@store_list');
	Route::get('stores/{city}/{location}/{latitude}/{longitude}', 'Frontstore@store_list');
	Route::get('stores/{category}', 'Frontstore@store_list');
	Route::get('marketplace/', 'Frontstore@marketplace');


	Route::post('/stores_outlet', 'Frontstore@store_outlet_list');
	Route::get('market/{category}', 'Frontstore@market');
	Route::get('market/{category}/{subcategory}', 'Frontstore@market');
	Route::post('market_ajax', 'Frontstore@market_ajax');
	Route::get('market_ajax', 'Frontstore@market_ajax');
	Route::post('market_ajax', 'Frontstore@market_ajax');
	Route::post('great_deals_ajax', 'Front@great_deals_ajax');
	Route::post('most_popular_ajax', 'Front@most_popular_ajax');
	Route::post('new_arrivals_ajax', 'Front@new_arrivals_ajax');

	
	Route::post('/login_user', 'Welcome@login_user');
	//Route::get('/', 'Front@welcome');
	Route::get('/about-us', 'Front@aboutus');
	Route::get('/weare-hiring', 'Front@weare_hiring');
	Route::get('/ourservice-areas', 'Front@ourservice_areas');
	Route::get('/aboutus/filter/{id}', 'Front@aboutus');
	Route::get('/sell-on-net', 'Front@register_your_store');
	//Route::get('/cart', 'Front@cart');
	Route::get('/portfolios/', 'Front@portfolios');
	Route::get('/portfolios/filter/{id}', 'Front@portfolios');
	Route::get('/portfolios/info/{id}', 'Front@portfolios_info');
	Route::get('/contact-us', 'Front@contactus');
	Route::get('/sitmap', 'Front@sitmap');
	Route::get('/faq', 'Front@faq');
	Route::get('/account-settings', 'Front@accountsettings');
	Route::get('/mint', 'Front@mint');
	Route::get('/price', 'Front@price');
	Route::get('/request', 'Front@request');
	Route::get('/editrequest', 'Front@editrequest');
	Route::get('/most-popular', 'Front@most_popular');
	Route::get('/new-arrivals', 'Front@new_arrivals');
	Route::get('/great-deals', 'Front@great_deals');
	Route::get('/offers', 'Front@great_deals');
	Route::get('/press', 'Front@press');
	
	
	Route::post('/postcontactus', 'Front@storecontact');
	Route::get('/blog', 'Front@blog');
	//Route::get('/blog/info/{url-key}', 'Front@blog');
	Route::get('/blog/info/{id}', 'Front@blog_info');
	Route::get('/thankyou', 'Front@thankyou');
	Route::get('thank-you', 'checkout@getDonePayFort');
	Route::get('/filter/{id}', 'Front@welcome');
	
	//front end cms//
	Route::get('cms/{id}', 'Front@cms');
	Route::get('/cms-mob/{id}/{language}', 'Front@cms_mob');
	Route::get('/mob/about-us/{language}', 'Front@aboutus_mob');
	Route::get('/mob/faq/{id}/{language}', 'Front@faq_mob');
	//Language Translate//
	Route::post('changelocale', ['as' => 'changelocale', 'uses' => 'Translation@changeLocale']);
	Route::get('/coupons', 'Front@offer');
	
	
	//All ajax requests goes here
	Route::post('list/CityList', 'Store@getCityData');
	Route::post('list/RoomList', 'Store@getRoomData');
	Route::post('rooms_check','Store@rooms_check');
	Route::post('expense_data','Store@expense_data');
	Route::post('days_check','Store@days_check');
	Route::post('summary_check','Store@summary_check');
	Route::post('guest_check','Store@guest_check');
	Route::post('price_check','Store@price_check');
	Route::post('list/LocationList', 'Store@getLocationData');
	Route::post('list1/LocationList', 'Store@getFrontLocationData');
	Route::post('list/OutletList', 'Store@getOutletData');
	Route::post('list/SubCategoryList', 'Store@getSubCategoryData');

	Route::post('list/SubCategoryListUpdated', 'Store@getSubCategoryDataUpdated');
	Route::post('list/Maincategorylist', 'Store@Maincategorylist');
	Route::post('list/getVendorcategorylist', 'Store@getVendorcategorylist');

	Route::post('list/ProductMaincategorylist', 'Store@ProductMaincategorylist');
	
	
	Route::post('translate', 'Translation@translate');
	Route::post('user/activity/ajaxlist', 'User@loadactivityajax');
	Route::post('admin/banner/ajaxupdate', 'Banner@ajaxupdate');
	// front End **/
	Route::get('/signup/confirmation/{key}/{email}/{password}', 'Front@signup_confirmation');
	Route::get('/account/welcome', 'Front@thankyou');

	Route::post('login-user', 'Front@login_user');
	Route::post('signup-user', 'Front@signup_user');
	Route::post('storeregister-user', 'Front@store_register_user');
	Route::post('member-ship', 'Front@user_membership');
	
	
	Route::post('product-rating', 'Front@product_rating');
	Route::post('forgot-password', 'Front@forgot_password');
	Route::get('/logout','Front@logout');
	Route::get('/profile','Profile@profile');
	Route::post('update-profile', 'Profile@update_profile');
	Route::get('/change-password','Welcome@change_password');
	Route::post('/update-password','Welcome@update_password');
	
	Route::get('auth/facebook', 'Front@redirectToProvider');
	Route::get('/auth/facebook/callback', 'Front@handleProviderCallback');
	Route::post('/location_outlet', 'Front@location_outlet');

	Route::get('auth/googleplus', 'Front@redirectToProviderGoogle');
    Route::get('/auth/googleplus/callback', 'Front@handleProviderCallbackGoogle');
	
	Route::get('store', 'Frontstore@index');
	Route::get('store/info/{id}', 'Frontstore@store_info');
	Route::get('store/info/{id}/{category}', 'Frontstore@store_info');
	Route::post('addtofavourite', 'Frontstore@addtofavourite');
	Route::get('cards', 'Welcome@cards');
	Route::get('new-card', 'Welcome@new_card');
	Route::post('store-card', 'Welcome@store_card');
	Route::get('edit-card/{id}', 'Welcome@edit_card');
	Route::post('update-card', 'Welcome@update_card');
	Route::get('delete-card/{id}', 'Welcome@delete_card');
	Route::get('checkout', 'checkout@index');
	
	Route::get('new-address', 'Welcome@new_address');
	Route::post('get_city', 'Welcome@get_city');
	Route::post('store-address', 'Welcome@store_address');
	Route::post('store-address-ajax', 'Welcome@store_address_ajax');
	
	Route::get('edit-address/{id}', 'Welcome@edit_address');
	Route::post('update-address', 'Welcome@update_address');
	Route::get('delete-address/{id}', 'Welcome@delete_address');
	
	Route::get('delete-address/{id}', 'Welcome@delete_address');
	Route::get('favourite-stores', 'Welcome@favourite_stores');
	Route::get('favourite-products', 'Welcome@favourite_products');
	Route::post('profile_image', 'Welcome@profile_image');
	Route::get('cart', 'Usercart@index');
	Route::get('cart-add', 'Usercart@add_to_cart');
	Route::post('addtocart', 'Usercart@add_cart_info');
	Route::post('update-cart', 'Usercart@update_cart');
	Route::post('delete-cart', 'Usercart@delete_cart');
	Route::post('refresh_cart', 'Usercart@refresh_cart');
	Route::post('proceed-checkout', 'checkout@proceed_checkout');
	Route::get('offline_payment', 'checkout@offline_payment');
	Route::get('thankyou/{id}', 'checkout@thankyou');
	Route::post('update-promcode', 'checkout@update_promocode');
	Route::post('check-otp', 'checkout@check_otp');
	Route::post('send-otp', 'checkout@send_otp');
	Route::get('re-order/{id}', 'checkout@re_order');
	
	Route::get('invoice-order/{id}', 'Welcome@invoice');
	//Product details
	Route::get('/product/info/{url_index}', 'Frontstore@product_info');

	//	Admin Reviews
	Route::get('admin/reviews', 'Reviews@index');
	Route::get('admin/reviews/approve/{id}', 'Reviews@approve');
	Route::get('admin/reviews/view/', 'Reviews@view');
	Route::get('admin/reviews/delete/{id}', 'Reviews@destroy');

	//	Vendor Reviews
    Route::get('vendors/reviews/', 'Store@reviews');
    Route::get('vendors/reviews/view/', 'Store@view_review');
    Route::get('vendors/reviews/approve/{id}', 'Store@approve');
    Route::get('vendors/reviews/delete/{id}', 'Store@review_destory');


    //	VENDOR SERVICE REVIEWS
    Route::get('vendors/review/services/', 'Store@services_reviews');
    Route::get('vendors/reviews/services/view/', 'Store@services_view_review');
    Route::get('vendors/reviews/services/approve/{id}', 'Store@services_approve');
    Route::get('vendors/reviews/services/delete/{id}', 'Store@services_review_destory');


	//	VENDOR CUSTOMER FEEDBACK
    Route::get('vendors/customer_feedback', 'Store@customer_feedback');
    Route::get('vendors/customer_feedback/view/{id}', 'Store@show_customer_feedback');
    Route::get('vendors/customer_feedback/delete/{id}', 'Store@customer_feedback_destroy');


	//Admin Notification
	Route::get('admin/notifications', 'Notification@index');
	Route::get('admin/push-notifications', 'CommonNotification@push_notification_view');
	Route::get('admin/email-notifications', 'CommonNotification@email_notificaition');
	// Route::get('admin/SMS-notifications', 'CommonNotification@sms_notificaition');
	Route::post('send_email', 'CommonNotification@send_email');
	// Route::post('send_email_otpbysms', 'CommonNotification@send_email_otpbysms');
	Route::post('push_notification', 'CommonNotification@push_notification');
	Route::post('send_notification', 'CommonNotification@send_notification');
	Route::post('list/newsletter', 'Admin@getUserData');
	
	//Vendors Notification
	Route::get('vendors/notifications', 'VendorsNotification@index');
	Route::post('vendors/read_notifications', 'VendorsNotification@notifications_read');

	Route::post('/user-subscribe', 'Front@user_subscribe');
	Route::get('/user-unsubscribe/{id}', 'Front@user_unsubscribe');
	Route::get('/mob-cms/{id}/{language}', 'Frontstore@cms_mob');
	Route::get('/send-notification','Cron@send_notification');
	Route::get('/order-assign-automated','Cron@OrderAssignNotification');

	Route::get('/search-product/{q}','Front@search_product');
	Route::get('/ajax-search-product','Front@ajax_search_product');


	Route::get('/vendors/billing', 'Store@billing');
	Route::post('/subscribe_vendor', 'Store@subscribe_vendor');

    Route::get('getDone', ['as' => 'getDone', 'uses' => 'checkout@getDone']);
    Route::get('getCancel', ['as' => 'getCancel', 'uses' => 'checkout@getCancel']);


    Route::get('vendorgetCancel', ['as' => 'vendorgetCancel', 'uses' => 'Store@vendorgetCancel']);

    Route::get('admin/subscription-payments', 'Store@subscription_payments');

    Route::get('user/invoice/{invoice}/{vendor_id}', 'Store@downloadinvoice');

	//Housekeeping Management
	Route::get('vendor/house_keeping', 'HouseKeeping@index');
	Route::get('vendor/house_keeping/create', 'HouseKeeping@create');
	Route::post('create_housekeeper','HouseKeeping@store');
	Route::get('vendor/house_keeping/edit/{id}','HouseKeeping@edit');
	Route::post('update_housekeeper/{id}','HouseKeeping@update');
	Route::get('vendor/house_keeping/delete/{id}','HouseKeeping@destroy');
	Route::get('vendor/house_keeping/view/{id}','HouseKeeping@show');

	//Housekeeping Task Management
	Route::get('vendor/housekeeping_tasks', 'HouseKeeping@housekeepingTaskIndex');
	Route::get('vendor/housekeeping_tasks/create', 'HouseKeeping@housekeepingTaskCreate');
	Route::post('create_housekeeping_task','HouseKeeping@housekeepingTaskStore');
	Route::get('vendor/housekeeping_tasks/edit/{id}','HouseKeeping@housekeepingTaskEdit');
	Route::post('update_housekeeping_task/{id}','HouseKeeping@housekeepingTaskUpdate');
	Route::get('vendor/housekeeping_tasks/delete/{id}','HouseKeeping@housekeepingTaskDestroy');
	Route::get('vendor/housekeeping_tasks/view/{id}','HouseKeeping@housekeepingTaskShow');

	Route::post('RoomsListing','HouseKeeping@allRoomsByProperty')->name('RoomsListing');
	Route::post('HousekeepersList','HouseKeeping@getAllHousekeepers')->name('HousekeepersList');
	Route::post('ManageTasks','HouseKeeping@updateTaskStatus')->name('ManageTasks');

	//Housekeeping Management
	Route::get('managers/house_keeping', 'Managers_HouseKeeping@index');
	Route::get('managers/house_keeping/create', 'Managers_HouseKeeping@create');
	Route::post('managerscreate_housekeeper','Managers_HouseKeeping@store');
	Route::get('managers/house_keeping/edit/{id}','Managers_HouseKeeping@edit');
	Route::post('managersupdate_housekeeper/{id}','Managers_HouseKeeping@update');
	Route::get('managers/house_keeping/delete/{id}','Managers_HouseKeeping@destroy');
	Route::get('managers/house_keeping/view/{id}','Managers_HouseKeeping@show');

	//Managers_HouseKeeping Task Management
	Route::get('managers/housekeeping_tasks', 'Managers_HouseKeeping@housekeepingTaskIndex');
	Route::get('managers/housekeeping_tasks/create', 'Managers_HouseKeeping@housekeepingTaskCreate');
	Route::post('managerscreate_housekeeping_task','Managers_HouseKeeping@housekeepingTaskStore');
	Route::get('managers/housekeeping_tasks/edit/{id}','Managers_HouseKeeping@housekeepingTaskEdit');
	Route::post('managersupdate_housekeeping_task/{id}','Managers_HouseKeeping@housekeepingTaskUpdate');
	Route::get('managers/housekeeping_tasks/delete/{id}','Managers_HouseKeeping@housekeepingTaskDestroy');

	Route::post('managersRoomsListing','Managers_HouseKeeping@allRoomsByProperty')->name('managersRoomsListing');
	Route::post('managersHousekeepersList','Managers_HouseKeeping@getAllHousekeepers')->name('managersHousekeepersList');
	Route::post('managersManageTasks','Managers_HouseKeeping@updateTaskStatus')->name('managersManageTasks');	


	//Subscription Packages Admin Module
	Route::get('admin/subscription_packages', 'SubscriptionPlans@index');
	Route::get('admin/subscription_packages/edit/{id}', 'SubscriptionPlans@edit');
	Route::post('update_subscription_package/{id}','SubscriptionPlans@update');
	Route::get('admin/subscription_packages/view/{id}','SubscriptionPlans@show');

	//Subscription Packages Vendor Module
	Route::get('vendors/subscription_packages', 'SubscriptionVendor@vendorIndex');
	Route::post('vendors/subscription_list', 'SubscriptionVendor@subscriptionModal');
	//Route::post('vendors/subscription_payment_response', 'SubscriptionVendor@paymentResponse');

	//Subscription Payments
	Route::post('subscription_payment/{id}','SubscriptionVendor@subscriptionPayments');
	
	// PAYMENT 

	Route::get('payment_respond','SubscriptionVendor@payment_respond');

	//Subscription Transactions for admin
	Route::get('admin/subscription_transactions','SubscriptionPlans@SubscriptionTransaction');
	Route::get('admin/subscription_transactions/view/{id}','SubscriptionPlans@viewSubscriptionTransaction');
	
	//Cron Running Urls for deactivating the vendors and sending mail to them for plan renewal
	Route::get('admin/inactive_vendor','CronController@inactiveVendors');
	Route::get('admin/send_expiring_mail','CronController@expiringPlan');
	Route::get('admin/send_expired_mail','CronController@expiredPlan');

	//Subscription Transactions for vendor
	Route::get('vendors/subscription_transactions','SubscriptionVendor@vendorSubscriptionTransaction');
	Route::get('vendors/subscription_transactions/view/{id}','SubscriptionVendor@viewVendorTransaction');
	Route::get('vendors/subs_payments/{id}','SubscriptionVendor@paymentShow');

	//Customers in Admin panel
	Route::get('admin/customers', 'CustomerController@index');
	Route::get('admin/customers/create', 'CustomerController@create');
	Route::post('create_customers','CustomerController@store');
	Route::get('admin/customers/edit/{id}','CustomerController@edit');
	Route::post('update_customers/{id}','CustomerController@update');
	Route::get('admin/customers/delete/{id}','CustomerController@destroy');
	Route::get('admin/customers/view/{id}','CustomerController@show');


	//SMS Templates//
	Route::get('admin/sms_template', 'SmsTemplate@index');
	Route::get('admin/sms_template/create', 'SmsTemplate@SMS_template_create');
	Route::post('createsms_template', 'SmsTemplate@SMS_template_store');
	Route::get('admin/sms_template/edit/{id}', 'SmsTemplate@SMS_template_edit');
	Route::get('admin/sms_template/view/{id}', 'SmsTemplate@SMS_template_show');
	Route::post('updatesms_template/{id}', 'SmsTemplate@SMS_template_update');
	// Route::get('admin/sms_template/delete/{id}', 'SmsTemplate@SMS_template_destroy');

	//Customer Referrals//
	Route::get('admin/customer_referrals', 'CustomerController@referralIndex');

Route::get('ajaxNotificationList.data', 'Notification@anyAjaxNotificationList' )->name('ajaxNotificationList.data');

Route::get('listcategoryajax.data', 'Category@anyAjaxCategory' )->name('listcategoryajax.data');

Route::get('ajaxpayment.data', 'Payment@anyAjaxpaymentsettings' )->name('ajaxpayment.data');

Route::get('ajaxpaymentmanager.data', 'Booking@anyAjaxPaymentmanager' )->name('ajaxpaymentmanager.data');


Route::get('ajaxcountry.data', 'Localisation@anyAjaxCountry' )->name('ajaxcountry.data');

Route::get('ajaxzones.data', 'Localisation@anyAjaxZones')->name('ajaxzones.data');

Route::get('ajaxcities.data','Localisation@anyAjaxCities')->name('ajaxcities.data');

Route::get('ajaxamenities.data','Amenity@anyAjaxAmenities')->name('ajaxamenities.data');

Route::get('ajaxexpenses.data','Expenses@anyAjaxExpenses')->name('ajaxexpenses.data');

Route::get('ajaxmanagerexpenses.data','Managers_Expenses@anyAjaxExpenses')->name('ajaxmanagerexpenses.data');

Route::get('ajaxaccomodation.data','Accomodation@anyAjaxAccomodation_type')->name('ajaxaccomodation.data');

Route::get('ajaxbookingstatus.data','Booking_Statuses@anyAjaxBookingStatus')->name('ajaxbookingstatus.data');


Route::get('ajaxmanageraccomodation.data','Accomodation@anyAjaxManagerAccomodation_type')->name('ajaxmanageraccomodation.data');

Route::get('ajaxroomtype.data','Room@anyAjaxRoomtypes')->name('ajaxroomtype.data');

Route::get('ajaxrooms.data','Room@anyAjaxRooms')->name('ajaxrooms.data');

Route::get('ajaxmanagersroomtype.data','Managers_Room@anyAjaxRoomtypes')->name('ajaxmanagersroomtype.data');

Route::get('ajaxmanagersrooms.data','Managers_Room@anyAjaxRooms')->name('ajaxmanagersrooms.data');

Route::get('listlanguageajax.data','Localisation@anyAjaxLanguage')->name('listlanguageajax.data');

Route::get('ajaxcurrency.data','Localisation@anyAjaxCurrency')->name('ajaxcurrency.data');

Route::get('ajaxbanner.data','Banner@anyAjaxbannerlist')->name('ajaxbanner.data');

Route::get('ajaxtemplate.data','Template@anyAjaxtemplatelist')->name('ajaxtemplate.data');

Route::get('listCouponAjax.data','Coupon@anyAjaxcouponlist')->name('listCouponAjax.data');

Route::get('listSubscriberAjax.data','Subscribers@anyAjaxSubscriberslist')->name('listSubscriberAjax.data');

Route::get('ajaxvendor.data','Vendor@anyAjaxVendor')->name('ajaxvendor.data');

Route::get('ajaxbranch.data','Vendor@anyAjaxBranch')->name('ajaxbranch.data');

Route::get('ajaxbranchmanager.data','Vendor@anyAjaxBranchmanager')->name('ajaxbranchmanager.data');

Route::get('ajaxgroup.data', 'User@anyAjaxgroupslist' )->name('ajaxgroup.data');

Route::get('ajaxaddresstype.data', 'User@anyAjaxaddresstype' )->name('ajaxaddresstype.data');

Route::get('listcmsajax.data', 'Acms@anyCmsAjax' )->name('listcmsajax.data');

Route::get('ajaxusers.data', 'User@anyAjaxuserlist' )->name('ajaxusers.data');

Route::get('listblogajax.data', 'Blog@anyAjaxbloglist' )->name('listblogajax.data');

Route::get('listroleajax.data', 'Roles@anyAjaxrolelist' )->name('listroleajax.data');

Route::get('listroleuserajax.data', 'Roles@anyAjaxroleuesrlist' )->name('listroleuserajax.data');

Route::get('ajaxReviewslist.data', 'Reviews@anyAjaxreviewlist' )->name('ajaxReviewslist.data');

Route::get('ajaxReviewslistvendor.data', 'Store@anyAjaxreviewlistvendor' )->name('ajaxReviewslistvendor.data');

Route::get('ajaxServiceReviewslistvendor.data', 'Store@anyAjaxservicereviewlistvendor' )->name('ajaxServiceReviewslistvendor.data');

Route::get('ajaxCustomerFeedbacklistvendor.data', 'Store@anyAjaxCustomerFeedback' )->name('ajaxCustomerFeedbacklistvendor.data');

// Route::controller('vendors/reviews', 'Store', [
//     'anyAjaxreviewlistvendor' => 'ajaxReviewslistvendor.data',
//     'reviews' => 'datatables',
// ]);

Route::get('anyajaxbranch.data', 'Store@anyAjaxBranches' )->name('anyajaxbranch.data');

Route::get('ajaxvendorbranchmanager.data', 'Store@anyAjaxVendorBranchmanager' )->name('ajaxvendorbranchmanager.data');

Route::get('ajaxstaffsbranchmanager.data', 'Staffs@anyAjaxStaffsBranchmanager' )->name('ajaxstaffsbranchmanager.data');

Route::get('listvendorroleajax.data', 'Vendorroles@anyAjaxvendorrolelist' )->name('listvendorroleajax.data');

Route::get('listrolevendorsajax.data', 'Vendorroles@anyAjaxvendorroleuesrlist' )->name('listrolevendorsajax.data');

Route::get('ajaxStoreNotificationList.data', 'VendorsNotification@anyAjaxStoreNotificationList' )->name('ajaxStoreNotificationList.data');

Route::get('listbillingAjaxadmin.data', 'Store@anyAjaxbilling' )->name('listbillingAjaxadmin.data');

Route::get('anyajaxhousekeeping.data', 'HouseKeeping@anyAjaxHousekeeping' )->name('anyajaxhousekeeping.data');
Route::get('anyajaxhousekeepingtasks.data', 'HouseKeeping@anyAjaxHousekeepingTasks' )->name('anyajaxhousekeepingtasks.data');

Route::get('anyajaxmanagerhousekeeping.data', 'Managers_HouseKeeping@anyAjaxHousekeeping' )->name('anyajaxmanagerhousekeeping.data');
Route::get('anyajaxmanagerhousekeepingtasks.data', 'Managers_HouseKeeping@anyAjaxHousekeepingTasks' )->name('anyajaxmanagerhousekeepingtasks.data');
Route::get('anyajaxsubscriptionplans.data', 'SubscriptionPlans@anyAjaxSubscriptionPlans' )->name('anyajaxsubscriptionplans.data');
Route::get('anyajaxsubscriptionTransactions.data', 'SubscriptionPlans@anyAjaxSubscriptionTransactions' )->name('anyajaxsubscriptionTransactions.data');
Route::get('anyajaxVendorTransactions.data', 'SubscriptionVendor@anyajaxVendorTransactions' )->name('anyajaxVendorTransactions.data');
Route::get('anyajaxcustomers.data', 'CustomerController@anyAjaxCustomers' )->name('anyajaxcustomers.data');

Route::get('ajaxsms_template.data', 'SmsTemplate@anyAjaxSmsTemplatelist' )->name('ajaxsms_template.data');

Route::get('listcouponajax.data', 'Coupon_vendor@anyAjaxcouponlist' )->name('listcouponajax.data');

Route::get('anyajaxreferralcustomers.data', 'CustomerController@anyAjaxreferralIndex')
			->name('anyajaxreferralcustomers.data');
//Vendor
Route::get('ajaxfundRequest.data', 'CustomController@anyAjaxfundRequest' )
									->name('ajaxfundrequest.data');	
//Admin									
Route::get('ajaxadminfundrequest.data', 'CustomController@anyAjaxAdminfunds' )
									->name('ajaxadminfundrequest.data');												
			
/*
Route::controller('admin/subscription-payments', 'Store', [
        'anyAjaxsubscriptionpayment' => 'listsubscriptionpaymentAjaxadmin.data',
        'index' => 'datatables',
]);
*/

/*Route::get('sitemap.xml', function(){

    // create new sitemap object
    $sitemap = App::make("sitemap");

    // set cache (key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean))
    // by default cache is disabled
    $sitemap->setCache('laravel.sitemap', 3600);

    // add item to the sitemap (url, date, priority, freq)
    $sitemap->add(URL::to(), '2012-08-25T20:10:00+02:00', '1.0', 'daily');
    $sitemap->add(URL::to('page'), '2012-08-26T12:30:00+02:00', '0.9', 'daily');

    // get all posts from db
    $posts = DB::table('posts')->orderBy('created_at', 'desc')->get();

    // add every post to the sitemap
    foreach ($posts as $post)
    {
        $sitemap->add($post->slug, $post->modified, $post->priority, $post->freq);
    }

    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $sitemap->render('xml');
});
*/

	Route::post('permissions_role/create', 'Permissions@role_create');
	
	Route::post('permissions_role/update_role', 'Permissions@role_update');

	Route::post('vendor/permissions_role/create', 'VendorPermissions@role_create');
	
	Route::post('vendor/permissions_role/update_role', 'VendorPermissions@role_update');	

	Route::post('change_property','Store@changeProperty');

});

	
	Route::get('/', 'Front@landing');
	Route::get('/vendor-signup', 'Front@landing_signup');
	Route::post('/vendors_signup','Store@vendors_store');
	Route::get('book-now', 'Front@home');

/*	15 MAY - AK
	Route::get('hotels/{location}/{lat}/{lng}/{startDate}/{endDate}/{guest_count}/{room_count}',
		'Front@hotels_list');
*/

	Route::get('hotels/{location}/{lat}/{lng}/{startDate}/{endDate}/{guest_count}/{room_count}/{user_lt}/{user_lg}','Front@hotels_list');


	//	Route::get('hotels/{location}/{lat}/{lng}','Front@hotels_list');
	Route::post('/signup_user', 'Front@signup_user');
	Route::post('/verify_user', 'Front@verify_user');

	//	STORE GUEST [PAY AT HOTEL]

	Route::post('/signup_guest', 'Front@signup_guest');
	Route::post('/verify_guest_otp', 'Front@verify_guest_otp');

	//	STORE GUEST [PAY NOW]

	Route::post('/signup_guest_now', 'Front@signup_guest_now');
	//	Route::post('/payment_details_page', 'Front@payment_details_page');
	Route::post('/payment_details','checkout@checkout');
	Route::post('/payment_store','checkout@storePayment');

	//	THANK YOU

	Route::get('thankyou/{id}', 'checkout@thankyou');
	Route::get('cancel_order/{id}', 'checkout@cancel_order');

	Route::post('/resend_otp', 'Front@resend_otp');
	Route::post('/resend_otp_changepassword', 'Store@resend_otp');

	Route::post('/roomshow', 'Front@roomshow');
	Route::post('/roomsavailablitycheck', 'Front@roomsavailablitycheck');

	//	My Appointments
	Route::get('orders/', 'Welcome@bookings');
	Route::get('order-info/{id}', 'Welcome@orders_info');

	Route::post('rating', 'Front@user_rating');
	//	Re Booking
	Route::get('re-book-order/{id}', 'checkout@re_book_order');

	//	Wallet
	Route::get('wallet','Welcome@user_wallet');
	Route::post('add-wallet', 'Welcome@add_wallet');

	//	Vendor Orders
    Route::get('vendors/orders/index', 'Store@orders');
    // Route::get('vendors/orders/info/{id}', 'Store@order_info');
    Route::post('vendors/orders/update-status', 'Store@update_status');

    //	Manager Orders
    Route::get('managers/orders/index', 'Store@Manager_orders');

    //Fund Requests - vendor
    Route::get('vendors/fund_requests', 'CustomController@fundRequestIndex');
    Route::get('vendors/fund_requests/create', 'CustomController@createfundRequest');
    Route::post('create_fund_requests','CustomController@storefundRequest');
	

	//Fund Requests - Admin
	Route::get('admin/fund_requests', 'CustomController@adminFundRequest');
	Route::post('update_fund_requests', 'CustomController@updatefundRequest');
	Route::post('update_fund_payment/{id}','CustomController@updatePaymentMethod');
	Route::post('reject_fund_requests','CustomController@rejectPaymentRequest');

    //	Admin Orders
    Route::get('admin/orders/index', 'Vendor@orders');
    Route::get('admin/showbooking/{id}', 'Booking@showAllBooking');
    Route::post('admin/orders/update-status', 'Store@order_update_status');

    //Vendor Coupon Management
    Route::get('vendor/coupons', 'Coupon_vendor@index');
    Route::get('vendor/coupons/create', 'Coupon_vendor@create');
    Route::post('vendor/create_coupon', 'Coupon_vendor@store');
    Route::get('vendor/coupons/edit/{id}', 'Coupon_vendor@edit');
    Route::post('vendor/coupons/update/{id}', 'Coupon_vendor@update');
    Route::get('vendor/coupons/view/{id}', 'Coupon_vendor@show');
    Route::get('vendor/coupons/delete/{id}', 'Coupon_vendor@delete');
    Route::post('c_list/coupon_outlet_list', 'Coupon_vendor@getAllVendorOutletList');
    Route::post('c_list/coupon_product_list', 'Coupon_vendor@getAllOutletProductList');
		

    // Route::get('vendors/orders/load_history/{id}', 'Store@load_history');
    // Route::get('vendors/create-order', 'Store@create_order');
	Route::post('/update_user', 'Front@update_user');
	Route::post('/signin_user', 'Front@signin_user');
	Route::post('/forgot_password', 'Front@forgot_password');
	Route::get('/view-hotel/{url_index}', 'Front@view_property');
	Route::post('/view-hotel/review_comments','Front@review_comments');
	Route::get('/checkout/{outlet_url_index}/{room_url_index}','Front@checkout');

	Route::get('/refer_friends','Front@user_referral');
	Route::get('/referral_signup/{id}','Front@signup_referral_user');
	
	
	Route::resource('/list-hotels/{city_category}/{url_index}', 'Property');





