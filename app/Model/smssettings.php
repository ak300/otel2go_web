<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class smssettings extends Model 
{
	public $timestamps  = true;
	protected $table = 'smssettings';
}
