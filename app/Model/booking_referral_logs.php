<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class booking_referral_logs extends Model
{
	public $timestamps  = true;
	protected $table = 'booking_referral_logs';
}
