<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;


class cart_outlet extends Model 
{    
    protected $table = 'cart_outlet';
    protected $primaryKey = 'cart_outlet_id';
}
