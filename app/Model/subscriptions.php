<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class subscriptions extends Model
{
	public $timestamps  = false;
	protected $table = 'subscriptions';
    
	/* Payment gateway list */
    public static function get_payment_gateways($language_id, $payment_gateway_id = '')
    {
        $query = 'payment_gateways_info.language_id = (case when (select count(payment_gateways_info.language_id) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $gateways = DB::table('payment_gateways')
                ->select('payment_gateways.payment_type','payment_gateways_info.name','payment_gateways.id as payment_gateway_id','payment_gateways.merchant_key','payment_gateways.account_id','payment_gateways.payment_mode','payment_gateways.commision','currencies.currency_code')
                ->leftJoin('payment_gateways_info','payment_gateways_info.payment_id','=','payment_gateways.id')
                ->leftJoin('currencies','currencies.id','=','payment_gateways.currency_id')
                ->where('payment_gateways.active_status','=','1');
        if($payment_gateway_id != '')
        {
            $gateways = $gateways->where('payment_gateways.id', '=', $payment_gateway_id);
        }
        $gateways = $gateways->whereRaw($query)->orderBy('payment_gateways.id', 'desc')->get();
        return $gateways;
    }
}
