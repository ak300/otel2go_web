<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class customers_view extends Model
{
	public $timestamps  = false;
	public $table  = 'customers_view';
    //public $primarykey = 'id';
}
