<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class expense_infos extends Model
{
  public $timestamps  = false;
  public $table  = 'expenses_infos';
  private $_languages = array();
  private $_cityLables = array();
  protected $primaryKey = 'info_id';
  //public $primarykey = 'id';
    
    public function getLabel($feild,$language_id=1,$id='') 
    {
      $label=$this->getExpenseLabels($feild,$language_id,$id);
      return isset($label->$feild) ? $label->$feild:''; 
  }
  
  public function getExpenseLabels($feild,$language_id,$id) 
    {
    if(!isset($this->_cityLables[$language_id])) {
            $this->_cityLables[$language_id] = array();
        }
        if(empty($this->_cityLables[$language_id])) {
            $amenityLables = DB::table('expenses_infos')
        ->where('expenses_infos.id','=',$id)
        ->where('expenses_infos.language_id','=',$language_id)
        ->get();
            $citysL = array();
            foreach($amenityLables as $coul) {
                $citysL[$coul->id] = $coul;
            }
            $this->_cityLables[$language_id] = $citysL;
        }
        return $id && isset($this->_cityLables[$language_id]) && isset($this->_cityLables[$language_id][$id]) ?
                            $this->_cityLables[$language_id][$id]:'';
  }
}
