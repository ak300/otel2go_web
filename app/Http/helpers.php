<?php  $_taskslist = array();
/**
 * Contains the most low-level helpers methods in Kohana:
 *
 * - Environment initialization
 * - Locating files within the cascading filesystem
 * - Auto-loading and transparent extension of classes
 * - Variable and path debugging
 *
 * @package    Laravel
 * @category   Base
 * @copyright  Copyright © Taylor Otwell
 * @license    https://laravel.com/license
 */

	/** get current Language **/
	function getCurrentLang()
	{
		$currentlanguages = DB::table('languages')->where('status', 1)->where('language_code',App::getLocale())->get();
		$current_language_id='';
		if(count($currentlanguages)>0)
		{
			$current_language_id = $currentlanguages[0]->id;
		}
		return $current_language_id;
	}
	/** get current Language **/
	function getAdminCurrentLang()
	{
		/** $currentlanguages = DB::table('languages')->where('status', 1)->where('language_code',App::getLocale())->get();
		$current_language_id='';
		if(count($currentlanguages)>0){
			$current_language_id = $currentlanguages[0]->id;
		}
		return $current_language_id;
		$current_language_id=1;
		if(App::getLocale()=='en'){
			$current_language_id = 1; 
		}
		if(App::getLocale()=='ar'){
			$current_language_id = 2; 
		}*/
		return 1;
	}
	//Get balance details for commision and amount
	function getBalanceData($user_id,$property_id,$type=0)
	{
		if($type==1){ //Vendor
			//   $vdata = DB::table('vendors')->select('current_balance')->where('id', $user_id)->get();

            $vdata = DB::table('booking_details')
                            ->select(DB::Raw('sum(payments) as current_balance'))
                            ->where('booking_details.vendor_id','=',$user_id)
                            ->where('booking_details.outlet_id','=',$property_id)
                            ->get();

            $vendor_bal = DB::table('booking_details')
                            ->select(DB::Raw('sum(payments) as current_balance'))
                            ->where('booking_details.vendor_id','=',$user_id)
                            //  ->where('booking_details.outlet_id','=',$property_id)
                            ->get();                  
             // dd($vendor_bal[0]->current_balance);

            //  dd($vendors);

            if($vdata[0]->current_balance !="")
            {
                $data = array('vendor_balance' => $vdata[0]->current_balance,'admin_balance'=>0 );
            }
            else
            {
                $data = array('vendor_balance' => 0,'admin_balance'=>0 );
            }

		} else{
			$vdata = DB::table('vendors')->sum('current_balance');
			$adata = DB::table('customers_view')->select('current_balance')->where('id', 1)->get();
			$data = array('vendor_balance' => $vdata,'admin_balance'=>$adata[0]->current_balance);
		}
		return $data;
	}

    function getExpenseData($user_id,$property_id,$type=0)
    {
        if($type==1){ //Vendor
            //   $vdata = DB::table('vendors')->select('current_balance')->where('id', $user_id)->get();

            $vdata = DB::table('expenses')
                            ->select(DB::Raw('sum(expense_cost) as expense'))
                            ->where('expenses.created_by','=',$user_id)
                            ->where('expenses.property_id','=',$property_id)
                            ->get(); 
              //dd($vdata);
            if($vdata[0]->expense !="")
            {
                $data = array('expense' => $vdata[0]->expense,'admin_balance'=>0 );
            }
            else
            {
                $data = array('expense' => 0,'admin_balance'=>0 );
            }                            

            
        } else{
            $vdata = DB::table('vendors')->sum('current_balance');
            $adata = DB::table('customers_view')->select('current_balance')->where('id', 1)->get();
            $data = array('vendor_balance' => $vdata,'admin_balance'=>$adata[0]->current_balance);
        }
        return $data;
    }

    function getNotificationsList($user_id)
    {
    	$notifications = DB::table('notifications')
							->select('notifications.id','notifications.order_id','notifications.message','notifications.created_date','notifications.read_status','customers_view.name','customers.image')
							->leftJoin('customers_view','customers_view.id','=','notifications.customer_id')
                            ->leftJoin('customers','customers.user_id','=','customers_view.id')
							->where('notifications.read_status', 0);
		if($user_id!=1)
		{
			$notifications = $notifications->where('vendor_id', $user_id);
		}
		$notifications = $notifications->orderBy('created_date', 'desc')->get();
		return $notifications;
    }
    /** get country list **/
	function getCountryLists()
	{
		
		$country_query = '"countries_infos"."language_id" = (case when (select count(*) as totalcount from countries_infos where countries_infos.language_id = '.getCurrentLang().' and countries.id = countries_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$countries = DB::table('countries')
			->select('countries.id','countries_infos.country_name')
			->leftJoin('countries_infos','countries_infos.id','=','countries.id')
			->whereRaw($country_query)
			->where('country_status', 1)
			->orderBy('country_name', 'asc')
			->get();
		$country_list=array();
		if(count($countries)>0){
			$country_list = $countries;
		}
		return $country_list; 
	}
    /*
     * To set task list for all modules
     */
    function merchanttasks()
    {
        return array(
            /** roles and users module task **/
              'permissions' => array(
                'sort'  => '2',
                'title' => trans('messages.Permissions'),
                'children'  => array(
                    'roles' => array(
                        'title' => trans('messages.Roles'),
                        'sort'  => '2',
                        'task_note'  => trans('messages.Managing the roles'),
                        'task_index' => '["vendor/permission","permission/users"]',
                        'apiresources'    => array (
                            'permissions' => array(
                                'title'   => trans('messages.Roles'),
                                'description' => 'Managing Roles',
                                'resource'    => array (
                                    'GET','POST','PUT','DELETE'
                                )
                            ),
                        ),
                    ),
                    'roles/edit' => array(
                        'title' => trans('messages.Roles Edit'),
                        'sort'  => '3',
                        'task_note'  => trans('messages.Edit,delete the roles and their tasks'),
                        'task_index' => '["vendor/permission/create", "vendor/permission/edit", "vendor/update_role", "vendor/rolecreate", "vendor/permission/delete"]'
                    ),
                    'users' => array(
                        'title' => trans('messages.Roles Users'),
                        'sort' => '4',
                        'task_note' => trans('messages.Managing the users'),
                        'task_index' => '["vendor/users"]',
                    ),
                    'edit' => array(
                        'title' => trans('messages.Roles User Edit'),
                        'sort' => '5',
                        'task_note' => trans('messages.Edit,delete the role users'),
                        'task_index' => '["vendor/usercreate", "vendor/userstore", "vendor/users/edit", "usersupdate", "vendor/users/delete"]'
                    ),
                ),
            ), 
            'salons' => array(
                'sort' => '2',
                'title' => trans('messages.Salons'),
                'children' => array(
                    'list' => array(
                        'title' => trans('messages.Salons'),
                        'sort' => '2',
                        'task_note' => trans('messages.Managing the Salons'),
                        'task_index' => '["vendor/salons","vendor/salon_details","vendor/salons"]',
                        'apiresources' => array (
                            'salons' => array(
                                'title' => trans('messages.Salons'),
                                'description' => 'Managing Salons',
                                'resource' => array (
                                    'GET','POST','PUT','DELETE'
                                )
                            ),
                        ),
                    ),
                    'salons/edit' => array(
                        'title' => trans('messages.Salons Edit'),
                        'sort' => '3',
                        'task_note' => trans('messages.Edit,delete the salons and their tasks'),
                        'task_index' => '["vendor/salon_details","vendor/create_salon","vendor/edit_salon","vendor/update_salon","vendor/create_salon","vendor/delete_salon"]'
                    ),
                ),
            ),
           /* 'staffs' => array(
                'sort' => '2',
                'title' => trans('messages.Staffs'),
                'children' => array(
                    'list' => array(
                        'title' => trans('messages.Staffs'),
                        'sort' => '2',
                        'task_note' => trans('messages.Managing the Staffs'),
                        'task_index' => '["vendor/staffs"]',
                        'apiresources' => array (
                            'staffs' => array(
                                'title' => trans('messages.Staffs'),
                                'description' => 'Managing Staffs',
                                'resource' => array (
                                    'GET','POST','PUT','DELETE'
                                )
                            ),
                        ),
                    ),
                    'staffs/edit' => array(
                        'title' => trans('messages.Staffs Edit'),
                        'sort' => '3',
                        'task_note' => trans('messages.Edit,delete the staffs and their tasks'),
                        'task_index' => '["vendor/create-staffs","vendor/edit_staffs","vendor/staffs/update","vendor/create_staffs_store","vendor/delete_staffs"]'
                    ),
                ),
            ),*/
            'products' => array(
                'sort' => '2',
                'title' => trans('messages.Products'),
                'children' => array(
                    'list' => array(
                        'title' => trans('messages.Products'),
                        'sort' => '2',
                        'task_note' => trans('messages.Managing the Products'),
                        'task_index' => '["vendor/products","vendor/products/product_details"]',
                        'apiresources' => array (
                            'products' => array(
                                'title' => trans('messages.Products'),
                                'description' => 'Managing Products',
                                'resource' => array (
                                    'GET','POST','PUT','DELETE'
                                )
                            ),
                        ),
                    ),
                    'products/edit' => array(
                        'title' => trans('messages.Products Edit'),
                        'sort' => '3',
                        'task_note' => trans('messages.Edit,delete the products and their tasks'),
                        'task_index' => '["vendor/products/create_product","vendor/products/edit_product","vendor/update_product","vendor/product_create","vendor/products/delete_product","vendor/products/product_details"]'
                    ),
                ),
            ),
            'coupons' => array(
                'sort' => '2',
                'title' => trans('messages.Coupons'),
                'children' => array(
                    'list' => array(
                        'title' => trans('messages.Coupons'),
                        'sort' => '2',
                        'task_note' => trans('messages.Managing the Coupons'),
                        'task_index' => '["vendor/coupons"]',
                        'apiresources' => array (
                            'products' => array(
                                'title' => trans('messages.Coupons'),
                                'description' => trans('messages.Managing Coupons'),
                                'resource' => array (
                                    'GET','POST','PUT','DELETE'
                                )
                            ),
                        ),
                    ),
                    'coupons/edit' => array(
                        'title' => trans('messages.Coupons Edit'),
                        'sort' => '3',
                        'task_note' => trans('messages.Add the coupons types and their tasks'),
                        'task_index' => '["vendor/coupons/create","vendor/create_coupon","vendor/coupons/edit","vendor/coupons/delete"]'
                    ),
                ),
            ),
            'reviews' => array(
                'sort' => '2',
                'title' => trans('messages.Reviews'),
                'children' => array(
                    'reviews' => array(
                        'title' => trans('messages.Reviews'),
                        'sort'  => '2',
                        'task_note'  => trans('messages.Managing the reviews'),
                        'task_index' => '["vendors/reviews"]',
                    ),
                    'vendor/reviews/view' => array(
                        'title' => trans('messages.View Reviews'),
                        'sort' => '3',
                        'task_note' => trans('messages.View reviews and their tasks'),
                        'task_index' => '["vendors/reviews/view","vendors/reviews/approve","vendors/reviews/delete"]'
                    )
                ),
            ),
            'sales' => array(
                'sort' => '2',
                'title' => trans('messages.Sales'),
                'children' => array(
                    'orders/index' => array(
                        'title' => trans('messages.Orders'),
                        'sort' => '2',
                        'task_note' => trans('messages.Orders List'),
                        'task_index' => '["vendors/orders/index"]',
                    ),
                    'orders/info' => array(
                        'title' => trans('messages.View Orders'),
                        'sort' => '3',
                        'task_note' => trans('messages.View orders and their tasks'),
                        'task_index' => '["vendors/orders/update-status","vendors/orders/info","vendors/orders/load_history","vendors/orders/delete","vendors/orders/info"]'
                    ),
                    /**'orders/return_orders' => array(
                        'title' => trans('messages.Return Orders'),
                        'sort' => '4',
                        'task_note' => trans('messages.Return Orders List'),
                        'task_index' => '["vendors/return_orders"]',
                    ),
                    'orders/return_orders_view' => array(
                        'title' => trans('messages.View Return Orders'),
                        'sort' => '5',
                        'task_note' => trans('messages.View orders and their tasks'),
                        'task_index' => '["vendors/return_orders_view","update_return_order"]'
                    ),*/
                    'orders/fund_requests' => array(
                        'title' => trans('messages.Fund Requests'),
                        'sort' => '6',
                        'task_note' => trans('messages.Fund Requests List'),
                        'task_index' => '["vendors/request_amount/index","vendors/request_amount/create","createamount","vendors/view_fund_status"]',
                    ),

                ),
            ),
            'reports' => array(
                'sort' => '2',
                'title' => trans('messages.Orders Reports'),
                'children' => array(
                    'reviews' => array(
                        'title' => trans('messages.Orders Reports'),
                        'sort'  => '2',
                        'task_note'  => trans('messages.Managing the order reports'),
                        'task_index' => '["vendors/report_orders"]',
                    ),
                    'vendors/report_products' => array(
                        'title' => trans('messages.Product Reports'),
                        'sort' => '3',
                        'task_note' => trans('messages.Managing the product reports'),
                        'task_index' => '["vendors/report_products"]'
                    )
                ),
            ),
            'user_notification' => array(
                'sort' => '2',
                'title' => trans('messages.Notifications'),
                'children' => array(
                    'notifications' => array(
                        'title' => trans('messages.Notifications'),
                        'sort'  => '2',
                        'task_note'  => trans('messages.Managing the notifications'),
                        'task_index' => '["vendors/notifications", "vendors/read_notifications"]',
                    ),
                ),
            ),
			'job' => array(
                'sort' => '2',
                'title' => trans('messages.Job'),
                'children' => array(
                    'list' => array(
                        'title' => trans('messages.Job'),
                        'sort' => '2',
                        'task_note' => trans('messages.Managing the job'),
                        'task_index' => '["admin/job"]',
                    ),
                    'job/edit' => array(
                        'title' => trans('messages.Job Edit'),
                        'sort' => '3',
                        'task_note' => trans('messages.Edit,delete the job and their tasks'),
                        'task_index' => '["admin/job/create","createjob","admin/job/edit","updatejob","admin/job/delete","admin/job/view"]'
                    ),
                ),
            ),
			'gallery' => array(
                'sort' => '2',
                'title' => trans('messages.Gallery'),
                'children' => array(
                    'gallery' => array(
                        'title' => trans('messages.Gallery'),
                        'sort'  => '2',
                        'task_note'  => trans('messages.Managing the Gallery'),
                        'task_index' => '["vendor/gallery"]',
                    ),
                ),
            ),
			'calendar' => array(
                'sort' => '2',
                'title' => trans('messages.Calendar'),
                'children' => array(
                    'calendar' => array(
                        'title' => trans('messages.Calendar'),
                        'sort'  => '2',
                        'task_note'  => trans('messages.Managing the Calendar'),
                        'task_index' => '["vendors/calendar"]',
                    ),
                ),
            ),
			'editprofile' => array(
                'sort' => '2',
                'title' => trans('messages.Editprofile'),
                'children' => array(
                    'editprofile' => array(
                        'title' => trans('messages.Editprofile'),
                        'sort'  => '2',
                        'task_note'  => trans('messages.Editprofile'),
                        'task_index' => '["vendors/editprofile"]',
                    ),
                ),
            ),
			'customers' => array(
                'sort' => '2',
                'title' => trans('messages.Customers'),
                'children' => array(
                    'users' => array(
                        'title' => trans('messages.Customers'),
                        'sort'  => '2',
                        'task_note'  => trans('messages.Customers'),
                        'task_index' => '["vendors/users"]',
                    ),
                ),
            ),
			'billing' => array(
                'sort' => '2',
                'title' => trans('messages.Billing'),
                'children' => array(
                    'users' => array(
                        'title' => trans('messages.Billing'),
                        'sort'  => '2',
                        'task_note'  => trans('messages.Billing'),
                        'task_index' => '["vendors/billing"]',
                    ),
                ),
            ),

        );
    }
	
	
	/** get category list **/
	function getCategoryLists($category_type,$parentOnly = null)
	{
		//Get the categories data
		$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$query  	= DB::table('categories')
							->select('categories.id','categories.image','categories.category_white_image','categories_infos.category_name','categories.url_key')
							->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
							->whereRaw($category_query)
							->where('category_status',  1);

							if(isset($parentOnly) && ($parentOnly === true) ){
								//getting parent categories only
								$query = $query->where('parent_id',  0);
							}
						 
		$categories = $query->where('category_type',$category_type)
							->orderBy('categories.sort_order', 'asc')
							->get();
		$categories_list = array();
		if(count($categories)>0)
		{
			$categories_list = $categories;
		}
		return $categories_list;
	}
	
	function getMainCategoryLists($category_id)
	{

		
		$categories_list=array();
		if($category_id !="")
		{
			//Get the categories data
			$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = 1 and categories.id = categories_infos.category_id) > 0 THEN 1 ELSE 1 END)';
			$categories=DB::table('categories')
				->select('categories.id','categories_infos.category_name','categories.url_key')
				->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
				->whereRaw($category_query)
				->where('category_status',  1)
				->where('category_level','=',2)
				->where('id','<>',$category_id)
				->where('parent_id', $category_id) //getting parent categories only
				->orderBy('categories_infos.category_name', 'asc')
				->get();
				//print_r($categories);exit;
			if(count($categories)>0)
			{
				$categories_list = $categories;
			}
		}
		return $categories_list;
	}


	function getMainCategoryLists1()
	{

		
		$categories_list=array();
			//Get the categories data
			$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
			$categories=DB::table('categories')
				->select('categories.id','categories_infos.category_name','categories.url_key')
				->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
				->whereRaw($category_query)
				->where('category_status',  1)
				->where('category_level','=',2)
				->where('category_type','=',1)
				//->where('id','<>',$category_id)
				//->where('parent_id', $category_id) //getting parent categories only
				->orderBy('categories_infos.category_name', 'asc')
				->get();
				//print_r($categories);exit;
			if(count($categories)>0)
			{
				$categories_list = $categories;
			}

		return $categories_list;
	}

	

	function getProductMainCategoryLists($category_id)
	{


		
		$categories_list=array();
		if($category_id !="")
		{
			$ids=implode(',',$category_id);
			//Get the categories data
			$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = 1 and categories.id = categories_infos.category_id) > 0 THEN 1 ELSE 1 END)';
			$categories=DB::table('categories')
				->select('categories.id','categories_infos.category_name','categories.url_key')
				->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
				->whereRaw($category_query)
				->where('category_status',  1)
				->where('category_type','=',1)
				->where('category_level','=',2)
				//->where('id','<>',$category_id)
				->whereRaw('parent_id IN('.$ids.')' ) //getting parent categories only
				->orderBy('categories_infos.category_name', 'asc')
				->get();
				//print_r($categories);exit;
			if(count($categories)>0)
			{
				$categories_list = $categories;
			}
		}
		return $categories_list;
	}
	
	
	
	/** get category details by category id**/
	function getCategoryListsById($category_id)
	{
		//Get the categories data
		$category_query = '"categories_infos"."language_id" = (case when (select count(category_id) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$categories = DB::table('categories')
						->select('categories.id','categories_infos.category_name','categories.url_key')
						->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
						->whereRaw($category_query)
						->where('categories.id',$category_id)
						->first();
		$categories_list = array();
		if(count($categories)>0)
		{
			$categories_list = $categories;
		}
		return $categories_list;
	}
	/** get category list **/
	function getSubCategoryLists($category_type,$parent_id,$category_url='')
	{
		//echo $parent_id;exit;
		//Get the categories data
		$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$categories=DB::table('categories')
			->select('categories.id','categories.url_key','categories_infos.category_name')
			->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
			->whereRaw($category_query)
			->where('category_status',  1);
		if(!empty($parent_id))
		{
			$categories = $categories->where('parent_id',  $parent_id);//getting parent categories only
		}
		if(!empty($category_url))
		{
			$categories = $categories->where('categories.url_key',  $category_url);//getting parent categories only
		}
		$categories = $categories->where('category_type',$category_type)
			->where('category_level',2)
			->orderBy('category_name', 'asc')
			->get();
		$categories_list=array();
		if(count($categories)>0){
			$categories_list = $categories;
		}
		return $categories_list;
	}


		/** get category list **/
	function getSubCategoryLists1($category_type,$parent_id,$category_url='',$language='')
	{


	
		//Get the categories data
		//$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = 1 and categories.id = categories_infos.category_id) > 0 THEN 1 ELSE 1 END)';

		if($language){
		
			$category_query  = '"vendors_infos"."lang_id" = (case when (select count(vendors_infos.id) as totalcount from vendors_infos where vendors_infos.lang_id = '.$language.' and vendors.id = vendors_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
			
		}else {
			$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		}

		
		
		
		$categories=DB::table('categories')
			->select('categories.id','categories.url_key','categories_infos.category_name')
			->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
			->whereRaw($category_query)
			->where('category_status',  1);
		if(!empty($parent_id))
		{
			$categories = $categories->where('parent_id',  $parent_id);//getting parent categories only
		}
		if(!empty($category_url))
		{
			$categories = $categories->where('categories.url_key',  $category_url);//getting parent categories only
		}
		$categories = $categories->where('category_type',$category_type)
			->orderBy('category_name', 'asc')
			->get();
		$categories_list=array();
		if(count($categories)>0){
			$categories_list = $categories;
		}
		return $categories_list;
	}

			/** get category list **/
	function getSubCategoryListsupdated($category_type,$parent_id,$category_url='',$language='',$head_category='')
	{
		
		//echo $parent_id;exit;
		//Get the categories data
		//$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = 1 and categories.id = categories_infos.category_id) > 0 THEN 1 ELSE 1 END)';
		if($language){
			
			$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.$language.' and categories.id = categories_infos.category_id) > 0 THEN '.$language.' ELSE 1 END)';
			
		}else {
			
			$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		}
		
		$categories=DB::table('categories')
			->select('categories.id','categories.url_key','categories_infos.category_name','head_category_ids as parent_id')
			->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
			->whereRaw($category_query)
			->where('category_status',  1);
		if(!empty($parent_id))
		{ 
			$categories = $categories->where('head_category_ids',  $parent_id);//getting parent categories only
			$categories = $categories->where('category_level',  3);
		}
		if(!empty($head_category))
		{ 
			$categories = $categories->where('parent_id',  $head_category);//getting parent categories only
		}
		if(!empty($category_url))
		{
			$categories = $categories->where('categories.url_key',  $category_url);//getting parent categories only
		}
		$categories = $categories->where('category_type',$category_type)
			->orderBy('category_name', 'asc')
			->get();
		$categories_list=array();
		if(count($categories)>0){
			$categories_list = $categories;
		}
		return $categories_list;
	}
	
	/** get city list **/
	function getCityList($country_id="1")
	{
		//Get the cities data
		$city_query = '"cities_infos"."language_id" = (case when (select count(*) as totalcount from cities_infos where cities_infos.language_id = '.getCurrentLang().' and cities.id = cities_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$cities=DB::table('cities')
			->select('cities.id','cities_infos.city_name')
			->leftJoin('cities_infos','cities_infos.id','=','cities.id')
			->leftJoin('countries','countries.id','=','cities.country_id')
			->whereRaw($city_query)
			->where('active_status', 'A')
			->where('default_status', 1)
			->where('countries.id', 1)
			->orderBy('city_name', 'asc')
			->get();
		$cities_list=array();
		if(count($cities)>0){
			$cities_list = $cities;
		}
		return $cities_list;
	}
	
    
    /** get city list **/
    function getRoomList($roomtype_id,$ac,$cc)
    {
        //Get the rooms data
        //  print_r($roomtype_id);exit;


        $room_query = '"rooms_infos"."language_id" = (case when (select count(*) as totalcount from rooms_infos where rooms_infos.language_id = '.getCurrentLang().' and rooms.id = rooms_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
        $rooms=DB::table('rooms')
            ->select('rooms.id','rooms_infos.room_name')
            ->leftJoin('rooms_infos','rooms_infos.id','=','rooms.id')
            ->leftJoin('room_type','room_type.id','=','rooms.room_type_id')
            ->whereRaw($room_query)
            //  ->where('rooms.active_status', 'A')
            //  ->where('default_status', 1)
            ->where('adult_count','>=',$ac)
            ->where('child_count','>=',$cc)            
            ->where('room_type.id', $roomtype_id)
            ->orderBy('room_name', 'asc')
            ->get();

        $rooms_list=array();
        if(count($rooms)>0){
            $rooms_list = $rooms;
        }
        return $rooms_list;
    }

    /** get Order Status list **/
    function getOrderStatus()
    {

        $order_status = DB::table('booking_status')
                                ->select('id','name')->orderBy('id', 'asc')->get();

        //  dd($order_status);

        $order_status_list=array();
        if(count($order_status)>0){
            $order_status_list = $order_status;
        }
        return $order_status_list;
    }

    /** get 5 city list **/
    function getTopCities($count)
    {

        $top_cities = DB::table('cities')
                        ->select('cities.id','cities.url_index','cities.city_image','cities_infos.city_name')
                        ->leftJoin('cities_infos','cities_infos.id','cities.id')
                        ->groupBy('cities.id','cities_infos.city_name')
                        ->orderBy('cities.id')
                        ->take($count)
                        ->get();

          //    dd($top_cities);

        $top_cities_list=array();
        if(count($top_cities)>0){
            $top_cities_list = $top_cities;
        }

        //  print($top_cities_list);exit;
        return $top_cities_list;
    }     

    /** get settings list values for footer */
    function getSettingsDetails(){

        // $settings = Settings::find(id);
       $settings = DB::table('settings')
                            ->select('telephone','email')
                            ->first();

        return $settings;
    }

    /** get city list **/
    function getRoomcost()
    {
        //Get the rooms data
        //  print_r($roomtype_id);exit;

        $order_status = DB::table('booking_status')
                                ->select('id','name')->orderBy('id', 'asc')->get();

        //  dd($order_status);

        $order_status_list=array();
        if(count($order_status)>0){
            $order_status_list = $order_status;
        }
        return $order_status_list;
    }


    /** get Room Booked Guest list **/
    function getRoomBookedGuestList(/*  $guest_name */ $guest_email)
    {
        //Get the rooms data
        //   print_r($guest_email);exit;


        $customers = DB::table('admin_customers')
                        ->select('admin_customers.*')
                        ->where('email','=',$guest_email)
                        //  ->where('status','=',1)
                        ->first();        
          //        print_r($customers);exit;
        $customers_list=array();
        if(count($customers)>0){
            $customers_list = $customers;
        }
        //  print_r($customers_list);exit;
        return $customers_list;
    }


    /** get Room list **/
    function getRoomBookList($room_type_id,$outlet_id)
    {
        //Get the rooms data
        //   print_r($guest_email);exit;


        $customers = DB::table('rooms')
                        ->select('id')
                        ->where('room_type_id',$room_type_id)
                        ->where('property_id',$outlet_id)
                        ->orderBy('id')
                        ->first();        
          //    print_r(array($customers));exit;
        $customers_list=array();
        if(count($customers)>0){
            $customers_list = $customers;
        }
        //  print_r($customers_list);exit;
        return $customers_list;
    }

    function getStaffReportSummary($st,$ed,$manager_id)
    {

        if(isset($st) && isset($ed))
        {        
            $st = date('Y-m-d',strtotime($st));
            $ed = date('Y-m-d',strtotime($ed));
        }
        else
        {
            $st = date('Y-m-01',strtotime('this month'));
            $ed = date('Y-m-t',strtotime('this month'));             
        }

        

        $categories_list=array();
               
        $property_id = Session::get('property_id');
        $vendor_id = Session::get('vendor_id');

        //  dd($property_id);      

//  BEFORE CREATED DATE WAS USED FOR GETTING DATA, BUT HAD A ISSUE WITH TIMESTAMP, SO NEW FIELD-> ROOM_BOOKED_DATE


        $outlets_query = "SELECT * FROM  (
                               SELECT day::date
                               FROM   generate_series( '$st'::timestamp, '$ed'::timestamp
                                                    , interval  '1 day') day
                               ) d";
            $count = DB::select($outlets_query);                     
              //    dd($count);

            $gs = array();

            foreach($count as $key => $value)
            {
                $all_array = array();
                $all_array['date'] = $value->day;
                //  $all_array['booking_count'] = ($value->booking_count !="")?$value->booking_count:0;


                //  TOTAL BOOKINGS

                $booking_details = DB::table('booking_details')
                                     ->selectRaw('count(id) AS booking_count')
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     ->where('manager_id',$manager_id)
                                     ->get()->toArray();
                                     // ->count();

               //    dd($booking_details); 

                $all_array['booking_count'] = ($booking_details[0]->booking_count !="")?$booking_details[0]->booking_count:0;               

                //  TOTAL CHECKED IN

                $checked_in = DB::table('booking_details')
                                     ->selectRaw('count(id) AS checked_in_count')
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     ->where('manager_id',$manager_id)
                                     ->where('booking_status',2)
                                     ->get()->toArray();
                $all_array['checked_in'] =  ($checked_in[0]->checked_in_count != "")?$checked_in[0]->checked_in_count:0;

                //  TOTAL CHECKED OUT

                $checked_out = DB::table('booking_details')
                                     ->selectRaw('count(id) AS checked_out_count')
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     ->where('manager_id',$manager_id)
                                     ->where('booking_status',3)
                                     ->get()->toArray();
                $all_array['checked_out'] = ($checked_out[0]->checked_out_count != "")?$checked_out[0]->checked_out_count:0;

                //  TOTAL PENDING

                $pending = DB::table('booking_details')
                                     ->selectRaw('count(id) AS pending_count')
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     ->where('manager_id',$manager_id)
                                     ->where('booking_status',5)
                                     ->get()->toArray();
                $all_array['pending'] = ($pending[0]->pending_count != "")?$pending[0]->pending_count:0; 


                //  TOTAL CANCELLED

                $cancelled = DB::table('booking_details')
                                     ->selectRaw('count(id) AS cancelled_count')
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     ->where('manager_id',$manager_id)
                                     ->where('booking_status',4)
                                     ->get()->toArray();
                $all_array['cancelled'] = ($cancelled[0]->cancelled_count != "")?$cancelled[0]->cancelled_count:0; 

                //  TOTAL BOOKING CHARGES

                $booking_charges = DB::table('booking_charges')
                                     ->selectRaw('sum(price) as booking_charge')
                                     ->where('created_date',$value->day)
                                     ->where('property_id',$property_id)
                                     ->where('manager_id',$manager_id)
                                     ->get()->toArray();
                $all_array['booking_charges'] =  ($booking_charges[0]->booking_charge != "")?$booking_charges[0]->booking_charge:0;

                //  TOTAL BOOKING PAYMENTS

                $payments = DB::table('payments')
                               ->selectRaw('sum(paid_amount) as paid_amount')
                               ->where('created_date',$value->day)
                               ->where('property_id',$property_id)
                               ->where('manager_id',$manager_id)
                               ->get()
                               ->toArray();

                $all_array['payments'] =($payments[0]->paid_amount !="")?$payments[0]->paid_amount:0;

                $gs[] = $all_array ;
            }


                 // print_r($gs);exit;
            if(count($gs)>0)
            {
                $categories_list = $gs;
            }

        return $categories_list;
    }  


    function getReportSummary($st,$ed)
    {

        if(isset($st) && isset($ed))
        {        
            $st = date('Y-m-d',strtotime($st));
            $ed = date('Y-m-d',strtotime($ed));
        }
        else
        {
            $st = date('Y-m-01',strtotime('this month'));
            $ed = date('Y-m-t',strtotime('this month'));             
        }

        

        $categories_list=array();
               
        $property_id = Session::get('property_id');
        $vendor_id = Session::get('vendor_id');

        //  dd($property_id);



        if (Session::get('manager_id')) {
            $property_id = Session::get('manager_outlet');
            $vendor_id = Session::get('manager_vendor');           
        }        

//  BEFORE CREATED DATE WAS USED FOR GETTING DATA, BUT HAD A ISSUE WITH TIMESTAMP, SO NEW FIELD-> ROOM_BOOKED_DATE


        $outlets_query = "SELECT * FROM  (
                               SELECT day::date
                               FROM   generate_series( '$st'::timestamp, '$ed'::timestamp
                                                    , interval  '1 day') day
                               ) d";
            $count = DB::select($outlets_query);                     
              //    dd($count);

            $gs = array();

            foreach($count as $key => $value)
            {
                $all_array = array();
                $all_array['date'] = $value->day;
                //  $all_array['booking_count'] = ($value->booking_count !="")?$value->booking_count:0;


                //  TOTAL BOOKINGS

                $booking_details = DB::table('booking_details')
                                     ->selectRaw('count(id) AS booking_count')
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     ->get()->toArray();
                                     // ->count();

                //  dd($booking_details); 

                $all_array['booking_count'] = ($booking_details[0]->booking_count !="")?$booking_details[0]->booking_count:0;               

                //  TOTAL CHECKED IN

                $checked_in = DB::table('booking_details')
                                     ->selectRaw('count(id) AS checked_in_count')
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     ->where('booking_status',2)
                                     ->get()->toArray();
                $all_array['checked_in'] =  ($checked_in[0]->checked_in_count != "")?$checked_in[0]->checked_in_count:0;

                //  TOTAL CHECKED OUT

                $checked_out = DB::table('booking_details')
                                     ->selectRaw('count(id) AS checked_out_count')
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     ->where('booking_status',3)
                                     ->get()->toArray();
                $all_array['checked_out'] = ($checked_out[0]->checked_out_count != "")?$checked_out[0]->checked_out_count:0;

                //  TOTAL PENDING

                $pending = DB::table('booking_details')
                                     ->selectRaw('count(id) AS pending_count')
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     ->where('booking_status',5)
                                     ->get()->toArray();
                $all_array['pending'] = ($pending[0]->pending_count != "")?$pending[0]->pending_count:0; 


                //  TOTAL CANCELLED

                $cancelled = DB::table('booking_details')
                                     ->selectRaw('count(id) AS cancelled_count')
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     ->where('booking_status',4)
                                     ->get()->toArray();
                $all_array['cancelled'] = ($cancelled[0]->cancelled_count != "")?$cancelled[0]->cancelled_count:0; 

                //  TOTAL BOOKING CHARGES

                $booking_charges = DB::table('booking_charges')
                                     ->selectRaw('sum(price) as booking_charge')
                                     ->where('created_date',$value->day)
                                     ->where('property_id',$property_id)
                                     ->get()->toArray();
                $all_array['booking_charges'] =  ($booking_charges[0]->booking_charge != "")?$booking_charges[0]->booking_charge:0;

                //  TOTAL BOOKING PAYMENTS

                $payments = DB::table('payments')
                               ->selectRaw('sum(paid_amount) as paid_amount')
                               ->where('created_date',$value->day)
                               ->where('property_id',$property_id)
                               ->get()
                               ->toArray();

                $all_array['payments'] =($payments[0]->paid_amount !="")?$payments[0]->paid_amount:0;

                $gs[] = $all_array ;
            }


                 // print_r($gs);exit;
            if(count($gs)>0)
            {
                $categories_list = $gs;
            }

        return $categories_list;
    }  

    
    function getStatisticsSummary($st,$ed)
    {

        if(isset($st) && isset($ed))
        {        
            $st = date('Y-m-d',strtotime($st));
            $ed = date('Y-m-d',strtotime($ed));
        }
        else
        {
            $st = date('Y-m-01',strtotime('this month'));
            $ed = date('Y-m-t',strtotime('this month'));             
        }

        

        $categories_list=array();
               
        $property_id = Session::get('property_id');
        $vendor_id = Session::get('vendor_id');

        //  dd($property_id);



        if (Session::get('manager_id')) {
            $property_id = Session::get('manager_outlet');
            $vendor_id = Session::get('manager_vendor');           
        }        

//  BEFORE CREATED DATE WAS USED FOR GETTING DATA, BUT HAD A ISSUE WITH TIMESTAMP, SO NEW FIELD-> ROOM_BOOKED_DATE


        $outlets_query = "SELECT * FROM  (
                               SELECT day::date
                               FROM   generate_series( '$st'::timestamp, '$ed'::timestamp
                                                    , interval  '1 day') day
                               ) d";
            $count = DB::select($outlets_query);                     
              //    dd($count);

            $gs = array();

            foreach($count as $key => $value)
            {
                $all_array = array();
                $all_array['date'] = $value->day;
                //  $all_array['booking_count'] = ($value->booking_count !="")?$value->booking_count:0;


                //  BOOKING SOURCE - 1

                $bs1 = DB::table('booking_details')
                                     ->where('booking_source',1)
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     // ->get()->toArray();
                                     ->count();

                //  dd($bs1); 

                $all_array['bs1'] = ($bs1 !="")?$bs1:0;


                //  BOOKING SOURCE - 1

                $bs2 = DB::table('booking_details')
                                     ->where('booking_source',2)
                                     ->where('room_booked_date',$value->day)
                                     ->where('outlet_id',$property_id)
                                     // ->get()->toArray();
                                     ->count();

                //  dd($bs1); 

                $all_array['bs2'] = ($bs2 !="")?$bs2:0;

                $gs[] = $all_array ;
            }


                 // print_r($gs);exit;
            if(count($gs)>0)
            {
                $categories_list = $gs;
            }

        return $categories_list;
    }  


    function setSessionData($sd,$ed,$duration,$room_count,$adult_count)
    {
        //  print_r($adult_count);exit;

        Session::put('start_date', $sd);
        Session::put('end_date',   $ed);
        Session::put('guest_count',$adult_count);
        Session::put('room_count', $room_count);
        Session::put('duration',   $duration);  

        $dur = Session::get('room_count');

          //    print_r($end); exit;
           
        return $dur;
    }


    function getRoomBookingList($check_in_date,$check_out_date,$rtd,$cc="",$ac="",$days)
    {
        //  Get the rooms data
        // print_r($check_in_date);  
        // print_r($check_out_date);
        // print_r($days); exit;

        if((Session::get('vendor_id') != ""))
            {
                $property_id = Session::get('property_id');
            }
        else if((Session::get('manager_vendor') !=""))   //  DONE BY AK
            {
                $property_id = Session::get('manager_outlet');
            }


        $date = str_replace('/', '/', $check_in_date);
        $nd = date('Y-m-d', strtotime($date));            

        $cout = str_replace('/', '/', $check_out_date);
        $cnd = date('Y-m-d', strtotime($cout)); 
//  print_r($nd);print_r($cnd); exit;
        $all_rooms = DB::table('rooms')
                        ->select('rooms.id','rooms_infos.room_name')
                        ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                        ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                        ->where('rooms.default_status',1)
                        ->where('room_type.id',$rtd)
                        ->where('adult_count','>=',$ac)
                        ->where('child_count','>=',$cc)
                        ->where('rooms.property_id','=',$property_id)  
                        ->get();

        //  print_r($all_rooms); exit;
        
        $arr = array();
        $i = 0;

        foreach ($all_rooms as $key) {
            
            $arr[$i] = $key->id;
              $i++;
        }

        

        $rooms=DB::table('rooms')
                    ->select('rooms.id as rid','booked_room.id' ,'rooms_infos.room_name')
                    ->leftJoin('rooms_infos','rooms_infos.id','=','rooms.id')
                    ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                    //  ->where('booked_room.check_out_date','>',$nd)
                    ->whereBetween('booked_room.date',array($nd,$cnd))
                    ->where('booked_room.status',1)
                    ->where('rooms.default_status',1)
                    ->get();

        //  print_r($rooms);exit;

        if(!empty($rooms))
        {
            
            $emt2 = array();
            $i = 0;

            foreach($rooms as $r)
            {
                $emt2[$i] = $r->rid;
                $i++;
            }


            $result=array_diff($arr,$emt2);

            $users = DB::table('rooms')
                                ->select('rooms.id','rooms_infos.room_name','room_type.discount_price','room_type.normal_price','room_type.id as room_type_id','room_type_infos.room_type')
                                ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                ->leftJoin('room_type_infos','room_type_infos.id','room_type.id')
                                ->whereIn('rooms.id', $result)
                                ->where('rooms.default_status',1)
                                ->get();

        }

        $rooms_list=array();
        if(count($users)>0){
            $rooms_list = $users;
        }
        //  print_r($rooms_list);exit;
        return $rooms_list;
    }

//  FRONT

    function getRoomFrontBookingList($check_in_date,$check_out_date,$room_count,$rtd,$ac="",$days)
    {
        //  Get the rooms data
        // print_r($check_in_date);  
        // print_r($check_out_date);
        // print_r($days); exit;

       //   return $rtd;


        $date = str_replace('/', '/', $check_in_date);
        $nd = date('Y-m-d', strtotime($date));            

        $cout = str_replace('/', '/', $check_out_date);
        $cnd = date('Y-m-d', strtotime($cout)); 
//  print_r($nd);print_r($cnd); exit;
        $all_rooms = DB::table('rooms')
                        ->select('rooms.id','rooms_infos.room_name')
                        ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                        ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                        ->whereIN('room_type.id',$rtd)
                        ->get();

        $all = DB::table('rooms')->whereIN('room_type.id',$rtd)
                                    ->select(DB::RAW('count(rooms.id) as rid'),'room_type.id')
                                    ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                    ->groupBy('room_type.id')
                                    ->get();

                                    return $all;

          print_r(array($all)); exit;
        
        $arr = array();
        $i = 0;

        foreach ($all_rooms as $key) {
            
            $arr[$i] = $key->id;
              $i++;
        }

        

        $rooms=DB::table('rooms')
                    ->select('rooms.id as rid','booked_room.id' ,'rooms_infos.room_name')
                    ->leftJoin('rooms_infos','rooms_infos.id','=','rooms.id')
                    ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                    //  ->where('booked_room.check_out_date','>',$nd)
                    ->whereBetween('booked_room.date',array($nd,$cnd))
                    ->get();

        if(!empty($rooms))
        {
            
            $emt2 = array();
            $i = 0;

            foreach($rooms as $r)
            {
                $emt2[$i] = $r->rid;
                $i++;
            }


            $result=array_diff($arr,$emt2);

            $users = DB::table('rooms')
                                ->select('rooms.id','rooms_infos.room_name','room_type.discount_price','room_type.normal_price','room_type.id as room_type_id','room_type_infos.room_type')
                                ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                ->leftJoin('room_type_infos','room_type_infos.id','room_type.id')
                                ->whereIn('rooms.id', $result)
                                ->get();

        }

        $rooms_list=array();
        if(count($users)>0){
            $rooms_list = $users;
        }
        //  print_r($rooms_list);exit;
        return $rooms_list;
    }
    /** get city list **/
    function getRoomPriceList($room_id,$room_type)
    {
        //Get the rooms data
          // print_r($room_type);
          // print_r($room_id);exit;


        // $rooms = DB::table('rooms')
        //                 ->select('rooms.id','room_type.discount_price','room_type.normal_price')
        //                 ->leftJoin('room_type','room_type.id','rooms.room_type_id')
        //                 ->where('rooms.id','=',$room_id)
        //                 ->first();
        $rooms = DB::table('room_type')
                        ->select('room_type.id','room_type.discount_price','room_type.normal_price')
                        //  ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                        //  ->where('rooms.id','=',$room_id)
                        ->where('room_type.id','=',$room_type)
                        ->first();                                
                  //    print_r($rooms);exit;
        $price_list=array();
        if(count($rooms)>0){
            $price_list = $rooms;
        }
        //    print_r($price_list);exit;
        return $price_list;
    }





    /** get city list **/
    function getExpensesData($from,$to)
    {
        //Get the rooms data    

        //  return $from;

        $property_id = Session::get('property_id');
        $vendor_id = Session::get('vendor_id');  

        if (Session::get('manager_id')) {

                $property_id = Session::get('manager_outlet');
                $vendor_id = Session::get('manager_vendor');
                
            }   

        //  $condition = '1=1';

        $condition1 = "booked_room.date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                                                  

        $sum_data = DB::table('booking_details')
                    ->select(DB::Raw('sum(charges) as cp, 
                                      sum(payments) as pp'))
                    ->leftJoin('booked_room','booked_room.booking_id','=','booking_details.id')
                    ->where('booking_details.vendor_id','=',$vendor_id)
                    ->where('booking_details.outlet_id','=',$property_id)
                     // ->where('booking_details.created_date','>=',$from)
                     // ->where('booking_details.created_date','<=',$to)
                    ->whereRaw($condition1)
                    ->get();
        //  dd($sum_data);
        $price_list=array();
        if(count($sum_data)>0){
            $price_list = $sum_data;
        }
        //    print_r($price_list);exit;
        return $price_list;
    }                                    

	/** get Location list **/
	function getLocationList($country_id,$city_id)
	{
		//Get the location areas data

		$locations_query = '"zones_infos"."language_id" = (case when (select count(*) as totalcount from zones_infos where zones_infos.language_id = '.getCurrentLang().' and zones.id = zones_infos.zone_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$locations=DB::table('zones')
			->select('zones.id','zones_infos.zone_name')
			->leftJoin('zones_infos','zones_infos.zone_id','=','zones.id')
			->leftJoin('countries','countries.id','=','zones.country_id')
			->leftJoin('cities','cities.id','=','zones.city_id')
			->whereRaw($locations_query)
			->where('zones_status',  1)
			->where('countries.id', $country_id)
			->where('cities.id', $city_id)
			->orderBy('zone_name', 'asc')
			->get();
		$locations_list=array();
		if(count($locations)>0){
			$locations_list = $locations;
		}
		return $locations_list;
	}
	
	/** get language list **/
	function getLanguageList()
	{
		$languages = DB::table('languages')->where('status', 1)->orderby("languages.id","asc")->get();
		$languages_list = array();
		if(count($languages)>0)
		{
			$languages_list = $languages;
		}
		return $languages_list;
	}
	
	/** get currency list **/
	function getCurrencyList()
	{ 
	 
		$language_id = getAdminCurrentLang();
	
		$query = '"currencies_infos"."language_id" = (case when (select count(*) as totalcount from currencies_infos where currencies_infos.language_id = '.$language_id.' and currencies.id = currencies_infos.currency_id) > 0 THEN '.$language_id.' ELSE 1 END)';
		$currencies = DB::table('currencies')
						->select('currencies.*','currencies_infos.*')
						 ->join('currencies_infos','currencies_infos.currency_id','=','currencies.id')
						->where("currencies.active_status","=","A")
						->where("currencies.default_status","=",1)
						 ->whereRaw($query)
						->orderBy('id', 'asc')
						->whereRaw($query)
						->get();
		$currencies_list = array();
		if(count($currencies)>0)
		{
			$currencies_list = $currencies;
		}
		return $currencies_list;
	}
	/** get weight class list **/
	function getWeightClass()
	{
		$query = '"weight_classes_infos"."lang_id" = (case when (select count(*) as totalcount from weight_classes_infos where weight_classes_infos.lang_id = '.getCurrentLang().' and weight_classes.id = weight_classes_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$weight_classes = DB::table('weight_classes')
							->select('weight_classes.*','weight_classes_infos.*')
							->leftJoin('weight_classes_infos','weight_classes_infos.id','=','weight_classes.id')
							->where('active_status','=',1)
							->whereRaw($query)
							->orderBy('title', 'asc')
							->get();  
		$weight_class_list=array();
		if(count($weight_classes)>0)
		{
			$weight_class_list = $weight_classes;
		}
		return $weight_class_list;
	}
	
	/** get generl settings configuration **/
	function getAppConfig()
	{
		//$defaultconfigs = DB::table('settings')->select('settings.*')->get();
		 $language = getCurrentLang();
		//$squery = '"settings_infos"."language_id" = (case when (select count(settings_infos.language_id) as totalcount from settings_infos where settings_infos.language_id = '.$language.' and settings.id = settings_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
        $config_items = DB::table('settings')
							->select('settings.*','settings_infos.site_name')
							//->whereRaw($squery)
							->leftJoin('settings_infos','settings_infos.id','=','settings.id')
							->first();
		
		return $config_items;
	}

    /** get generl settings configuration **/
    function getSMSConfig()
    {
        //$defaultconfigs = DB::table('settings')->select('settings.*')->get();
         $language = getCurrentLang();
        //$squery = '"settings_infos"."language_id" = (case when (select count(settings_infos.language_id) as totalcount from settings_infos where settings_infos.language_id = '.$language.' and settings.id = settings_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
        $config_items = DB::table('smssettings')
                            ->select('*')
                            //->whereRaw($squery)
                            ->get()->toArray();
        
        return $config_items;
    }
	
	/** get social media settings configuration **/
	function getAppSocialConfig()
	{
		$defaultconfigs=DB::table('socialmediasettings')->select('socialmediasettings.*')->get();
		$config_items=array();
		if(count($defaultconfigs)>0)
		{
			$config_items = $defaultconfigs[0];
		}
		return $config_items;
	}
	
	/** get email settings configuration **/
	function getAppConfigEmail()
	{
		$defaultconfigs = DB::table('emailsettings')->select('emailsettings.*')->get();
		$config_items   = array();
		if(count($defaultconfigs)>0)
		{
			$config_items = $defaultconfigs[0];
		}
		return $config_items;
	}
	
	/** get current language code **/
	function getCurrentLangCode()
    {
		$currentlanguagecodes = DB::table('languages')->where('status', 1)->where('id',getAppConfig()->default_language)->get();
		$current_language_code='';
		if(count($currentlanguagecodes)>0)
		{
			$current_language_code = $currentlanguagecodes[0]->language_code;
		}
		return $current_language_code;
    }
   
   /** get category types **/
   function getCategoryTypes()
   {
		$types=array (/* 1 => 'Price', 2 =>'Vendor',  3 => 'Blog',  5 => 'Coupon', */  6 => 'Price', 7 => 'Place');
		return $types;
   }

   /** get Booking types **/
   function getBookingTypes()
   {
        $types=array ( 1 =>'Walk in'    /*  , 2 => 'Reservation', 3 => 'Checked Out',  4 => 'Cancelled',  5 => 'Out of Order'   */  );
        return $types;
   }  

   /** get Customer types **/
   function getCustomerTypes()
   {
       $types=array ( 1 => 'Traveller   '    /* , 2 =>'Walk in',  3 => 'Checked Out',  4 => 'Cancelled',  5 => 'Out of Order'     */    );
        return $types;
   }    

   /** get Payment types **/
   function getPaymentTypes()
   {
       $types=array ( 1 => 'Cash Payment   '    /* , 2 =>'Walk in',  3 => 'Checked Out',  4 => 'Cancelled',  5 => 'Out of Order'     */    );
        return $types;
   }  


   function getChargeTypes()
   {
       $types=array ( 1 => 'Room Charge'  , 2 =>'Cleaning Charge',  3 => 'Food Charge',  4 => 'Extra Charges'  );
        return $types;
   }     

   /** get Booking Sources types **/
   function getBookingSources()
   {
        $types=array ( 1 => 'Walk-in / Telephone'    /* , 2 =>'Walk in',  3 => 'Checked Out',  4 => 'Cancelled',  5 => 'Out of Order'     */    );
        return $types;
   }

   /** get Adult Count **/
   function getAdultcount()
   {
        $types=array ( 1 => '1' , 2 =>'2',  3 => '3',  4 => '4',  5 => '5',  6 => '6' ,  7 =>'7',  8 => '8',  9 => '9',  10 => '10', 11 => '11', 12 => '12', 13 => '13', 14 => '14', 15 => '15');
        return $types;
   }

   /** get Child Count **/
   function getChildCount()
   {
        //  $types=array ( 1 => '0' , 2 =>'1',  3 => '2',  4 => '3',  5 => '4',  6 => '5' ,  7 =>'6',  8 => '7',  9 => '8',  10 => '9', 11 => '10', 12 => '11', 13 => '12', 14 => '13', 15 => '14',16 => '15');
        $types=array ( 1 => '1' , 2 =>'2',  3 => '3',  4 => '4',  5 => '5',  6 => '6' ,  7 =>'7',  8 => '8',  9 => '9',  10 => '10', 11 => '11', 12 => '12', 13 => '13', 14 => '14', 15 => '15');
        return $types;
   }

   /** get Guest Count **/
   function getGuestcount()
   {
        $types=array ( 1 => '1' , 2 =>'2',  3 => '3',  4 => '4',  5 => '5' );
        return $types;
   }

   /** get Room Count **/
   function getRoomcount()
   {
        $types=array ( 1 => '1' , 2 =>'2',  3 => '3',  4 => '4',  5 => '5' );
        return $types;
   }

   /** get Room Count **/
   function getRoomscount($j)
   {
          //    print_r($j);exit;

        if($j == 0)
        {
             $types=array();

             return $types;
        }
        $i=1;
        $type = array();
        for($i;$i<=$j;$i++)
        {
            $type = $i;
            $i++;
        }

        //  print_r($type); exit;
        if($type == 1)
        {
            $types=array ( 1 => '1');
        }
        elseif($type == 2)
        {
            $types=array ( 1 => '1' , 2 =>'2');
        }
        elseif($type == 3)
        {
            $types=array ( 1 => '1' , 2 =>'2',  3 => '3');
        }
        elseif($type == 4)
        {
            $types=array ( 1 => '1' , 2 =>'2',  3 => '3',  4 => '4');
        }        
        
        return $types;
   }

   /** get Discount Price **/
   function getDiscountPrice($outlet_id,$sd,$ed)
   {
        //  print_r($outlet_id);exit;

    //  CHECK WHETHER THE ROOM IS FREE OR NOT -> IT SHOWS UNWANTED ISSUE ,SO COMMENTED
/*
            $query = DB::table('rooms')
                        ->leftjoin('room_type','room_type.id','=','rooms.room_type_id')
                        ->where('rooms.property_id',32)
                        ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id')
                        ->groupBy('room_type.id');
                        //  ->get();
           
            $room_type = $query->get();
                       //   print_r($room_type);exit;

            $rt = $room_type->pluck('id');

            $rtr = DB::table('rooms')->whereIN('room_type_id',$rt)
                        ->pluck('id')->toArray();

            $rtrs = DB::table('rooms')->whereIN('room_type_id',$rt)
                        ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->whereBetween('booked_room.date',array($sd,$ed))
                        ->pluck('rooms.id')->toArray();

            $result=array_diff($rtr,$rtrs);

            //  print_r($result);exit;
            $users = DB::table('rooms')
                                ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id',
                                    'room_type.normal_price','room_type.discount_price')
                                ->Join('room_type','room_type.id','rooms.room_type_id')
                                ->whereIn('rooms.id', $result)
                                ->groupBy('room_type.id')
                                ->groupBy('room_type.normal_price')
                                ->groupBy('room_type.discount_price')
                                ->get();
                                //  echo "<pre>";
                  //    print_r($users);exit;
*/

            $users = DB::table('room_type')
                            ->select(DB::RAW('min(room_type.normal_price) as normal_price'),
                                     DB::RAW('max(room_type.discount_price) as discount_price'),
                                    'room_type.id')
                            ->where('room_type.property_id','=',$outlet_id)
                            ->groupBy('room_type.id')
                            ->get();

              //    print_r($users);exit;

            $out = DB::table('outlets')->where('id',$outlet_id)
                ->update([
                    'rid' => $users[0]->id,
                    'rd'  => $users[0]->discount_price,
                    'ra'  => $users[0]->normal_price,
                ]);
            
        $total_rating_list=array();
        if(count($users)>0){
            $total_rating_list = $users;
        }
        return $total_rating_list;
   } 

   /** get Star Count **/
   function getRatingCount($outlet_id)
   {
        //  print_r($outlet_id);exit;
        $total_rating = DB::table('outlet_reviews')
                        ->select(DB::Raw('avg(ratings) as avg_rate'),DB::Raw('count(ratings) as avg_count'))
                        ->where('outlet_id','=',$outlet_id)
                        ->where('outlet_reviews.approval_status','1')
                        ->get();
        //  print_r($total_rating);exit;

            $avg = round($total_rating[0]->avg_rate);

              //    print_r($avg);exit;

            $out = DB::table('outlets')->where('id',$outlet_id)
                ->update(['average_rating' => $avg]);

        $total_rating_list=array();
        if(count($total_rating)>0){
            $total_rating_list = $total_rating;
        }
        return $total_rating_list;
   } 

   /** get Rate Filters **/
   function getRateFilters()
   {
       $types=array ( 5 => 'Diamond'  , 4 =>'Platinum',  3 => 'Gold',  2 => 'Bronze' , 1 => 'Silver');
        return $types;
   } 

   /** get Room Count **/
   function getDuration()
   {
       $types=array ( 1 => '1 Night'  , 2 =>'2 Nights',  3 => '3 Nights',  4 => '4 Nights' , 5 => '5 Nights' 
        , 6 => '6 Nights'  );
        return $types;
   } 

   /** get Review Question **/
   function review_question()
   {
       $types=array ( 1 => 'location' , 2 =>'comfort',  3 => 'sleep_quality',  4 => 'rooms' , 5 => 'cleanliness');
       return $types;
   } 

   /** get Place types **/
   function getPlaceTypes()
   {
        $types=array ( 1 => 'Travel', 2 =>'Restaurants', 3 => 'Markerts');
        return $types;
   }   

      /** get category types **/
   function getAmenityTypes()
   {
        $types=array(1 => 'Property', 2 =>'Room Type');
        return $types;
   }

    function getAllCustomerList()
    {

        $customers_list=array();

        $customers = DB::table('admin_customers')
                        ->select('admin_customers.id','admin_customers.firstname')
                        ->where('status','=',1)
                        ->get();
               //  print_r($customers);exit;
            if(count($customers)>0)
            {
                $customers_list = $customers;
            }

        return $customers_list;
    }   


    function getPaymentlist()
    {

        $payment_list=array();

        $payments = DB::table('payment_gateways')
                        ->select('payment_gateways.id','payment_gateways_info.name')
                        ->leftJoin('payment_gateways_info','payment_gateways_info.payment_id','=','payment_gateways.id')
                        ->where('active_status','=',1)
                        ->get();
               //  print_r($customers);exit;
            if(count($payments)>0)
            {
                $payment_list = $payments;
            }

        return $payment_list;
    }   

    function getAllAmenityTypes()
    {

        
        $categories_list=array();
                
            $amenities=DB::table('amenities')
                ->select('amenities.id','amenities_infos.amenity_name','amenity_icon','amenty_image')
                ->leftJoin('amenities_infos','amenities_infos.id','=','amenities.id')
                ->orderBy('amenities.id', 'asc')
                ->where('amenities.default_status','=',1)
                ->where('created_by',Session::get('vendor_id'))
                ->get();

                //  print_r($amenities);exit;
            if(count($amenities)>0)
            {
                $categories_list = $amenities;
            }

        return $categories_list;
    }   


    function getsomeAmenities($property_id)
    {

        //  dd($property_id);

        $amenity_id = DB::table('outlet_amenity_mapping')->where('outlet_id',$property_id)->pluck('amenity_id');

        //  dd($amenity_id);
        
        $categories_list=array();
                
            $amenities=DB::table('amenities')
                ->select('amenities.id','amenities_infos.amenity_name','amenity_icon','amenty_image')
                ->leftJoin('amenities_infos','amenities_infos.id','=','amenities.id')
                ->orderBy('amenities.id', 'asc')
                ->where('amenities.default_status','=',1)
                ->whereIn('amenities.id',$amenity_id)
                ->take(5)
                ->get();

             // print_r($amenities);exit;
            if(count($amenities)>0)
            {
                $categories_list = $amenities;
            }

        return $categories_list;
    }  

    function getAmenities($property_id)
    {

        //  dd($property_id);

        $amenity_id = DB::table('outlet_amenity_mapping')->where('outlet_id',$property_id)->pluck('amenity_id');
        
        $categories_list=array();
                
            $amenities=DB::table('amenities')
                ->select('amenities.id','amenities_infos.amenity_name','amenity_icon','amenty_image')
                ->leftJoin('amenities_infos','amenities_infos.id','=','amenities.id')
                ->orderBy('amenities.id', 'asc')
                ->where('amenities.default_status','=',1)
                ->whereIn('amenities.id',$amenity_id)
                ->get();

             // print_r($amenities);exit;
            if(count($amenities)>0)
            {
                $categories_list = $amenities;
            }

        return $categories_list;
    }      

    function getroomtypeAmenities($room_type_id,$vendor_id)
    {
        //  print_r($vendor_id);exit;
        
        $categories_list=array();
                
            $amenities=DB::table('amenities')
                ->select('amenities.id','amenities_infos.amenity_name','amenity_icon','amenty_image')
                ->leftJoin('amenities_infos','amenities_infos.id','=','amenities.id')
                //  ->orderBy('amenities.id', 'asc')
                ->where('amenities.created_by',$vendor_id)
                ->where('amenities.default_status','=',1)
                ->where('amenities.room_type','=',2)
                ->take(2)
                ->get();

             // print_r($amenities);exit;
            if(count($amenities)>0)
            {
                $categories_list = $amenities;
            }
            else
            {
                //  print_r('Ak');exit;
                $amenities = DB::table('amenities')
                    ->select('amenities.id','amenities_infos.amenity_name','amenity_icon','amenty_image')
                    ->leftJoin('amenities_infos','amenities_infos.id','=','amenities.id')
                    //  ->orderBy('amenities.id', 'asc')
                    ->where('amenities.default_status','=',1)
                    ->where('amenities.created_by',$vendor_id)
                    //  ->where('amenities.room_type','=',2)
                    ->take(2)
                    ->get(); 

                $categories_list = $amenities;               
            }

        return $categories_list;
    }      

    function getAllAccomodationTypes()
    {

        
        $categories_list=array();
                
            $amenities=DB::table('accomodation_type')
                ->select('accomodation_type.id','accomodation_type_infos.accomodation_type_name')
                ->leftJoin('accomodation_type_infos','accomodation_type_infos.id','=','accomodation_type.id')
                ->orderBy('accomodation_type.id', 'asc')
                ->where('created_by',Session::get('vendor_id'))
                ->get();

                //  print_r($amenities);exit;
            if(count($amenities)>0)
            {
                $categories_list = $amenities;
            }

        return $categories_list;
    }    


    function getAllPlaceofInterest($outlet_id)
    {

        // print_r($outlet_id);
        // exit;
        $placesofinterest=array();
                
            $poi=DB::table('place_of_interest')
                    ->select('place_of_interest.*')
                    ->leftJoin('outlets','outlets.id','=','place_of_interest.property_id')
                    ->where('outlets.id','=',$outlet_id)
                    ->get();

                //  print_r($amenities);exit;
            if(count($poi)>0)
            {
                $placesofinterest = $poi;
            }

        return $placesofinterest;
    }      
    /** get image resize  settings **/
    
   function getImageResize($param='')
   {
		if(!$param)
		{
			return false;
		}
		$settings=DB::table('imageresizesettings')->select('*')->get();
		$image_items=array();
		if(count($settings)>0)
		{
			foreach($settings as $key =>$value)
			{
				switch($param)
				{
					case 'LOGO':
						if($value->type==1)
						{
							$image_items['WIDTH'] = $value->list_width;
							$image_items['HEIGHT'] = $value->list_height;
						}
					break;
					case 'FAVICON':
						if($value->type==1)
						{
							$image_items['WIDTH'] = $value->detail_width;
							$image_items['HEIGHT'] = $value->detail_height;
						}
					break;
					case 'CATEGORY':
						if($value->type==1)
						{
							$image_items['WIDTH'] = $value->thumb_width;
							$image_items['HEIGHT'] = $value->thumb_height;
						}
					break;
					case 'STORE':
						if($value->type==2)
						{
							$image_items['LIST_WIDTH'] = $value->list_width;
							$image_items['LIST_HEIGHT'] = $value->list_height;
							$image_items['DETAIL_WIDTH'] = $value->detail_width;
							$image_items['DETAIL_HEIGHT'] = $value->detail_height;
							$image_items['THUMB_WIDTH'] = $value->thumb_width;
							$image_items['THUMB_HEIGHT'] = $value->thumb_height;
						}
					break;
					case 'PRODUCT':
						if($value->type==3)
						{
							$image_items['LIST_WIDTH'] = $value->list_width;
							$image_items['LIST_HEIGHT'] = $value->list_height;
							$image_items['DETAIL_WIDTH'] = $value->detail_width;
							$image_items['DETAIL_HEIGHT'] = $value->detail_height;
							$image_items['THUMB_WIDTH'] = $value->thumb_width;
							$image_items['THUMB_HEIGHT'] = $value->thumb_height;
						}
					break;
					case 'BANNER':
						if($value->type==4)
						{
							$image_items['LIST_WIDTH'] = $value->list_width;
							$image_items['LIST_HEIGHT'] = $value->list_height;
						}
					break;
					case 'VENDOR':
						if($value->type==5)
						{
							$image_items['LIST_WIDTH'] = $value->list_width;
							$image_items['LIST_HEIGHT'] = $value->list_height;
							$image_items['DETAIL_WIDTH'] = $value->detail_width;
							$image_items['DETAIL_HEIGHT'] = $value->detail_height;
							$image_items['THUMB_WIDTH'] = $value->thumb_width;
							$image_items['THUMB_HEIGHT'] = $value->thumb_height;
						}
					break;
				}
			}
		}
		return $image_items;
   }
   
    /** Add user activity log info **/
    
	function userlog($message,$userid = '',$activity_type='',$severity = 1,$device='')
	{ 
		return addActivity($message,$activity_type,$severity,$userid,'',$device);
	}
   
	function addActivity($message,$activity_type='',$severity = 1,$userid = '',$ip='',$device='')
	{ 
		
		$date = date("Y-m-d H:i:s");
		if(!$userid) {
			return false;
		}
		$ip = $ip ? $ip : Request::ip();
		//$browser = get_browser(null, true);
		$browser ='';
		$device = $device ? $device : isset($browser['browser'])?$browser['browser']:'';
		$data = array(
			'message' => $message,
			'date' => $date,
			'ip_' => $ip,
			'device' =>$device,
			'user_id' => $userid,
			'activity_type' => $activity_type
		);
		try {
			DB::table('user_activity_log')->insert($data);
		} catch(Exception $e) {
			print_r($e); exit;
		}
	}
	
	 /** Compare old data and new data its return modified data values only - formate array() **/
	function logcompare($olddata,$newdata,$type=false,$arr=array(),$unset=array())
    {
		 if(!$olddata){
			 $olddata=array();
		 }
		$diff=array_diff($olddata,$newdata);
		if(count($diff)){
			$final_data=array();
			foreach($diff as $key => $val){
				if(isset($newdata[$key])){
					$final_data[$key][]=$val;
					$final_data[$key][]=$newdata[$key];
				}	
			}
			$text="";
			foreach($final_data as $key1=>$data){
				if(in_array($key1,$unset)){ continue;} 
				$text .= $key1.' - '.'<b>'.strip_tags($data[0]).'</b>'.' -> '.'<b>'.strip_tags($data[1]).'</b> ';
			}
			if($type){	
				return $diff;
			}
			else {	
				return $text; 	
			}
		}

    }
    
     /** Its return 3 days ago , 1 minute age this type of result **/
     
    function nicetime($date)
    {
        if(empty($date)) {
            return "";
        }
        $periods         = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths         = array("60","60","24","7","4.35","12","10");

        $now             = time();
        $unix_date         = strtotime($date);

        // check validity of date
        if(empty($unix_date)) {
            return "Bad date";
        }

        // is it future date or past date
        if($now > $unix_date) {
            $difference     = $now - $unix_date;
            $tense         = trans('messages.ago');

        } else {
            $difference     = $unix_date - $now;
            $tense         = trans('messages.from now');
        }

        for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        if($difference != 1) {
            $periods[$j].= "s";
        }
        return "$difference $periods[$j] {$tense}";
    }
    
         /**
     * Calculates timezone offset
     *
     * @param  string $timezone
     * @return int offset between timezone and gmt
     */
    function calculateOffset($timezone = null)
    {
        $result = true;
        $offset = 0;
        if (!is_null($timezone)){
            $oldzone = @date_default_timezone_get();
            $result = date_default_timezone_set($timezone);
        }

        if ($result === true) {
            $offset = gmmktime(0, 0, 0, 1, 2, 1970) - mktime(0, 0, 0, 1, 2, 1970);
        }

        if (!is_null($timezone)){
            date_default_timezone_set($oldzone);
        }

        return $offset;
    }
    
    
        /**
     * Forms GMT date
     *
     * @param  string $format
     * @param  int|string $input date in current timezone
     * @return string
     */
    function gmtDate($format = null, $input = null)
    {
        if (is_null($format)) {
            $format = 'Y-m-d H:i:s';
        }

        $date = $this->gmtTimestamp($input);

        if ($date === false) {
            return false;
        }
        date_default_timezone_set(Config::get('app.timezone'));
        $result = date($format, $date);
        return $result;
    }
    
    
        /**
     * Forms GMT timestamp
     *
     * @param  int|string $input date in current timezone
     * @return int
     */
	function gmtTimestamp($input = null)
    {
        if (is_null($input)) {
            return gmdate('U');
        } else if (is_numeric($input)) {
            $result = $input;
        } else {
            $result = strtotime($input);
        }

        if ($result === false) {
            // strtotime() unable to parse string (it's not a date or has incorrect format)
            return false;
        }

        $timestamp = time()+date("Z");


        return $timestamp;

    }
    
        /**
     * Get current timezone offset in seconds/minutes/hours
     *
     * @param  string $type
     * @return int
     */
	function getGmtOffset($type = 'seconds')
    {
        $result = $this->_offset;
        switch ($type) {
            case 'seconds':
            default:
                break;

            case 'minutes':
                $result = $result / 60;
                break;

            case 'hours':
                $result = $result / 60 / 60;
                break;
        }
        return $result;
    }
    
    function is_validdate($date)
    {
		if( DateTime::createFromFormat('Y-m-d', $date) != false || $date != '0000-00-00 00:00:00') {
			$value = date("Y-m-d H:i:s",strtotime($date)); 
		} else {
			$value = '0000-00-00 00:00:00';
		}
		return $value;
	}
	
	function timeAgo($timestamp)
	{
		$datetime1=new DateTime("now");
		$datetime2=date_create($timestamp);
		$diff=date_diff($datetime1, $datetime2);
		$timemsg='';
			if($diff->y > 0){
				$timemsg = $diff->y .' year'. ($diff->y > 1?"s":'');
			}
			else if($diff->m > 0){
				$timemsg = $diff->m . ' month'. ($diff->m > 1?"s":'');
			}
			else if($diff->d > 0){
				$timemsg = $diff->d .' day'. ($diff->d > 1?"s":'');
			}
			else if($diff->h > 0){
				$timemsg = $diff->h .' hour'.($diff->h > 1 ? "s":'');
			}
			else if($diff->i > 0){
				$timemsg = $diff->i .' minute'. ($diff->i > 1?"s":'');
			}
			else if($diff->s > 0){
				$timemsg = $diff->s .' second'. ($diff->s > 1?"s":'');
			} else {
				$timemsg = 'a few second';	
			}

			$timemsg = $timemsg.' ago';
			return $timemsg;
	}
	
	 /**
     * Get email  template subject types
     *
     * @param  
     * @return array
     */
	function getSubjectType()
	{
		return array(
			'user_assignment' => 'User Assignment',
			'order_receipt' => 'Order Receipt',
			'order_pickup' => 'Order Pickup',
			'delivery_confirmation' => 'Delivery Confirmation',				
			'payment_receipt' => 'Payment Receipt',
			'payment_transfer' => 'Payment Transfer',
			'order_cancellation' => 'Order Cancellation',
			'goods_return' => 'Goods Return',
			'requirement_post' => 'Requirement Post',
			'product_review' => 'Product Review',
			'people_review' => 'People Review',
			'place_review' => 'Place Review',
			'product_service_add_confirmation' => 'Product / Service Add Confirmation',
			'new_product_service' => 'New Product / Service',
			'mail' => 'Mail',		
			'subscription' => 'Subscription',
			'review_report_abuse'=>'Review Report Abuse',
			'item_cancellation'=>'Item Cancellation',
			'return_request'=>'Return Request'
		);
	}
	
	
	/**
     * Get all email template 
     *
     * @param  
     * @return array object
     */
	function getTemplates() 
	{
		$templates=DB::table('email_templates')
			 ->select('email_templates.*')
			->get();
		return $templates;

	}
	
	/**
     * Get user types
     *
     * @param  
     * @return array 
     */
	function getUserTypes()
	{
		//    $types=array(3=>'Website users',2=>'Role User (Moderator)');
        $types=array(2=>'Role User (Moderator)');
		return $types;
	}
   
    function getStaffTypes()
    {
        $types=array(1=>'Manager',2=>'Front Desk');
        return $types;
    }   
	/**
     * mailchimp subscribe email
     *
     * @param  
     * @return 
     */
	function mailchimpSubscribe($email_id)
    {
		$mailchimp = new Mailchimp();
        try {
            $mailchimp
            ->lists
            ->subscribe(
                env('MAILCHIMP_UNIQUEID'),
                ['email' => $email_id]
            );
        	return;
        } catch (\Mailchimp_List_AlreadySubscribed $e) {
			echo $e->getMessage();exit;
        } catch (\Mailchimp_Error $e) {
			echo $e->getMessage();exit;
        }
    }

	/**
     * mailchimp email send
     *
     * @param  
     * @return 
     */
    function mailchimpCampaign($from = "",$from_name="", $subject = "", $message = array(),$attachment = array())
    {
		$mailchimp = new Mailchimp();
        try {
	        $options = [
				'list_id'   => env('MAILCHIMP_UNIQUEID'),
				'subject' => $subject,
				'from_name' => $from_name,
				'from_email' => $from,
				'to_name' => 'All FrontEnd Users'
	        ];
	        $content = [
				'html' => $message,
				'text' => strip_tags($message)
	        ];
	        $campaign = $mailchimp->campaigns->create('regular', $options, $content);
	        $mailchimp->campaigns->send($campaign['id']);
        	return;
        } catch (Exception $e) {
			echo $e->getMessage();exit;
        }
	}

	/**
     * smtp email send
     *
     * @param  
     * @return 
     */
    function smtp($from = "",$from_name="",$receiver = array(), $subject = "", $message = array(),$file = "",$attachment = array())
    {
		$smtp=getAppConfigEmail()->smtp_enable;
		if($smtp)
        {//echo "<pre>";print_r($message);die;
		  //echo $message->subscribe_link;die;
			require_once(base_path().'/includes/mail/class.phpmailer.php');
			$mail = new PHPMailer(TRUE);
			$mail->IsSMTP();
			try {
				$content = _TemplateResponse($message,$file);
				$mail->Host       = "mail.yourdomain.com";
				$mail->SMTPDebug  = 2;
				$mail->SMTPAuth   = TRUE;
				$mail->SMTPSecure = getAppConfigEmail()->smtp_encryption;
				$mail->Host       = getAppConfigEmail()->smtp_host_name;
				$mail->Port       = getAppConfigEmail()->smtp_port;
				$mail->Username   = getAppConfigEmail()->smtp_username;
				$mail->Password   = getAppConfigEmail()->smtp_password;
				$mail->AddReplyTo($from);
				//$this->setReceivers($receiver)->setSubject($subject);
				if(is_array($receiver)) 
				{
					call_user_func_array(array($mail, "addAddress"), $receiver);
					foreach($receiver as $f)
					{
						$mail->addAddress($f);
					}
				}
				else {
					$mail->addAddress($receiver);
				}
				$mail->SetFrom($from,$from_name); 
				$message = array_merge($message,_TemplateDefaultResponse());
				$subject = parseTemplate($subject,$message);
				$mail->Subject = $subject;
				$mail->MsgHTML($content);
				if(!empty($attachment))
				{
					foreach($attachment as $f)
					{
						$mail->AddAttachment($f);
					}
				} 
				//$this->setMessage($content);
				//App::dispatchEvent('Mail_Send_Before',array('phpmailer' => $mail, 'email' => $this));
				//if(!$this->getQueue())
                //echo "<pre>";print_r($mail);

				$mail->Send();
			}
			catch(phpmailerException $e)
			{
				  echo $e->errorMessage();
                  return false;
			}
			catch (Exception $e)
			{
                return false;
				//  echo $e->getMessage();exit;
			}
			return true;
		}
		else{
			try { 
				if(count($attachment))
				{
					$content = _TemplateResponse($message,$file);
					$message = array_merge($message,_TemplateDefaultResponse());
					$subject = parseTemplate($subject,$message);
					$fileatt = $attachment[0]; // Path to the file

					$filename = basename($fileatt);
					$file_size = filesize($fileatt);
					$content1 = chunk_split(base64_encode(file_get_contents($fileatt))); 
					$uid = md5(uniqid(time()));
					$ecmessage = $content;
					$header = "From: ".$from_name." <".$from.">\n";
					$header .= "MIME-Version: 1.0\n";
					$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\n\n";
					$emessage= "--".$uid."\n";
					$emessage.= "Content-type:text/html; charset=iso-8859-1\n";
					$emessage.= "Content-Transfer-Encoding: 7bit\n\n";
					$emessage .= $ecmessage."\n\n";
					$emessage.= "--".$uid."\n";
					$emessage .= "Content-Type: application/octet-stream; name=\"".$filename."\"\n"; // use different content types here
					$emessage .= "Content-Transfer-Encoding: base64\n";
					$emessage .= "Content-Disposition: attachment; filename=\"".$filename."\"\n\n";
					$emessage .= $content1."\n\n";
					$emessage .= "--".$uid."--";
					if(is_array($receiver))
					{
						foreach($receiver as $f)
						{
							//mail($f,$subject,$content,$headers);
							@mail($f, $subject, $emessage, $header);
						}
					}
					else {
						//mail($receiver,$subject,$content,$headers);
						@mail($receiver, $subject, $emessage, $header);
					}
				}
				else { 
					$content = _TemplateResponse($message,$file);
					$message = array_merge($message,_TemplateDefaultResponse());
					$subject = parseTemplate($subject,$message);
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: <'.$from.'>' . "\r\n";
					if(is_array($receiver)) 
					{
						foreach($receiver as $f)
						{
							@mail($f,$subject,$content,$headers);
						}
					}
					else {
						@mail($receiver,$subject,$content,$headers);
					}
			    }
			}
			catch (Exception $e) {
				return false;
			}
			return true;
		}	  
    }
    
    /**
     * filter blackword from email content
     *
     * @param  
     * @return 
     */
    function _TemplateResponse($message=array(),$file)
    { 
		
		$message=array_merge($message,_TemplateDefaultResponse()); 
		/*if($file instanceof Model_Core_Email_Template) { */
			$mail_content = $file[0]->content;
		/* } */ 
		/* else {
			$lang=App::getConfig('LOCALE'); 
			$lang=explode("-",$lang);   
			$lang=isset($lang[0])?$lang[0]:'en';
			$path=Kohana::find_file('i18n', $lang.'/email/'.$file,'html');
			if(isset($path[0]) && !file_exists($path[0])){
				throw new Kohana_Exception(__('Invalid Mail Template'));
				return $this;
			}
			$mail_content = (string) file_get_contents($path[0]);
		}
		*/ 
		$mail_content = filter($mail_content,$message);
		return $mail_content;
		
	}
	
	/**
     * Default Variables values set here
     *
     * @param  
     * @return 
     */
	/** Default Variables**/
	function _TemplateDefaultResponse()
    {
		$default = array('default' => array('SITE_NAME' => getAppConfig()->site_name,
		"STOREADMIN_LOGO" => url('/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.getAppConfig()->logo),
		"ADMIN_LOGO" => url('/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.getAppConfig()->logo),
		"FRONT_LOGO" => url('/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.getAppConfig()->logo),
		"CONTACT_EMAIL" => getAppConfigEmail()->contact_mail,
		"SUPPORT_EMAIL" => getAppConfigEmail()->support_mail,
		"SITE_URL"=>url('/'),
		"SITE_ASSETS_URL" => url('/assets/front/'.Session::get("general")->theme.'/images/'),
		"SITE_TWITTER_PAGE" => 'http://www.twitter.com',
		'SITE_FACEBOOK_PAGE' => 'http://www.facebook.com/'
		)); 
		return $default;
	}
	
	/** filter Content  removeBlackList**/
	
	function filter($value,$templateVariables = array())
    {
		$CONSTRUCTION_PATTERN = '/{{([a-z]{0,10})(.*?)}}/si';
		$templateVariables = array_merge($templateVariables,_TemplateDefaultResponse());
		if(preg_match_all($CONSTRUCTION_PATTERN, $value, $constructions, PREG_SET_ORDER)) { 			
            foreach($constructions as $index => $construction) {
                
                $replacedValue = ''; 
				$callback = array($this, $construction[1].'Directive');
                if(!is_callable($callback)) {
                    continue;
                }
                try { 
                    $replacedValue = call_user_func($callback, $construction);
                } catch (Exception $e) {
                    throw $e;
                }
                
                $value = str_replace($construction[0], $replacedValue, $value); 
            }
        }
 
		$values = parseTemplatereplace($value,$templateVariables);
		removeBlackList($value);
		return $values;
	}
	
	function parseTemplate($content = '', $templateVariables = array())
	{ 
		preg_match_all('/\${(.*?)}/',$content,$matches);
		$matchings = $matches[1];
		$replaceset = array();
		foreach($matchings as $match) {
			$data = explode(".",$match);
			if(!isset($templateVariables[$data[0]])) {
				continue;
			} 
			$object = json_decode(json_encode($templateVariables[$data[0]]), FALSE);
			if(is_object($object)) {
				$replaceset['${'.$match.'}'] = $object->{$data[1]};
			}
			else if(is_string($templateVariables[$data[0]])) {
				$replaceset['${'.$match.'}'] = $templateVariables[$data[0]];
			} 
		} 
		$con = str_replace(array_keys($replaceset),array_values($replaceset),$content);
		return $con;
	}
	
	/** replace the variable to value  **/
	function parseTemplatereplace(&$content = '', $templateVariables = array())
	{ 
		preg_match_all('/\${(.*?)}/',$content,$matches);
		$matchings = $matches[1];
		$replaceset = array();
		foreach($matchings as $match) {
			$data = explode(".",$match);
			if(!isset($templateVariables[$data[0]])) {
				continue;
			} 
			$object = json_decode(json_encode($templateVariables[$data[0]]), FALSE);
			if(is_object($object)) {
				$replaceset['${'.$match.'}'] = $object->{$data[1]};
			}
			else if(is_string($templateVariables[$data[0]])) {
				$replaceset['${'.$match.'}'] = $templateVariables[$data[0]];
			} 
		} 
		$content = str_replace(array_keys($replaceset),array_values($replaceset),$content);
		return $content;
	}


	
	/** Filter Black Words - varible passing only not open string **/
	function removeBlackList(&$text = '')
    {
        $blacklistwords = getAppConfig()->blocklist_words;
        $blacklistwords = explode(",",$blacklistwords);
        foreach($blacklistwords as $bl) {
            $text = preg_replace('/\b('.$bl.')(s?)\b/u', '****', $text, 1); 
        } 
        return $text ;
    }
	/*
	 * timing schedule for doctors purpose
	*/
	function getDaysWeekArray()
	{
		return array('Monday' => 1, 'Tuesday' => 2, 'Wednesday' => 3, 'Thursday' => 4, 'Friday' => 5, 'Saturday' => 6, 'Sunday' => 7);
	}
	
	/*
	 * Get data here for outlet with days
	*/
	function getOpenTimings($v_id,$day_week)
	{
		$time_data = DB::table('opening_timings')
			->where("vendor_id",$v_id)
			->where("day_week",$day_week)
			->orderBy('id', 'asc')
			->get();
		$time_list=array();
		if(count($time_data)>0){
			$time_list = $time_data;
		}
		return $time_list;
	}
	
	/*
	 * Get data here for outlet with days
	*/
	function getDeliveryTimings($v_id,$day_week)
	{
		$time_data = DB::table('delivery_timings')
			->where("vendor_id",$v_id)
			->where("day_week",$day_week)
			->orderBy('id', 'asc')
			->get();
		$time_list=array();
		if(count($time_data)>0){
			$time_list = $time_data;
		}
		return $time_list;
	}
	/** get vendors list **/
	function getVendorLists($onlyFeatured = false)
	{
		//Get the vendors data
		$query = '"vendors_infos"."lang_id" = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = '.getCurrentLang().' and vendors.id = vendors_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		if($onlyFeatured){
			$data=DB::table('vendors')
			->leftJoin('vendors_infos','vendors_infos.vendors_view_id','=','vendors.user_id')
			->select('vendors.user_id','vendors_infos.vendor_name')
			->whereRaw($query)
			->where('active_status',  1)
			->where('featured_vendor',  1)
			->orderBy('vendor_name', 'asc')
			->get();
		} else {
			$data=DB::table('vendors')
			->leftJoin('vendors_infos','vendors_infos.vendors_view_id','=','vendors.user_id')
			->select('vendors.user_id','vendors_infos.vendor_name')
			->whereRaw($query)
			->where('active_status',  1)
			->orderBy('vendor_name', 'asc')
			->get();
		}

		$data_list=array();
		if(count($data)>0){
			$data_list = $data;
		}
		return $data_list;
	}

    /** get vendors list **/
    function getVendorName($id)
    {
        //Get the vendors data
        //  print_r($id);exit;
        $query = '"vendors_infos"."lang_id" = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = '.getCurrentLang().' and vendors.id = vendors_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
  
            $data=DB::table('vendors')
            ->leftJoin('vendors_infos','vendors_infos.vendors_view_id','=','vendors.user_id')
            ->select('vendors_infos.vendor_name')
            ->whereRaw($query)
            ->where('active_status',  1)
            ->where('vendors.user_id','=',$id)
            ->get();
        

        $data_list=array();
        if(count($data)>0){
            $data_list = $data;
        }
        return $data_list;
    }

	/* To coupon outlet list */
	function getOutletLists($coupon_id)
	{
		//print_r($coupon_id);die;
		$data = DB::table('coupon_outlet')
				->select('outlet_id')
				->where('coupon_id',  $coupon_id)
				->get();
		$data_list = array();
		if(count($data)>0){
			$data_list = $data;
		}
		return $data_list;
	}

	/** get vendors categories **/
	function gethead_categories()
	{
		$data_list = array();
		$query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$data=DB::table('categories')
				->select('categories.id','categories_infos.category_name')
				->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
				->whereRaw($query)
				->where('category_status',  1)
				->where('category_type',2)
				->orderBy('category_name', 'asc')
				->get();
		if(count($data)>0)
		{
			$data_list = $data;
		}
		return $data_list;
	}
	
	
	/** get vendors list **/
	function getStoreVendorLists($vendor_id)
	{
		//Get the vendors data
		$query = '"vendors_infos"."lang_id" = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = '.getCurrentLang().' and vendors.id = vendors_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$data=DB::table('vendors')
			->leftJoin('vendors_infos','vendors_infos.id','=','vendors.id')
			->select('vendors.id','vendors_infos.vendor_name')
			->whereRaw($query)
			->where('vendors.id', $vendor_id)
			->where('active_status',  1)
			->orderBy('vendor_name', 'asc')
			->get();
		$data_list=array();
		if(count($data)>0){
			$data_list = $data;
		}
		return $data_list;
	}
	
	/* Get weight classes data here */
	function getVendorCategoryList($id)
	{
		$vdata = DB::table('vendors')->select('category_ids')->where('id',$id)->get();
		$data_list = array();
		if(count($vdata)){
			$cids = explode(',',$vdata[0]->category_ids);
			//Get the categories data
			$query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
			$data=DB::table('categories')
					->select('categories.id','categories_infos.category_name','categories.url_key')
					->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
					->whereRaw($query)
					->where('category_status',  1)
					->where('category_type',2)
					->whereIn('categories.id', $cids)
					->orderBy('category_name', 'asc')
					->get();
			if(count($data)>0){
				$data_list = $data;
			}
			return $data_list;
		} else {
			return $data_list;
		}
	}
	
	function getVendorsubCategoryLists($id)
	{
		$vdata = DB::table('vendors')->select('category_ids')->where('id',$id)->get();
		//print_r($vdata);//die;
		$data_list = array();
		if(count($vdata))
		{
			$cids = explode(',',$vdata[0]->category_ids);
			
			//Get the categories data
			$query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
			$data = DB::table('categories')
					->select('categories.id','categories_infos.category_name','categories.url_key')
					->join('categories_infos','categories_infos.category_id','=','categories.id')
					->whereRaw($query)
					->where('category_status','=',1)
					->where('category_type','=',2)
					->whereIn('categories.id', $cids)
					->orderBy('category_name', 'asc')
					->get();
			if(count($data)>0)
			{
				$data_list = $data;
			}
			return $data_list;
		} 
		else
		{
			return $data_list;
		}
	}
	
	
	function getProduct_category_list($store_id)
	{
		//echo $store_id;exit;
			$data_list = array();
			//Get the categories data
			$query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
			$data = DB::table('categories')
					->select('categories.id as category_id','categories.url_key','categories_infos.category_name','categories.image')
					->Join('products','products.category_id','=','categories.id')
					->join('categories_infos','categories_infos.category_id','=','categories.id')
					->whereRaw($query)
					->where('outlet_id','=',$store_id)
					->where('category_status','=',1)
					->where('category_level','=',2)
					->where('products.active_status','=',1)
					->where('category_type','=',1)
					->groupBy('categories.id',"categories_infos.category_name")
					->orderBy('category_name', 'asc')
					->get();
			if(count($data)>0)
			{
				$data_list = $data;
			}
			return $data_list;
	}
	
	/*SELECT cat.id,p.id,ci.category_name
		FROM categories cat
		RIGHT JOIN products p ON cat.id = p.sub_category_id
		left join categories_infos ci on ci.category_id = cat.id
		where ci.language_id = 1
	*/
	
	/* To get all outlet list based on vendor */
	function getOutletList($c_id)
	{
		$query = '"outlet_infos"."language_id" = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.getCurrentLang().' and outlets.id = outlet_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$data = DB::table('outlets')
					->select('outlets.id','outlets.vendor_id','outlet_infos.outlet_name')
					->join('outlet_infos','outlet_infos.id','=','outlets.id')
					->whereRaw($query)
					->where('vendor_id',$c_id)
					->where('active_status', 1)
					->get();
		$data_list = array();
		if(count($data)>0)
		{
			$data_list = $data;
		}
		return $data_list;
	}
	
    /* To get all customers list */
    function all_customers_list($group_id = "")
    {
		$customers = DB::table('customers_view')
						->select('id','email','social_title','first_name','last_name')
						->where("user_type",3)->where("status",1);
		if ($group_id != "")
		{
			$customers = $customers->whereIn('user_group',$group_id);
		}
		$customers = $customers->orderBy('id', 'desc')->get();
		$customers_list = array();
		if(count($customers)>0)
		{
			$customers_list = $customers;
		}
		return $customers_list;
	}
    /* To get all newsletter subscribeers list */
    function all_newsletter_subscribers_list()
    {
		$newsletter_subscribers = DB::table('newsletter_subscribers')
									->select('id','email')
									->where("active_status",1)
									->orderBy('id', 'desc')->get();
		$newsletter_subscribers_list = array();
		if(count($newsletter_subscribers)>0)
		{
			$newsletter_subscribers_list = $newsletter_subscribers;
		}
		return $newsletter_subscribers_list;
	}
    /* To get all customers groups list */
    function all_customers_groups_list()
    {
		$customers_groups = DB::table('users_group')
								->select('group_id','group_name')
								->where("group_status",1)
								->orderBy('group_id', 'desc')->get();
		$customers_group_list = array();
		if(count($customers_groups)>0)
		{
			$customers_group_list = $customers_groups;
		}
		return $customers_group_list;
	}

	/** get generl settings configuration **/
	function getAdminpaymentemail()
	{					
		$defaultconfigs=DB::table('customers_view')
			 ->select('customers_view.payment_account')
			 ->where('id', '=', 1)
			->get();
		$config_items=array();
		if(count($defaultconfigs)>0){
			$config_items = $defaultconfigs[0];
		}
		return $config_items;
	}
	/** get generl settings configuration **/
	function getAppPaymentConfig()
	{					
		$defaultconfigs=DB::table('payment_gateways')
			 ->select('payment_gateways.*')
			 ->where('id', '=', 1)
			->get();
		$config_items=array();
		if(count($defaultconfigs)>0){
			$config_items = $defaultconfigs[0];
		}
		return $config_items;
	}
	/** get generl settings configuration **/
	function getCms()
	{
		$language = getCurrentLang();
		$query = 'cms_infos.language_id = (case when (select count(cms_infos.language_id) as totalcount from cms_infos where cms_infos.language_id = '.$language.' and cms.id = cms_infos.cms_id) > 0 THEN '.$language.' ELSE 1 END)';
		$cms = DB::table('cms')->select('cms.id','cms.url_index','cms.sort_order','cms_infos.title')
			->leftJoin('cms_infos','cms_infos.cms_id','=','cms.id')
			->whereRaw($query)
			->where('cms.cms_type',"<>",2)
			->where('cms.cms_status','=',1)
			->orderBy('cms.sort_order', 'asc')
			->get();
			$cms_items=array();
			if(count($cms)>0){
				$cms_items = $cms;
			}
			return $cms_items;
			
	}

		/** get city list **/
	function getUserGroups()
	{
		$groups=DB::table('users_group')
				 ->select('users_group.*')
				->orderBy('group_id', 'asc')
				->get();
		$groups_list=array();
		if(count($groups)>0){
			$groups_list = $groups;
		}
		return $groups_list;
	}

	/** get city list **/
	function getUserList($user_type)
	{
        $users=DB::table('customers_view')
            ->select('customers_view.email','customers_view.first_name','customers_view.last_name','customers_view.id')
            ->where('customers_view.active_status', 'A')
            //->where('status', 1)
            ->where('customers_view.user_type',"!=",1)
            ->where('customers_view.is_verified',1)
            ->orderBy('customers_view.email', 'asc')
            ->get();

			
		$doctors=DB::table('vendors_view')
			->select('email','first_name as name','last_name','id','phone_number','mobile_number')
			//->where('active_status', 1)
			->orderBy('email', 'asc')
			->get();	
			
		$user_list=array();
		if(count($users)>0 && $user_type==1){
			$user_list = $users;
			return $user_list;
		}
		if(count($doctors)>0 && $user_type==2){
			$user_list = $doctors;
			return $user_list;
		}
		
	}
	/*
	 * To get the outlet list based on vendor
	 */
	function get_outlet_list($vendor_id = "")
	{
        //print_r($vendor_id);exit;
		$query = '"outlet_infos"."language_id" = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.getCurrentLang().' and outlets.id = outlet_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$outlets = DB::table('outlets')
					->select('outlets.id','outlet_infos.outlet_name')
					->join('outlet_infos','outlet_infos.id','=','outlets.id')
					->whereRaw($query)
					->where('active_status', 1);
		if($vendor_id != '')
		{
			$outlets = $outlets->where('vendor_id', $vendor_id);
		}
		$outlets = $outlets->orderBy('outlets.id', 'desc')->get();

		$outlets_list = array();
		if(count($outlets)>0)
		{
			$outlets_list = $outlets;
		}
		return $outlets_list;
	}

    /*
     * To get the First Outlet based on vendor
     */
    function get_first_outlet($vendor_id = "")
    {
        $query = '"outlet_infos"."language_id" = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.getCurrentLang().' and outlets.id = outlet_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
        $outlets = DB::table('outlets')
                    ->select('outlets.id','outlet_infos.outlet_name')
                    ->join('outlet_infos','outlet_infos.id','=','outlets.id')
                    ->whereRaw($query)
                    ->where('active_status', 1);
        if($vendor_id != '')
        {
            $outlets = $outlets->where('vendor_id', $vendor_id);
        }
        $outlets = $outlets->orderBy('outlets.id', 'desc')->first();
        $outlets_list = 0;
        if(count($outlets)>0)
        {
            $outlets_list = $outlets->id;
        }
        return $outlets_list;
    }


	/*
	 * To get the product list based on outlet
	 */
	function get_product_list($outlet_ids = "")
	{
		$query = '"products_infos"."lang_id" = (case when (select count(id) as totalcount from products_infos where products_infos.lang_id = '.getCurrentLang().' and products.id = products_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		//Get the product data
		$products = DB::table('products')
					->select('products.id','products_infos.product_name')
					->join('products_infos','products_infos.id','=','products.id')
					->whereRaw($query)
					->where('products.active_status', '=', 1);
		if(!empty($outlet_ids))
		{
			$products = $products->whereIn('products.outlet_id', $outlet_ids);
		}
		$products = $products->orderBy('products_infos.product_name', 'asc')->get();
		$products_list = array();
		if(count($products)>0)
		{
			$products_list = $products;
		}
		return $products_list;
	}

	/*
	 * To get the outlet list based on vendor
	 */
	function getuserrole($user_id = "")
	{
		//Get the outlet data
		$roles = DB::table('roles_users')
					->select('ruid','role_id','role_name')
					->leftJoin('user_roles','user_roles.id','=','roles_users.role_id')
					->where('active_status', 1)
					->where('roles_users.user_id', $user_id);
		$roles = $roles->orderBy('role_name', 'asc')->get();
		$outlets_list = array();
		if(count($roles)>0)
		{
			$outlets_list = $roles;
		}
		return $outlets_list;
	}

	/*
	 * To set task list for all modules
	 */
	function tasks()
	{
		return array(
			/** roles and users module task **/
			'permissions' => array(
				'sort'  => '2',
				'title' => trans('messages.Permissions'),
				'children'  => array(
					'roles' => array(
						'title' => trans('messages.Roles'),
						'sort'  => '2',
						'task_note'  => trans('messages.Managing the roles'),
						'task_index' => '["system/permission"]',
						'apiresources'    => array (
							'permissions' => array(
								'title'   => trans('messages.Roles'),
								'description' => 'Managing Roles',
								'resource'    => array (
									'GET','POST','PUT','DELETE'
								)
							),
						),
					),
					'role/edit' => array(
						'title' => trans('messages.Roles Edit'),
						'sort'  => '3',
						'task_note'  => trans('messages.Edit,delete the roles and their tasks'),
						'task_index' => '["system/permission/create", "system/permission/edit", "update_role", "system/rolecreate", "system/permission/delete"]'
					),
					'users' => array(
						'title' => trans('messages.Roles Users'),
						'sort' => '4',
						'task_note' => trans('messages.Managing the users'),
						'task_index' => '["permission/users"]',
					),
					'edit' => array(
						'title' => trans('messages.Roles User Edit'),
						'sort' => '5',
						'task_note' => trans('messages.Edit,delete the role users'),
						'task_index' => '["permission/usercreate", "permission/userstore", "permission/users/edit", "usersupdate", "permission/users/delete"]'
					),
				),
			),
			/** roles and users module task **/
			/** vendors and outlets  module task **/
			'vendors' => array(
				'sort' => '2',
				'title' => trans('messages.Vendors'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Vendors'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the vendors'),
						'task_index' => '["vendors/vendors","vendors/vendor_details"]',
						'apiresources' => array (
							'vendors' => array(
								'title' => trans('messages.Vendors'),
								'description' => 'Managing Vendors',
								'resource' => array (
									'GET','POST','PUT','DELETE'
								)
							),
						),
					),
					'vendors/edit' => array(
						'title' => trans('messages.Vendors Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Edit,delete the vendors and their tasks'),
						'task_index' => '["vendors/create_vendor","vendors/edit_vendor","update_vendor","vendor_create ","vendors/delete_vendor"]'
					),
				),
			),
			'outlets' => array(
				'sort' => '2',
				'title' => trans('messages.Outlets'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Outlets'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the Outlets'),
						'task_index' => '["vendors/outlets","vendors/outlet_details"]',
						'apiresources' => array (
							'outlets' => array(
								'title' => trans('messages.Outlets'),
								'description' => 'Managing Outlets',
								'resource' => array (
									'GET','POST','PUT','DELETE'
								)
							),
						),
					),
					'outlets/edit' => array(
						'title' => trans('messages.Outlets Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Edit,delete the outlets and their tasks'),
						'task_index' => '["vendors/create_outlet","vendors/edit_outlet","update_outlet","outlet_create","vendors/delete_outlet"]'
					),
				),
			),
			'outletsmanagers' => array(
				'sort' => '2',
				'title' => trans('messages.Outlets Managers'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Outlets Managers'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the Outlets Managers'),
						'task_index' => '["vendors/outlet_managers"]',
					),
					'outletsmanagers/edit' => array(
						'title' => trans('messages.Outlets Managers Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Edit,delete the outlets managers and their tasks'),
						'task_index' =>	'["vendors/create_outlet_managers","vendors/edit_outlet_manager","admin/managers/update","create_manager","vendors/delete_outlet_managers"]'
					),
				),
			),
			'products' => array(
				'sort' => '2',
				'title' => trans('messages.Products'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Products'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the Products'),
						'task_index' => '["admin/products","admin/products/product_details"]',
						'apiresources' => array (
							'products' => array(
								'title' => trans('messages.Products'),
								'description' => 'Managing Products',
								'resource' => array (
									'GET','POST','PUT','DELETE'
								)
							),
						),
					),
					'products/edit' => array(
						'title' => trans('messages.Products Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Edit,delete the products and their tasks'),
						'task_index' => '["admin/products/create_product","admin/products/edit_product","update_product","product_create","admin/products/delete_product"]'
					),
				),
			),
			'drivers' => array(
				'sort' => '2',
				'title' => trans('messages.Drivers'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Drivers'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the Drivers'),
						'task_index' => '["admin/drivers","admin/drivers/view"]',
						'apiresources' => array (
							'drivers' => array(
								'title' => trans('messages.Drivers'),
								'description' => 'Managing Drivers',
								'resource' => array (
									'GET','POST','PUT','DELETE'
								)
							),
						),
					),
					'drivers/edit' => array(
						'title' => trans('messages.Drivers Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Edit,delete the drivers and their tasks'),
						'task_index' => '["admin/drivers/create","admin/drivers/edit","admin/drivers/update","create_driver","admin/drivers/delete"]'
					),
				),
			),
			'coupons' => array(
				'sort' => '2',
				'title' => trans('messages.Coupons'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Coupons'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the Coupons'),
						'task_index' => '["admin/coupons","admin/coupons/view"]',
						'apiresources' => array (
							'drivers' => array(
								'title' => trans('messages.Coupons'),
								'description' => 'Managing Coupons',
								'resource' => array (
									'GET','POST','PUT','DELETE'
								)
							),
						),
					),
					'coupons/edit' => array(
						'title' => trans('messages.Coupons Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Edit,delete the coupons and their tasks'),
						'task_index' => '["admin/coupons/create","admin/coupons/edit","admin/coupons/update","create_coupon","admin/coupons/delete"]'
					),
				),
			),
			'subscribers' => array(
				'sort' => '2',
				'title' => trans('messages.Subscribers'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Subscribers'),
						'sort' => '2',
						'task_note' => trans('messages.Subscribers List'),
						'task_index' => '["admin/subscribers"]',
					),
					'delete' => array(
						'title' => trans('messages.Subscribers'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the subscribers'),
						'task_index' => '["admin/subscribers/delete","admin/subscribers/updateStatus"]',
					),
				),
			),
			'newsletter' => array(
				'sort' => '2',
				'title' => trans('messages.Newsletter'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Newsletter'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the newsletter'),
						'task_index' => '["admin/newsletter","send_newsletter"]',
					),
				),
			),
			'cms' => array(
				'sort' => '2',
				'title' => trans('messages.CMS'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.CMS'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the cms'),
						'task_index' => '["admin/cms","admin/cms/view"]',
					),
					'cms/edit' => array(
						'title' => trans('messages.Cms Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Edit,delete the cms and their tasks'),
						'task_index' => '["admin/cms/create","admin/cms/edit","updatecms","createcms","admin/cms/delete"]'
					),
				),
			),
			'blog' => array(
				'sort' => '2',
				'title' => trans('messages.Blog'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Blog'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the blog'),
						'task_index' => '["admin/blog"]',
					),
					'blog/edit' => array(
						'title' => trans('messages.Blog Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Edit,delete the blog and their tasks'),
						'task_index' => '["admin/blog/create","createblog","admin/blog/edit","updateblog","admin/blog/delete","admin/blog/view"]'
					),
				),
			),
			/** roles and users module task **/
			'users' => array(
				'sort' => '2',
				'title' => trans('messages.Users'),
				'children' => array(
					'users/groups' => array(
						'title' => trans('messages.Groups'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the groups'),
						'task_index' => '["admin/users/groups"]',
					),
					'groups/edit' => array(
						'title' => trans('messages.Edit Group'),
						'sort' => '3',
						'task_note' => trans('messages.Add,Edit,delete the group and their tasks'),
						'task_index' => '["admin/groups/create","creategroup","admin/groups/edit","update_group","admin/groups/delete"]'
					),
					'users/addresstype' => array(
						'title' => trans('messages.Address Type'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the address type'),
						'task_index' => '["admin/users/addresstype"]',
					),
					'addresstype/edit' => array(
						'title' => trans('messages.Edit Address Type'),
						'sort' => '3',
						'task_note' => trans('messages.Add,Edit,delete the address type and their tasks'),
						'task_index' => '["admin/addresstype/create","createaddresstype","admin/addresstype/edit","update_addresstype","admin/addresstype/delete"]'
					),
					'list' => array(
						'title' => trans('messages.Users'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the users'),
						'task_index' => '["admin/users/index"]',
						'apiresources' => array (
							'permissions' => array(
								'title' => trans('messages.Users'),
								'description' => 'Managing Users',
								'resource' => array (
									'GET','POST','PUT','DELETE'
								)
							),
						),
					),
					'users/edit' => array(
						'title' => trans('messages.Users Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Edit,delete the users and their tasks'),
						'task_index' => '["admin/users/create","admin/users/edit","update_users","createuser","admin/users/delete"]'
					),
				),
			),
			'category' => array(
				'sort' => '2',
				'title' => trans('messages.Category'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Category'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the category'),
						'task_index' => '["admin/category"]',
					),
					'category/edit' => array(
						'title' => trans('messages.Category Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Add,Edit,delete the category and their tasks'),
						'task_index' => '["admin/category/create","createcategory","admin/category/edit","updatecategory","admin/category/delete"]'
					),
				),
			),
			/** email notification **/
			'notification' => array(
				'sort' => '2',
				'title' => trans('messages.Email Notification'),
				'children' => array(
					/*'list' => array(
						'title' => trans('messages.Email Notification Subject'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the notification subjects'),
						'task_index' => '["admin/template/subjects"]',
					),
					'subjects/edit' => array(
						'title' => trans('messages.Notification subjects edit'),
						'sort' => '3',
						'task_note' => trans('messages.Add,Edit the notification subjects'),
						'task_index' => '["admin/subjects/create","admin/subjects/edit","admin/subjects/update","createsubject"]'
					),*/
					'list' => array(
						'title' => trans('messages.Notification Templates'),
						'sort' => '4',
						'task_note' => trans('messages.Managing the notification templates'),
						'task_index' => '["admin/templates/email"]',
					),
					'templates/edit' => array(
						'title' => trans('messages.Notification templates edit'),
						'sort' => '5',
						'task_note' => trans('messages.Add,edit the notification templates'),
						'task_index' => '["admin/templates/create","createtemplate", "admin/templates/edit","admin/template/update","admin/templates/view","admin/templates/delete"]'
					),
				),
			),
			'banners' => array(
				'sort' => '2',
				'title' => trans('messages.Banners'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Banners'),
						'sort' => '2',
						'task_note' => trans('messages.Managing the banners'),
						'task_index' => '["admin/banners"]',
					),
					'banners/edit' => array(
						'title' => trans('messages.Banners Edit'),
						'sort' => '3',
						'task_note' => trans('messages.Add,Edit,delete the banner and their tasks'),
						'task_index' => '["admin/banner/create", "createbanner", "admin/banner/edit", "admin/banner/update", "admin/banner/ajaxupdate"]'
					),
				),
			),
			/*'settings' => array(
				'sort' => '2',
				'title' => trans('messages.Settings'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Settings'),
						'sort'  => '2',
						'task_note'  => trans('messages.Managing the all site settings'),
						'task_index' => '[admin/settings/general", "admin/settings/store", "admin/settings/local", "admin/settings/email", "admin/settings/socialmedia", "admin/settings/image", "admin/payment/settings", "admin/modules/settings"]',
					),
				),
			),
			'reports_analytics' => array(
				'sort' => '2',
				'title' => trans('messages.Reports & Analytics'),
				'children' => array(
					'list' => array(
						'title' => trans('messages.Reports & Analytics'),
						'sort'  => '2',
						'task_note'  => trans('messages.Managing the all reports and analytics'),
						'task_index' => '[reports/order", "reports/returns", "reports/user", "reports/vendor"]',
					),
				),
			),*/
			'sales' => array(
				'sort' => '2',
				'title' => trans('messages.Sales'),
				'children' => array(
					'orders/index' => array(
						'title' => trans('messages.Orders'),
						'sort' => '2',
						'task_note' => trans('messages.Orders List'),
						'task_index' => '["admin/orders/index"]',
					),
					'orders/info' => array(
						'title' => trans('messages.View Orders'),
						'sort' => '3',
						'task_note' => trans('messages.View orders and their tasks'),
						'task_index' => '["admin/orders/update-status","admin/orders/info","admin/orders/load_history","admin/orders/delete"]'
					),
					'orders/return_orders' => array(
						'title' => trans('messages.Return Orders'),
						'sort' => '4',
						'task_note' => trans('messages.Return Orders List'),
						'task_index' => '["orders/return_orders"]',
					),
					'orders/return_orders_view' => array(
						'title' => trans('messages.View Return Orders'),
						'sort' => '5',
						'task_note' => trans('messages.View orders and their tasks'),
						'task_index' => '["orders/return_orders_view","update_return_order"]'
					),
					'orders/fund_requests' => array(
						'title' => trans('messages.Fund Requests'),
						'sort' => '6',
						'task_note' => trans('messages.Fund Requests List'),
						'task_index' => '["orders/fund_requests"]',
					),
					'orders/approve_fund_status' => array(
						'title' => trans('messages.Approve Fund Requests'),
						'sort' => '6',
						'task_index' => '["orders/approve_fund_status"]',
					),
				),
			),
			'user_notification' => array(
				'sort' => '2',
				'title' => trans('messages.Notifications'),
				'children' => array(
					'notifications' => array(
						'title' => trans('messages.Notifications'),
						'sort'  => '2',
						'task_note'  => trans('messages.Managing the notifications'),
						'task_index' => '["admin/notifications", "admin/read_notifications", "admin/email-notifications","send_email", "admin/push-notifications"]',
					),
				),
			),
			'reviews' => array(
				'sort' => '2',
				'title' => trans('messages.Reviews'),
				'children' => array(
					'reviews' => array(
						'title' => trans('messages.Reviews'),
						'sort'  => '2',
						'task_note'  => trans('messages.Managing the reviews'),
						'task_index' => '["admin/reviews"]',
					),
					'admin/reviews/view' => array(
						'title' => trans('messages.View Reviews'),
						'sort' => '3',
						'task_note' => trans('messages.View reviews and their tasks'),
						'task_index' => '["admin/reviews/view","admin/reviews/approve","admin/reviews/delete"]'
					)
				),
			),
            'brands' => array(
                'sort' => '2',
                'title' => trans('messages.Brands'),
                'children' => array(
                    'list' => array(
                        'title' => trans('messages.Brands'),
                        'sort'  => '2',
                        'task_note'  => trans('messages.Managing the brands'),
                        'task_index' => '["admin/brands"]',
                    ),
                    'edit' => array(
                        'title' => trans('messages.Edit Brands'),
                        'sort' => '3',
                        'task_note' => trans('messages.Add,Edit,delete the brands'),
                        'task_index' => '["admin/brand/create","createbrand","admin/brand/edit","updatebrand","admin/brand/delete"]'
                    )
                ),
            ),
		);
	}

	/*
	 * To check the module access 
	 */
	function hasTask($task_index, $check_owner = true)
    {
        if(Auth::id()==1 && $check_owner)
        {
            return true;
        }
        else {
			$access = getAccessTasks();
			if(in_array($task_index,$access))
			{
				return true;
			}
		}
        return false;
    }
    
	/*
	 * To check the module access 
	 */
    function getAccessTasks()
    {
		$roletasks  = array();
		$user_id    = Auth::id();
		$role_users = DB::table('roles_users')
						->select('role_id')
						->where('user_id', "=",$user_id)
						->get();
		if(count($role_users))
		{
			foreach($role_users as $user)
			{
				$rolet = getTasksIndex($user->role_id);
				if(count($rolet))
				{
					foreach($rolet as $tasks)
					{
						$roletasks[] = $tasks;
					}
				}
			}
		}
		//$diff = array_intersect(array_unique($roletasks),$menulist);
		$_assTasks = $roletasks;
		return $_assTasks;
    }

	/*
	 * To check the module access 
	 */
    function getTasksIndex($role_id)
    {
		$db = DB::table('role_tasks')
				->select('*')
				->where('role_id','=',$role_id)
				->get();
		$tasks = array();
		foreach($db as $result)
		{
			if($result->task_index!="")
			{
				$array = json_decode($result->task_index,true);
				if(is_array($array))
				{
					foreach($array as $ar)
					{
						$tasks[] = $ar;
					}
				}
				else {
					$tasks[] = $result->task_index;
				}
			}
		}
		//$this->_tasksIndex[$role_id] = $tasks;
		return isset($tasks)? $tasks: array();
	}

    function getlocation($api)
    {
		return $api->getLocation();
	}

	function getCity($api)
    {
		return $api->getCity();
	}

	function getFeatureSstore($api)
    {
		return $api->getFeatureSstore();
	}

	/** get Location list **/
	function getFrontLocationList($city_url)
	{
		//Get the location areas data
		$locations_query = '"zones_infos"."language_id" = (case when (select count(*) as totalcount from zones_infos where zones_infos.language_id = '.getCurrentLang().' and zones.id = zones_infos.zone_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$locations=DB::table('zones')
			->select('zones.id','zones_infos.zone_name','zones.url_index')
			->leftJoin('zones_infos','zones_infos.zone_id','=','zones.id')
			->leftJoin('countries','countries.id','=','zones.country_id')
			->leftJoin('cities','cities.id','=','zones.city_id')
			->whereRaw($locations_query)
			->where('zones_status',  1)
			->where('cities.url_index', $city_url)
			->orderBy('zone_name', 'asc')
			->get();
		$locations_list=array();
		if(count($locations)>0){
			$locations_list = $locations;
		}
		return $locations_list;
	}

	function hasTaskmerchant($task_index, $check_owner = true)
    {
        if(Session::get('vendor_type')==1 && $check_owner)
        {
            return true;
        }
        else {
            $access = getMerchantAccessTasks();
            if(in_array($task_index,$access))
            {
                return true;
            }
        }
        return false;
    }


	function getoffers($api)
    {
		return $api->getOffers(); 	
	}

	/**
     * Get user types
     *
     * @param  
     * @return array 
     */
	function getBannerTypes()
    {
		$types=array(1=>'Common',2=>'Store',3=>'New Arrivals', 4=>'Great Deals', 5=>'Most popular');
		return $types;
    }

    /** get current Position **/
	function getCurrencyPosition()
	{
		$currency_side = DB::table('settings')->select('settings.currency_side')->first();
		return $currency_side;
	}
    /** get current Language **/
	function getCurrency($language_id = '')
	{ 
		if($language_id == '')
		{
		$language_id = getAdminCurrentLang();
	    }
		$query = '"currencies_infos"."language_id" = (case when (select count(*) as totalcount from currencies_infos where currencies_infos.language_id = '.$language_id.' and currencies.id = currencies_infos.currency_id) > 0 THEN '.$language_id.' ELSE 1 END)';
		$currentcurrency = DB::table('currencies')
		                  ->select('currencies_infos.currency_symbol')
		                  ->leftjoin('settings','settings.default_currency','=','currencies.id')
		                  ->leftjoin('currencies_infos','currencies_infos.currency_id','=','currencies.id')
		                 ->where("currencies.active_status","=","A")
		                  ->where("currencies.default_status","=",1)
		                  ->whereRaw($query)
		                  ->first();
		                 // print_r($currentcurrency);exit;
		$current_currency_code = '';
		if(count($currentcurrency)>0)
		{
			if($currentcurrency->currency_symbol)
			{
				$current_currency_code = $currentcurrency->currency_symbol;
			}
		}
		return $current_currency_code;
	}
	
	function getCurrencycode()
	{
		$currentcurrency = DB::table('currencies')->where('active_status', 'A')->where('id',Session::get('general')->default_currency)->get();
		$current_currency_code = '';
		if(count($currentcurrency)>0)
		{
			$current_currency_code = $currentcurrency[0]->currency_code;
		}
		return $current_currency_code;
	}
	
	 function get_coperativess()
    {
		$cooprative_list[""] = trans("messages.Select cooperative");
		$category_id = DB::table('categories')->select('id')->where('url_key','=','cooperative')
						->first();
		
		if(isset($category_id->id))
		{
			$condition ="(regexp_split_to_array(category_ids,',')::integer[] @> '{". $category_id->id."}'::integer[]  and category_ids !='')";
			$query1    = 'outlet_infos.language_id = (case when (select count(outlet_infos.id) as totalcount from outlet_infos where outlet_infos.language_id = '.getCurrentLang().' and outlets.id = outlet_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
			$vendors = DB::table('vendors')
				//->Leftjoin('vendors_infos','vendors_infos.id','=','vendors.id')
				->Leftjoin('outlets','outlets.vendor_id','=','vendors.id')
				->Leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
				->select('outlets.id as outlets_id','outlet_infos.outlet_name')
				->whereRaw($condition)
				->whereRaw($query1)
				->get();
			if(count($vendors) > 0)
			{
				//print_r($vendors);
				foreach ($vendors as $cooprative)
				{
					//echo $cooprative->outlet_name;
					if(isset($cooprative->outlets_id) && isset($cooprative->outlet_name))
					{
						$cooprative_list[$cooprative->outlets_id] = $cooprative->outlet_name;
					}
				}
			}
		}
		return $cooprative_list;
	}
	function getOutLetCount($vendor_id)
	{
		$outlet_count = '';
		return $outlet_count;
	}
	function get_user_details($user_id)
	{
		$user_detail = DB::table('customers_view')
						->select('customers_view.social_title','customers_view.first_name','customers_view.last_name','customers_view.email','customers_view.gender','customers_view.civil_id','customers_view.cooperative as cooperative_id','customers_view.cooperative','customers_view.member_id','customers_view.image','customers_view.mobile','customers_view.name')
						->where('customers_view.id',$user_id)
						->first();
		return $user_detail;
	}
        function get_user_details1($user_id)
    {
        $user_detail = DB::table('admin_customers')
                        ->select(/*'admin_customers.social_title',*/'admin_customers.firstname',
                            'admin_customers.lastname','admin_customers.email','admin_customers.gender',
                            'admin_customers.image','admin_customers.mobile_number',
                            'admin_customers.customer_unique_id','admin_customers.referred_customer_id','admin_customers.referral_status','admin_customers.wallet_amount')
                        ->where('admin_customers.id',$user_id)
                        ->first();
        return $user_detail;
    }
	/* To get the active payment list */
	function get_active_payment_gateway_list()
	{
		$payment_details = DB::table('payment_gateways')
							->select('id')
							->where('active_status',1)
							->get();
		return $payment_details;
	}
	function get_coupon_details($coupon_id)
	{
		
		$coupon_details = DB::table('coupons')
							->select('coupons_infos.coupon_title','coupons.coupon_code')
						    ->join('coupons_infos','coupons_infos.id','=','coupons.id')
							->where('coupons.id',$coupon_id)
							->first();
		return $coupon_details;
	}
	function get_admin_product_details($product_id)
	{
		$query = '"products_infos"."lang_id" = (case when (select count(products_infos.id) as totalcount from products_infos where products_infos.lang_id = '.getAdminCurrentLang().' and products.id = products_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
		$product_details = DB::table('products')
							->select('products_infos.product_name','products.id')
							->join('products_infos','products_infos.id','=','products.id')
							->where('products.id',$product_id)
							->whereRaw($query)
							->first();
		return $product_details;
	}
	function get_admin_vendor_details($product_id)
	{
		$query  = '"vendors_infos"."lang_id" = (case when (select count(vendors_infos.id) as totalcount from vendors_infos where vendors_infos.lang_id = '.getAdminCurrentLang().' and vendors.id = vendors_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
		$query1 = '"products_infos"."lang_id" = (case when (select count(products_infos.id) as totalcount from products_infos where products_infos.lang_id = '.getAdminCurrentLang().' and products.id = products_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
		$vendor_details = DB::table('products')
							->select('vendors_infos.vendor_name','vendors.id')
							->join('vendors','vendors.id','=','products.vendor_id')
							->join('vendors_infos','vendors_infos.id','=','vendors.id')
							->join('products_infos','products_infos.id','=','products.id')
							->where('products.id',$product_id)
							->whereRaw($query)
							->whereRaw($query1)
							->first();
		return $vendor_details;
	}
	/* to get modules list */
	function modules_list()
	{
		$module_list = DB::table('modules')->select('module_name','active_status')->first();
		return $module_list;
	}
	function get_cms_list($index="")
    {
        $cms_list = DB::table('cms')
                        ->select('cms_infos.title','cms_infos.content','cms.id','cms.url_index')
                        ->join('cms_infos','cms_infos.cms_id','=','cms.id')
                        ->where('cms.url_index','=',$index)
                        ->where('cms.cms_status','=',1)
                        ->first();
        return $cms_list;
    }
    function get_users_list_ids($user_type)
    {
		$user_list = DB::table('customers_view')
                        ->select('id','email','first_name','last_name','android_device_token','ios_device_token')
                        ->where('status','=',1)
                        ->where('is_verified','=',1);
		if($user_type == 1)
		{
			$user_list = $user_list->where('android_device_token','<>','');
		}
		if($user_type == 2)
		{
			$user_list = $user_list->where('ios_device_token','<>','');
		}
		$user_list = $user_list->get();
		return $user_list;
	}

	/* drivers list by outlet_id */
    function drivers_list($outlet_latitude, $outlet_longitude)
    {
        $drivers = DB::select("select DISTINCT ON (driver_track_location.driver_id) driver_id, drivers.first_name, drivers.last_name, earth_distance(ll_to_earth(".$outlet_latitude.",".$outlet_longitude."), 

        	ll_to_earth(driver_track_location.latitude, driver_track_location.longitude)) as distance 

        	from drivers 

        	left join driver_track_location on driver_track_location.driver_id = drivers.id 

        	where 

        	/*earth_box(ll_to_earth(".$outlet_latitude.",".$outlet_longitude."), 5000) @> 

        	ll_to_earth(driver_track_location.latitude, driver_track_location.longitude)

        	 and 

        	 */
        	 drivers.active_status=1 and drivers.is_verified=1 order by driver_track_location.driver_id,distance asc");
        return $drivers;
    }
    /* drivers detail by id */
    function driver_details($driver_track_id)
    {
        $drivers = DB::table('drivers')
                        ->join('driver_track_location','driver_track_location.driver_id','=','drivers.id')
                        ->select('drivers.id','drivers.first_name','drivers.last_name','drivers.driver_status','driver_track_location.latitude','driver_track_location.longitude')
                        ->where('driver_track_location.id','=',$driver_track_id)
                        ->where('drivers.is_verified','=',1)
                        ->where('drivers.active_status','=',1)
                        ->first();
        return $drivers;
    }

	/* drivers list by outlet_id */
    /*function drivers_list($outlet_latitude, $outlet_longitude)
    {
        $drivers = DB::select("select DISTINCT ON (driver_track_location.driver_id) driver_id, drivers.first_name, drivers.last_name, earth_distance(ll_to_earth(".$outlet_latitude.",".$outlet_longitude."), ll_to_earth(driver_track_location.latitude, driver_track_location.longitude)) as distance from drivers left join driver_track_location on driver_track_location.driver_id = drivers.id where earth_box(ll_to_earth(".$outlet_latitude.",".$outlet_longitude."), 5000) @> ll_to_earth(driver_track_location.latitude, driver_track_location.longitude) and drivers.active_status=1 and drivers.is_verified=1 order by driver_track_location.driver_id,distance asc");
        return $drivers;
    }
    /* drivers detail by id */
    /*function driver_details($driver_track_id)
    {
        $drivers = DB::table('drivers')
                        ->join('driver_track_location','driver_track_location.driver_id','=','drivers.id')
                        ->select('drivers.id','drivers.first_name','drivers.last_name','drivers.driver_status','driver_track_location.latitude','driver_track_location.longitude')
                        ->where('driver_track_location.id','=',$driver_track_id)
                        ->where('drivers.is_verified','=',1)
                        ->where('drivers.active_status','=',1)
                        ->first();
        return $drivers;
    }*/

	function getCategoryVendorLists($head_categories,$language='')
	{
		if($language){
			$query  = '"vendors_infos"."lang_id" = (case when (select count(vendors_infos.id) as totalcount from vendors_infos where vendors_infos.lang_id = '.$language.' and vendors.id = vendors_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
			
		}else {
			$query  = '"vendors_infos"."lang_id" = (case when (select count(vendors_infos.id) as totalcount from vendors_infos where vendors_infos.lang_id = '.getAdminCurrentLang().' and vendors.id = vendors_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
		}
		   $c_ids = $head_categories;
           $c_ids = explode(",", $c_ids);
           $c_ids = implode($c_ids, "','");
           $c_ids = "'" . $c_ids . "'";
		   $condition = " vendor_category_mapping.category in($c_ids)";
			$data=DB::table('vendors')
					->select('vendors.id','vendors_infos.vendor_name')
					->leftJoin('vendors_infos','vendors_infos.id','=','vendors.id')
					->join('vendor_category_mapping', 'vendor_category_mapping.vendor_id', '=', 'vendors.id')
					->where('active_status',  1)
					//->where('featured_vendor',  1)
					->whereRaw($query)
					->whereRaw($condition)
					->orderBy('vendor_name', 'asc')
					->get();
		  $data_list=array();
		if(count($data)>0){
			$data_list = $data;
		}
		return $data_list;
		
	}
	function head_categories_list_by_url($category_url)
	{
		$categories = DB::table('categories')
						->select('categories.id')
						->where('category_type',2)
						->where('url_key',$category_url)
						->where('category_status',  1)
						->first();
		return $categories;
	}
    function get_feature_brands()
	{
		$brands = DB::table('brands')
						->select('brands.brand_title','brands.brand_image','brands.brand_link')
						->where('status',  1)
						->get();
		return $brands;
	}
	
	 function get_view_store()
	{
		$query = '"outlet_infos"."language_id" = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.getCurrentLang().' and outlets.id = outlet_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
		$outlets = DB::table('outlets')
					->select('outlets.id','outlet_infos.outlet_name','outlets.url_index')
					->join('outlet_infos','outlet_infos.id','=','outlets.id')
					->whereRaw($query)
					->where('active_status', 1)
					->orderBy('outlets.view_count', 'desc')
					->limit(8)
					->get();
					
		return $outlets;
	}
	function get_cart_count()
	{
		if(Session::get('user_id'))
		{
			$cdata = DB::table('cart')
					->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
					->select('cart_detail.cart_id',DB::raw('count(cart_detail.cart_detail_id) as cart_count'))
					->where("cart.user_id","=",Session::get('user_id'))
					->groupby('cart_detail.cart_id')
					->get();
			if(count($cdata))
			{
				$cart_item = $cdata[0]->cart_count;
			}
			return $cdata;
		}
		
	}
	function get_banner_list($language_id)
	{
		$language_id = getCurrentLang();
		$banners = DB::table('banner_settings')
					->select('banner_settings.banner_setting_id','banner_settings.banner_title','banner_settings.banner_image','banner_settings.banner_link','banner_settings.language_type')
					->where('banner_type', 1)
					->where('status', 1)
					->where('banner_settings.language_type',$language_id)
					->orderBy('default_banner', 'desc')
					->get();
		
			return $banners;			
	}
	function get_store_banner_list($language_id)
	{
		$language_id = getCurrentLang();
		$banners = DB::table('banner_settings')
					->select('banner_settings.banner_setting_id','banner_settings.banner_title','banner_settings.banner_image','banner_settings.banner_link','banner_settings.language_type')
					->where('banner_type', 2)
					->where('status', 1)
					->where('banner_settings.language_type',$language_id)
					->orderBy('default_banner', 'desc')
					->get();
		
			return $banners;			
	}
	function get_product_vendor($vendor_category_id,$product_url)
	{
		$product_vendors   = DB::table('products')
							->select('products.vendor_id')
							->where('products.vendor_category_id','=',$vendor_category_id)
							->where('products.product_url','=',$product_url)
							->orderBy('products.vendor_id', 'desc')
							->groupBy('products.vendor_id')
							->get();
		return $product_vendors;					
	}
	function getProductBasedOutlet($vendor_category_id, $product_url, $vendor_id)
	{
		$product_outlets = DB::table('products')
							->select('products.outlet_id','products.id')
							->where('products.vendor_category_id','=',$vendor_category_id)
							->where('products.vendor_id','=',$vendor_id)
							->where('products.product_url','=',$product_url)
							->orderBy('products.outlet_id', 'desc')
							->distinct()
							->get();
		return $product_outlets;					
	}
	 function getSettingsLists()
    {
        $language_id = getCurrentLang();
        $query = 'settings_infos.language_id = (case when (select count(settings_infos.language_id) as totalcount from settings_infos where settings_infos.language_id = '.$language_id.' and settings.id = settings_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $data = DB::table('settings')
                    ->leftjoin('settings_infos','settings_infos.id','=','settings.id')
                    ->select('settings.id','settings_infos.copyrights','settings_infos.site_name','settings_infos.site_description','settings_infos.meta_title',
                        'settings_infos.meta_keywords','settings_infos.meta_description')
                    ->whereRaw($query)->where('settings.id','=',1)->first();
        return $data;
        //print_r($data);exit;
    }
    function getoutletsCategoryLists($id)
	{
		$vdata = DB::table('vendors')->select('category_ids')->where('id',$id)->get();
		//print_r($vdata);die;
		$data_list = array();
		if(count($vdata))
		{
			$cids = explode(',',$vdata[0]->category_ids);
			//Get the categories data
			$query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
			$data = DB::table('categories')
					->select('categories.id','categories_infos.category_name','categories.url_key')
					->join('categories_infos','categories_infos.category_id','=','categories.id')
					->whereRaw($query)
					->where('category_status','=',1)
					->where('category_type','=',2)
					->whereIn('categories.id',"IN", $cids)
					->orderBy('category_name', 'asc')
					->get();
					//print_r($data);exit;
			if(count($data)>0)
			{
				$data_list = $data;
			}
			return $data_list;
		} 
		else
		{
			return $data_list;
		}
	}
	function geoutletCategoryLists($cids,$language)
	{
		$query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.$language.' and categories.id = categories_infos.category_id) > 0 THEN '.$language.' ELSE 1 END)';
		$data = DB::table('categories')
					->select('categories.id','categories_infos.category_name','categories.url_key')
					->join('categories_infos','categories_infos.category_id','=','categories.id')
					->whereRaw($query)
					->where('category_status','=',1)
					->where('category_type','=',2)
					->whereIn('categories.id', $cids)
					->orderBy('category_name', 'asc')
					->get();
		return $data;
	}
	 function GetDrivingDistance($lat1, $long1, $lat2, $long2,$unit="k")
    { 
		
			$theta = $long1 - $long2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$unit = strtoupper($unit);
            $distance =($miles * 1.609344);
			return number_format($distance,1);
    }
	function utf8_encode_deep(&$input) {
		if (is_string($input)) {
			$input = utf8_encode($input);
		} else if (is_array($input)) {
			foreach ($input as &$value) {
				utf8_encode_deep($value);
			}

			unset($value);
		} else if (is_object($input)) {
			$vars = array_keys(get_object_vars($input));

			foreach ($vars as $var) {
				utf8_encode_deep($input->$var);
			}
		}
	}
	function getProduct_category_image($cate_url)
	{
		//echo $store_id;exit;
			$data_list = array();
			//Get the categories data
			$query = '"categories_infos"."language_id" = (case when (select count(categories_infos.category_id) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
			$data = DB::table('categories')
					->select('categories.id','categories.url_key','categories_infos.category_name','categories.image')
					->rightJoin('products','products.category_id','=','categories.id')
					->join('categories_infos','categories_infos.category_id','=','categories.id')
					->whereRaw($query)
					->where('url_key','=',$cate_url)
					//->where('categories.id','=',$category_id)
					->where('products.active_status','=',1)
					->where('category_type','=',1)
					->groupBy('categories.id',"categories_infos.category_name")
					->get();
			if(count($data)>0)
			{
				$data_list = $data;
			}
			return $data_list;
	}
	function getCms_faq()
	{
		$language = getCurrentLang();
		$query = 'cms_infos.language_id = (case when (select count(cms_infos.language_id) as totalcount from cms_infos where cms_infos.language_id = '.$language.' and cms.id = cms_infos.cms_id) > 0 THEN '.$language.' ELSE 1 END)';
		$cms = DB::table('cms')->select('cms.id','cms.url_index','cms.sort_order','cms_infos.title')
			->leftJoin('cms_infos','cms_infos.cms_id','=','cms.id')
			->whereRaw($query)
			->where('cms.cms_type','=',2)
			->where('cms.cms_status','=',1)
			->orderBy('cms.sort_order', 'asc')
			->get();
			$cms_items=array();
			if(count($cms)>0){
				$cms_items = $cms;
			}
			return $cms_items;
			
	}
	function getNewProductBasedOutlet($product_url, $outlet_id)
	{
		$product_new_outlets = DB::table('products')
							->select('products.outlet_id')
							//->where('products.vendor_category_id','=',$vendor_category_id)
							->where('products.outlet_id','=',$outlet_id)
							->where('products.product_url','=',$product_url)
							->orderBy('products.outlet_id', 'desc')
							->distinct()
							->get();
		return $product_new_outlets;					
	}

	function openurl($url)
	{

		$ch=curl_init();

		curl_setopt($ch,CURLOPT_URL,$url);

		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_TIMEOUT, ’3′);
		$content = trim(curl_exec($ch));  curl_close($ch);

		return true;

	}
    
    function getbrands()
    {
        $query = "brand_infos.language_id = (
                    case 
                        when ( 
                                select count(*) as totalcount FROM brand_infos 
                                WHERE brand_infos.language_id = " . getCurrentLang() . " and brands.id = brand_infos.brand_id 
                        )    >  0
                        then  " . getCurrentLang() . "   else   1   
                        
                    end ) ";
        
        $data = DB::table('brands')
                ->select('brands.id', 'brand_infos.title')
                ->leftJoin('brand_infos', 'brand_infos.brand_id','=','brands.id')
                ->whereRaw($query)
                ->where('status', 1)
                ->orderBy('title', 'asc')
                ->get();
                
        if(count($data) > 0)
        {
            return $data;
        }
    }
    
    /**
    * returns tree of Parent product categories with child categories
    */
    function getProductCategorySubcategory($parent = array())
    {
	
		$ci1 = 'ci1.language_id = (
								
							CASE 
							WHEN (
										select count(*) as totalcount from categories_infos 
										where 
											categories_infos.language_id = '.getCurrentLang().' 
											and 
											c1.id = categories_infos.category_id) > 0 
							THEN '.getCurrentLang().' ELSE 1 END)';

		$ci2 = 'ci2.language_id = (
								
							CASE 
							WHEN (
										select count(*) as totalcount from categories_infos 
										where 
											categories_infos.language_id = '.getCurrentLang().' 
											and 
											c2.id = categories_infos.category_id) > 0 
							THEN '.getCurrentLang().' ELSE 1 END)';
 
    	$query = "	WITH 
					parent_tab AS (

					SELECT 	c1.url_key, 
							c1.id,
							c1.parent_id,
							c1.head_category_ids,
							c1.category_level,
							c1.sort_order,
							c1.image as image,
							ci1.category_name,
							ci1.language_id
					FROM 	categories c1 
							left join categories_infos ci1 
								on(c1.id = ci1.category_id )
					WHERE c1.category_status = 1 and " . $ci1 ." ";

					if(isset($parent) && !empty($parent))
					{
						$query .= " and c1.id IN " . implode(',', $parent) ;
					}

		$query .=	"),

					child_tab AS (
					
					SELECT 	c2.url_key,
							c2.id,
							c2.parent_id,
							c2.head_category_ids,
							c2.category_level,
							c2.sort_order,
							c2.image as image,
							ci2.category_name,
							ci2.language_id
					FROM 	categories c2 
							left join categories_infos ci2 
							on(c2.id = ci2.category_id )
					WHERE c2.category_status = 1 and " . $ci2 ."
					)

					SELECT 	parent_tab.id, 
							parent_tab.url_key,
							parent_tab.category_name,
							parent_tab.image as image,
							parent_tab.category_level,
							array_agg('[' || 
											child_tab.id || 
											':' || 
											child_tab.url_key || 
											':' || 
											REPLACE(child_tab.category_name, ',', ';') || 
											':' || 
											child_tab.image ||
											':' ||
											child_tab.head_category_ids
									|| ']') as children     
					FROM  	parent_tab left join child_tab
							on (parent_tab.id = child_tab.head_category_ids)
					WHERE   parent_tab.category_level = 2
					GROUP BY (parent_tab.id, parent_tab.url_key, parent_tab.category_name, parent_tab.image,parent_tab.category_level,parent_tab.sort_order)
					ORDER BY parent_tab.sort_order asc

					";

			$data = DB::select($query);
//echo '<pre>'; print_r($data);exit;
			$result = [];

			$i = 0;
			foreach ($data as $value) {

				$result[$i]['category_id'] 		=  $value->id;
				$result[$i]['url_key'] 			=  $value->url_key;
				$result[$i]['category_name'] 	=  $value->category_name;
				$result[$i]['image'] 			=  $value->image;


				if($value->children == '{NULL}'){

					//$result[$i]['children'] = '';
				}
				else //  {[Chips:16:chips],[Biscuits:17:biscuits]}
				{

					$children = trim($value->children, '{}'); // [Chips:16:chips],[Biscuits:17:biscuits]
					$childArr = explode(',', $children);      // array( 0=> [Chips:16:chips],
															  // 		1=> [Biscuits:17:biscuits])

					$j=0;
					$childArr = [];
					foreach (explode(',', $children) as $val) {
						$val = trim($val,'[]"'); // Chips:16:chips
						$tmp = explode(':', $val); // array( 0=> Chips, 1=>16, 2=>chips)
	 		
						$cat_id 	= isset($tmp[0])?$tmp[0]:'';
						$url_key 	= isset($tmp[1])?$tmp[1]:'';
						$cat_name 	= isset($tmp[2])?$tmp[2]:'';
						$img 		= isset($tmp[3])?$tmp[3]:'';
						$parent_id 	= isset($tmp[4])?$tmp[4]:'';

						array_push($childArr, array(
													'category_id'=> $cat_id,
													'url_key'=>  $url_key,
													'category_name'=> str_replace(';', ',', $cat_name),
													'image'=> $img,
													'parent_id' => $parent_id
													)
						);

 						$j++;
					}

					$result[$i]['children'] = $childArr;
				}

				$i++;
				
			}
			return $result;
    }

    function categoryFromKey($url_key)
    {
    	$query = ' categories_infos.language_id = (
    					CASE 

    					WHEN (	select count(info_id) as totalcount 
    							FROM 	
    							categories_infos 
    							WHERE 
    							categories_infos.language_id = '.getCurrentLang().' 
    							and categories.id = categories_infos.category_id
    						 ) 
    						 > 0 
    					THEN '.getCurrentLang().' ELSE 1 END)';


		$product = DB::table('categories')
					->select('categories.id','categories_infos.category_name','categories.url_key','categories.image')
					->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
					->whereRaw($query)
					->where('categories.url_key','=', $url_key)
					->where('categories.category_status', '=', 1)
					->get();

		return $product[0];
    }

    function getGroupedProductCountInCategory($category_id)
    {
		$query = ' categories_infos.language_id = (
				CASE 

				WHEN (	select count(info_id) as totalcount 
						FROM 	
						categories_infos 
						WHERE 
						categories_infos.language_id = '.getCurrentLang().' 
						and categories.id = categories_infos.category_id
					 ) 
					 > 0 
				THEN '.getCurrentLang().' ELSE 1 END)';

    	$count = DB::table('products')
    					->select(DB::raw('COUNT(products.id) as totalcount'),'products.sub_category_id','categories.category_white_image','categories_infos.category_name', 'categories.category_status')
    					->leftJoin('categories', 'categories.id', '=', 'products.sub_category_id')
    					->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
    					->whereRaw($query)
    					->where('products.category_id', '=', $category_id)
    					->where('products.active_status', '=', 1)
    					->where('products.approval_status', '=', 1)
    					->groupBy('products.sub_category_id', 'categories.category_white_image','categories_infos.category_name','categories.category_status')
    					->get();

    	return $count;
    }

    function getProductCountInCategory($category_id)
    {

    	$count = DB::table('products')
    					->select(DB::raw('COUNT(products.id) as totalcount'))
    					->leftJoin('categories', 'categories.id', '=', 'products.category_id')
    					->where('products.category_id', '=', $category_id)
    					->where('products.active_status', '=', 1)
    					->where('products.approval_status', '=', 1)
    					->get();

    	return $count[0]->totalcount;

    }

    function getProductCountInSubCategory($subcategory_id)
    {

    	$count = DB::table('products')
    					->select(DB::raw('COUNT(products.id) as totalcount'))
    					->leftJoin('categories', 'categories.id', '=', 'products.category_id')
    					->where('products.sub_category_id', '=', $subcategory_id)
    					->where('products.active_status', '=', 1)
    					->where('products.approval_status', '=', 1)
    					->get();

    	return $count[0]->totalcount;

    }


    function initMeta()
    {
    			SEOMeta::setTitle(Session::get("general_site")->site_name.' - ' .'Stores');
		SEOMeta::setDescription(Session::get("general_site")->site_name.' - ' .'Stores');
		SEOMeta::addKeyword(Session::get("general_site")->site_name.' - ' .'Stores');
		OpenGraph::setTitle(Session::get("general_site")->site_name.' - ' .'Stores');
		OpenGraph::setDescription(Session::get("general_site")->site_name.' - ' .'Stores');
		OpenGraph::setUrl(URL::to('/'));
		Twitter::setTitle(Session::get("general_site")->site_name.' - ' .'Stores');
		Twitter::setSite(Session::get("general_site")->site_name.' - ' .'Stores');
    }
    
    function getCityKey($city_id)
    {
    	$city = DB::table('cities')
    			->select('url_index')
    			->where('id', $city_id)
    			->get();
    	return $city[0]->url_index;


    }


    function getCountryKey($country_id)
    {
    	$country = DB::table('cities')
    			->select('url_index')
    			->where('id', $country_id)
    			->get();
    	return $country->url_index;
    }


    
    function getCurrCityKey()
    {

    	$settings = App\Model\settings::find(1);
        $country = $settings->default_country;
        $city_id = $settings->default_city;

    	$city = DB::table('cities')
    			->select('url_index')
    			->where('id', $city_id)
    			->get();
    	return $city[0]->url_index;

    }

    function get_promotional_banner($promo)
	{
		$language_id = getCurrentLang();
		$banners = DB::table('banner_settings')
					->select('banner_settings.banner_setting_id','banner_settings.banner_title','banner_settings.banner_image','banner_settings.banner_link','banner_settings.language_type')
					->where('banner_type', $promo)
					->where('status', 1)
					->where('banner_settings.language_type',$language_id)
					->orderBy('default_banner', 'desc')
					->get();
		
			return $banners;			
	}

 
 	function getProductGallery($id)
 	{
 		$rs = DB::table('product_gallery')
				->select('id','product_id', 'image_name')
				->where('product_id', $id)
				->get();
		return $rs;
 	}


 	function getWeightClasses()
 	{
 		$lang_id = getAdminCurrentLang();
 		$data = DB::table('weight_classes_infos')
 				->join('weight_classes','weight_classes_infos.id', '=', 'weight_classes.id')
 				->select('weight_classes.id', 'weight_classes_infos.title', 'weight_classes_infos.unit')
 				->where('weight_classes_infos.lang_id', $lang_id)
 				->groupBy('weight_classes.id','weight_classes_infos.title', 'weight_classes_infos.unit')
 				->orderBy('weight_classes.id')
 				->get();

 		$weight_classes = array();


 		foreach ($data as $key => $value) {
 			$weight_classes[$value->id] = array('unit' => $value->unit, 'title' => $value->title );
 		}

 		return $weight_classes;
 	}

 	 function getCategoryClasses()
 	{
 		$lang_id = getAdminCurrentLang();
 		$data = DB::table('categories_infos')
 				->join('categories','categories_infos.category_id', '=', 'categories.id')
 				->select('categories.id', 'categories.url_key','categories_infos.category_name', 'categories_infos.description')
 				->where('categories_infos.language_id', $lang_id)
 				->groupBy('categories.id','categories_infos.category_name', 'categories_infos.description')
 				->orderBy('categories.id')
 				->get();

 		$category_classes = array();


 		foreach ($data as $key => $value) {
 			$category_classes[$value->id] = array('url_key' => $value->url_key, 'category_name' => $value->category_name, 'description' => $value->description );
 		}

 		return $category_classes;
 	}

 	function get_product_categories()
 	{
		$language_id = getCurrentLang();

		$category_query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';

 		$data = DB::table('categories')
 				->join('categories_infos','categories.id', '=', 'categories_infos.category_id')
 				->select('categories.url_key','categories_infos.category_name','categories.id')
 				->where('category_type', 1)
 				->where('category_status',1)
 				->whereRaw($category_query)
 				->groupBy('categories.id','categories_infos.category_name')
 				->get();


 		return $data;		
 	}


 	function getSettings($property)
 	{
 		$settings = App\Model\settings::find(1);

 		return $settings->{$property};

 	}

 	function getGoogleMapKey()
 	{

 		$key = DB::table('socialmediasettings')
 				->select('gmap_api_key')
 				->first();

 		return $key->gmap_api_key;
 	}

 // 	function breadcrumbs($separator = ' » ', $home = 'Home') {

	// 	$path = $path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));

	// 	$base = substr($_SERVER['SERVER_PROTOCOL'], 0, strpos($_SERVER['SERVER_PROTOCOL'], '/')) . '://' . $_SERVER['HTTP_HOST'] . '/';

	// 	$breadcrumbs = array("<a href=\"$base\">$home</a>");

	// 	$tmp = array_keys($path);

	// 	$last = end ($tmp);

	// 	foreach ($path AS $x => $crumb)
	// 	{

	// 	// If we are not on the last index, then display an <a> tag
	// 	if ($x != $last)
	// 	$breadcrumbs[] = "<a href=\"$base$crumb\">$crumb</a>";
	// 	else
	// 	$breadcrumbs[] = $crumb;
	// 	}


	// 	echo  implode($separator, $breadcrumbs);
	// }

	function getVendorById($vid){

		$query = '"vendors_infos"."lang_id" = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = '.getCurrentLang().' and vendors.id = vendors_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';

	 
		$data=DB::table('vendors')
		->leftJoin('vendors_infos','vendors_infos.id','=','vendors.id')
		->select('vendors.id','vendors_infos.vendor_name','vendors.logo_image')
		->whereRaw($query)
		->where('active_status',  1)
		->where('vendors.id','=', $vid)
		->first();

		return $data;

	}

	function getOutletById($vid,$oid)
	{
		$query = '"outlet_infos"."language_id" = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.getCurrentLang().' and outlets.id = outlet_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';

		$data = DB::table('outlets')
					->select('outlets.id','outlets.vendor_id','outlet_infos.outlet_name')
					->join('outlet_infos','outlet_infos.id','=','outlets.id')
					->whereRaw($query)
					->where('vendor_id',$vid)
					->where('outlets.id',$oid)
					->where('active_status', 1)
					->first();

		return $data;
	}


    function has_permission($task_index, $check_owner = true)
    {


        if(Auth::id()==1 && $check_owner)
        {
            return true;
        }
        else 
        {
            $access = getroles_users();
            if(in_array($task_index,$access))
            {
                return true;
            }else{
                return false;
            }
        }

       //return view('errors.404');
    }
    
    /*
     * To check the module access 
     */
    function getroles_users()
    {
        $roletasks  = array();
        $user_id    = Auth::id();
        $role_users = DB::table('roles_users')
                        ->select('role_id')
                        ->where('user_id', "=",$user_id)
                        ->get();
/*
print_r($user_id);
print_r($role_users);
exit;
*/

        if(count($role_users))
        {
            foreach($role_users as $user)
            {
                $rolet = getpermission_menu($user->role_id); // passing variable here


                if(count($rolet))
                {
                    foreach($rolet as $tasks)
                    {
                        $roletasks[] = $tasks;
                    }
                }
            }
        }
        //$diff = array_intersect(array_unique($roletasks),$menulist);
        $_assTasks = $roletasks;
        return $_assTasks;
    }

    /*
     * To check the module access 
     */
    function getpermission_menu($role_id)
    {

//  print_r($role_id);


        $check = DB::table('permission_menu')
                    ->select('permission_menu.controller_method')
                    ->leftJoin('roles_permission','roles_permission.menu_id','=','permission_menu.id')
                    ->leftJoin('roles_users','roles_users.role_id','=','roles_permission.role_id')
                    ->where('roles_permission.role_id','=',$role_id)
                    ->get()->all();  



 
               
        $tasks = array();
        foreach($check as $result)
        {
            if($result->controller_method!="")
            {
                // $array = json_decode($result->controller_method,true);
                // if(is_array($array))
                // {
                //     foreach($array as $ar)
                //     {
                //         $tasks[] = $ar;
                //     }
                // }
                // else {
                    array_push($tasks, $result->controller_method);
                //}
            }
        }

        //$this->_tasksIndex[$role_id] = $tasks;
        return isset($tasks)? $tasks: array();
    }    

    function getRoomCleanStatus(){
        $tasks = array(
           '0' => 'Need To Clean/Dirty',
           '1' => 'Cleaning Inprogress',
           '2' => 'Cleaning Completed'
        );
        return $tasks;
    } 

    function has_staff_permission($task_index, $check_owner = true)
    {

     // print_r($task_index);
     // exit;

    $vendor_id = Session::get('vendor_id');

    $vendor_type = Session::get('vendor_type');

      // print_r($vendor_type);exit;


           if($vendor_type ==1 && $check_owner)
           {
             // print_r('exskaopsi9pression');exit;
               return true;
           }
           else 
           {
                   //   print_r('kju');exit;
               $access = getroles_staff();
                //print_r($access);exit;
               if(in_array($task_index,$access))
               {
                 // dd('10000');exit;
                 // print_r($task_index);
                 // print_r($access);exit;
                   return true;
               }else{
                // dd('111');exit;
                   return false;
               }
           }

          //return view('errors.404');
       }
       
       /*
        * To check the module access 
        */
       function getroles_staff()
       {

           $mid = Session::get('vendor_id');
           // print_r("Ak");
            // print_r($mid);exit;
           //  exit;        

           $roletasks  = array();
           $user_id    = $mid;
           $role_users = DB::table('roles_users')
                           ->select('role_id')
                           ->where('user_id', "=",$user_id)
                           ->get();

     // print_r($user_id);exit;
                           //echo "<pre>";
     // print_r($role_users);exit;
        // print_r($role_users[0]->role_id);exit;
    //  exit;
           if(count($role_users))
           {
            // print_r(count($role_users));exit;
               foreach($role_users as $user)
               {
                   //  echo "inside";
                   //  exit;
                   $rolet = getstaffpermission_menu($user->role_id); // passing variable here
                    // echo "<pre>";
                     //print_r($rolet);
                    // exit;
    // echo "outside";
    //                 exit;
                         // print_r($role_users);exit;
                   if(count($rolet))
                   {
                       foreach($rolet as $tasks)
                       {
                           $roletasks[] = $tasks;
                       }
                   }
               }
           }
           //$diff = array_intersect(array_unique($roletasks),$menulist);
           $_assTasks = $roletasks;
           return $_assTasks;
       }

       /*
        * To check the module access 
        */
       function getstaffpermission_menu($role_id)
       {

     // print_r($role_id);exit;


           $check = DB::table('permission_menu')
                       ->select('permission_menu.controller_method')
                       ->leftJoin('roles_permission','roles_permission.menu_id','=','permission_menu.id')
                       ->leftJoin('roles_users','roles_users.role_id','=','roles_permission.role_id')
                       ->where('roles_permission.role_id','=',$role_id)
                       ->get()->all();  
           $tasks = array();
           foreach($check as $result)
           {
               if($result->controller_method!="")
               {
                   // $array = json_decode($result->controller_method,true);
                   // if(is_array($array))
                   // {
                   //     foreach($array as $ar)
                   //     {
                   //         $tasks[] = $ar;
                   //     }
                   // }
                   // else {
                       array_push($tasks, $result->controller_method);
                   //}
               }
           }
           //$this->_tasksIndex[$role_id] = $tasks;
           return isset($tasks)? $tasks: array();
       }        

       function getSubscription_package($id){
             $subscription_packages = DB::table('subscription_packages')->where('plan_id','=',$id)
                                      ->where('status','=',1)
                                      ->select('package_id')->get()->toArray();
            $package = array();
            if(count($subscription_packages)>0){
                $package = $subscription_packages;
            }                                      

            return $package;
       }

       function getCurrentPlanStatus($id){
            $package =  DB::table('subscriptions')->where('vendor_id','=',$id)
                              ->select('*')->get()->toArray();
            $currentplan = array();
            if(count($package)>0){
                $currentplan = $package;
            }                                      
            return $currentplan;                              
       }

       function getRandomNumber($length)
       {
            $pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randon_number = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
            return $randon_number;
       }

       function getRandomBookingNumber($length)
       {
            $pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randon_number = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
            return $randon_number;
       }

       function getSubscriptionPlan($id){
            $plan =  DB::table('subscription_plan')->where('id','=',$id)
                              ->select('*')->get()->toArray();
            $currentplan = array();
            if(count($plan)>0){
                $currentplan = $plan;
            }                                      
            return $currentplan;
       }

       function getOTPNumber($length)
       {
            $pool = '0123456789';
            $randon_number = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
            return $randon_number;
       }

        function send_sms($mobileNumber="",$message="")
        {
            
            $app_config = getSMSConfig();

            //print_r($app_config); exit;
            $number  = $mobileNumber;
            $twilo_sid    = $app_config[0]->sms_account_id;
            $twilio_token = $app_config[0]->sms_account_token;
            $from_number  = $app_config[0]->sms_sender_number;

            //  return array($number);

            
            // Create an authenticated client for the Twilio API
            try {
            /*    $basic  = new \Nexmo\Client\Credentials\Basic($twilo_sid, $twilio_token);
                $client = new \Nexmo\Client($basic);
                $message = $client->message()->send([
                    //'to'   => $number,
                    'to'   => '+919597141607',
                    'from' => $from_number,
                    'text' => $message
                ]);
                */

                    $result = Nexmo::message()->send([
                        'to'   => $number,
                        'from' => $from_number,
                        'text' => $message
                    ]);

                    //  print_r(array($result));exit;
                    return 2;
            }
            catch (Exception $e) {
                return $e->getMessage();
                $result = array("response" => array("httpCode" => 400,"Message" => $e->getMessage()));
                return json_encode($result);
            }
        }

        function getCustomerName($customer_id)
        {
            $customer =  DB::table('admin_customers')->where('id','=',$customer_id)
                              ->select('firstname','lastname')->first();
            $name = "-";
            if(count($customer)>0){
                if($customer->firstname != "" && $customer->lastname != "")
                     $name = $customer->firstname.' '.$customer->lastname;
                elseif($customer->firstname != "")
                    $name =  $customer->firstname;
            }                                                                 
            return $name;
        }

        function getReferrerCode($customer_id)
        {
            $customer =  DB::table('admin_customers')->where('id','=',$customer_id)
                              ->select('customer_unique_id')->first();
            $code = (count($customer) > 0) ? $customer->customer_unique_id : "-";                
            return $code;
        }

        function getProperty($type,$vendor_id){
            if($type == 1){
                $property = DB::table('outlets')->where('vendor_id',$vendor_id)->count();
            } else if($type == 2){
                $plan_details = DB::table('subscriptions')->where('vendor_id',$vendor_id)
                            ->where('plan_status',1)
                            ->select('plan_id')->orderby('id','desc')->first();
                if(count($plan_details)>0) 
                {
                    $plan_id = $plan_details->plan_id;
                    $property_details = DB::table('subscription_plan')->where('id',$plan_id)->select('total_properties')
                                ->first();
                    $property = $property_details->total_properties;                                
                }
            }

            return $property;
        }

        function getRoom($type,$id){
            if($type == 1){
                $room = DB::table('rooms')->where('property_id',$id)->count();
            } else if($type == 2){
                $plan_details = DB::table('subscriptions')->where('vendor_id',$id)
                            ->where('plan_status',1)
                            ->select('plan_id')->orderby('id','desc')->first();
                if(count($plan_details)>0) 
                {
                    $plan_id = $plan_details->plan_id;
                    $property_details = DB::table('subscription_plan')->where('id',$plan_id)->select('total_rooms')
                                ->first();
                    $room = $property_details->total_rooms;                                
                }
            }

            return $room;
        }

        function getOutletUserLimit($type,$id){
            if($type == 1){
                $users = DB::table('outlet_managers')->where('outlet_id',$id)->where('staff_type',1)->count();
            } else if($type == 2){
                $users = DB::table('outlet_managers')->where('outlet_id',$id)->where('staff_type',2)->count();
            }else if($type == 3){
                $plan_details = DB::table('subscriptions')->where('vendor_id',$id)
                            ->where('plan_status',1)
                            ->select('plan_id')->orderby('id','desc')->first();
                if(count($plan_details)>0) 
                {
                    $plan_id = $plan_details->plan_id;
                    $property_details = DB::table('subscription_plan')->where('id',$plan_id)->select('total_users')
                                ->first();
                    $users = $property_details->total_users;                                
                }
            }

            return $users;
        }

        function getHousekeepingStaffLimit($type,$id){
            if($type == 1){
                $staffs = DB::table('housekeeping_staffs')->where('outlet_id',$id)->count();
            } else if($type == 2){
                 $plan_details = DB::table('subscriptions')->where('vendor_id',$id)
                            ->where('plan_status',1)
                            ->select('plan_id')->orderby('id','desc')->first();
                if(count($plan_details)>0) 
                {
                    $plan_id = $plan_details->plan_id;
                    $property_details = DB::table('subscription_plan')->where('id',$plan_id)->select('total_users')
                                ->first();
                    $staffs = $property_details->total_users;                                
                }
            }

            return $staffs;
        }