<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;
use App\Model\subscription_transaction as Subscriptiontransaction;
use App\Model\subscriptions as Subscriptionrenewal;
use App\Model\subscription_payment;

class SubscriptionVendor extends Controller
{
    
     public function __construct(){

     }

     public function vendorIndex(){
      if (!Session::get('vendor_id')) {
                return redirect()->guest('vendors/login');
        } else {
            $subscription_plan = DB::table('subscription_plan')
                ->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
                ->where('subscription_plan.id','!=',1)
                ->where('subscription_plan.subscription_plan_status',1)
                ->select('subscription_plan.id','subscription_plan.subscription_plan_price','subscription_plan.subscription_plan_duration','subscription_plan.description','subscription_plan.subscription_discount_price','subscription_plan_infos.subscription_plan_name')
                ->orderby('subscription_plan_infos.subscription_plan_id','asc')
                ->get()->toArray();

          //Total Packages
          $packages = DB::table('package_modules')->get();
          $packages = iterator_to_array($packages);    

          return view('vendors.subscription_plans.list')->with('data',$subscription_plan)->with('allpackages',$packages);
        }
     }
  //Processing the payments for subscription process for vendors
  public function subscriptionModal(){
    if (!Session::get('vendor_id')) {
      return redirect()->guest('vendors/login');
    } else {
      $plan_id = $_REQUEST['plan_id'];
      
        $data = DB::table('subscription_plan')
          ->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
          ->where('subscription_plan.id','=',$plan_id)
          ->select('subscription_plan.id','subscription_plan.subscription_plan_price','subscription_plan.subscription_plan_duration','subscription_plan.description','subscription_plan.subscription_discount_price','subscription_plan_infos.subscription_plan_name')
          ->orderby('subscription_plan_infos.subscription_plan_id','asc')
          ->get()
          ->toArray();
          
        $packages = DB::table('package_modules')->get();
        $packages = iterator_to_array($packages);               
      
    //  $payment_gateway_list = Subscriptionrenewal::get_payment_gateways(1);
       return view('vendors.subscription_plans.plan_modal_list')->with('data',$data)->with('allpackages',$packages);
    }
  }

  //  CREATE SHA-HASH SIGNATURE
  
    public function iPay88_signature($source)
    {
      return hash('sha256', $source);
    }

     public function subscriptionPayments(Request $request,$id){
        if (!Session::get('vendor_id')) {
            return redirect()->guest('vendors/login');
        } else {
      //  dd($request->all());
      $payment_type = $request->payment_type;
      
      //  Online payment gateway - iPay88
      if($payment_type == 28) {
        $plan_id = $id;
        $vendor_id = Session::get('vendor_id');

        //  dd($vendor_id);
        
        $payment_gateway_ipay88 = Subscriptionrenewal::get_payment_gateways(1,28);

        //  dd($payment_gateway_ipay88);
        
        
        $data = DB::table('subscription_plan')
          ->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
          ->where('subscription_plan.id','=',$plan_id)
          ->select('subscription_plan.id','subscription_plan.subscription_plan_price','subscription_plan.subscription_plan_duration','subscription_plan.description','subscription_plan.subscription_discount_price','subscription_plan_infos.subscription_plan_name')
          ->orderby('subscription_plan_infos.subscription_plan_id','asc')
          ->get()
          ->toArray();
        //print_r($data);
        $payment_gateway_ipay88 = Subscriptionrenewal::get_payment_gateways(1,28);
        //print_r($payment_gateway_ipay88);
        $payment_amount_price = $data[0]->subscription_plan_price;
        $payment_amount = $data[0]->subscription_discount_price;
        $payment_title = $data[0]->subscription_plan_name;
        $merchant_key = $payment_gateway_ipay88[0]->merchant_key;
        $merchant_code = $payment_gateway_ipay88[0]->account_id;
        $reference_no = getRandomNumber(12);
        
        //Store the session data for future usage
          Session::put('reference_no', $reference_no);
  
        $amount = str_replace('.', '', $payment_amount);
        $currency = $payment_gateway_ipay88[0]->currency_code;
        $sign_src = $merchant_key.$merchant_code.$reference_no.$amount.$currency;
        //echo $sign_src.'<br>';
        $signature = $this->iPay88_signature($sign_src);
        //echo $signature;
        //Total Packages
        $packages = DB::table('package_modules')->get();
        $packages = iterator_to_array($packages);

        $vendor_id = Session::get('vendor_id');
        $vendor_name =  Session::get('user_name');
        $vendor_email =  Session::get('vendor_email');
        $vendor_mobile =  '919626526726';//Session::get('mobile_number');   
        
        $merchant_key = $payment_gateway_ipay88[0]->account_id;
        
        $subscription_payment = new subscription_payment();
        $subscription_payment->vendor_id = $vendor_id;
        $subscription_payment->payment_status = 0;
        $subscription_payment->country_code = '+60';
        $subscription_payment->currency_code = 'MYR';
        $subscription_payment->payment_type = Input::get('payment_type');
        $subscription_payment->plan_id = $plan_id;
        $subscription_payment->reference_no = $reference_no;
        $subscription_payment->plan_price =  $request->plan_price;
        $subscription_payment->plan_discount_price =  $request->plan_discount;
        $subscription_payment->plan_duration =  $request->plan_duration;
        $subscription_payment->merchant_key = $merchant_key;
        $subscription_payment->save();
        
        
        if($payment_gateway_ipay88[0]->payment_mode==1){
          $action_url = 'https://www.mobile88.com/ePayment/entry.asp';
        } else {
          $action_url = 'https://www.mobile88.com/ePayment/entry.asp';
        }
        $response_url = env("APP_URL")."/api/subscription_payment_response";
        $backend_url = env('APP_URL').'/api/payment_response_subscription';
        //  echo $action_url; exit;
        $html_pay = "<html xmlns='http://www.w3.org/1999/xhtml'>\n
        <head></head>\n
        <body>\n";
        $html_pay .=  "<form action='$action_url' method='post' name='frm'>\n";
        $html_pay .=   '<input type="hidden" name="MerchantCode" value="'.$merchant_code.'">';
        $html_pay .=   '<input type="hidden" name="RefNo" value="'.$reference_no.'">';
        $html_pay .=   '<input type="hidden" name="Amount" value="'.$amount.'">';
        $html_pay .=   '<input type="hidden" name="Currency" value="'.$currency.'">';
        $html_pay .=   '<input type="hidden" name="Lang" value="UTF-8">';
        $html_pay .=   '<input type="hidden" name="SignatureType" value="SHA256">';
        $html_pay .=   '<input type="hidden" name="Signature" value="'.$signature.'">';
        $html_pay .=   '<input type="hidden" name="ProdDesc" value="'.$payment_title.'">';
        $html_pay .=   '<input type="hidden" name="UserName" value="'.$vendor_name.'">';
        $html_pay .=   '<input type="hidden" name="UserEmail" value="'.$vendor_email.'">';
        $html_pay .=   '<input type="hidden" name="UserContact" value="'.$vendor_mobile.'">';
        $html_pay .=   '<input type="hidden" name="Remark" value="'.$vendor_id.'">';
        $html_pay .=   '<input type="hidden" name="ResponseURL" value="'.$response_url.'">';
        $html_pay .=   '<input type="hidden" name="BackendURL" value="'.$backend_url.'">';
        $html_pay .=   "</form>\n";
        $html_pay .=   "\t<script type='text/javascript'>\n";
        $html_pay .=   "\t\tdocument.frm.submit();\n";
        $html_pay .=   "\t</script>\n";
        $html_pay .= "</body>\n</html>";  
        echo $html_pay;exit;
        
      } else if($payment_type == 29) { // Cash paymemnt
      
        $payment_status = 1;
        $plan_id = $id;
        $vendor_id = Session::get('vendor_id');

        $subscription_payments = new Subscriptiontransaction();
        $subscription_payments->vendor_id = $vendor_id;
        $subscription_payments->transaction_id = getRandomNumber(8);
        $subscription_payments->payment_status = $payment_status;
        $subscription_payments->country_code = '+60';
        $subscription_payments->currency_code = 'MYR';
        $subscription_payments->payment_type = Input::get('payment_type');
        $subscription_payments->plan_id = $plan_id;
        $subscription_payments->auth_id = getRandomNumber(8);
        $subscription_payments->reference_no = getRandomNumber(8);
        $subscription_payments->plan_price =  $request->plan_price;
        $subscription_payments->plan_discount_price =  $request->plan_discount;
        $subscription_payments->plan_duration =  $request->plan_duration;
        $subscription_payments->save();
        /*
        *If the transaction success Insert the current plan details in subscriptions table
        */
        if($payment_status == 1){
          $duration = $request->plan_duration;
          $current_date = date('Y-m-d h:i:s');
          $start_date = date('Y-m-d');
          $end_date = date('Y-m-d', strtotime($start_date. ' + '.$duration.' days'));
        
          $subscription_renewal = Subscriptionrenewal::where('vendor_id','=',$vendor_id)->get()->toArray();
          $last_date = $subscription_renewal[0]['subscription_end_date'];
          /*
          *check if the vendor renewing date is today or old date.if it is today or old date then save it as normally.
          */
          if($last_date <= $current_date){
            $subscription_renewal = Subscriptionrenewal::where('vendor_id','=',$vendor_id)
                       ->first();
            $subscription_renewal->plan_id = $plan_id;
            $subscription_renewal->last_updated_date = $current_date;
            $subscription_renewal->subscription_start_date = $start_date;
            $subscription_renewal->subscription_end_date = $end_date;
            $subscription_renewal->plan_status = 1;
            $subscription_renewal->renewal_status = 0;
            $subscription_renewal->save(); 
          }else{
            /*
            *if already selected plan's end date is future date then add the currently selected plan's total days to the previously selected plan's end date.
            */
            $plan_end_date = date('Y-m-d', strtotime($last_date. ' + '.$duration.' days'));
            $subscription_renewal = Subscriptionrenewal::where('vendor_id','=',$vendor_id)
                        ->first();
            $subscription_renewal->plan_id = $plan_id;
            $subscription_renewal->last_updated_date = $current_date;
            $subscription_renewal->subscription_start_date = $start_date;
            $subscription_renewal->subscription_end_date = $plan_end_date;
            $subscription_renewal->plan_status = 1;
            $subscription_renewal->renewal_status = 0;
            $subscription_renewal->save(); 
          }
        }
        return Redirect::to('vendors/subscription_packages');
      }
          
        }
     }

    /*
    *List of subscription transaction 
    */
    public function vendorSubscriptionTransaction(){
        if (!Session::get('vendor_id')) {
          return redirect()->guest('vendors/login');
        } else {
          return view('vendors.subscription_plans.subscription_transaction.list');
        }
    }

    /*
    *List of subscription transaction ajax
    */
    public function anyajaxVendorTransactions(){
         $vendor_id = Session::get('vendor_id');

         $transactions = DB::table('subscription_transaction')->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_transaction.plan_id')->leftjoin('vendors_infos','vendors_infos.id','=','subscription_transaction.vendor_id')
         ->where('subscription_transaction.vendor_id','=',$vendor_id)
        ->select('subscription_transaction.*','subscription_plan_infos.subscription_plan_name as plan_name','vendors_infos.vendor_name as vendor_name')
        ->orderBy('subscription_transaction.id','desc')
        ->get(); 

        return Datatables::of($transactions)->addColumn('action', function ($transactions) {
                return '<div class="btn-group"> <a href="'.URL::to("vendors/subscription_transactions/view/".$transactions->id).'"  title="'.trans("messages.View").'"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;'.@trans("messages.View").'</a>
                    </div>';
            })
            ->editColumn('payment_status', function ($transactions) {
                    $data = '-';
                      if($transactions->payment_status == 0){ 
                        $data = '<span class="label label-danger">'.trans('messages.Incompleted').'</span>';
                       }elseif($transactions->payment_status == 1){ 
                        $data = '<span class="label label-success">'.trans('messages.Completed').'</span>';
                       }
                  return $data;
            })
            ->make(true);
    }

    /*
    *view of subscription transaction 
    */
    public function viewVendorTransaction($id){
        if (!Session::get('vendor_id')) {
                return redirect()->guest('vendors/login');
        } else {
        $transactions = DB::table('subscription_transaction')
                    ->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_transaction.plan_id')
                    ->leftjoin('vendors_infos','vendors_infos.id','=','subscription_transaction.vendor_id')
                    ->where('subscription_transaction.id','=',$id)
                    ->select('subscription_transaction.*','subscription_plan_infos.subscription_plan_name as plan_name','vendors_infos.vendor_name as vendor_name')
                    ->orderBy('subscription_transaction.id','desc')
                    ->get()->toArray(); 
          return view('vendors.subscription_plans.subscription_transaction.show')->with('data',
          $transactions);   
        }       
    }
    
    //  STO
    public function paymentShow($id)
    {
    
        if (!Session::get('vendor_id')) {
                return redirect()->guest('vendors/login');
        } else {
      
        //  dd($id);
        
  
      $vendor_id = Session::get('vendor_id');
      
      //  print_r($ref_no);exit;
      
      $subscription_payments = subscription_payment::where('vendor_id','=',$vendor_id)->orderBy('id','desc')->first();
      
      $ref_no = $subscription_payments->reference_no;
      $plan_price = $subscription_payments->plan_price;
            $plan_discount_price = $subscription_payments->plan_discount_price;
            $plan_id = $subscription_payments->plan_id;
            $plan_duration = $subscription_payments->plan_duration;
            $reason_code = $subscription_payments->reason_code;
      
      //  print_r($plan_id);exit;
      
      if($id == 1)
      {
          //  dd("Payment Success");

        $transactions = DB::table('subscription_transaction')
              ->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_transaction.plan_id')
              ->leftjoin('vendors_infos','vendors_infos.id','=','subscription_transaction.vendor_id')
              ->where('subscription_transaction.reference_no','=',$ref_no)
              ->select('subscription_transaction.*','subscription_plan_infos.subscription_plan_name as plan_name','vendors_infos.vendor_name as vendor_name')
              ->orderBy('subscription_transaction.id','desc')
              ->get()->toArray();

          $current_date = date('Y-m-d h:i:s');
          $start_date = date('Y-m-d');
          $end_date = date('Y-m-d', strtotime($start_date. ' + '.$plan_duration.' days'));
        
          $subscription_renewal = Subscriptionrenewal::where('vendor_id','=',$vendor_id)->get()->toArray();
          $last_date = $subscription_renewal[0]['subscription_end_date'];
          /*
          *check if the vendor renewing date is today or old date.if it is today or old date then save it as normally.
          */
          if($last_date <= $current_date){
            $subscription_renewal = Subscriptionrenewal::where('vendor_id','=',$vendor_id)
                       ->first();
            $subscription_renewal->plan_id = $plan_id;
            $subscription_renewal->last_updated_date = $current_date;
            $subscription_renewal->subscription_start_date = $start_date;
            $subscription_renewal->subscription_end_date = $end_date;
            $subscription_renewal->plan_status = 1;
            $subscription_renewal->renewal_status = 0;
            $subscription_renewal->save(); 
          }else{
            /*
            *if already selected plan's end date is future date then add the currently selected plan's total days to the previously selected plan's end date.
            */
            $plan_end_date = date('Y-m-d', strtotime($last_date. ' + '.$duration.' days'));
            $subscription_renewal = Subscriptionrenewal::where('vendor_id','=',$vendor_id)
                        ->first();
            $subscription_renewal->plan_id = $plan_id;
            $subscription_renewal->last_updated_date = $current_date;
            $subscription_renewal->subscription_start_date = $start_date;
            $subscription_renewal->subscription_end_date = $plan_end_date;
            $subscription_renewal->plan_status = 1;
            $subscription_renewal->renewal_status = 0;
            $subscription_renewal->save();
          }             
              
        return view('vendors.subscription_plans.subscription_transaction.show')->with('data',$transactions);
          
      }
      else
      {
            //  dd("Payment Failed");
        $plan_name = DB::table('subscription_plan_infos')->select('subscription_plan_name')->where('subscription_plan_id','=',$plan_id)->get();

        return view('vendors.subscription_plans.subscription_transaction.payement_show')->with('plan_name',$plan_name)
                                                ->with('plan_duration',$plan_duration)
                                                ->with('plan_discount_price',$plan_discount_price)
                                                ->with('reason_code',$reason_code);
      }
      
      
    }
        
    
  }
}
