<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\email_templates;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;


class Template extends Controller
{
                /**
                 * Create a new controller instance.
                 *
                 * @return void
                 */
                public function __construct()
                {
                    $this->site_name = isset(getAppConfig()->site_name)?ucfirst(getAppConfig()->site_name):'';
                    $this->middleware('auth');
                    SEOMeta::setTitle($this->site_name);
                    SEOMeta::setDescription($this->site_name);
                    SEOMeta::addKeyword($this->site_name);
                    OpenGraph::setTitle($this->site_name);
                    OpenGraph::setDescription($this->site_name);
                    OpenGraph::setUrl($this->site_name);
                    Twitter::setTitle($this->site_name);
                    Twitter::setSite('@'.$this->site_name);
                    App::setLocale('en');
                }

                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function index()
                {
                    if (Auth::guest()){
                        return redirect()->guest('admin/login');
                    }else{
                        if(!has_permission('admin/templates/email')){
                            return view('errors.404');
                        }
                        return view('admin.notification.template.list');
                    }
                }
                /**
                 * Display a listing of the weight classes.
                 *
                 * @return Response
                 */
                public function anyAjaxtemplatelist()
                {
                    if(Auth::guest())
                    {
                        return 404;
                    }                                             
                    $templates = DB::table('email_templates')->select('*')->orderBy('template_id', 'desc');
                    return Datatables::of($templates)->addColumn('action', function ($templates) {
                        if(has_permission('admin/templates/edit/{id}'))
                        {
                            $html ='<div class="btn-group"><a href="'.URL::to("admin/templates/edit/".$templates->template_id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("admin/templates/view/".$templates->template_id).'" title="'.trans("messages.Preview Template").'"><i class="fa fa-search"></i>&nbsp;&nbsp;'.@trans("messages.Preview Template").'</a></li>
                                    <li><a href="'.URL::to("admin/templates/delete/".$templates->template_id).'" class="delete-'.$templates->template_id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                </ul>
                            </div>
                            <script type="text/javascript">
                                $( document ).ready(function() {
                                    $(".delete-'.$templates->template_id.'").on("click", function(){
                                        return confirm("'.trans("messages.Are you sure want to delete?").'");
                                    });
                                });
                            </script>';
                            return $html;
                        }
                    })
                    ->addColumn('is_system', function ($templates) {
                        if($templates->is_system==0):
                            $data = '<span class="label label-danger">'.trans("messages.Custom").'</span>';
                        elseif($templates->is_system==1):
                            $data = '<span class="label label-success">'.trans("messages.System").'</span>';
                        endif;
                        return $data;
                    })
                    ->addColumn('from', function ($templates) {
                        $data = '-';
                        if($templates->from != ''):
                            $data = $templates->from;
                        endif;
                        return $data;
                    })
                    ->make(true);
                }
                /**
                 * Show the form for creating a new blog.
                 *
                 * @return Response
                 */
                public function create()
                { 
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    else{
                        if(!has_permission('admin/templates/create'))
                        {
                            return view('errors.404');
                        }
                        return view('admin.notification.template.create');
                    }
                }
                /**
                 * Store a newly created blog in storage.
                 *
                 * @return Response
                 */
                public function store(Request $data)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                          
                    if(!has_permission('createtemplate'))
                    {
                        return view('errors.404');
                    }
                    $data1=Input::all();

                    $validation = Validator::make($data->all(), array(
                        // 'ref_name' => 'required|unique:email_templates,ref_name',
                        'from_name' => 'nullable|regex:/^[a-zA-Z_]*$/',
                        'ref_name' => 'required|regex:/^[a-zA-Z]+$/u|max:255|unique:email_templates,ref_name',
                        'from_email' => 'required|email|max:255',
                        'reply_to' => 'nullable|email',
                        'subject'=> 'required|min:2|alpha_dash|max:255',
                        'content'=>'required|string',
                    ));
                    // process the validation
                    if ($validation->fails())
                    {
                        return Redirect::back()->withErrors($validation)->withInput();
                    }
                    else {
                        // store
                        $Templates = new Email_templates;
                        $Templates->from      = $_POST['from_name'];
                        $Templates->subject    = $_POST['subject'];
                        $Templates->content    = $_POST['content'];
                        $Templates->from_email    = $_POST['from_email'];
                        $Templates->reply_to    = $_POST['reply_to'];
                        $Templates->ref_name    = $_POST['ref_name'];
                        $Templates->created_date = date("Y-m-d H:i:s");
                        $Templates->updated_date = date("Y-m-d H:i:s");
                        $Templates->is_system    = 1;
                        $Templates->save();
                        // redirect
                        Session::flash('message', trans('messages.Template has been created successfully'));
                        return Redirect::to('admin/templates/email');
                    }
                }
                public function edit($id)
                {
                    if (Auth::guest()){
                        return redirect()->guest('admin/login');
                    }else{
                        if(!has_permission('admin/templates/edit/{id}')){
                            return view('errors.404');
                        }
                        $templates = Email_templates::find($id);
                        return view('admin.notification.template.edit')->with('data', $templates);
                    }
                }
                /**
                 * Store a newly created blog in storage.
                 *
                 * @return Response
                 */
                public function update(Request $data,$id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                        
                    if(!has_permission('admin/template/update/{id}'))
                    {
                        return view('errors.404');
                    }
                    $data1=Input::all();
                    // validate
                    // read more on validation at http://laravel.com/docs/validation
                    $validation = Validator::make($data->all(), array(
                        'from_name' => 'nullable|regex:/^[a-zA-Z_]*$/',
                        'ref_name' => 'required|unique:email_templates,ref_name,'.$id.',template_id',
                        'from_email' => 'required|email|max:255',
                        'reply_to' => 'nullable|email',
                        'subject'=> 'required|min:2|alpha_dash|max:255',
                        'content'=>'required|string',
                    ));
                    // process the validation
                    if ($validation->fails())
                    {
                        //return redirect('create')->withInput($data1)->withErrors($validation);
                        return Redirect::back()->withErrors($validation)->withInput();
                    }
                    else {
                        // store
                        $Templates = Email_templates::find($id); 
                        $Templates->from      = $_POST['from_name'];
                        $Templates->subject    = $_POST['subject'];
                        $Templates->content    = $_POST['content'];
                        $Templates->from_email    = $_POST['from_email'];
                        $Templates->reply_to    = $_POST['reply_to'];
                        $Templates->ref_name    = $_POST['ref_name'];
                        $Templates->updated_date = date("Y-m-d H:i:s");
                        $Templates->is_system    = 1;
                        $Templates->save();
                        // redirect
                        Session::flash('message', trans('messages.Template has been updated successfully'));
                        return Redirect::to('admin/templates/email');
                    }
                }
                /* To remove the template */
                public function destroy($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                        
                    if(!has_permission('admin/templates/delete/{id}')){
                        return view('errors.404');
                    }
                    $groups = DB::select('select template_id from email_templates where template_id = '.$id);
                    if(count($groups))
                    {
                        DB::table('email_templates')->where('template_id', '=', $id)->delete();
                        Session::flash('message', trans('messages.Template has been deleted successfully!'));
                        return Redirect::to('admin/templates/email');
                    }
                    else {
                        Session::flash('message', trans('messages.No template found'));
                        return Redirect::to('admin/templates/email');
                    }
                }
                
                public function view($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                        
                    if(!has_permission('admin/templates/view/{id}')){
                        return view('errors.404');
                    }
                    else{
                        $templates = Email_templates::find($id);
                        return view('admin.notification.template.view')->with('data', $templates);
                    }
                }
}
