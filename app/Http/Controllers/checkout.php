<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use GuzzleHttp\Client;
use DB;
use App\Model\contactus;
use App\Model\users;
use App\Model\settings;
use App\Model\emailsettings;
use App\Model\cms;
use App\Model\products;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Registrar;
use MetaTag;
use Mail;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Cart;
//use App\Http\Controllers\Api\Api;
use App\Model\api;
use Paypal;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Details;
use Stripe;
use App\Model\admin_customers as Admincustomer;
use App\Model\vendors;
use App\Model\subscriptions as Subscriptionrenewal;
use App\Model\subscription_payment;
use App\Model\rooms;
use App\Model\outlets;
use App\Model\user_payment;

class Checkout extends Controller
{
    const USERS_SIGNUP_EMAIL_TEMPLATE = 1;
    const USERS_WELCOME_EMAIL_TEMPLATE = 3;
    const COMMON_MAIL_TEMPLATE = 8;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $_apiContext;
    public function __construct()
    {
        $this->user_details = array();
        $this->api = New Api;
        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => url('/'),
            // You can set any number of default request options.
            'timeout'  => 3000.0,
        ]);
        /*
        $user_details = $this->check_login();
        $this->_apiContext = PayPal::ApiContext(getAppPaymentConfig()->merchant_key,getAppPaymentConfig()->merchant_secret_key);
        $this->_apiContext->setConfig(array(
           'mode' => 'sandbox',
          'service.EndPoint' => 'https://api.sandbox.paypal.com',
           'http.ConnectionTimeOut' => 30,
           'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));
        */
        $this->theme = Session::get("general")->theme;
    }
    
    public function check_login()
    {

        
        $user_id = Session::get('user_id');
        $token = Session::get('token');
        //Session::put('token', $response->response->token);
        if(empty($user_id))
        {
            return Redirect::to('/')->send();
        }
        $user_array = array("user_id" => $user_id,"token"=>$token);
        $method = "POST";
        $data = array('form_params' => $user_array);
        $response = $this->api->call_api($data,'api/user_detail',$method);
        if($response->response->httpCode == 400)
        {
            return Redirect::to('/')->send();
        }
        else
        {
            $this->user_details = $response->response->user_data[0];
            if($this->user_details->email == "")
            {
                Session::flash('message-failure',trans("messages.Please fill your personal details"));
                return Redirect::to('/profile')->send();
            }
            return $this->user_details;
        }
    }

    
    function GetDrivingDistance($lat1, $long1, $lat2, $long2)
    {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
        $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
        $dist = str_replace(',', '.', $dist);
        $distance = array('distance' => $dist, 'distance_km' => ($dist*1.609344), 'time' => $time);
        return $distance;
    }
    
    public function index()
    {
		//$stripe = new Stripe();
		//$stripe = Stripe::make();

		//echo $charge['id']; exit;

        //  dd("ak");
        $user_id  = Session::get('user_id');
        $token    = Session::get('token');
        $language = getCurrentLang();
        $cart     = Session::get('cart');//print_r($cart);exit;
        $vendor_id     = $cart['vendors_id'];
        $outlet_id =$cart[$vendor_id]['outlet_id'];
        $total_service_time=$this->getTotal_service_time($cart,$vendor_id,$outlet_id);
        $vendor_details= DB::table('vendors')->select('*')->where('id',$vendor_id)->get();
		 
        $cart_count = count(Session::get('cart'));
        if($cart_count == 0)
        {
            Session::flash('message-failure', trans('messages.No cart items found'));
            return Redirect::to('/')->send();
        }
        $checkout_details = $this->checkout_detail($language, $user_id, $cart);
        //print_r($checkout_details);exit;
    
        $address_types[""] = trans("messages.Select address type");
        foreach ($checkout_details['response']['address_type'] as $row)
        {
            $address_types[$row->id] = ucfirst($row->name);
        }
		$timearray = getDaysWeekArray();
		$dayname_numeric = idate('w', strtotime(date('Y-m-d')));
		$u_time = $this->getOpenTimings($checkout_details['response']['outlet_detail']->id,$dayname_numeric);
		$open_time = 0;
		$close_time = 0;
		if(count($u_time) > 0)
		{
			$open_time = strtotime($u_time[0]->start_time);
			$close_time = strtotime($u_time[0]->end_time);
		}

       // print_r($open_time);
       // echo "HI";
       // print_r($close_time);exit;
        SEOMeta::setTitle(Session::get('general')->site_name);
        SEOMeta::setDescription(Session::get('general')->site_name);
        SEOMeta::addKeyword(Session::get('general')->site_name);
        OpenGraph::setTitle(Session::get('general')->site_name);
        OpenGraph::setDescription(Session::get('general')->site_name);
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle(Session::get('general')->site_name);
        Twitter::setSite(Session::get('general')->site_name);
        return view('front.'.$this->theme.'.checkout')->with("user_details",$this->user_details)->with("checkout_details",$checkout_details['response'])->with("address_types",$address_types)->with('open_time',$open_time)->with('close_time',$close_time)->with('vendor_details',$vendor_details[0])->with('total_service_time',$total_service_time);
    }
	
    public function getTotal_service_time($cart,$vendor_id,$outlet_id)
    {
            $all_service_time=0;
         foreach($cart[$vendor_id][$outlet_id] as $key => $items)
                {
                    $old_product_id = $items['product_id'];
                    $all_service_time = $all_service_time + ((integer)$items[$old_product_id]['service_time']*(integer)$items[$old_product_id]['quantity']);
                }


                // $all_service_time=date('H:i:s', strtotime($all_service_time));
                //$all_service_time = date('H:i:s',$all_service_time);

                  return $all_service_time;


    }

	public function customer_orders_count($user_id)
    {
		$orders_count = DB::select('SELECT count(id) as order_count from orders where customer_id = ? ',array($user_id));
		return $orders_count[0]->order_count;
	}
    public function checkout_detail($language, $user_id, $cart_data)
    {
        $cart_items   = $this->calculate_cart($cart_data);
        $address_list = $this->get_address($language,$user_id);
        $gateway_list = $this->get_payment_gateways($language);
        $delivery_settings  = getDeliverySettings($cart_items['outlet_id']);
        $address_type = $this->address_type();
        $vendor_id = $cart_data['vendors_id'];
        $outlet_id = $cart_data[$vendor_id]['outlet_id'];
		$outlet_professionals = $this->get_outlet_professionals($outlet_id);
		$customer_orders_count = $this->customer_orders_count($user_id);
        $outlet_detail = $this->get_outlet_detail($outlet_id);
        if(count($outlet_detail) == 0)
        {
            Session::flash('message-failure', trans('messages.No cart items found'));
            return Redirect::to('/')->send();
        }
        // if($outlet_detail->active_status!='1' || $outlet_detail->open_status!=1){


        //     Session::flash('message-failure', trans('messages.salon is closed for the day'));
        //     return Redirect::to('/')->send();
        // }
		
		$date       = date('Y-m-d'); //today date
		$weekOfdays = $week = $deliver_slot_array = $u_time = array();
		$timearray  = getDaysWeekArray();
		$dayname_numeric = idate('w', strtotime(date('Y-m-d')));
		$u_time = $this->getOpenTimings($outlet_id,$dayname_numeric);
		$open_time = "";
		$close_time = "";
		if(count($u_time)>0)
		{
			$open_time = strtotime($u_time[0]->start_time);
			$close_time = strtotime($u_time[0]->end_time);
		}
		
        $city_list = getCityList($outlet_detail->outlet_country_id);
        $result = array("response" => array("httpCode" => 200, "Message" => "Cart details", "cart_items" => $cart_data, "total" => $cart_items['total'], "sub_total" => $cart_items['sub_total'], "tax" => $cart_items['tax'], "delivery_cost" => $cart_items['delivery_cost'], "tips_percentage" => $cart_items['tips_percentage'],  "address_list" => $address_list, "gateway_list" => $gateway_list, 'outlet_detail' => $outlet_detail, 'address_type' => $address_type, 'city_list' => $city_list, 'delivery_settings' => $delivery_settings, 'outlet_professionals' => $outlet_professionals,'open_time' => $open_time,'close_time' => $close_time,'customer_orders_count' => $customer_orders_count));
        return $result;
    }
	public function getOpenTimings($v_id,$day_week)
	{
		$time_list = DB::table('opening_timings')->select('opening_timings.start_time','opening_timings.end_time')->where("vendor_id",$v_id)->where("day_week",$day_week)->orderBy('id', 'asc')->get();
		return $time_list;
	}
    public function calculate_cart($cart_data)
    {
        $total = $sub_total = $tax = $delivery_cost = $old_vendor_id = $old_outlet_id =$tips_percentage= $all_service_time= 0;
        $vendor_id = $outlet_id = $ingredient_detail = '';
       
        if(count($cart_data) > 0)
        {
            //echo $cart_data[$old_vendor_id]['outlet_id'];exit;
            //print_r($cart_data);exit;
            
            //print_r($delivery_settings);exit;
            $old_vendor_id = $cart_data['vendors_id'];
            $old_outlet_id = $cart_data[$old_vendor_id]['outlet_id'];
            $delivery_settings = getDeliverySettings($old_outlet_id);
            //print_r($delivery_settings);exit;
            $vendor_detail = getStoreVendorLists($old_vendor_id);//
            //print_r($vendor_detail[0]);exit;
            $outlet_products_align = array_values($cart_data[$old_vendor_id][$old_outlet_id]);
            $cart_data[$old_vendor_id][$old_outlet_id] = $outlet_products_align;
            $new_cart = Session::set('cart',$cart_data);
            $new_cart = Session::get('cart');
            if(count($new_cart[$old_vendor_id][$old_outlet_id])> 0)
            {
                foreach($new_cart[$old_vendor_id][$old_outlet_id] as $key => $items)
                {
                    //print_r($items);exit;
                    $old_product_id = $items['product_id'];
                    $ingredient_price = isset($items[$old_product_id]['ingredient_price'])?$items[$old_product_id]['ingredient_price']:0;
                    $old_product_id = $items['product_id'];
                    $sub_total += $items[$old_product_id]['quantity'] * ($items[$old_product_id]['discount_price'] + $ingredient_price);
                    $all_service_time = $all_service_time + (60*(integer)$items[$old_product_id]['service_time']);
                }

                // $all_service_time=date('H:i:s', strtotime($all_service_time));
                  $all_service_time = date('H:i:s',$all_service_time);
            }
            
            //$checkout_details['outlet_detail']->delivery_type
            /* if(isset($delivery_settings->delivery_type) && $delivery_settings->delivery_type == 2)
            {
                $delivery_cost = $delivery_settings->delivery_cost_fixed;
            }
            if(isset($delivery_settings->tax_type) && $delivery_settings->tax_type == 1)
            {
                $tax = round(($delivery_settings->tax_percentage*$sub_total)/100,2);
            } */
            if(isset($vendor_detail[0]->delivery_charges_fixed))
            {
                $delivery_cost = $vendor_detail[0]->delivery_charges_fixed;
            }
            if(isset($vendor_detail[0]->service_tax))
            {
                $tax = round(($vendor_detail[0]->service_tax*$sub_total)/100,2);
            } 
			$user_details = $this->check_login();
            $tips_percentage=0;
			if($user_details->tips_percentage > 0)
			{
				$tips_percentage = round(($user_details->tips_percentage*$sub_total)/100,2);
			}
            $total = $sub_total+$tax+$tips_percentage;
            return array("total" => round($total,2), "sub_total" => round($sub_total,2), "tax" => round($tax,2), "delivery_cost" => round($delivery_cost,2), "vendor_id" => $old_vendor_id, "outlet_id" => $old_outlet_id, "tips_percentage" => round($tips_percentage,2),"all_service_time"=>$all_service_time ) ;
        }
        else
        {
            return array("total" => $total, "sub_total" => $sub_total, "tax" => $tax, "delivery_cost" => $delivery_cost, "vendor_id" => $old_vendor_id, "outlet_id" => $old_outlet_id , "tips_percentage" => $tips_percentage, "all_service_time"=>$all_service_time);
        }
    }
    public function get_address($language_id,$user_id)
    {
        $address = DB::table('user_address')
                    ->select('user_address.address','user_address.latitude','user_address.longtitude','user_address.location_id','user_address.id as address_id','address_type.name as address_type')
                    ->where('user_id','=',$user_id)
                    ->where('user_address.active_status','=','1')
                    ->leftJoin('address_type','address_type.id','=','user_address.address_type')
                    ->orderBy('user_address.id', 'desc')
                    ->get();
        //~ if(count($address) > 0)
        //~ {
            //~ foreach($address as $key=>$val)
            //~ {
                //~ $address[$key]->city_id = ($val->city_id != '')?$val->city_id:'';
                //~ $address[$key]->country_id = ($val->country_id != '')?$val->country_id:'';
                //~ $address[$key]->postal_code = ($val->postal_code != '')?$val->postal_code:'';
            //~ }
        //~ }
        return $address;
    }
    
    public function get_payment_gateways($language_id)
    {
        //echo $language_id;
        $query = '"payment_gateways_info"."language_id" = (case when (select count(payment_gateways_info.language_id) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $gateways = DB::table('payment_gateways')
                ->select('payment_gateways.payment_type','payment_gateways_info.name','payment_gateways.id as payment_gateway_id')
                ->leftJoin('payment_gateways_info','payment_gateways_info.payment_id','=','payment_gateways.id')
                ->orderBy('payment_gateways.id', 'desc')
                 ->where('active_status',"=",1)
                ->whereRaw($query)
                ->get();
        //print_r($gateways);exit;
        return $gateways;
    }
    public function get_delivery_slots()
    {
        $delivery_slots = DB::table('delivery_time_slots')->select('*')->get();
        return $delivery_slots;
    }
    
    public function get_delivery_settings()
    {
        $delivery_settings = DB::table('delivery_settings')->select('delivery_type', 'delivery_cost_fixed', 'delivery_cost_variation', 'delivery_km_fixed', 'minimum_order_amount', 'tax_type', 'label_name', 'tax_percentage')->first();
        return $delivery_settings;
    }
    public function get_delivery_time_interval()
    {
        $time_interval = DB::table('delivery_time_interval')->select('*')
                ->orderBy('start_time', 'asc')->get();
        return $time_interval;
    }
    public function get_avaliable_slot_mobl()
    {
        $available_slots = DB::select('SELECT dts.day,dti.start_time,dti.end_time,dts.id AS slot_id FROM delivery_time_slots dts LEFT JOIN delivery_time_interval dti ON dti.id = dts.time_interval_id');
        return $available_slots;
    }
    public function address_type()
    {
        $address_type = DB::table('address_type')->select('id','name')->where('active_status', 1)->orderBy('name', 'asc')->get();
        return $address_type;
    }
    public function check_value_exist($delivery_slots,$interval_id,$day,$key1,$key2)
    {
        foreach ($delivery_slots as $slots)
        {
            if (is_array($slots) && check_value_exist($delivery_slots, $interval_id,$day,$key1,$key2)) return $slots->id;
            if (isset($slots->$key1) && $slots->$key1 == $interval_id && isset($slots->$key2) &&$slots->$key2 == $day) return $slots->id;
        }
        return 0;
    }
    public function get_outlet_detail($outlet_id)
    {

        $outlet_detail = DB::table('outlets')
                        ->select('outlets.id','outlets.contact_address','outlets.suite_number','outlets.country_id as outlet_country_id','outlets.latitude','outlets.open_status','outlets.active_status','outlets.longitude','outlets.delivery_time','outlets.contact_phone','restaurant_delivery_settings.id as restaurant_delivery_settings_id','restaurant_delivery_settings.delivery_type','restaurant_delivery_settings.delivery_cost_fixed','restaurant_delivery_settings.tax_type','restaurant_delivery_settings.label_name','restaurant_delivery_settings.tax_percentage','restaurant_delivery_settings.merchant_id','vendors.service_at_home','vendors.credit_card_customer_type','vendors.credit_card_required')
                        ->leftJoin('restaurant_delivery_settings','restaurant_delivery_settings.restaurant_id','=','outlets.id')
                        ->leftJoin('vendors','vendors.id','=','outlets.vendor_id')
                        ->where('outlets.id',"=",$outlet_id)
                        ->first();
        $outlet_detail->delivery_areas = array();
        if(count($outlet_detail)>0)
        {
            if($outlet_detail->delivery_type == 3)
            {
                $delivery_areas = DB::select('SELECT * FROM restaurant_delivery_settings_locations WHERE restaurant_delivery_settings_id = ?',array($outlet_detail->restaurant_delivery_settings_id));
                $outlet_detail->delivery_areas = $delivery_areas;
            }
        }
        //print_r($outlet_detail);exit;
        return $outlet_detail;
    }
	
	
    
    public function converCurrency($from,$to,$amount)
    {
        $url      = "http://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
        $request  = curl_init();
        $timeOut  = 0;
        curl_setopt($request,CURLOPT_URL,$url);
        curl_setopt($request,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($request,CURLOPT_USERAGENT,"Mozilla/4.0(compatible;MSIE8.0;Windows NT6.1)");
        curl_setopt($request,CURLOPT_CONNECTTIMEOUT,$timeOut);
        $response = curl_exec($request);
        curl_close($request);
        return $response;
    }
    
    public function iPay88_signature($source)
    {
      return hash('sha256', $source);
    }

    public function checkout(Request $data)
    {
              //    dd($data->all());

        $post_data = $data->all();

                  //    dd($post_data);

        if(isset($post_data['user_id']))
            {
                //  return 1;

                $user_id = $post_data['user_id'];

                $mobile = $post_data['mobile'];

                $country_code = $post_data['country_code'];

                $book_user = Admincustomer::where('id','=',$user_id)->first();
                                           
            }
        else
        {

/*            
                //  IF USER NOT VERIFIED 

                $user = DB::table('admin_customers')
                            ->where('email',$post_data['email'])
                            ->where('mobile_number',$post_data['mobile'])
                            ->where('country_code',$post_data['country_code'])
                            ->where('otp_verify_status',0)
                            ->first();

                //  return array($user);

                if($user)
                {

                        $book_user = Admincustomer::where('id','=',$user->id)->first();

                         // return $book_user;

                        $otp = getOTPNumber(6);

                        //  $mobile = $book_user->country_code.trim($book_user->mobile_number);
                        $mobile = $post_data['country_code'].trim($post_data['mobile']);

                        $msg = DB::table('sms_templates')
                                    ->select('message')
                                    ->where('id','=',self::USER_SIGNUP_VERIFICATION_TEMPLATE)
                                    //  ->where('id','=',self::USER_BOOKING_VERIFICATION_TEMPLATE)
                                    ->first();

                        $result = $msg->message;
                        $default_sitename = getAppConfig()->site_name;
                        $message = str_replace('${otp_number}',$otp,$result);
                        $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                        $result = send_sms($mobile,$message,$result);

                      

                        if($result == 2){

                            $book_user->otp_number = $otp;
                            $book_user->save();

                           $result = array("response" => array("httpCode" => 700, "status" => true, "Message" => trans("messages.Please confirm your otp number for your account activation in ".getAppConfig()->site_name), "user_id" => $user->id));           
                        }  

                                                         

                    return json_encode($result);
                }    

*/                        

            //  IF USER ALREADY THERE MEANS [ SEND SMS TO HIM & LOGGED HIM]

            $user = DB::table('admin_customers')
                        ->where('email',$post_data['email'])
                        ->where('mobile_number',$post_data['mobile'])
                        // ->where('country_code',$post_data['country_code'])
                        ->first();

            if($user)
                {
                    //  IF USER EXISTS

                    $user = Admincustomer::where('id','=',$user->id)->first();

                    $user_id = $user->id;

                    Session::put('user_id',$user->id);
                    Session::put('user_name',$user->firstname);
                    Session::put('user_email',$user->email);
                    Session::put('user_mobile',$user->mobile_number);
                    Session::put('country_code',$user->country_code);
                    Session::put('token', $post_data['_token']);                    

                    //  return $user_id;
                                            
                }

            else {

            $validation = Validator::make($data->all(), array(
                //~ 'social_title'  => 'required',
                'user_name'     => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                'email'         => 'required|email|unique:admin_customers,email',
                'mobile'        => 'required|numeric|unique:admin_customers,mobile_number',

            ));
            // process the validation
            if ($validation->fails())
            {
                return Redirect::back()->withErrors($validation)->withInput();
            }
            else {
                // store datas in to database

                $pwd = getRandomBookingNumber(8);

                $user = new Admincustomer();
                $user->firstname = $post_data['user_name'];
                $user->email = $post_data['email'];
                $user->hash_password = md5($pwd);
                $user->country_code = $post_data['country_code'];
                $user->mobile_number = $post_data['mobile'];
                $user->login_type = 1; //   WEB
                //  $user->otp_number = $otp;
                $user->otp_verify_status = 1;   //  NO NEED OF VERIFICATION
                $user->customer_unique_id = getRandomBookingNumber(8);
                $user->referred_customer_id = 0;
                $user->save();

                Session::put('user_id',$user->id);
                Session::put('user_name',$user->firstname);
                Session::put('user_email',$user->email);
                Session::put('user_mobile',$user->mobile_number);
                Session::put('country_code',$user->country_code);
                Session::put('token', $post_data['_token']);

                $user_id = $user->id;

            }  

            }   //  USER REGISTRATION
        }

        $check_in = $post_data['check_in'];
        $check_out = $post_data['check_out'];
        $room_count = $post_data['room_count'];
        $guest_count = $post_data['guest_count'];
        $no_of_nights = $post_data['no_of_nights'];
        $coupon_id = $post_data['coupon_id'];
        $coupon_amount = $post_data['coupon_amount'];
        $coupon_type = $post_data['coupon_type'];
        $original_price = $post_data['original_price'];
        $total_cost = $post_data['total_cost'];
        $vendor_id = $post_data['vendor_id'];
        $outlet_id = $post_data['outlet_id'];
        $room_type_id = $post_data['room_type_id'];

        //  dd($outlet_id);
        $outlets = DB::table('outlets')
                     ->select('outlets.id','outlets.outlet_image','outlet_infos.outlet_name'
                            ,'outlet_infos.contact_address','outlets.url_index')
                     ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                     ->where('outlets.id',$outlet_id)
                     ->get();
        //  dd($outlets);
        $room_type = DB::table('room_type')
                     ->select('room_type.created_by','room_type.id','room_type_infos.room_type','room_type.discount_price')
                     ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                     ->where('room_type.id','=',$room_type_id)
                     ->get();  
        //  dd($room_type);

        $user = Admincustomer::find($user_id);

        //  return $user;

        $user_id = $user->id;
        $user_name =  $user->firstname;
        $user_email =  $user->email;
        $user_mobile =  $user->country_code."".$user->mobile_number;                     

        $payment_gateway_ipay88 = Subscriptionrenewal::get_payment_gateways(1,28);

        $payment_amount_price = $original_price;
        //  $payment_amount = $total_cost;
        $payment_amount = "1.00";
        //  dd($payment_amount);
        $payment_title = 'Booking';
        $merchant_key = $payment_gateway_ipay88[0]->merchant_key;
        $merchant_code = $payment_gateway_ipay88[0]->account_id;
        $reference_no = getRandomNumber(12);

        //  $amount = str_replace('.', '', $payment_amount);  

        //  $amount = round($payment_amount);

        $currency = $payment_gateway_ipay88[0]->currency_code;

    //  dd($currency);
        $sign_src = $merchant_key.$merchant_code.$reference_no.$payment_amount.$currency;
        //  dd($sign_src);
        $signature = $this->iPay88_signature($sign_src);
            //  dd($signature);

            if($payment_gateway_ipay88[0]->payment_mode==1){
              $action_url = 'https://www.mobile88.com/ePayment/entry.asp';
            } else {
              $action_url = 'https://www.mobile88.com/ePayment/entry.asp';
            }


            $response_url = env("APP_URL")."/api/booking_payment_response";
            $backend_url = env('APP_URL').'/api/response_booking_payment';

            $wallet_payment = new user_payment();
            $wallet_payment->customer_id = $user_id;
            $wallet_payment->payment_status = 0;
            $wallet_payment->country_code = '+60';
            $wallet_payment->currency_code = 'MYR';
            $wallet_payment->payment_type = 1;    /*Input::get('payment_type');*/
            $wallet_payment->reference_no = $reference_no;
            $wallet_payment->merchant_key = $merchant_key;
            $wallet_payment->total_amount = $payment_amount;
            $wallet_payment->save();            
            //  dd($response_url);
        return view('front.'.$this->theme.'.checkout_payment')->with('data',$post_data)
                                ->with('properties',$outlets[0])->with('room_type',$room_type[0])
                                ->with('amount',$payment_amount)->with('merchant_key',$merchant_key)
                                ->with('merchant_code',$merchant_code)->with('reference_no',$reference_no)
                                ->with('currency',$currency)->with('signature',$signature)
                                ->with('payment_title',$payment_title)->with('user_name',$user_name)
                                ->with('user_email',$user_email)->with('user_mobile',$user_mobile)
                                ->with('user_id',$user_id)->with('action_url',$action_url);
                                //->with('response_url',$response_url)->with('backend_url',$backend_url);
    }

    public function storePayment()
    {
        print_r('Ak');exit;
        echo 1; exit;
    }    
    public function proceed_checkout(Request $data)
    {
		$stripe = new Stripe();
		$stripe = Stripe::make();
        $post_data  = $data->all();
		
		
        // echo '<pre>';print_r($post_data); exit;
        $user_id    = Session::get('user_id');
        $vendorDetails=getOutletVendorDetails($post_data['outlet_id']);


        //print_r($vendorDetails);exit;
        $token      = Session::get('token');
        $language   = getCurrentLang();
        $payment_gateway_id = isset($post_data['payment_gateway_id'])?$post_data['payment_gateway_id']:22;
        $cart       = Session::get('cart');
        $cart       = json_encode($cart);
		
        //Get the payment gateway details here
        $user_array = array("user_id" => $user_id, "token" => $token, "language" => $language, "payment_gateway_id" => $payment_gateway_id, "cart" => $cart);
        $method     = "POST";
        $data       = array('form_params' => $user_array);
        $response   = $this->api->call_api($data,'api/get_payment_details',$method);
        //print_r($response);exit;
        $cart_detail = $response->response;
        $cart_detail->delivery_notes = $post_data['delivery_instructions'];
        $delivery_address = (isset($post_data['delivery_address']) && ($post_data['delivery_address'] != ""))?$post_data['delivery_address']:0;
        $cart_detail->delivery_address = $delivery_address;
        $delivery_settings = getDeliverySettings($post_data['outlet_id']);
       // print_r($cart_detail);exit;
         if(isset($vendorDetails->service_tax))
            {
                $tax = round(($vendorDetails->service_tax*$cart_detail->sub_total)/100,2);
                    $cart_detail->tax  = $tax;
            }else{
                $cart_detail->tax  = 0;
            }
    
        /*if($delivery_settings->tax_type == 1)
        {
            $cart_detail->tax = round(($delivery_settings->tax_percentage/100)*$cart_detail->sub_total,2);
        } */
        $cart_detail->delivery_date = "NOW()";
        $cart_detail->delivery_cost = (isset($post_data['delivery_cost']) && $post_data['delivery_cost'] !="" )?$post_data['delivery_cost']:0;
        $cart_detail->order_type = (isset($post_data['order_type']) && $post_data['order_type'] !="" )?$post_data['order_type']:2;
      //  $cart_detail->order_type    = $post_data['order_type'];
        if($cart_detail->order_type == 2)
        {
            //~ $cart_detail->delivery_cost = 0;
            //~ $cart_detail->delivery_slot = 0;
            //~ $cart_detail->delivery_date = "NOW()";
            $cart_detail->delivery_address = 0;
        }
        $cart_detail->coupon_id     = ($post_data['coupon_id'] !="" )?$post_data['coupon_id']:0;
        $cart_detail->coupon_amount = ($post_data['coupon_amount'] != "")?$post_data['coupon_amount']:0;
        $cart_detail->coupon_type   = ($post_data['coupon_type'] != "")?$post_data['coupon_type']:0;
       
		$cart_detail->appoinment_time   = ($post_data['appoinment_time'] != "")?$post_data['appoinment_time']:0;
        $cart_detail->appoinment_date   = ($post_data['appoinment_date'] != "")?$post_data['appoinment_date']:0;
        $cart_detail->service_provider_preference   = ($post_data['service_provider_preference'] != "")?$post_data['service_provider_preference']:0;
        $cart_detail->service_professionals   = ($post_data['service_professionals'] != "")?$post_data['service_professionals']:0;
		$user_details = $this->check_login();
		$cart_detail->tips_percentage =0;
		$cart_detail->tips_percentage_amount = 0;
		if($user_details->tips_percentage >0)
		{
			$cart_detail->tips_percentage = $user_details->tips_percentage;
			$cart_detail->tips_percentage_amount =  (($cart_detail->sub_total * $cart_detail->tips_percentage)/100);
		}
		if($cart_detail->order_type == 1)
		{
			$delivery_settings  = array();
			$check_user_address = user_address_details($user_id, $delivery_address);
			if(count($check_user_address) > 0 && isset($check_user_address->location_id) && $check_user_address->location_id != '')
			{
				$delivery_settings  = DB::table('outlets')
								->select('restaurant_delivery_settings.id')
								->Join('restaurant_delivery_settings','restaurant_delivery_settings.restaurant_id','=','outlets.id')
								->Join('restaurant_delivery_settings_locations','restaurant_delivery_settings.id','=','restaurant_delivery_settings_locations.restaurant_delivery_settings_id')
								->where('outlets.id','=',$post_data['outlet_id'])
								->where('restaurant_delivery_settings_locations.location_id','=',$check_user_address->location_id)
								->first();
			}
            /*if(count($delivery_settings) == 0)
            {
                $result = array('httpCode' => 400, 'status' => false,'Message'=>'Sorry we are not delivering on the selected area');
                return json_encode($result);
            }*/
        }
		if(isset($post_data['stripeToken']) && $post_data['stripeToken'] !="")
		{
			$user_id = Session::get('user_id');
            $cart_details  = $cart_detail;
            $payment_array = array();
			
            $total_amount  = (($cart_details->sub_total+$cart_details->tax+$cart_details->delivery_cost+$cart_detail->tips_percentage_amount)-$cart_details->coupon_amount);
			
			//print_r($cart_details);
            $admin_commission  = ((($cart_details->sub_total+ $cart_details->tax + $cart_details->delivery_cost+$cart_details->tips_percentage_amount) * $cart_details->payment_gateway_detail->commision)/100);
            
			//print_r($cart_details);exit;
            $vendor_commission = $cart_details->sub_total - $admin_commission;
            $payment_array['admin_commission']   = $admin_commission;
            $payment_array['application_fee']   = $admin_commission;
            $payment_array['vendor_commission']  = ($vendor_commission+ $cart_details->tax + $cart_details->delivery_cost+$cart_details->tips_percentage_amount);
            $payment_array['user_id']            = $user_id;
            $payment_array['store_id']           = $cart_details->cart_items->vendor_id;
            $payment_array['outlet_id']          = $cart_details->cart_items->outlet_id;
            $vendotr_detail                      = getStoreVendorLists($cart_details->cart_items->vendor_id);
            $payment_array['vendor_key']         = $vendotr_detail[0]->vendor_key;
            $payment_array['total']              = $total_amount;
            $payment_array['sub_total']          = $cart_details->sub_total;
            $payment_array['service_tax']        = $cart_details->tax;
            $payment_array['order_status']       = 1;
            $payment_array['order_key']          = str_random(32);
            //$payment_array['invoice_id']       = 'INV'.str_random(8).time();
            $payment_array['transaction_id']     = str_random(32);
            $payment_array['transaction_staus']  = 1;
            $payment_array['transaction_amount'] = $total_amount;
            $payment_array['payer_id']           = str_random(32);
            $payment_array['currency_code']      = getCurrency();
            $payment_array['currency_side']      = getCurrencyPosition()->currency_side;
            $payment_array['payment_gateway_id'] = $cart_details->payment_gateway_detail->payment_gateway_id;
            $payment_array['delivery_charge']    = 0;
            $payment_array['payment_status']     = 0;
            $payment_array['payment_gateway_commission'] = 0;
            $payment_array['delivery_instructions']      = $cart_details->delivery_notes;
            $payment_array['delivery_address']   = $cart_details->delivery_address;
            //~ $payment_array['delivery_slot']  = $cart_details->delivery_slot;
            $payment_array['delivery_date']      = $cart_details->delivery_date;
            $payment_array['order_type']         = $cart_details->order_type;
            $payment_array['coupon_id']          = $cart_details->coupon_id;
            $payment_array['coupon_amount']      = $cart_details->coupon_amount;
            $payment_array['coupon_type']        = $cart_details->coupon_type;
            $payment_array['delivery_cost']      = $cart_details->delivery_cost;
			
            $payment_array['appoinment_time']      = $cart_details->appoinment_time;
            $payment_array['appoinment_date']      = date('Y-m-d',strtotime($cart_details->appoinment_date));
			
            $payment_array['service_provider_preference']      = $cart_details->service_provider_preference;
            $payment_array['service_professionals']      = $cart_details->service_professionals;
            $payment_array['tips_percentage_amount']      = $cart_details->tips_percentage_amount;
			$items = array();
            $i     = 0;
            $from_currency = "USD";
            $to_currency   = getCurrencycode();
            if($to_currency=="USD"){
                
            $total_amount = $total_amount;
            }else{

            $amount        = urlencode($total_amount);
            $from_currency = urlencode($from_currency);
            $to_currency   = urlencode($to_currency);
            $get           = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from_currency&to=$to_currency");
            $get = explode("<span class=bld>",$get);
            $converted_amount = $total_amount;
            if(isset($get[1]))
            {
                $get = explode("</span>",$get[1]);
                if(isset($get[0]))
                {
                    $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
                }
            }
            $total_amount = $converted_amount;
                
            }
            $cart_session = Session::get('cart');
            foreach($cart_session[$cart_details->cart_items->vendor_id][$cart_details->cart_items->outlet_id] as $cartitems)
            {
                $product_id = $cartitems['product_id'];
                $items[$i]['product_id']  = $product_id;
                $items[$i]['quantity']    = $cartitems[$product_id]['quantity'];
                $items[$i]['discount_price'] = $cartitems[$product_id]['discount_price'];
                $items[$i]['ingredients'] = isset($cartitems[$product_id]['ingredients'])?$cartitems[$product_id]['ingredients']:'';
                $items[$i]['item_offer']  = 0;
                $i++;
            }
            $user_email_id = Session::get('email');
			
			$stripetoken = $post_data['stripeToken'];
			$customer = $stripe->customers()->create([
				'email' => $user_email_id,
				'source'  => $stripetoken
			]);

            // new 
              $aplication_fee_amt=round($admin_commission,2);
            //old 
            // $charge =  $stripe->charges()->create([
            //  'customer' => $customer['id'],
            //  'amount'   => $total_amt,
            //  'currency' => 'usd'
            // ]);


            

               $vendor_details=Vendors::find($cart_details->cart_items->vendor_id);
            if($vendor_details->stripe_connect_status!=1 || $vendor_details->stripe_connect_user_id==null || $vendor_details->stripe_connect_user_id==''){


                Session::flash('message-failure', $response->response->Message);
                return Redirect::to('/checkout')->send();

            }else{

        // Set your secret key: remember to change this to your live secret key in production
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            \Stripe\Stripe::setApiKey("sk_test_wFcqx8WYVjmrFNRqCIHDiyEe");



                if ( is_numeric( $total_amount ) && strpos( $total_amount, '.' ) != false ){
                        $amount_planned=number_format((float)$total_amount, 2, '.', '');
                        $submitting_amount=$amount_planned*100;
                        //$response = $user->charge($submitting_amount);
                    }else{
                    $submitting_amount=$total_amount.'00';
                    }


                    if ( is_numeric( $aplication_fee_amt ) && strpos( $aplication_fee_amt, '.' ) != false ){
                        $amount_planned=number_format((float)$aplication_fee_amt, 2, '.', '');
                        $aplication_fee_amt=$amount_planned*100;
                        //$response = $user->charge($submitting_amount);
                    }else{
                    $aplication_fee_amt=$aplication_fee_amt.'00';
                    }


               
            $charge = \Stripe\Charge::create(array(
              "amount" => $submitting_amount,
              "currency" => "usd",
              "source" => "tok_visa",
              "application_fee" => $aplication_fee_amt,
            ), array("stripe_account" => $vendor_details->stripe_connect_user_id));


            //old


			// $charge =  $stripe->charges()->create([
			// 	'customer' => $customer['id'],
			// 	'amount'   => $total_amount,
			// 	'currency' => 'usd'
			// ]);
            $payment_array['items'] = $items;
			
			$payment_params = $charge;
            Session::put('checkout_info',$payment_array);
		
			
			$payment_array = json_encode($payment_array);
			$payment_params = json_encode($payment_params);
			$user_array = array("user_id" => $user_id,"token"=>$token,"language"=>$language,"payment_array" =>$payment_array,"payment_params" =>$payment_params);
			$method = "POST";
			$data = array('form_params' => $user_array);
			//print_r($data);exit;
			$response = $this->api->call_api($data,'api/online_payment',$method);
			// print_r($response);exit;
			if($response->response->httpCode == 200)
			{
				Session::flash('message-success', trans('messages.Your order has been placed successfully'));
				return Redirect::to('/thankyou/'.encrypt($response->response->order_id))->send();
			}
			else
			{
				Session::flash('message-failure', $response->response->Message);
				return Redirect::to('/checkout')->send();
			}

        }
        //Session::forget('cart');
		}
		else if($cart_detail->payment_gateway_detail->payment_type == 0) { //Cash on delivery
            $this->offline_payment($cart_detail);
            Session::forget('cart');
        }
		else if($cart_detail->payment_gateway_detail->payment_type == 1) 
		{ //Paypal
            $user_id = Session::get('user_id');
            $cart_details  = $cart_detail;
            $payment_array = array();
            $total_amount  = ($cart_details->sub_total+$cart_details->tax+$cart_details->delivery_cost+$cart_detail->tips_percentage_amount)-$cart_details->coupon_amount;
            $admin_commission  = ((($cart_details->sub_total+ $cart_details->tax + $cart_details->delivery_cost+$cart_details->tips_percentage_amount) * $cart_details->payment_gateway_detail->commision)/100);
            $vendor_commission = $cart_details->sub_total - $admin_commission;
            $payment_array['admin_commission']   = $admin_commission ;
            $payment_array['vendor_commission']  = ($vendor_commission+ $cart_details->tax + $cart_details->delivery_cost+$cart_details->tips_percentage_amount);
            $payment_array['user_id']            = $user_id;
            $payment_array['store_id']           = $cart_details->cart_items->vendor_id;
            $payment_array['outlet_id']          = $cart_details->cart_items->outlet_id;
            $vendotr_detail                      = getStoreVendorLists($cart_details->cart_items->vendor_id);
            $payment_array['vendor_key']         = $vendotr_detail[0]->vendor_key;
            $payment_array['total']              = $total_amount;
            $payment_array['sub_total']          = $cart_details->sub_total;
            $payment_array['service_tax']        = $cart_details->tax;
            $payment_array['order_status']       = 1;
            $payment_array['order_key']          = str_random(32);
            //$payment_array['invoice_id']       = 'INV'.str_random(8).time();
            $payment_array['transaction_id']     = str_random(32);
            $payment_array['transaction_staus']  = 1;
            $payment_array['transaction_amount'] = $total_amount;
            $payment_array['payer_id']           = str_random(32);
            $payment_array['currency_code']      = getCurrency();
            $payment_array['currency_side']      = getCurrencyPosition()->currency_side;
            $payment_array['payment_gateway_id'] = $cart_details->payment_gateway_detail->payment_gateway_id;
            $payment_array['delivery_charge']    = 0;
            $payment_array['payment_status']     = 0;
            $payment_array['payment_gateway_commission'] = 0;
            $payment_array['delivery_instructions']      = $cart_details->delivery_notes;
            $payment_array['delivery_address']   = $cart_details->delivery_address;
            //~ $payment_array['delivery_slot']  = $cart_details->delivery_slot;
            $payment_array['delivery_date']      = $cart_details->delivery_date;
            $payment_array['order_type']         = $cart_details->order_type;
            $payment_array['coupon_id']          = $cart_details->coupon_id;
            $payment_array['coupon_amount']      = $cart_details->coupon_amount;
            $payment_array['coupon_type']        = $cart_details->coupon_type;
            $payment_array['delivery_cost']      = $cart_details->delivery_cost;
			
            $payment_array['appoinment_time']      = $cart_details->appoinment_time;
            $payment_array['appoinment_date']      = date('Y-m-d',strtotime($cart_details->appoinment_date));
            $payment_array['service_provider_preference']      = $cart_details->service_provider_preference;
            $payment_array['service_professionals']      = $cart_details->service_professionals;
			$payment_array['tips_percentage_amount']      = $cart_details->tips_percentage_amount;
			$items = array();
            $i     = 0;
            $from_currency = "USD";
            $to_currency   = getCurrencycode();
            $amount        = urlencode($total_amount);
            $from_currency = urlencode($from_currency);
            $to_currency   = urlencode($to_currency);
            $get           = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from_currency&to=$to_currency");
            $get = explode("<span class=bld>",$get);
            $converted_amount = $total_amount;
            if(isset($get[1]))
            {
                $get = explode("</span>",$get[1]);
                if(isset($get[0]))
                {
                    $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
                }
            }
            //$this->currency_converter($post);
            $total_amount = $converted_amount;
            $cart_session = Session::get('cart');
            foreach($cart_session[$cart_details->cart_items->vendor_id][$cart_details->cart_items->outlet_id] as $cartitems)
            {
                $product_id = $cartitems['product_id'];
                $items[$i]['product_id']  = $product_id;
                $items[$i]['quantity']    = $cartitems[$product_id]['quantity'];
                $items[$i]['discount_price'] = $cartitems[$product_id]['discount_price'];
                $items[$i]['ingredients'] = isset($cartitems[$product_id]['ingredients'])?$cartitems[$product_id]['ingredients']:'';
                $items[$i]['item_offer']  = 0;
                $i++;
            }
            $payment_array['items'] = $items;
            Session::put('checkout_info',$payment_array);
            $paypal=Paypal::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);
            try {
                $payer = PayPal::Payer();
                $payer->setPaymentMethod('paypal');
                $amount = PayPal:: Amount();
                $amount->setCurrency('USD');
                $amount->setTotal($total_amount);//->setDetails($details);
                $item  = $items = array();
                $index = 0;
                $itemList = new ItemList(); 
                $transaction = PayPal::Transaction();
                $transaction->setAmount($amount);
                $info ='Place order - Pay on $ '.$total_amount;
                //print_r($itemList);exit;
                $transaction->setDescription($info)->setItemList($itemList);
                $redirectUrls = PayPal:: RedirectUrls();
                $redirectUrls->setReturnUrl(route('getDone'));
                $redirectUrls->setCancelUrl(route('getCancel'));
                $payment = PayPal::Payment();
                $payment->setIntent('sale');
                $payment->setPayer($payer);
                $payment->setRedirectUrls($redirectUrls);
                $payment->setTransactions(array($transaction));
                $response = $payment->create($this->_apiContext);
                Session::forget('cart');
                $redirectUrl = $response->links[1]->href;
                return redirect()->to($redirectUrl);
            }
            catch(Exception $ex) 
            {
                ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex); exit(1);
            }
            Session::flash('message', 'Error: Oops. Something went wrong. Please try again later.'); 
            return Redirect::to('/');
        }
		else if($cart_detail->payment_gateway_detail->payment_type == 2) 
		{ //PayFort
            $user_id = Session::get('user_id');
            $user_email = Session::get('email');
            $merchant_reference = str_random(16);
            $lang = App::getLocale(); //getCurrentLang();
            $return_url = url("/")."/checkout/thankyou";
            $cart_details  = $cart_detail;
            $payment_array = array();
            $total_amount  = ($cart_details->sub_total+$cart_details->tax+$cart_details->delivery_cost+$cart_detail->tips_percentage_amount)-$cart_details->coupon_amount;
            $admin_commission  = ((($cart_details->sub_total+ $cart_details->tax + $cart_details->delivery_cost+$cart_details->tips_percentage_amount) * $cart_details->payment_gateway_detail->commision)/100);
            $vendor_commission = $cart_details->sub_total - $admin_commission;
            $payment_array['admin_commission']   = $admin_commission;
            $payment_array['vendor_commission']  = ($vendor_commission  + $cart_details->tax + $cart_details->delivery_cost+$cart_details->tips_percentage_amount);
            $payment_array['user_id']            = $user_id;
            $payment_array['store_id']           = $cart_details->cart_items->vendor_id;
            $payment_array['outlet_id']          = $cart_details->cart_items->outlet_id;
            $vendotr_detail                      = getStoreVendorLists($cart_details->cart_items->vendor_id);
            $payment_array['vendor_key']         = $vendotr_detail[0]->vendor_key;
            $payment_array['total']              = $total_amount;
            $payment_array['sub_total']          = $cart_details->sub_total;
            $payment_array['service_tax']        = $cart_details->tax;
            $payment_array['order_status']       = 1;
            $payment_array['order_key']          = str_random(32);
            //$payment_array['invoice_id']       = 'INV'.str_random(8).time();
            $payment_array['transaction_id']     = str_random(32);
            $payment_array['transaction_staus']  = 1;
            $payment_array['transaction_amount'] = $total_amount;
            $payment_array['payer_id']           = str_random(32);
            $payment_array['currency_code']      = getCurrency();
            $payment_array['currency_side']      = getCurrencyPosition()->currency_side;
            $payment_array['payment_gateway_id'] = $cart_details->payment_gateway_detail->payment_gateway_id;
            $payment_array['delivery_charge']    = 0;
            $payment_array['payment_status']     = 0;
            $payment_array['payment_gateway_commission'] = 0;
            $payment_array['delivery_instructions']      = $cart_details->delivery_notes;
            $payment_array['delivery_address']   = $cart_details->delivery_address;
            //~ $payment_array['delivery_slot']  = $cart_details->delivery_slot;
            $payment_array['delivery_date']      = $cart_details->delivery_date;
            $payment_array['order_type']         = $cart_details->order_type;
            $payment_array['coupon_id']          = $cart_details->coupon_id;
            $payment_array['coupon_amount']      = $cart_details->coupon_amount;
            $payment_array['coupon_type']        = $cart_details->coupon_type;
            $payment_array['delivery_cost']      = $cart_details->delivery_cost;
            $payment_array['tips_percentage_amount']      = $cart_details->tips_percentage_amount;
            $items = array();
            $i     = 0;
            $cart_session = Session::get('cart');
            foreach($cart_session[$cart_details->cart_items->vendor_id][$cart_details->cart_items->outlet_id] as $cartitems) {
                $product_id = $cartitems['product_id'];
                $items[$i]['product_id']  = $product_id;
                $items[$i]['quantity']    = $cartitems[$product_id]['quantity'];
                $items[$i]['discount_price'] = $cartitems[$product_id]['discount_price'];
                $items[$i]['ingredients'] = isset($cartitems[$product_id]['ingredients'])?$cartitems[$product_id]['ingredients']:'';
                $items[$i]['item_offer']  = 0;
                $i++;
            }
            $payment_array['items'] = $items;
            Session::put('checkout_info',$payment_array);
            $total_new_amount = $total_amount*100;
            $str = "qwertwqertaccess_code=".$cart_detail->payment_gateway_detail->merchant_key."amount=".$total_new_amount."command=PURCHASEcurrency=".$cart_detail->payment_gateway_detail->currency_code."customer_email=".$user_email."language=".$lang."merchant_identifier=".$cart_detail->payment_gateway_detail->account_id."merchant_reference=".$merchant_reference."payment_option=MASTERCARDqwertwqert";
            $signature = hash('sha256', $str);
            $signature = hash('sha256', $str);
            $requestParams = array(
            'access_code' => $cart_detail->payment_gateway_detail->merchant_key,
            'amount' => $total_new_amount,
            'currency' => $cart_detail->payment_gateway_detail->currency_code,
            'customer_email' => $user_email,
            'merchant_reference' => $merchant_reference,
            'language' => $lang,
            'merchant_identifier' => $cart_detail->payment_gateway_detail->account_id,
            'signature' => $signature,
            'command' => 'PURCHASE',
            'payment_option' => 'MASTERCARD',
            );
            if($cart_detail->payment_gateway_detail->payment_mode==1){
                $redirectUrl = 'https://sbcheckout.payfort.com/FortAPI/paymentPage';
            } else {
                $redirectUrl = 'https://checkout.payfort.com/FortAPI/paymentPage';
            }
            echo "<html xmlns='http://www.w3.org/1999/xhtml'>\n<head></head>\n<body>\n";
            echo "<form action='$redirectUrl' method='post' name='frm'>\n";
            foreach ($requestParams as $a => $b) {
                echo "\t<input type='hidden' name='".htmlentities($a)."' value='".htmlentities($b)."'>\n";
            }
            echo "\t<script type='text/javascript'>\n";
            echo "\t\tdocument.frm.submit();\n";
            echo "\t</script>\n";
            echo "</form>\n</body>\n</html>";
            
            /*return view('front.'.$this->theme.'.checkout_cards')->with("total_amount",$total_amount)->with("payment_details",$payment_array);*/
        }
    }
    /*public function paypal_payment($cart_details)
    {
        $payment_array =array();
        $total_amount = ($cart_details->sub_total+$cart_details->tax+$cart_details->delivery_cost)-$cart_details->coupon_amount;
        $payment_array['user_id'] = $cart_details->cart_items[0]->user_id;
        $payment_array['store_id'] = $cart_details->cart_items[0]->store_id;
        $payment_array['outlet_id'] = $cart_details->cart_items[0]->outlet_id;
        $payment_array['vendor_key'] = $cart_details->cart_items[0]->vendor_key;
        $payment_array['total'] = $total_amount;
        $payment_array['sub_total'] = $cart_details->sub_total;
        $payment_array['service_tax'] = $cart_details->tax;
        $payment_array['order_status'] = 1;
        $payment_array['order_key'] = str_random(32);
        $payment_array['invoice_id'] = str_random(32);
        $payment_array['transaction_id'] = str_random(32);
        $payment_array['transaction_staus'] = 1;
        $payment_array['transaction_amount'] = $total_amount;
        $payment_array['payer_id'] = str_random(32);
        $payment_array['currency_code'] = getCurrency();
        $payment_array['payment_gateway_id'] = $cart_details->payment_gateway_detail->id;
        $payment_array['coupon_type'] = 0;
        $payment_array['delivery_charge'] = 0;
        $payment_array['payment_status'] = 0;
        $payment_array['vendor_commission'] = 0;
        $payment_array['payment_gateway_commission'] = 0;
        $payment_array['delivery_instructions'] = $cart_details->delivery_notes;
        $payment_array['delivery_address'] = $cart_details->delivery_address;
        $payment_array['delivery_slot'] = $cart_details->delivery_slot;
        $payment_array['delivery_date'] = $cart_details->delivery_date;
        $payment_array['order_type'] = $cart_details->order_type;
        $payment_array['coupon_id'] = $cart_details->coupon_id;
        $payment_array['coupon_amount'] = $cart_details->coupon_amount;
        $payment_array['delivery_cost'] = $cart_details->delivery_cost;
        $items = array();
        $i = 0;
        foreach($cart_details->cart_items as $cartitems)
        {
            $items[$i]['product_id'] = $cartitems->product_id;
            $items[$i]['quantity'] = $cartitems->quantity;
            $items[$i]['discount_price'] = $cartitems->discount_price;
            $items[$i]['item_offer'] = 0;
            $i++;
        }
        $payment_array['items'] = $items;
        print_r($payment_array);exit;
        Session::put('checkout_info',$payment_array);
        $paypal=Paypal::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);
        // process the validation
             //Save Details
            try    {
                //echo $total_amount;exit;
                $payer = PayPal    ::Payer();
                $payer->setPaymentMethod('paypal');
                $amount = PayPal:: Amount();
                $amount->setCurrency('USD');
                $amount->setTotal($total_amount);//->setDetails($details);
                
                /*$item1 = new Item();
                $item1->setName("dark choclate")->setCurrency('USD')->setQuantity(1)->setPrice(1235);
                */
                
                /*$itemList = new ItemList(); 
                $itemList->setItems(array($item1));
                
                $item  = array();
                $items = array();
                $index = 0;
                foreach ($cart_details->cart_items as $_item) 
                {
                    $index++;
                    $item[$index] = new Item();
                    $item[$index]->setName($_item->product_name)
                                 ->setCurrency('USD')
                                 ->setQuantity($_item->quantity)
                                 ->setPrice($_item->discount_price);
                }
                $itemList = new ItemList(); $itemList->setItems($items);
                $transaction = PayPal::Transaction();
                $transaction->setAmount($amount);
                $info ='Place order - Pay on $ '.$total_amount;
                $transaction->setDescription($info)->setItemList($itemList);
                $redirectUrls = PayPal:: RedirectUrls();
                $redirectUrls->setReturnUrl(route('getDone'));
                $redirectUrls->setCancelUrl(route('getCancel'));
                $payment = PayPal::Payment();
                $payment->setIntent('sale');
                $payment->setPayer($payer);
                $payment->setRedirectUrls($redirectUrls);
                $payment->setTransactions(array($transaction));
                $response = $payment->create($this->_apiContext);
                $redirectUrl = $response->links[1]->href;
                header("Location: ".$redirectUrl);
            }
            catch(Exception $ex) 
            {
                 ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex); exit(1);
            }
            Session::flash('message', 'Error: Oops. Something went wrong. Please try again later.'); 
            return Redirect::to('/');
    } */
    
    public function getDone(Request $request)
    {
        $checkout_info = Session::get('checkout_info');
        if($checkout_info=='')
        {
            Session::flash('message', 'Error: Oops. Something went wrong. Please try again later.'); 
            return Redirect::to('/');
        }
        $id    = $request->get('paymentId');
        $token = $request->get('token');
        $payer_id = $request->get('PayerID');
        $all_info = Paypal::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);

        /** get payment request responce  **/
            $payment = PayPal::getById($id, $this->_apiContext);
            $paymentExecution = PayPal::PaymentExecution();
            $paymentExecution->setPayerId($payer_id);
            $executePayment = $payment->execute($paymentExecution, $this->_apiContext);
        /** get payment request responce  **/
        //Session::put('checkout_info','');
        if(isset($executePayment->state) && $executePayment->state=="approved")
        {
            $Id = $executePayment->getId();
            $Intent = $executePayment->getIntent();
            $Payer  = $executePayment->getPayer();
            $Payee  = $executePayment->getPayee();
            $Cart   = $executePayment->getCart();
            $payment_method = $executePayment->payer->payment_method;
            $paypal_email   = $executePayment->payer->payer_info->email;
            $country_code   = $executePayment->payer->payer_info->country_code;
            $Transactions   = $executePayment->getTransactions();
            $PaymentInstruction = $executePayment->getPaymentInstruction();
            $State = $executePayment->getState();
            $ExperienceProfileId = $executePayment->getExperienceProfileId();
            $CreateTime = $executePayment->getCreateTime();
            $UpdateTime = $executePayment->getUpdateTime();
            $ApprovalLink = $executePayment->getApprovalLink();
            $all_data = $executePayment->get($id, $this->_apiContext);
            $payment_params = '';
            if(isset($Transactions['0']) && $Transactions['0'] != '')
            {
                $related_resource = $Transactions['0']->related_resources;
                //print_r($related_resource);exit;
                foreach($related_resource as $key => $value)
                {
                    /** get payment transaction responce  **/
                    $payment_id = $value->sale->id;
                    $payment_state  = $value->sale->state;
                    $payment_amount = $value->sale->amount;
                    $payment_mode   = $value->sale->payment_mode;
                    //$reason_code  = $value->state->reason_code;
                    $protection_eligibility = $value->sale->protection_eligibility;
                    $protection_eligibility = $value->sale->protection_eligibility_type;
                    $parent_payment = $value->sale->parent_payment;
                    $create_time = $value->sale->create_time;
                    $update_time = $value->sale->update_time;
                    $links = $value->sale->links;
                    /** get payment transaction responce  end **/
                    /**calculate admin and vendor commision ammount*/
                        /*$admin_commision_per=getAppPaymentConfig()->commision;
                        $admin_commision=round($payment_amount->total)/100*$admin_commision_per;
                        $vendor_commision=round($payment_amount->total)-$admin_commision;*/
                    /**calculate admin and vendor commision ammount end*/

                    /** set payment_params  */
                    $payment_params = array("parent_payment_id"=>$Id,"token"=>$token,"payer_id"=>$payer_id,"Intent"=>$Intent,"Payee"=>$Payee,"cart_id"=>$Cart,"payment_id"=>$payment_id,"payment_state"=>$State,"payment_amount"=>$payment_amount,"payment_mode"=>$payment_mode,"create_time"=>$create_time,"update_time"=>$update_time,"links"=>$links,'payment_method'=>$payment_method,'paypal_email'=>$paypal_email,'country_code'=>$country_code);
                    //$payment_params = array("parent_payment_id"=>$Id,"token"=>$token,"payer_id"=>$payer_id,"Intent"=>$Intent,"Payee"=>$Payee,"cart_id"=>$Cart,"payment_id"=>$payment_id,"payment_state"=>$State,"payment_amount"=>$payment_amount,"payment_mode"=>$payment_mode,"reason_code"=>$reason_code,"valid_until"=>$valid_until,"create_time"=>$create_time,"update_time"=>$update_time,"links"=>$links,'payment_method'=>$payment_method,'paypal_email'=>$paypal_email,'country_code'=>$country_code,'admin_commision'=>$admin_commision,'vendor_commision'=>$vendor_commision);
                    /** set payment_params  end*/
                    //$appoinment_id=$this->Appointmentbook($checkout_info,$payment_params);
                    $user_id  = Session::get('user_id');
                    $token    = Session::get('token');
                    $language = getCurrentLang();
                    $checkout_info  = json_encode($checkout_info);
                    $payment_params = json_encode($payment_params);
                    $user_array = array("user_id" => $user_id,"token"=>$token,"language"=>$language,"payment_array" =>$checkout_info,"payment_params"=>$payment_params);
                    //print_r($user_array);exit;
                    $method = "POST";
                    $data   = array('form_params' => $user_array);
                    //~ echo '<pre>';print_r($user_array);die;
                    $response = $this->api->call_api($data,'api/online_payment',$method);
                    if($response->response->httpCode == 200)
                    {
                        Session::flash('message-success', trans('messages.Your order has been placed successfully'));
                        return Redirect::to('/thankyou/'.encrypt($response->response->order_id))->send();
                    }
                    else
                    {
                        Session::flash('message-failure', $response->response->Message);
                        return Redirect::to('/checkout')->send();
                    }
                }
            }
            else
            {
                Session::flash('message', 'Error:PaymentSuccess Oops. Something went wrong. Please try again later.'); 
                return Redirect::to('/');
            }
        }
    }
    
    public  function getCancel()
    {
        //echo "asdfasdf";exit;
        Session::flash('message', 'Error:Proccess has been cancelled by user.'); 
        return Redirect::to('/checkout');
    }
    
    public function getDonePayFort(Request $request)
    {
        $checkout_info = Session::get('checkout_info');
        if($checkout_info=='')
        {
            Session::flash('message', 'Error: Oops. Something went wrong. Please try again later.'); 
            return Redirect::to('/');
        }
        $payfort_response    = $request;
        /** get payment request responce  **/
        //Session::put('checkout_info','');
        if((isset($payfort_response->response_code) && $payfort_response->response_code==14000) && (isset($payfort_response->response_message) && $payfort_response->response_message=='Success') && (isset($payfort_response->status) && $payfort_response->status==14)) {
            /** set payment_params  */
            $payment_params = array("amount"=>$payfort_response->get("amount"),
            "response_code"=>$payfort_response->get("response_code"),
            "card_number"=>$payfort_response->get("card_number"),
            "signature"=>$payfort_response->get("signature"),
            "merchant_identifier"=>$payfort_response->get("merchant_identifier"),
            "expiry_date"=>$payfort_response->get("expiry_date"),
            "access_code"=>$payfort_response->get("access_code"),
            "payment_option"=>$payfort_response->get("payment_option"),
            "customer_ip"=>$payfort_response->get("customer_ip"),
            "language"=>$payfort_response->get("language"),
            "eci"=>$payfort_response->get("eci"),
            "fort_id"=>$payfort_response->get("fort_id"),
            "command"=>$payfort_response->get("command"),
            "payment_method"=>'credit card',
            "response_message"=>$payfort_response->get("response_message"),
            'authorization_code'=>$payfort_response->get("authorization_code"),
            'customer_email'=>$payfort_response->get("customer_email"),
            'merchant_reference'=>$payfort_response->get("merchant_reference"),
            'token_name'=>$payfort_response->get("token_name"),
            'currency'=>$payfort_response->get("currency"),
            'status'=>$payfort_response->get("status"),
            'sdk_token'=>'');
           
            $user_id  = Session::get('user_id');
            $token    = Session::get('token');
            $language = getCurrentLang();
            $checkout_info  = json_encode($checkout_info);
            $payment_params = json_encode($payment_params);
            $user_array = array("user_id" => $user_id,"token"=>$token,"language"=>$language,"payment_array" =>$checkout_info,"payment_params"=>$payment_params);
            //print_r($user_array);exit;
            $method = "POST";
            $data   = array('form_params' => $user_array);
            //~ echo '<pre>';print_r($user_array);die;
            $response = $this->api->call_api($data,'api/online_payment',$method);
            //echo '<pre>';print_r($response);die;
            if($response->response->httpCode == 200) {
                Session::flash('message-success', trans('messages.Your order has been placed successfully'));
                return Redirect::to('/thankyou/'.encrypt($response->response->order_id))->send();
            } else {
                Session::flash('message-failure', $response->response->Message);
                return Redirect::to('/checkout')->send();
            }
        } else {
            Session::flash('message', 'Error:PaymentSuccess Oops. Something went wrong. Please try again later.'); 
            return Redirect::to('/');
        }
    }
    
    
    public function offline_payment($cart_details)
    {
        $payment_array = array();
        $user_id       = Session::get('user_id');
        $total_amount  = ($cart_details->sub_total+$cart_details->tax+$cart_details->delivery_cost+$cart_details->tips_percentage_amount)-$cart_details->coupon_amount;
        $admin_commission  = ((($cart_details->sub_total+ $cart_details->tax + $cart_details->delivery_cost+$cart_details->tips_percentage_amount) * $cart_details->payment_gateway_detail->commision)/100);
        $payment_array['admin_commission']  = $admin_commission ;
        $vendor_commission = $cart_details->sub_total - $admin_commission;
        $payment_array['vendor_commission'] = ($vendor_commission+ $cart_details->tax + $cart_details->delivery_cost+$cart_details->tips_percentage_amount);
        $payment_array['user_id']        = $user_id;
        $payment_array['store_id']       = $cart_details->cart_items->vendor_id;
        $payment_array['outlet_id']      = $cart_details->cart_items->outlet_id;
        $vendotr_detail                  = getStoreVendorLists($cart_details->cart_items->vendor_id);
        $payment_array['vendor_key']     = $vendotr_detail[0]->vendor_key;
        $payment_array['total']          = $total_amount;
        $payment_array['sub_total']      = $cart_details->sub_total;
        $payment_array['service_tax']    = $cart_details->tax;
        $payment_array['order_status']   = 1;
        $payment_array['order_key']      = str_random(32);
        $payment_array['invoice_id']     = str_random(32);
        $payment_array['transaction_id'] = str_random(32);
        $payment_array['transaction_staus']  = 1;
        $payment_array['transaction_amount'] = $total_amount;
        $payment_array['payer_id']           = str_random(32);
        $payment_array['currency_code']      = getCurrency();
        $payment_array['payment_gateway_id'] = $cart_details->payment_gateway_detail->payment_gateway_id;
        $payment_array['delivery_charge']    = 0;
        $payment_array['payment_status']     = 0;
        $payment_array['payment_gateway_commission'] = 0;
        $payment_array['delivery_instructions']      = $cart_details->delivery_notes;
        $payment_array['delivery_address'] = $cart_details->delivery_address;
        $payment_array['delivery_date']    = $cart_details->delivery_date;
        $payment_array['order_type']       = $cart_details->order_type;
        $payment_array['coupon_id']        = $cart_details->coupon_id;
        $payment_array['coupon_amount']    = $cart_details->coupon_amount;
        $payment_array['coupon_type']      = $cart_details->coupon_type;
        $payment_array['delivery_cost']    = $cart_details->delivery_cost;
		
		$payment_array['appoinment_time']      = $cart_details->appoinment_time;
		$payment_array['appoinment_date']      = date('Y-m-d',strtotime($cart_details->appoinment_date));
		$payment_array['service_provider_preference']      = $cart_details->service_provider_preference;
		$payment_array['service_professionals']      = $cart_details->service_professionals;
		$payment_array['tips_percentage_amount']      = $cart_details->tips_percentage_amount;
		
		
        $items = array();
        $i = 0;
        $cart_session = Session::get('cart');
        foreach($cart_session[$cart_details->cart_items->vendor_id][$cart_details->cart_items->outlet_id] as $cartitems)
        {
            $product_id = $cartitems['product_id'];
            $items[$i]['product_id']  = $product_id;
            $items[$i]['quantity']    = $cartitems[$product_id]['quantity'];
            $items[$i]['discount_price'] = $cartitems[$product_id]['discount_price'];
            $items[$i]['ingredients'] = isset($cartitems[$product_id]['ingredients'])?$cartitems[$product_id]['ingredients']:'';
            $items[$i]['item_offer']  = 0;
            $i++;
        }
        $payment_array['items'] = $items;
        $payment_array = json_encode($payment_array);
        $user_id = Session::get('user_id');
        $token = Session::get('token');
        $language = getCurrentLang();
        $user_array = array("user_id" => $user_id,"token"=>$token,"language"=>$language,"payment_array" =>$payment_array);
        $method = "POST";
        $data = array('form_params' => $user_array);
         //print_r($data);exit;
        $response = $this->api->call_api($data,'api/offline_payment',$method);
        //print_r($response);exit;
        if($response->response->httpCode == 200)
        {
            Session::flash('message-success', trans('messages.Your order has been placed successfully'));
            return Redirect::to('/thankyou/'.encrypt($response->response->order_id))->send();
        }
        else
        {
            Session::flash('message-failure', $response->response->Message);
            return Redirect::to('/checkout')->send();
        }
    }
    public function update_promocode(Request $data)
    {
        $post_data = $data->all();

        //  dd($post_data);
        
        //  $post_data['user_id'] = Session::get('user_id');
                //  USER CAN BE AS GUEST

        $post_data['language'] = getCurrentLang();
        $post_data['token'] = Session::get('token');
        $method = "POST";
        $data = array('form_params' => $post_data);
        $checkout_details = $this->api->call_api($data,'api/update_promocode',$method);

        //  print_r($checkout_details);exit;

        return response()->json($checkout_details->response);
    }
    
    public function thankyou($id)
    {

        $order_id = decrypt($id);

        //  $order_id = $id;

        // dd($order_id);


        //  session::forget('cart');
        $user_id = Session::get('user_id');
        //  dd($user_id);
        $token = Session::get('token');

          //    dd($token);
        $language = getCurrentLang();
        $user_array = array("user_id" => $user_id,"token"=>$token,"language"=>$language,"order_id" =>$order_id);
        //~ print_r($user_array);die;
        $method = "POST";
        $data = array('form_params' => $user_array);
        $response = $this->api->call_api($data,'api/order_detail',$method);


          //    dd($outlets);

      //       echo "<pre>";
    		// print_r($response);exit;

        $booking_details = $response->response->booking_details;
        $outlets = $response->response->properties;
        $rooms_count = $response->response->rooms_count;
        $site_owner = getAppConfig()->site_owner;
        $outlet_id = $response->response->outlet_id;
        $room_count = $response->response->room_count;
        $vendors = $response->response->vendors;

//        dd($outlets);

        SEOMeta::setTitle($outlets[0]->outlet_name);
        SEOMeta::setDescription($outlets[0]->outlet_name);
        SEOMeta::addKeyword($outlets[0]->outlet_name);
        OpenGraph::setTitle($outlets[0]->outlet_name);
        OpenGraph::setDescription($outlets[0]->outlet_name);
        // OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle($outlets[0]->outlet_name);
        Twitter::setSite($outlets[0]->outlet_name);

        //$user_details = $this->check_login();


        return view('front.'.$this->theme.'.confirm_booking')->with("properties",$outlets[0])
        ->with("booking_details",$booking_details[0])->with("site_owner",$site_owner)
        ->with("rooms_count",$rooms_count[0])->with("outlet_id",$outlet_id)
        ->with("room_count",$room_count)->with("vendors",$vendors);
    }

    public function send_otp(Request $data)
    {
        $post_data = $data->all();
        $post_data['user_id'] = Session::get('user_id');
        $post_data['language'] = getCurrentLang();
        $post_data['token'] = Session::get('token');
        $method = "POST";
        $data = array('form_params' => $post_data);
        $checkout_details = $this->api->call_api($data,'api/send_otp',$method);
        //~ print_r($checkout_details);exit;
        return response()->json($checkout_details->response);
    }
    public function check_otp(Request $data)
    {
        $post_data = $data->all();
        $post_data['user_id'] = Session::get('user_id');
        $post_data['language'] = getCurrentLang();
        $post_data['token'] = Session::get('token');
        $method = "POST";
        $data = array('form_params' => $post_data);
        $checkout_details = $this->api->call_api($data,'api/check_otp',$method);
        return response()->json($checkout_details->response);
    }
    
    public function re_book_order($order_id) // to re book the same order again : 
    {

       
        $order_id = decrypt($order_id);

        //  dd($order_id);
        $post_data['user_id'] = Session::get('user_id');

        //  dd($post_data['user_id']);
         if( !isset($post_data['user_id']) || ($post_data['user_id']==null) ){
            Session::flash('message-failure', "Please login to re-schedule the Booking");
            return Redirect::to('/book-now');
        }

        //  dd('Ak');
        $post_data['language'] = getCurrentLang();
        $post_data['token'] = Session::get('token');
       // $post_data['cart'] = Session::get('cart');
        $post_data['order_id'] = $order_id;
        $method = "POST";
        //$data = array('form_params' => $post_data);
       // $checkout_details = $this->api->call_api($data,'api/re_book_order',$method);
        $query = 'SELECT o.vendor_id,o.outlet_id,oi.room_id AS room_id,o.room_type as room_type_id,
                  o.adult_count as guest_count
                  FROM booking_details o
                  LEFT JOIN booking_info oi ON oi.booking_id = o.id 
                  where o.id = ? AND o.customer_id= ? ORDER BY oi.id';

        $order_items = DB::select($query,array($post_data['order_id'],$post_data['user_id']));

        //  dd($order_items);

        $room_type_id = $order_items[0]->room_type_id;

        $guest_count = $order_items[0]->guest_count;

        $property_id = $order_items[0]->outlet_id;

        //  dd($property_id);

        $room_type = DB::table('room_type')->select('room_type.*','room_type_infos.room_type')
                                           ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                                           ->where('room_type.id','=',$room_type_id)
                                           ->get();

        $room_url_index = $room_type[0]->url_index;

        $property_details = DB::table('outlets')
                            ->select('outlets.url_index')
                            //->leftJoin('outlet_infos','outlet_infos.id','=','outlets.id')
                            ->where('outlets.id','=',$property_id)
                            ->get();

        $outlet_url_index = $property_details[0]->url_index;




        //  $property_details = outlets::pluck('url_index')->where('outlets.id','=',$property_id);
         // dd($outlet_index);

        $room_id = array();
        foreach($order_items as $out)
          {
            $room_id[] = $out->room_id;
          }

        //  dd($room_id);

        $room_count = count($room_id);
        //  $guest_count = 1;
        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime(' +1 day'));          

        //  dd($rooms_count);

        // $resp_array=array();
        // foreach($order_items as $order)
        // {
        //     $re_order_data = array();
        //     $re_order_data['user_id'] = $post_data['user_id'];
        //     $re_order_data['vendors_id'] = $order->vendor_id;
        //     $re_order_data['outlet_id'] = $order->outlet_id;

        //     $re_order_data['item_unit'] = $order->item_unit;
            
        //     $re_order_data['product_id']=$re_order_data['service_id'] = $order->product_id;
        //     $product_details= products::find($re_order_data['product_id']);
        //     $re_order_data['price']=$product_details->discount_price;
        //     $re_order_data['service_time']=$product_details->time;
        //     $cart_data = $this->re_book_add_cart($re_order_data);
        //     array_push($resp_array, $cart_data);
        //    // print_r($order);
        //     //print_r($cart_data);
     
        // }

        $query = DB::table('rooms')
                    ->leftjoin('room_type','room_type.id','=','rooms.room_type_id')
                    ->where('rooms.property_id',$property_id)
                    ->where('room_type.id',$room_type_id)
                    ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id')
                    ->groupBy('room_type.id');
                  
        $room_type = $query->get();
            //  dd($room_type);

        $rt = $room_type->pluck('id');

        //  dd($rt);

        $rtr = rooms::whereIN('room_type_id',$rt)
                    ->pluck('id')->toArray();

        $rtrs = rooms::whereIN('room_type_id',$rt)
                    ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                    ->whereBetween('booked_room.date',array($startDate,$endDate))
                    ->where('booked_room.status','=',1)
                    ->pluck('rooms.id')->toArray();

        //  return $rtrs;
        $result=array_diff($rtr,$rtrs);
        
        //  dd($result);

        $users = DB::table('rooms')
                            ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id as rt')
                            ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                            //  ->leftJoin('outlets','outlets.id','rooms.property_id')
                            ->whereIn('rooms.id', $result)
                            ->groupBy('room_type.id')
                            ->get();

        //   return $users;

        //  $room_count = 6;    

        $romco = $users->where('rooms_count','>=',$room_count)->pluck('rt')->toArray();

        if(!empty($romco))
            {
                $rooms_available = 1;

                Session::put('room_count',$room_count);
                Session::put('guest_count',$guest_count);
            }
        else
            {
                $rooms_available = 0;
            }


        //  dd($romco);

        //  dd($rooms_available);


        if($rooms_available == 0)
        {
            Session::flash('reorder-message', trans('messages.Rooms are Not Available for this Date!'));
            return Redirect::to('/view-hotel/'.$outlet_url_index)->send();
        }

        Session::flash('reorder-message', trans('messages.Checkout has been updated successfully!'));
        return Redirect::to('/checkout/'.$outlet_url_index.'/'.$room_url_index)->send();
    }


    public function re_book_add_cart($post_data){ // new function to add products to cart here only to Avoid the session issue with Web middleware : Vishal.  

       // $post_data    = $data;
        $vendors_id   = $post_data['vendors_id'];
        $outlet_id    = $post_data['outlet_id'];
        $product_id   = $post_data['service_id'];
        $qty          = $post_data['item_unit'];
        $price        = $post_data['price'];
        $service_time   = $post_data['service_time'];
        $language     = getCurrentLang();
       // $update_session = $this->update_cart_session($vendors_id, $outlet_id, $product_id, $qty, $price, $language);
        //$old_cart="";
        $old_cart = Session::get("cart");


    if(isset($old_cart) && $old_cart !=null){

        $old_vendor_id  = isset($old_cart['vendors_id'])?$old_cart['vendors_id']:0;
        $old_outlet_id  = isset($old_cart[$old_vendor_id]['outlet_id'])?$old_cart[$old_vendor_id]['outlet_id']:0;
        if(($old_vendor_id == $vendors_id) && ($old_outlet_id == $outlet_id) && $old_vendor_id != 0 && $old_outlet_id != 0)
        {
            $al_crt = $new_pro = 0;
            if(isset($old_cart[$old_vendor_id][$old_outlet_id]) && count($old_cart[$old_vendor_id][$old_outlet_id]) > 0)
            {
                foreach($old_cart[$old_vendor_id][$old_outlet_id] as $key => $val)
                {
                    $old_product_id = $val['product_id'];
                    if($old_product_id == $product_id)
                    {
                        //$result = array("response" => array("httpCode" => 200 , "Message" => "Service already added!","cart_count"=>0,"type" => 1));
                        return 1;// for same product
                        //return json_encode($result,JSON_UNESCAPED_UNICODE);
                    }
                }
                $cart_item = 0;
                if(isset($old_cart[$old_vendor_id][$old_outlet_id]) && $old_cart[$old_vendor_id][$old_outlet_id] != '')
                {
                    $cart_item = count($old_cart[$old_vendor_id][$old_outlet_id]);
                }
                $old_cart[$vendors_id][$outlet_id][$cart_item]['product_id'] = $product_id;
                $old_cart[$vendors_id][$outlet_id][$cart_item][$product_id]['quantity'] = $qty;
                $old_cart[$vendors_id][$outlet_id][$cart_item][$product_id]['discount_price'] = $price;
                $old_cart[$vendors_id][$outlet_id][$cart_item][$product_id]['service_time'] = $service_time;
                $outlet_products_align = array_values($old_cart[$vendors_id][$outlet_id]);
                $old_cart[$vendors_id][$outlet_id] = $outlet_products_align;
                $old_cart['cart_count'] = $cart_item;
                $cart1 = Session::put("cart", $old_cart);
                $result = array("response" => array("httpCode" => 200 , "Message" => "Cart has been added successfully!","type" => 1,"cart_count" => $cart_item));
                return 2;  // successfully updated
            }
           // Dont know why is htis part : need to check Vishal
           /* else
            {
                $cart_item = 0;
                if(isset($old_cart[$old_vendor_id][$old_outlet_id]) && $old_cart[$old_vendor_id][$old_outlet_id] != '')
                {
                    $cart_item = count($old_cart[$old_vendor_id][$old_outlet_id]);
                }
                $ingredient_id_count = 0;
                if($ingredient_ids != '')
                    $ingredient_id_count = count(explode(', ',$ingredient_ids));
                $ingredient_prices = array_sum(explode(', ',$prices));
                $outlet_det = get_outlet_details($outlet_id, $language);
                $old_cart['vendors_id'] = $vendors_id;
                $old_cart[$vendors_id]['outlet_id'] = $outlet_id;
                $old_cart[$vendors_id]['outlet_name'] = isset($outlet_det->outlet_name)?$outlet_det->outlet_name:'';
                $old_cart[$vendors_id]['minimum_order_amount'] = isset($outlet_det->minimum_order_amount)?$outlet_det->minimum_order_amount:'';
                $old_cart[$vendors_id][$outlet_id][$cart_item]['product_id'] = $product_id;
                $old_cart[$vendors_id][$outlet_id][$cart_item][$product_id]['quantity'] = $qty;
                $outlet_products_align = array_values($old_cart[$vendors_id][$outlet_id]);
                $old_cart[$vendors_id][$outlet_id] = $outlet_products_align;
                $old_cart['cart_count'] = $cart_item;
            //  print_r($old_cart);exit;
                Session::put("cart", $old_cart);
                $user_cart_list = user_cart_list();
                $result = array("response" => array("httpCode" => 200 , "Message" => "Cart has been added successfully!","type" => 1,"cart_count" => $cart_item,"user_cart_list" => json_decode($user_cart_list, true)));
                    return 3;// new element added successfully 
            }*/
            //echo "if";
        //  print_r($cart_item);exit;
        }
        else{



            Session::forget('cart');
            $cart_item = 0;
            $outlet_det = get_outlet_details($outlet_id, $language);
            $cart['vendors_id'] = $vendors_id;
            $cart[$vendors_id]['outlet_id'] = $outlet_id;
            $cart[$vendors_id]['outlet_name'] = isset($outlet_det->outlet_name)?$outlet_det->outlet_name:'';
            $cart[$vendors_id]['minimum_order_amount'] = isset($outlet_det->minimum_order_amount)?$outlet_det->minimum_order_amount:'';
            $cart[$vendors_id][$outlet_id][$cart_item]['product_id'] = $product_id;
            $cart[$vendors_id][$outlet_id][$cart_item][$product_id]['quantity'] = $qty;
            $cart[$vendors_id][$outlet_id][$cart_item][$product_id]['discount_price']   = $price;
            $cart[$vendors_id][$outlet_id][$cart_item][$product_id]['service_time']   = $service_time;
            $cart['cart_count'] = 1;
           // print_r($cart);
            Session::put("cart", $cart);
            $result = array("response" => array("httpCode" => 200 , "Message" => "Cart has been added successfully!","type" => 1,"cart_count" => 1));
             //return json_encode($result,JSON_UNESCAPED_UNICODE);
            return 4;
             
        }
    }
    else 
    {
            
            //Session::forget('cart');
            $cart_item = 0;
            $outlet_det = get_outlet_details($outlet_id, $language);
            $cart['vendors_id'] = $vendors_id;
            $cart[$vendors_id]['outlet_id'] = $outlet_id;
            $cart[$vendors_id]['outlet_name'] = isset($outlet_det->outlet_name)?$outlet_det->outlet_name:'';
            $cart[$vendors_id]['minimum_order_amount'] = isset($outlet_det->minimum_order_amount)?$outlet_det->minimum_order_amount:'';
            $cart[$vendors_id][$outlet_id][$cart_item]['product_id'] = $product_id;
            $cart[$vendors_id][$outlet_id][$cart_item][$product_id]['quantity'] = $qty;
            $cart[$vendors_id][$outlet_id][$cart_item][$product_id]['discount_price']   = $price;
            $cart[$vendors_id][$outlet_id][$cart_item][$product_id]['service_time']   = $service_time;
            $cart['cart_count'] = 1;
           // print_r($cart);
            Session::put("cart", $cart);
            $result = array("response" => array("httpCode" => 200 , "Message" => "Cart has been added successfully!","type" => 1,"cart_count" => 1));
             //return json_encode($result,JSON_UNESCAPED_UNICODE);
            return 4;
        }

    }


    
    public function cancel_order($order_id)
    {
         $order_id = decrypt($order_id);

        //  dd($order_id);
        $post_data['user_id'] = Session::get('user_id');
        $post_data['language'] = getCurrentLang();
        $post_data['token'] = Session::get('token');
        $post_data['order_id'] = $order_id;
        $method = "POST";

    //     $orders    = DB::table('transaction')->select('transaction_id','payment_type', 'id')->where('order_id','=',$order_id)->where('payment_status','=','SUCCESS')->first();

    //       $orders    = DB::table('transaction')->select('transaction_id','payment_type', 'id')->where('order_id','=',$post_data['order_id'])->where(function ($query) {
    // $query->where('payment_status', '=', 'SUCCESS')
    //       ->orWhere('payment_status', '=', 'succeeded');
    //     })->first();
		
		
        $data = array('form_params' => $post_data);
		//print_r($data);exit;
        $checkout_details = $this->api->call_api($data,'api/cancel_booking',$method);


        //  dd($checkout_details);
        

        $booking_details = $checkout_details->response->booking_details;
        $outlets = $checkout_details->response->properties;
        $rooms_count = $checkout_details->response->rooms_count;
        $site_owner = getAppConfig()->site_owner;
        $outlet_id = $checkout_details->response->outlet_id; 
        $vendors = $checkout_details->response->vendors;

        
/*        
        if($checkout_details->response->httpCode == 200)
        {
            Session::flash('message-success', trans('messages.Order cancelled successfully'));
        }
        else
        {
            Session::flash('message-failure', trans($checkout_details->response->Message));
        }
        //  return Redirect::to('/orders')->send();
*/
        return view('front.'.$this->theme.'.cancel_booking')->with("properties",$outlets[0])
        ->with("booking_details",$booking_details[0])->with("site_owner",$site_owner)
        ->with("rooms_count",$rooms_count[0])->with("outlet_id",$outlet_id)->with("vendors",$vendors);
    }

    public function get_outlet_professionals($outlet_id)
    {
		$staffs = DB::select('SELECT * FROM outlet_staff_mapping
								JOIN vendors on vendors.id =  outlet_staff_mapping.staff_id
								WHERE outlet_staff_mapping.outlet_id =? ',array($outlet_id));
		//print_r($staffs);exit;
		return $staffs;
        //return $outlet_detail;
    }
	
	public function get_stylist(Request $data)
    {
		$post_data = $data->all();
		$staffs = DB::select('SELECT * FROM outlet_staff_mapping
								JOIN vendors on vendors.id =  outlet_staff_mapping.staff_id
                                JOIN outlet_infos on outlet_infos.id =  outlet_staff_mapping.outlet_id
								WHERE outlet_staff_mapping.outlet_id =? AND vendors.gender =? AND vendors.active_status=? AND outlet_infos.language_id=?',array($post_data['outlet_id'],$post_data['service_preference'],1,getCurrentLang()));
		return $staffs;
    }

	//$outlet_professionals = $this->get_outlet_professionals($outlet_id);
	public function get_available_slots(Request $data)
    {
		$post_data = $data->all();
		$selected_date = $post_data['sel_date'];
		$outlet_id = $post_data['outlet_id'];
		$outlet_professionals = $post_data['outlet_professionals'];
		$dayname_numeric = idate('w', strtotime($selected_date));
		$u_time = $this->getOpenTimings($outlet_id,$dayname_numeric);
		$avail_slots = '<h2>Sorry no slots available at this time</h2>';
        $count_avail_slots=0;
        $count_blocked_slots=0;
        $blocked_slots=array();
        $blocked_slots_html=array();
		if(count($u_time)>0)
		{
			$open_time = strtotime($u_time[0]->start_time);
			$close_time = strtotime($u_time[0]->end_time);
			$avail_slots = '<table class="responsive_table" cellpadding="0" cellspacing="2"><tbody><tr>';
							$j = 0;$k = 0;
							for($i=$open_time; $i<$close_time; $i+=1800) 
							{
								if((strtotime(date($selected_date." H:i:s",$i)) > strtotime(date("Y-m-d H:i:s"))))
								{
									$slot = date("H:i",$i);
									$check_professional_available = $this->check_professional_available($outlet_professionals,$selected_date,$slot);
                                    if($check_professional_available == 1)
									{
                                        $count_avail_slots++;
										if(($j%5) == 0)
										{
											$avail_slots .= "</tr><tr>";
										}
										$slot_html = date("g:i A", strtotime($slot));
										$avail_slots .= '<td data-slotcount='.$k.' data-status="A" data-text="'.$slot_html.'" onclick="appointment_time_select(this)" class=" time_select_slots time_select">'.$slot_html.'</td>';
										$j++; $k++;
									}else{
                                        //

                                                if(($j%5) == 0)
                                        {
                                            $avail_slots .= "</tr><tr>";
                                        }
                                        $slot_html = date("g:i A", strtotime($slot));
                                        $avail_slots .= '<td data-slotcount='.$k.' data-status="B" data-text="'.$slot_html.'"  class="blocked_time time_select_slots">'.$slot_html.'</td>';
                                        $j++; $k++;
                                        //

                                        $count_blocked_slots++;
                                        array_push($blocked_slots, $slot);
                                        $slot_for_html = date("g:i A", strtotime($slot));
                                        array_push($blocked_slots_html, $slot_for_html);


                                    }
								}
							}
			$avail_slots .='</tr></tbody></table>';
		}
        if($count_avail_slots==0){

        $avail_slots = '<h2>Sorry no slots available at this time</h2>';
        }

        $result = array("avail_slots" => $avail_slots , "blocked_slots" => $blocked_slots,"blocked_slots_html" => $blocked_slots_html);
             return json_encode($result,JSON_UNESCAPED_UNICODE);
             exit;
		//echo $avail_slots;exit;
    }
	
	public function check_professional_available($outlet_professionals,$date,$slot)
	{
		//echo 'SELECT from orders where service_professionals = '.$outlet_professionals.' AND appoinment_date ='.$date.' AND appoinment_time ='.$slot;exit;
		//DB::enableQueryLog();

		// $staffs_check = DB::select('SELECT from orders where service_professionals = ? AND appoinment_date =? AND appoinment_time =? AND 
  //           order_status NOT IN (1,12,10)',array($outlet_professionals,$date,$slot));
            $slot1=date('H:i:s', strtotime($slot));
            $staffs_check=DB::table('orders')
            ->select('*')
            ->where('service_professionals','=',$outlet_professionals)
            ->where('appoinment_date','=',$date)
            ->whereIn('order_status',[1,12,10])
          ->whereRaw("(appoinment_time = '".$slot1."' OR ('".$slot1."' >= appoinment_time and '".$slot1."' < appoinment_time_end ))")->get();
            //->where('appoinment_time','=',$slot1)
            //->where('appoinment_time_end','>',$slot1)->get();

            //v addded order_status NOT IN (4,11,8,20)',array($outlet_professionals,$date,$slot));
		  // order_status NOT IN (3,8,11,13)',array($outlet_professionals,$date,$slot));

//$queries = DB::getQueryLog();
//print_r($queries);exit;

		if(count($staffs_check) == 0 )
		{
			return 1;
		}
		return 0;
	}


    // my checked 
    public function crossCheckSelectedSlots(Request $data)
    {

            /*
                client side code 

                //
            var sel_date = $('#appointment_date').val();
            var sel_time = $("#appointment_time_hidden").val();
            var outlet_professionals = $("#outlet_professionals").val(); 
            var total_service_time=$('#total_service_time').val();
            var p_url = '/verify-booking-slots';

                $.ajax({
                url: p_url,
                headers: {'X-CSRF-TOKEN': token},
                data: {service_professionals:outlet_professionals,total_service_time:total_service_time,appointment_time:sel_time,appointment_date:sel_date},
                type: 'POST',
                datatype: 'JSON',
                success: function (resp) 
                {
                    if(resp!=0)
                    {
                      alert('Sorry, Refresh selected date and select other slots');
                      return;
                    }
                    
                },
                error:function(resp)
                {
                    console.log('out--'+resp); 
                    return false;
                }
            });

            //


            */
        
            $post_data = $data->all();
       

            //$outlet_id=$post_data['outlet_id'];
            $appoinment_date=$post_data['appointment_date'];
            $appoinment_time=$post_data['appointment_time'];
            $total_service_time=$post_data['total_service_time'];

            $service_professionals   = ($post_data['service_professionals'] != "")?$post_data['service_professionals']:0;


            // 

            $slot1=date('H:i:s', strtotime($appoinment_time));



            $endtime = date("H:i:s", strtotime("$slot1 + $total_service_time mins"));


            $slot2=date('H:i:s', strtotime($endtime));


//DB::enableQueryLog();




            $staffs_check=DB::table('orders')
            ->select('*')
            ->where('service_professionals','=',$service_professionals)
            ->where('appoinment_date','=',$appoinment_date)
            ->whereIn('order_status',[1,12,10])
          ->whereRaw("(('".$slot1."' >= appoinment_time and '".$slot1."' < appoinment_time_end ) OR ('".$slot2."' >= appoinment_time and '".$slot2."' < appoinment_time_end ))")->get();
# your laravel query builder goes here

//$laQuery = DB::getQueryLog();

//return $laQuery;

            if(count($staffs_check) == 0 )
            {
                return 0;
            }
            return 1;

            //

            // select * from orders where service_professionals=189 and appoinment_date = '2018-03-12' and order_status in(1,12,10) and (('13:30:00' >= appoinment_time and '13:30:00'  < appoinment_time_end ) OR ('13:30:00' >= appoinment_time and '13:30:00' < appoinment_time_end ))
            //

    
    }



}