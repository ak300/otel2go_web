<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Support\Facades\Text;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use Closure;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use URL;
use App\Model\notifications;
use App\Model\role;
use App\Model\roles_users;
use App\Model\permission_menu;
use App\Model\roles_permission;



class Permissions extends Controller
{
                /**
                 * Create a new controller instance.
                 *
                 * @return void
                 */
                public function __construct()
                {
            		$this->site_name = isset(getAppConfig()->site_name)?ucfirst(getAppConfig()->site_name):'';
                    App::setLocale('en');
                }

                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function index()
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }
                    else {
                        return view('vendors.notification.list');
                    }
                }



                public function role_create(Request $data)
                {

                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }

                    $post_data = $data->all();

                    //  print_r($post_data);exit;

                    $rules = [
                        'role_name'  => ['required', 'alpha', 'max:250', 'unique:user_roles,role_name'],
                        'nodes'      => ['required'],
                    ];   

                    $error = $result = array();

                    $messages = array(
                        'terms_condition.required_if' => 'The Terms and Condition field is required.',
                    );                    

                    $validator = app('validator')->make($post_data, $rules , $messages);

                    if ($validator->fails()) {
                        
                        foreach ($validator->errors()->messages() as $key => $value)
                        {
                            $error[] = is_array($value) ? implode(',', $value) : $value;
                        }
                        $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));
                    }
                    else {                            

                        $roleUser= new role;

                        $roleUser->role_name =  strtolower($_POST['role_name']);
                        $roleUser->created_by =  Auth::id();
                        $roleUser->created_date = date("Y-m-d H:i:s");
                        $roleUser->active_status = isset($_POST['status']) ? $_POST['status']: 0;
                        $roleUser->save();

                        $role_id=$roleUser->id;
                            
                        if($role_id){

                            if(isset($_POST['nodes'])){

                                    $nodes=$_POST['nodes'];   
                                }
                            else{
                                    $nodes;
                                }

                            if(count($nodes)>0) {

                                foreach ($nodes as $node) {
                                    # code...

                                        $permission_menu= permission_menu::find($node);
                                        $roles_permission= new roles_permission;
                                        $roles_permission->role_id =  $role_id;
                                        $roles_permission->menu_id =  $permission_menu->id;
                                        $roles_permission->menu_key = $permission_menu->menu_key;
                                        $roles_permission->created_at =  date("Y-m-d H:i:s");;
                                        $roles_permission->save();
                                    }
                                }
                            //  AFTER REDIRECT WE HAVE TO SHOW SUCCESS MESSAGE
                            Session::flash('message', trans('messages.Roles has been created successfully'));
                            $result = array("response" => array("httpCode" => 200, "status" => true, 
                                "Message" => trans("messages.Roles Added Successfully")));

                        }
                        else
                        {
                            $result = array("response" => array("httpCode" => 400, "status" => false, 
                                "Message" => trans("messages.Roles Failed to Create")));
                        }
                    }   //  else

                    return response()->json($result);

                }

                 public function role_update(Request $data)
                {

                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }

                    $post_data = $data->all();

                    //  print_r($post_data);exit;

                    $roleId= $_POST['role_id'];

                    $rules = [
                        'role_name'  => ['required', 'alpha', 'max:250', 'unique:user_roles,role_name,'.$roleId],
                        'nodes'      => ['required'],
                    ];   

                    $error = $result = array();

                    $messages = array(
                        'terms_condition.required_if' => 'The Terms and Condition field is required.',
                    );                    

                    $validator = app('validator')->make($post_data, $rules , $messages);

                    if ($validator->fails()) {
                        
                        foreach ($validator->errors()->messages() as $key => $value)
                        {
                            $error[] = is_array($value) ? implode(',', $value) : $value;
                        }
                        $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));
                    }
                    else {  
                   
                        
                         // echo ($roleId);

                        DB::table('roles_permission')->where('role_id', $roleId)->delete();

                        $roleUser = Role::find($roleId);
                        //  print_r($roleUser);exit;
                        $roleUser->role_name =  strtolower($_POST['role_name']);
                        $roleUser->created_by =  Auth::id();
                        $roleUser->updated_date = date("Y-m-d H:i:s");
                        $roleUser->active_status = isset($_POST['status']) ? $_POST['status']: 0;
                        $roleUser->save();

                        $role_id=$roleId;


                        
                        if($role_id){

                            if(isset($_POST['nodes'])){
                                $nodes=$_POST['nodes'];   
                            }
                            else{
                                $nodes;
                            }

                            if(count($nodes)>0) {

                                foreach ($nodes as $node) {
                                    # code...

                                        $permission_menu = permission_menu::find($node);
                                        $roles_permission = new roles_permission;
                                        $roles_permission->role_id =  $role_id;
                                        $roles_permission->menu_id =  $permission_menu->id;
                                        $roles_permission->menu_key = $permission_menu->menu_key;
                                        $roles_permission->created_at =  date("Y-m-d H:i:s");
                                        $roles_permission->save();

                                }
                        
                            }

                            //  AFTER REDIRECT WE HAVE TO SHOW SUCCESS MESSAGE
                            Session::flash('message', trans('messages.Roles has been Updated successfully'));
                            $result = array("response" => array("httpCode" => 200, "status" => true, 
                                "Message" => trans("messages.Roles Added Successfully")));

                        }
                        else
                            {
                                $result = array("response" => array("httpCode" => 400, "status" => false, 
                                    "Message" => trans("messages.Roles Failed to Create")));
                            }

                }

                return response()->json($result);
            }
               

}
