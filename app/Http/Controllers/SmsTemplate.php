<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\sms_templates;
// use App\Model\blog_infos;
use App\Http\Requests;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;

        class SmsTemplate extends Controller
        {

                public function __construct()
                {
                    $this->site_name = isset(getAppConfig()->site_name)?ucfirst(getAppConfig()->site_name):'';
                    SEOMeta::setTitle($this->site_name);
                    SEOMeta::setDescription($this->site_name);
                    SEOMeta::addKeyword($this->site_name);
                    OpenGraph::setTitle($this->site_name);
                    OpenGraph::setDescription($this->site_name);
                    OpenGraph::setUrl($this->site_name);
                    Twitter::setTitle($this->site_name);
                    Twitter::setSite('@'.$this->site_name);
                    App::setLocale('en');
                }
                /**
                 * Display a listing of the blogs.
                 *
                 * @return Response
                 */
                public function index()
                {
                    if (Auth::guest()){
                        return redirect()->guest('admin/login');
                    }else{

                        return view('admin.sms_template.list');
                    }
                }

                public function anyAjaxSmsTemplatelist(){
                    $sms_template=  DB::table('sms_templates')
                                    ->select('id','reference_name','subject','message')
                                    ->get();

                    return Datatables::of($sms_template)->addColumn('action', function ($sms_template) {
                            return '<div class="btn-group"><a href="'.URL::to("admin/sms_template/edit/".$sms_template->id).'" class="btn btn-xs btn-black" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown" style="display: none;">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("admin/sms_template/delete/".$sms_template->id).'" class="delete-'.$sms_template->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                    </ul>
                                </div><script type="text/javascript">
                                $( document ).ready(function() {
                                $(".delete-'.$sms_template->id.'").on("click", function(){
                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });});</script>';
                        })
                        ->make(true);
                }

                public function SMS_template_create($value='')
                {
                    if (Auth::guest())
                        {
                        return redirect()->guest('admin/login');
                    }else{
                        // if(!has_permission('admin/sms_template')){
                        //     return view('errors.404');
                        // }

                        return view('admin.sms_template.create');
                    }
                }
                 // Store SMS template
                        public function SMS_template_store(Request $request)
                        {
                            // print_r($request->all());exit;
                        $fields['reference_name'] = Input::get('reference_name');
                        $rules = array(
                        'reference_name' => 'required', 
                               );
                        $subject = Input::get('subject');
                        foreach ($subject  as $key => $value) {
                         $fields['subject'.$key] = $value;
                         $rules['subject'.'1'] = 'required|max:255';
                        }   
                        $message = Input::get('message');
                        foreach ($message  as $key => $value) {
                         $fields['message'.$key] = $value;
                         $rules['message'.'1'] = 'required|max:255';
                        }
                        $validate = Validator::make($fields, $rules);    
                               if ($validate->fails()) {
                                   return Redirect::back()->withErrors($validate)->withInput();
                               } else {
                               
                        $sms_template = new Sms_templates;
                        // $sms_template->_id = $inc_id;
                        // $sms_template->default_type = 1;
                        $sms_template->reference_name = Input::get('reference_name');
                        $sms_template->subject = $_POST['subject'][1];
                        $sms_template->message = $_POST['message'][1];

                           $sms_template->save();
                           Session::flash('message', 'SMS template has been submited successfully!');
                                   return Redirect::to('admin/sms_template');
                         
                        }
                    }
                    public function SMS_template_edit($id)
                        {
                        $sms_template = Sms_templates::find($id);
                        if(count($sms_template)==0){
                        Session::flash('message', 'SMS Template Not found');
                        return Redirect::to('admin/sms_template');
                        }
                        // show the edit form and pass the customer
                        return view('admin.sms_template.edit')->with('data', $sms_template);
                        }
                         // Update SMS template    
                    public function SMS_template_update(Request $data,$id)
                        {

                        $fields['reference_name'] = Input::get('reference_name');
                        $rules = array(
                        'reference_name' => 'required', 
                               );
                        $subject = Input::get('subject');
                        foreach ($subject  as $key => $value) {
                         $fields['subject'.$key] = $value;
                         $rules['subject'.'1'] = 'required|max:255';
                        }   
                        $message = Input::get('message');
                        foreach ($message  as $key => $value) {
                         $fields['message'.$key] = $value;
                         $rules['message'.'1'] = 'required|max:255';
                        }
                        $validate = Validator::make($fields, $rules);    
                               if ($validate->fails()) {
                                   return Redirect::back()->withErrors($validate)->withInput();
                               } else {
                        $sms_template = Sms_templates::find($id);
                        $sms_template->reference_name  = Input::get('reference_name');
                        $sms_template->subject  = $_POST['subject'][1];
                        $sms_template->message  = $_POST['message'][1];
                        $sms_template->save();

                        // echo "lofsdf";exit;
                        // redirect
                        Session::flash('message', 'SMS template has been updated successfully!');
                        return Redirect::to('admin/sms_template');
                           }
                         }

                        public function SMS_template_destroy($id)
                            {
                             if (Auth::guest())
                                {
                                    return redirect()->guest('admin/login');
                                }                                          
                            $sms_template = Sms_templates::find($id);
                            
                            $sms_template->delete();
                            Session::flash('message', trans('messages.SMS template has been deleted successfully!'));
                            return Redirect::to('admin/sms_template');
                            }

             }
                                     