<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;
use App\Model\vendors;
use App\Model\subscription_plan as Subscriptionplan;
use App\Model\subscription_plan_infos as Subscriptioninfo;
use App\Model\subscription_packages as Subscriptionpackages;
use App\Model\subscription_transaction as Subscriptiontransaction;
use App\Model\subscriptions as Subscriptionrenewal;

class SubscriptionPlans extends Controller
{
     public function __construct(){

     }

     /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyAjaxSubscriptionPlans()
    { 
        $subscription_plans=DB::table('subscription_plan')
         ->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
        ->select('subscription_plan_infos.subscription_plan_name','subscription_plan.id','subscription_plan.created_at','subscription_plan.subscription_plan_status','subscription_plan.subscription_plan_price','subscription_plan.subscription_plan_duration','subscription_plan.total_properties','subscription_plan.total_rooms','subscription_plan.total_users')
        ->orderBy('subscription_plan_infos.subscription_plan_name', 'asc')
        ->get();
       // echo "<pre>";print_r($subscription_plans);die;
        return Datatables::of($subscription_plans)->addColumn('action', function ($subscription_plans) {
                return '<div class="btn-group"><a href="'.URL::to("admin/subscription_packages/edit/".$subscription_plans->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                        <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu xs pull-right" role="menu">
                        <li>
                          <a href="'.URL::to("admin/subscription_packages/view/".$subscription_plans->id).'"  title="'.trans("messages.View").'"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;'.@trans("messages.View").'</a>
                        </li>
                        </ul>
                    </div><script type="text/javascript">
                    $( document ).ready(function() {
                    $(".delete-'.$subscription_plans->id.'").on("click", function(){
                         return confirm("'.trans("messages.Are you sure want to delete?").'");
                    });});</script>';
            })
            ->editColumn('subscription_plan_duration', function ($subscription_plans) {
                    $data = '-';
                    if($subscription_plans->subscription_plan_duration != null)
                    {
                      $data = $subscription_plans->subscription_plan_duration.' days';
                    }
                  return $data;
            })
            ->editColumn('subscription_plan_status', function ($subscription_plans) {
                    $data = '-';
                      if($subscription_plans->subscription_plan_status == 0){ 
                        $data = '<span class="label label-warning">'.trans('messages.Inactive').'</span>';
                       }elseif($subscription_plans->subscription_plan_status == 1){ 
                        $data = '<span class="label label-success">'.trans('messages.Active').'</span>';
                       }
                  return $data;
            })
            ->make(true);
    }

    /**
     * Show the Subscription List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        if (Auth::guest()) {
                return redirect()->guest('admin/login');
        } else { 
            return view('admin.subscription_plans.list');
        }
        
    }

    /**
     * Subscription Details For Editing.
     *
     */
    public function edit($id){  
       if (Auth::guest()) {
                return redirect()->guest('admin/login');
        } else { 
            $subscription_details = Subscriptionplan::find($id);
            if(count($subscription_details) > 0){
                 $subscription_plans = DB::table('subscription_plan')
                                     ->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
                                    ->where('subscription_plan.id','=',$id)->get();
                  $subscription_plans = iterator_to_array($subscription_plans);
                  $packages = DB::table('package_modules')->get();
                  $packages = iterator_to_array($packages);

                   //Take already selected packages
                  $already_packages = Subscriptionpackages::where('plan_id','=',$id)->where('status','=',1)->select('package_id')->get()->toArray();

                  $previous_packages = array();
                  foreach($already_packages as $key => $val){
                      $previous_packages[]= $val['package_id'];
                  }
                  return view('admin.subscription_plans.edit')->with('data',$subscription_plans)->with('packages',$packages)->with('old_packages',$previous_packages);
            } else {
                Session::flash('message', trans('messages.Invalid Subscription Plan Details!'));
                return Redirect::to('admin/subscription_packages');
            }   
        }
    }

   /**
     * Updating Subscription Details.
     **/
    public function update(Request $data,$id){
      if (Auth::guest())
      {
          return redirect()->guest('admin/login');
      } else {
         $validation = Validator::make($data->all(),array(
            'plan_name' => 'required|alpha',
            'description' => 'required',
            'price' => 'required|integer',
            'properties' => 'required|integer',
            'rooms' => 'required|integer',
            'user_limit' => 'required|integer',
            'discount_price' => 'required|integer',
            'duration' => 'required|integer',
            'packages' => 'required',
        ));   
        
        // process the validation
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } else {
          
            //Save Subscription 
            $subscription_plans = SubscriptionPlan::find($id);
            $subscription_plans->subscription_plan_status = (int)Input::get('status');
            $subscription_plans->subscription_plan_price = Input::get('price');
            $subscription_plans->subscription_plan_duration = Input::get('duration');
            $subscription_plans->total_properties = Input::get('properties');
            $subscription_plans->total_rooms = Input::get('rooms');
            $subscription_plans->total_users = Input::get('user_limit');
            $subscription_plans->description = Input::get('description');
            $subscription_plans->subscription_discount_price = Input::get('discount_price');
            $subscription_plans->save();

            $plan_name = Input::get('plan_name');
            $subscription_info = Subscriptioninfo::where('subscription_plan_id','=',$id)->update(array('subscription_plan_name' => $plan_name));

            $all_packages = Input::get('packages');

            //Active all packages that is checked now
            $active_package = Subscriptionpackages::where('plan_id',$id)->whereIn('package_id',$all_packages)->update(array('status' => 1));
            
            //Inactive all packages that is unchecked now but previously selected
            $Inactive_package = Subscriptionpackages::where('plan_id',$id)->whereNotIn('package_id',$all_packages)->update(array('status' => 0));

            //Take already selected packages
            $already_packages = Subscriptionpackages::where('plan_id','=',$id)->where('status','=',1)->select('package_id')->get()->toArray();

            $previous_packages = array();
            foreach($already_packages as $key => $val){
                $previous_packages[]= $val['package_id'];
            }

            //Saving the newly added package
            foreach($all_packages as $key => $value){
              if(!in_array($value,$previous_packages)){
                $subscription = new Subscriptionpackages();
                $subscription->plan_id = $id;
                $subscription->package_id = $value;
                $subscription->status = 1;
                $subscription->save();
              }
            }

            Session::flash('message', trans('messages.Subscription Details has been updated successfully!'));
            return Redirect::to('admin/subscription_packages');
        }
      }
    }

     /**
     * Show Subscription Details.
     *
     */
    public function show($id){
      if (Auth::guest())
      {
          return redirect()->guest('admin/login');
      } else {
        $subscription_details = Subscriptionplan::find($id);
        if(count($subscription_details) > 0){
            $subscription_plan = DB::table('subscription_plan')->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
            ->where('subscription_plan.id','=',$id)
            ->select('subscription_plan.id','subscription_plan.created_at','subscription_plan.updated_at','subscription_plan.subscription_plan_status','subscription_plan.subscription_plan_price','subscription_plan.subscription_plan_duration','subscription_plan.description','subscription_plan.subscription_discount_price','subscription_plan_infos.subscription_plan_name','subscription_plan.total_properties','subscription_plan.total_rooms','subscription_plan.total_users')->get()->toArray();

            //Packages for the current plan
            $subscription_packages = Subscriptionpackages::where('plan_id','=',$id)->where('status','=',1)->select('package_id')->get()->toArray();

            //Total Packages
            $packages = DB::table('package_modules')->get();
            $packages = iterator_to_array($packages);

            return view('admin.subscription_plans.show')->with('data',$subscription_plan)->with('packages',$subscription_packages)->with('allpackages',$packages);
       } else {
            Session::flash('message', trans('messages.Invalid Subscription Plan Details!'));
            return Redirect::to('admin/subscription_packages');
       }
      }
    }

    /*
    *List of subscription transaction 
    */
    public function SubscriptionTransaction(){

        return view('admin.subscription_plans.subscription_transactions.list');
    }

    /*
    *List of subscription transaction ajax
    */
    public function anyAjaxSubscriptionTransactions(){
         $transactions = DB::table('subscription_transaction')->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_transaction.plan_id')->leftjoin('vendors_infos','vendors_infos.id','=','subscription_transaction.vendor_id')
        ->select('subscription_transaction.*','subscription_plan_infos.subscription_plan_name as plan_name','vendors_infos.vendor_name as vendor_name')
        ->orderBy('subscription_transaction.id','desc')
        ->get(); 

        return Datatables::of($transactions)->addColumn('action', function ($transactions) {
          if (has_permission('admin/subscription_transactions/view')) {
                return '<div class="btn-group"> <a href="'.URL::to("admin/subscription_transactions/view/".$transactions->id).'"  title="'.trans("messages.View").'"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;'.@trans("messages.View").'</a>
                    </div>';
                  }
            })
            ->editColumn('payment_status', function ($transactions) {
                    $data = '-';
                      if($transactions->payment_status == 0){ 
                        $data = '<span class="label label-danger">'.trans('messages.Incompleted').'</span>';
                       }elseif($transactions->payment_status == 1){ 
                        $data = '<span class="label label-success">'.trans('messages.Completed').'</span>';
                       }
                  return $data;
            })
            ->make(true);
    }

    /*
    *view of subscription transaction 
    */
    public function viewSubscriptionTransaction($id){
      $transactions = DB::table('subscription_transaction')
                  ->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_transaction.plan_id')
                  ->leftjoin('vendors_infos','vendors_infos.id','=','subscription_transaction.vendor_id')
                  ->where('subscription_transaction.id','=',$id)
                  ->select('subscription_transaction.*','subscription_plan_infos.subscription_plan_name as plan_name','vendors_infos.vendor_name as vendor_name')
                  ->orderBy('subscription_transaction.id','desc')
                  ->get()->toArray(); 
        return view('admin.subscription_plans.subscription_transactions.show')->with('data',
        $transactions);          
    }

}
