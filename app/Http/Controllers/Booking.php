<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\booking_user_details;
use App\Model\booking_detail;
use App\Model\booked_room_details;
use App\Model\payment;
use App\Model\admin_customers;
use App\Model\booking_charge;
use App\Model\booking_infos;
use App\Model\rooms_infos;
use App\Model\vendors;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;


class Booking extends Controller
{

    const USERS_SIGNUP_EMAIL_TEMPLATE = 1;
    const USERS_WELCOME_EMAIL_TEMPLATE = 3;
    const USERS_FORGOT_PASSWORD_EMAIL_TEMPLATE = 6;
    const USER_CHANGE_PASSWORD_EMAIL_TEMPLATE = 13;
    const OTP_EMAIL_TEMPLATE = 14;
    const ORDER_MAIL_TEMPLATE = 5;
    const ORDER_MAIL_VENDOR_TEMPLATE = 16;
    const RETURN_STATUS_CUSTOMER_EMAIL_TEMPLATE = 17;
    const ORDER_STATUS_UPDATE_USER = 18;    
                
                public function new_booking()
                {
//  dd('Ak');
                    if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        } else{

                        $room_type = DB::table('room_type')
                                    ->select('room_type.*','room_type_infos.*')
                                    ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                                    ->where('default_status', 1)
                                    ->where('property_id',Session::get('property_id'))
                                    ->orderBy('room_type', 'asc')
                                    ->get();

                        $room = DB::table('rooms')
                                    ->select('rooms.*','rooms_infos.*')
                                    ->leftJoin('rooms_infos','rooms_infos.id','=','rooms.id')
                                    ->where('rooms.property_id',Session::get('property_id'))
                                    ->where('default_status',1)
                                    ->orderBy('room_name','asc')
                                    ->get();
                        //  print_r($room); exit;

                        $order_status = DB::table('booking_status')
                                            ->select('id','name')->orderBy('id', 'asc')->get();                                    

                        return view('vendors.booking.create')->with('room_type',$room_type)
                                                             ->with('room',$room)
                                                             ->with('order_status',$order_status);
                    }
                }


                public function booking_user(Request $data)
                {
                        // echo "<pre>";

                //   dd($data->all());

                    $fields['booking_type']    = Input::get('booking_type');
                    $fields['booking_source']  = Input::get('booking_source');
                    $fields['guest_check']     = Input::get('guest_check');
                    $fields['adult_count']     = Input::get('adult_count');
                    $fields['child_count']     = Input::get('child_count');
                    $fields['description']     = Input::get('description');                    
                    //  $fields['image']           = Input::file('image');
                    //  $fields['guest_name']      = Input::get('guest_name');
                    //  $fields['mobile']          = Input::get('mobile');
                    $fields['email']           = Input::get('email');
                    //  $fields['address']         = Input::get('address');

                    $fields['check_in']        = Input::get('check_in');
                    $fields['check_out']       = Input::get('check_out');
                    $fields['notes']           = Input::get('notes');

                    $rules = array(
                        
                        'booking_type'   => 'required',
                        'booking_source' => 'required',
                        //  'guest_check'    => 'required',
                        'adult_count'    => 'required',
                        'child_count'    => 'required',
                        'email'          => 'required',
                        // 'guest_name'     => 'required',
                        // 'mobile'         => 'required|numeric',
                        // 'address'        => 'required',
                        //  'city'           => 'required',
                        'check_in'       =>  'required',
                        'check_out'      =>  'required',
                        //  'notes'          =>  'required',    //  IT DOESN'T SHOW BEFORE CHECKIN & CHECKOUT DATE
                    );
  
                    $country_short = Input::get('country_short');

                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput()->with('country_short',$country_short);
                    } 
                    else {
                        try{

                    $vendor_id = Session::get('vendor_id');

                    $property_id = Session::get('property_id');

                    $rooms = $_POST['room'];

                    $free_room_name = rooms_infos::where('id',$rooms)->pluck('room_name')->toArray();
                    
                    $roo_nm = implode(',', $free_room_name);                    

                    //  $user_id = Input::get('user_id');

                    $mail_id = Input::get('email');

                    //  print_r($user_id); exit;
                    //  dd($data->all());

                    $user_query = Admin_customers::where('email',$mail_id)->first();

                    //  dd($user_query);

                    if($user_query == "")
                    {
                        Session::flash('invalid-email','These Credentials do not match with our records');
                        return Redirect::back()->withInput();
                    }

                    // print_r($user_query->id);
                    // exit;

                     // echo "<pre>";

                     // print_r($user_query); exit;

                     /*

                    if(count($data)>0){
                        $data->firstname = Input::get('guest_name');
                        $data->email = Input::get('email');
                        $data->mobile_number = Input::get('mobile');
                        $data->country_code = Input::get('country_code');
                        $data->address = Input::get('address');
                        $data->save();
                    }

                    else
                    {
                                         
                        $customers = new Admin_customers;

                        $customers->firstname = Input::get('guest_name');
                        $customers->email = Input::get('email');
                        $customers->mobile_number = Input::get('mobile');
                        $customers->country_code = Input::get('country_code');
                        $customers->address = Input::get('address');
                        $customers->status = 1;
                        $customers->is_verified = 1;
                        $customers->save();
                    }

                    */

//  print_r($data->id); exit;
//  print_r($customers->id);exit;

                    //  DUE TO DATEPICKER CHANGES , ORDINARY DATE MAKES ERROR SO IT IS CHANGED WITH STRTOTIME

                    $d1 = date(Input::get('check_in'));

                    $d2 = date(Input::get('check_out'));

                    $d1 = str_replace('/', '-', $d1);

                    $d2 = str_replace('/', '-', $d2);

                    $cout_date = date('Y-m-d',strtotime($d2. ' - 1 day'));

                    $d1 = date('Y-m-d', strtotime($d1));

                    $d2 = date('Y-m-d', strtotime($d2));                    

                    $cin = strtotime($d1);

                    $cout =strtotime($cout_date);                    

                    // print_r($cout);
                    // exit;

//                      foreach ($rooms as $key => $value) {

                    $booking_random_id = getRandomBookingNumber(6);

                    //  $invoice_id = "INV".$booking_random_id;

                    //  $this->downloadinvoice($invoice_id,Session::get('vendor_id'));

                    // dd($invoice_id);

                            $booking_detail = new booking_detail;

                            $booking_detail->booking_status =  Input::get('booking_type');
                            $booking_detail->booking_source = Input::get('booking_source');
                            $booking_detail->adult_count = Input::get('adult_count');
                            $booking_detail->child_count = Input::get('child_count');
                            $booking_detail->check_in_date = $d1;
                            $booking_detail->check_out_date = $d2;
                            //  $booking_detail->check_out_date = $cout_date;
                            $booking_detail->no_of_days = Input::get('nights');
                            $booking_detail->room_type = Input::get('room_type_id');
                            $booking_detail->rooms = $rooms;
                            $booking_detail->room_name = $roo_nm;
                            $booking_detail->notes = Input::get('notes');
                            $booking_detail->booking_random_id = $booking_random_id;
                            $booking_detail->created_date = date('Y-m-d H:i:s');
                            $booking_detail->room_booked_date = date('Y-m-d');
                            $booking_detail->charges = Input::get('price');
                            $booking_detail->original_price = Input::get('price');
                            $booking_detail->room_type_cost = Input::get('one_night_price');
                            $booking_detail->payments = 0;
                            $booking_detail->booking_type = 2;
                            $booking_detail->room_normal_price = Input::get('one_night_normal_price'); 

                            //  $booking_detail->date = date("Y-m-d", $seconds);
                            // if($user_id!="")
                            // {
                            //     $booking_detail->customer_id = $data->id;    
                            // }
                            // else
                            // {
                            //     $booking_detail->customer_id = $customers->id; 
                            // }

                            $booking_detail->customer_id = $user_query->id;
                            
                            $booking_detail->created_by = $vendor_id;
                            $booking_detail->outlet_id = $property_id;
                            $booking_detail->vendor_id = $vendor_id;

                            $booking_detail->save(); 

                            //  STORING ROOMS ID IN BOOKING INFOS TABLE

                            $infos = new booking_infos;
                            $infos->room_id = $rooms;
                            $infos->booking_id = $booking_detail->id;
                            $infos->save();


                            $charges = new booking_charge;

                            //  $charges->room_id = $rooms;
                            $charges->room_type = Input::get('room_type_id');
                            $charges->price = Input::get('price');
                            $charges->charge_type = 1;
                            $charges->booking_id = $booking_detail->id;
                            $charges->customer_id = $user_query->id;
                            $charges->created_by =  $vendor_id;
                            $charges->created_date = date('Y-m-d H:i:s');
                            $charges->notes = Input::get('notes');
                            $charges->active_status = 1;
                            $charges->property_id = $property_id;
                            $charges->save();
    

                            /*
                                $booking_ud = new booking_user_details;

                                $booking_ud->guest_name =  Input::get('guest_name');
                                $booking_ud->email = Input::get('email');
                                $booking_ud->customer_type = Input::get('customer_type');
                                $booking_ud->country_code = Input::get('country_code');
                                $booking_ud->mobile_number = Input::get('mobile');
                                $booking_ud->created_date = date("Y-m-d H:i:s");
                                $booking_ud->created_by = Session::get('vendor_id');
                                $booking_ud->address = Input::get('address');
                                $booking_ud->city_id = Input::get('city');
                                $booking_ud->postal_code = Input::get('postal_code');
                                $booking_ud->booking_id = $booking_detail->id;
                                $booking_ud->vendor_id = Session::get('vendor_id');
                                $booking_ud->outlet_id = Session::get('property_id');

                                $booking_ud->save();

                            */

                    for ($seconds=$cin; $seconds<=$cout; $seconds+=86400)
                        {
                            $brd = new booked_room_details;

                            $brd->check_in_date  = $d1;
                            //  $brd->check_out_date = Input::get('check_out');
                            $brd->check_out_date = $cout_date;
                            $brd->no_of_days     = Input::get('nights');
                            $brd->room_id        = $rooms;
                            $brd->booking_id     = $booking_detail->id;
                            $brd->date = date("Y-m-d", $seconds);
                            $brd->availablity_status = 1;
                            $brd->booking_status = Input::get('booking_type');
                            $brd->customer_id = $user_query->id;

                            $brd->save();
                        }

                    //  }   FOREACH

                    $users = DB::table('rooms')
                                        //  ->whereIn('rooms.id', $rooms)
                                        ->where('rooms.id',$rooms)
                                        ->update([
                                            'room_clean_status' => 0,
                                            'availability_status' => 1,
                                            'check_in_date' => $d1,
                                            'check_out_date' => $d2,
                                            'customer_id' => $user_query->id,
                                        ]);

                            Session::flash('message', trans('messages.Booking has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('vendors/dashboard');                    
                    }
                }
                           /**
                             * Process datatables ajax request.
                             *
                             * @return \Illuminate\Http\JsonResponse
                             */
                public function anyAjaxPaymentmanager()
                            {
         
                                $vendor_id = Session::get('vendor_id');
                                

                                if(Session::get('vendor_type') == 2 )
                                    {
                                        $vendor_id = Session::get('created_vendor_id');
                                    } 

                                $payment = DB::table('payments')
                                                ->select('payments.id as payment_id','payments.active_status as payment_status','payments.*','admin_customers.*')
                                                ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                                //  ->where('payments.booking_id','=',$id)
                                                ->leftJoin('admin_customers','admin_customers.id','payments.customer_id')
                                                ->where('payments.vendor_id','=',$vendor_id)
                                                ->orderBy('payments.id', 'desc')
                                                ->get();
                                  //    print_r($payment);exit;

                                return Datatables::of($payment) /*->addColumn('action', function ($payment) {
                                    if(has_staff_permission('vendor/edit_outlet_manager')){
                                        $html='<div class="btn-group"><a href="'.URL::to("booking/editPayment/".$payment->payment_id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                                    <li><a href="'.URL::to("booking/deletePayment/".$payment->payment_id).'" class="delete-'.$payment->payment_id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                                    </ul>
                                                </div><script type="text/javascript">
                                                $( document ).ready(function() {
                                                $(".delete-'.$payment->payment_id.'").on("click", function(){
                                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                                });});</script>';
                                    
                                        return $html;
                                        }
                                    })
                                    */
                            ->addColumn('payment_status', function ($payment) {
                                if($payment->payment_status==0):
                                    $data = '<span class="label label-warning">'.trans("messages.Inactive").'</span>';
                                elseif($payment->payment_status==1):
                                    $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                                elseif($payment->payment_status==2):
                                    $data = '<span class="label label-danger">'.trans("messages.Delete").'</span>';
                                endif;
                                return $data;
                            })
                            ->addColumn('paid_type', function ($payment) {
                                $data = '-';
                                if($payment->paid_type==1):
                                     $data = trans("messages.Cash Payment");
                                endif;
                                return $data;                                                              
                                })                                
                            ->make(true);
                            }
                public function showBooking($id)
                {
                //  dd($id);
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        /*
                            if(!has_staff_permission('vendor/outlet_details'))
                            {
                            return view('errors.405');
                            }
                        */
                        else
                        {
                           
                        $vendor_id = Session::get('vendor_id');
                      
                        if (Session::get('vendor_type') == 2 ){

                            $vendor_id = Session::get('created_vendor_id');

                        }                            
                        //  $property_id = Session::get('property_id');                        
                        $vendor_orders = DB::table('booking_details')
                                            ->select('admin_customers.id as cust_id','booking_details.charges as total_charges','booking_details.payments as total_payments','booking_details.id as bid','booking_details.*','admin_customers.*','booking_status.*','booking_info.*')
                                            ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                            ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                            ->leftJoin('rooms','rooms.id','booking_info.room_id')
                                            ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                                            ->where('booking_details.vendor_id',$vendor_id)
                                            //  ->where('booking_details.outlet_id',Session::get('property_id'))
                                            ->where('booking_details.id','=',$id)
                                            ->first();
                        //  dd($vendor_orders);

                        $payment = DB::table('payments')
                                        ->select('payments.id')
                                        ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                        ->where('payments.booking_id','=',$id)
                                        //  ->paginate(1)
                                        ->get();


                        $payment_list = DB::table('payments')
                                        ->select('payments.id as payment_id','payments.active_status as payment_status','payments.*','admin_customers.*')
                                        ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                        ->where('payments.booking_id','=',$id)
                                        ->leftJoin('admin_customers','admin_customers.id','payments.customer_id')
                                        ->where('payments.vendor_id','=',$vendor_id)
                                        ->orderBy('payments.id', 'asc')
                                        ->get(); 

                        //  Multiple Rooms Makes the Duplicate Issues , So Booking_details.rooms are used

                        $charges_list = DB::table('booking_charges')
                                                    ->select('booking_charges.id as bc_id','booking_charges.notes as bc_notes','booking_charges.active_status as booking_status','booking_charges.*','booking_details.*','admin_customers.*'/*, 'rooms_infos.room_name', 'booking_info.*'*/)
                                                    ->leftJoin('booking_details','booking_details.id','booking_charges.booking_id')
                                                    //  ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                                    ->leftJoin('admin_customers','admin_customers.id','booking_charges.customer_id')
                                                    //  ->leftJoin('rooms_infos','rooms_infos.id','booking_info.room_id')
                                                    ->where('booking_charges.booking_id','=',$id)
                                                    ->orderBy('booking_charges.id','asc')
                                                    ->get();
                            //  dd($charges_list);

                        $charges_count = DB::table('booking_charges')
                                                    ->select(DB::raw('sum(price) as price'))
                                                    ->leftJoin('booking_details','booking_details.id','booking_charges.booking_id')
                                                    ->where('booking_charges.booking_id','=',$id)
                                                    ->get();

                        $payment_count = DB::table('payments')
                                                   ->select(DB::raw('sum(paid_amount) as paid_amount'))
                                                   ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                                   ->where('payments.booking_id','=',$id)
                                                   ->get();
                        $order_status = DB::table('booking_status')
                                                   ->select('id','name')
                                                   ->orderBy('id', 'asc')
                                                   ->get();

                        $order_history = DB::select('SELECT ol.order_comments,ol.booking_status,log_time,booking_status.name as status_name,booking_status.color_code as color_code
                            FROM orders_log ol
                            left join booking_status on booking_status.id = ol.booking_status
                            where ol.order_id = ? ORDER BY ol.id',array($id));

                         // dd($order_history);

                        return view('vendors.booking.show')->with('data', $vendor_orders)
                                                           ->with('payment',$payment)
                                                           ->with('payment_list',$payment_list)
                                                           ->with('charges_list',$charges_list)
                                                           ->with('charges_count',$charges_count[0])
                                                           ->with('payment_count',$payment_count[0])
                                                           ->with('order_status',$order_status)
                                                           ->with('order_history',$order_history);
                        }                    
                }

                public function showAllBooking($id)
                {
                //  dd($id);
                        if(Auth::guest())
                        {
                            return redirect()->guest('admin/login');
                        } 
                        if(!has_permission('vendors/orders/index'))
                        {
                            return view('errors.404');
                        } 
                        else
                        {

                        $vendor_orders = DB::table('booking_details')
                                            ->select('admin_customers.id as cust_id','booking_details.id as bid','booking_details.*','admin_customers.*','booking_status.*','booking_info.*')
                                            ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                            ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                            ->leftJoin('rooms','rooms.id','booking_info.room_id')
                                            ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                                            //  ->where('booking_details.vendor_id',$vendor_id)
                                            //  ->where('booking_details.outlet_id',Session::get('property_id'))
                                            ->where('booking_details.id','=',$id)
                                            ->first();
                        //  dd($vendor_orders);

                        $vendor_details = DB::table('outlets')
                                            ->leftJoin('booking_details','booking_details.outlet_id','=','outlets.id')
                                            ->leftJoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                            ->leftJoin('vendors_view','vendors_view.id','=','outlets.vendor_id')
                                            ->select('outlet_infos.outlet_name','vendors_view.first_name',
                                                'vendors_view.last_name')
                                            ->where('booking_details.id','=',$id)
                                            ->get();

                        //  dd($vendor_details);


                        $payment = DB::table('payments')
                                        ->select('payments.id')
                                        ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                        ->where('payments.booking_id','=',$id)
                                        //  ->paginate(1)
                                        ->get();


                        $payment_list = DB::table('payments')
                                        ->select('payments.id as payment_id','payments.active_status as payment_status','payments.*','admin_customers.*')
                                        ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                        ->where('payments.booking_id','=',$id)
                                        ->leftJoin('admin_customers','admin_customers.id','payments.customer_id')
                                        //  ->where('payments.vendor_id','=',$vendor_id)
                                        ->orderBy('payments.id', 'asc')
                                        ->get(); 

                        $charges_list = DB::table('booking_charges')
                                                    ->select('booking_charges.id as bc_id','booking_charges.notes as bc_notes','booking_charges.active_status as booking_status','booking_charges.*','booking_details.*','admin_customers.*','rooms_infos.room_name','booking_info.*')
                                                    ->leftJoin('booking_details','booking_details.id','booking_charges.booking_id')
                                                    ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                                    ->leftJoin('admin_customers','admin_customers.id','booking_charges.customer_id')
                                                    ->leftJoin('rooms_infos','rooms_infos.id','booking_info.room_id')
                                                    ->where('booking_charges.booking_id','=',$id)
                                                    ->orderBy('booking_charges.id','asc')
                                                    ->get();

                        $charges_count = DB::table('booking_charges')
                                                    ->select(DB::raw('sum(price) as price'))
                                                    ->leftJoin('booking_details','booking_details.id','booking_charges.booking_id')
                                                    ->where('booking_charges.booking_id','=',$id)
                                                    ->get();

                        $payment_count = DB::table('payments')
                                                   ->select(DB::raw('sum(paid_amount) as paid_amount'))
                                                   ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                                   ->where('payments.booking_id','=',$id)
                                                   ->get();
                        $order_status = DB::table('booking_status')
                                                   ->select('id','name')
                                                   ->orderBy('id', 'asc')
                                                   ->get();

                        $order_history = DB::select('SELECT ol.order_comments,ol.booking_status,log_time,booking_status.name as status_name,booking_status.color_code as color_code
                            FROM orders_log ol
                            left join booking_status on booking_status.id = ol.booking_status
                            where ol.order_id = ? ORDER BY ol.id',array($id));

                         // dd($order_history);

                        return view('admin.bookings.show')->with('data', $vendor_orders)
                                                           ->with('payment',$payment)
                                                           ->with('payment_list',$payment_list)
                                                           ->with('charges_list',$charges_list)
                                                           ->with('charges_count',$charges_count[0])
                                                           ->with('payment_count',$payment_count[0])
                                                           ->with('order_status',$order_status)
                                                           ->with('order_history',$order_history)
                                                           ->with('vendor_details',$vendor_details[0]);
                        }                    
                }                

                public function storePayment()
                {
                        $bid = $_POST['booking_id'];

                   //   print_r($_POST); exit;

                        //  print_r($booking_details); exit;

                        $payment= new payment;

                        $vendor_id = Session::get('vendor_id');

                        $payment->booking_id =  $_POST['booking_id'];
                        $payment->customer_id = $_POST['customer_name'];
                        $payment->created_date = date("Y-m-d H:i:s");
                        $payment->active_status =  1;
                        $payment->created_by =  $vendor_id;
                        $payment->paid_type = $_POST['paid_type'];
                        $payment->paid_amount = $_POST['amount'];
                        $payment->description = $_POST['description'];
                        $payment->property_id = Session::get('property_id');
                        $payment->vendor_id = $vendor_id;
                        $payment->room_id = $_POST['room_id'];
                        $payment->room_type = $_POST['room_type_id'];                        
                        $payment->save();

                        $booking_details = DB::table('booking_details')
                                            ->where('id',$bid)
                                            ->increment('payments', $_POST['amount']);

                        $vendor_bal = DB::table('booking_details')
                                            ->select(DB::Raw('sum(payments) as current_balance'))
                                            ->where('booking_details.vendor_id','=',$vendor_id)
                                            ->get();
                                        
                        $vendor = vendors::find($vendor_id);

                        //  dd($vendor);

                        $vendor->current_balance = $vendor_bal[0]->current_balance;
                        $vendor->save();                        

                        echo 1; exit;
                }

                public function storeCharges()
                {
                        $bid = $_POST['booking_id'];

                       //    print_r($_POST); exit;

                        
                        $charges = new booking_charge;

                        $vendor_id = Session::get('vendor_id');

                        $charges->booking_id =  $_POST['booking_id'];
                        $charges->customer_id = $_POST['customer_name_charges'];
                        $charges->created_date = date("Y-m-d H:i:s");
                        $charges->active_status =  1;
                        $charges->created_by =  $vendor_id;
                        $charges->charge_type = $_POST['charge_type'];
                        $charges->property_id = Session::get('property_id');
                        $charges->price = $_POST['charges_amount'];
                        $charges->notes = $_POST['notes'];
                        $charges->room_id = $_POST['room_id'];
                        $charges->room_type = $_POST['room_type_id'];
                        $charges->save();

                        $booking_details = DB::table('booking_details')
                                             ->where('id',$bid)
                                             ->increment('charges', $_POST['charges_amount']);                        

                        echo 1; exit;
                }
                public function payment_destroy($id)
                {
                    //  dd($id);
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }
                        $data = Payment::find($id);
                        if(!count($data))
                        {
                            Session::flash('message', 'Invalid Payment Details'); 
                            return Redirect::to('vendor/editbooking/19');    
                        }
                        $data->active_status = 2 ;
                        $data->save();
                        Session::flash('message', trans('messages.Payment has been deleted successfully!'));
                        return Redirect::to('vendor/editbooking/19');

                }

                public function editBooking($id)
                {
                    //  dd($id);
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        /*
                            if(!has_staff_permission('vendor/outlet_details'))
                            {
                            return view('errors.405');
                            }
                        */
                        else
                        {
                           
                        $vendor_id = Session::get('vendor_id');
                      
                        if (Session::get('vendor_type') == 2 ){

                            $vendor_id = Session::get('created_vendor_id');

                        }                            
                        $property_id = Session::get('property_id');                        
                        $vendor_orders = DB::table('booking_details')
                                            ->select('booking_details.id as bid','booking_details.*','admin_customers.*')
                                            ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                            ->leftJoin('rooms','rooms.id','booking_details.rooms')
                                            ->where('booking_details.vendor_id',$vendor_id)
                                            ->where('booking_details.outlet_id',Session::get('property_id'))
                                            ->where('booking_details.id','=',$id)
                                            ->first();
                        //  dd($vendor_orders);

                        $payment = DB::table('payments')
                                        ->select('payments.id')
                                        ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                        ->where('payments.booking_id','=',$id)
                                        ->get();

                        $room_type = DB::table('room_type')
                                    ->select('room_type.*','room_type_infos.*')
                                    ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                                    ->where('default_status', 1)
                                    ->where('property_id',Session::get('property_id'))
                                    ->orderBy('room_type', 'asc')
                                    ->get();                                        

                              //    dd($payment);

                        return view('vendors.booking.edit')->with('data', $vendor_orders)
                                                           ->with('payment',$payment)
                                                           ->with('room_type',$room_type);
                                                           
                    }                    
                }

                public function autocomplete(Request $request)
                {
                    //  $data = DB::table('admin_customers')

                    $data = Admin_customers::select("email as name")
                                            ->where("email","LIKE","%{$request->input('query')}%")
                                            ->get();

                    return response()->json($data);
                }
//  NEW MODULE
                public function newUser(Request $data)
                {

                    $post_data = $data->all();

                      //    print_r($post_data);exit;

                    $rules = [
                        'guest_name' => ['required', 'alpha', 'max:250' /*,'unique:admin_customers,firstname' */],
                        'email'      => ['required', 'email', 'max:250', 'unique:admin_customers,email'],
                        'mobile'     => ['required','numeric', 'unique:admin_customers,mobile_number'],
                        'address'    => ['required'],
                    ];

                    $error = $result = array();
                    $messages = array(
                    'terms_condition.required_if' => 'The Terms and Condition field is required.',
                    );
                    $validator = app('validator')->make($post_data, $rules , $messages);

                    if ($validator->fails()) {
                        
                        foreach ($validator->errors()->messages() as $key => $value)
                        {
                            $error[] = is_array($value) ? implode(',', $value) : $value;
                        }
                        $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));
                    }else {

                            //  print_r('Ak');exit;
                            $pwd = getRandomBookingNumber(6);

                            $mobile = $post_data['country_code'].trim($post_data['mobile']);

                            $msg = 'Your account has been created with '.getAppConfig()->site_name.' and Your Password is '.$pwd;
                            $result = send_sms($mobile,$msg);

                            //  print_r($result);exit;

                            if($result == 2){                        
                                $customers = new Admin_customers;

                                $customers->firstname = Input::get('guest_name');
                                $customers->email = Input::get('email');
                                $customers->mobile_number = Input::get('mobile');
                                $customers->hash_password = md5($pwd);
                                $customers->country_code = Input::get('country_code');
                                $customers->address = Input::get('address');
                                $customers->status = 1;
                                $customers->is_verified = 1;
                                $customers->customer_unique_id = getRandomBookingNumber(8);
                                $customers->save();

                                //  echo 1; exit;


                            $template = DB::table('email_templates')
                                            ->select('*')
                                            ->where('template_id','=',self::USERS_WELCOME_EMAIL_TEMPLATE)
                                            ->get();

                            if(count($template)){
                                $from = $template[0]->from_email;
                                $from_name=$template[0]->from;
                                $subject = $template[0]->subject;
                                if(!$template[0]->template_id)
                                {
                                    $template = 'mail_template';
                                    $from=getAppConfigEmail()->contact_email;
                                    $subject = "Welcome to ".getAppConfig()->site_name;
                                    $from_name="";
                                }
                                $user['password'] = $pwd;
                                $content =array("customer" => $customers->firstname,
                                    'u_password' => $pwd);
                                $email=smtp($from,$from_name,$customers->email,$subject,$content,$template);
                            }
                        

                            $result = array("response" => array("httpCode" => 200, "status" => true, 
                                "Message" => trans("messages.New Customer Added Successfully")));                                
                            }
                            else{
                                $result = array("response" => array("httpCode" => 300, "status" => false, 
                                        "Message" => $result));
                            }                             
                    }

                //  return $result;

                    //  print_r($result);exit;

                return response()->json($result);

                }                
/*
                public function newUser(Request $request)
                {
                    

                    $customers = new Admin_customers;

                    $customers->firstname = Input::get('guest_name');
                    $customers->email = Input::get('email');
                    $customers->mobile_number = Input::get('mobile');
                    $customers->country_code = Input::get('country_code');
                    $customers->address = Input::get('address');
                    $customers->status = 1;
                    $customers->is_verified = 1;
                    $customers->save();

                    echo 1; exit;
               
                }
*/                

                public function downloadinvoice($invoiceId,$vendor_id="")
                    {
/*                        
                        $vendor_id="";
                        if(Session::get('vendor_type') == 2)
                        {
                            $vendor_id = Session::get('staffs_under_by');
                        }
                        else
                        {
                            
                            $vendor_id = Session::get('vendor_id');

                        } 
*/                        

                        $vendor_id = Session::get('vendor_id');




            $html = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><table width="700px" cellspacing="0" cellpadding="0" bgcolor="#fff" style="border:1px solid #ccc;">
            <tbody>
            <tr>
            <td style="border-bottom:1px solid #ccc;">
            <table style="padding-top: 25px; padding-bottom: 25px;" width="700px" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td width="20">&nbsp;</td>
            <td>
            <table>
            <tr>
            <td style="font-size:16px; font-weight:bold; font-family:Verdana; color:#000; padding-bottom:10px;">BILL FORM :</td>
            </tr>
            <tr>
            <td style="font-size:16px; font-weight:500; font-family:dejavu sans,arial; color:#666; line-height:28px;">'.ucfirst($out->vendor_name).','.wordwrap(ucfirst($out->contact_address),70,"<br>\n").'<br/>'.ucfirst($out->contact_email).'</td>
            </tr>
            </table>
            </td>
            <td align="right"><a title="'.$site_name.'" href="'.url('/').'"><img src="'.$logo.'" alt="'.$site_name.'" /></a></td>
            <td width="20">&nbsp;</td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>
            <!-- end 1 tr -->
            <tr>
            <td>
            <table style="padding-top: 25px; padding-bottom: 25px;" width="700px" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td width="20">&nbsp;</td>
            <td colspan="4">
            <table>
            <tr>
            <td style="font-size:16px; font-weight:bold; font-family:Verdana; color:#000; padding-bottom:10px;">'.$delivery_type.'</td>
            </tr>
            <tr>
            <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.wordwrap($delivery_address,70,"<br>\n").'
            <br/>'.$delivery_email.'</td>
            </tr>
            </table>
            </td>
            <td align="right">
            <table cellpadding="0" cellspacing="0">
            <tr>
            <td style="font-size:15px; font-weight:bold; font-family:Verdana; color:#000; line-height:28px;">Invoice</td>
            <td></td>
            <td align="left" style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.$out->invoice_id.'</td>
            </tr>
             <tr>
            <td style="font-size:15px; font-weight:bold; font-family:Verdana; color:#000; line-height:28px;">Delivery date</td>
            <td></td>
            <td align="left" style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.date('M d, Y', strtotime($delivery_details[0]->delivery_date)).'</td>
            </tr>

            <tr>
            <td style="font-size:15px; font-weight:bold; font-family:Verdana; color:#000; line-height:28px;">Invoice date</td>
            <td></td>
            <td align="left" style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.date('M d, Y', strtotime($order_detail->created_date)).'</td>
            </tr>
            <tr>
            <td style="font-size:15px; font-weight:bold; font-family:arial; color:#000; line-height:28px; background:#d1d5d4; padding:0 9px;">Amount due</td>
            <td></td>
            <td align="left" style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;background:#d1d5d4;padding:0 9px;">'.$total_amount.'</td>
            </tr>
            </table>
            </td>
            <td width="20">&nbsp;</td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>
            <!-- end 2 tr -->
            <tr>
            <td>
            <table cellpadding="0" cellspacing="0" width="100%">
            <tr style="background:#d1d5d4;padding:0 9px;">
            <td align="center" style=" padding:7px 0; font-size:17px; font-family:Verdana; font-weight:bold;">Item</th>
            <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Description</th>
            <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Quantity</th>
            <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Unit cost</th>
            <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Line total</th>
            </tr>'.$item.'
            </table>
            </td>
            </tr>
            <!-- end 3 tr -->
            <tr>
            <td>
            <table style="padding-top: 25px; padding-bottom: 25px;" width="787" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td width="20">&nbsp;</td>
            <td>
            </td>
            <td align="right">
            <table cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td style="font-size:15px; font-weight:bold; font-family:dejavu sans,arial; color:#000; line-height:28px;">SUBTOTAL</td>
            <td width="10"></td>
            <td style="font-size:16px; font-weight:500; font-family:dejavu sans,arial; color:#666; line-height:28px;" align="right">'.$sub_total.'</td>
            </tr>
            <tr>
            <td style="font-size:15px; font-weight:bold; font-family:dejavu sans,arial; color:#000; line-height:28px;">Delivery fee</td>
            <td width="10"></td>
            <td style="font-size:16px; font-weight:500; font-family:dejavu sans,arial; color:#666; line-height:28px;" align="right">'.$delivery_charge.'</td>
            </tr>
            '.$coupon_offer.'
            <tr>
            <td style="font-size:15px; font-weight:bold; font-family:dejavu sans,arial; color:#000; line-height:28px; background:#d1d5d4; padding:0 9px;">TOTAL</td>
            <td style="background:#d1d5d4;padding:0 9px;" width="10"></td>
            <td style="font-size:16px; font-weight:500; font-family:dejavu sans,arial; color:#666; line-height:28px;background:#d1d5d4;padding:0 9px;" align="right">'.$total_amount.'</td>
            </tr>
            </tbody></table>
            </td>
            <td width="20">&nbsp;</td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>
            <tr>
            <td>
            <table>
            <tr>
            <td width="20">&nbsp;</td>
            <td width="20">&nbsp;</td>
            </tr>
            </tbody>
            </table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html)->save(base_path().'/public/assets/front/'.Session::get("general")->theme.'/images/invoice/'.$out->invoice_id.'.pdf');
                    }                
                

/*                

//  NEW MODULE
                public function newUser(Request $data)
                {

                    $post_data = $data->all();

                    $rules = [
                        'guest_name' => ['required', 'alpha', 'max:250', 'unique:admin_customers,firstname'],
                        'email'      => ['required', 'email', 'max:250', 'unique:admin_customers,email'],
                        'mobile'     => ['required','numeric', 'unique:admin_customers,mobile_number'],
                        'address'    => ['required'],
                    ];

                    $error = $result = array();
                    $messages = array(
                    'terms_condition.required_if' => 'The Terms and Condition field is required.',
                    );
                    $validator = app('validator')->make($post_data, $rules , $messages);

                    if ($validator->fails()) {
                        
                        foreach ($validator->errors()->messages() as $key => $value)
                        {
                            $error[] = is_array($value) ? implode(',', $value) : $value;
                        }
                        $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));
                    }else {

                            $pwd = getRandomBookingNumber(6);

                            $msg = 'Your account has been created with '.getAppConfig()->site_name.' and Your Password is '.$pwd;
                            $result = send_sms($mobile,$msg);

                            if($result == 2){                        
                                $customers = new Admin_customers;

                                $customers->firstname = Input::get('guest_name');
                                $customers->email = Input::get('email');
                                $customers->mobile_number = Input::get('mobile');
                                $customers->hash_password = md5($pwd);
                                $customers->country_code = Input::get('country_code');
                                $customers->address = Input::get('address');
                                $customers->status = 1;
                                $customers->is_verified = 1;
                                $customers->customer_unique_id = getRandomBookingNumber(8);
                                $customers->save();

                                //  echo 1; exit;


                            $template = DB::table('email_templates')
                                            ->select('*')
                                            ->where('template_id','=',self::USERS_WELCOME_EMAIL_TEMPLATE)
                                            ->get();

                            if(count($template)){
                                $from = $template[0]->from_email;
                                $from_name=$template[0]->from;
                                $subject = $template[0]->subject;
                                if(!$template[0]->template_id)
                                {
                                    $template = 'mail_template';
                                    $from=getAppConfigEmail()->contact_email;
                                    $subject = "Welcome to ".getAppConfig()->site_name;
                                    $from_name="";
                                }
                                $user['password'] = $pwd;
                                $content =array("customer" => $$customers->firstname,
                                    'u_password' => $pwd);
                                $email=smtp($from,$from_name,$customers->email,$subject,$content,$template);
                            }
                        

                            $result = array("response" => array("httpCode" => 200, "status" => true, 
                                "Message" => trans("messages.New Customer Added Successfully")));                                
                            }
                            else{
                                $result = array("response" => array("httpCode" => 300, "status" => false, 
                                        "Message" => $result));
                            }                             
                    }

                //  return $result;

                return response()->json($result);

                }
*/                
}
