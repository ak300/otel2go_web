<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Text;
use PushNotification;
use Session;
use Closure;
use DB;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Hash;
use URL;
use Yajra\Datatables\Datatables;
use App\Model\categories;
use App\Model\categories_infos;
use App\Model\vendors;
use App\Model\vendors_infos;
use App\Model\users;
use App\Model\customers_view;
use App\Model\outlets;
use App\Model\outlet_infos;
use App\Model\outlet_reviews;
use App\Model\delivery_timings;
//use App\Model\opening_timings;
use App\Model\settings;
use App\Model\outlet_managers;
use App\Model\return_reasons;
use App\Model\return_actions;
use App\Model\return_status;
use App\Model\return_orders_log;
use App\Model\vendors_view;
use App\Model\amenities;
use App\Model\amenities_infos;
use App\Model\accomodation_type;
use App\Model\accomodation_type_infos;
use App\Model\place_of_interest;
use App\Model\booking_infos;
use App\Model\rooms;
use App\Model\rooms_infos;
use App\Model\admin_customers;
use App\Model\customer_feedback;
use App\Model\booking_referral_logs;
use App\Model\subscriptions as Subscriptionrenewal;
use App\Model\booking_detail;
use Illuminate\Http\UploadedFile;
use Nexmo\Laravel\Facade\Nexmo;

class Store extends Controller
{
                    const VENDORS_FORGOT_PASSWORD_EMAIL_TEMPLATE = 6;
                    const VENDORS_CHANGE_PASSWORD_EMAIL_TEMPLATE = 7;
                    const MANAGER_WELCOME_EMAIL_TEMPLATE = 11;
                    const MANAGER_SIGNUP_EMAIL_TEMPLATE = 12;
                    const RETURN_STATUS_CUSTOMER_EMAIL_TEMPLATE = 17;
                    const ORDER_STATUS_UPDATE_USER = 18;
                    const REFUND_APPROVE_EMAIL_TEMPLATE = 20;
                    const VENDOR_CHANGE_PASSWORD_OTP_SMS_TEMPLATE = 1;
                    const APPOINTMENT_FINAL_INVOICE_THANKS = 31;
                    const VENDORS_REGISTER_EMAIL_TEMPLATE = 4;
                    const USER_ORDER_STATUS_UPDATE_TEMPLATE = 3;
                    const USER_FINAL_ORDER_UPDATE_TEMPLATE = 4;

                    /**
                     * Create a new controller instance.
                     *
                     * @return void
                     */
                public function __construct()
                    {
                        $this->site_name = isset(getAppConfig()->site_name)?ucfirst(getAppConfig()->site_name):'';
                        SEOMeta::setTitle('Vendor Panel - '.$this->site_name);
                        SEOMeta::setDescription('Vendor Panel - '.$this->site_name);
                        SEOMeta::addKeyword('Vendor Panel - '.$this->site_name);
                        OpenGraph::setTitle('Vendor Panel - '.$this->site_name);
                        OpenGraph::setDescription('Vendor Panel - '.$this->site_name);
                        OpenGraph::setUrl('Vendor Panel - '.$this->site_name);
                        Twitter::setTitle('Vendor Panel - '.$this->site_name);
                        Twitter::setSite('@Vendor Panel - '.$this->site_name);
                       // App::setLocale('en');
                    }
                    //Get city list for ajax request
                public function getCityData(Request $request)
                    {

                         // if (Auth::guest())
                         //    {
                         //        return 404;
                         //    }
                        if($request->ajax()){
                            $country_id = 1; /*  $request->input('cid');  */
                            $city_data = getCityList($country_id);
                            return response()->json([
                                'data' => $city_data
                            ]);
                        }
                    }

                    //Get city list for ajax request
                public function getRoomData(Request $request)
                    {

                         // if (Auth::guest())
                         //    {
                         //        return 404;
                         //    }
                        if($request->ajax()){
                            $roomtype_id =  $request->input('rid');
                            $ac =  $request->input('ac');
                            $cc =  $request->input('cc');
                            $room_data = getRoomList($roomtype_id,$ac,$cc);
                            return response()->json([
                                'data' => $room_data
                            ]);
                        }
                    }
                public function rooms_check(Request $request)
                {
                        if($request->ajax()){
                            $cin = $request->input('cin');
                            $cout = $request->input('cout');
                            $rtd = $request->input('rtd');
                            $cc  = $request->input('cc');
                            $ac  = $request->input('ac');
                            $days = $request->input('days');
                            $room_chk = getRoomBookingList($cin,$cout,$rtd,$cc,$ac,$days);

                            return response()->json([
                                'data' => $room_chk
                            ]);
                        }
                }


                public function expense_data(Request $request)
                {
                        if($request->ajax()){
                            $cin = $request->input('from');
                            $cout = $request->input('to');
                            $room_chk = getExpensesData($cin,$cout);

                            return response()->json([
                                'data' => $room_chk
                            ]);
                        }
                }


                public function days_check(Request $request)
                {
                        if($request->ajax()){
                            $sd = $request->input('sd');
                            $ed = $request->input('ed');
                            $duration = $request->input('duration');
                            $room_count  = $request->input('room_count');
                            $adult_count  = $request->input('adult_count');
                            
                            $days_check = setSessionData($sd,$ed,$duration,$room_count,$adult_count);

                            return response()->json([
                                'data' => $days_check
                            ]);
                        }
                }                

/*
                public function summary_check(Request $request)
                {
                        if($request->ajax()){
                            $st = $request->input('from');
                            $ed = $request->input('to');
                            $summary_check = getReportSummary($st,$ed);

                            return response()->json([
                                'data' => $summary_check
                            ]);
                        }
                }
*/
                public function guest_check(Request $request)
                {
                        if($request->ajax()){
                            //  $guest_name = $request->input('guest_name');
                            $guest_email = $request->input('guest_email');
                            $guest_chk = getRoomBookedGuestList($guest_email);

                            return response()->json([
                                'data' => $guest_chk
                            ]);
                        }
                }

                public function price_check(Request $request)
                {
                        if($request->ajax()){
                            $room_id = $request->input('room_id');
                            $room_type = $request->input('room_type');
                            $price_chk = getRoomPriceList($room_id,$room_type);

                            return response()->json([
                                'data' => $price_chk
                            ]);
                        }
                }

                    //Get city list for ajax request
                public function getLocationData(Request $request)
                    {
                         // if (Auth::guest())
                         //    {
                         //        return 404;
                         //    }
                        if($request->ajax()){
                            $country_id = $request->input('country_id');
                            $city_id = $request->input('city_id');
                            $location_data = getLocationList($country_id,$city_id);
                            return response()->json([
                                'data' => $location_data
                            ]);
                        }
                    }

                    //Get city list for ajax request
                public function getFrontLocationData(Request $request)
                    {
                         // if (Auth::guest())
                         //    {
                         //        return 404;
                         //    }
                        if($request->ajax()){
                            $city_url = $request->input('city_url');
                            $location_data = getFrontLocationList($city_url);
                            return response()->json([
                                'data' => $location_data
                            ]);
                        }
                    }
                    
                    


                    /* Get outlets list based on the vendor */
                public function getOutletData(Request $request)
                    {
                        // if (Auth::guest())
                        //     {
                        //         return 404;
                        //     }
                        if($request->ajax()){
                            $c_id = $request->input('cid');
                            $data = getOutletList($c_id);
                            $cdata = getVendorsubCategoryLists($c_id);
                            //print_r($cdata);die;
                            return response()->json([
                                'data' => $data,'cdata'=>$cdata
                            ]);
                        }
                    }
                    /* Get outlets list based on the vendor */
                public function getSubCategoryData(Request $request)
                    {   
                        // if (Auth::guest())
                        //     {
                        //         return 404;
                        //     }
                        if($request->ajax()){
                            $c_id = $request->input('cid');
                            $language = $request->input('language');
                            $data = getSubCategoryLists1(1,$c_id,$language); // get product sub category data here
                            return response()->json([
                                'data' => $data
                            ]);
                        }
                    }


                        /* Get outlets list based on the vendor */
                public function getSubCategoryDataUpdated(Request $request)
                    {

                        // if (Auth::guest())
                        //     {
                        //         return 404;
                        //     }
                        if($request->ajax()){
                            $c_id = $request->input('cid');
                            $language = $request->input('language');
                            $head_category = $request->input('head_category');
                            $data = getSubCategoryListsupdated(1,$c_id,'',$language,$head_category); // get product sub category data here
                            return response()->json([
                                'data' => $data
                            ]);
                        }
                    }
                    
                    
                    
                    //Get city list for ajax request
                public function notifications_read(Request $request)
                    {

                        // if (Auth::guest())
                        //     {
                        //         return 404;
                        //     }

                        if($request->ajax()){
                            $vendor_id = Session::get('vendor_id');
                            $c_id = $request->input('cid');

                            $res = DB::table('notifications')
                                ->where('id', $c_id)
                                ->update(['read_status' => 1,'modified_date'=>date('Y-m-d H:i:s')]);
                            $notifications = DB::table('notifications')
                                ->select('notifications.id','notifications.order_id','notifications.message','notifications.created_date','notifications.read_status','customers_view.name','customers_view.image')
                                ->leftJoin('customers_view','customers_view.id','=','notifications.customer_id')
                                ->where('read_status', 0);
                                if(!empty($vendor_id) && $vendor_id!=1){
                                    $notifications = $notifications->where('vendor_id', $vendor_id);
                                }
                                $notifications = $notifications->orderBy('created_date', 'desc')
                                ->get();
                                
                                $count = count($notifications);
                            $data = ($res==true)?1:0;
                            return response()->json([
                                'data' => $data,'count' => $count,'vid'=>$vendor_id
                            ]);
                        }
                    }
                    /*
                     * Vendor login process goes here
                    */
                public function login () 
                    {        
                        return view('vendors.login');
                    }
                    
                    /**
                     * Get post values from form and checks the details exists
                     * and redirects to dashboard page or else redirec to errors
                     * @return \Illuminate\Http\Response
                     */
                public function signin(Request $data) 
                    {

                        $datas=Input::all();
                        // print_r($datas);exit;

                        $validation = Validator::make($data->all(), array(
                            'email' => 'required|email',
                            'password' => 'required',
                        ));
                        
                        // process the validation
                        if ($validation->fails()) {
                                //return redirect('create')->withInput($datas)->withErrors($validation);
                                return Redirect::back()->withErrors($validation)->withInput();
                        } else {
                            $email = Input::get('email');
                            $password = md5(Input::get('password'));
                            // print_r($datas);exit;
/*                            
                            $vendors = DB::table('vendors_view')
                                        ->select('vendors_view.id','vendors_view.first_name','vendors_view.email')
                                        ->where('email',$email)
                                        ->where('password',$password)
                                        ->where('active_status',1)
                                        ->get();
*/                                        
                             $vendors = DB::table('vendors_view')
                                        ->select('vendors_view.id','vendors_view.first_name','vendors_view.email','vendors_infos.vendor_name','vendors_view.mobile_number','vendors_view.logo_image','vendors_view.vendor_type','vendors_view.created_vendor_id','vendors_view.vendor_name')
                                        ->leftJoin('vendors_infos','vendors_infos.vendors_view_id','=','vendors_view.id')
                                        ->where('email',$email)
                                        ->where('password',$password)
                                        ->where('active_status',1)
                                        ->get();
                                            // print_r($vendors);exit;

                            if(count($vendors) > 0){
                                $vendors_data = $vendors[0];

                                $vendor_bal = DB::table('booking_details')
                                                ->select(DB::Raw('sum(payments) as current_balance'))
                                                ->where('booking_details.vendor_id','=',$vendors[0]->id)
                                                //  ->where('booking_details.outlet_id','=',$property_id)
                                                ->get();  
                                //  dd($vendor_bal);

                            $cur_bal = isset($vendor_bal[0]->current_balance)?$vendor_bal[0]->current_balance:0;

/*                                                
                                $vendor = vendors::find($vendors_data->id);

                                $vendor->current_balance = $cur_bal;
                                $vendor->save();                             
*/  

                                $v = vendors::where('id','=',$vendors_data->id)
                                            ->update(['current_balance' => $cur_bal]);

                                

                                //Store the session data for future usage
                                Session::put('vendor_id', $vendors_data->id);
                                Session::put('user_name', ucfirst($vendors_data->first_name));
                                Session::put('vendor_name', $vendors_data->first_name);
                                Session::put('vendor_email', $vendors_data->email);
                                Session::put('vendor_image', $vendors_data->logo_image);
                                Session::put('vendor_type', $vendors_data->vendor_type);
                                Session::put('created_vendor_id', $vendors_data->created_vendor_id);
                                Session::put('vendor_mobile_number',$vendors_data->mobile_number);
                                Session::put('vendor_name', $vendors_data->vendor_name);

                                if (Session::get('vendor_type') == 2 ){

                                    $vendor_id = Session::get('created_vendor_id');

                                }
                                else
                                {
                                    $vendor_id = Session::get('vendor_id');
                                }
                                $outlet_property_id = get_first_outlet($vendor_id); 
                                Session::put('property_id',$outlet_property_id);


                                //Show the flash message if successful logged in
                                Session::flash('message', trans('messages.Logged in successfully'));
                                return Redirect::to('vendors/dashboard');
                            } else {
                                $validation->errors()->add('email', 'These credentials do not match our records.');
                                return Redirect::back()->withErrors($validation)->withInput();
                            }
                        }
                    }
                    /*
                     * After logged in redirects to dashboard page
                    */

                public function booking()
                    {
                        //  dd('Ak');

                        //  $mob = Session::get('vendor_mobile_number');
                        //  dd($mob);
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        else {
                            
                            $vendor_id = Session::get('vendor_id');
                      
                        if (Session::get('vendor_type') == 2 ){

                            $vendor_id = Session::get('created_vendor_id');

                        }

                        if (Session::get('property_id') == 0)
                            {
                                //  dd('Ak');

                                $outlet_property_id = get_first_outlet($vendor_id); 
                                Session::put('property_id',$outlet_property_id);                                
                            }




                        $property_id = Session::get('property_id');  

                        //  dd($property_id);


                        $vendor_orders = DB::table('booking_details')
                                            ->select('booking_details.id as bid','booking_details.*',
                                                'admin_customers.*')
                                            ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                            //  ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                            //  ->leftJoin('rooms','rooms.id','booking_info.room_id')
                                            ->where('booking_details.vendor_id',$vendor_id)
                                            ->where('booking_details.outlet_id',$property_id)
                                            //  ->groupBy('bid','admin_customers.id')
                                            //  ->distinct('bid')
                                            ->get();
                                            
                            //  dd($vendor_orders);
                  

/*
            if($vendor_id)
            {
                $q1 = "booking_details.vendor_id >= $vendor_id";
            }

            if($property_id !="")
            {
                $q2 = "booking_details.outlet_id >= $property_id";
            }         

            $vendor_orders = DB::select('SELECT Distinct on (booking_details.id) booking_details.id, 
                                  (booking_details.id) as bid, 
                                  admin_customers.firstname,booking_details.check_in_date,
                                  booking_details.check_out_date,booking_info.room_id,
                                  booking_details.booking_random_id
                                  FROM booking_details
                                  left join admin_customers on admin_customers.id = booking_details.customer_id
                                  left join booking_info on booking_info.booking_id = booking_details.id
                                  WHERE '.$q1.' and '.$q2.'
                                  GROUP BY booking_info.booking_id,bid,admin_customers.id,
                                  booking_info.room_id'); 

             // dd($vendor_orders);
*/             
/*

                        $vendor_orders = DB::table('rooms')
                                            ->select('rooms.id as rid')
                                            ->leftJoin('booking_details','booking_details.rooms','rooms.id')
                                            ->leftJoin('booking_user_details','booking_user_details.booking_id','booking_details.id')
                                            ->leftJoin('booked_room','booked_room.booking_id','booking_details.id')
                                            ->where('booking_details.vendor_id',$vendor_id) 
                                            ->where('booking_details.outlet_id',Session::get('property_id'))
                                            ->groupBy('rid')
                                            ->get();
*/
                                    // dd($vendor_orders);

                          

                        $vendors_orders_aray = array();
                        $i = 0;
                        foreach($vendor_orders as $vendor_order)
                        {
                            if($vendor_order->check_in_date!="")
                            {

                                $cin_date = date('Y-m-d',strtotime($vendor_order->check_in_date));

                                $cout_date = date('Y-m-d',strtotime($vendor_order->check_out_date. ' + 1 day'));

                                $cout_date1 = date('Y-m-d',strtotime($vendor_order->check_out_date));

                                //  $cout_date->modify('+1 day');

                                //  dd($date);
                                $vendors_orders_aray[$i]['id'] = $vendor_order->bid;
                                $vendors_orders_aray[$i]['title'] = ucfirst($vendor_order->firstname);
                                $vendors_orders_aray[$i]['start'] = $cin_date /*    .'T'.'00:00:00' */; 
                                $vendors_orders_aray[$i]['end'] = $cout_date /* .'T'.'00:00:00' */;
                                $vendors_orders_aray[$i]['end_date'] = $cout_date1 /* .'T'.'00:00:00' */;
                                $vendors_orders_aray[$i]['notes'] = $vendor_order->room_name;
                                $vendors_orders_aray[$i]['random_id'] = $vendor_order->booking_random_id;

                    

                                $i++;
                            }

                            //$vendors_orders_aray['end'] = $vendor_order->date.'T1'.$vendor_order->date;
                        }

                        $vendor_orders_encoded = json_encode($vendors_orders_aray,true);

                        return view('vendors.home')->with('vendor_booking',$vendor_orders_encoded);

                    }
                }
                    
                public function home() 
                    {
                       $vt = Session::get('vendor_type');
                       // $cv = Session::get('created_vendor_id');
                       //   print_r($cv);
                       //print_r($vt); exit;
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        else {
                            
                            $vendor_id = Session::get('vendor_id');
                      
                        if (Session::get('vendor_type') == 2 ){

                            $vendor_id = Session::get('created_vendor_id');

                        }

                        $property_id = Session::get('property_id');

                        //  dd($property_id);
/*
                        $vendor_orders = DB::table('booking_details')
                                            ->select('booking_details.id as bid','booking_details.*','admin_customers.*')
                                            ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                            ->leftJoin('rooms','rooms.id','booking_details.rooms')
                                            ->groupBy('booking_details.created_date')
                                            ->where('booking_details.vendor_id',$vendor_id)
                                            ->where('booking_details.outlet_id',$property_id)
                                            ->get();          

                        //  dd($vendor_orders);
*/
                        $vendor_booking = DB::select('SELECT DATE(booking_details.created_date) as date ,sum(price) as price from booking_details join booking_charges on booking_details.id = booking_charges.booking_id where outlet_id = '.$property_id.' and vendor_id = '.$vendor_id.' GROUP BY DATE(booking_details.created_date) order by DATE(booking_details.created_date) asc');

                        //  dd($vendor_booking);

                        $vendor_booking_array = array();

                        $i = 0;

                        foreach($vendor_booking as $booking)
                        {
                            $date = strtotime($booking->date).'000';
                            $vendor_booking_array[][] = $date.','.$booking->price;
                            $i++;
                        }

                        $vendor_booking_encoded =  json_encode($vendor_booking_array,JSON_NUMERIC_CHECK);
                        $vendor_booking_encoded =  str_replace("\"", "", $vendor_booking_encoded);

                             // dd($vendor_booking_encoded);

                        $vendor_orders_count = DB::select('SELECT DATE(booking_details.created_date) as date,count(booking_details.id) as order_count FROM booking_details where outlet_id = '.$property_id.' and vendor_id = '.$vendor_id.' GROUP BY DATE(booking_details.created_date) ORDER BY DATE(booking_details.created_date) asc');
                        $vendors_orderscount_array = array();
                        $i = 0;
                        foreach($vendor_orders_count as $vendor_order_count)
                        {
                            $date = strtotime($vendor_order_count->date).'000';
                            $vendors_orderscount_array[][] = $date.','.$vendor_order_count->order_count;
                            $i++;
                        }
                        $vendors_orderscount_count_encoded =  json_encode($vendors_orderscount_array,JSON_NUMERIC_CHECK);
                        $vendors_orderscount_count_encoded =  str_replace("\"", "", $vendors_orderscount_count_encoded);

                        //  dd($vendors_orderscount_count_encoded);

                        $staff_count = DB::select('SELECT /* DATE(created_date) as date , */   count(id) as staff_count from outlet_managers where vendor_id  = '.$vendor_id.' and outlet_id = '.$property_id.' GROUP BY DATE(created_date) ORDER BY DATE(created_date) asc');

                        $staff_count_array = array();

                        $i = 0;

                        foreach($staff_count as $sc)
                        {
                            //  $date = strtotime($sc->date).'000';
                            $staff_count_array[][] = $sc->staff_count;
                            $i++;
                        }

                        $staff_count_encoded = json_encode($staff_count_array,JSON_NUMERIC_CHECK);
                        $staff_count_encoded = str_replace("\"", "", $staff_count_encoded);

                          //    dd($staff_count_encoded);

                        $data_check_months=0;
                            for ($i = 0; $i <= 11; $i++)
                            {
                                //print_r($vendor_id);
                                //  exit;

                                $months[] = date("M-Y", strtotime( date( 'Y-m-01' )." -$i months"));
                                $mon = date('m', strtotime( date( 'Y-m-01' )." -$i months"));

                                $sales['total'][$i] = DB::select("SELECT count(id) as staff_count FROM outlet_managers WHERE EXTRACT(MONTH FROM created_date) = $mon AND vendor_id = $vendor_id AND outlet_id = '$property_id'");
                                $sales['total'][$i] = $sales['total'][$i][0]->staff_count;

                            if($sales['total'][$i]>0){
                              $data_check_months++;
                            }   
                        }                     

                        //  dd($staff_count_encoded);

                        $months_encoded =  json_encode($months,JSON_NUMERIC_CHECK);


            //  MONTHLY WISE REPORTS                        

                        $vendor_orders_count = DB::select('SELECT DATE(created_date) as date,count(payments) as order_count FROM booking_details where vendor_id = '.$vendor_id.' GROUP BY DATE(created_date) ORDER BY DATE(created_date) asc');
                        $vendors_orderscount_array = array();
                        $i = 0;
                        foreach($vendor_orders_count as $vendor_order_count)
                        {
                            $date = strtotime($vendor_order_count->date).'000';
                            $vendors_orderscount_array[][] = $date.','.$vendor_order_count->order_count;
                            $i++;
                        }

                        $vendors_orderscount_count_encoded =  json_encode($vendors_orderscount_array,JSON_NUMERIC_CHECK);
                        $vendors_orderscount_count_encoded =  str_replace("\"", "", $vendors_orderscount_count_encoded);
                        $language_id = getAdminCurrentLang();
                        $data_check_months=0;


                        for ($i = 0; $i <= 11; $i++)
                        {
                                //print_r($vendor_id);
                                //  exit;

                                $months[] = date("M-Y", strtotime( date( 'Y-m-01' )." -$i months"));
                                $mon = date('m', strtotime( date( 'Y-m-01' )." -$i months"));

                                $sales['total'][$i] = DB::select("SELECT count(payments) as payments FROM booking_details WHERE EXTRACT(MONTH FROM created_date) = $mon AND vendor_id = $vendor_id");

                                //  dd($sales['total'][$i]);
                                $sales['total'][$i] = $sales['total'][$i][0]->payments;

                            if($sales['total'][$i]>0){
                              $data_check_months++;
                            }

                            //
                                $sales['walkin'][$i] = DB::select("SELECT count(payments) as total_denied FROM booking_details WHERE EXTRACT(MONTH FROM created_date) = $mon AND booking_status = 1 AND vendor_id = $vendor_id");
                                    $sales['walkin'][$i] = $sales['walkin'][$i][0]->total_denied;
                            if($sales['walkin'][$i]>0){
                              $data_check_months++;
                            }

                            //

                                    $sales['confirmed'][$i] = DB::select("SELECT count(payments) as total_confirmed FROM booking_details WHERE EXTRACT(MONTH FROM created_date) = $mon AND booking_status = 2 AND vendor_id = $vendor_id");
                                    $sales['confirmed'][$i] = $sales['confirmed'][$i][0]->total_confirmed;
                            if($sales['confirmed'][$i]>0){
                              $data_check_months++;
                            }

                            //
                                    $sales['complete'][$i] = DB::select("SELECT count(payments) as total_complete FROM booking_details WHERE EXTRACT(MONTH FROM created_date) = $mon AND booking_status = 3 AND vendor_id = $vendor_id");
                                    $sales['complete'][$i] = $sales['complete'][$i][0]->total_complete;
                            if($sales['complete'][$i]>0){
                              $data_check_months++;
                            }

                            //
                                    $sales['canceled'][$i] = DB::select("SELECT count(payments) as total_canceled FROM booking_details WHERE EXTRACT(MONTH FROM created_date) = $mon AND booking_status = 4 AND vendor_id = $vendor_id");
                                    $sales['canceled'][$i] = $sales['canceled'][$i][0]->total_canceled;
                            if($sales['canceled'][$i]>0){
                              $data_check_months++;
                            }   

                            //
                                $sales['pending'][$i] = DB::select("SELECT count(payments) as total_pending FROM booking_details WHERE EXTRACT(MONTH FROM created_date) = $mon AND booking_status = 5 AND vendor_id = $vendor_id");
                                    $sales['pending'][$i] = $sales['pending'][$i][0]->total_pending;
                            if($sales['pending'][$i]>0){
                              $data_check_months++;
                            }

                                $sales['denied'][$i] = DB::select("SELECT count(payments) as total_denied FROM booking_details WHERE EXTRACT(MONTH FROM created_date) = $mon AND booking_status = 6 AND vendor_id = $vendor_id");
                                    $sales['denied'][$i] = $sales['denied'][$i][0]->total_denied;
                            if($sales['denied'][$i]>0){
                              $data_check_months++;
                            }                            


/*
                            //
                                    $sales['missed'][$i] = DB::select("SELECT count(payments) as total_missed FROM booking_details WHERE EXTRACT(MONTH FROM created_date) = $mon AND booking_status = 8 AND vendor_id = $vendor_id");
                                    $sales['missed'][$i] = $sales['missed'][$i][0]->total_missed;
                            if($sales['missed'][$i]>0){
                              $data_check_months++;
                            }

                            // 

                                $sales['initiated'][$i] = DB::select("SELECT count(payments) as total_initiated FROM booking_details WHERE EXTRACT(MONTH FROM created_date) = $mon AND booking_status = 10 AND vendor_id = $vendor_id");
                                    $sales['initiated'][$i] = $sales['initiated'][$i][0]->total_initiated;
                            if($sales['initiated'][$i]>0){
                              $data_check_months++;
                            }
*/    
                            //

                        

                        }

                       $sales_colors = DB::select("SELECT color_code FROM booking_status 
                                                   ORDER BY booking_status.id ASC");

                       //   dd($sales_colors);
                    

                        $colors=array('#1b1919');   //  black for all appointments default
                        foreach ($sales_colors as $key => $value) {
                                
                                 $color = array_values((array)$value);
                                 $color1 = array_values($color);
                                 array_push($colors, $color1[0]);
                              
                        } 

                        $colors_list = json_encode(array_values($colors),JSON_NUMERIC_CHECK);
                        //  dd($colors_list);
                        $total_sales = json_encode(array_values($sales['total']),JSON_NUMERIC_CHECK);
                        $total_canceled = json_encode(array_values($sales['canceled']),JSON_NUMERIC_CHECK);
                        $total_walkin = json_encode(array_values($sales['walkin']),JSON_NUMERIC_CHECK);
                        $total_denied = json_encode(array_values($sales['denied']),JSON_NUMERIC_CHECK);
                        $total_complete = json_encode(array_values($sales['complete']),JSON_NUMERIC_CHECK);
                        $total_confirmed = json_encode(array_values($sales['confirmed']),JSON_NUMERIC_CHECK);
                        //  $total_missed = json_encode(array_values($sales['missed']),JSON_NUMERIC_CHECK);
                        //  $total_initiated = json_encode(array_values($sales['initiated']),JSON_NUMERIC_CHECK);
                        $total_pending = json_encode(array_values($sales['pending']),JSON_NUMERIC_CHECK);
                        $months_encoded =  json_encode($months,JSON_NUMERIC_CHECK);                                                                       

                        $outlets_query = "SELECT  
                        (
                        SELECT COUNT(1)
                        FROM outlets
                        WHERE date_trunc('day', created_date) = date_trunc('day', CURRENT_DATE) AND vendor_id = $vendor_id ) AS day_count,

                        (SELECT COUNT(1)
                        FROM outlets
                        WHERE date_trunc('WEEK', created_date) = date_trunc('WEEK', CURRENT_DATE) AND vendor_id = $vendor_id ) AS week_count,

                        (SELECT COUNT(1)
                        FROM outlets
                        WHERE date_trunc('month', created_date) = date_trunc('month', CURRENT_DATE) AND vendor_id = $vendor_id ) AS month_count,

                        (SELECT COUNT(1)
                        FROM outlets
                        WHERE date_trunc('year', created_date) = date_trunc('year', CURRENT_DATE) AND vendor_id = $vendor_id  ) AS year_count,
                        COUNT(1) AS total_count
                        FROM outlets WHERE vendor_id = $vendor_id ";
                        $outlets_period_count = DB::select($outlets_query); 
                        
                        $outletmanager_query = "SELECT  
                        (
                        SELECT COUNT(1)
                        FROM outlet_managers
                        WHERE date_trunc('day', created_date) = date_trunc('day', CURRENT_DATE) AND outlet_id = $property_id AND vendor_id = $vendor_id ) AS day_count,

                        (SELECT COUNT(1)
                        FROM outlet_managers
                        WHERE date_trunc('WEEK', created_date) = date_trunc('WEEK', CURRENT_DATE) AND outlet_id = $property_id AND vendor_id = $vendor_id ) AS week_count,

                        (SELECT COUNT(1)
                        FROM outlet_managers
                        WHERE date_trunc('month', created_date) = date_trunc('month', CURRENT_DATE) AND outlet_id = $property_id AND vendor_id = $vendor_id ) AS month_count,

                        (SELECT COUNT(1)
                        FROM outlet_managers
                        WHERE date_trunc('year', created_date) = date_trunc('year', CURRENT_DATE) AND outlet_id = $property_id AND vendor_id = $vendor_id ) AS year_count,
                        COUNT(1) AS total_count
                        FROM outlet_managers WHERE vendor_id = $vendor_id AND outlet_id = $property_id";
                        $outletmanager_query = DB::select($outletmanager_query);

                        //  dd($outletmanager_query);

                        $amenities_query = "SELECT  
                        (
                        SELECT COUNT(1)
                        FROM amenities
                        WHERE date_trunc('day', created_date) = date_trunc('day', CURRENT_DATE) AND created_by = $vendor_id ) AS day_count,

                        (SELECT COUNT(1)
                        FROM amenities
                        WHERE date_trunc('WEEK', created_date) = date_trunc('WEEK', CURRENT_DATE) AND created_by = $vendor_id ) AS week_count,

                        (SELECT COUNT(1)
                        FROM amenities
                        WHERE date_trunc('month', created_date) = date_trunc('month', CURRENT_DATE) AND created_by = $vendor_id ) AS month_count,

                        (SELECT COUNT(1)
                        FROM amenities
                        WHERE date_trunc('year', created_date) = date_trunc('year', CURRENT_DATE) AND created_by = $vendor_id ) AS year_count,
                        COUNT(1) AS total_count
                        FROM amenities WHERE created_by = $vendor_id ";
                        $amenities_query = DB::select($amenities_query);

                        $accomodation_type_query = "SELECT  
                        (
                        SELECT COUNT(1)
                        FROM accomodation_type
                        WHERE date_trunc('day', created_date) = date_trunc('day', CURRENT_DATE) AND created_by = $vendor_id ) AS day_count,

                        (SELECT COUNT(1)
                        FROM accomodation_type
                        WHERE date_trunc('WEEK', created_date) = date_trunc('WEEK', CURRENT_DATE) AND created_by = $vendor_id ) AS week_count,

                        (SELECT COUNT(1)
                        FROM accomodation_type
                        WHERE date_trunc('month', created_date) = date_trunc('month', CURRENT_DATE) AND created_by = $vendor_id ) AS month_count,

                        (SELECT COUNT(1)
                        FROM accomodation_type
                        WHERE date_trunc('year', created_date) = date_trunc('year', CURRENT_DATE) AND created_by = $vendor_id ) AS year_count,
                        COUNT(1) AS total_count
                        FROM accomodation_type WHERE created_by = $vendor_id ";
                        $accomodation_type_query = DB::select($accomodation_type_query);

                        $room_type_query = "SELECT  
                        (
                        SELECT COUNT(1)
                        FROM room_type
                        WHERE date_trunc('day', created_date) = date_trunc('day', CURRENT_DATE) AND property_id = $property_id ) AS day_count,

                        (SELECT COUNT(1)
                        FROM room_type
                        WHERE date_trunc('WEEK', created_date) = date_trunc('WEEK', CURRENT_DATE) AND property_id = $property_id ) AS week_count,

                        (SELECT COUNT(1)
                        FROM room_type
                        WHERE date_trunc('month', created_date) = date_trunc('month', CURRENT_DATE) AND property_id = $property_id ) AS month_count,

                        (SELECT COUNT(1)
                        FROM room_type
                        WHERE date_trunc('year', created_date) = date_trunc('year', CURRENT_DATE) AND property_id = $property_id ) AS year_count,
                        COUNT(1) AS total_count
                        FROM room_type WHERE property_id = $property_id ";
                        $room_type_query = DB::select($room_type_query);

                        $rooms_query = "SELECT  
                        (
                        SELECT COUNT(1)
                        FROM rooms
                        WHERE date_trunc('day', created_at) = date_trunc('day', CURRENT_DATE) AND property_id = $property_id ) AS day_count,

                        (SELECT COUNT(1)
                        FROM rooms
                        WHERE date_trunc('WEEK', created_at) = date_trunc('WEEK', CURRENT_DATE) AND property_id = $property_id ) AS week_count,

                        (SELECT COUNT(1)
                        FROM rooms
                        WHERE date_trunc('month', created_at) = date_trunc('month', CURRENT_DATE) AND property_id = $property_id ) AS month_count,

                        (SELECT COUNT(1)
                        FROM rooms
                        WHERE date_trunc('year', created_at) = date_trunc('year', CURRENT_DATE) AND property_id = $property_id ) AS year_count,
                        COUNT(1) AS total_count
                        FROM rooms WHERE property_id = $property_id ";
                        $rooms_query = DB::select($rooms_query);

                         $housekeeping_staffs_query = "SELECT  
                        (
                        SELECT COUNT(1)
                        FROM housekeeping_staffs
                        WHERE date_trunc('day', created_at) = date_trunc('day', CURRENT_DATE) AND vendor_id = $vendor_id ) AS day_count,

                        (SELECT COUNT(1)
                        FROM housekeeping_staffs
                        WHERE date_trunc('WEEK', created_at) = date_trunc('WEEK', CURRENT_DATE) AND vendor_id = $vendor_id ) AS week_count,

                        (SELECT COUNT(1)
                        FROM housekeeping_staffs
                        WHERE date_trunc('month', created_at) = date_trunc('month', CURRENT_DATE) AND vendor_id = $vendor_id ) AS month_count,

                        (SELECT COUNT(1)
                        FROM housekeeping_staffs
                        WHERE date_trunc('year', created_at) = date_trunc('year', CURRENT_DATE) AND vendor_id = $vendor_id ) AS year_count,
                        COUNT(1) AS total_count
                        FROM housekeeping_staffs WHERE vendor_id = $vendor_id ";
                        $housekeeping_staffs_query = DB::select($housekeeping_staffs_query);
                        
                            $outlets = DB::table('outlets')->select('outlets.id')->where('vendor_id',$vendor_id)->get();
                            //print_r($outlets);exit;
                            $outlet_managers = DB::table('outlet_managers')->select('outlet_managers.id')->where('vendor_id',$vendor_id)->get();

                            $amenities = DB::table('amenities')->select('amenities.id')->where('created_by',$vendor_id)->get();

                            $accomodation_type = DB::table('accomodation_type')->select('accomodation_type.id')->where('created_by',$vendor_id)->get();

                            $room_type = DB::table('room_type')->select('room_type.id')->where('property_id',$property_id)->get();

                            $rooms = DB::table('rooms')->select('rooms.id')->where('property_id',$property_id)->get();

                            $housekeeping_staffs = DB::table('housekeeping_staffs')->select('housekeeping_staffs.id')->where('vendor_id',$vendor_id)->get();

                            $language_id = getAdminCurrentLang();
                            $currency_symbol = getCurrency($language_id);
                            $currency_side   = getCurrencyPosition()->currency_side;

                            return view('vendors.reports_analysis.home')->with('outlets', $outlets)
                                                       ->with('currency_symbol', $currency_symbol)
                                                       ->with('currency_side', $currency_side)
                                                       ->with('outlet_managers', $outlet_managers)
                                                       ->with('outlets_period_count',$outlets_period_count)
                                                       ->with('outletmanager_query', $outletmanager_query)
                                                       ->with('amenities', $amenities)
                                                       ->with('amenities_query', $amenities_query)
                                                       ->with('accomodation_type', $accomodation_type)
                                                       ->with('accomodation_type_query', $accomodation_type_query)
                                                       ->with('room_type', $room_type)
                                                       ->with('room_type_query', $room_type_query)
                                                       ->with('rooms', $rooms)
                                                       ->with('rooms_query', $rooms_query)
                                                       ->with('vendor_booking_encoded',$vendor_booking_encoded)
                                                       ->with('vendors_orderscount_count_encoded',$vendors_orderscount_count_encoded)
                                                       ->with('staff_count_encoded',$staff_count_encoded)
                                                       ->with('months_data_check',$data_check_months)
                                                       ->with('months_encoded',$months_encoded)
                                                       ->with('housekeeping_staffs',$housekeeping_staffs)
                                                       ->with('housekeeping_staffs_query',$housekeeping_staffs_query)
                                                       ->with('total_sales', $total_sales)
                                                       ->with('total_canceled', $total_canceled)
                                                       ->with('total_walkin', $total_walkin)
                                                       ->with('total_denied', $total_denied)
                                                       ->with('total_complete', $total_complete)
                                                       ->with('total_confirmed', $total_confirmed)
                                                       //   ->with('total_missed', $total_missed)
                                                       //   ->with('total_initiated', $total_initiated)
                                                       ->with('total_pending', $total_pending)
                                                       ->with('colors',$colors_list);
                                                       
                        }
                    }
                    /*
                     * Log out vendor and current sessions
                    */
                public function logout() 
                    {

                        Session::flush();
                        return Redirect::to('vendors/login')->with('status', 'Your are now logged out!');
                    }
                    
                    /*
                     * Render the forgot password view
                    */
                public function forgot() 
                    {
                        return view('vendors.email');    
                    }
                    
                    /*
                     * Forgot password details & post data
                    */
                public function forgot_details(Request $data) 
                    {


                        $datas = Input::all();
                        // validate
                        // read more on validation at http://laravel.com/docs/validation        
                        $validation = Validator::make($data->all(), array(
                            'email' => 'required|email'
                        ));
                        // process the validation
                        if ($validation->fails()) {
                            //return redirect('create')->withInput($datas)->withErrors($validation);
                            return Redirect::back()->withErrors($validation)->withInput();
                        } else {
                            // Input Email value of form. which is checked if it is present in table or not.
                            $email = Input::get('email');
                            $vendors=DB::table('vendors')
                                        ->select('vendors.user_id','vendors.first_name','vendors.email')
                                        ->where('email',$email)
                                        ->where('active_status',1)
                                        ->get();
                            if(count($vendors) > 0){
                                $vendors_data = $vendors[0];
                                //Generate random password string
                                $string = str_random(8);
                                $pass_string = md5($string);
                                //Sending the mail to vendors
                                $template=DB::table('email_templates')
                                        ->select('*')
                                        ->where('template_id','=',self::VENDORS_FORGOT_PASSWORD_EMAIL_TEMPLATE)
                                        ->get();
                                if(count($template)){
                                   $from = $template[0]->from_email;
                                   $from_name = $template[0]->from;
                                   $subject = $template[0]->subject;
                                   if(!$template[0]->template_id){
                                       $template = 'mail_template';
                                       $from = getAppConfigEmail()->contact_email;
                                       $subject = getAppConfig()->site_name." Password Request Details";
                                       $from_name = "";
                                   }
                                   $content = array("name" => $vendors_data->first_name,"email"=>$vendors_data->email,"password"=>$string);
                                   $email = smtp($from,$from_name,$vendors_data->email,$subject,$content,$template);
                                }
                                //Update random password to vendors table to coreesponding vendor id
                                $res = DB::table('vendors')
                                    ->where('id', $vendors_data->id)
                                    ->update(['password' => $pass_string]);
                                //Show the flash message if successful logged in
                                Session::flash('status', trans('messages.Password was sent your email successfully.'));
                                return Redirect::to('vendors/login');
                            } else {
                                $validation->errors()->add('email', 'These credentials do not match our records.');
                                return Redirect::back()->withErrors($validation)->withInput();
                            }
                        }
                    }
                    
                    /*
                     * Render the change password view
                    */
                public function change_password() 
                    {
                         // if (Auth::guest())
                         //            {
                         //                return redirect()->guest('admin/login');
                         //            }
                        if (!Session::get('vendor_id')){
                            return redirect()->guest('vendors/login');
                        }else{

                            return view('vendors.reset');
                        }
                    }

                    public function change_details(Request $data) 
                    {
                        $post_data = $data->all();
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }
                        // validate
                        // read more on validation at http://laravel.com/docs/validation
                        $validation = Validator::make($data->all(), array(
                            'password' => 'min:6|max:16',
                            'password_confirmation' => 'required_with:password|same:password|min:6|max:16'
                        ));
                        // This is to check for Validation Errors.
                        $error = array();
                        // process the validation
                        if ($validation->fails()) {
                            foreach ($validation->errors()->messages() as $key => $value)
                            {
                                $error[] = is_array($value) ? implode(',', $value) : $value;
                            }
                            //Associative array of error response.
                            $result = array("httpCode" => 400, "status" => false,
                                "errors" => $error);
                            return response()->json($result);
                            //return Redirect::back()->withErrors($validation)->withInput();
                        } else {

                            //Get new password details from posts
                            $old_password = md5($post_data['old_password']);

                            $session_userid = Session::get('vendor_id');
                            $vendors=DB::table('vendors_view')
                                        ->select('vendors_view.id','vendors_view.first_name','vendors_view.email','vendors_view.mobile_number')
                                        ->where('id',$session_userid)
                                        ->where('password',$old_password)
                                        // ->where('original_password',$password)
                                        ->where('active_status',1)
                                        ->get();

                            if(count($vendors) > 0){
                               
                                $vendor_id = Session::get('vendor_id');

                                $user_details = DB::table('users')->where('id',$vendor_id)
                                ->select('country_code','mobile_number')->get()->toArray();
                                if(count($user_details) > 0 ){
                                    $country_code = $user_details[0]->country_code;
                                    $mobile_number = $user_details[0]->mobile_number;


                                        if(!empty($country_code) && !empty($mobile_number)){
                                            $mobile = $country_code.$mobile_number;
                                             $otp_number = getOTPNumber(6);

                                             $default_sitename = getAppConfig()->site_name;
                                             // $templates = get_sms_templates(self::VENDOR_CHANGE_PASSWORD_OTP_SMS_TEMPLATE,$default_sitename,$otp_number);

                                             // print_r($default_sitename);exit;

                                              $msg = DB::table('sms_templates')
                                                ->select('message')
                                                ->where('id','=',self::VENDOR_CHANGE_PASSWORD_OTP_SMS_TEMPLATE)
                                                ->first();

                                              $result = $msg->message;
                                              $message = str_replace('${otp_number}',$otp_number,$result);
                                              $message = str_replace('${SITE_NAME}',$default_sitename,$message);

                                             $send_sms = send_sms($mobile,$message,$result);
                                             // print_r($send_sms);exit;
                                            
                                             if($send_sms == 2){
                                                    $vendors = Vendors::where('user_id',$vendor_id)->first();
                                                    $vendors->otp_number = $otp_number;
                                                    $vendors->otp_verify_status = 0;
                                                    $vendors->save(); 

                                                $error = array('The otp has been sent to your mobile. Please verify otp to change your password.');

                                                $result = array("httpCode" => 200,"user_id" => $vendor_id, "status" => true,"errors" => $error);
                                                return response()->json($result); 

                                             } else {
                                                $error = array($send_sms);
                                                $result = array("httpCode" => 400, "status" => false,"errors" => $error);
                                                return response()->json($result);
                                             }   

                                        }else{
                                            $error = array("Your mobile number was not found in our records.");
                                            $result = array("httpCode" => 400, "status" => false,
                                            "errors" => $error);
                                            return response()->json($result);
                                        }
                                }else{
                                    $error = array("User details not found.");
                                    $result = array("httpCode" => 400, "status" => false,
                                    "errors" => $error);
                                    return response()->json($result);
                                    }
                            }else{
                                $error = array("Please enter the correct old password.");
                                $result = array("httpCode" => 400, "status" => false,
                                "errors" => $error);
                                return response()->json($result);
                            }
                        }
                    }

                    public function verify_otp_password(Request $data){
                        $post_data = $data->all();

                        //To get otp number
                        $current_otp = $post_data['otp_number'];
                        $otp_id = $post_data['verify_otp_id']; // Vendors id = 2
                        $password = $post_data['password'];
                        if($otp_id > 0){
                            $vendor = Vendors::where('user_id',$otp_id)->first();
                            //echo "<pre>";print_r($user);die;
                            if(count($vendor) > 0){
                                $old_otp = $vendor->otp_number;  //variable to store DB otp No.
                                if($current_otp == $old_otp){ // check the condition
                                     $vendor->otp_verify_status = 1; // changing the status
                                     $vendor->save();

                                     $user = Users::find($otp_id);
                                     $user->password = md5($password);
                                     $user->save();

                                     $vendors = Vendors::where('user_id',$otp_id)
                                    ->update(['original_password' => $post_data['password']]);

                                    $vendors_data = vendors_view::where('user_id',$otp_id)->first();

                                    //  print_r($vendors_data);exit;
                                    //  CHANGE PWD Mail
                                    $template=DB::table('email_templates')
                                            ->select('*')
                                            ->where('template_id','=',self::VENDORS_CHANGE_PASSWORD_EMAIL_TEMPLATE)
                                            ->get();
                                    if(count($template)){
                                       $from = $template[0]->from_email;
                                       $from_name = $template[0]->from;
                                       $subject = $template[0]->subject;
                                       if(!$template[0]->template_id){
                                           $template = 'mail_template';
                                           $from = getAppConfigEmail()->contact_email;
                                           $subject = getAppConfig()->site_name." New Password Request Updated";
                                           $from_name = "";
                                       }
                                       $content = array("name" => $vendors_data->first_name,"email"=>$vendors_data->email,"password"=>$password);
                                       $email = smtp($from,$from_name,$vendors_data->email,$subject,$content,$template);
                                    }                                    

                                    $error = array("The otp verification is success.");
                                    $result = array("httpCode" => 200, "status" => true,
                                                     "errors" => $error);
                                } else {

                                    $error = array("The otp you have entered is not matched with our records.");
                                    $result = array("httpCode" => 400, "status" => false,
                                                     "errors" => $error);
                                }
                            } else {

                            $error = array("We don't have any matching records for your request.");
                            $result = array("httpCode" => 400, "status" => false,
                                "errors" => $error);

                            }
                        } else {
                            $error = array("We don't have any matching records for your request.");
                            $result = array("httpCode" => 400, "status" => false,
                                "errors" => $error);
                                
                        }
                    return response()->json($result);
                } 

                    public function resend_otp(Request $data){
                            $post_data = $data->all();
                      
                            //To get otp number
                            $otp_id = $post_data['verify_otp_id'];
                            if($otp_id > 0){

                                // Check wheather the OTP id is present in vendor table.
                                $vendor = Vendors::where('user_id',$otp_id)->select('otp_number')->get()->toArray();

                                // If OTP id is present the get the OTP Number from table and count.
                                 if(count($vendor) > 0){
                                    $otp_number = $vendor[0]['otp_number'];

                                     $vendor_id = Session::get('vendor_id');

                                    $user_details = DB::table('users')->where('id',$vendor_id)
                                    ->select('country_code','mobile_number')->get()->toArray();
                                    

                                    $default_sitename = getAppConfig()->site_name;

                                    $msg = DB::table('sms_templates')
                                                ->select('message')
                                                ->where('id','=',self::VENDOR_CHANGE_PASSWORD_OTP_SMS_TEMPLATE)
                                                ->first();


                                              $result = $msg->message;
                                              $message = str_replace('${otp_number}',$otp_number,$result);
                                              $message = str_replace('${SITE_NAME}',$default_sitename,$message);

                                              
                                if(count($user_details) > 0 ){
                                    $country_code = $user_details[0]->country_code;
                                    $mobile_number = $user_details[0]->mobile_number;
                                }    
                                if(!empty($country_code) && !empty($mobile_number)){
                                    $mobile = $country_code.$mobile_number;
                                    // $msg = "The otp to change your Password is".$otp_number;
                                    $result = send_sms($mobile,$message);
                                }
                                    if($result == 2) {

                                    $result = array("httpCode" => 200, "status" => true,
                                                     "errors" => "The otp has been resent to your mobile number. Please verify your otp number.");
                                } else {
                                        $result = array("httpCode" => 400, "status" => false, 
                                        "errors" => $result); 
                                    }    
                                } else {
                                     $result = array("httpCode" => 400, "status" => false, 
                                "errors" => "Mobile number and country code was not found.");
                                }
                            } else {
                                 $result = array("httpCode" => 400, "status" => false, 
                                "errors" => "We don't have any matching records for your request.");
                            }
                        return response()->json($result);
                    } 

                    /*
                     * Vendor Edit Profile
                    */
                public function edit_profile() 
                    {

                        // if (Auth::guest())
                        //     {
                        //         return redirect()->guest('vendors/login');
                        //     }
                        if (!Session::get('vendor_id')){
                            return redirect()->guest('vendors/login');
                        }
                        else
                        {
                            //Get session user details.        
                            $id=Session::get('vendor_id');

                            //Get vendor details
                            $vendors = Vendors_view::find($id);
                            if(!count($vendors)){
                                 Session::flash('message', 'Invalid Vendor Details'); 
                                 return Redirect::to('vendors/dashboard');    
                            }
                            //Get the vendors information
                            $info = new Vendors_infos;
                            //Get countries data
                            $countries = getCountryLists();
                            //Get the categories data with type vendor
                            $categories= getCategoryLists(2);
                            return view('vendors.edit_profile')->with('countries', $countries)->with('categories', $categories)->with('data', $vendors)->with('infomodel', $info);
                        }
                    }
                    
                    /**
                     * Update the specified blog in storage.
                     *
                     * @param  int  $id
                     * @return Response
                     */
                public function update_profile($id)
                    {
                        $data = Input::all();
                        //  echo "<pre>";
                        //  print_r($data);exit;
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }

                        $fields['first_name'] = Input::get('first_name');
                        $fields['last_name'] = Input::get('last_name');
                        $fields['email'] = Input::get('email');
                        $fields['mobile_number'] = Input::get('mobile_number');
                        $fields['phone_number'] = Input::get('phone_number');
                        $fields['country'] = Input::get('country');
                        $fields['city'] = Input::get('city');
                        //  $fields['category'] = Input::get('category');
                        $fields['delivery_time'] = Input::get('delivery_time');
                        $fields['pickup_time'] = Input::get('pickup_time');
                        $fields['cancel_time'] = Input::get('cancel_time');
                        $fields['return_time'] = Input::get('return_time');
                        $fields['delivery_charges_fixed'] = Input::get('delivery_charges_fixed');
                        $fields['delivery_cost_variation'] = Input::get('delivery_cost_variation');
                        $fields['service_tax'] = Input::get('service_tax');
                        $fields['contact_email'] = Input::get('contact_email');
                        $fields['contact_address'] = Input::get('contact_address');
                        $fields['featured_vendor'] = Input::get('featured_vendor');
                        $fields['active_status'] = Input::get('active_status');
                        $fields['logo'] = Input::file('logo');
                        $fields['featured_image'] = Input::file('featured_image');
                        $rules = array(
                            'first_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3|max:32',
                            'last_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:1|max:32',
                            'email' => 'required|email|max:255|unique:vendors_view,email,'.$id,
                            'mobile_number' => 'required|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
                            'phone_number' => 'required|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
                            //'country' => 'required',
                            'city' => 'required',
                            //  'category' => 'required',
                            // 'delivery_time' => 'required|numeric|min:0',
                            // 'pickup_time' => 'required|numeric|min:0',
                            // 'cancel_time' => 'required|numeric|min:0',
                            // 'return_time' => 'required|numeric|min:0',
                            // 'delivery_charges_fixed' => 'required|numeric|min:0',
                            // 'delivery_cost_variation' => 'required|numeric|min:0',
                            // 'service_tax' => 'required|numeric|min:0.1|max:99.9',
                            // 'contact_email' => 'required|email',
                            'contact_address' => 'required',
                            //'featured_vendor' => 'required',
                            //'active_status' => 'required',
                            //'logo' => 'mimes:png,jpg,jpeg,bmp',
                            //'featured_image' => 'mimes:png,jpg,jpeg,bmp'
                        );
                       /* $vendor_name = Input::get('vendor_name');
                        foreach ($vendor_name  as $key => $value) {
                            $fields['vendor_name'.$key] = $value;
                            $rules['vendor_name'.'1'] = 'required|regex:/(^[A-Za-z0-9 ]+$)+/';
                        }
                        $vendor_description = Input::get('vendor_description');
                        foreach ($vendor_description  as $key => $value) {
                            $fields['vendor_description'.$key] = $value;
                            $rules['vendor_description'.'1'] = 'required';
                        }*/
                        $validator = Validator::make($fields, $rules);    
                        // process the validation
                        if ($validator->fails())
                        { 
                            return Redirect::back()->withErrors($validator)->withInput();
                        } else {
                            try{


                                $users = Users::find($id);

                                $users->name = $_POST['first_name'];
                                $users->first_name = $_POST['first_name'];
                                $users->last_name = $_POST['last_name'];
                                $users->email = $_POST['email'];
                                //$users->password = md5($_POST['password']);
                                $users->mobile_number = $_POST['mobile_number'];
                                $users->country_id = 1;
                                $users->city_id = $_POST['city'];
                                //$users->created_date = date('Y-m-d H:i:s');
                                $users->updated_date = date('Y-m-d H:i:s');
                                //$users->created_by = Auth::user()->id;

                                $users->save();

                                $this->vendor_insert_update($users->id,$data); 

                                $this->vendor_save_after_infos($users->id,$users,$_POST,1);
                                

                                Session::put('user_name', ucfirst($_POST['first_name']));
                            }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                            Session::flash('message', trans('messages.Profile has been successfully updated'));
                            return Redirect::to('vendors/editprofile');
                        }
                }


            public function vendor_insert_update($id,$data)
                {
                    if (Session::get('vendor_type') != 2){
                    $vendor_name = $_POST['vendor_name'];

                    $Vendors = Vendors::where('user_id',$id)
                                ->update([
                                    'phone_number'   => $_POST['phone_number'],
                                    //  'delivery_time'  => $_POST['delivery_time'],
                                    //  'pickup_time'    => $_POST['pickup_time'],
                                    //  'category_ids'   => implode(',',$_POST['category']),
                                    // 'cancel_time'    => $_POST['cancel_time'],
                                    // 'return_time'    => $_POST['return_time'],                                    
                                    'contact_email'  => $_POST['contact_email'],
                                    'contact_address'=> $_POST['contact_address'],
                                    // 'active_status'  => isset($_POST['active_status'])?$_POST['active_status']:0,
                                    'featured_vendor'=> isset($_POST['featured_vendor'])?$_POST['featured_vendor']:0,
                                    'latitude'       => $_POST['latitude'],
                                    'longitude'      => $_POST['longitude'],
                                    'vendor_key'     => strtoupper(substr($vendor_name[1],0,3)),
                                    ]);
                                }
                                
            /*
                    $Vendors->delivery_charges_fixed = $_POST['delivery_charges_fixed'];
                    $Vendors->delivery_cost_variation = $_POST['delivery_cost_variation'];       
                    $Vendors->service_tax = !empty($_POST['service_tax'])?$_POST['service_tax']:0;
            */

                    if(isset($_FILES['logo']['name']) && $_FILES['logo']['name']!=''){
                                //get last insert id
                                $destinationPath = base_path() .'/public/assets/admin/base/images/vendors/logos'; // upload path
                                $imageName = strtolower($id . '_' . $_FILES['logo']['name']);
                                // echo $imageName;die;                               
                                $data['logo']->move($destinationPath, $imageName);

                                $destinationPath1 = url('/assets/admin/base/images/vendors/logos/'.$imageName.'');
                                Image::make( $destinationPath1 )->fit(253, 133)->save(base_path() .'/public/assets/admin/base/images/vendors/logos/'.$imageName)->destroy();

                                $Vendors = Vendors::where('user_id',$id)->update(['logo_image' => $imageName]);

                                //  AFTER IMAGE UPLOAD NEED TO CHANGE PROFILE IMG
                                Session::put('vendor_image', $imageName);                                
                            } 



                    if(isset($_FILES['featured_image']['name']) && $_FILES['featured_image']['name']!=''){

                                //  IMG FILES COMING THROUGH ARRAY FORMAT SO, IT IS COMMENTED

                                //  $imageName = strtolower($id . '.' . $data->image('featured_image')->getClientOriginalExtension());

                                $imageName = strtolower($id . '.' . $data['featured_image']->getClientOriginalExtension());


                                //  $imageName = strtolower($id . '_' . $_FILES['featured_image']['name']);

                                $data['featured_image']->move(
                                    base_path() . '/public/assets/admin/base/images/vendors/', $imageName
                                );
                                $destinationPath2 = url('/assets/admin/base/images/vendors/'.$imageName.'');
                                $size=getImageResize('VENDOR');
                                Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/vendors/list/'.$imageName)->destroy();
                                Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/vendors/detail/'.$imageName)->destroy();
                                Image::make( $destinationPath2 )->fit($size['THUMB_WIDTH'], $size['THUMB_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/vendors/thumb/'.$imageName)->destroy();
                                Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/vendors/thumb/detail/'.$imageName)->destroy();

                                $Vendors = Vendors::where('user_id',$id)->update(['featured_image' => $imageName]);
                            }                       
                }

            public static function vendor_save_after_infos($user_id,$object,$post,$method=0)
                    {

                        if(isset($post['vendor_name']) && isset($post['vendor_description'])){
                            $vendor_name = $post['vendor_name'];
                            $vendor_description = $post['vendor_description'];
                            try{
                                $data = Vendors_infos::where('vendors_view_id',$user_id)->first();
                                if(count($data)>0){
                                    $data->delete();
                                }
                                $languages = DB::table('languages')->where('status', 1)->get();
                                foreach($languages as $key => $lang){
                                    if((isset($vendor_name[$lang->id]) && $vendor_name[$lang->id]!="") && (isset($vendor_description[$lang->id]) && $vendor_description[$lang->id]!="")){
                                        $infomodel = new Vendors_infos;

                                        $infomodel->vendors_view_id = $user_id;
                                        $infomodel->lang_id = $lang->id;
                                        $infomodel->id = $object->id; 
                                        $infomodel->vendor_name = $vendor_name[$lang->id];
                                        $infomodel->vendor_description = $vendor_description[$lang->id];
                                        $infomodel->save();
                                    }
                                }
                            }catch(Exception $e) {
                                
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                        }
                    }   

                    /**
                     * Show the application outlets.
                     * @return \Illuminate\Http\Response
                     */
            public function branches()
                    {
                        $id = Session::get('vendor_id');
                        if (!$id){
                            return redirect()->guest('vendors/login');
                        }
                        // 
                        if(!has_staff_permission('vendor/outlets'))
                        {
                        return view('errors.405');
                        }
                        else{   

                            $property_id = Session::get('property_id');

                            if($property_id =="" || $property_id == 0)
                            {
                               $outlet_property_id = get_first_outlet($id);
                               Session::put('property_id',$outlet_property_id);                         
                            }    
                                                    
                            return view('vendors.outlets.list');
                        }
                    }

                    /**
                     * Process datatables ajax request.
                     *
                     * @return \Illuminate\Http\JsonResponse
                     */
            public function anyAjaxBranches()
                    {

                        $vendor_id = Session::get('vendor_id');

                        $property_id = Session::get('property_id');                      

                        
                        //  print_r($id); exit;

                        if(Session::get('vendor_type') == 2 )
                            {
                                $vendor_id = Session::get('created_vendor_id');
                                //  print_r($id); exit;
                            }

                        if($property_id =="" || $property_id == 0)
                        {
                               $outlet_property_id = get_first_outlet($vendor_id); 
                               Session::put('property_id',$outlet_property_id);                         
                        }   

                        $query = '"vendors_infos"."lang_id" = (case when (select count(lang_id) as totalcount from vendors_infos where vendors_infos.lang_id = '.getAdminCurrentLang().' and vendors_view.id = vendors_infos.vendors_view_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $query1 = '"outlet_infos"."language_id" = (case when (select count(id) as totalcount from outlet_infos where outlet_infos.language_id = '.getAdminCurrentLang().' and outlets.id = outlet_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $vendors = Outlets::Leftjoin('vendors_view','vendors_view.id','=','outlets.vendor_id')
                                     ->Leftjoin('vendors_infos','vendors_infos.vendors_view_id','=','vendors_view.id')
                                      ->Leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                      ->select('vendors_infos.vendor_name','outlets.id','outlets.active_status','outlets.modified_date','outlets.contact_email','outlets.contact_phone','outlet_infos.contact_address','outlets.created_date','outlets.modified_date','outlet_infos.outlet_name')
                                    ->whereRaw($query)
                                    ->whereRaw($query1)
                                    ->where('outlets.vendor_id','=',$vendor_id)
                                    ->orderby('outlets.id','desc')
                                    ->get();
                                    //  print_r($vendors);exit;
                        return Datatables::of($vendors)->addColumn('action', function ($vendors) {
                            if(has_staff_permission('vendor/edit_outlet')){
                                $html='<div class="btn-group"><a href="'.URL::to("vendor/edit_outlet/".$vendors->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                            <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu xs pull-right" role="menu">
                                            <li><a href="'.URL::to("vendor/outlet_details/".$vendors->id).'" class="view-'.$vendors->id.'" title="'.trans("messages.View").'"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;'.@trans("messages.View").'</a></li>
                                            <li><a href="'.URL::to("vendor/delete_outlet/".$vendors->id).'" class="delete-'.$vendors->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                            </ul>
                                        </div><script type="text/javascript">
                                        $( document ).ready(function() {
                                        $(".delete-'.$vendors->id.'").on("click", function(){
                                             return confirm("'.trans("messages.Are you sure want to delete?").'");
                                        });});</script>';
                                return $html;
                                }
                            })
                            ->addColumn('active_status', function ($vendors) {
                                if($vendors->active_status==0):
                                    $data = '<span class="label label-warning">'.trans("messages.Inactive").'</span>';
                                elseif($vendors->active_status==1):
                                    $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                                elseif($vendors->active_status==2):
                                    $data = '<span class="label label-danger">'.trans("messages.Delete").'</span>';
                                endif;
                                return $data;
                            })
                            ->addColumn('modified_date', function ($vendors) {
                            $data = '-';
                            if($vendors->modified_date != ''):
                            $data = $vendors->modified_date;
                            endif;
                            return $data;
                            })
                            ->make(true);
                    }

                    /**
                     * Create the specified outlet in view.
                     * @param  int  $id
                     * @return Response
                     */
                    
            public function branch_create()
                    {

                        $id=Session::get('vendor_id');
                        if (!$id)
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/create_outlet'))
                        {
                        return view('errors.405');
                        }
                        else {
                            //Get countries data
                            $countries = getCountryLists();
                            //  $city = getCityList();
                            $pricecategories  = getCategoryLists(6);
                            $placecategories  = getCategoryLists(7);
                            $accmodation_type = getAllAccomodationTypes();

                                 // dd($accmodation_type);
                            if(count($accmodation_type) == 0)
                            {
                                Session::flash('no-accmodation-type','Add Accomodation First');
                                return redirect::to('vendor/accomodation');                                
                            }
                            $amty = getAllAmenityTypes();

                            if(count($amty) == 0)
                            {
                                Session::flash('no-amenities','Add Amenities First');
                                return redirect::to('vendor/amenities');                                
                            }
                            //  dd($amty);

                            return view('vendors.outlets.create')->with('countries', $countries)
                                            ->with('pricecategories',$pricecategories)
                                            ->with('placecategories',$placecategories)
                                            ->with('accmodation_type',$accmodation_type)
                                            ->with('amty',$amty);
                        }
                    }

                    /**
                     * Add the specified outlet in storage.
                     * @param  int  $id
                     * @return Response
                     */
            public function branch_store(Request $data)
                    { 
                    
                        $id=Session::get('vendor_id');
                        if (!$id)
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/outlets'))
                        {
                            return view('errors.405');
                        }

                        $vendor_id = Session::get('vendor_id');
                        $total_properties = getProperty(1,$vendor_id);
                        $property_limit = getProperty(2,$vendor_id);

                        if($total_properties >= $property_limit){
                            $errors = array('You have reached your maximum property limit.');
                            return Redirect::back()->withErrors($errors)->withInput();
                        }

                        $fields['contact_phone_number'] = Input::get('contact_phone_number');
                        $fields['city'] = Input::get('city');
                        $fields['location'] = Input::get('location');
                        $fields['property_image'] = Input::file('property_image');
                        $fields['contact_email'] = Input::get('contact_email');
                        // $fields['contact_address'] = Input::get('contact_address');
                        $fields['latitude'] = Input::get('latitude');
                        $fields['longitude'] = Input::get('longitude');
                        $fields['active_status'] = Input::get('active_status');
                        $fields['pricecategories'] = Input::get('pricecategories');
                        $fields['placecategories'] = Input::get('placecategories');
                        $fields['accomodation_type'] = Input::get('accomodation_type');
                        $fields['amenity_type'] = Input::get('amenity_type');
                        $fields['directions'] = Input::get('directions');
                        $fields['landmark'] = Input::get('landmark');
                        $fields['outlet_name'] = Input::get('outlet_name');

                        $rules = array(
                            //  'country' => 'required',
                            'city' => 'required',
                            'location' => 'required',
                            'contact_phone_number' => 'required',
                            'contact_email' => 'required|email',
                            'property_image' => 'required|mimes:png,jpg,jpeg,bmp',
                            'pricecategories' => 'required',
                            'placecategories' => 'required',                            
                            'accomodation_type' => 'required',                            
                            'amenity_type' => 'required',
                            'directions' => 'required',
                            'landmark' => 'required',
                            'outlet_name' => 'required',
                            //'contact_address' => 'required',
                            /*
                                'delivery_areas' => 'required',
                                'delivery_time' => 'required|numeric|min:0',
                                'pickup_time' => 'required|numeric|min:0',
                                'cancel_time' => 'required|numeric|min:0',
                                'return_time' => 'required|numeric|min:0',
                                'delivery_charges_fixed' => 'required|numeric|min:0',
                                'delivery_cost_variation' => 'required|numeric|min:0',
                                'service_tax' => 'required|numeric|min:0.1|max:99.9',
                                'minimum_order_amount' => 'required|numeric|min:0',
                            */
                        );

                    
                        if(isset($post_data['place_name']) && $post_data['place_name']!=''){

                            for($i=0; $i<count($post_data['place_name']); $i++)
                            {
                                $rules['place_name.'.$i] = 'required';
                            }
                        }else {
                            //  $rules['amenities_name'] = 'required';
                        }

                        if(isset($post_data['places']) && $post_data['places']!=''){

                            for($i=0; $i<count($post_data['places']); $i++)
                            {
                                $rules['places.'.$i] = 'required';
                            }
                        }else {
                            //  $rules['amenities_image'] = 'required';
                        }

                        if(isset($post_data['distance']) && $post_data['distance']!=''){

                            for($i=0; $i<count($post_data['distance']); $i++)
                            {
                                $rules['distance.'.$i] = 'required';
                            }
                        }else {
                            //  $rules['amenities_image'] = 'required';
                        }                            


                        $outlet_name = Input::get('outlet_name');
                        foreach ($outlet_name  as $key => $value)
                        {
                            $fields['outlet_name'.$key] = $value;
                            $rules['outlet_name'.'1']   = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:outlet_infos,outlet_name';
                        }

                        $contact_address = Input::get('contact_address');
                        foreach ($contact_address  as $key => $value)
                        {
                            $fields['contact_address'.$key] = $value;
                            $rules['contact_address'.'1']   = 'required';
                        }

                        $hotel_rules = Input::get('hotel_rules');
                        foreach ($hotel_rules  as $key => $value)
                        {
                            $fields['hotel_rules'.$key] = $value;
                            $rules['hotel_rules'.'1']   = 'required';
                        }

                        $cancellation_policy = Input::get('cancellation_policy');
                        foreach ($cancellation_policy  as $key => $value)
                        {
                            $fields['cancellation_policy'.$key] = $value;
                            $rules['cancellation_policy'.'1']   = 'required';
                        }

                        $highlights = Input::get('highlights');
                        foreach ($highlights  as $key => $value)
                        {
                            $fields['highlights'.$key] = $value;
                            $rules['highlights'.'1']   = 'required';
                        }

                        $near_by = Input::get('near_by');
                        foreach ($near_by  as $key => $value)
                        {
                            $fields['near_by'.$key] = $value;
                            $rules['near_by'.'1']   = 'required';
                        }

                        $facilities = Input::get('facilities');
                        foreach ($facilities  as $key => $value)
                        {
                            $fields['facilities'.$key] = $value;
                            $rules['facilities'.'1']   = 'required';
                        }
                                                                      

                        $validation = Validator::make($fields, $rules);
                        // process the validation
                        if ($validation->fails())
                        { 
                            return Redirect::back()->withErrors($validation)->withInput();
                        } else {
                            //Store the data here with database
                            
                            try{

                                $Outlets = new Outlets;

// echo "<pre>";
//                     print_r($data->all());
//                     exit;
//                                 //  $amenity_type = Input::get('amenity_type');                          

                                //  $value = implode(',', $amenity_type);

                                $Outlets->amenity_type = isset($_POST['amenity_type']) ? implode(',',Input::get('amenity_type')):0;
                                $Outlets->accomodation_type = implode(',',Input::get('accomodation_type'));
                                $Outlets->contact_phone = $_POST['contact_phone_number'];
                                $Outlets->url_index     =  str_slug($_POST['outlet_name'][1]);
                                $Outlets->country_id = 1; //  1-> Malasyia  $_POST['country'];
                                $Outlets->city_id = $_POST['city'];
                                $Outlets->location_id = $_POST['location'];
                                $Outlets->vendor_id = Session::get('vendor_id');
                                $Outlets->price_category = implode(',',Input::get('pricecategories'));
                                $Outlets->place_category = implode(',',Input::get('placecategories'));
                                $Outlets->hotel_rules = $_POST['hotel_rules'][1];
                                $Outlets->directions = $_POST['directions'];
                                $Outlets->landmark = $_POST['landmark'];
                                $Outlets->highlights = $_POST['highlights'][1];
                                $Outlets->cancellation_policy = $_POST['cancellation_policy'][1];
                                $Outlets->facilities = $_POST['facilities'][1];
                                $Outlets->near_by = $_POST['near_by'][1];
                                // $Outlets->outlet_image = Input::file('property_image');
                                //  $Outlets->early_check_in = $_POST['early_check_in'];

                                /*
                                    $Outlets->delivery_time = $_POST['delivery_time'];
                                    $Outlets->pickup_time = $_POST['pickup_time'];
                                    $Outlets->delivery_areas = implode(',',$_POST['delivery_areas']);
                                    $Outlets->cancel_time = $_POST['cancel_time'];
                                    $Outlets->return_time = $_POST['return_time'];
                                    $Outlets->delivery_charges_fixed = $_POST['delivery_charges_fixed'];
                                    $Outlets->delivery_charges_variation = $_POST['delivery_cost_variation'];
                                    $Outlets->service_tax = $_POST['service_tax'];
                                    $Outlets->minimum_order_amount = $_POST['minimum_order_amount'];
                                */

                                $Outlets->created_date = date('Y-m-d H:i:s');
                                $Outlets->created_by = Session::get('vendor_id');
                                $Outlets->active_status = isset($_POST['active_status'])?$_POST['active_status']:'0';
                                $Outlets->online_payment = isset($_POST['online_payment'])?$_POST['online_payment']:0;
                                $Outlets->contact_email = $_POST['contact_email'];
                                $Outlets->latitude = $_POST['latitude'];
                                $Outlets->longitude = $_POST['longitude'];
   
                                $Outlets->save();
                                $last_insert_id = $Outlets->id;

                                $destinationPath = base_path().'/public/assets/admin/base/images/vendors/property_image/'; // upload path
                                
                                $imageName = strtolower($Outlets->id.'.'.$data->file('property_image')->getClientOriginalExtension());
                                $data->file('property_image')->move($destinationPath, $imageName);

                                $destinationPath1 = url('/assets/admin/base/images/vendors/property_image/'.$imageName);
                                Image::make( $destinationPath1 )->fit(550, 350)->save(base_path().'/public/assets/admin/base/images/vendors/property_image/list/'.$imageName);
                                // print_r($destinationPath1);exit;
                                $Outlets->outlet_image = $imageName;

                                if (!file_exists(base_path().'/public/assets/admin/base/images/vendors/property/'.$Outlets->id.'/'))
                                {
                                    $result = File::makeDirectory(base_path().'/public/assets/admin/base/images/vendors/property/'.$Outlets->id.'/', 0777, true);
                                }
                                Image::make( $destinationPath1 )->save(base_path().'/public/assets/admin/base/images/vendors/property/'.$Outlets->id.'/'.$imageName)->destroy();                                            
                                $Outlets->save();                         


                                for($i=0; $i<count($_POST['place_name']); $i++)
                                {

                                    $poi = new Place_of_interest;

                                    $poi->place_name  = $_POST['place_name'][$i];
                                    $poi->place  = $_POST['places'][$i];
                                    $poi->distance  = $_POST['distance'][$i];
                                    $poi->url_index =  $_POST['place_name'][$i] ? str_slug($_POST['place_name'][$i]): str_slug($_POST['place_name'][$i]);
                                    $poi->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                                    $poi->property_id = $Outlets->id;
                                    $poi->vendor_id = Session::get('vendor_id');
                                    $poi->created_date = date("Y-m-d H:i:s");
                                    $poi->save();

                                }                                

         
                                /*                                
                                    //  Store the opening timing schedules here
                                    $opening_time = $_POST['opening_timing'];
                                    $opentime_array = getDaysWeekArray();
                                    foreach($opening_time as $key => $values) {
                                        $Open_Timings = new Opening_timings;
                                        if(isset($values['istrue']) && $values['istrue']==1 && (array_key_exists($key, $opentime_array))) {
                                            $day_week = $opentime_array[$key];
                                            $Open_Timings->vendor_id = $last_insert_id;
                                            $Open_Timings->day_week = $day_week;
                                            $Open_Timings->start_time = $values['from'];
                                            $Open_Timings->end_time = $values['to'];
                                            $Open_Timings->created_date = date('Y-m-d');
                                            $Open_Timings->save();
                                        }
                                    }
                                                                
                                    //  Store the delivery timing schedules here
                                    $delivery_time = $_POST['delivery_timing'];
                                    $deliverytime_array = getDaysWeekArray();
                                    foreach($delivery_time as $key => $values) {
                                        $Delivery_Timings = new Delivery_timings;
                                        if(isset($values['istrue']) && $values['istrue']==1 && (array_key_exists($key, $deliverytime_array))) {
                                            $day_week = $deliverytime_array[$key];
                                            $Delivery_Timings->vendor_id = $last_insert_id;
                                            $Delivery_Timings->day_week = $day_week;
                                            $Delivery_Timings->start_time = $values['from'];
                                            $Delivery_Timings->end_time = $values['to'];
                                            $Delivery_Timings->created_date = date('Y-m-d');
                                            $Delivery_Timings->save();
                                        }
                                    }
                                */
                                $this->branch_save_after($Outlets,$_POST);
                                Session::flash('message', trans('messages.Property has been added successfully'));
                            }catch(Exception $e) {
                                    Log::Instance()->add(Log::ERROR, $e);
                            }
                            return Redirect::to('vendor/outlets');
                        }
                    }
                    /**
                     * add,edit datas  saved in main table 
                     * after inserted in sub tabel.
                     *
                     * @param  int  $id
                     * @return Response
                     */
            public static function branch_save_after($object,$post)
                    {
                        if(isset($post['outlet_name']))
                        {
                            $outlet_name = $post['outlet_name'];
                            $contact_address = $post['contact_address'];
                            try {
                                $data = Outlet_infos::find($object->id);
                                if(count($data)>0)
                                {
                                    $data->delete();
                                }
                                $languages = DB::table('languages')->where('status', 1)->get();
                                foreach($languages as $key => $lang)
                                {
                                    if((isset($outlet_name[$lang->id]) && $outlet_name[$lang->id]!="") )
                                    {
                                        $infomodel = new Outlet_infos;
                                        $infomodel->language_id = $lang->id;
                                        $infomodel->id          = $object->id; 
                                        $infomodel->outlet_name = $outlet_name[$lang->id];
                                        $infomodel->contact_address = $contact_address[$lang->id];
                                        $infomodel->save();
                                    }
                                }
                            }
                            catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                        }
                   }

                    /**
                     * Create the specified outlet in view.
                     * @param  int  $id
                     * @return Response
                     */
            public function branch_edit($id)
                    {
                        $vid    = Session::get('vendor_id');

                        if(!$vid)
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/edit_outlet'))
                        {
                        return view('errors.405');
                        }
                        else{

                           
                            //Get vendor details
                            $data = Outlets::find($id);
                            // echo"<pre>";
                            // print_r($data);exit;
                            if(!count($data))
                            {
                                Session::flash('message', 'Invalid Property Details'); 
                                return Redirect::to('vendor/outlets');    
                            }
                            //Get countries data
                            $countries = getCountryLists();
                            $info      = new Outlet_infos;
                            $price = getCategoryLists(6);
                            $place = getCategoryLists(7);
                            $accmodation_type = getAllAccomodationTypes();
                            $poi = getAllPlaceofInterest($id);
                            
                            return view('vendors.outlets.edit')->with('countries', $countries)->with('data', $data)->with('infomodel', $info)->with('price',$price)->with('place',$place)->with('accmodation_type',$accmodation_type)->with('poi',$poi);
                        }
                    }

                     /**
                     * Update the specified outlet in storage.
                     * @param  int  $id
                     * @return Response
                     */ 
            public function branch_update(Request $data, $id)
                    {
                        if (!Session::get('vendor_id')){
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/outlets'))
                        {
                        return view('errors.405');
                        }

                        //  dd($data->all());
                        // validate the post data
                        // read more on validation at http://laravel.com/docs/validation


                        $fields['contact_phone_number'] = Input::get('contact_phone_number');
                        //	$fields['country'] = Input::get('country');
                        $fields['city'] = Input::get('city');
                        $fields['location'] = Input::get('location');
                        $fields['property_image'] = Input::file('property_image');
                        $fields['contact_email'] = Input::get('contact_email');
                        // $fields['contact_address'] = Input::get('contact_address');
                        $fields['latitude'] = Input::get('latitude');
                        $fields['longitude'] = Input::get('longitude');
                        $fields['active_status'] = Input::get('active_status');
                        $fields['price_category'] = Input::get('price_category');
                        $fields['place_category'] = Input::get('place_category');
                        $fields['accomodation_type'] = Input::get('accomodation_type');
                        $fields['amenity_type'] = Input::get('amenity_type');
                        $fields['directions'] = Input::get('directions');
                        $fields['landmark'] = Input::get('landmark');
                        $fields['outlet_name'] = Input::get('outlet_name');

                        $rules = array(
                            
                            //  'country' => 'required',
                            'city' => 'required',
                            'location' => 'required',
                            'contact_phone_number' => 'required',
                            'contact_email' => 'required|email',
                            'property_image' => 'nullable|mimes:png,jpg,jpeg,bmp',
                            'price_category' => 'required',
                            'place_category' => 'required',                            
                            'accomodation_type' => 'required',                            
                            'amenity_type' => 'required',
                            'directions' => 'required',
                            'landmark' => 'required',
                            'outlet_name' => 'required',
                            //'contact_address' => 'required',
                            /*
                            'contact_phone_number' => 'required|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
                                'delivery_areas' => 'required',
                                'delivery_time' => 'required|numeric|min:0',
                                'pickup_time' => 'required|numeric|min:0',
                                'cancel_time' => 'required|numeric|min:0',
                                'return_time' => 'required|numeric|min:0',
                                'delivery_charges_fixed' => 'required|numeric|min:0',
                                'delivery_cost_variation' => 'required|numeric|min:0',
                                'service_tax' => 'required|numeric|min:0.1|max:99.9',
                                'minimum_order_amount' => 'required|numeric|min:0',
                            */
                            
                            
                        );
                        $outlet_name = Input::get('outlet_name');
                        foreach ($outlet_name  as $key => $value)
                        {
                            $fields['outlet_name'.$key] = $value;
                            $rules['outlet_name'.'1']   = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:outlet_infos,outlet_name,'.$id;
                        }
                        $contact_address = Input::get('contact_address');
                        foreach ($contact_address  as $key => $value)
                        {
                            $fields['contact_address'.$key] = $value;
                            $rules['contact_address'.'1']   = 'required';
                        }

                        $hotel_rules = Input::get('hotel_rules');
                        foreach ($hotel_rules  as $key => $value)
                        {
                            $fields['hotel_rules'.$key] = $value;
                            $rules['hotel_rules'.'1']   = 'required';
                        }

                        $cancellation_policy = Input::get('cancellation_policy');
                        foreach ($cancellation_policy  as $key => $value)
                        {
                            $fields['cancellation_policy'.$key] = $value;
                            $rules['cancellation_policy'.'1']   = 'required';
                        }

                        $highlights = Input::get('highlights');
                        foreach ($highlights  as $key => $value)
                        {
                            $fields['highlights'.$key] = $value;
                            $rules['highlights'.'1']   = 'required';
                        }

                        $near_by = Input::get('near_by');
                        foreach ($near_by  as $key => $value)
                        {
                            $fields['near_by'.$key] = $value;
                            $rules['near_by'.'1']   = 'required';
                        }

                        $facilities = Input::get('facilities');
                        foreach ($facilities  as $key => $value)
                        {
                            $fields['facilities'.$key] = $value;
                            $rules['facilities'.'1']   = 'required';
                        }                        
                        $validator = Validator::make($fields, $rules);    
                        // process the validation
                        if ($validator->fails())
                        { 
                            return Redirect::back()->withErrors($validator)->withInput();
                        } else {
                            try{
                                $Outlets = Outlets::find($id); 
                                $Outlets->contact_phone = $_POST['contact_phone_number'];
                                $Outlets->url_index     =  str_slug($_POST['outlet_name'][1]);
                                $Outlets->country_id = 1; //  1-> Malasyia  $_POST['country'];
                                $Outlets->city_id = $_POST['city'];
                                $Outlets->location_id = $_POST['location'];
                                $Outlets->vendor_id = Session::get('vendor_id');
                                $Outlets->modified_date = date("Y-m-d H:i:s");
                                $Outlets->created_by = Session::get('vendor_id');
                                $Outlets->active_status = isset($_POST['active_status'])?$_POST['active_status']:0;
                                $Outlets->online_payment = isset($_POST['online_payment'])?$_POST['online_payment']:0;
                                $Outlets->contact_email = $_POST['contact_email'];
                                $Outlets->latitude = $_POST['latitude'];
                                $Outlets->longitude = $_POST['longitude'];
                                $Outlets->amenity_type = isset($_POST['amenity_type']) ? implode(',',Input::get('amenity_type')):0;
                                $Outlets->accomodation_type = implode(',',Input::get('accomodation_type'));
                                $Outlets->price_category = implode(',',Input::get('price_category'));
                                $Outlets->place_category = implode(',',Input::get('place_category'));
                                $Outlets->hotel_rules = $_POST['hotel_rules'][1];
                                $Outlets->directions = $_POST['directions'];
                                $Outlets->landmark = $_POST['landmark'];
                                $Outlets->highlights = $_POST['highlights'][1];
                                $Outlets->cancellation_policy = $_POST['cancellation_policy'][1];
                                $Outlets->facilities = $_POST['facilities'][1];
                                $Outlets->near_by = $_POST['near_by'][1];   
                                /*
                                    $Outlets->delivery_time = $_POST['delivery_time'];
                                    $Outlets->pickup_time = $_POST['pickup_time'];
                                    $Outlets->delivery_areas = implode(',',$_POST['delivery_areas']);
                                    $Outlets->cancel_time = $_POST['cancel_time'];
                                    $Outlets->return_time = $_POST['return_time'];
                                    $Outlets->delivery_charges_fixed = $_POST['delivery_charges_fixed'];
                                    $Outlets->delivery_charges_variation = $_POST['delivery_cost_variation'];
                                    $Outlets->service_tax = $_POST['service_tax'];
                                    $Outlets->minimum_order_amount = $_POST['minimum_order_amount'];
                                */
                                $Outlets->save();

                               //   dd($Outlets->id);


                     if(isset($_FILES['property_image']['name']) && $_FILES['property_image']['name'] != '') {

                                $destinationPath = base_path().'/public/assets/admin/base/images/vendors/property_image/'; // upload path
                                $imageName = strtolower($Outlets->id.'.'.$data->file('property_image')->getClientOriginalExtension());
                                $data->file('property_image')->move($destinationPath, $imageName);
                                $destinationPath1 = url('/assets/admin/base/images/vendors/property_image/'.$imageName);
                                Image::make( $destinationPath1 )->fit(550, 350)->save(base_path().'/public/assets/admin/base/images/vendors/property_image/list/'.$imageName);
                                // print_r($destinationPath1);exit;
                                $Outlets->outlet_image = $imageName;

                                if (!file_exists(base_path().'/public/assets/admin/base/images/vendors/property/'.$Outlets->id.'/'))
                                {
                                    $result = File::makeDirectory(base_path().'/public/assets/admin/base/images/vendors/property/'.$Outlets->id.'/', 0777, true);
                                }
                                    Image::make( $destinationPath1 )->save(base_path().'/public/assets/admin/base/images/vendors/property/'.$Outlets->id.'/'.$imageName)->destroy();                                            

                                $Outlets->save();
                    }

                                $data = Place_of_interest::where('property_id',$id)->delete();
                                   

                                for($i=0; $i<count($_POST['place_name']); $i++)
                                {

                                    $poi = new Place_of_interest;

                                    $poi->place_name  = $_POST['place_name'][$i];
                                    $poi->place  = $_POST['places'][$i];
                                    $poi->distance  = $_POST['distance'][$i];
                                    $poi->url_index =  $_POST['place_name'][$i] ? str_slug($_POST['place_name'][$i]): str_slug($_POST['place_name'][$i]);
                                    $poi->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                                    $poi->property_id = $Outlets->id;
                                    $poi->vendor_id = Session::get('vendor_id');
                                    $poi->created_date = date("Y-m-d H:i:s");
                                    $poi->save();

                                } 
                                /*
                                    $last_insert_id = $id;
                                    //If posting new data means delete the old data in timings table and inserts new data here
                                    $del = DB::table('opening_timings')->where('vendor_id', $last_insert_id)->delete();
                                    //Store the timing schedules here
                                    $opening_time = $_POST['opening_timing'];
                                    $opening_time_array = getDaysWeekArray();
                                    foreach($opening_time as $key => $values) {
                                        $Timings = new Opening_timings;
                                        if(isset($values['istrue']) && $values['istrue']==1 && (array_key_exists($key, $opening_time_array))) {
                                            $day_week = $opening_time_array[$key];
                                            $Timings->vendor_id = $last_insert_id;
                                            $Timings->day_week = $day_week;
                                            $Timings->start_time = $values['from'];
                                            $Timings->end_time = $values['to'];
                                            $Timings->created_date = date('Y-m-d');
                                            $Timings->save();
                                        }
                                    }
                                    If posting new data means delete the old data in timings table and inserts new data here
                                    $del = DB::table('delivery_timings')->where('vendor_id', $last_insert_id)->delete();
                                    Store the timing schedules here
                                    $delivery_time = $_POST['delivery_timing'];
                                    $delivery_time_array = getDaysWeekArray();
                                    foreach($delivery_time as $key => $values) {
                                        $Timings = new Delivery_timings;
                                        if(isset($values['istrue']) && $values['istrue']==1 && (array_key_exists($key, $delivery_time_array))) {
                                            $day_week = $delivery_time_array[$key];
                                            $Timings->vendor_id = $last_insert_id;
                                            $Timings->day_week = $day_week;
                                            $Timings->start_time = $values['from'];
                                            $Timings->end_time = $values['to'];
                                            $Timings->created_date = date('Y-m-d');
                                            $Timings->save();
                                        }
                                    }
                                */
                                $this->branch_save_after($Outlets,$_POST);
                                Session::flash('message', trans('messages.Property has been updated successfully'));
                            }catch(Exception $e) {
                                    Log::Instance()->add(Log::ERROR, $e);
                            }
                            return Redirect::to('vendor/outlets');
                        }
                    }

            public function branch_show($id)
                    {
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/outlet_details'))
                        {
                        return view('errors.405');
                        }
                        else {
                             DB::enableQueryLog();

                            //Get vendor details
                            $query  = '"vendors_infos"."lang_id" = (case when (select count(lang_id) as totalcount from vendors_infos where vendors_infos.lang_id = '.getAdminCurrentLang().' and outlets.vendor_id = vendors_infos.vendors_view_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                            $query1 = '"outlet_infos"."language_id" = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.getAdminCurrentLang().' and outlets.id = outlet_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                            $data = DB::table('outlets')
                                    ->select('vendors_infos.vendor_name','outlets.*','outlet_infos.*')
                                    ->leftJoin('vendors_infos','vendors_infos.vendors_view_id','=','outlets.vendor_id')
                                    ->leftJoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                    ->whereRaw($query)
                                    ->whereRaw($query1)
                                    ->where('outlets.id',$id)
                                    ->get();
                                // $var =  DB::getQueryLog();

                                //   echo "<pre>";print_r($var);exit;
                            $data = iterator_to_array($data);


                            $all_places = DB::table('place_of_interest')->where('property_id','=',$id)
                                          ->select('place_name','place','distance')->get();  
                            $all_places = iterator_to_array($all_places);
                            if(count($all_places) > 0 && count($data) > 0){
                                $data[0]->places = $all_places;
                            }        

                              // echo "<pre>";print_r($all_places);die;
                            if(!count($data))
                            {
                                Session::flash('message', 'Invalid Property Details'); 
                                return Redirect::to('vendor/outlets');
                            }
                            //Get countries data
                            $countries = getCountryLists();
                            return view('vendors.outlets.show')->with('countries', $countries)->with('data', $data);
                        }
                    }

                public function branch_destroy($id)
                    {
                         // if (Auth::guest())
                         //            {
                         //                return redirect()->guest('admin/login');
                         //            }
                        if (!Session::get('vendor_id')){
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/delete_outlet'))
                        {
                        return view('errors.405');
                        }
                        $data = Outlets::find($id);
                        if(!count($data)){
                            Session::flash('message', 'Invalid Outlet Details'); 
                            return Redirect::to('vendor/outlets');    
                        }
                        //$data->delete();
                        //Update delete status while deleting
                        $data->active_status = 2;
                        $data->save();
                        Session::flash('message', trans('messages.Property has been deleted successfully!'));
                        return Redirect::to('vendor/outlets');
                    }

                    /**
                     * Show the application outlets.
                     * @return \Illuminate\Http\Response
                     */
                public function outlet_managers()
                    {
                        // if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }

                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/outletmanagers'))
                        {
                        return view('errors.405');
                        }
                        if (!Session::get('property_id'))
                            {
                                Session::flash('no-property','Add Property First');
                                return redirect::to('vendor/outlets');
                            }                          
                        else{
                            return view('vendors.outlets.managers.list');
                        }
                    }

                    /**
                     * Create the specified outlet manager in view.
                     * @param  int  $id
                     * @return Response
                     */
                    
            public function outlet_managers_create()
                    {
                        //  if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }
                        if (!Session::get('vendor_id')){
                            return redirect()->guest('vendor/login');
                        }
                        if(!has_staff_permission('vendor/create_outlet_managers'))
                        {
                        return view('errors.405');
                        }
                        else{
                            $id = 1;
                            $settings = Settings::find($id);
                            //Get countries data
                            $countries = getCountryLists();
                            return view('vendors.outlets.managers.create')->with('countries', $countries)->with('settings', $settings);
                        }
                    }

                    /**
                     * Process datatables ajax request.
                     *
                     * @return \Illuminate\Http\JsonResponse
                     */
        public function anyAjaxVendorBranchmanager()
                    {
                        //  if (Auth::guest())
                        // {
                        //     return 404;
                        // }
                        $vendor_id = Session::get('vendor_id');

                        $property_id = Session::get('property_id');
                        //  print_r($id); exit;

                        if(Session::get('vendor_type') == 2 )
                            {
                                $vendor_id = Session::get('created_vendor_id');
                                //  print_r($id); exit;
                            } 
                        $query = '"vendors_infos"."lang_id" = (case when (select count(id) as totalcount from vendors_infos where vendors_infos.lang_id = '.getAdminCurrentLang().' and outlet_managers.vendor_id = vendors_infos.vendors_view_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $query1 = '"outlet_infos"."language_id" = (case when (select count(id) as totalcount from outlet_infos where outlet_infos.language_id = '.getAdminCurrentLang().' and outlets.id = outlet_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $outlet_managers = DB::table('outlet_managers')
                                            ->select('outlet_managers.first_name','outlet_managers.email','outlet_managers.mobile_number','outlet_managers.email','outlet_managers.active_status','outlet_managers.created_date','outlet_managers.staff_type','outlet_managers.id as outletmanagerid','vendors_infos.vendor_name','outlet_infos.outlet_name')
                                            ->leftJoin('vendors_infos','vendors_infos.vendors_view_id','=','outlet_managers.vendor_id')
                                            ->leftJoin('outlets','outlets.id','=','outlet_managers.outlet_id')
                                            ->leftJoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                            ->whereRaw($query)
                                            ->whereRaw($query1)
                                            ->where('outlet_managers.outlet_id','=',$property_id)
                                            ->where('outlet_managers.vendor_id','=',$vendor_id)
                                            ->orderBy('outlet_managers.id', 'desc');
                        return Datatables::of($outlet_managers)->addColumn('action', function ($outlet_managers) {
                            if(has_staff_permission('vendor/edit_outlet_manager')){
                                $html='<div class="btn-group"><a href="'.URL::to("vendor/edit_outlet_manager/".$outlet_managers->outletmanagerid).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                            <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu xs pull-right" role="menu">
                                            <li><a href="'.URL::to("vendor/delete_outlet_managers/".$outlet_managers->outletmanagerid).'" class="delete-'.$outlet_managers->outletmanagerid.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                            </ul>
                                        </div><script type="text/javascript">
                                        $( document ).ready(function() {
                                        $(".delete-'.$outlet_managers->outletmanagerid.'").on("click", function(){
                                             return confirm("'.trans("messages.Are you sure want to delete?").'");
                                        });});</script>';
                            
                                return $html;
                                }
                            })
                            ->addColumn('activestatus', function ($outlet_managers) {
                                if($outlet_managers->active_status==0):
                                    $data = '<span class="label label-warning">'.trans("messages.Inactive").'</span>';
                                elseif($outlet_managers->active_status==1):
                                    $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                                elseif($outlet_managers->active_status==2):
                                    $data = '<span class="label label-danger">'.trans("messages.Delete").'</span>';
                                endif;
                                return $data;
                            })
                            ->addColumn('staff_type', function ($outlet_managers) {
                                //$data = '-';
                                if($outlet_managers->staff_type == 1):
                                    $data = trans("messages.Manager");
                                elseif($outlet_managers->staff_type == 2):
                                    $data = trans("messages.Front Desk");                                
                                endif;
                                return $data;
                                })                            
                            ->addColumn('mobile_number', function ($outlet_managers) {
                                $data = '-';
                                if($outlet_managers->mobile_number != ''):
                                $data = $outlet_managers->mobile_number;
                                endif;
                                return $data;
                                })
                            ->make(true);
                    }

                    /**
                     * Edit the specified driver in storage.
                     *
                     * @param  int  $id
                     * @return Response
                     */
            public function outlet_managers_edit($id)
                    {

                        //  if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/edit_outlet_manager'))
                        {
                        return view('errors.405');
                        } else {
                            
                            //Get driver details
                            $managers = Outlet_managers::find($id);
                            if(!count($managers))
                            {
                                Session::flash('message', 'Invalid staff Details'); 
                                return Redirect::to('vendor/outletmanagers');
                            }
                            $settings = Settings::find(1);
                            $countries = getCountryLists();
                            SEOMeta::setTitle('Edit Manager - '.$this->site_name);
                            SEOMeta::setDescription('Edit Manager - '.$this->site_name);
                            return view('vendors.outlets.managers.edit')->with('settings', $settings)->with('countries', $countries)->with('data', $managers);
                        }
                    }

                    /**
                     * Update the specified blog in storage.
                     *
                     * @param  int  $id
                     * @return Response
                     */
            public function outlet_managers_update(Request $data ,$id)
                    {
                        //  if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/outletmanagers'))
                        {
                        return view('errors.405');
                        }
                        $validation = Validator::make($data->all(), array(
                            //~ 'social_title'  => 'required',
                            'first_name'    => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                            'last_name'     => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                            'email'         => 'required|email|max:255|unique:outlet_managers,email,'.$id,
                            'mobile'        => 'regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
                        //'user_password'   => 'required|min:5|max:32',
                            'date_of_birth' => 'date',
                            'gender'        => 'required',
                            // 'gender'     => 'required',
                            'outlet_name'   => 'required',
                            'postal_code'   => 'required|numeric',
                            'address'       => 'required',
                            'staff_type'    => 'required',
                            'image'          => 'mimes:png,jpg,jpeg,bmp|max:2024',
                        ));
                        // process the validation
                        if ($validation->fails()) {
                            return Redirect::back()->withErrors($validation)->withInput();
                        } else {
                            // store datas in to database
                            $managers = Outlet_managers::find($id);
                            /** $manager_token = sha1(uniqid(Text::random('alnum', 32), TRUE));
                            if(!$managers->manager_token)
                            {
                                $managers->manager_token = $manager_token;
                            }**/
                            //~ $managers->social_title  = $_POST['social_title'];
                            $managers->first_name    = $_POST['first_name'];
                            $managers->last_name     = $_POST['last_name'];
                            $managers->email         = $_POST['email'];
                            //$managers->password = $_POST['user_password'];
                            $managers->mobile_number = $_POST['mobile'];
                             //$managers->date_of_birth = $_POST['date_of_birth'];
                            $managers->gender        = $_POST['gender'];
                             if($_POST['date_of_birth']!='')
                            {
                                $managers->date_of_birth = $_POST['date_of_birth'];
                            }
                            if(isset($_POST['country']) && $_POST['country']!='')
                            {
                                $managers->country_id = $_POST['country'];
                            }
                            if(isset($_POST['city']) && $_POST['city']!='')
                            {
                                $managers->city_id = $_POST['city'];
                            }
                            $managers->active_status     = isset($_POST['active_status'])?$_POST['active_status']:0;
                            $managers->is_verified       = isset($_POST['is_verified'])?$_POST['is_verified']:0;
                            //$drivers->ip_address      = Request::ip();
                            $managers->modified_date     = date("Y-m-d H:i:s");
                            //$managers->created_by = Auth::id();
                            $verification_key           = Text::random('alnum',12);
                            $managers->verification_key  = $verification_key;
                            $managers->postal_code        = $_POST['postal_code'];
                            $managers->address        = $_POST['address'];
                            $managers->vendor_id        = Session::get('vendor_id');
                            $managers->outlet_id        = $_POST['outlet_name'];
                            $managers->staff_type       = $_POST['staff_type'];
                            $managers->save();
                            if(isset($_FILES['image']['name']) && $_FILES['image']['name']!='')
                            {
                                $destinationPath = base_path().'/public/assets/admin/base/images/staff/'; // upload path
                                $imageName = strtolower($managers->id.'.'.$data->file('image')->getClientOriginalExtension());
                                $data->file('image')->move($destinationPath, $imageName);
                                $destinationPath1 = url('/assets/admin/base/images/staff/'.$imageName);
                                Image::make( $destinationPath1 )->fit(75, 75)->save(base_path().'/public/assets/admin/base/images/staff/thumb/'.$imageName)->destroy();
                                $managers->profile_image = $imageName;
                                $managers->save();
                            } 
                            // redirect
                            Session::flash('message', trans('messages.Outlet manager has been updated successfully'));
                            return Redirect::to('vendor/outletmanagers');
                        }
                    }

                    /**
                     * Update the specified blog in storage.
                     *
                     * @param  int  $id
                     * @return Response
                     */
            public function outlet_managers_store(Request $data)
                    {
                        //  if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/outletmanagers'))
                        {
	                        return view('errors.405');
                        }
                        $vendor_id = Session::get('vendor_id');
                        $outlet_id = Session::get('property_id');
                        $staff_type = Input::get('staff_type');

                        $validation = Validator::make($data->all(), array(
                            //~ 'social_title'  => 'required',
                            'first_name'    => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                            'last_name'     => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
                            'email'         => 'required|email|unique:outlet_managers,email',
                            'user_password' => 'required|min:5|max:32',
                            'gender'        => 'required',
                            'mobile'        => 'required|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
                            'outlet_name'   => 'required',
                            'postal_code'   => 'required|numeric',
                            'address'       => 'required',
                            'staff_type'    => 'required',
                            'image'          => 'mimes:png,jpg,jpeg,bmp|max:2024',
                        ));
                        // process the validation
                        if ($validation->fails())
                        {
                            return Redirect::back()->withErrors($validation)->withInput();
                        }
                        else {
                            // store datas in to database

                            //  dd($data->all());

                            $vendor_session_id = Session::get('vendor_id');
                            if($staff_type == 1)
                                $total_managers = getOutletUserLimit(1,$outlet_id);
                            else if($staff_type == 2)
                                $total_managers = getOutletUserLimit(2,$outlet_id);
                            $managers_limit = getOutletUserLimit(3,$vendor_session_id);

                            if($total_managers >= $managers_limit){
                                if($staff_type == 1)
                                    $errors = array('You have reached your maximum  managers limit.');
                                else if($staff_type == 2)
                                    $errors = array('You have reached your maximum front desk staff limit.');
                                return Redirect::back()->withErrors($errors)->withInput();
                            }
                            // print_r($vendor_session_id);

                            //exit;
                            $managers      = new Outlet_managers;
                            $manager_token = sha1(uniqid(Text::random('alnum', 32), TRUE));
                            if($manager_token)
                            {
                                $managers->manager_token = $manager_token;
                            } 
                            //~ $managers->social_title  = $_POST['social_title'];
                            $managers->first_name    = $_POST['first_name'];
                            $managers->last_name     = $_POST['last_name'];
                            $managers->email         = $_POST['email'];
                            $managers->hash_password = $_POST['user_password'];
                            $managers->mobile_number = $_POST['mobile'];
                            //$managers->date_of_birth = $_POST['date_of_birth'];
                            $managers->gender        = $_POST['gender'];
                            if($_POST['date_of_birth']!='')
                            {
                                $managers->date_of_birth = $_POST['date_of_birth'];
                            }
                            /*
                                if(isset($_POST['country']) && $_POST['country']!='')
                                {
                                    $managers->country_id = $_POST['country'];
                                }
                            */
                            if(isset($_POST['city']) && $_POST['city']!='')
                            {
                                $managers->city_id = $_POST['city'];
                            }

                            $managers->country_id = 1; /*  $_POST['country'];  */
                            $managers->active_status    = isset($_POST['active_status'])?$_POST['active_status']:0;
                            $managers->is_verified      = isset($_POST['is_verified'])?$_POST['is_verified']:0;
                            //$drivers->ip_address      = Request::ip();
                            $managers->created_date     = date("Y-m-d H:i:s");
                            $managers->modified_date    = date("Y-m-d H:i:s");
                            $managers->created_by       = Session::get('vendor_id');
                            $verification_key           = Text::random('alnum',12);
                            $managers->verification_key = $verification_key;
                            $managers->postal_code      = $_POST['postal_code'];
                            $managers->address          = $_POST['address'];
                            $managers->vendor_id        = Session::get('vendor_id');
                            $managers->outlet_id        = $_POST['outlet_name'];
                            $managers->staff_type       = $_POST['staff_type'];
                            //print_r($vendor_session_id);
                            //exit;                            
                            $managers->save();

                            $this->manager_save_after($managers,$_POST);
                            //  $managers->hash_password = hash::make($_POST['user_password']);

                            $managers->hash_password = md5($_POST['user_password']);
                            $managers->save();
                            if(isset($_FILES['image']['name']) && $_FILES['image']['name']!='')
                            {
                                $destinationPath = base_path().'/public/assets/admin/base/images/managers/'; // upload path
                                $imageName = strtolower($managers->id.'.'.$data->file('image')->getClientOriginalExtension());
                                $data->file('image')->move($destinationPath, $imageName);
                                $destinationPath1 = url('/assets/admin/base/images/managers/'.$imageName);
                                Image::make( $destinationPath1 )->fit(75, 75)->save(base_path().'/public/assets/admin/base/images/managers/thumb/'.$imageName)->destroy();
                                $managers->profile_image = $imageName;
                                $managers->save();
                            } 
                            // redirect
                            Session::flash('message', trans('messages.Outlet manager has been created successfully'));
                            return Redirect::to('vendor/outletmanagers');
                        }
                    }

            public function manager_save_after($object,$post)
                    { 
                        //  if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }
                        $manager = $object->getAttributes();
                        /*if($manager['is_verified'])
                        {*/
                            $template = DB::table('email_templates')
                                        ->select('from_email', 'from', 'subject', 'template_id','content')
                                        ->where('template_id','=',self::MANAGER_WELCOME_EMAIL_TEMPLATE)
                                        ->get();
                            if(count($template))
                            {
                                $from      = $template[0]->from_email;
                                $from_name = $template[0]->from;
                                $subject   = $template[0]->subject;
                                if(!$template[0]->template_id)
                                {
                                    $template  = 'mail_template';
                                    $from      = getAppConfigEmail()->contact_email;
                                    $subject   = "Welcome to ".getAppConfig()->site_name;
                                    $from_name = "";
                                }
                                $content = array("driver" => $manager,'u_password' => $manager['hash_password']);
                                $email   = smtp($from,$from_name,$manager['email'],$subject,$content,$template);
                            }
                        /*}
                        else {
                            $template = DB::table('email_templates')
                                        ->select('from_email', 'from', 'subject', 'template_id','content')
                                        ->where('template_id','=',self::MANAGER_SIGNUP_EMAIL_TEMPLATE)
                                        ->get();
                            if(count($template))
                            {
                                $from      = $template[0]->from_email;
                                $from_name = $template[0]->from;
                                $subject   = $template[0]->subject;
                                if(!$template[0]->template_id)
                                {
                                    $template  = 'mail_template';
                                    $from      = getAppConfigEmail()->contact_email;
                                    $subject   = "Welcome to ".getAppConfig()->site_name;
                                    $from_name = "";
                                }
                                $url1 ='<a href="'.url('/').'/managers/confirmation?key='.$manager['verification_key'].'&email='.$manager['email'].'&u_password='.$manager['password'].'"> This Confirmation Link </a>';
                                $content = array("driver" => $manager,"first_name" => $manager['first_name'], "confirmation_link" => $url1);
                                $email   = smtp($from,$from_name,$manager['email'],$subject,$content,$template);
                            }
                        }*/
                    }
                    /*
                     * vendor based outlet list
                     */
            public function getAllVendorOutletList(Request $request)
                    { 
                        //  if (Auth::guest())
                        // {
                        //     return 404;
                        // }
                        if($request->ajax())
                        {
                            $vendor_id   = $request->input('vendor_name');
                            $outlet_list = get_outlet_list($vendor_id);
                            return response()->json([
                                'data' => $outlet_list
                            ]);
                        }
                    }
                    /*
                     * outlets based product list
                     */
            public function getAllOutletProductList(Request $request)
                    { 
                        //  if (Auth::guest())
                        // {
                        //     return 404;
                        // }
                        if($request->ajax())
                        {
                            $outlet_id    = $request->input('outlet_name');
                            $product_list = get_product_list($outlet_id);
                            return response()->json([
                                'data' => $product_list
                            ]);
                        }
                    }

                        /**
                     * Delete the specified vendor in storage.
                     *
                     * @param  int  $id
                     * @return Response
                     */
            public function outlet_managers_destroy($id)
                    {
                        //  if (Auth::guest())
                        // {
                        //     return 404;
                        // }
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/delete_outlet_managers'))
                        {
                        return view('errors.405');
                        }
                        $data = Outlet_managers::find($id);
                        if(!count($data))
                        {
                            Session::flash('message', 'Invalid Outlet manager details'); 
                            return Redirect::to('vendor/outlet_managers');    
                        }
                        $data->delete();
                        Session::flash('message', trans('messages.Outlet manager has been deleted successfully!'));
                        return Redirect::to('vendor/outletmanagers');
                    }
                    
                    
            public function get_order_detail($order_id)
                    {
/*
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }
*/
                            $order_id = $order_id;


                            $booking_details = DB::table('booking_details')
                                                ->select('booking_details.created_date as bc_date','booking_details.charges as bc_charges','admin_customers.id as cust_id','booking_details.id as bid','booking_details.*','admin_customers.*','booking_status.*','booking_info.*')
                                                ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                                ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                                ->leftJoin('rooms','rooms.id','booking_info.room_id')
                                                ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                                                ->where('booking_details.id','=',$order_id)
                                                ->get();

                    //          return $booking_details;
/*
                            $room = booking_infos::where('booking_id' ,'=' ,$order_id)
                                                 ->pluck('room_id')->toArray();

                            $rooms = DB::table('rooms')
                                                ->select('rooms.id','rooms_infos.room_name','room_type.discount_price','room_type.normal_price','room_type.id as room_type_id','room_type_infos.room_type')
                                                ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                                ->leftJoin('room_type_infos','room_type_infos.id','room_type.id')
                                                ->whereIn('rooms.id', $room)
                                                ->get()
                                                ->toArray();      

         
                             return $rooms;
*/
                            $outlet_id = (count($booking_details) > 0)? $booking_details[0]->outlet_id:'';

                            //  return $outlet_id;

                            $outlets = DB::table('outlets')
                                        ->select('outlets.*','outlet_infos.*')
                                        ->leftJoin('outlet_infos','outlet_infos.id','outlets.id')
                                        ->where('outlets.id','=',$outlet_id)
                                        ->get();

                            $vendor_id = (count($outlets) > 0)? $outlets[0]->vendor_id:'';



                            //  return $vendor_id;

                            $vendors = vendors_view::where('id','=',$vendor_id)->get();

                            //  return $vendors;

                            $rooms_count = DB::table('booking_details')
                                        ->select(DB::RAW('count(booking_info.id) as rooms_count'))
                                        ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                        ->where('booking_details.id','=',$order_id)
                                        ->get();                  

                              //    return $rooms_count;


                            if(count($outlets) > 0)  {
                                $result = array("properties" => $outlets, "booking_details" => $booking_details,
                                                "rooms_count" => $rooms_count, "outlet_id" => $outlet_id,
                                                "vendors" => $vendors, "vendor_id" => $vendor_id,
                                                //  "rooms" => $rooms,
                                            );
                            }          

                            return $result;
                    }

            public function get_booking_detail($order_id)
                    {

                        if(Auth::guest())
                        {
                            return redirect()->guest('admin/login');
                        } 
 
                            $order_id = $order_id;


                            $booking_details = DB::table('booking_details')
                                                ->select('booking_details.created_date as bc_date','booking_details.charges as bc_charges','admin_customers.id as cust_id','booking_details.id as bid','booking_details.*','admin_customers.*','booking_status.*','booking_info.*')
                                                ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                                ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                                ->leftJoin('rooms','rooms.id','booking_info.room_id')
                                                ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                                                ->where('booking_details.id','=',$order_id)
                                                ->get();

                    //          return $booking_details;
/*
                            $room = booking_infos::where('booking_id' ,'=' ,$order_id)
                                                 ->pluck('room_id')->toArray();

                            $rooms = DB::table('rooms')
                                                ->select('rooms.id','rooms_infos.room_name','room_type.discount_price','room_type.normal_price','room_type.id as room_type_id','room_type_infos.room_type')
                                                ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                                ->leftJoin('room_type_infos','room_type_infos.id','room_type.id')
                                                ->whereIn('rooms.id', $room)
                                                ->get()
                                                ->toArray();      

         
                             return $rooms;
*/
                            $outlet_id = (count($booking_details) > 0)? $booking_details[0]->outlet_id:'';

                            //  return $outlet_id;

                            $outlets = DB::table('outlets')
                                        ->select('outlets.*','outlet_infos.*')
                                        ->leftJoin('outlet_infos','outlet_infos.id','outlets.id')
                                        ->where('outlets.id','=',$outlet_id)
                                        ->get();

                            $vendor_id = (count($outlets) > 0)? $outlets[0]->vendor_id:'';



                            //  return $vendor_id;

                            $vendors = vendors_view::where('id','=',$vendor_id)->get();

                            //  return $vendors;

                            $rooms_count = DB::table('booking_details')
                                        ->select(DB::RAW('count(booking_info.id) as rooms_count'))
                                        ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                        ->where('booking_details.id','=',$order_id)
                                        ->get();                  

                              //    return $rooms_count;


                            if(count($outlets) > 0)  {
                                $result = array("properties" => $outlets, "booking_details" => $booking_details,
                                                "rooms_count" => $rooms_count, "outlet_id" => $outlet_id,
                                                "vendors" => $vendors, "vendor_id" => $vendor_id,
                                                //  "rooms" => $rooms,
                                            );
                            }          

                            return $result;
                    }

                    
                    /* Get outlets list based on the vendor */
            public function Maincategorylist(Request $request)
                    {
                        //  if (Auth::guest())
                        // {
                        //     return 404;
                        // }
                        if($request->ajax()){
                            $c_id = $request->input('cid');
                            $data = getMainCategoryLists($c_id); // get product sub category data here
                            return response()->json([
                                'data' => $data
                            ]);
                        }
                    }


                        /* Get outlets list based on the vendor */
            public function ProductMaincategorylist(Request $request)
                    {
                        //  if (Auth::guest())
                        // {
                        //     return 404;
                        // }
                        if($request->ajax()){
                            $c_id = $request->input('cid');
                            $data = getProductMainCategoryLists($c_id); // get product sub category data here
                            return response()->json([
                                'data' => $data
                            ]);
                        }
                    }

                    
             public function getVendorcategorylist(Request $request)
                    {
                        //  if (Auth::guest())
                        // {
                        //     return 404;
                        // }
                        if($request->ajax()){
                            $c_id = $request->input('cid');
                            $language = $request->input('language');
                            $data = getCategoryVendorLists($c_id,$language);
                             $cdata = getMainCategoryLists($c_id,$language); 
                            return response()->json([
                                'data' => $data,'cdata' => $cdata
                            ]);
                        }
                    }

                         /* Billing page  */
            public function billing()
                    {
                        //  if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }

                        if(Session::get('vendor_type') == 2)
                        {
                            $vendor_id = Session::get('staffs_under_by');
                        }
                        else
                        {
                            $vendor_id = Session::get('vendor_id');
                        }
                        $user = vendors::find($vendor_id);

                        $language = getAdminCurrentLang();
                        $subscription_plans = DB::select('SELECT
                        "subscription_plan"."id",
                        "subscription_plan_infos"."subscription_plan_name",
                        "subscription_plan_infos"."description",
                        "subscription_plan"."subscription_plan_price",
                        "subscription_plan"."subscription_plan_duration",
                        "subscription_plan_infos"."price_text",
                        "subscription_plan"."url_key",
                        "subscription_plan"."sort_order",
                        "subscription_plan"."subscription_plan_status"
                        FROM subscription_plan subscription_plan
                        LEFT JOIN "subscription_plan_infos" ON "subscription_plan_infos"."subscription_plan_id" = "subscription_plan"."id"
                        where subscription_plan.subscription_plan_status = ? ORDER BY subscription_plan.id DESC LIMIT 3 ',array(1));

                        if(Session::get('vendor_type') == 2)
                        {
                            $vendor_id = Session::get('staffs_under_by');
                        }
                        else
                        {
                            $vendor_id = Session::get('vendor_id');
                        }
                        //echo $vendor_id;exit;
                        // $invoices = $user->invoices();

                        $query = 'subscription_plan_infos.language_id = (case when (select count(language_id) as totalcount from subscription_plan_infos where subscription_plan_infos.language_id = '.getAdminCurrentLang().' and subscription_plan.id = subscription_plan_infos.subscription_plan_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';

                        $current_paln = DB::table('subscriptions')
                        ->select('subscription_plan.*','subscription_plan_infos.*','subscriptions.created_at as plan_date')
                        ->Leftjoin('subscription_plan','subscription_plan.id','=','subscriptions.plan_id')
                        ->Leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
                        ->where('subscriptions.user_id',$vendor_id)
                        ->whereRaw($query)
                        ->orderBy('subscriptions.id', 'desc')
                        ->limit(1)
                        ->get();
                        $plan_endate = "";
                        if(count($current_paln)>0)
                        {
                            $plan_endate =  date('Y-m-d H:i:s',strtotime('+'.$current_paln[0]->subscription_plan_duration.' days',strtotime($current_paln[0]->plan_date)));
                        }
                        return view('vendors.billing-info')->with('subscription_plans', $subscription_plans)->with('current_paln', $current_paln)->with('user', $user)->with('plan_endate', $plan_endate);
                    }



                    /**
                     * Process datatables ajax request.
                     *
                     * @return \Illuminate\Http\JsonResponse
                     */
            public function anyAjaxbilling()
                    {
                        //  if (Auth::guest())
                        // {
                        //     return 404;
                        // }
                        if(Session::get('vendor_type') == 2)
                        {
                            $vendor_id = Session::get('staffs_under_by');
                        }
                        else
                        {
                            $vendor_id = Session::get('vendor_id');
                        }
                        $user = vendors::find($vendor_id);
                        //$invoices = $user->invoices();
                         $query = 'subscription_plan_infos.language_id = (case when (select count(language_id) as totalcount from subscription_plan_infos where subscription_plan_infos.language_id = '.getAdminCurrentLang().' and vendors.id = subscription_plan_infos.subscription_plan_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $subscriptions = DB::table('subscription_transaction')
                        ->select('subscription_transaction.*','subscription_plan.*','vendors.*','subscription_plan_infos.*','subscription_transaction.created_date','subscription_transaction.id as transaction_id')
                        ->leftJoin('subscription_plan','subscription_plan.id','=','subscription_transaction.plan_id')
                        ->leftJoin('vendors','vendors.id','=','subscription_transaction.vendor_id')
                        ->Leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
                        ->where('subscription_transaction.vendor_id',$vendor_id)
                        ->whereRaw($query)
                        ->orderBy('subscription_transaction.id', 'desc');

                        return Datatables::of($subscriptions)
                            ->addColumn('created_at', function ($subscriptions) {
                            $data = '-';
                            if($subscriptions->created_at != ''):
                            $data = $subscriptions->created_at;
                            endif;
                            return $data;
                            })
                            ->addColumn('updated_at', function ($subscriptions) {
                            $data = '-';
                            if($subscriptions->updated_at != ''):
                            $data = $subscriptions->updated_at;
                            endif;
                            return $data;
                            })
                            ->addColumn('invoice', function ($subscriptions) {
                                $data = '<a target="_blank" href="'.URL::to("user/invoice/".$subscriptions->transaction_id."/".$subscriptions->vendor_id).'" class="invoice-'.$subscriptions->transaction_id.'" title="'.trans("messages.Invoice").'">&nbsp;&nbsp;'.@trans("messages.Invoice").'</a>';;
                                return $data;
                            })
                            ->make(true);
                    }

                public function downloadinvoice($invoiceId,$vendor_id="")
                    {
                        $vendor_id="";
                         if(Session::get('vendor_type') == 2)
                        {
                            $vendor_id = Session::get('staffs_under_by');
                        }
                        else
                        {
                            
                            $vendor_id = Session::get('vendor_id');

                        } 
                        $query = 'subscription_plan_infos.language_id = (case when (select count(language_id) as totalcount from subscription_plan_infos where subscription_plan_infos.language_id = '.getAdminCurrentLang().' and vendors.id = subscription_plan_infos.subscription_plan_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';

                        $subscriptions = DB::table('subscription_transaction')
                        ->select('subscription_transaction.*','subscription_plan.*','vendors.*','subscription_plan_infos.*','subscription_transaction.created_date','subscription_transaction.created_date','subscription_transaction.id as transaction_id')
                        ->leftJoin('subscription_plan','subscription_plan.id','=','subscription_transaction.plan_id')
                        ->leftJoin('vendors','vendors.id','=','subscription_transaction.vendor_id')
                        ->Leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
                        ->where('subscription_transaction.id',$invoiceId)
                        ->whereRaw($query)->get();

                        $plan_endate =  date('Y-m-d H:i:s',strtotime('+'.$subscriptions[0]->subscription_plan_duration.' days',strtotime($subscriptions[0]->created_date)));
                      
                        if(($vendor_id==null) || ($vendor_id=="")){
                            $vendor_id=$subscriptions[0]->vendor_id;
                        }
                        $user = vendors::find($vendor_id);
                        $logo = url('assets/front/'.Session::get("general")->theme.'/images/'.Session::get("general")->theme.'.png');
                        $site_name = ucfirst(Session::get("general")->site_name);
                        $item = '<tr><td align="center" style="font-size:15px;padding:10px 0; font-family:arial; font-weight:normal; border-bottom:1px solid #ccc;">'.wordwrap(ucfirst(strtolower($subscriptions[0]->subscription_plan_name)),40,"<br>\n").'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:arial; font-weight:normal;">'.wordwrap(ucfirst(strtolower($subscriptions[0]->description)),40,"<br>\n").'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:arial; font-weight:normal;">1</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:DejaVu Sans,arial; font-weight:normal;">'.$subscriptions[0]->subscription_plan_price.'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:DejaVu Sans,arial; font-weight:normal;">'.$subscriptions[0]->subscription_plan_price.'</td></tr>';

                        //$sub_total += ($items->item_cost+$ingredient_price)*$items->item_unit;

                        $bill_address="";
                        $addArray=explode(",",$user->contact_address);
                                if(sizeof($addArray)>2){
                                    $brplace=1;

                                }else{
                                    $brplace=0;

                                }


                                  foreach ($addArray as $key => $value) {

                                    $bill_address=$bill_address.ucfirst($value);
                                    if($key==sizeof($addArray)-1){

                                    $bill_address= $bill_address.".";

                                    }else{

                                    $bill_address= $bill_address.",";

                                    }
                                    if($key==$brplace){
                                        $bill_address=$bill_address." <br> ";
                                    }
                                    
                                }


                        $html = '<table width="700px" cellspacing="0" cellpadding="0" bgcolor="#fff" style="border:1px solid #ccc;">
                        <tbody>
                        <tr>
                        <td style="border-bottom:1px solid #ccc;">
                        <table style="padding-top: 25px; padding-bottom: 25px;" width="700px" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td width="20">&nbsp;</td>
                        <td>
                        <table>
                        <tr>
                        <td style="font-size:16px; font-weight:bold; font-family:Verdana; color:#000; padding-bottom:10px;">BILL From :</td>
                        </tr>
                        <tr>
                        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.$site_name.'</td>
                        </tr>
                        </table>
                        </td>
                        <td align="right"><a title="'.$site_name.'" href="'.url('/').'"><img src="'.$logo.'" alt="'.$site_name.'" /></a></td>
                        <td width="20">&nbsp;</td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        <!-- end 1 tr -->
                        <tr>
                        <td>
                        <table style="padding-top: 25px; padding-bottom: 25px;" width="700px" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td width="20">&nbsp;</td>
                        <td colspan="4">
                        <table>
                        <tr>
                        <td style="font-size:16px; font-weight:bold; font-family:Verdana; color:#000; padding-bottom:10px;">Bill to :</td>
                        </tr>

                        <tr>

                        <td style="font-size:16px; font-weight:500; font-family:dejavu sans,arial; color:#666; line-height:28px;">'.$user->first_name.' '.$user->last_name.', <br>'.$bill_address.'</td>


                        </tr>
                        </table>
                        </td>
                        <td align="right">
                        <table cellpadding="0" cellspacing="0">
                        <tr>
                        <td style="font-size:15px; font-weight:bold; font-family:Verdana; color:#000; line-height:28px;">Invoice </td>
                        <td></td>
                        <td align="left" style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.$subscriptions[0]->invoice_id.'</td>
                        </tr>
                        <tr>
                        <td style="font-size:15px; font-weight:bold; font-family:Verdana; color:#000; line-height:28px;">Date</td>
                        <td></td>
                        <td align="left" style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.date('F d, Y', strtotime($subscriptions[0]->created_date)).'</td>
                        </tr>

                        </table>
                        </td>
                        <td width="20">&nbsp;</td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        <!-- end 2 tr -->
                        <tr>
                        <td>
                        <table cellpadding="0" cellspacing="0" width="100%">
                        <tr style="background:#d1d5d4;padding:0 9px;">
                        <td align="center" style=" padding:7px 0; font-size:17px; font-family:Verdana; font-weight:bold;">Plan</th>
                        <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Description</th>
                        <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Quantity</th>
                        <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Unit cost</th>
                        <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Unit cost total</th>
                        </tr>'.$item.'
                        </table>
                        </td>
                        </tr>
                        <!-- end 3 tr -->
                        <tr>
                        <td>
                        <table style="padding-top: 25px; padding-bottom: 25px;" width="787" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td width="20">&nbsp;</td>
                        <td>
                        <table>
                        <tbody><tr>
                        <td style="font-size:16px; font-weight:bold; font-family:Verdana; color:#000; padding-bottom:10px;">NOTES / MEMO :</td>
                        </tr>
                        <tr>
                        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">If any clarification contact site owner </td>
                        </tr>
                        </tbody></table>
                        </td>
                        <td align="right">
                        <table cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td style="font-size:15px; font-weight:bold; font-family:arial; color:#000; line-height:28px;">SUBTOTAL</td>
                        <td width="10"></td>
                        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;" align="right">'.$subscriptions[0]->total_amount.'</td>
                        </tr>
                        <tr>
                        <td style="font-size:15px; font-weight:bold; font-family:arial; color:#000; line-height:28px; background:#d1d5d4; padding:0 9px;">TOTAL</td>
                        <td style="background:#d1d5d4;padding:0 9px;" width="10"></td>
                        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;background:#d1d5d4;padding:0 9px;" align="right">'.$subscriptions[0]->total_amount.'</td>
                        </tr>
                        </tbody></table>
                        </td>
                        <td width="20">&nbsp;</td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <table>
                        <tr>
                        <td width="20">&nbsp;</td>
                        <td width="20">&nbsp;</td>
                        </tr>
                        </tbody>
                        </table>';


                        //echo $html;exit;
                        $pdf = App::make('dompdf.wrapper');
                        $pdf->loadHTML($html);
                        return $pdf->stream('invoice.pdf',array('Attachment'=>0));
                    }

                    //Booking Details Listing in Vendors
                    public function orders()
                    {
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        else {

                            if(!has_staff_permission('vendors/orders/index'))
                            {
                                return view('errors.404');
                            }

                            //$vendor_id = Session::get('vendor_id');
                            if(Session::get('vendor_type') == 2)
                            {
                                $vendor_id = Session::get('created_vendor_id');
                            }
                            else
                            {
                                $vendor_id = Session::get('vendor_id');
                            }
                            $condition ="booking_details.room_type!=0";
                            $from = "";
                            $to = "";
                            if(Input::get('from') && Input::get('to'))
                            {
                                $from = date('Y-m-d H:i:s', strtotime(Input::get('from')));
                                $to = date('Y-m-d H:i:s', strtotime(Input::get('to')));
                                $condition .=" and booking_details.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            }
                            $from_amount = "";
                            $to_amount = "";
                            if(Input::get('from_amount') && Input::get('to_amount'))
                            {
                                $from_amount = Input::get('from_amount');
                                $to_amount = Input::get('to_amount');
                                $condition .=" and booking_details.total_amount BETWEEN '".$from_amount."' and '".$to_amount."'";
                            }
                            $booking_status = "";
                            if(Input::get('booking_status'))
                            {
                                $booking_status = Input::get('booking_status');
                                $condition .=" and booking_details.booking_status = ".$booking_status."";
                            }
                            /*$payment_type = "";
                            if(Input::get('payment_type'))
                            {
                                $payment_type = Input::get('payment_type');
                                $condition .=" and orders.payment_gateway_id = ".$payment_type."";
                            }*/
                            $outlet = "";
                            if(Input::get('outlet'))
                            {
                                $outlet = Input::get('outlet');
                                $condition .=" and booking_details.outlet_id = ".$outlet."";
                            }
                           /* $query  = 'payment_gateways_info.language_id = (case when (select count(payment_gateways_info.payment_id) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.getAdminCurrentLang().' and orders.payment_gateway_id = payment_gateways_info.payment_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)'; */
                            $query1 = 'outlet_infos.language_id = (case when (select count(outlet_infos.id) as totalcount from outlet_infos where outlet_infos.language_id = '.getAdminCurrentLang().' and booking_details.outlet_id = outlet_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                            $query2 = 'vendors_infos.lang_id = (case when (select count(vendors_infos.id) as totalcount from vendors_infos where vendors_infos.lang_id = '.getAdminCurrentLang().' and booking_details.vendor_id = vendors_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                            $booking_details = DB::table('booking_details')
                                        ->select('booking_details.booking_random_id',
                                            'booking_details.customer_id','booking_details.outlet_id',
                                            'booking_details.booking_status','booking_details.check_in_date',
                                            'booking_details.check_out_date','booking_details.created_date',
                                            'booking_details.modified_date','booking_details.id as bid',
                                            'vendors_infos.*','outlet_infos.*',
                                            'admin_customers.*','booking_status.*')
                                        ->leftJoin('admin_customers','admin_customers.id','=','booking_details.customer_id')
                                        ->leftJoin('booking_status','booking_status.id','=','booking_details.booking_status')
                                        /*->leftjoin('transaction','transaction.order_id','=','orders.id')
                                        ->Join('payment_gateways_info','payment_gateways_info.payment_id','=','orders.payment_gateway_id')*/
                                        ->Join('vendors_infos','vendors_infos.id','=','booking_details.vendor_id')
                                        ->Join('outlet_infos','outlet_infos.id','=','booking_details.outlet_id')
                                        ->where('booking_details.vendor_id',$vendor_id)
                                        /* ->whereRaw($query) */
                                        ->whereRaw($query1)
                                        ->whereRaw($query2)
                                        ->whereRaw($condition)
                                        ->orderBy('booking_details.id', 'desc')
                                        ->paginate(10);
                                        // echo "<pre>";
                                        // print_r($booking_details);exit;
                            $booking_details->appends(['outlet' => $outlet,/*'payment_type'=>$payment_type,*/'booking_status'=>$booking_status,'from_amount'=>$from_amount,'to_amount'=>$to_amount,'from'=>$from,'to'=>$to])->links();
                            $booking_status = DB::table('booking_status')->select('id','name')
                                            ->orderBy('id', 'asc')->get();
                                            
                            // echo "<pre>";
                            // print_r($booking_status);exit;
                            /* $query3  = 'payment_gateways_info.language_id = (case when (select count(payment_gateways_info.language_id) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.getAdminCurrentLang().' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                            $payment_seetings = DB::table('payment_gateways')
                                                    ->select('payment_gateways.id','payment_gateways_info.name')
                                                    ->leftJoin('payment_gateways_info','payment_gateways_info.payment_id','=','payment_gateways.id')
                                                    ->whereRaw($query3)
                                                    ->orderBy('id', 'asc')
                                                    ->get(); */
                            if(Input::get('export'))
                            {
                                $out = '"Booking Id","Name","Merchant Name","Restaurant Name","Status","Total Amount","Payment Mode","Order Date"'."\r\n";
                                foreach($booking_details as $d)
                                {
                                    $out .= $d->id.',"'.$d->user_name.'","'.$d->vendor_name.'","'.$d->outlet_name.'","'.$d->status_name.'","'.$d->total_amount.$d->currency_code.'","'.$d->payment_type.'","'.date("d F, Y", strtotime($d->created_date)).'"'."\r\n";
                                }
                                header('Content-Description: File Transfer');
                                header('Content-Type: application/octet-stream');
                                header('Content-Disposition: attachment; filename=Orders.csv');
                                header('Content-Transfer-Encoding: binary');
                                header('Expires: 0');
                                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                                header('Pragma: public');
                                echo "\xEF\xBB\xBF"; // UTF-8 BOM
                                echo $out;
                                exit;
                            }
                            return view('vendors.bookings.list')->with('booking_details', $booking_details)->with('booking_status', $booking_status)/*->with('payment_seetings', $payment_seetings)*/;
                        }
                    }

                    //Booking Details Listing in Vendors
                    public function Manager_orders()
                    {
                    if (!Session::get('manager_id')) {

                        return redirect()->guest('managers/login');

                    }
                        else {
                            $vendor_id = Session::get('manager_vendor');

                            $outlet_id = Session::get('manager_outlet');

                            $condition ="booking_details.room_type!=0";
                            $from = "";
                            $to = "";
                            if(Input::get('from') && Input::get('to'))
                            {
                                $from = date('Y-m-d H:i:s', strtotime(Input::get('from')));
                                $to = date('Y-m-d H:i:s', strtotime(Input::get('to')));
                                $condition .=" and booking_details.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            }
                            $from_amount = "";
                            $to_amount = "";
                            if(Input::get('from_amount') && Input::get('to_amount'))
                            {
                                $from_amount = Input::get('from_amount');
                                $to_amount = Input::get('to_amount');
                                $condition .=" and booking_details.total_amount BETWEEN '".$from_amount."' and '".$to_amount."'";
                            }
                            $booking_status = "";
                            if(Input::get('booking_status'))
                            {
                                $booking_status = Input::get('booking_status');
                                $condition .=" and booking_details.booking_status = ".$booking_status."";
                            }
                            /*$payment_type = "";
                            if(Input::get('payment_type'))
                            {
                                $payment_type = Input::get('payment_type');
                                $condition .=" and orders.payment_gateway_id = ".$payment_type."";
                            }*/
                            $outlet = "";
                            if(Input::get('outlet'))
                            {
                                $outlet = Input::get('outlet');
                                $condition .=" and booking_details.outlet_id = ".$outlet."";
                            }
                           /* $query  = 'payment_gateways_info.language_id = (case when (select count(payment_gateways_info.payment_id) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.getAdminCurrentLang().' and orders.payment_gateway_id = payment_gateways_info.payment_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)'; */
                            $query1 = 'outlet_infos.language_id = (case when (select count(outlet_infos.id) as totalcount from outlet_infos where outlet_infos.language_id = '.getAdminCurrentLang().' and booking_details.outlet_id = outlet_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                            $query2 = 'vendors_infos.lang_id = (case when (select count(vendors_infos.id) as totalcount from vendors_infos where vendors_infos.lang_id = '.getAdminCurrentLang().' and booking_details.vendor_id = vendors_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                            $booking_details = DB::table('booking_details')
                                        ->select('booking_details.booking_random_id',
                                            'booking_details.customer_id','booking_details.outlet_id',
                                            'booking_details.booking_status','booking_details.check_in_date',
                                            'booking_details.check_out_date','booking_details.created_date',
                                            'booking_details.modified_date','booking_details.id as bid',
                                            'vendors_infos.*','outlet_infos.*',
                                            'admin_customers.*','booking_status.*')
                                        ->leftJoin('admin_customers','admin_customers.id','=','booking_details.customer_id')
                                        ->leftJoin('booking_status','booking_status.id','=','booking_details.booking_status')
                                        /*->leftjoin('transaction','transaction.order_id','=','orders.id')
                                        ->Join('payment_gateways_info','payment_gateways_info.payment_id','=','orders.payment_gateway_id')*/
                                        ->Join('vendors_infos','vendors_infos.id','=','booking_details.vendor_id')
                                        ->Join('outlet_infos','outlet_infos.id','=','booking_details.outlet_id')
                                        ->where('booking_details.vendor_id',$vendor_id)
                                        ->where('booking_details.outlet_id',$outlet_id)
                                        /* ->whereRaw($query) */
                                        ->whereRaw($query1)
                                        ->whereRaw($query2)
                                        ->whereRaw($condition)
                                        ->orderBy('booking_details.id', 'desc')
                                        ->paginate(10);
                                        // echo "<pre>";
                                        // print_r($booking_details);exit;
                            $booking_details->appends(['outlet' => $outlet,/*'payment_type'=>$payment_type,*/'booking_status'=>$booking_status,'from_amount'=>$from_amount,'to_amount'=>$to_amount,'from'=>$from,'to'=>$to])->links();
                            $booking_status = DB::table('booking_status')->select('id','name')
                                            ->orderBy('id', 'asc')->get();
                                            
                            // echo "<pre>";
                            // print_r($booking_status);exit;
                            /* $query3  = 'payment_gateways_info.language_id = (case when (select count(payment_gateways_info.language_id) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.getAdminCurrentLang().' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                            $payment_seetings = DB::table('payment_gateways')
                                                    ->select('payment_gateways.id','payment_gateways_info.name')
                                                    ->leftJoin('payment_gateways_info','payment_gateways_info.payment_id','=','payment_gateways.id')
                                                    ->whereRaw($query3)
                                                    ->orderBy('id', 'asc')
                                                    ->get(); */
                            if(Input::get('export'))
                            {
                                $out = '"Booking Id","Name","Merchant Name","Restaurant Name","Status","Total Amount","Payment Mode","Order Date"'."\r\n";
                                foreach($booking_details as $d)
                                {
                                    $out .= $d->id.',"'.$d->user_name.'","'.$d->vendor_name.'","'.$d->outlet_name.'","'.$d->status_name.'","'.$d->total_amount.$d->currency_code.'","'.$d->payment_type.'","'.date("d F, Y", strtotime($d->created_date)).'"'."\r\n";
                                }
                                header('Content-Description: File Transfer');
                                header('Content-Type: application/octet-stream');
                                header('Content-Disposition: attachment; filename=Orders.csv');
                                header('Content-Transfer-Encoding: binary');
                                header('Expires: 0');
                                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                                header('Pragma: public');
                                echo "\xEF\xBB\xBF"; // UTF-8 BOM
                                echo $out;
                                exit;
                            }
                            return view('managers.booking.list')->with('booking_details', $booking_details)->with('booking_status', $booking_status)/*->with('payment_seetings', $payment_seetings)*/;
                        }
                    }

/*
                //Booking Details Create in Vendors.
                    public function create_order()
                    {
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        else
                        {
                            if(!has_staff_permission('vendors/create_order'))
                            {
                                return view('errors.404');
                            }

                             $address_type=$this->get_address_type();
                            // print_r($address_type);
                             $address_types[""]=trans("messages.Select address type");
                              foreach ($address_type as $row)
                                {
                                    $address_types[$row->id] = ucfirst($row->name);
                                }

                            $vendor_details = vendors::find(Session::get('vendor_id'));
                            SEOMeta::setTitle('Product Reports - '.$this->site_name);
                            SEOMeta::setDescription('Product Reports - '.$this->site_name);
                           // $booking_status = DB::table('booking_status')->select('id','name')->orderBy('name', 'asc')->get();
                            return view('vendors.orders.create')->with('vendor_details', $vendor_details)->with('address_types',$address_types);
                        }
                    } */
               
                    /**
                     * Subscription Payments  */
                public function subscription_payments()
                    {
                        // if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }
                        SEOMeta::setTitle('Subscription Payments - '.$this->site_name);
                        SEOMeta::setDescription('Subscription Payments - '.$this->site_name);
                        $transactions = DB::table('subscription_transaction')->orderBy('id', 'asc')->get();
                        return view('admin.subscription_transactions.list');
                    }

                    /**
                     * Process datatables ajax request.
                     *
                     * @return \Illuminate\Http\JsonResponse
                     */
                public function anyAjaxsubscriptionpayment()
                    {

                        // if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }
                        //$invoices = $user->invoices();
                         $query = 'subscription_plan_infos.language_id = (case when (select count(language_id) as totalcount from subscription_plan_infos where subscription_plan_infos.language_id = '.getAdminCurrentLang().' and vendors.id = subscription_plan_infos.subscription_plan_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $subscriptions = DB::table('subscription_transaction')
                        ->select('subscription_transaction.*','subscription_plan.*','vendors.*','subscription_plan_infos.*','subscription_transaction.created_date','subscription_transaction.id as transaction_id')
                        ->leftJoin('subscription_plan','subscription_plan.id','=','subscription_transaction.plan_id')
                        ->leftJoin('vendors','vendors.id','=','subscription_transaction.vendor_id')
                        ->Leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
                        ->whereRaw($query)
                        ->orderBy('subscription_transaction.id', 'desc');
                        //print_r($subscriptions);exit;
                        return Datatables::of($subscriptions)
                            ->addColumn('created_at', function ($subscriptions) {
                            $data = '-';
                            if($subscriptions->created_at != ''):
                            $data = $subscriptions->created_at;
                            endif;
                            return $data;
                            })
                            ->addColumn('updated_at', function ($subscriptions) {
                            $data = '-';
                            if($subscriptions->updated_at != ''):
                            $data = $subscriptions->updated_at;
                            endif;
                            return $data;
                            })
                            ->addColumn('invoice', function ($subscriptions) {
                                $data = '<a target="_blank" href="'.URL::to("user/invoice/".$subscriptions->transaction_id."/".$subscriptions->vendor_id).'" class="invoice-'.$subscriptions->transaction_id.'" title="'.trans("messages.Invoice").'">&nbsp;&nbsp;'.@trans("messages.Invoice").'</a>';;
                                return $data;
                            })
                            ->addColumn('vendor', function ($subscriptions) {
                                $data = '<a target="_blank" href="'.URL::to("vendors/edit_vendor/".$subscriptions->vendor_id).'" class="invoice-'.$subscriptions->transaction_id.'" title="'.trans("messages.Show").'">&nbsp;&nbsp;'.$subscriptions->first_name.'</a>';;
                                return $data;
                            })
                            ->addColumn('view_transaction', function ($subscriptions) {
                                $data = '<a href="'.URL::to("vendors/view_transaction/".$subscriptions->transaction_id).'" class="invoice-'.$subscriptions->transaction_id.'" title="'.trans("messages.Show").'">view</a>';;
                                return $data;
                            })
                            ->make(true);
                    }

                    //vendorgetDone

                public function getDone(Request $request)
                    {
                        // if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }
                        $checkout_info = Session::get('checkout_info');
                        if($checkout_info=='')
                        {
                            Session::flash('message', 'Error: Oops. Something went wrong. Please try again later.');
                            return Redirect::to('/');
                        }
                        $id    = $request->get('paymentId');
                        $token = $request->get('token');
                        $payer_id = $request->get('PayerID');
                        $all_info = Paypal::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);

                        /** get payment request responce  **/
                            $payment = PayPal::getById($id, $this->_apiContext);
                            $paymentExecution = PayPal::PaymentExecution();
                            $paymentExecution->setPayerId($payer_id);
                            $executePayment = $payment->execute($paymentExecution, $this->_apiContext);
                        /** get payment request responce  **/
                        //Session::put('checkout_info','');
                        if(isset($executePayment->state) && $executePayment->state=="approved")
                        {
                            $Id = $executePayment->getId();
                            $Intent = $executePayment->getIntent();
                            $Payer  = $executePayment->getPayer();
                            $Payee  = $executePayment->getPayee();
                            $Cart   = $executePayment->getCart();
                            $payment_method = $executePayment->payer->payment_method;
                            $paypal_email   = $executePayment->payer->payer_info->email;
                            $country_code   = $executePayment->payer->payer_info->country_code;
                            $Transactions   = $executePayment->getTransactions();
                            $PaymentInstruction = $executePayment->getPaymentInstruction();
                            $State = $executePayment->getState();
                            $ExperienceProfileId = $executePayment->getExperienceProfileId();
                            $CreateTime = $executePayment->getCreateTime();
                            $UpdateTime = $executePayment->getUpdateTime();
                            $ApprovalLink = $executePayment->getApprovalLink();
                            $all_data = $executePayment->get($id, $this->_apiContext);
                            $payment_params = '';
                            if(isset($Transactions['0']) && $Transactions['0'] != '')
                            {
                                $related_resource = $Transactions['0']->related_resources;
                               // print_r($related_resource);exit;
                                foreach($related_resource as $key => $value)
                                {
                                    /** get payment transaction responce  **/
                                    $payment_id = $value->sale->id;
                                    $payment_state  = $value->sale->state;
                                    $payment_amount = $value->sale->amount;
                                    $payment_mode   = $value->sale->payment_mode;
                                    //$reason_code  = $value->state->reason_code;
                                    $protection_eligibility = $value->sale->protection_eligibility;
                                    $protection_eligibility = $value->sale->protection_eligibility_type;
                                    $parent_payment = $value->sale->parent_payment;
                                    $create_time = $value->sale->create_time;
                                    $update_time = $value->sale->update_time;
                                    $links = $value->sale->links;
                                    /** get payment transaction responce  end **/
                                    /**calculate admin and vendor commision ammount*/
                                        /*$admin_commision_per=getAppPaymentConfig()->commision;
                                        $admin_commision=round($payment_amount->total)/100*$admin_commision_per;
                                        $vendor_commision=round($payment_amount->total)-$admin_commision;*/
                                    /**calculate admin and vendor commision ammount end*/

                                    /** set payment_params  */
                                    $payment_params = array("parent_payment_id"=>$Id,"token"=>$token,"payer_id"=>$payer_id,"Intent"=>$Intent,"Payee"=>$Payee,"cart_id"=>$Cart,"payment_id"=>$payment_id,"payment_state"=>$State,"payment_amount"=>$payment_amount,"payment_mode"=>$payment_mode,"create_time"=>$create_time,"update_time"=>$update_time,"links"=>$links,'payment_method'=>$payment_method,'paypal_email'=>$paypal_email,'country_code'=>$country_code);
                                    //$payment_params = array("parent_payment_id"=>$Id,"token"=>$token,"payer_id"=>$payer_id,"Intent"=>$Intent,"Payee"=>$Payee,"cart_id"=>$Cart,"payment_id"=>$payment_id,"payment_state"=>$State,"payment_amount"=>$payment_amount,"payment_mode"=>$payment_mode,"reason_code"=>$reason_code,"valid_until"=>$valid_until,"create_time"=>$create_time,"update_time"=>$update_time,"links"=>$links,'payment_method'=>$payment_method,'paypal_email'=>$paypal_email,'country_code'=>$country_code,'admin_commision'=>$admin_commision,'vendor_commision'=>$vendor_commision);
                                    /** set payment_params  end*/
                                    //$appoinment_id=$this->Appointmentbook($checkout_info,$payment_params);
                                    $user_id  = Session::get('user_id');
                                    $token    = Session::get('token');
                                    $language = getCurrentLang();
                                    $checkout_info  = json_encode($checkout_info);
                                    $payment_params = json_encode($payment_params);
                                    $user_array = array("user_id" => $user_id,"token"=>$token,"language"=>$language,"payment_array" =>$checkout_info,"payment_params"=>$payment_params);
                                    //print_r($user_array);exit;
                                    $method = "POST";
                                    $data   = array('form_params' => $user_array);
                                    //~ echo '<pre>';print_r($user_array);die;
                                    $response = $this->api->call_api($data,'api/online_payment',$method);
                                    if($response->response->httpCode == 200)
                                    {
                                        Session::flash('message-success', trans('messages.Your order has been placed successfully'));
                                        return Redirect::to('/thankyou/'.encrypt($response->response->order_id))->send();
                                    }
                                    else
                                    {
                                        Session::flash('message-failure', $response->response->Message);
                                        return Redirect::to('/checkout')->send();
                                    }
                                }
                            }
                            else
                            {
                                Session::flash('message', 'Error:PaymentSuccess Oops. Something went wrong. Please try again later.');
                                return Redirect::to('/');
                            }
                        }
                    }

                public  function getCancel()
                    {
                        Session::flash('message', 'Error:Proccess has been cancelled by user.');
                        return Redirect::to('/checkout');
                    }

                public function vendorgetCancel()
                    {
                        // if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }
                        Session::flash('message', 'Error:Proccess has been cancelled by user.');
                        return Redirect::to('/vendors/billing');
                    }


                public function subscribe_vendor(Request $data)
                    {
                        // if (Auth::guest())
                        // {
                        //     return redirect()->guest('admin/login');
                        // }
                        $post_data = $data->all();
                         //print_r($post_data);exit;
                        if(Session::get('vendor_type') == 2)
                        {
                            $vendor_id = Session::get('staffs_under_by');
                        }
                        else
                        {
                            $vendor_id = Session::get('vendor_id');
                        }
                        $user = vendors::find($vendor_id);
                        $amount = strtolower($post_data['subscription_plan_price_common']);
                        $plan_name = strtolower($post_data['subscription_plan_name_common']);
                        $plan_id= strtolower($post_data['subscription_plan_id_common']);
                        //Vishal for renewal status
                         $subscriptionplan_details=Subscription_plan::find($plan_id);
                                    
                                    if($subscriptionplan_details->renewal_status==1){
                                        $user->plan_renewal_status=1;

                                    }




                        if($amount == 0)
                        {
                            $subscription_plan = DB::select('select * from subscriptions where user_id = ?',array($vendor_id));
                            if(count($subscription_plan)>0)
                            {
                                DB::update('delete from subscriptions where user_id = ? ',array($vendor_id));
                                $values = array('user_id'       =>$vendor_id,
                                            'plan_id'      => $plan_id,
                                            'created_at'   => date("Y-m-d H:i:s"));
                                DB::table('subscriptions')->insert($values);
                            }
                            else
                            {
                                $values = array('user_id'  =>$vendor_id,
                                            'plan_id'      => $plan_id,
                                            'created_at'   => date("Y-m-d H:i:s"));
                                DB::table('subscriptions')->insert($values);
                            }
                            
                            $plan_endate = "";
                            $current_paln = DB::table('subscriptions')
                            ->select('subscription_plan.*','subscription_plan_infos.*','subscriptions.created_at as plan_date')
                            ->Leftjoin('subscription_plan','subscription_plan.id','=','subscriptions.plan_id')
                            ->Leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
                            ->where('subscriptions.user_id',$vendor_id)
                            //->whereRaw($query)
                            ->orderBy('subscriptions.id', 'desc')
                            ->limit(1)
                            ->get();
                            if(count($current_paln)>0)
                            {
                                $plan_endate =  date('Y-m-d H:i:s',strtotime('+'.$current_paln[0]->subscription_plan_duration.' days',strtotime($current_paln[0]->plan_date)));
                            }
                            
                            $user->active_status = 1;
                            $user->plan_ends_at = $plan_endate;
                            $user->save();
                            Session::flash('message', trans('messages.Plan updated successfully'));
                            return Redirect::to('vendors/billing');
                        }
                        $payment_type = $post_data['payment_type'];

                        $plan_amount = $post_data['subscription_plan_price_common'];
                        $user_id = $vendor_id;
                        if($payment_type == 1)
                        {
                            $payment_gateway_type = $post_data['payment_gateway'];
                            if($payment_gateway_type == 1)
                            {
                                $payment_array = array();
                                $payment_array['user_id']            = $user_id;
                                $payment_array['total']              = $plan_amount;
                                $payment_array['order_status']       = 1;
                                $payment_array['order_key']          = str_random(32);
                                $payment_array['transaction_id']     = str_random(32);
                                $payment_array['transaction_staus']  = 1;
                                $payment_array['transaction_amount'] = $plan_amount;
                                $payment_array['payer_id']           = str_random(32);
                                $payment_array['currency_code']      = getCurrency();
                                $items = array();
                                $i     = 0;
                                $from_currency = "USD";
                                $to_currency   = getCurrencycode();
                                Session::put('checkout_info',$payment_array);
                                $paypal=Paypal::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);
                                try
                                {
                                    $payer = PayPal::Payer();
                                    $payer->setPaymentMethod('paypal');
                                    $amount = PayPal:: Amount();
                                    $amount->setCurrency('USD');
                                    $amount->setTotal($plan_amount);
                                    $itemList = new ItemList();
                                    $transaction = PayPal::Transaction();
                                    $transaction->setAmount($amount);
                                    $info ='Place order - Pay on $ '.$plan_amount;
                                    $transaction->setDescription($info);
                                    $redirectUrls = PayPal::RedirectUrls();
                                    $redirectUrls->setReturnUrl(route('vendorgetDone'));
                                    $redirectUrls->setCancelUrl(route('vendorgetCancel'));
                                    $payment = PayPal::Payment();
                                    $payment->setIntent('sale');
                                    $payment->setPayer($payer);
                                    $payment->setRedirectUrls($redirectUrls);
                                    $payment->setTransactions(array($transaction));
                                    $response = $payment->create($this->_apiContext);
                                    $redirectUrl = $response->links[1]->href;
                                    return redirect()->to($redirectUrl);
                                }
                                catch(Exception $ex)
                                {
                                    ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex); exit(1);
                                }
                                Session::flash('message', 'Error: Oops. Something went wrong. Please try again later.');
                                return Redirect::to('/');
                            }
                            else if($payment_gateway_type == 2)
                            {
                                $stripe = new Stripe();
                                //print_r($stripe);exit;
                                $stripe = Stripe::make();

                                if($plan_amount >0)
                                {
                                    $stripetoken = $post_data['stripeToken'];
                                    if($user->stripe_id == "")
                                    {
                                        $stripetoken = $post_data['stripeToken'];
                                        $customer = $stripe->customers()->create([
                                            'email' =>$user->email,
                                            'source'  => $stripetoken
                                        ]);
                                        $user->stripe_id = $customer['id'];
                                        $user->save();
                                    }

                                if ( is_numeric( $plan_amount ) && strpos( $plan_amount, '.' ) != false ){
                                        $amount_planned=number_format((float)$plan_amount, 2, '.', '');
                                        $submitting_amount=$amount_planned*100;
                                        $response = $user->charge($submitting_amount);
                                    }else{
                                    $response = $user->charge($plan_amount."00");
                                    }
                                    /*$values = array('vendor_id'       =>$vendor_id,
                                                    'invoice_id'      => str_random(32),
                                                    'total_amount'      => $response->amount,
                                                    'payment_status' => "SUCCESS",
                                                    'payer_id' =>  $response->name,
                                                    'country_code'   => $response->country,
                                                    'payment_type'   => $response->type,
                                                    'transaction_id' => $response->id,
                                                    'currency_code'   => $response->currency,
                                                    'payment_type'   => "Stripe",
                                                    'plan_id'   => $plan_id,
                                                    'created_date'   => date("Y-m-d H:i:s"));
                                    DB::table('subscription_transaction')->insert($values);*/
                                    
                                    $plan_amount_res=$response->amount/100;
                                    $subscription_transaction = new subscription_transaction;
                                    $subscription_transaction->vendor_id = $vendor_id;
                                    //$subscription_transaction->invoice_id =  str_random(32);
                                    $subscription_transaction->transaction_id =  $response->id;
                                    $subscription_transaction->total_amount =  $plan_amount_res;
                                    $subscription_transaction->payment_status =  "SUCCESS";
                                    $subscription_transaction->payer_id = $response->name;
                                    $subscription_transaction->payment_type =  "Stripe";
                                    $subscription_transaction->country_code =  $response->country;
                                    $subscription_transaction->plan_id =  $plan_id;
                                    $subscription_transaction->currency_code =  $response->currency;
                                    $subscription_transaction->payment_notes =  $post_data['payment_notes'];
                                    $subscription_transaction->created_date =  date("Y-m-d H:i:s");
                                    $subscription_transaction->save();
                                    
                                    $subscription_transaction->invoice_id = 'INV'.str_pad($subscription_transaction->id,8,"0",STR_PAD_LEFT).time();
                                    $subscription_transaction->save();

                                        \Stripe\Stripe::setApiKey("sk_test_wFcqx8WYVjmrFNRqCIHDiyEe");

                                    $customerID=$user->stripe_customer_id;
                                    if($subscriptionplan_details->renewal_status==1){


                                            

                                            if($customerID){
                                               $cu= \Stripe\Customer::retrieve($customerID);
                                               // $cu = Stripe_Customer::retrieve( $customerID );
                 
                                                // update the customer's card info (in case it has changed )
                                                $cu->card = $post_data['stripeToken'];
                                         
                                                // update a customer's subscription
                                                $cu->updateSubscription(array(
                                                        "plan" => $subscriptionplan_details->url_key,
                                                    )
                                                );
                                         
                                                //$user->stripe_subscription_id=$sub_response->id;
                                                // save everything
                                                $cu->save();

                                            }else{

                                                $customer = \Stripe\Customer::create(array(
                                              "email" => $user->email,
                                              'card' => $post_data['stripeToken']
                                            ));

                                                $customerID=$customer->id;


                                            $sub_response=\Stripe\Subscription::create(array(
                                              "customer" => $customerID,
                                              "items" => array(
                                                array(
                                                  "plan" => $subscriptionplan_details->url_key,
                                                ),
                                              ),
                                            ));


                                            
                                            // {
                                            //   "id": "cus_4fdAW5ftNQow1a",
                                            //   "object": "customer",
                                            //   "account_balance": 0,
                                            //   "created": 1511335725,
                                            //   "currency": null,
                                            //   ...
                                            //   "livemode": false,
                                            //   "email": "jenny.rosen@example.com",
                                            //   ...
                                            // }


                                           // \Stripe\Stripe::setApiKey("sk_test_wFcqx8WYVjmrFNRqCIHDiyEe");

                                            

                                            $user->stripe_subscription_id=$sub_response->id;

                                        }
                                            
                                            $user->stripe_customer_id=$customerID;



                                    }else{

                                         if($customerID){

                                                // $cu = Stripe_Customer::retrieve( $customerID );
                                                // // update the customer's card info (in case it has changed )
                                                // $cu->card = $post_data['stripeToken'];
                                                // // update a customer's subscription
                                                // $cu->updateSubscription(array(
                                                //         "plan" => "",
                                                //     )
                                                // );
                                                // // save everything
                                                // $cu->save();
                                            $subscription = \Stripe\Subscription::retrieve($user->stripe_subscription_id);
                                            $subscription->cancel(array('at_period_end' => true));

                                            }
                                         

                                    }
                                    
                                }
                                $subscription_plan = DB::select('select * from subscriptions where user_id = ?',array($vendor_id));
                                if(count($subscription_plan)>0)
                                {
                                    DB::update('delete from subscriptions where user_id = ? ',array($vendor_id));
                                    $values = array('user_id'       =>$vendor_id,
                                                'plan_id'      => $plan_id,
                                                'created_at'   => date("Y-m-d H:i:s"));
                                    DB::table('subscriptions')->insert($values);
                                }
                                else
                                {
                                    $values = array('user_id'       =>$vendor_id,
                                                'plan_id'      => $plan_id,
                                                'created_at'   => date("Y-m-d H:i:s"));
                                    DB::table('subscriptions')->insert($values);
                                }
                                $plan_endate = "";
                                $current_paln = DB::table('subscriptions')
                                ->select('subscription_plan.*','subscription_plan_infos.*','subscriptions.created_at as plan_date')
                                ->Leftjoin('subscription_plan','subscription_plan.id','=','subscriptions.plan_id')
                                ->Leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscription_plan.id')
                                ->where('subscriptions.user_id',$vendor_id)
                                ->orderBy('subscriptions.id', 'desc')
                                ->limit(1)
                                ->get();
                                if(count($current_paln)>0)
                                {
                                    $plan_endate =  date('Y-m-d H:i:s',strtotime('+'.$current_paln[0]->subscription_plan_duration.' days',strtotime($current_paln[0]->plan_date)));
                                }
                                $user->active_status = 1;
                                $user->plan_ends_at = $plan_endate;
                                $user->save();
                            }
                        }
                        else if($payment_type ==2)
                        {
                            $subscription_transaction = new subscription_transaction;
                            $subscription_transaction->vendor_id = $vendor_id;
                            //$subscription_transaction->invoice_id =  str_random(32);
                            $subscription_transaction->transaction_id =  str_random(32);
                            $subscription_transaction->total_amount =  $amount;
                            $subscription_transaction->payment_status =  "SUCCESS";
                            $subscription_transaction->payer_id = $user->email;
                            $subscription_transaction->payment_type =  "Offline";
                            $subscription_transaction->country_code =  "";
                            $subscription_transaction->plan_id =  $plan_id;
                            $subscription_transaction->currency_code =  "";
                            $subscription_transaction->payment_notes =  $post_data['payment_notes'];
                            $subscription_transaction->created_date =  date("Y-m-d H:i:s");
                            $subscription_transaction->save();
                            if(isset($_FILES['transaction_receipt']['name']) && $_FILES['transaction_receipt']['name']!=''){
                                //get last insert id
                                $imageName = $subscription_transaction->id . '.' . $data->file('transaction_receipt')->getClientOriginalExtension();
                                $data->file('transaction_receipt')->move(
                                    base_path() . '/public/assets/admin/base/images/vendors/transaction_receipt/', $imageName
                                );
                                $subscription_transaction->transaction_receipt=$imageName;
                                $subscription_transaction->save();
                            }
                            
                             $subscription_transaction->invoice_id = 'INV'.str_pad($subscription_transaction->id,8,"0",STR_PAD_LEFT).time();
                             $subscription_transaction->save();
                            
                            
                            $subscription_plan = DB::select('select * from subscriptions where user_id = ?',array($vendor_id));
                            if(count($subscription_plan)>0)
                            {
                                DB::update('delete from subscriptions where user_id = ? ',array($vendor_id));
                                $values = array('user_id'       =>$vendor_id,
                                            'plan_id'      => $plan_id,
                                            'created_at'   => date("Y-m-d H:i:s"));
                                DB::table('subscriptions')->insert($values);
                            }
                            else
                            {
                                $values = array('user_id'       =>$vendor_id,
                                            'plan_id'      => $plan_id,
                                            'created_at'   => date("Y-m-d H:i:s"));
                                DB::table('subscriptions')->insert($values);
                            }
                            $user->active_status = 0;
                            $user->save();
                        }
                        Session::flash('message', trans('messages.Plan updated successfully'));
                        return Redirect::to('vendors/billing');
                    }

                    public function changeProperty(Request $data)
                    {

                       if (!Session::get('vendor_id'))
                            {
                                return redirect()->guest('vendors/login');
                            }
                                                
                        $property_id = Input::get('outlet_name'); 

                        Session::put('property_id',$property_id);
                        //  Session::put('property_id',1);

                        return Redirect::to('vendors/dashboard');

                    }

                    public function reviews()
                    {
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        else {

                            if(!has_staff_permission('vendors/reviews'))
                            {
                                return view('errors.404');
                            }

                        // if (!Session::get('vendor_id')){
                        //     return redirect()->guest('vendors/login');
                        // }
                        // if(!hasTaskmerchant('vendors/reviews'))
                        // {
                        //     return view('errors.404');
                        // }
                            //  SEOMeta::setTitle('Manage Reviews - '.$this->site_name);
                            SEOMeta::setTitle('Manage Reviews');
                            SEOMeta::setDescription('Manage Reviews');
                            return view('vendors.reviews.list');
                        }

                    }


                    public function anyAjaxreviewlistvendor()
                    {

                        $language = getAdminCurrentLang();
                        $query = 'outlet_infos.language_id = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.$language.' and outlets.id = outlet_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
                        $reviews = DB::table('outlet_reviews')
                                    ->select('outlet_reviews.id as review_id','outlet_reviews.customer_id as review_customer_id','outlet_reviews.vendor_id as review_vendor_id','outlet_reviews.comments','outlet_reviews.title','outlet_reviews.approval_status','outlet_reviews.ratings','outlet_reviews.created_date as review_posted_date','admin_customers.firstname as user_name','admin_customers.email as user_email','admin_customers.id as user_id'/*,'admin_customers.image as user_image'*/,'vendors.id as store_id','vendors.first_name as store_first_name','vendors.last_name as store_last_name','vendors.email as store_email','vendors.phone_number as store_phone_number','outlet_infos.outlet_name','outlets.id as outletid')
                                    ->leftJoin('admin_customers','admin_customers.id','=','outlet_reviews.customer_id')
                                    ->leftJoin('outlets','outlets.id','=','outlet_reviews.outlet_id')
                                    ->leftJoin('outlet_infos','outlets.id','=','outlet_infos.id')
                                    ->leftJoin('vendors','vendors.id','=','outlets.vendor_id')
                                    ->where('outlet_reviews.vendor_id','=',Session::get('vendor_id'))
                                    ->where('outlet_reviews.outlet_id','=',Session::get('property_id'))
                                    ->whereRaw($query)
                                    ->orderBy('outlet_reviews.id', 'desc');
                            return Datatables::of($reviews)->addColumn('action', function ($reviews) {
                             
                                $review_status_opt = '';

                                if(has_staff_permission('vendors/reviews/view'))
                                {
                                
                                if($reviews->approval_status == 0):
                                    $review_status_opt = '<li><a href="'.URL::to("vendors/reviews/approve/".$reviews->review_id.'?status=1').'" class="block-'.$reviews->review_id.'"  title="'.trans("messages.Approve").'"> <i class="fa fa-lock"></i> '.trans("messages.Approve").'</a></li>
                                    <script type="text/javascript">
                                        $( document ).ready(function() {
                                            $(".block-'.$reviews->review_id.'").on("click", function(){
                                                return confirm("'.trans("messages.Are you sure want to approve ?").'");
                                            });
                                        });
                                    </script>';
                                endif;
                                return '<div class="btn-group"><a href="'.URL::to("vendors/reviews/view/?review_id=".$reviews->review_id).'" class="btn btn-xs btn-white" title="'.trans("messages.View").'"><i class="fa fa-eye"></i>&nbsp;'.trans("messages.View").'</a>
                                        <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu xs pull-right" role="menu">'.$review_status_opt.'
                                            <li><a href="'.URL::to("vendors/reviews/delete/".$reviews->review_id).'" class="delete-'.$reviews->review_id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li></ul>
                                        </ul>
                                    </div>
                                    <script type="text/javascript">
                                        $( document ).ready(function() {
                                            $(".delete-'.$reviews->review_id.'").on("click", function(){
                                                return confirm("'.trans("messages.Are you sure want to delete?").'");
                                            });
                                        });
                                    </script>';
                                }
                            
                            })
                            ->addColumn('approval_status', function ($reviews) {
                                if($reviews->approval_status==0):
                                    $data = '<span  class="label label-danger">'.trans("messages.Pending").'</span>';
                                elseif($reviews->approval_status==1):
                                    $data = '<span  class="label label-success">'.trans("messages.Approved").'</span>';
                                endif;
                                return $data;
                            })
                            ->addColumn('transaction_date', function ($reviews) {
                                    $data = '<span> '.date('d - M - Y h:i A' , strtotime($reviews->review_posted_date)).'</span>';
                                return $data;
                            })
                            ->make(true);
                    }

                    public function view_review()
                    {
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        else {

                            if(!has_staff_permission('vendors/reviews/view'))
                            {
                                return view('errors.404');
                            }
                            $review_id = Input::get('review_id');
                            $language  = getAdminCurrentLang();
                            $query1    = 'outlet_infos.language_id = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.$language.' and outlets.id = outlet_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
                            $reviews   = DB::table('outlet_reviews')
                                            ->select('outlet_reviews.id as review_id','outlet_reviews.customer_id as review_customer_id','outlet_reviews.vendor_id as review_vendor_id','outlet_reviews.comments','outlet_reviews.title','outlet_reviews.approval_status','outlet_reviews.ratings','outlet_reviews.created_date as review_posted_date','admin_customers.firstname as user_name','admin_customers.email as user_email','admin_customers.id as user_id'/*,'admin_customers.image as user_image'*/,'vendors.id as store_id','vendors.first_name as store_first_name','vendors.last_name as store_last_name','vendors.email as store_email','vendors.phone_number as store_phone_number','outlet_infos.outlet_name','outlets.id as outletid')
                                            ->leftJoin('admin_customers','admin_customers.id','=','outlet_reviews.customer_id')
                                            ->leftJoin('outlets','outlets.id','=','outlet_reviews.outlet_id')
                                            ->leftJoin('outlet_infos','outlets.id','=','outlet_infos.id')
                                            ->leftJoin('vendors','vendors.id','=','outlets.vendor_id')
                                            ->where('outlet_reviews.id','=',$review_id)
                                            ->where('outlet_reviews.vendor_id','=',Session::get('vendor_id'))
                                            ->whereRaw($query1)
                                            ->first();
                            if(!count($reviews))
                            {
                                Session::flash('message', trans('messages.Invalid Request'));
                                return Redirect::to('vendors/reviews');
                            }
                            SEOMeta::setTitle('View Review - '.$this->site_name);
                            SEOMeta::setDescription('View Review - '.$this->site_name);
                            return view('vendors.reviews.show')->with('review', $reviews);
                        }
                    }

                    public function approve($id)
                    {
                        if(!has_staff_permission('vendors/reviews/approve'))
                        {
                            return view('errors.404');
                        }
                        $reviews = outlet_reviews::find($id);

                        if(!count($reviews)){
                            Session::flash('message', trans('messages.Invalid data'));
                            // print_r('expression');exit;
                            return Redirect::to('admin/reviews');
                        }
                        $reviews->approval_status    = 1;
                        $reviews->save();

                        /**  vendor review average calculating and updated here **/ 
                        $reviews_average=DB::table('outlet_reviews')
                                ->selectRaw('SUM(ratings) as total_rating,count(outlet_reviews.outlet_id) as tcount')
                                ->where("outlet_reviews.outlet_id","=",$reviews->outlet_id)
                                ->where("outlet_reviews.approval_status","=",1)
                                ->get();
                        if(count($reviews_average)){
                            $total_rating = $reviews_average[0]->total_rating;
                            $average_rating=$total_rating/$reviews_average[0]->tcount;
                            $outlets = outlets::find($reviews->outlet_id);
                            $outlets->average_rating    = round($average_rating);
                            $outlets->save();
                        }
                        
                        /**  vendor review average calculating and updated here **/ 
                        $outlets_reviews_average = DB::table('outlets')
                                                    ->selectRaw('SUM(average_rating) as total_outlet_rating,count(outlets.id) as tcount')
                                                    ->where('outlets.vendor_id','=',$reviews->vendor_id)
                                                    ->first();
                        if(count($outlets_reviews_average)){
                            $ototal_rating = $outlets_reviews_average->total_outlet_rating;
                            $oaverage_rating=$ototal_rating/$outlets_reviews_average->tcount;
                            $vendors = Vendors::find($reviews->vendor_id);
                            $vendors->average_rating = round($oaverage_rating);
                            $vendors->save();
                        }
                        
                        Session::flash('message', trans('messages.Review has been approved successfully!'));
                        return Redirect::to('vendors/reviews');
                    }

                    public function review_destory($id)
                    {
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendors/reviews/delete'))
                        {
                            return view('errors.404');
                        }
                        //print_r($id);exit;
                        $reviews = outlet_reviews::find($id);
                        // print_r('deleted');exit;
                        $reviews->delete();
                        Session::flash('message', trans('messages.Review has been deleted successfully!'));
                        return Redirect::to('vendors/reviews');
                    }

                    public function services_reviews()
                    {
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        else {

                            if(!has_staff_permission('vendors/reviews'))
                            {
                                return view('errors.404');
                            }

                        // if (!Session::get('vendor_id')){
                        //     return redirect()->guest('vendors/login');
                        // }
                        // if(!hasTaskmerchant('vendors/reviews'))
                        // {
                        //     return view('errors.404');
                        // }
                            //  SEOMeta::setTitle('Manage Reviews - '.$this->site_name);
                            SEOMeta::setTitle('Manage Service Reviews');
                            SEOMeta::setDescription('Manage Service Reviews');
                            return view('vendors.reviews_service.list');
                        }

                    }


                    public function anyAjaxservicereviewlistvendor()
                    {

                        $language = getAdminCurrentLang();
                        $query = 'outlet_infos.language_id = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.$language.' and outlets.id = outlet_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
                        $reviews = DB::table('outlet_reviews')
                                    ->select('outlet_reviews.id as review_id',
                                        'outlet_reviews.customer_id as review_customer_id',
                                        'outlet_reviews.vendor_id as review_vendor_id',
                                        'outlet_reviews.comments','outlet_reviews.title',
                                        'outlet_reviews.approval_status','outlet_reviews.location',
                                        'outlet_reviews.comfort','outlet_reviews.sleep_quality',
                                        'outlet_reviews.rooms','outlet_reviews.cleanliness',
                                        'outlet_reviews.created_date as review_posted_date',
                                        'admin_customers.firstname as user_name',
                                        'admin_customers.email as user_email','admin_customers.id as user_id'
                                            /*,'admin_customers.image as user_image'*/,
                                        'vendors_view.id as store_id',
                                        'vendors_view.first_name as store_first_name',
                                        'vendors_view.last_name as store_last_name',
                                        'vendors_view.email as store_email',
                                        'vendors_view.phone_number as store_phone_number',
                                        'outlet_infos.outlet_name',
                                        'outlets.id as outletid')
                                    ->leftJoin('admin_customers','admin_customers.id','=','outlet_reviews.customer_id')
                                    ->leftJoin('outlets','outlets.id','=','outlet_reviews.outlet_id')
                                    ->leftJoin('outlet_infos','outlets.id','=','outlet_infos.id')
                                    ->leftJoin('vendors_view','vendors_view.id','=','outlets.vendor_id')
                                    ->where('outlet_reviews.vendor_id','=',Session::get('vendor_id'))
                                    ->where('outlet_reviews.outlet_id','=',Session::get('property_id'))
                                    ->whereRaw($query)
                                    ->orderBy('outlet_reviews.id', 'desc');
                                    //  ->get();
                            //  print_r($reviews);exit;
                            return Datatables::of($reviews)->addColumn('action', function ($reviews) {
                             
                                $review_status_opt = '';

                                if(has_staff_permission('vendors/reviews/view'))
                                {
                                
                                if($reviews->approval_status == 0):
                                    $review_status_opt = '<li><a href="'.URL::to("vendors/reviews/services/approve/".$reviews->review_id.'?status=1').'" class="block-'.$reviews->review_id.'"  title="'.trans("messages.Approve").'"> <i class="fa fa-lock"></i> '.trans("messages.Approve").'</a></li>
                                    <script type="text/javascript">
                                        $( document ).ready(function() {
                                            $(".block-'.$reviews->review_id.'").on("click", function(){
                                                return confirm("'.trans("messages.Are you sure want to approve ?").'");
                                            });
                                        });
                                    </script>';
                                endif;
                                return '<div class="btn-group"><a href="'.URL::to("vendors/reviews/services/services/view/?review_id=".$reviews->review_id).'" class="btn btn-xs btn-white" title="'.trans("messages.View").'"><i class="fa fa-eye"></i>&nbsp;'.trans("messages.View").'</a>
                                        <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu xs pull-right" role="menu">'.$review_status_opt.'
                                            <li><a href="'.URL::to("vendors/reviews/services/delete/".$reviews->review_id).'" class="delete-'.$reviews->review_id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li></ul>
                                        </ul>
                                    </div>
                                    <script type="text/javascript">
                                        $( document ).ready(function() {
                                            $(".delete-'.$reviews->review_id.'").on("click", function(){
                                                return confirm("'.trans("messages.Are you sure want to delete?").'");
                                            });
                                        });
                                    </script>';
                                }
                            
                            })
                            ->addColumn('approval_status', function ($reviews) {
                                if($reviews->approval_status==0):
                                    $data = '<span  class="label label-danger">'.trans("messages.Pending").'</span>';
                                elseif($reviews->approval_status==1):
                                    $data = '<span  class="label label-success">'.trans("messages.Approved").'</span>';
                                endif;
                                return $data;
                            })
                            ->addColumn('transaction_date', function ($reviews) {
                                    $data = '<span> '.date('d - M - Y h:i A' , strtotime($reviews->review_posted_date)).'</span>';
                                return $data;
                            })
                            ->addColumn('user_name', function ($reviews) {
                                    $data = ucfirst($reviews->user_name);
                                return $data;
                            })                            
                            ->make(true);
                    }

                    public function services_view_review()
                    {
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        else {

                            if(!has_staff_permission('vendors/reviews/view'))
                            {
                                return view('errors.404');
                            }
                            $review_id = Input::get('review_id');
                            $language  = getAdminCurrentLang();
                            $query1    = 'outlet_infos.language_id = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.$language.' and outlets.id = outlet_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
                            $reviews   = DB::table('outlet_reviews')
                                            ->select('outlet_reviews.id as review_id','outlet_reviews.customer_id as review_customer_id','outlet_reviews.vendor_id as review_vendor_id','outlet_reviews.comments','outlet_reviews.title','outlet_reviews.approval_status','outlet_reviews.ratings','outlet_reviews.created_date as review_posted_date','admin_customers.firstname as user_name','admin_customers.email as user_email','admin_customers.id as user_id'/*,'admin_customers.image as user_image'*/,'vendors.id as store_id','vendors.first_name as store_first_name','vendors.last_name as store_last_name','vendors.email as store_email','vendors.phone_number as store_phone_number','outlet_infos.outlet_name','outlets.id as outletid')
                                            ->leftJoin('admin_customers','admin_customers.id','=','outlet_reviews.customer_id')
                                            ->leftJoin('outlets','outlets.id','=','outlet_reviews.outlet_id')
                                            ->leftJoin('outlet_infos','outlets.id','=','outlet_infos.id')
                                            ->leftJoin('vendors','vendors.id','=','outlets.vendor_id')
                                            ->where('outlet_reviews.id','=',$review_id)
                                            ->where('outlet_reviews.vendor_id','=',Session::get('vendor_id'))
                                            ->whereRaw($query1)
                                            ->first();
                            if(!count($reviews))
                            {
                                Session::flash('message', trans('messages.Invalid Request'));
                                return Redirect::to('vendors/reviews');
                            }
                            SEOMeta::setTitle('View Review - '.$this->site_name);
                            SEOMeta::setDescription('View Review - '.$this->site_name);
                            return view('vendors.reviews.show')->with('review', $reviews);
                        }
                    }

                    public function services_approve($id)
                    {
                        if(!has_staff_permission('vendors/reviews/approve'))
                        {
                            return view('errors.404');
                        }
                        $reviews = outlet_reviews::find($id);

                        if(!count($reviews)){
                            Session::flash('message', trans('messages.Invalid data'));
                            // print_r('expression');exit;
                            return Redirect::to('admin/reviews');
                        }
                        $reviews->approval_status    = 1;
                        $reviews->save();

                        /**  vendor review average calculating and updated here **/ 
                        $reviews_average=DB::table('outlet_reviews')
                                ->selectRaw('SUM(ratings) as total_rating,count(outlet_reviews.outlet_id) as tcount')
                                ->where("outlet_reviews.outlet_id","=",$reviews->outlet_id)
                                ->where("outlet_reviews.approval_status","=",1)
                                ->get();
                        if(count($reviews_average)){
                            $total_rating = $reviews_average[0]->total_rating;
                            $average_rating=$total_rating/$reviews_average[0]->tcount;
                            $outlets = outlets::find($reviews->outlet_id);
                            $outlets->average_rating    = round($average_rating);
                            $outlets->save();
                        }
                        
                        /**  vendor review average calculating and updated here **/ 
                        $outlets_reviews_average = DB::table('outlets')
                                                    ->selectRaw('SUM(average_rating) as total_outlet_rating,count(outlets.id) as tcount')
                                                    ->where('outlets.vendor_id','=',$reviews->vendor_id)
                                                    ->first();
                        if(count($outlets_reviews_average)){
                            $ototal_rating = $outlets_reviews_average->total_outlet_rating;
                            $oaverage_rating=$ototal_rating/$outlets_reviews_average->tcount;
                            $vendors = Vendors::find($reviews->vendor_id);
                            $vendors->average_rating = round($oaverage_rating);
                            $vendors->save();
                        }
                        
                        Session::flash('message', trans('messages.Review has been approved successfully!'));
                        return Redirect::to('vendors/reviews');
                    }

                    public function services_review_destory($id)
                    {
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendors/reviews/delete'))
                        {
                            return view('errors.404');
                        }
                        //print_r($id);exit;
                        $reviews = outlet_reviews::find($id);
                        // print_r('deleted');exit;
                        $reviews->delete();
                        Session::flash('message', trans('messages.Review has been deleted successfully!'));
                        return Redirect::to('vendors/reviews');
                    }


                    public function update_status(Request $data)
                    {
/*                        
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }
*/                        
                        $post_data = $data->all();

                        //  print_r($post_data);exit;
                        $affected = DB::update('update booking_details set modified_date = ?,booking_status = ?,order_comments = ? where id = ?', array(date("Y-m-d H:i:s"),$post_data['order_status_id'],$post_data['comment'],$post_data['order_id']));
                        $affected = DB::update('update orders_log set booking_status=?, order_comments = ? where id = (select max(id) from orders_log where order_id = '. $post_data['order_id'].')', array($post_data['order_status_id'],$post_data['comment']));


                        if($post_data['order_status_id'] == 4)
                        {
                                //  CANCELLED

                                $order_id = $post_data['order_id'];

                                $booking_detail = booking_detail::find($order_id);

                                $booking_detail->booking_status = 4;   //  CANCELLED

                                $booking_detail->save();

                                $brd = DB::table('booked_room')
                                        ->where('booking_id','=',$order_id)
                                        ->update(['booking_status' => 4, 'status' => 2]);
                        }
                       
                            $order_detail = $this->get_order_detail($post_data['order_id']);

                            //  print_r($order_detail);exit;
/*
                            $order_details = $order_detail->rooms->room_name;

                            //  print_r($order_details); exit;
                             $item = "";
                            $numItems = count($order_details);
                            $i=0;
                            foreach($order_details as $items)
                            {
                                //$item .= wordwrap(ucfirst(strtolower($items->product_name)),40,"<br>\n");

                                 if(++$i === $numItems) {
                                    $item .= wordwrap(ucfirst(strtolower($items->room_name.'.')),40,"<br>\n");

                                  }else{
                                    $item .= wordwrap(ucfirst(strtolower($items->room_name.',')),40,"<br>\n");

                                  }
                            }

                            print_r($item);exit;
*/                            
                            //  $delivery_details = $order_detail["delivery_details"];


                            $vendor_info = $order_detail["vendors"];

                            $booking_details = $order_detail["booking_details"];

                            $outlets = $order_detail["properties"];

                            $logo = url('/assets/front/'.Session::get("general")->theme.'/images/'.Session::get("general")->site_name.'.png');
                            if(file_exists(base_path().'/public/assets/admin/base/images/vendors/list/'.$vendor_info[0]->logo_image)) {
                                $vendor_image ='<img width="100px" height="100px" src="'.URL::to("assets/admin/base/images/vendors/list/".$vendor_info[0]->logo_image).'") >';
                            }
                            else
                            {
                                $vendor_image ='<img width="100px" height="100px" src="'.URL::to("assets/front/'".Session::get("general")->theme."'/images/blog_no_images.png").'") >';
                            }
/*                            
                            $delivery_date = date("d F, l", strtotime($delivery_details[0]->delivery_date));
                            $delivery_time = date('g:i a', strtotime($delivery_details[0]->start_time)).'-'.date('g:i a', strtotime($delivery_details[0]->end_time));
*/                            
                            $users = admin_customers::find($booking_details[0]->customer_id);

                            //  print_r($users);exit;
                            $to=$users->email;

                            $subject = 'Your Appointment with '.getAppConfig()->site_name.' has been successfully '.$booking_details[0]->name.'!';

                            $outlet_name = $outlets[0]->outlet_name;
                            

                            if($post_data['order_status_id'] == 3){

                            //  CHECKED OUT
                               /* $customer_referrals = admin_customers::where('id',$users->id)
                                                        ->select('referred_customer_id',
                                                            'referral_status',)->get(); */
                                $customer_details = get_user_details1($users->id);
                                if(count($customer_details) > 0){
                                    $referred_customer_id = $customer_details->referred_customer_id;
                                    $referral_status = $customer_details->referral_status;
                                    if($referred_customer_id != 0 && $referral_status != 1)
                                    {
                                        try{
                                            //Amount Calculation
                                            $amount = 0 ;

                                            //Insert booking referral logs
                                            $booking_referrals = new Booking_referral_logs;
                                            $booking_referrals->customer_id = $users->id;
                                            $booking_referrals->referrer_id = $referred_customer_id;
                                            $booking_referrals->referral_amount = $amount;
                                            $booking_referrals->save();

                                            //Update the total amount of a referred customer 
                                            $referral_amount = get_user_details1($referred_customer_id);
                                            $referrer_amount = $referral_amount->wallet_amount + $amount;

                                            $ref_balance = Admin_customers::find($referred_customer_id);
                                            $ref_balance->wallet_amount = $referrer_amount;
                                            $ref_balance->save();

                                            //Update referral status in customers table
                                            $ref_status = Admin_customers::find($users->id);
                                            $ref_status->referral_status = 1;
                                            $ref_status->save();

                                        } catch(Exception $e){
                                            Log::Instance()->add(Log::ERROR, $e);
                                        }
                                    }
                                }                                                                                                           
                                $template=DB::table('email_templates')
                                        ->select('*')
                                        ->where('template_id','=',self::APPOINTMENT_FINAL_INVOICE_THANKS)
                                        ->get();

                                        $attachment = "";

                                        //  $attachment[] = base_path().'/public/assets/admin/base/images/invoice/'.$delivery_details[0]->invoice_id.'.pdf';

                                $msg = DB::table('sms_templates')
                                            ->select('message')
                                            ->where('id','=',self::USER_FINAL_ORDER_UPDATE_TEMPLATE)
                                            ->first();                                        
                                                            
                            }else{

                            //  CHECKIN,CANCEL,PENDING


                                $template=DB::table('email_templates')
                                        ->select('*')
                                        ->where('template_id','=',self::ORDER_STATUS_UPDATE_USER)
                                        ->get();

                                        $attachment = "";

                                $msg = DB::table('sms_templates')
                                            ->select('message')
                                            ->where('id','=',self::USER_ORDER_STATUS_UPDATE_TEMPLATE)
                                            ->first();                                        

                            }

                    //  SEND E-MAIL

                            if(count($template))
                            {
                                $from = $template[0]->from_email;
                                $from_name=$template[0]->from;
                                if(!$template[0]->template_id)
                                {
                                    $template = 'mail_template';
                                    $from = getAppConfigEmail()->contact_mail;
                                      // $subject = 'Your Appointment status with '.getAppConfig()->site_name.' ['.$vendor_info[0]->order_key_formated .'] has been changed to '.$vendor_info[0]->status_name.'!';
                                    $from_name="";
                                }

                                $order = '<a href="'.URL::to("/orders").'" title="Order">Orders</a>';
                               
                                $check_in_date = date("d F, l", strtotime($booking_details[0]->check_in_date));
                                $check_out_date = date("d F, l", strtotime($booking_details[0]->check_out_date));
                                                                            
                                $slot_date = $check_in_date ." - ". $check_out_date;
                                
                                $hotel_address = ($outlets[0]->contact_address != '')?ucfirst($outlets[0]->contact_address):'-';

                                $default_sitename = getAppConfig()->site_name;

                                $hotel_name='<a href="'.URL::to("view-hotel/".$outlets[0]->url_index).'" title="'.$outlet_name.'">'.$outlet_name.'</a>';

                                $hotel_review='<a href="'.URL::to("order-info/".encrypt($post_data['order_id'])).'" title= "Review Hotel"">Click here to share your Feedback</a>';

                                $salon_review_link = url('/order-info/'.encrypt($post_data['order_id']));
                                
                                $outlet_address = $hotel_address;
                                $user_profile= url('/profile/');

                                $content =array("order" => array('order_key'=>$booking_details[0]->booking_random_id,'status_name'=>$booking_details[0]->name,
                                    'name' =>$users->firstname,
                                    'appointment_date'=>$slot_date, 
                                    'outlet_name'=>$outlet_name,'location'=>$outlet_address,
                                    'cancellation_policy'=>$outlets[0]->cancellation_policy,
                                    'hotel_name' => $hotel_name, 'hotel_address' => $hotel_address,
                                    'user_profile'=>$user_profile,/*'service'=>$item, */ 
                                    'salon_review_link'=>$salon_review_link,
                                    'hotel_review' => $hotel_review,'order' => $order,
                                    'vendor_message'=>$post_data['comment']));
                            
                                //print_r($attachment);exit;
                                $email=smtp($from,$from_name,$to,$subject,$content,$template,$attachment);
                            }  

                    //  SEND SMS

                            $number  = $users->country_code."".$users->mobile_number;

                            $sms_subject = 'Your Appointment with '.$outlet_name.' ['.$booking_details[0]->booking_random_id .']  at  '.getAppConfig()->site_name .'has been successfully '.$booking_details[0]->name.'!';

                            $result = $msg->message;
                            $default_sitename = getAppConfig()->site_name;
                            $message = str_replace('${outlet_name}',$outlet_name,$result);
                            $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                            $message = str_replace('${booking_random_id}', $booking_details[0]->booking_random_id, $message);
                            $message = str_replace('${status_name}', $booking_details[0]->name, $message);
                            $message = str_replace('${feedback_link}', $salon_review_link, $message);
                            

                            if($post_data['comment']!=""){
                                    $message = $message." Hotel Message: ".$post_data['comment'];
                            }else{
                                    $message = $message;
                            }

                            $result = send_sms($number,$message,$result);                                                       
/*
                            $number  = $users->mobile;
                            if(isset($post_data['notify']) && ($post_data['notify'] == 1) && ($number!=null) && ($number!=''))
                            {
                                    
                                    if($post_data['comment']!=""){
                                    $message = $sms_subject." Vendor Message: ".$post_data['comment'];
                                    $subject= $subject." Vendor Message: ".$post_data['comment'];
                                }else{

                                        $message = $sms_subject;
                                        $subject= $subject;
                                

                                }
                                send_sms($users->mobile,$message);
                              
                            $values = array('order_id' => $post_data['order_id'],
                                            'customer_id'  => $delivery_details[0]->customer_id,
                                            'vendor_id'    => $vendor_info[0]->vendor_id,
                                            'outlet_id'    => $vendor_info[0]->outlet_id,
                                            'message'      => $subject,
                                            'read_status'  => 0,
                                            'created_date' => date('Y-m-d H:i:s'));
                            DB::table('notifications')->insert($values);
                        }
*/                        
                        return 1;
                    } 

                    public function order_update_status(Request $data)
                    {
                        if(Auth::guest())
                        {
                            return redirect()->guest('admin/login');
                        } 
                        if(!has_permission('vendors/orders/index'))
                        {
                            return view('errors.404');
                        }  
                        $post_data = $data->all();

                        //  print_r($post_data);exit;
                        $affected = DB::update('update booking_details set modified_date = ?,booking_status = ?,order_comments = ? where id = ?', array(date("Y-m-d H:i:s"),$post_data['order_status_id'],$post_data['comment'],$post_data['order_id']));
                        $affected = DB::update('update orders_log set booking_status=?, order_comments = ? where id = (select max(id) from orders_log where order_id = '. $post_data['order_id'].')', array($post_data['order_status_id'],$post_data['comment']));


                        if($post_data['order_status_id'] == 4)
                        {
                                //  CANCELLED

                                $order_id = $post_data['order_id'];

                                $booking_detail = booking_detail::find($order_id);

                                $booking_detail->booking_status = 4;   //  CANCELLED

                                $booking_detail->save();

                                $brd = DB::table('booked_room')
                                        ->where('booking_id','=',$order_id)
                                        ->update(['booking_status' => 4, 'status' => 2]);
                        }
                       
                            $order_detail = $this->get_order_detail($post_data['order_id']);

                            //  print_r($order_detail);exit;
/*
                            $order_details = $order_detail->rooms->room_name;

                            //  print_r($order_details); exit;
                             $item = "";
                            $numItems = count($order_details);
                            $i=0;
                            foreach($order_details as $items)
                            {
                                //$item .= wordwrap(ucfirst(strtolower($items->product_name)),40,"<br>\n");

                                 if(++$i === $numItems) {
                                    $item .= wordwrap(ucfirst(strtolower($items->room_name.'.')),40,"<br>\n");

                                  }else{
                                    $item .= wordwrap(ucfirst(strtolower($items->room_name.',')),40,"<br>\n");

                                  }
                            }

                            print_r($item);exit;
*/                            
                            //  $delivery_details = $order_detail["delivery_details"];


                            $vendor_info = $order_detail["vendors"];

                            $booking_details = $order_detail["booking_details"];

                            $outlets = $order_detail["properties"];

                            $logo = url('/assets/front/'.Session::get("general")->theme.'/images/'.Session::get("general")->site_name.'.png');
                            if(file_exists(base_path().'/public/assets/admin/base/images/vendors/list/'.$vendor_info[0]->logo_image)) {
                                $vendor_image ='<img width="100px" height="100px" src="'.URL::to("assets/admin/base/images/vendors/list/".$vendor_info[0]->logo_image).'") >';
                            }
                            else
                            {
                                $vendor_image ='<img width="100px" height="100px" src="'.URL::to("assets/front/'".Session::get("general")->theme."'/images/blog_no_images.png").'") >';
                            }
/*                            
                            $delivery_date = date("d F, l", strtotime($delivery_details[0]->delivery_date));
                            $delivery_time = date('g:i a', strtotime($delivery_details[0]->start_time)).'-'.date('g:i a', strtotime($delivery_details[0]->end_time));
*/                            
                            $users = admin_customers::find($booking_details[0]->customer_id);

                            //  print_r($users);exit;
                            $to=$users->email;

                            $subject = 'Your Appointment with '.getAppConfig()->site_name.' has been successfully '.$booking_details[0]->name.'!';

                            $outlet_name = $outlets[0]->outlet_name;
                            

                            if($post_data['order_status_id'] == 3){

                            //  CHECKED OUT
                               /* $customer_referrals = admin_customers::where('id',$users->id)
                                                        ->select('referred_customer_id',
                                                            'referral_status',)->get(); */
                                $customer_details = get_user_details1($users->id);
                                if(count($customer_details) > 0){
                                    $referred_customer_id = $customer_details->referred_customer_id;
                                    $referral_status = $customer_details->referral_status;
                                    if($referred_customer_id != 0 && $referral_status != 1)
                                    {
                                        try{
                                            //Amount Calculation
                                            $amount = 0 ;

                                            //Insert booking referral logs
                                            $booking_referrals = new Booking_referral_logs;
                                            $booking_referrals->customer_id = $users->id;
                                            $booking_referrals->referrer_id = $referred_customer_id;
                                            $booking_referrals->referral_amount = $amount;
                                            $booking_referrals->save();

                                            //Update the total amount of a referred customer 
                                            $referral_amount = get_user_details1($referred_customer_id);
                                            $referrer_amount = $referral_amount->wallet_amount + $amount;

                                            $ref_balance = Admin_customers::find($referred_customer_id);
                                            $ref_balance->wallet_amount = $referrer_amount;
                                            $ref_balance->save();

                                            //Update referral status in customers table
                                            $ref_status = Admin_customers::find($users->id);
                                            $ref_status->referral_status = 1;
                                            $ref_status->save();

                                        } catch(Exception $e){
                                            Log::Instance()->add(Log::ERROR, $e);
                                        }
                                    }
                                }                                                                                                           
                                $template=DB::table('email_templates')
                                        ->select('*')
                                        ->where('template_id','=',self::APPOINTMENT_FINAL_INVOICE_THANKS)
                                        ->get();

                                        $attachment = "";

                                        //  $attachment[] = base_path().'/public/assets/admin/base/images/invoice/'.$delivery_details[0]->invoice_id.'.pdf';

                                $msg = DB::table('sms_templates')
                                            ->select('message')
                                            ->where('id','=',self::USER_FINAL_ORDER_UPDATE_TEMPLATE)
                                            ->first();                                        
                                                            
                            }else{

                            //  CHECKIN,CANCEL,PENDING


                                $template=DB::table('email_templates')
                                        ->select('*')
                                        ->where('template_id','=',self::ORDER_STATUS_UPDATE_USER)
                                        ->get();

                                        $attachment = "";

                                $msg = DB::table('sms_templates')
                                            ->select('message')
                                            ->where('id','=',self::USER_ORDER_STATUS_UPDATE_TEMPLATE)
                                            ->first();                                        

                            }

                    //  SEND E-MAIL

                            if(count($template))
                            {
                                $from = $template[0]->from_email;
                                $from_name=$template[0]->from;
                                if(!$template[0]->template_id)
                                {
                                    $template = 'mail_template';
                                    $from = getAppConfigEmail()->contact_mail;
                                      // $subject = 'Your Appointment status with '.getAppConfig()->site_name.' ['.$vendor_info[0]->order_key_formated .'] has been changed to '.$vendor_info[0]->status_name.'!';
                                    $from_name="";
                                }

                                $order = '<a href="'.URL::to("/orders").'" title="Order">Orders</a>';
                               
                                $check_in_date = date("d F, l", strtotime($booking_details[0]->check_in_date));
                                $check_out_date = date("d F, l", strtotime($booking_details[0]->check_out_date));
                                                                            
                                $slot_date = $check_in_date ." - ". $check_out_date;
                                
                                $hotel_address = ($outlets[0]->contact_address != '')?ucfirst($outlets[0]->contact_address):'-';

                                $default_sitename = getAppConfig()->site_name;

                                $hotel_name='<a href="'.URL::to("view-hotel/".$outlets[0]->url_index).'" title="'.$outlet_name.'">'.$outlet_name.'</a>';

                                $hotel_review='<a href="'.URL::to("order-info/".encrypt($post_data['order_id'])).'" title= "Review Hotel"">Click here to share your Feedback</a>';

                                $salon_review_link = url('/order-info/'.encrypt($post_data['order_id']));
                                
                                $outlet_address = $hotel_address;
                                $user_profile= url('/profile/');

                                $content =array("order" => array('order_key'=>$booking_details[0]->booking_random_id,'status_name'=>$booking_details[0]->name,
                                    'name' =>$users->firstname,
                                    'appointment_date'=>$slot_date, 
                                    'outlet_name'=>$outlet_name,'location'=>$outlet_address,
                                    'cancellation_policy'=>$outlets[0]->cancellation_policy,
                                    'hotel_name' => $hotel_name, 'hotel_address' => $hotel_address,
                                    'user_profile'=>$user_profile,/*'service'=>$item, */ 
                                    'salon_review_link'=>$salon_review_link,
                                    'hotel_review' => $hotel_review,'order' => $order,
                                    'vendor_message'=>$post_data['comment']));
                            
                                //print_r($attachment);exit;
                                $email=smtp($from,$from_name,$to,$subject,$content,$template,$attachment);
                            }  

                    //  SEND SMS

                            $number  = $users->country_code."".$users->mobile_number;

                            $sms_subject = 'Your Appointment with '.$outlet_name.' ['.$booking_details[0]->booking_random_id .']  at  '.getAppConfig()->site_name .'has been successfully '.$booking_details[0]->name.'!';

                            $result = $msg->message;
                            $default_sitename = getAppConfig()->site_name;
                            $message = str_replace('${outlet_name}',$outlet_name,$result);
                            $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                            $message = str_replace('${booking_random_id}', $booking_details[0]->booking_random_id, $message);
                            $message = str_replace('${status_name}', $booking_details[0]->name, $message);
                            $message = str_replace('${feedback_link}', $salon_review_link, $message);
                            

                            if($post_data['comment']!=""){
                                    $message = $message." Hotel Message: ".$post_data['comment'];
                            }else{
                                    $message = $message;
                            }

                            $result = send_sms($number,$message,$result);                                                       
/*
                            $number  = $users->mobile;
                            if(isset($post_data['notify']) && ($post_data['notify'] == 1) && ($number!=null) && ($number!=''))
                            {
                                    
                                    if($post_data['comment']!=""){
                                    $message = $sms_subject." Vendor Message: ".$post_data['comment'];
                                    $subject= $subject." Vendor Message: ".$post_data['comment'];
                                }else{

                                        $message = $sms_subject;
                                        $subject= $subject;
                                

                                }
                                send_sms($users->mobile,$message);
                              
                            $values = array('order_id' => $post_data['order_id'],
                                            'customer_id'  => $delivery_details[0]->customer_id,
                                            'vendor_id'    => $vendor_info[0]->vendor_id,
                                            'outlet_id'    => $vendor_info[0]->outlet_id,
                                            'message'      => $subject,
                                            'read_status'  => 0,
                                            'created_date' => date('Y-m-d H:i:s'));
                            DB::table('notifications')->insert($values);
                        }
*/                        
                        return 1;

                    }
                    public function send_sms($mobile,$message)
                    {
                        $app_config = getSMSConfig();
                        $number  = $mobile;
                        $twilo_sid    = $app_config[0]->sms_account_id;
                        $twilio_token = $app_config[0]->sms_account_token;
                        $from_number  = $app_config[0]->sms_sender_number;
                        
                        // Create an authenticated client for the Twilio API
                        try {
                                
                                $result = Nexmo::message()->send([
                                    'to'   => $number,
                                    'from' => $from_number,
                                    'text' => $message
                                ]);
                                return 2;
                        }
                        catch (Exception $e) {
                            return $e->getMessage();
                            //$result = array("response" => array("httpCode" => 400,"Message" => $e->getMessage()));
                            //return json_encode($result);
                        }
                    }

                /**
                 * Add the specified vendor in storage.
                 * @param  int  $id
                 * @return Response
                 */
                public function vendors_store(Request $data)
                {

                    $post_data = $data->all();

                    //  dd($post_data);

                    $fields['first_name'] = Input::get('first_name');
                    $fields['last_name'] = Input::get('last_name');
                    $fields['vendor_name'] = Input::get('vendor_name');
                    $fields['email'] = Input::get('email');
                    $fields['password'] = Input::get('password');
                    
                    $rules = array(
                        'first_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3|max:32',
                        'last_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:1|max:32',
                        'email' => 'required|email|max:255|unique:vendors_view,email',
                        'password' => 'required',
                        'vendor_name' => 'required',
                    );
                         
                    $validation = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validation->fails())
                    { 
                        return Redirect::back()->withErrors($validation)->withInput();
                    } else {
                        //Store the data here with database
                        try{

                            $users = new Users;                

                            $users->name = $_POST['first_name'];
                            $users->first_name = $_POST['first_name'];
                            $users->last_name = $_POST['last_name'];
                            $users->email = $_POST['email'];
                            $users->password = md5($_POST['password']);
                            $users->country_id = 1;      /*  1-> Malaysia    $_POST['country'];   */  
                            $users->created_date = date('Y-m-d H:i:s');
                            //$users->updated_date = date('Y-m-d H:i:s');
                            $users->save();

                            $this->vendor_insert($users->id,$_POST,$data);
                     
                            Session::flash('message', trans('messages.Vendor has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('vendor-signup');
                    }
                }
                
                public function vendor_insert($user_id,$post,$data) 
                {
                            $vendor_name = $_POST['vendor_name'];
                            $Vendors = new Vendors;

                            $Vendors->user_id = $user_id;
                            $Vendors->vendor_type = 1;
                            $Vendors->active_status = isset($_POST['active_status'])?$_POST['active_status']:0;
                            $Vendors->original_password = $_POST['password'];
                            $Vendors->vendor_unique_id = getRandomNumber(8);
                            $Vendors->save();


                            //  After saving the vendors add the vendor's subscription details in subscriptions table(Free trial day details)

                            $current_date = date('Y-m-d h:i:s');
                            $start_date = date('Y-m-d');
                            $get_plan = getSubscriptionPlan(1);
                            $duration = 0;
                            if(count($get_plan)>0){
                                $duration = $get_plan[0]->subscription_plan_duration;
                            }
                            $end_date = date('Y-m-d', strtotime($start_date. ' + '.$duration.' days'));

                              $subscription_renewal = new Subscriptionrenewal();
                              $subscription_renewal->vendor_id = $user_id;
                              $subscription_renewal->plan_id = 1;
                              $subscription_renewal->last_updated_date = $current_date;
                              $subscription_renewal->subscription_start_date = $start_date;
                              $subscription_renewal->subscription_end_date = $end_date;
                              $subscription_renewal->plan_status = 1;
                              $subscription_renewal->renewal_status = 0;
                              $subscription_renewal->save(); 

                            $this->vendor_save_after($user_id,$Vendors,$_POST,1);             

                }                      

                /**
                 * add,edit datas  saved in main table 
                 * after inserted in sub tabel.
                 * @param  int  $id
                 * @return Response
                 */
                public static function vendor_save_after($user_id,$object,$post,$method=0)
                {

                    if(isset($post['vendor_name'])){
                        $vendor_name = $post['vendor_name'];

                        //  dd('Ak');
                        
                        try{
                            $data = Vendors_infos::find($object->id);
                            if(count($data)>0){
                                $data->delete();
                            }
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
                                if((isset($vendor_name) && $vendor_name!="")){

                                    $infomodel = new Vendors_infos;
                                    $infomodel->vendors_view_id = $user_id;
                                    $infomodel->lang_id = $lang->id;
                                    $infomodel->id = $object->id;
                                    $infomodel->vendor_name = $vendor_name;
                                    $infomodel->save();
                                }
                            }
                            if($method==1){

                                //  dd('Ak');
                                $vendor=$object->getAttributes();
                                $password = $post['password'];
                                $template=DB::table('email_templates')
                                    ->select('*')
                                    ->where('template_id','=',self::VENDORS_REGISTER_EMAIL_TEMPLATE)
                                    ->get();
                                if(count($template)){
                                   $from = $template[0]->from_email;
                                   $from_name = $template[0]->from;
                                   $subject = $template[0]->subject;
                                   if(!$template[0]->template_id){
                                       $template = 'mail_template';
                                       $from = getAppConfigEmail()->contact_email;
                                       $subject = "Welcome to ".getAppConfig()->site_name;
                                       $from_name = "";
                                   }
                                   $content = array("vendor_name" => $vendor_name[getAdminCurrentLang()],"email"=>$post['email'],"password"=>$password);
                                   $email = smtp($from,$from_name,$post['email'],$subject,$content,$template);
                               }
                            }
                        }catch(Exception $e) {
                            
                            Log::Instance()->add(Log::ERROR, $e);
                        }
                    }
                }

                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function customer_feedback()
                {
                    $id = Session::get('vendor_id');
                    if (!$id)
                        {
                            return redirect()->guest('vendors/login');
                        } 
                        if(!has_staff_permission('vendor/customer_feedback'))
                        {
                        return view('errors.405');
                        }
                    else{
                        return view('vendors.customer_feedback.list');
                    }
                    
                }
                
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function customer_feedback_destroy($id)
                {
                    if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/customer_feedback/delete'))
                        {
                        return view('errors.405');
                        }
                        $amenities = DB::table('customer_feedback')
                        ->where('id','=',$id)->get();

                        if (count($amenities) > 0)
                        {
                            $data = customer_feedback::find($id);
                            $data->active_status = 0;
                            $data->save();
                            Session::flash('message', trans('messages.Customer Feedback has been deleted successfully!'));
                            return Redirect::to('vendors/customer_feedback');
                        }
                        else
                        {
                            Session::flash('message', trans('messages.Invalid Customer Feedback details!'));
                            return Redirect::to('vendors/customer_feedback');
                        }
                        
                }

                public function show_customer_feedback($id)
                {

                    if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }
                    else {

                        if(!has_staff_permission('vendors/reviews/view'))
                        {
                            return view('errors.404');
                        }
                        $review_id = $id;

                        //  dd($review_id);
                        $language  = getAdminCurrentLang();
                        $query1    = 'outlet_infos.language_id = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.$language.' and outlets.id = outlet_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
                        $reviews   = DB::table('customer_feedback')
                                        ->select('customer_feedback.id as review_id','customer_feedback.customer_id as review_customer_id','customer_feedback.vendor_id as review_vendor_id','customer_feedback.comments','customer_feedback.title','customer_feedback.active_status','customer_feedback.created_date as review_posted_date','admin_customers.firstname as user_name','admin_customers.email as user_email','admin_customers.id as user_id'/*,'admin_customers.image as user_image'*/,'vendors.id as store_id','vendors.first_name as store_first_name','vendors.last_name as store_last_name','vendors.email as store_email','vendors.phone_number as store_phone_number','outlet_infos.outlet_name','outlets.id as outletid')
                                        ->leftJoin('admin_customers','admin_customers.id','=','customer_feedback.customer_id')
                                        ->leftJoin('outlets','outlets.id','=','customer_feedback.outlet_id')
                                        ->leftJoin('outlet_infos','outlets.id','=','outlet_infos.id')
                                        ->leftJoin('vendors','vendors.id','=','outlets.vendor_id')
                                        ->where('customer_feedback.id','=',$review_id)
                                        ->where('customer_feedback.vendor_id','=',Session::get('vendor_id'))
                                        ->whereRaw($query1)
                                        ->first();

                        //  dd($reviews);
                        if(!count($reviews))
                        {
                            Session::flash('message', trans('messages.Invalid Request'));
                            return Redirect::to('vendors/customer_feedback');
                        }
                        SEOMeta::setTitle('View Review - '.$this->site_name);
                        SEOMeta::setDescription('View Review - '.$this->site_name);
                        return view('vendors.customer_feedback.show')->with('review', $reviews);
                    }
                }

                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxCustomerFeedback()
                {

                        $language = getAdminCurrentLang();
                        $query    = 'outlet_infos.language_id = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.$language.' and outlets.id = outlet_infos.id) > 0 THEN '.$language.' ELSE 1 END)';                        
                        $reviews = DB::table('customer_feedback')
                                    ->select('customer_feedback.id as feedback_id','customer_feedback.customer_id as review_customer_id','customer_feedback.vendor_id as review_vendor_id',
                                        'customer_feedback.comments','customer_feedback.title',
                                        'customer_feedback.active_status',
                                        'customer_feedback.created_date as review_posted_date',
                                        'admin_customers.firstname as user_name',
                                        'admin_customers.email as user_email','admin_customers.id as user_id'
                                        /*,'admin_customers.image as user_image'*/,
                                        'vendors.id as store_id','vendors.first_name as store_first_name',
                                        'vendors.last_name as store_last_name','vendors.email as store_email',
                                        'vendors.phone_number as store_phone_number','outlet_infos.outlet_name',
                                        'outlets.id as outletid')
                                    ->leftJoin('admin_customers','admin_customers.id','=','customer_feedback.customer_id')
                                    ->leftJoin('outlets','outlets.id','=','customer_feedback.outlet_id')
                                    ->leftJoin('outlet_infos','outlets.id','=','outlet_infos.id')
                                    ->leftJoin('vendors','vendors.id','=','outlets.vendor_id')
                                    ->where('customer_feedback.vendor_id','=',Session::get('vendor_id'))
                                    ->where('customer_feedback.outlet_id','=',Session::get('property_id'))
                                    ->whereRaw($query)
                                    ->orderBy('customer_feedback.id', 'desc');

                            return Datatables::of($reviews)->addColumn('action', function ($reviews) {
                             
                                $review_status_opt = '';

                                if(has_staff_permission('vendors/reviews/view'))
                                {
                                
                                return '<div class="btn-group"><a href="'.URL::to("vendors/customer_feedback/view/".$reviews->feedback_id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.View").'</a>
                                        <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu xs pull-right" role="menu">
                                        <li><a href="'.URL::to("vendors/customer_feedback/delete/".$reviews->feedback_id).'" class="delete-'.$reviews->feedback_id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                        </ul>
                                    </div><script type="text/javascript">
                                    $( document ).ready(function() {
                                    $(".delete-'.$reviews->feedback_id.'").on("click", function(){
                                         return confirm("'.trans("messages.Are you sure want to delete?").'");
                                    });});</script>';

                                }
                            
                            })
                            ->addColumn('active_status', function ($reviews) {
                                if($reviews->active_status==0):
                                    $data = '<span  class="label label-danger">'.trans("messages.Inactive").'</span>';
                                elseif($reviews->active_status==1):
                                    $data = '<span  class="label label-success">'.trans("messages.Active").'</span>';
                                endif;
                                return $data;
                            })
                            ->addColumn('transaction_date', function ($reviews) {
                                    $data = '<span> '.date('d - M - Y h:i A' , strtotime($reviews->review_posted_date)).'</span>';
                                return $data;
                            })
                            ->make(true);
                }               


            }                    


