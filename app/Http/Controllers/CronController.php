<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;
use App\Model\vendors;
use App\Model\subscription_plan as Subscriptionplan;
use App\Model\subscription_plan_infos as Subscriptioninfo;
use App\Model\subscription_packages as Subscriptionpackages;
use App\Model\subscription_transaction as Subscriptiontransaction;
use App\Model\subscriptions as Subscriptionrenewal;

class CronController extends Controller
{
    const VENDOR_SUBSCRIPTION_EMAIL_TEMPLATE = 32;
    const VENDOR_EXPIRED_EMAIL_TEMPLATE = 33;

     public function __construct(){

     }

    /*
    *Cron function::Deactivating subscription plan ended vendors
    */
    public function inactiveVendors(){ 
      $today = date('Y-m-d');
      /*
      *Inactive all the packages that was completed by yesterday in subscriptions table
      */
      $Inactive_package = DB::table('subscriptions')
                          ->where('subscription_end_date','<',$today)
                          ->update(array('plan_status' => 0,'renewal_status' => 1));
      /*
      *Inactive all vendors whose plan was completed by yesterday in vendors table  
      */
      //Take all plan expired vendor_id for deactivating            
      $get_inactive_vendor =   DB::table('subscriptions')
                          ->where('subscription_end_date','<',$today)->select('vendor_id')->get();
      $get_all_vendor = iterator_to_array($get_inactive_vendor);

      $all_inactive_vendor = array();
      foreach($get_all_vendor as $key => $val){
        $all_inactive_vendor[]= $val->vendor_id;
      }

      //Inactive vendor plan status in vendors table
      $Inactive_vendor = Vendors::whereIn('user_id',$all_inactive_vendor)->update(array('vendor_plan_status' => 0));
    }

    /*
    *Cron function::Send Mail to Vendor whose plan is going to expire in 3 days.
    */
    public function expiringPlan(){
    $today = date('Y-m-d');
    $expire_date = date('Y-m-d', strtotime($today. ' + 2 days'));

    $Expiring_vendor = DB::table('subscriptions')
                      ->leftjoin('vendors','vendors.user_id','=','subscriptions.vendor_id')
                      ->leftjoin('vendors_infos','vendors_infos.id','=','vendors.user_id')
                      ->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscriptions.plan_id')
                      ->where('subscriptions.subscription_end_date','=',$expire_date)
                      ->select('vendors_infos.vendor_name','vendors.contact_email','subscriptions.subscription_start_date','subscriptions.subscription_end_date','subscription_plan_infos.subscription_plan_name')
                      ->get()->toArray();
      
          $template = DB::table('email_templates')
                      ->select('*')
                      ->where('template_id','=',self::VENDOR_SUBSCRIPTION_EMAIL_TEMPLATE)
                      ->get();
          $template = iterator_to_array($template);                      

           if(count($template))
           {
              $from = 'nextbrain.sender@gmail.com';
              $from_name=$template[0]->from;
              $subject = $template[0]->subject;
              $to = 'gopal.s@nextbrainitech.com';
              $subscribe_link = URL::to('vendors/subscription_packages');
              if(!$template[0]->template_id)
              {
                  $template = 'mail_template';
                  $from=getAppConfigEmail()->contact_email;
                  $subject = "Welcome to ".getAppConfig()->site_name;
                  $from_name="";
              }
                     
              if(count($Expiring_vendor) > 0){
                foreach($Expiring_vendor as $key => $val){
                $content = array("vendor_name" => $val->vendor_name,"subscription_end_date" => $val->subscription_end_date,'subscribe_link' => $subscribe_link);  
                $to = $val->contact_email;
                
                    $email=smtp($from,$from_name,$to,$subject,$content,$template); 
                  }
              }    
            }       
      }

    /*
    *Cron function::Send Mail to Vendor whose plan is already expired.
    */
     public function expiredPlan(){     
         $today = date('Y-m-d');    
         $Expired_vendor = DB::table('subscriptions')
                          ->leftjoin('vendors','vendors.user_id','=','subscriptions.vendor_id')
                          ->leftjoin('vendors_infos','vendors_infos.id','=','vendors.user_id')
                          ->leftjoin('subscription_plan_infos','subscription_plan_infos.subscription_plan_id','=','subscriptions.plan_id')
                          ->where('subscriptions.subscription_end_date','<',$today)
                          ->select('vendors_infos.vendor_name','vendors.contact_email','subscriptions.subscription_start_date','subscriptions.subscription_end_date','subscription_plan_infos.subscription_plan_name')
                          ->get()->toArray();    
        
         $template = DB::table('email_templates')
                      ->select('*')
                      ->where('template_id','=',self::VENDOR_EXPIRED_EMAIL_TEMPLATE)
                      ->get();
          $template = iterator_to_array($template);                      

           if(count($template))
           {
              $from = 'nextbrain.sender@gmail.com';
              $from_name=$template[0]->from;
              $subject = $template[0]->subject;
              
              $subscribe_link = URL::to('vendors/subscription_packages');
              if(!$template[0]->template_id)
              {
                  $template = 'mail_template';
                  $from=getAppConfigEmail()->contact_email;
                  $subject = "Welcome to ".getAppConfig()->site_name;
                  $from_name="";
              }
                     
              if(count($Expired_vendor) > 0){
                foreach($Expired_vendor as $key => $val){
                $content = array("vendor_name" => $val->vendor_name,"subscription_end_date" => $val->subscription_end_date,'subscribe_link' => $subscribe_link);  
                $to = $val->contact_email;
                
                    $email=smtp($from,$from_name,$to,$subject,$content,$template); 
                  }
              }    
            }                                      
    }

}
