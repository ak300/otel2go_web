<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\countries;
use App\Model\countries_infos;
use App\Model\users;
use App\Model\zones;
use App\Model\zones_infos;
use App\Model\cities;
use App\Model\cities_infos;
use App\Model\currencies_infos;
use App\Model\languages;
use App\Model\currencies;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;
use App\Model\weight_classes_infos;

class Localisation extends Controller
{

                /**
                 * Create a new controller instance.
                 *
                 * @return void
                 */
                public function __construct()
                {
                    $this->site_name = isset(getAppConfig()->site_name)?ucfirst(getAppConfig()->site_name):'';
                    $this->middleware('auth');
                    SEOMeta::setTitle($this->site_name);
                    SEOMeta::setDescription($this->site_name);
                    SEOMeta::addKeyword($this->site_name);
                    OpenGraph::setTitle($this->site_name);
                    OpenGraph::setDescription($this->site_name);
                    OpenGraph::setUrl($this->site_name);
                    Twitter::setTitle($this->site_name);
                    Twitter::setSite('@'.$this->site_name);
                    App::setLocale('en');
                }

                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function country()
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                                          
                    if(!has_permission('admin/localisation/country'))
                    {
                        return view('errors.404');
                    }
                    else{
                        $query = '"countries_infos"."language_id" = (case when (select count(*) as totalcount from countries_infos where countries_infos.language_id = '.getAdminCurrentLang().' and countries.id = countries_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $countries=DB::table('countries')
                         ->select('countries.*','countries_infos.*')
                        ->leftJoin('countries_infos','countries_infos.id','=','countries.id')
                        ->whereRaw($query)
                        ->orderBy('country_name', 'asc')
                        ->get();
                        return view('admin.country.list')->with('countries', $countries);
                    }
                    
                }
                
                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function zones()
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                      
                    if(!has_permission('admin/localisation/zones'))
                    {
                        return view('errors.404');
                    }else{
                        $query = '"zones_infos"."language_id" = (case when (select count(*) as totalcount from zones_infos where zones_infos.language_id = '.getAdminCurrentLang().' and zones.id = zones_infos.zone_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $zones=DB::table('zones')
                         ->select('zones.*','zones_infos.*')
                        ->leftJoin('zones_infos','zones_infos.zone_id','=','zones.id')
                        ->whereRaw($query)
                        ->orderBy('zone_name', 'asc')  
                        ->get(); 
                        return view('admin.zones.list')->with('zones', $zones);
                    }
                    
                }
                
                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function city()
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/localisation/city'))
                    {
                        return view('errors.404');
                    } else{
                        $query = '"cities_infos"."language_id" = (case when (select count(*) as totalcount from cities_infos where cities_infos.language_id = '.getAdminCurrentLang().' and cities.id = cities_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $cities=DB::table('cities')
                         ->select(DB::raw('cities.* ,cities.id as cid'),'cities_infos.*')
                        ->leftJoin('cities_infos','cities_infos.id','=','cities.id')
                        ->whereRaw($query)
                        ->orderBy('city_name', 'asc')
                        ->get();
                        return view('admin.cities.list')->with('cities', $cities);
                    }
                    
                }
                
                public function country_create()
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                                          
                    if(!has_permission('admin/country/create'))
                    {
                        return view('errors.404');
                    }else{
                        return view('admin.country.create');
                    }
                }
                
                public function zones_create()
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                                       
                    if(!has_permission('admin/zones/create'))
                    {
                        return view('errors.404');
                    } else{
                        return view('admin.zones.create');
                    }
                }
                
                public function city_create()
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                                       
                    if(!has_permission('admin/city/create'))
                    {
                        return view('errors.404');
                    } else{
                        return view('admin.cities.create');
                    }
                }

                public function country_edit($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                                        
                    if(!has_permission('admin/country/edit/{id}'))
                    {
                        return view('errors.404');
                    } else{
                        $info = new Countries_infos;
                        $countries = Countries::find($id);
                        return view('admin.country.edit')->with('data', $countries)->with('infomodel', $info);
                    }
                }
                
                public function zone_edit($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                                        
                    if(!has_permission('admin/zones/edit/{id}'))
                    {
                        return view('errors.404');
                    } else{
                        $info = new Zones_infos;
                        $zones = Zones::find($id);
                        return view('admin.zones.edit')->with('data', $zones)->with('infomodel', $info);
                    }
                }
                
                public function city_edit($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/city/edit/{id}'))
                    {
                        return view('errors.404');
                    } else{
                        $info = new Cities_infos;
                        $cities = Cities::find($id);
                        return view('admin.cities.edit')->with('data', $cities)->with('infomodel', $info);
                    }
                }
                
                public function country_store(Request $data)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('createcountry'))
                    {
                        return view('errors.404');
                    }
                    $fields['iso_code'] = Input::get('iso_code');
                    $fields['alpha_code'] = Input::get('alpha_code');
                    $fields['country_isd_code'] = Input::get('country_isd_code');
                    $rules = array(
                        //'iso_code'         => 'required|integer|min:004|max:894',
                        //'alpha_code'       => 'required|alpha|max:2',
                        //'country_isd_code' => 'required|integer|min:1|max:998',
                    );
                    $country_name = Input::get('country_name');
                    foreach ($country_name  as $key => $value) {
                        $fields['country_name'.$key] = $value;
                        $rules['country_name'.'1'] = 'required|unique:countries_infos,country_name';
                        
                    }
                    $validator = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{

                            $Countries = new Countries;
                            $Countries->url_index =  $_POST['country_name'][1] ? str_slug($_POST['country_name'][1]): str_slug($_POST['country_name'][1]);
                            $Countries->iso_code = $_POST['iso_code'];
                            $Countries->alpha_code = $_POST['alpha_code'];
                            $Countries->country_isd_code = $_POST['country_isd_code'];
                            $Countries->created_at = date("Y-m-d H:i:s");          
                            $Countries->updated_at = date("Y-m-d H:i:s");
                            $Countries->country_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            //$this->country_save_before($Countries,$_POST);
                            $Countries->save();
                            $this->country_save_after($Countries,$_POST);
                            Session::flash('message', trans('messages.Country has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('admin/localisation/country');
                    }
                }
                
                
                public function zone_store(Request $data)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('createzone'))
                    {
                        return view('errors.404');
                    }
                    //$fields['country'] = Input::get('country');
                    $fields['city'] = Input::get('city');
                    $rules = array(
                        //'country' => 'required',
                        'city' => 'required',
                    );
                    $zone_name = Input::get('zone_name');
                    foreach ($zone_name  as $key => $value) {
                        $fields['zone_name'.$key] = $value;
                        $rules['zone_name'.'1'] = 'required|unique:zones_infos,zone_name';
                        
                    }
                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{
                            $Zones = new Zones;
                            $Zones->url_index =  $_POST['zone_name'][1] ? str_slug($_POST['zone_name'][1]): str_slug($_POST['zone_name'][1]);
                            $Zones->city_id = $_POST['city'];
                            $Zones->country_id = 1;      /*  1-> Malaysia    $_POST['country'];   */
                            $Zones->created_at = date("Y-m-d H:i:s");
                            $Zones->zones_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            $Zones->save();
                            $this->zone_save_after($Zones,$_POST);
                            Session::flash('message', trans('messages.Zone has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('admin/localisation/zones');
                    }
                }
                
                public function city_store(Request $data)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('createcity'))
                    {
                        return view('errors.404');
                    }
                    $fields['zone_code'] = Input::get('zone_code');
                    $fields['city_image'] = Input::file('city_image');

                    //  $fields['country'] = Input::get('country');
                    $rules = array(
                        'zone_code' => 'required|numeric',
                        'city_image' => 'required|mimes:png,jpg,jpeg,bmp|max:2024'
                        //  'country' => 'required',
                    );
                    $city_name = Input::get('city_name');
                    foreach ($city_name  as $key => $value) {
                        $fields['city_name'.$key] = $value;
                        $rules['city_name'.'1'] = 'required|regex:/(^[A-Za-z]+$)+/|unique:cities_infos,city_name';
                        
                    }
                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{

                            $Cities = new Cities;
                            $Cities->url_index =  $_POST['city_name'][1] ? str_slug($_POST['city_name'][1]): str_slug($_POST['city_name'][1]);
                            $Cities->zone_code = $_POST['zone_code'];
                            $Cities->country_id = 1;      /*  1-> Malaysia    $_POST['country'];   */  
                            $Cities->created_date = date("Y-m-d H:i:s");          
                            $Cities->modified_date = date("Y-m-d H:i:s");
                            $Cities->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            //$this->country_save_before($Countries,$_POST);
                            $Cities->save();

                            $imageName = strtolower($Cities->id . '.' . $data->file('city_image')->getClientOriginalExtension());
                            $data->file('city_image')->move(
                                base_path() . '/public/assets/admin/base/images/city/', $imageName
                            );
                            $destinationPath2 = url('/assets/admin/base/images/city/'.$imageName.'');
                            $size=getImageResize('VENDOR');
                            Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/list/'.$imageName)->destroy();
                            
                            Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/detail/'.$imageName)->destroy();
                            
                            Image::make( $destinationPath2 )->fit($size['THUMB_WIDTH'], $size['THUMB_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/'.$imageName)->destroy();
                            
                            Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/detail/'.$imageName)->destroy();
                            $Cities->city_image=$imageName;
                            $Cities->save();


                            $this->city_save_after($Cities,$_POST);
                            Session::flash('message', trans('messages.City has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('admin/localisation/city');
                    }
                }
                
                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function country_update(Request $data, $id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/updatecountry/{id}'))
                    {
                        return view('errors.404');
                    }
                    $fields['iso_code']         = Input::get('iso_code');
                    $fields['alpha_code']       = Input::get('alpha_code');
                    $fields['country_isd_code'] = Input::get('country_isd_code');
                    $rules = array(
                       // 'iso_code'         => 'required|integer|min:004|max:894',
                        //'alpha_code'       => 'required|alpha|max:2',
                        //'country_isd_code' => 'required|integer|min:1|max:998',
                    );
                    $country_name = Input::get('country_name');
                    foreach ($country_name  as $key => $value) {
                        $fields['country_name'.$key] = $value;
                        $rules['country_name'.'1'] = 'required|unique:countries_infos,country_name,'.$id;
                        
                    }
                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{
                            $Countries = Countries::find($id); 
                            $Countries->url_index =  $_POST['country_name'][1] ? str_slug($_POST['country_name'][1]): str_slug($_POST['country_name'][1]);
                            $Countries->iso_code = $_POST['iso_code'];
                            $Countries->alpha_code = $_POST['alpha_code'];
                            $Countries->country_isd_code = $_POST['country_isd_code'];          
                            $Countries->updated_at = date("Y-m-d H:i:s");
                            $Countries->country_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            //$this->country_save_before($Countries,$_POST);
                            $Countries->save();
                            $this->country_save_after($Countries,$_POST);
                            Session::flash('message', trans('messages.Country has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('admin/localisation/country');
                    }
                }
                
                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function zone_update(Request $data, $id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/updatezone/{id}'))
                    {
                        return view('errors.404');
                    }
                    //$fields['country'] = Input::get('country');
                    $fields['city'] = Input::get('city');
                    //print_r($fields['city']);
                    //exit;
                    $rules = array(
                        //'country' => 'required',
                        'city' => 'required',
                    );
                    $zone_name = Input::get('zone_name');
                    foreach ($zone_name  as $key => $value) {
                        $fields['zone_name'.$key] = $value;
                        $rules['zone_name'.'1'] = 'required|unique:zones_infos,zone_name,'.$id.',zone_id';
                        
                    }
                    
                    $validator = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{
                            $Zones = Zones::find($id); 
                            $Zones->url_index =  $_POST['zone_name'][1] ? str_slug($_POST['zone_name'][1]): str_slug($_POST['zone_name'][1]);
                            $Zones->country_id = 1;      /*  1-> Malaysia    $_POST['country'];   */ 
                            $Zones->city_id = $_POST['city'];
                            $Zones->updated_at = date("Y-m-d H:i:s");
                            $Zones->zones_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            $Zones->save();
                            $this->zone_save_after($Zones,$_POST);
                            Session::flash('message', trans('messages.Zones has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('admin/localisation/zones');
                    }
                }
                
                
                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function city_update(Request $data, $id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/updatecity/{id}'))
                    {
                        return view('errors.404');
                    }
                    $fields['zone_code'] = Input::get('zone_code');
                    $fields['city_image'] = Input::file('city_image');
                    //$fields['country'] = Input::get('country');
                    $rules = array(
                        'zone_code' => 'required|numeric',
                        // 'city_image' => 'required|mimes:png,jpg,jpeg,bmp|max:2024',
                        // 'country' => 'required',
                    );
                    $city_name = Input::get('city_name');
                    foreach ($city_name  as $key => $value) {
                        $fields['city_name'.$key] = $value;
                        $rules['city_name'.'1'] = 'required|regex:/(^[A-Za-z]+$)+/|unique:cities_infos,city_name,'.$id.',id';
                        }
                    
                    $validator = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        
                        try{
                            
                            $Cities = Cities::find($id); 
                            $Cities->url_index =  $_POST['city_name'][1] ? str_slug($_POST['city_name'][1]): str_slug($_POST['city_name'][1]);
                            $Cities->zone_code = $_POST['zone_code'];
                            $Cities->country_id = 1;      /*  1-> Malaysia    $_POST['country'];   */         
                            $Cities->modified_date = date("Y-m-d H:i:s");
                            $Cities->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            $Cities->save();

                    if(isset($_FILES['city_image']['name']) && $_FILES['city_image']['name']!=''){
                                $imageName = strtolower($id . '.' . $data->file('city_image')->getClientOriginalExtension());
                                $data->file('city_image')->move(
                                    base_path() . '/public/assets/admin/base/images/city/', $imageName
                                );
                                $destinationPath2 = url('/assets/admin/base/images/city/'.$imageName.'');
                                $size=getImageResize('VENDOR');
                                Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/list/'.$imageName)->destroy();
                                Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/detail/'.$imageName)->destroy();
                                Image::make( $destinationPath2 )->fit($size['THUMB_WIDTH'], $size['THUMB_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/'.$imageName)->destroy();
                                Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/detail/'.$imageName)->destroy();

                                $Cities->city_image = $imageName;

                                $Cities->save();
                            }

                            $this->city_save_after($Cities,$_POST);
                            Session::flash('message', trans('messages.City has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('admin/localisation/city');
                    }
                }
                /**
                 * add,edit datas  saved in main table 
                 * after inserted in sub tabel.
                 *
                 * @param  int  $id
                 * @return Response
                 */
               public static function country_save_after($object,$post)
               {
                    $country = $object;
                    $post = $post;
                    if(isset($post['country_name'])){
                        $country_name = $post['country_name'];
                        try{
                            $data = Countries_infos::find($object->id);
                            if(count($data)>0){
                                $data->delete();
                            }
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
                                if(isset($country_name[$lang->id]) && $country_name[$lang->id]!=""){
                                    $infomodel = new Countries_infos;
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->id = $object->id; 
                                    $infomodel->country_name = $country_name[$lang->id];
                                    $infomodel->save();
                                }
                            }
                            }catch(Exception $e) {
                                
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                    }
               }
               
                /**
                 * add,edit datas  saved in main table 
                 * after inserted in sub tabel.
                 *
                 * @param  int  $id
                 * @return Response
                 */
               public static function zone_save_after($object,$post)
               {
                    $zone = $object;
                    $post = $post;
                    if(isset($post['zone_name'])){
                        $zone_name = $post['zone_name'];
                        try{
                            $affected = DB::table('zones_infos')->where('zone_id', '=', $object->id)->delete();
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
                                if(isset($zone_name[$lang->id]) && $zone_name[$lang->id]!=""){
                                    $infomodel = new Zones_infos;
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->zone_id = $object->id; 
                                    $infomodel->zone_name = $zone_name[$lang->id];
                                    $infomodel->save();
                                }
                            }
                        }catch(Exception $e) {
                            Log::Instance()->add(Log::ERROR, $e);
                        }
                    }
               }
               
               
               
                /**
                 * add,edit datas  saved in main table 
                 * after inserted in sub tabel.
                 *
                 * @param  int  $id
                 * @return Response
                 */
               public static function city_save_after($object,$post)
               {
                    $city = $object;
                    $post = $post;
                    if(isset($post['city_name'])){
                        $city_name = $post['city_name'];
                        try{                
                            $affected = DB::table('cities_infos')->where('id', '=', $object->id)->delete();
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
                                if(isset($city_name[$lang->id]) && $city_name[$lang->id]!=""){
                                    $infomodel = new Cities_infos;
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->id = $object->id; 
                                    $infomodel->city_name = $city_name[$lang->id];
                                    $infomodel->save();
                                }
                            }
                            }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                    }
               }
               
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function country_destroy($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/country/delete/{id}'))
                    {
                        return view('errors.404');
                    }
                    $data = Countries::find($id);
                    $data->delete();
                    Session::flash('message', trans('messages.Country has been deleted successfully!'));
                    return Redirect::to('admin/localisation/country');
                }
                
                
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function zone_destroy($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/zones/delete/{id}'))
                    {
                        return view('errors.404');
                    }
                    $data = Zones::find($id);
                    $data->delete();
                    Session::flash('message', trans('messages.Zone has been deleted successfully!'));
                    return Redirect::to('admin/localisation/zones');
                }
                
                
                
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function city_destroy($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/city/delete/{id}'))
                    {
                        return view('errors.404');
                    }
                    $data = Cities::find($id);
                    $data->delete();
                    Session::flash('message', trans('messages.City has been deleted successfully!'));
                    return Redirect::to('admin/localisation/city');
                }

                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxCountry()
                {
                    if(Auth::guest())
                    {
                        return 404;
                    }                     
                    $query = '"countries_infos"."language_id" = (case when (select count(*) as totalcount from countries_infos where countries_infos.language_id = '.getAdminCurrentLang().' and countries.id = countries_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                    $country = Countries::Leftjoin('countries_infos','countries_infos.id','=','countries.id')
                    ->select('countries.*','countries_infos.*')
                    ->whereRaw($query)
                    ->orderBy('countries.id', 'desc')
                    ->get();
                    return Datatables::of($country)->addColumn('action', function ($country) {
                        if(has_permission('admin/zones/edit')){
                            return '<div class="btn-group"><a href="'.URL::to("admin/country/edit/".$country->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("admin/country/delete/".$country->id).'" class="delete-'.$country->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                    </ul>
                                </div><script type="text/javascript">
                                $( document ).ready(function() {
                                $(".delete-'.$country->id.'").on("click", function(){
                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });});</script>';
                            }
                        })
                        ->addColumn('country_isd_code', function ($country) {
                                $data = '-';
                                if($country->country_isd_code != ''):
                                $data = $country->country_isd_code;
                                endif;
                                return $data;
                        })
                        ->addColumn('alpha_code', function ($country) {
                                $data = '-';
                                if($country->alpha_code != ''):
                                $data = $country->alpha_code;
                                endif;
                                return $data;
                        })
                        ->addColumn('iso_code', function ($country) {
                                $data = '-';
                                if($country->iso_code != ''):
                                $data = $country->iso_code;
                                endif;
                                return $data;
                        })
                        ->make(true);
                }
                
                
                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxZones()
                {
                    if(Auth::guest())
                    {
                        return 404;
                    }                                         
                    $country=getCountryLists();
                    $query = '"zones_infos"."language_id" = (case when (select count(*) as totalcount from zones_infos where zones_infos.language_id = '.getAdminCurrentLang().' and zones.id = zones_infos.zone_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                    $zones = Zones::Leftjoin('zones_infos','zones_infos.zone_id','=','zones.id')
                    ->join('countries','countries.id','=','zones.country_id')
                    ->join('countries_infos','countries_infos.id','=','countries.id')
                    ->join('cities','cities.id','=','zones.city_id')
                    ->join('cities_infos','cities_infos.id','=','cities.id')
                    ->select(DB::raw('zones.* ,zones.id as zid'),'zones_infos.*',"countries_infos.*","cities_infos.*")
                    ->where("countries_infos.language_id","=",getAdminCurrentLang())
                    ->where("cities_infos.language_id","=",getAdminCurrentLang())
                    ->whereRaw($query)
                    ->get();
                    return Datatables::of($zones)->addColumn('action', function ($zones) {
                        if(has_permission('admin/zones/edit')){
                            return '<div class="btn-group"><a href="'.URL::to("admin/zones/edit/".$zones->zid).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("admin/zones/delete/".$zones->zid).'" class="delete-'.$zones->zid.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                    </ul>
                                </div><script type="text/javascript">
                                $( document ).ready(function() {
                                $(".delete-'.$zones->zid.'").on("click", function(){
                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });});</script>';
                            }
                        })
                        ->addColumn('zones_status', function ($zones) {
                            if($zones->zones_status==0):
                                $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                            elseif($zones->zones_status==1):
                                $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                            endif;
                            return $data;
                        })
                        ->make(true);
                }
                
                
                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxCities()
                {
                    if(Auth::guest())
                    {
                        return 404;
                    }                                         
                    $country=getCountryLists();
                    $query = '"cities_infos"."language_id" = (case when (select count(*) as totalcount from cities_infos where cities_infos.language_id = '.getAdminCurrentLang().' and cities.id = cities_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                    $cities = Cities::Leftjoin('cities_infos','cities_infos.id','=','cities.id')
                    ->join('countries','countries.id','=','cities.country_id')
                    ->join('countries_infos','countries_infos.id','=','countries.id')
                    ->select(DB::raw('cities.* ,cities.id as cid'),'cities_infos.*',"countries_infos.*")
                    ->where("countries_infos.language_id","=",getAdminCurrentLang())
                    ->whereRaw($query)
                    ->orderBy('cities.id', 'desc')
                    ->get();
                    return Datatables::of($cities)->addColumn('action', function ($cities) {
                         if(has_permission('admin/city/edit')){
                            return '<div class="btn-group"><a href="'.URL::to("admin/city/edit/".$cities->cid).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("admin/city/delete/".$cities->cid).'" class="delete-'.$cities->cid.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                    </ul>
                                </div><script type="text/javascript">
                                $( document ).ready(function() {
                                $(".delete-'.$cities->cid.'").on("click", function(){
                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });});</script>';
                            }    
                        })
                        ->addColumn('default_status', function ($cities) {
                            if($cities->default_status==0):
                                $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                            elseif($cities->default_status==1):
                                $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                            endif;
                            return $data;
                        })
                        ->addColumn('city_name', function ($cities) {
                                $data = ucfirst($cities->city_name);
                            return $data;
                        })
                        ->make(true);
                }
             
                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function language()
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/localisation/language'))
                    {
                        return view('errors.404');
                    } else{
                        $languages=DB::table('languages')
                         ->select('languages.*')
                        ->orderBy('name', 'asc')
                        ->paginate(10);   
                        return view('admin.language.list')->with('languages', $languages);
                    }
                }
                
                /**
                 * Display a listing of the weight classes.
                 *
                 * @return Response
                 */
                public function anyAjaxLanguage()
                {
                    if(Auth::guest())
                    {
                        return 404;
                    }                     
                    $languages = DB::table('languages')->select('*')->orderBy('id', 'desc');
                    return Datatables::of($languages)->addColumn('action', function ($languages) {
                        if(has_permission('admin/language/edit')){
                            return '<div class="btn-group"><a href="'.URL::to("admin/language/edit/".$languages->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("admin/language/delete/".$languages->id).'" class="delete-'.$languages->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                    </ul>
                                </div><script type="text/javascript">
                                $( document ).ready(function() {
                                $(".delete-'.$languages->id.'").on("click", function(){
                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });});</script>';
                            }
                        })
                        ->addColumn('status', function ($languages) {
                            if($languages->status==0):
                                $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                            elseif($languages->status==1):
                                $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                            endif;
                            return $data;
                        })
                        ->make(true);
                }
                
                /**
                 * Show the form for creating a new blog.
                 *
                 * @return Response
                 */
                public function language_create()
                { 
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/language/create'))
                    {
                        return view('errors.404');
                    } else{    
                        return view('admin.language.create');
                    }
                }
                
                /**
                 * Store a newly created blog in storage.
                 *
                 * @return Response
                 */
                public function language_store(Request $data)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('createlanguage'))
                    {
                        return view('errors.404');
                    }
                    $data1=Input::all();

                    $validation = Validator::make($data->all(), array(
                        'name' => 'required|alpha|unique:languages,name|max:25',
                        'language_code' => 'required|alpha|unique:languages,language_code|max:2',
                        'short_date_format' => 'required',
                        'full_date_format' => 'required',
                    ));
                    // process the validation
                    if ($validation->fails()) {
                            //return redirect('create')->withInput($data1)->withErrors($validation);
                            return Redirect::back()->withErrors($validation)->withInput();
                    } else {
                        // store
                        $languages = new Languages;
                        $languages->name      = $_POST['name'];
                        $languages->language_code    = $_POST['language_code'];
                        $languages->date_format_short    = $_POST['short_date_format'];
                        $languages->date_format_full    = $_POST['full_date_format'];
                        $languages->status    = isset($_POST['status']);
                        $languages->is_rtl    = isset($_POST['rtl']);
                        $languages->created_at = date("Y-m-d H:i:s");
                        $languages->save();
                        // redirect
                        Session::flash('message', trans('messages.Language has been created successfully'));
                        //return Redirect::to('blog')->with('updatemsg', 'Blog has been successfully create');
                        return Redirect::to('admin/localisation/language');
                    }
                }
                
                /**
                 * Show the form for editing the specified blog.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function language_edit($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/language/edit/{id}'))
                    {
                        return view('errors.404');
                    } else{
                    // get the blog
                     $languages =  Languages::find($id);
                     if(!count($languages)){
                         Session::flash('message', 'Invalid Language'); 
                         Session::flash('alert-class', 'alert-danger'); 
                         return Redirect::to('admin/localisation/language');    
                     }
                    return view('admin.language.edit')->with('data', $languages);
                    }
                }
                
                /**
                 * Store a newly created blog in storage.
                 *
                 * @return Response
                 */
                public function language_update(Request $data, $id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/language/update/{id}'))
                    {
                        return view('errors.404');
                    }
                    $data1=Input::all();

                    $validation = Validator::make($data->all(), array(
                        'name' => 'required|alpha|max:25|unique:languages,name,'.$id,
                        'language_code' => 'required|alpha|max:2|unique:languages,language_code,'.$id,
                        'short_date_format' => 'required|',
                        'full_date_format' => 'required|',
                    ));
                    // process the validation
                    if ($validation->fails()) {
                            //return redirect('create')->withInput($data1)->withErrors($validation);
                            return Redirect::back()->withErrors($validation)->withInput();
                    } else {
                        // store
                        $languages = Languages::find($id);
                        $languages->name      = $_POST['name'];
                        $languages->language_code    = $_POST['language_code'];
                        $languages->date_format_short    = $_POST['short_date_format'];
                        $languages->date_format_full    = $_POST['full_date_format'];
                        $languages->status    = isset($_POST['status']);
                        $languages->is_rtl    = isset($_POST['rtl']);
                        $languages->updated_at = date("Y-m-d H:i:s");
                        $languages->save();
                        // redirect
                        Session::flash('message', trans('messages.Language has been updated successfully'));
                        return Redirect::to('admin/localisation/language');
                    }
                }
                
                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function currency()
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/localisation/currency'))
                    {
                        return view('errors.404');
                    }
                    else
                    {
                        return view('admin.currencies.list');
                    }
                }
                
                /**
                 * Display a listing of the weight classes.
                 *
                 * @return Response
                 */
                public function anyAjaxCurrency()
                { 
                    if(Auth::guest())
                    {
                        return 404;
                    }                  
            		$language_id = getAdminCurrentLang();
            	    $query = '"currencies_infos"."language_id" = (case when (select count(*) as totalcount from currencies_infos where currencies_infos.language_id = '.$language_id.' and currencies.id = currencies_infos.currency_id) > 0 THEN '.$language_id.' ELSE 1 END)';
                    $currencies=Currencies::Leftjoin('currencies_infos','currencies_infos.currency_id','=','currencies.id')
                                     ->select('currencies.id','currencies_infos.currency_name', 'currencies.currency_code', 'currencies_infos.currency_symbol', 'currencies.exchange_rate', 'currencies.numeric_iso_code', 'currencies.created_date', 'currencies.default_status') 
                                     ->whereRaw($query)
                                     ->orderBy('id', 'desc')
                                     ->get();
               
                    return Datatables::of($currencies)->addColumn('action', function ($currencies) {
                        if(has_permission('admin/currency/edit')){
                        return '<div class="btn-group"><a href="'.URL::to("admin/currency/edit/".$currencies->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                            <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu xs pull-right" role="menu">
                                <li><a href="'.URL::to("admin/currency/delete/".$currencies->id).'" class="delete-'.$currencies->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                            </ul>
                        </div>
                        <script type="text/javascript">
                            $( document ).ready(function() {
                                $(".delete-'.$currencies->id.'").on("click", function(){
                                    return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });
                            });
                        </script>';
                        }
                    })
                    ->addColumn('default_status', function ($currencies) {
                        if($currencies->default_status==0):
                            $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                        elseif($currencies->default_status==1):
                            $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                        endif;
                        return $data;
                    })
                    ->make(true);
                }

                /**
                 * Show the form for creating a new blog.
                 *
                 * @return Response
                 */
                public function currency_create()
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/currency/create'))
                    {
                        return view('errors.404');
                    }
                    else {
                        return view('admin.currencies.create');
                    }
                }

                /**
                 * Store a newly created currency in storage.
                 *
                 * @return Response
                 */
                public function currency_store(Request $data)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('createcurrency'))
                    {
                        return view('errors.404');
                    }
                    $data_all   = $data->all();
                    $validation = Validator::make($data_all, array(
                        'currency_name'    => 'required|unique:currencies,currency_name|max:15',
                        'currency_code'    => 'required|alpha|unique:currencies,currency_code|max:3',
                        'numeric_iso_code' => 'required|integer',
                       // 'currency_symbol'  => 'required',
                        'exchange_rate'    => 'required|between:0,99.99',
                        'decimal_values'   => 'required|between:0,99.99',
                    ));
                    $currency_name = Input::get('currency_name');
                    foreach ($currency_name  as $key => $value) {
                        $fields['currency_name'.$key] = $value;
                        $rules['currency_name'.'1'] = 'required|unique:currencies_infos,currency_name';
                    }
                    $currency_symbol = Input::get('currency_symbol');
                    foreach ($currency_symbol  as $key => $value) {
                        $fields['currency_symbol'.$key] = $value;
                        $rules['currency_symbol'.'1'] = 'required';
                    }
                    // process the validation
                    if ($validation->fails())
                    {
                        return Redirect::back()->withErrors($validation)->withInput();
                    }
                    else {
                        // store
                        $Currencies = new Currencies;
                        //~ $Currencies->currency_symbol_left    = $_POST['currency_symbol_left'];
                        //~ $Currencies->currency_symbol_right    = $_POST['currency_symbol_right'];
                       // $Currencies->currency_name    = $data_all['currency_name'];
                        $Currencies->currency_code    = $data_all['currency_code'];
                        $Currencies->numeric_iso_code = $data_all['numeric_iso_code'];
                       // $Currencies->currency_symbol  = $data_all['currency_symbol'];
                        $Currencies->exchange_rate    = $data_all['exchange_rate'];
                        $Currencies->decimal_values   = $data_all['decimal_values'];
                        $Currencies->default_status   = isset($data_all['status'])?$data_all['status']:0;
                        $Currencies->created_date     = date("Y-m-d H:i:s");
                        $Currencies->save();
                        $this->currencies_save_after($Currencies,$_POST);
                        // redirect
                        Session::flash('message', trans('messages.Currency has been created successfully'));
                        return Redirect::to('admin/localisation/currency');
                    }
                }

                /**
                 * Currecy name added in currencies infos table the specified currency in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                 public static function currencies_save_after($object,$post)
                {   
                    $Currencies = $object;
                    $post = $post;
                    if(isset($post['currency_name'])){
                        $currency_name = $post['currency_name'];
                         $currency_symbol = $post['currency_symbol'];
                        try{
                            $affected = DB::table('currencies_infos')->where('currency_id', '=', $object->id)->delete();
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
            					 if(isset($currency_name[$lang->id]) && $currency_name[$lang->id]!=""){
                                    $infomodel = new Currencies_infos;
                                    $infomodel->currency_name = $currency_name[$lang->id]; 
                                    $infomodel->currency_symbol = $currency_symbol[$lang->id]; 
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->currency_id = $object->id; 
                                    $infomodel->save();
            					}
                                  
                               
                            }
                            }catch(Exception $e) {
                                
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                    }
                }

                /**
                 * Show the form for editing the specified currency.
                 *
                 * @param  int  $id
                 * @return Response
                 */                
                public function currency_edit($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/currency/edit/{id}'))
                    {
                        return view('errors.404');
                    }
                    else{
                        // get the currency
                        $currencies =  Currencies::find($id);
                        if(!count($currencies))
                        {
                            Session::flash('message', 'Invalid currncy'); 
                            Session::flash('alert-class', 'alert-danger'); 
                            return Redirect::to('admin/localisation/currency');
                        }
                         $info = new currencies_infos;
                        return view('admin.currencies.edit')->with('data', $currencies)->with('infomodel', $info);;
                    }
                }

                /**
                 * Store a newly created currency in storage.
                 *
                 * @return Response
                 */
                public function currency_update(Request $data, $id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/currency/update/{id}'))
                    {
                        return view('errors.404');
                    }
                    $data_all   = $data->all();
                    $validation = Validator::make($data_all, array(
                        //'currency_name'    => 'required|unique:currencies,currency_name,'.$id,
                        'currency_code'    => 'required|alpha|unique:currencies,currency_code,'.$id,
                        'numeric_iso_code' => 'required|integer',
                       // 'currency_symbol'  => 'required',
                        'exchange_rate'    => 'required|between:0,99.99',
                        'decimal_values'   => 'required|between:0,99.99',
                    ));
                     $currency_name = Input::get('currency_name');
                    foreach ($currency_name  as $key => $value) {
                        $fields['currency_name'.$key] = $value;
                        $rules['currency_name'.'1'] = 'required|unique:currencies_infos,currency_name,'.$id.',currency_id';
                    }
                    $currency_symbol = Input::get('currency_symbol');
                    foreach ($currency_symbol  as $key => $value) {
                        $fields['currency_symbol'.$key] = $value;
                        $rules['currency_symbol'.'1'] = 'required';
                    }
                    // process the validation
                    if ($validation->fails())
                    {
                        return Redirect::back()->withErrors($validation)->withInput();
                    }
                    else {
                        // store
                        $Currencies = Currencies::find($id);
                        //~ $Currencies->currency_symbol_left  = $_POST['currency_symbol_left'];
                        //~ $Currencies->currency_symbol_right = $_POST['currency_symbol_right'];
                        //$Currencies->currency_name    = $data_all['currency_name'];
                        $Currencies->currency_code    = $data_all['currency_code'];
                       // $Currencies->currency_symbol = $data_all['currency_symbol'];
                        $Currencies->numeric_iso_code = $data_all['numeric_iso_code'];
                        $Currencies->exchange_rate    = $data_all['exchange_rate'];
                        $Currencies->decimal_values   = $data_all['decimal_values'];
                        $Currencies->default_status   = isset($data_all['status'])?$data_all['status']:0;
                        $Currencies->save();
                         $this->currencies_save_after($Currencies,$_POST);
                        // redirect
                        Session::flash('message', trans('messages.Currency has been updated successfully'));
                        return Redirect::to('admin/localisation/currency');
                    }
                }
              
                /**
                 * Delete the specified currency in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function currency_destroy($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/currency/delete/{id}'))
                    {
                        return view('errors.404');
                    }
                    $data = Currencies::find($id);
                    $data->delete();
                    Session::flash('message', trans('messages.Currency has been deleted successfully!'));
                    return Redirect::to('admin/localisation/currency');
                }
                
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function language_destroy($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                    
                    if(!has_permission('admin/language/delete/{id}'))
                    {
                        return view('errors.404');
                    }
                    $data = Languages::find($id);
                    $data->delete();
                    Session::flash('message', trans('messages.Language has been deleted successfully!'));
                    return Redirect::to('admin/localisation/language');
                }
}
