<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\rooms;
use App\Model\rooms_infos;
use App\Model\room_type;
use App\Model\room_type_infos;
use App\Model\extra_amenities;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;


class Managers_Room extends Controller
{
                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function index()
                {

                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } 
                    else{
                        $query = '"room_type_infos"."language_id" = (case when (select count(*) as totalcount from room_type_infos where room_type_infos.language_id = '.getAdminCurrentLang().' and room_type.id = room_type_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $Room_type=DB::table('room_type')
                         ->select(DB::raw('room_type.* ,room_type.id as cid'),'room_type_infos.*')
                        ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                        ->whereRaw($query)
                        ->orderBy('room_type', 'asc')
                        ->get();
                        return view('managers.room_type.list')->with('room_type', $Room_type);
                    }
                    
                }
                
                public function roomtype_create()
                {
                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    }  else{
                        return view('managers.room_type.create');
                    }
                }


                public function roomtype_edit($id)
                {

                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    }  else{
                        $info = new Room_type_infos;
                        $Room_type = Room_type::find($id);
                        return view('managers.room_type.edit')->with('data', $Room_type)->with('infomodel', $info);
                    }
                }
                
                
                public function roomtype_store(Request $data)
                {
 // print_r($data->all());exit;
                    $fields['room_type']      = Input::get('room_type');
                    $fields['normal_price']   = Input::get('normal_price');
                    $fields['discount_price'] = Input::get('discount_price');
                    $fields['adult_count']    = Input::get('adult_count');
                    $fields['child_count']    = Input::get('child_count');
                    $fields['description']    = Input::get('description');                    
                    $fields['image']          = Input::file('image');


                    $rules = array(
                        
                        'normal_price'   => 'required|numeric',
                        'discount_price' => 'required|numeric',
                        'adult_count'    => 'required|numeric',
                        'child_count'    => 'required|numeric',
                        //'description'    => 'required',
                        'image'          => 'required|mimes:png,jpg,jpeg,bmp|max:2024',
                    );

                    /*
                            if(isset($post_data['amenities_name']) && $post_data['amenities_name']!=''){

                                for($i=0; $i<count($post_data['amenities_name']); $i++)
                                {
                                    $rules['amenities_name.'.$i] = 'required';
                                }
                            }else {
                                //  $rules['amenities_name'] = 'required';
                            }

                            if(isset($post_data['amenities_image']) && $post_data['amenities_image']!=''){

                                for($i=0; $i<count($post_data['amenities_image']); $i++)
                                {
                                    $rules['amenities_image.'.$i] = 'required';
                                }
                            }else {
                                //  $rules['amenities_image'] = 'required';
                            }
                            
                    $room_type = Input::get('room_type');
                    foreach ($room_type  as $key => $value) {
                        $fields['room_type'.$key] = $value;
                        $rules['room_type'.'1'] = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:room_type_infos,room_type';
                    }

                    */                                        

                    $room_type = Input::get('room_type');
                    foreach ($room_type  as $key => $value) {
                        $fields['room_type'.$key] = $value;
                        $rules['room_type'.'1'] = 'required';
                    }

                    $description = Input::get('description');
                    foreach ($description  as $key => $value) {
                        $fields['description'.$key] = $value;
                        $rules['description'.'1'] = 'required';
                    }                    

                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{

                            $Room_type = new Room_type;

                            $Room_type->normal_price = $_POST['normal_price'];
                            $Room_type->discount_price = $_POST['discount_price'];
                            $Room_type->adult_count = $_POST['adult_count'];
                            $Room_type->child_count = $_POST['child_count'];
                            $Room_type->description = $_POST['description'][1];
                            $Room_type->url_index =  $_POST['room_type'][1] ? str_slug($_POST['room_type'][1]): str_slug($_POST['room_type'][1]);

                            $vendor_id = Session::get('manager_vendor');
                            $property_id = Session::get('manager_outlet');
                            $manager_id = Session::get('manager_id');

                            $Room_type->property_id = $property_id;                            
                            $Room_type->created_by = $vendor_id;
                            $Room_type->manager_id = $manager_id;
                            $Room_type->created_date = date("Y-m-d H:i:s");          
                            //  $Room_type->modified_date = date("Y-m-d H:i:s");
                            $Room_type->default_status =  isset($_POST['status']) ? $_POST['status']: 0;

                            $Room_type->save();

                            $imageName = strtolower($Room_type->id . '.' . $data->file('image')->getClientOriginalExtension());
                            $data->file('image')->move(
                                base_path() . '/public/assets/admin/base/images/room_type/', $imageName
                            );
                            $destinationPath2 = url('/assets/admin/base/images/room_type/'.$imageName.'');
                          
                            $size=getImageResize('VENDOR');
                            Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/room_type/list/'.$imageName)->destroy();
                            
                            
                            $Room_type->room_image=$imageName;

                            $Room_type->save();

                            /*

                            for($i=0; $i<count($_POST['amenities_name']); $i++)
                            {

                                $extra_amenities = new Extra_amenities;

                                $extra_amenities->amenity_name  = $_POST['amenities_name'][$i];
                                $extra_amenities->room_type_id  = $Room_type->id;
                                $extra_amenities->url_index =  $_POST['amenities_name'][$i] ? str_slug($_POST['amenities_name'][$i]): str_slug($_POST['amenities_name'][$i]);
                                $extra_amenities->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                                $extra_amenities->created_date = date("Y-m-d H:i:s");
                                $extra_amenities->save();


                                $imageName = strtolower($Room_type->id . $_POST['amenities_name'][$i] . '.' . $data->file('amenities_image')[$i]->getClientOriginalExtension());
                                $data->file('amenities_image')[$i]->move(
                                    base_path() . '/public/assets/admin/base/images/amenities/extra_amenities/', $imageName
                                );
                                $destinationPath2 = url('/assets/admin/base/images/amenities/extra_amenities/'.$imageName.'');

                                */

                                /*                              
                                    $size=getImageResize('VENDOR');
                                    Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/room_type/list/'.$imageName)->destroy();
                                                                
                                */                       

                                /*         
                                $extra_amenities->amenity_image=$imageName;

                                $extra_amenities->save();

                            }

                            */

                            $this->roomtype_save_after($Room_type,$_POST);
                            Session::flash('message', trans('messages.Room Type has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('managers/room_type');
                    }
                }
              
                
                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function roomtype_update(Request $data, $id)
                {
                    /*                   
                        print_r($id);
                        print_r($data->all());
                        exit;
                    */                   

                    $fields['room_type']      = Input::get('room_type');
                    $fields['normal_price']   = Input::get('normal_price');
                    $fields['discount_price'] = Input::get('discount_price');
                    $fields['adult_count']    = Input::get('adult_count');
                    $fields['child_count']    = Input::get('child_count');
                    $fields['description']    = Input::get('description');                    
                    $fields['room_image']     = Input::file('room_image');

                    $rules = array(
                        'normal_price'   => 'required|numeric',
                        'discount_price' => 'required|numeric',
                        'adult_count'    => 'required|numeric',
                        'child_count'    => 'required|numeric',
                        'description'    => 'required',
                        //'image'          => 'required|mimes:png,jpg,jpeg,bmp|max:2024',
                        'room_image'     => 'nullable|mimes:png,jpg,jpeg,bmp|max:2024',
                    );

                    
                    $room_type = Input::get('room_type');
                    foreach ($room_type  as $key => $value) {
                        $fields['room_type'.$key] = $value;
                        //	$rules['room_type'.'1'] = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:room_type_infos,room_type,'.$id.',id';
                        $rules['room_type'.'1'] = 'required|regex:/[A-Za-z\s ]+/';
                    }

                    $description = Input::get('description');
                    foreach ($description  as $key => $value) {
                        $fields['description'.$key] = $value;
                        $rules['description'.'1'] = 'required';
                    }                    

                    $validator = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        
                        try{
                          
                            $Room_type = Room_type::find($id); 
                            $Room_type->normal_price = $_POST['normal_price'];
                            $Room_type->discount_price = $_POST['discount_price'];
                            $Room_type->adult_count = $_POST['adult_count'];
                            $Room_type->child_count = $_POST['child_count'];
                            $Room_type->description = $_POST['description'][1];
                            $Room_type->url_index =  $_POST['room_type'][1] ? str_slug($_POST['room_type'][1]): str_slug($_POST['room_type'][1]);

                            $vendor_id = Session::get('manager_vendor');
                            $property_id = Session::get('manager_outlet');
                            $manager_id = Session::get('manager_id');

                            $Room_type->created_by = $vendor_id;
                            $Room_type->property_id = $property_id;
                            $Room_type->manager_id = $manager_id;
                            $Room_type->modified_date = date("Y-m-d H:i:s");
                            $Room_type->default_status =  isset($_POST['status']) ? $_POST['status']: 0;

                            $Room_type->save();

                            if(isset($_FILES['room_image']['name']) && $_FILES['room_image']['name']!=''){
                                        $imageName = strtolower($id . '.' . $data->file('room_image')->getClientOriginalExtension());
                                        $data->file('room_image')->move(
                                            base_path() . '/public/assets/admin/base/images/room_type/', $imageName
                                        );
                                        $destinationPath2 = url('/assets/admin/base/images/room_type/'.$imageName.'');
                                /*                                         
                                        $size=getImageResize('VENDOR');
                                        Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/room_type/list/'.$imageName)->destroy();

                                       Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/detail/'.$imageName)->destroy();
                                        Image::make( $destinationPath2 )->fit($size['THUMB_WIDTH'], $size['THUMB_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/'.$imageName)->destroy();
                                        Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/detail/'.$imageName)->destroy();
                                */
                                $Room_type->room_image = $imageName;

                                $Room_type->save();
                            }

                            $this->roomtype_save_after($Room_type,$_POST);
                            Session::flash('message', trans('messages.Room Types has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('managers/room_type');
                    }
                }

               
               
                /**
                 * add,edit datas  saved in main table 
                 * after inserted in sub tabel.
                 *
                 * @param  int  $id
                 * @return Response
                 */
               public static function roomtype_save_after($object,$post)
               {
                    $city = $object;
                    $post = $post;
                    if(isset($post['room_type'])){
                        $room_type = $post['room_type'];
                        try{                
                            $affected = DB::table('room_type_infos')->where('id', '=', $object->id)->delete();
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
                                if(isset($room_type[$lang->id]) && $room_type[$lang->id]!=""){
                                    $infomodel = new Room_type_infos;
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->id = $object->id; 
                                    $infomodel->room_type = $room_type[$lang->id];
                                    $infomodel->save();
                                }
                            }
                            }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                    }
               }
                
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function roomtype_destroy($id)
                {

                    //  print_r($id);
                    //  exit;
                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } 
                    $data = Room_type::find($id);
                    $data->delete();
                    Session::flash('message', trans('messages.Room Type has been deleted successfully!'));
                    return Redirect::to('managers/room_type');
                }

                
                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxRoomtypes()
                {
                                                          
                $query = '"room_type_infos"."language_id" = (case when (select count(*) as totalcount from room_type_infos where room_type_infos.language_id = '.getAdminCurrentLang().' and room_type.id = room_type_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                $Room_type = Room_type::Leftjoin('room_type_infos','room_type_infos.id','=','room_type.id')
                                ->select(DB::raw('room_type.* ,room_type.id as cid'),'room_type_infos.*')
                                ->where("room_type_infos.language_id","=",getAdminCurrentLang())
                                ->where("room_type.created_by",'=',Session::get('manager_vendor'))
                                ->where("room_type.property_id",'=',Session::get('manager_outlet'))                    
                                ->whereRaw($query)
                                ->orderBy('room_type.id', 'desc')
                                ->get();
                    return Datatables::of($Room_type)->addColumn('action', function ($Room_type) {
                            return '<div class="btn-group"><a href="'.URL::to("managers/room_type/edit/".$Room_type->cid).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("managers/room_type/delete/".$Room_type->cid).'" class="delete-'.$Room_type->cid.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                    </ul>
                                </div><script type="text/javascript">
                                $( document ).ready(function() {
                                $(".delete-'.$Room_type->cid.'").on("click", function(){
                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });});</script>';
                        })
                        ->addColumn('default_status', function ($Room_type) {
                            if($Room_type->default_status==0):
                                $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                            elseif($Room_type->default_status==1):
                                $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                            endif;
                            return $data;
                        })
                        ->addColumn('property_name', function ($Room_type) {
                                $data = Session::get('outlet_name');
                            return $data;
                        })                         
                        ->addColumn('normal_price', function ($Room_type) {
                                $data = ucfirst($Room_type->normal_price);
                            return $data;
                        })          
                        ->addColumn('discount_price', function ($Room_type) {
                                $data = ucfirst($Room_type->discount_price);
                            return $data;
                        })      
                        ->addColumn('adult_count', function ($Room_type) {
                                $data = ucfirst($Room_type->adult_count);
                            return $data;
                        })        
                        ->addColumn('child_count', function ($Room_type) {
                                $data = ucfirst($Room_type->child_count);
                            return $data;
                        })                                                                                       
                        ->addColumn('room_type', function ($Room_type) {
                                $data = ucfirst($Room_type->room_type);
                            return $data;
                        })
                        ->make(true);
                }
                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function rooms()
                {
                    //  dd("Ak")
                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } 
                    else
                    {
                        $query = '"rooms_infos"."language_id" = (case when (select count(*) as totalcount from rooms_infos where rooms_infos.language_id = '.getAdminCurrentLang().' and rooms.id = rooms_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $zones=DB::table('rooms')
                         ->select('rooms.*','rooms_infos.*')
                        ->leftJoin('rooms_infos','rooms_infos.id','=','rooms.id')
                        ->whereRaw($query)
                        ->orderBy('room_name', 'asc')  
                        ->get(); 
                        return view('managers.rooms.list')->with('rooms', $zones);
                    }
                    
                }
                
                public function rooms_create()
                {
                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } 
                    else{


                        $room_type = DB::table('room_type')
                            ->select('room_type.*','room_type_infos.*')
                            ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                            ->where('default_status', 1)
                            ->where('property_id',Session::get('manager_outlet'))
                            ->orderBy('room_type', 'asc')
                            ->get();
                        return view('managers.rooms.create')->with('room_type', $room_type);
                    }
                }
                
                public function rooms_edit($id)
                {
                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } 
                    else
                    {
                        $info = new Rooms_infos;       
                        $rooms = Rooms::find($id);

                        $rooms_infos = DB::table('rooms')
                            ->select('rooms.*','rooms_infos.*')
                            ->leftJoin('rooms_infos','rooms_infos.id','=','rooms.id')
                            ->where('rooms.id','=',$id)
                            ->get();

                        $room_type = DB::table('room_type')
                            ->select('room_type.*','room_type_infos.*')
                            ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                            ->where('default_status', 1)
                            ->orderBy('room_type', 'asc')
                            ->get();

                        return view('managers.rooms.edit')->with('data', $rooms)->with('infomodel', $info)->with('room_type', $room_type)->with('rooms_infos',$rooms_infos[0]);
                    }
                }
                
                public function rooms_store(Request $data)
                {
                    //print_r($data->all());

                    //  dd("Ak");

                    $rooms_count = DB::Table('rooms')
                                    ->where('created_by','=',Session::get('manager_vendor'))
                                    ->where('property_id','=',Session::get('manager_outlet'))
                                    ->count();

                    $freeuser = 5;

                    if($rooms_count >= $freeuser)
                        {
                            Session::flash('message', trans('messages.Already 5 Rooms are Added'));
                            return Redirect::to('vendor/rooms');                            
                        }

                    $fields['room_name'] = Input::get('room_name');
                    $fields['room_type_id'] = Input::get('room_type_id');
                    $fields['room_floor'] = Input::get('room_floor');

                    $rules = array(

                        'room_name' => 'required',
                        'room_type_id' => 'required',
                        'room_floor' => 'required',
                    );
                    $room_name = Input::get('room_name');
                    /*                  
                        foreach ($room_name  as $key => $value) {
                            $fields['room_name'.$key] = $value;
                            $rules['room_name'.'1'] = 'required|alpha|unique:rooms_infos,room_name';
                        }
                    */
                    
                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{

                            $Rooms = new Rooms;
                            $Rooms->url_index =  $_POST['room_name'] ? str_slug($_POST['room_name']): str_slug($_POST['room_name']);
                            $Rooms->room_type_id = $_POST['room_type_id'];
                            $Rooms->room_floor = $_POST['room_floor'];
                            $Rooms->created_at = date("Y-m-d H:i:s");
                            $Rooms->default_status =  isset($_POST['status']) ? $_POST['status']: 0;

                            $vendor_id = Session::get('manager_vendor');
                            $property_id = Session::get('manager_outlet');
                            $manager_id = Session::get('manager_id');

                            $Rooms->created_by = $vendor_id;
                            $Rooms->property_id = $property_id;
                            $Rooms->manager_id = $manager_id;
 
                            $Rooms->save();

                            $this->rooms_save_after($Rooms,$_POST);
                            Session::flash('message', trans('messages.Room has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('managers/rooms');
                    }
                }
                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function rooms_update(Request $data, $id)
                {

                    $fields['room_name'] = Input::get('room_name');
                    $fields['room_type_id'] = Input::get('room_type_id');
                    $fields['room_floor'] = Input::get('room_floor');

                    $rules = array(

                        'room_name' => 'required',
                        'room_type_id' => 'required',
                        'room_floor' => 'required',
                        
                    );
                    $room_name = Input::get('room_name');
                   
                    /*                    
                        foreach ($room_name  as $key => $value) {
                            $fields['room_name'.$key] = $value;
                            $rules['room_name'.'1'] = 'required|alpha|unique:rooms_infos,room_name,'.$id.',id';
                            
                        }
                    */                    
                    $validator = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{
                         
                            $Rooms = Rooms::find($id);

                            $Rooms->url_index =  $_POST['room_name'] ? str_slug($_POST['room_name']): str_slug($_POST['room_name']);
                            $Rooms->room_type_id = $_POST['room_type_id'];
                            $Rooms->room_floor = $_POST['room_floor'];
                            $Rooms->updated_at = date("Y-m-d H:i:s");
                            $Rooms->default_status =  isset($_POST['status']) ? $_POST['status']: 0;

                            $vendor_id = Session::get('manager_vendor');
                            $property_id = Session::get('manager_outlet');
                            $manager_id = Session::get('manager_id');

                            $Rooms->manager_id = $manager_id;
                            $Rooms->property_id = $property_id;
                            $Rooms->created_by = $vendor_id;

                            $Rooms->save();
                            $this->rooms_save_after($Rooms,$_POST);
                            Session::flash('message', trans('messages.Rooms has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('managers/rooms');
                    }
                }
                
               
                /**
                 * add,edit datas  saved in main table 
                 * after inserted in sub tabel.
                 *
                 * @param  int  $id
                 * @return Response
                 */
               public static function rooms_save_after($object,$post)
               {
                    $rooms = $object;
                    $post = $post;

                    if(isset($post['room_name'])){
                        $room_name = $post['room_name'];
                        try{
                            
                            $affected = DB::table('rooms_infos')->where('id', '=', $object->id)->delete();
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
                                if(isset($room_name) && $room_name!=""){
                                    $infomodel = new Rooms_infos;
                                    $infomodel->id = $object->id; 
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->room_name = $room_name;
                                    $infomodel->save();
                                }
                            }
                        }catch(Exception $e) {
                            Log::Instance()->add(Log::ERROR, $e);
                        }
                    }
               }
                
                
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function rooms_destroy($id)
                {
                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } 

                    $data = Rooms::find($id);
                    $data->delete();
                    Session::flash('message', trans('messages.Rooms has been deleted successfully!'));
                    return Redirect::to('managers/rooms');
                }
                
                
                
                
                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxRooms()
                {
                                       
             
                    $query = '"rooms_infos"."language_id" = (case when (select count(*) as totalcount from rooms_infos where rooms_infos.language_id = '.getAdminCurrentLang().' and rooms.id = rooms_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                    $rooms = Rooms::Leftjoin('rooms_infos','rooms_infos.id','=','rooms.id')
                                ->leftJoin('room_type','room_type.id','=','rooms.room_type_id')
                                ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')

                                ->select(DB::raw('rooms.* ,rooms.id as cid , rooms.default_status as status_default'),'rooms_infos.*','room_type.*','room_type_infos.*')
                                ->where("rooms_infos.language_id","=",getAdminCurrentLang())
                                ->where("rooms.created_by",'=',Session::get('manager_vendor'))
                                ->where("rooms.property_id",'=',Session::get('manager_outlet'))
                                ->orderBy("rooms.id",'desc')
                                ->whereRaw($query)
                                ->get();

                    return Datatables::of($rooms)->addColumn('action', function ($rooms) {
                            return '<div class="btn-group"><a href="'.URL::to("managers/rooms/edit/".$rooms->cid).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("managers/rooms/delete/".$rooms->cid).'" class="delete-'.$rooms->cid.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                    </ul>
                                </div><script type="text/javascript">
                                $( document ).ready(function() {
                                $(".delete-'.$rooms->cid.'").on("click", function(){
                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });});</script>';
                        })
                        ->addColumn('property_name', function ($Room_type) {
                                $data = Session::get('outlet_name');
                            return $data;
                        })  
                        ->addColumn('status_default', function ($rooms) {

                            if($rooms->status_default==0):
                                $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                            elseif($rooms->status_default==1):
                                $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                            endif;
                            return $data;
                        })
                        ->make(true);
                }
                

}
