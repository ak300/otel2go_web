<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;
use App\Model\vendors;
use App\Model\vendors_infos;
use App\Model\users;
use App\Model\outlets;
use App\Model\outlet_infos;
use App\Model\delivery_timings;
use App\Model\opening_timings;
use App\Model\settings;
use App\Model\outlet_managers;
use Illuminate\Support\Facades\Text;
class Gallery extends Controller
{
    const VENDORS_REGISTER_EMAIL_TEMPLATE = 4;
    const MANAGER_WELCOME_EMAIL_TEMPLATE = 11;
    const MANAGER_SIGNUP_EMAIL_TEMPLATE = 12;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

   
	public function outlets_gallery_index()
	{
		//$vendor_id = Session::get('vendor_id');
		//print_r($vendor_id);exit;
        if (!Session::get('vendor_id')) {
                return redirect()->guest('vendors/login');
        }
        if (!Session::get('property_id'))
            {
                Session::flash('no-property','Add Property First');
                return redirect::to('vendor/outlets');
            }          
        		
		$settings = Settings::find(1);
		return view('vendors.property.list');
		
	}
	public function gallery_upload(Request $data)
	{
		
		$post = $data->all();
		$index = $post['index'];
		$file = Input::file('file');
		//echo '<pre>';print_r($file);die;
		$filename = strtotime(date("Y-m-d H:i:s")).strtolower(str_slug($file->getClientOriginalName()));
		$extension = $file->getClientOriginalExtension();
		$filename = $filename.'.'.$extension;
		$vendor_id = Session::get('vendor_id');
		$property_id = Session::get('property_id');
		
		//echo $vendor_id;die;
		if (!file_exists(base_path().'/public/assets/admin/base/images/vendors/property/'.$property_id.'/'))
		{
			$result = File::makeDirectory(base_path().'/public/assets/admin/base/images/vendors/property/'.$property_id.'/', 0777, true);
		}
		if (!file_exists(base_path().'/public/assets/admin/base/images/vendors/property/'.$property_id.'/thumb/'))
		{
			$result = File::makeDirectory(base_path().'/public/assets/admin/base/images/vendors/property/'.$property_id.'/thumb/', 0777, true);
		}
		$data->file('file')->move(
			base_path().'/public/assets/admin/base/images/vendors/property/'.$property_id.'/',$filename);
		$destinationPath = url('/assets/admin/base/images/vendors/property/'.$property_id.'/'.$filename);
		Image::make( $destinationPath )->fit(1174,534)->save(base_path().'/public/assets/admin/base/images/vendors/property/'.$property_id.'/'.$filename)->destroy();
		
		Image::make( $destinationPath )->fit(250,250)->save(base_path().'/public/assets/admin/base/images/vendors/property/'.$property_id.'/thumb/'.$filename)->destroy();
		echo $index;exit;

	}
	
	public function delete_image(Request $data)
	{
		$post = $data->all();
		unlink($post['image_path']);
		unlink($post['thumb']);
		return response()->json(['status'=>'200']);
	}
}