<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;
use App\Model\housekeeping_staffs as HouseKeepingModel;
use App\Model\housekeeping_tasks as HouseKeepingTasks;
use App\Model\rooms as Rooms;

class Managers_HouseKeeping extends Controller
{
    
     public function __construct(){

     }

     /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyAjaxHousekeeping()
    { 
                           
        $vendor_id = Session::get('manager_vendor');
        $property_id = Session::get('manager_outlet');
        $outlet_staffs=DB::table('housekeeping_staffs')
         ->leftjoin('outlet_infos','outlet_infos.id','=','housekeeping_staffs.outlet_id')
        ->where('housekeeping_staffs.vendor_id','=',$vendor_id)
        ->where('housekeeping_staffs.outlet_id','=',$property_id)
        ->select('housekeeping_staffs.id','outlet_infos.outlet_name','housekeeping_staffs.firstname','housekeeping_staffs.lastname','housekeeping_staffs.mobile_number','housekeeping_staffs.status','housekeeping_staffs.created_at','housekeeping_staffs.updated_at')
        ->orderBy('outlet_infos.outlet_name', 'asc')
        ->get();
        return Datatables::of($outlet_staffs)->addColumn('action', function ($outlet_staffs) {
                return '<div class="btn-group"><a href="'.URL::to("managers/house_keeping/edit/".$outlet_staffs->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                        <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu xs pull-right" role="menu">
                        <li><a href="'.URL::to("managers/house_keeping/view/".$outlet_staffs->id).'"  title="'.trans("messages.View").'"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;'.@trans("messages.View").'</a></li>
                        </li>
                        <li><a href="'.URL::to("managers/house_keeping/delete/".$outlet_staffs->id).'" class="delete-'.$outlet_staffs->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                        </ul>
                    </div><script type="text/javascript">
                    $( document ).ready(function() {
                    $(".delete-'.$outlet_staffs->id.'").on("click", function(){
                         return confirm("'.trans("messages.Are you sure want to delete?").'");
                    });});</script>';
            })
            ->editColumn('firstname', function ($outlet_staffs) {
                $data = '-';
                if($outlet_staffs->firstname != null && $outlet_staffs->lastname != null):
              $data = $outlet_staffs->firstname.' '.$outlet_staffs->lastname;
                else:
              $data = $outlet_staffs->firstname;
                endif;
                return $data;
            })
            ->editColumn('status', function ($outlet_staffs) {
                    $data = '-';
                    if($outlet_staffs->status != null)
                    {
                      if($outlet_staffs->status == 0){ 
                        $data = "<span class='label label-danger'>Inactive</span>";
                       }elseif($outlet_staffs->status == 1){ 
                        $data = "<span class='label label-success'>Active</span>";
                       }
                    } 
                    return $data;
            })
            ->make(true);
    }

    /**
     * Show the House Keeper List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //  dd('Ak');

         if (!Session::get('manager_id')) {
            return redirect()->guest('managers/login');
        } else {
            return view('managers.house_keeping.list');
        }
        
    }

    /**
     * Show the House Keeper Add Form.
     *
     */
    public function create()
    {
        if (!Session::get('manager_id')) {
            return redirect()->guest('managers/login');
        } else{
            return view('managers.house_keeping.create');
        }
    }

    /**
     * Storing the House Keeper Details.
     *
     */
    public function store(Request $data)
    {
        $validation = Validator::make($data->all(),array(
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'mobile' => 'required|numeric|digits_between:6,16',
            'date_of_birth' => 'required',
            'gender' => 'required',
            //  'outlet_name' => 'required', 
            'address' => 'required|alpha',
            'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:2048',
        ));   
                // process the validation
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } else {
            $housekeeping_staffs = new HouseKeepingModel();
            $housekeeping_staffs->firstname = Input::get('first_name');
            $housekeeping_staffs->lastname = Input::get('last_name');
            $housekeeping_staffs->mobile_number = Input::get('mobile');
            $housekeeping_staffs->date_of_birth = Input::get('date_of_birth');
            $housekeeping_staffs->gender = Input::get('gender');
            $housekeeping_staffs->outlet_id = Session::get('manager_outlet');
            $housekeeping_staffs->vendor_id = Session::get('manager_vendor');
            $housekeeping_staffs->address = Input::get('address');
            $housekeeping_staffs->status = (int)Input::get('status');
            $housekeeping_staffs->manager_id = Session::get('manager_id');
            $housekeeping_staffs->save();

            //For Housekeeper Image
            if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=''){ 
                $destinationPath = base_path().'/public/assets/admin/base/images/house_keepers';
                $imageName = $_FILES['image']['name'];
                $data->file('image')->move($destinationPath, $imageName);

                $housekeeping_staffs->image = $imageName;
                $housekeeping_staffs->save();
            }
            Session::flash('message', trans('messages.Housekeeper Details has been submitted successfully!'));
            return Redirect::to('managers/house_keeping');
        }
    }

    /**
     * House Keeper Details For Editing.
     *
     */
    public function edit($id){
        if (!Session::get('manager_id')) {
            return redirect()->guest('managers/login');
        }      
        $housekeepers = HouseKeepingModel::find($id);

        return view('managers.house_keeping.edit')->with('data',$housekeepers);
    }

     /**
     * Updating House Keeper Details.
     *
     */
    public function update(Request $data,$id){
      
         $validation = Validator::make($data->all(),array(
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'mobile' => 'required|numeric|digits_between:6,16',
            'date_of_birth' => 'required',
            'gender' => 'required',
            //  'outlet_name' => 'required', 
            'address' => 'required|alpha',
            'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:2048',
        ));   
                // process the validation
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } else {
            $housekeeping_staffs = HouseKeepingModel::find($id);
            $housekeeping_staffs->firstname = Input::get('first_name');
            $housekeeping_staffs->lastname = Input::get('last_name');
            $housekeeping_staffs->mobile_number = Input::get('mobile');
            $housekeeping_staffs->date_of_birth = Input::get('date_of_birth');
            $housekeeping_staffs->gender = Input::get('gender');
            $housekeeping_staffs->outlet_id = Session::get('manager_outlet');
            $housekeeping_staffs->vendor_id = Session::get('manager_vendor');
            $housekeeping_staffs->address = Input::get('address');
            $housekeeping_staffs->status = (int)Input::get('status');
            $housekeeping_staffs->manager_id = Session::get('manager_id');
            $housekeeping_staffs->save();

            //For Housekeeper Image
            if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=''){ 
                $destinationPath = base_path().'/public/assets/admin/base/images/house_keepers';
                $imageName = $_FILES['image']['name'];
                $data->file('image')->move($destinationPath, $imageName);

                $housekeeping_staffs->image = $imageName;
                $housekeeping_staffs->save();
            }
            Session::flash('message', trans('messages.Housekeeper Details has been updated successfully!'));
            return Redirect::to('managers/house_keeping');
        }
    }

     /**
     * Deleting House Keeper Details.
     *
     */
    public function destroy($id){
        $housekeepers = HouseKeepingModel::find($id);
        $housekeepers->delete();
        Session::flash('message', trans('messages.Housekeeper Details has been deleted successfully!'));
        return Redirect::to('managers/house_keeping');
    }

    /**
     * View the House Keeper Details.
     *
     */
    public function show($id){
        $housekeepers = HouseKeepingModel::find($id)->toArray();
        return view('managers.house_keeping.show')->with('data',$housekeepers);
    }

    
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyAjaxHousekeepingTasks()
    {
                           
        
        //echo "<pre>";print_r($room_clean_status);die;                          
        $vendor_id = Session::get('manager_vendor');
        $property_id = Session::get('manager_outlet');
        $outlet_tasks=DB::table('housekeeping_tasks')
         ->leftjoin('outlet_infos','outlet_infos.id','=','housekeeping_tasks.outlet_id')
         ->leftjoin('rooms_infos','rooms_infos.id','=','housekeeping_tasks.room_id')
         ->leftjoin('housekeeping_staffs','housekeeping_staffs.id','=','housekeeping_tasks.housekeeper_id')
        ->where('housekeeping_tasks.vendor_id','=',$vendor_id)
        ->where('housekeeping_tasks.outlet_id','=',$property_id)
        ->select('housekeeping_tasks.id','outlet_infos.outlet_name','housekeeping_tasks.taskname','housekeeping_staffs.firstname','housekeeping_staffs.lastname','rooms_infos.room_name','housekeeping_tasks.task_date','housekeeping_tasks.task_time','housekeeping_tasks.created_at','housekeeping_tasks.updated_at','housekeeping_tasks.room_clean_status')
        ->orderBy('housekeeping_tasks.id', 'desc')
        ->get();
         
        return Datatables::of($outlet_tasks)->addColumn('action', function ($outlet_tasks) {
                return '<div class="btn-group"><a href="'.URL::to("managers/housekeeping_tasks/edit/".$outlet_tasks->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                        <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu xs pull-right" role="menu">
                        <li><a href="'.URL::to("managers/housekeeping_tasks/delete/".$outlet_tasks->id).'" class="delete-'.$outlet_tasks->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                        </ul>
                    </div><script type="text/javascript">
                    $( document ).ready(function() {
                    $(".delete-'.$outlet_tasks->id.'").on("click", function(){
                         return confirm("'.trans("messages.Are you sure want to delete?").'");
                    });});</script>';
            })
            ->addColumn('room_cleaning_status',function($outlet_tasks){
                $room_cleans = getRoomCleanStatus();
                $data = "<select name='clean_status' id='clean_status' class='clean_status form-control'>";
                    if(count($room_cleans)>0){
                        foreach($room_cleans as $km => $val){
                          if($km == $outlet_tasks->room_clean_status)
                            $data .= "<option data-id='".$outlet_tasks->id."' value='".$km."' selected>".$val."</option>";
                          else
                            $data .= "<option data-id='".$outlet_tasks->id."' value='".$km."'>".$val."</option>";
                        }
                      } 
                $data .= "</select>";
                return $data;                             
              })
            ->editColumn('firstname', function ($outlet_tasks) {
                $data = '-';
                if($outlet_tasks->firstname != null && $outlet_tasks->lastname != null):
              $data = $outlet_tasks->firstname.' '.$outlet_tasks->lastname;
                else:
              $data = $outlet_tasks->firstname;
                endif;
                return $data;
            })
            ->make(true);
    }


    /**
     * Show the House Keeper Tasks List.
     *
     * @return \Illuminate\Http\Response
     */
    public function housekeepingTaskIndex(){

      //  dd('Ak');
         if (!Session::get('manager_id')) {
            return redirect()->guest('managers/login');
        } else {
            return view('managers.house_keeping.house_keeping_tasks.list');
        }
    }

    /**
     * Show the House Keeper Task Add Form.
     *
     */
    public function housekeepingTaskCreate()
    {
        if (!Session::get('manager_id')) {
            return redirect()->guest('managers/login');
        } else{

            $outlet_id = Session::get('manager_outlet');  
            $rooms = DB::table('rooms')->leftjoin('rooms_infos','rooms_infos.id','=','rooms.id')
                 ->where('rooms.property_id','=',$outlet_id)
                ->select('rooms.id','rooms_infos.room_name')
                ->get(); 
            $rooms = iterator_to_array($rooms);
            
            $Housekeepers = DB::table('housekeeping_staffs')
                   ->where('outlet_id','=',$outlet_id)
                  ->select('id','firstname','lastname')
                  ->get();    
            $Housekeepers = iterator_to_array($Housekeepers);                                 
            return view('managers.house_keeping.house_keeping_tasks.create')->with('room_details',$rooms)->with('housekeeper_details',$Housekeepers);
        }
    }

    public function housekeepingTaskStore(Request $data){


         $validation = Validator::make($data->all(),array(
            //  'outlet_name' => 'required',
            'room_name' => 'required',
            'housekeeper_name' => 'required',
            'task_name' => 'required|string',
            'date' => 'required',
            'time' => 'required',
        ));   
         //Incase of validation failure send the room and housekeeper details to the create page
          $outlet_id = Input::get('outlet_name');
          $rooms = DB::table('rooms')->leftjoin('rooms_infos','rooms_infos.id','=','rooms.id')
                   ->where('rooms.property_id','=',$outlet_id)
                  ->select('rooms.id','rooms_infos.room_name')
                  ->get();    
          $rooms = iterator_to_array($rooms);

          $Housekeepers = DB::table('housekeeping_staffs')
                        ->where('outlet_id','=',$outlet_id)
                        ->select('id','firstname','lastname')
                        ->get();  
          $Housekeepers = iterator_to_array($Housekeepers);        
        // process the validation
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput()->with('room_details',$rooms)->with('housekeeper_details',$Housekeepers);
        } else {
            $housekeeping_tasks = new HouseKeepingTasks();
            $housekeeping_tasks->vendor_id = Session::get('manager_vendor');
            $housekeeping_tasks->manager_id = Session::get('manager_id');
            $housekeeping_tasks->outlet_id = Input::get('outlet_name');
            $housekeeping_tasks->room_id = Input::get('room_name');
            $housekeeping_tasks->housekeeper_id = Input::get('housekeeper_name');
            $housekeeping_tasks->taskname = Input::get('task_name');
            $housekeeping_tasks->task_date = Input::get('date');
            $housekeeping_tasks->task_time = Input::get('time');
            $housekeeping_tasks->save();

            Session::flash('message', trans('messages.Housekeeper Task Details has been submitted successfully!'));
            return Redirect::to('managers/housekeeping_tasks');
        }
    }

    public function housekeepingTaskEdit($id){
            $task_details = HouseKeepingTasks::find($id)->toArray();
            //echo "<pre>";print_r($task_details);die;
            $property_id = $task_details['outlet_id'];
            $rooms = DB::table('rooms')->leftjoin('rooms_infos','rooms_infos.id','=','rooms.id')
                   ->where('rooms.property_id','=',$property_id)
                  ->select('rooms.id','rooms_infos.room_name')
                  ->get();    
            $rooms = iterator_to_array($rooms);

            $Housekeepers = DB::table('housekeeping_staffs')
                   ->where('outlet_id','=',$property_id)
                  ->select('id','firstname','lastname')
                  ->get();    
            $Housekeepers = iterator_to_array($Housekeepers);

            return view('managers.house_keeping.house_keeping_tasks.edit')->with('data',$task_details)->with('room_details',$rooms)->with('housekeeper_details',$Housekeepers);
    }

     /**
     * Updating House Keeper Details.
     *
     */
    public function housekeepingTaskUpdate(Request $data,$id){

         $validation = Validator::make($data->all(),array(
            //  'outlet_name' => 'required',
            'room_name' => 'required',
            'housekeeper_name' => 'required',
            'task_name' => 'required|string',
            'date' => 'required',
            'time' => 'required',
        ));    
                // process the validation
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } else {
            $housekeeping_tasks = HouseKeepingTasks::find($id);
            $housekeeping_tasks->vendor_id = Session::get('manager_vendor');
            $housekeeping_tasks->outlet_id = Session::get('manager_outlet');
            $housekeeping_tasks->manager_id= Session::get('manager_id');
            $housekeeping_tasks->room_id = Input::get('room_name');
            $housekeeping_tasks->housekeeper_id = Input::get('housekeeper_name');
            $housekeeping_tasks->taskname = Input::get('task_name');
            $housekeeping_tasks->task_date = Input::get('date');
            $housekeeping_tasks->task_time = Input::get('time');
            $housekeeping_tasks->save();

            Session::flash('message', trans('messages.Housekeeper Task Details has been updated successfully!'));
            return Redirect::to('managers/housekeeping_tasks');
        }
    }

    public function housekeepingTaskDestroy($id){
        $housekeepers = HouseKeepingTasks::find($id);
        $housekeepers->delete();
        Session::flash('message', trans('messages.Housekeeper Task Details has been deleted successfully!'));
        return Redirect::to('managers/housekeeping_tasks');
    }

    /**
     *Selecting All Rooms based on Property id.
     */
    public function allRoomsByProperty(Request $request){
        $result = '';
        if($request){
          $property = $request['property_id'];
          $rooms = DB::table('rooms')->leftjoin('rooms_infos','rooms_infos.id','=','rooms.id')
                   ->where('rooms.property_id','=',$property)
                  ->select('rooms.id','rooms_infos.room_name')
                  ->get();    
          $rooms = iterator_to_array($rooms);
          $result = (count($rooms) > 0)?$rooms:array();
        }
        $json_data = array(
          "data"            => $result,
        );
        echo json_encode($json_data);
        }

    /**
     *Selecting All Housekeepers based on Property id.
     */    
    public function getAllHousekeepers(Request $request){
        $result = '';
        if($request){
          $property = $request['property_id'];
          $Housekeepers = DB::table('housekeeping_staffs')
                   ->where('outlet_id','=',$property)
                  ->select('id','firstname','lastname')
                  ->get();    
          $Housekeepers = iterator_to_array($Housekeepers);
          $result = (count($Housekeepers) > 0)?$Housekeepers:array();
        }
        $json_data = array(
          "data"            => $result,
        );
        echo json_encode($json_data);
      
    } 

    public function updateTaskStatus(Request $request){
      $result = '';
      if($request){
          $task_status = $request['status'];
          $task_id = $request['task_id'];
          $managetask = HouseKeepingTasks::find($task_id);
          $managetask->room_clean_status = $task_status;
          $managetask->save();
          $json_data = array(
          "success" => true,
        );
        echo json_encode($json_data);
      }
    }   
}
