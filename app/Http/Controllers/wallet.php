<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Support\Facades\Input;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ServerErrorResponseException;
use DB;
use Session;
use Closure;
use MetaTag;
use Mail;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use App\Model\api;
use App\Model\Api_model;
use App\Model\admin_customers;
use App\Model\user_wallets;

class wallet extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $_apiContext;
    public function __construct()
    {
        $this->user_details = array();
        $this->api = New Api;
        $this->language = getCurrentLang();
        $user_details = $this->check_login();
        $this->theme = Session::get("general")->theme;
    }
    
    public function check_login()
    {
        //  dd(Session()->all());
        $user_id = Session::get('user_id');

        //  dd($user_id);
        $token = Session::get('token');
        //Session::put('token', $response->response->token);
        if(empty($user_id))
        {
            return Redirect::to('/')->send();
        }
        $user_array = array("user_id" => $user_id,"token"=>$token);
        $method = "POST";
        $data = array('form_params' => $user_array);
        $response = $this->api->call_api($data,'api/user_detail',$method);
        if($response->response->httpCode == 400)
        {
            return Redirect::to('/')->send();
        }
        else
        {
            $this->user_details = $response->response->user_data[0];
            if($this->user_details->email == "")
            {
                Session::flash('message-failure',trans("messages.Please fill your personal details"));
                return Redirect::to('/profile')->send();
            }
            return $this->user_details;
        }
    }
    /* To show the wallet page */
    public function user_wallet()
    {
        //  dd('Ak');
		$post_data['user_id']  = Session::get('user_id');
		$post_data['token']    = Session::get('token');
		$post_data['language'] = $this->language;
		$method = "POST";
        $data   = array('form_params' => $post_data);
        $wallet_list = $this->api->call_api($data,'api/user_wallet',$method);

        if(isset($wallet_list->response->httpCode) && $wallet_list->response->httpCode == 200)
        {
            $wallet_list = $wallet_list->response;
        }
        else
        {
            Session::flash('message-failure', trans('messages.Kindly login'));
            return Redirect::to('/')->send();
        }
        SEOMeta::setTitle(Session::get('general')->site_name);
        SEOMeta::setDescription(Session::get('general')->site_name);
        SEOMeta::addKeyword(Session::get('general')->site_name);
        OpenGraph::setTitle(Session::get('general')->site_name);
        OpenGraph::setDescription(Session::get('general')->site_name);
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle(Session::get('general')->site_name);
        Twitter::setSite(Session::get('general')->site_name);
        return view('front.'.$this->theme.'.wallet.wallet')->with('wallet_list', $wallet_list);
	}
	/* To add the wallet */
	public function add_wallet(Request $data)
	{
		$post_data = $data->all();
		$user_id   = Session::get('user_id');
        $token     = Session::get('token');
		$language  = $this->language;
		$validator = Validator::make($post_data, array(
            'wallet_amount'=> 'required|numeric',
        ));
        $errors = $result = array();
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validation)->withInput();
        } else {
			$users = Users::find($user_id);
			$total_new_amount       = round($post_data['wallet_amount'])*100;
			$merchant_reference     = str_random(16);
            $lang                   = App::getLocale();
            $user_email             = $users->email;
			$payment_gateway_detail = User_wallets::get_payment_gateways($language);
			if(count($payment_gateway_detail) == 0)
			{
				Session::flash('message-failure', trans('messages.Invalid Merchant Details'));
				return Redirect::to('/wallet')->send();
			}
			$str = "qwertwqertaccess_code=".$payment_gateway_detail->merchant_key."amount=".$total_new_amount."command=PURCHASEcurrency=".$payment_gateway_detail->currency_code."customer_email=".$user_email."language=".$lang."merchant_identifier=".$payment_gateway_detail->account_id."merchant_reference=".$merchant_reference."order_description=add_walletqwertwqert";
            $signature = hash('sha256', $str);
            $requestParams = array(
            'access_code' => $payment_gateway_detail->merchant_key,
            'amount'      => $total_new_amount,
            'currency'    => $payment_gateway_detail->currency_code,
            'customer_email' => $user_email,
            'merchant_reference' => $merchant_reference,
            'language'       => $lang,
            'merchant_identifier' => $payment_gateway_detail->account_id,
            'signature'      => $signature,
            'command'        => 'PURCHASE',
            'order_description'   => 'add_wallet',
            );
            if($payment_gateway_detail->payment_mode==1){
                $redirectUrl = 'https://sbcheckout.payfort.com/FortAPI/paymentPage';
            } else {
                $redirectUrl = 'https://checkout.payfort.com/FortAPI/paymentPage';
            }
            echo "<html xmlns='http://www.w3.org/1999/xhtml'>\n<head></head>\n<body>\n";
            echo "<form action='$redirectUrl' method='post' name='frm'>\n";
            foreach ($requestParams as $a => $b) {
                echo "\t<input type='hidden' name='".htmlentities($a)."' value='".htmlentities($b)."'>\n";
            }
            echo "\t<script type='text/javascript'>\n";
            echo "\t\tdocument.frm.submit();\n";
            echo "\t</script>\n";
            echo "</form>\n</body>\n</html>";exit;
            return true;
		}
	}
}
