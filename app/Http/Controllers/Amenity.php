<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\amenities;
use App\Model\amenities_infos;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;


class Amenity extends Controller
{

                
                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function index()
                {
                    $id = Session::get('vendor_id');
                    if (!$id)
                        {
                            return redirect()->guest('vendors/login');
                        } 
                        if(!has_staff_permission('vendor/amenities'))
                        {
                        return view('errors.405');
                        }
                    else{
/*                        
                        $query = '"amenities_infos"."language_id" = (case when (select count(*) as totalcount from amenities_infos where amenities_infos.language_id = '.getAdminCurrentLang().' and amenities.id = amenities_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $Amenities=DB::table('amenities')
                         ->select(DB::raw('amenities.* ,amenities.id as cid'),'amenities_infos.*')
                        ->leftJoin('amenities_infos','amenities_infos.id','=','amenities.id')
                        ->whereRaw($query)
                        ->orderBy('amenity_name', 'asc')
                        ->get();
*/                        
                        return view('vendors.amenities.list')/*->with('amenities', $Amenities)*/;
                    }
                    
                }
                
                public function amenity_create()
                {

                    if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/amenities/create'))
                        {
                        return view('errors.405');
                        } else{
                        return view('vendors.amenities.create');
                    }
                }


                public function amenity_edit($id)
                {
                   // print_r($id);
                    //exit;
                   if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/amenities/edit'))
                        {
                        return view('errors.405');
                        }

                        $amenities = DB::table('amenities')
                        ->where('id','=',$id)->get();
                        // print_r(count($amenities));exit;
                         if (count($amenities) > 0)
                         {
                            $info = new Amenities_infos;
                            $Amenities = Amenities::find($id);
                            return view('vendors.amenities.edit')->with('data', $Amenities)->with('infomodel', $info);
                         }
                         else{
                            Session::flash('message', trans('messages.Invalid amenities details!'));
                           return Redirect::to('vendor/amenities');
                    }
                }
                
                
                public function amenity_store(Request $data)
                {
/*
print_r($_POST['amenity_type']);
exit;
*/
// echo "<pre>";
// print_r($data->all());
// exit;

                    $fields['amenity_type']  = Input::get('amenity_type');
                    $fields['amenty_image'] = Input::file('amenty_image');
                    $fields['amenity_icon'] = Input::file('amenity_icon');
                    $rules = array(
                        
                        'amenty_image' => 'required|mimes:png,jpg,jpeg,bmp|max:2024',
                        'amenity_type' => 'required',
                        //  'amenity_icon' => 'required',
                    );
                    $amenity_name = Input::get('amenity_name');
                    foreach ($amenity_name  as $key => $value) {
                        $fields['amenity_name'.$key] = $value;
                        //  $rules['amenity_name'.'1'] = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:amenities_infos,amenity_name';
                        $rules['amenity_name'.'1'] = 'required|regex:/(^[A-Za-z\s ]+$)+/';
                        
                    }
                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{

                            $Amenities = new Amenities;

                            $Amenities->created_date = date("Y-m-d H:i:s");          
                            $Amenities->modified_date = date("Y-m-d H:i:s");
                            $Amenities->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            $Amenities->amenity_icon = $_POST['amenity_icon'];

                            $Amenities->url_index =  $_POST['amenity_name'][1] ? str_slug($_POST['amenity_name'][1]): str_slug($_POST['amenity_name'][1]);

                            $vendor_id = Session::get('vendor_id');
                            
                            $Amenities->created_by = $vendor_id;


                            $sp = $kp = "";

                            if($_POST['amenity_type'][0] == 2)
                                    {
                                        $kp = isset($_POST['amenity_type'][0]) ? $_POST['amenity_type'][0]: 0;

                                        $Amenities->room_type = $kp;
                                        $Amenities->property = 0;

                                    }
                            else
                                    {
                                        $sp = isset($_POST['amenity_type'][0]) ? $_POST['amenity_type'][0]: 0;
                                        $kp = isset($_POST['amenity_type'][1]) ? $_POST['amenity_type'][1]: 0;

                                        $Amenities->property = $sp;
                                        $Amenities->room_type = $kp;
                                    }

                            $Amenities->save();

                            $imageName = strtolower($Amenities->id . '.' . $data->file('amenty_image')->getClientOriginalExtension());
                            $data->file('amenty_image')->move(
                                base_path() . '/public/assets/admin/base/images/amenities/', $imageName
                            );
                            $destinationPath2 = url('/assets/admin/base/images/amenities/'.$imageName.'');
                          
                            $size=getImageResize('VENDOR');
                            Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/amenities/list/'.$imageName)->destroy();
                            
                            
                            $Amenities->amenty_image=$imageName;

                            $Amenities->save();


                            $this->amenities_save_after($Amenities,$_POST);
                            Session::flash('message', trans('messages.Amenities has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('vendor/amenities');
                    }
                }
              
                
                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function amenity_update(Request $data, $id)
                {
                    $fields['amenty_image'] = Input::file('amenty_image');
                    $fields['amenity_icon'] = Input::file('amenity_icon');

                    $rules = array(

                        //  'amenity_type' => 'required',

                        //  'amenity_icon' => 'required',
                    );
                    $amenity_name = Input::get('amenity_name');
                    foreach ($amenity_name  as $key => $value) {
                        $fields['amenity_name'.$key] = $value;
                        //  $rules['amenity_name'.'1'] = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:amenities_infos,amenity_name,'.$id.',id';

                        $rules['amenity_name'.'1'] = 'required|regex:/(^[A-Za-z\s ]+$)+/';
                        
                    }


                    $validator = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        
                        try{
                            
                            $Amenities = Amenities::find($id); 

                            $vendor_id = Session::get('vendor_id');
                            $Amenities->property = Input::get('property');
                            $Amenities->room_type = Input::get('room_type');
                            $Amenities->amenity_icon = Input::get('amenity_icon');
                            $Amenities->created_by = $vendor_id;
                            //  $Amenities->amenity_type = Input::get('amenity_type');
                            $Amenities->url_index =  $_POST['amenity_name'][1] ? str_slug($_POST['amenity_name'][1]): str_slug($_POST['amenity_name'][1]);                            
                            $Amenities->modified_date = date("Y-m-d H:i:s");
                            $Amenities->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            $Amenities->save();

                            if(isset($_FILES['amenty_image']['name']) && $_FILES['amenty_image']['name']!=''){
                                        $imageName = strtolower($id . '.' . $data->file('amenty_image')->getClientOriginalExtension());
                                        $data->file('amenty_image')->move(
                                            base_path() . '/public/assets/admin/base/images/amenities/', $imageName
                                        );
                                        $destinationPath2 = url('/assets/admin/base/images/amenities/'.$imageName.'');
                                        $size=getImageResize('VENDOR');
                                        Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/amenities/list/'.$imageName)->destroy();

                             /*           Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/detail/'.$imageName)->destroy();
                                        Image::make( $destinationPath2 )->fit($size['THUMB_WIDTH'], $size['THUMB_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/'.$imageName)->destroy();
                                        Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/detail/'.$imageName)->destroy();
                                */
                                        $Amenities->amenty_image = $imageName;

                                        $Amenities->save();
                                    }

                            $this->amenities_save_after($Amenities,$_POST);
                            Session::flash('message', trans('messages.Amenities has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('vendor/amenities');
                    }
                }

               
               
                /**
                 * add,edit datas  saved in main table 
                 * after inserted in sub tabel.
                 *
                 * @param  int  $id
                 * @return Response
                 */
               public static function amenities_save_after($object,$post)
               {
                    $city = $object;
                    $post = $post;
                    if(isset($post['amenity_name'])){
                        $amenity_name = $post['amenity_name'];
                        try{                
                            $affected = DB::table('amenities_infos')->where('id', '=', $object->id)->delete();
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
                                if(isset($amenity_name[$lang->id]) && $amenity_name[$lang->id]!=""){
                                    $infomodel = new Amenities_infos;
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->id = $object->id; 
                                    $infomodel->amenity_name = $amenity_name[$lang->id];
                                    $infomodel->save();
                                }
                            }
                            }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                    }
               }
                
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function amenity_destroy($id)
                {
                    if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/amenities/delete'))
                        {
                        return view('errors.405');
                        }
                        $amenities = DB::table('amenities')
                        ->where('id','=',$id)->get();

                        if (count($amenities) > 0)
                        {
                            $data = Amenities::find($id);
                            $data->delete();
                            Session::flash('message', trans('messages.Amenities has been deleted successfully!'));
                            return Redirect::to('vendor/amenities');
                        }
                        else
                        {
                            Session::flash('message', trans('messages.Invalid amenities details!'));
                            return Redirect::to('vendor/amenities');
                        }
                        
                }

                
                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxAmenities()
                {
                 $vendor_id = Session::get('vendor_id');
                        //  print_r($id); exit;

                        if(Session::get('vendor_type') == 2 )
                            {
                                $vendor_id = Session::get('created_vendor_id');
                                //  print_r($id); exit;
                            }                                         
                    
                $query = '"amenities_infos"."language_id" = (case when (select count(*) as totalcount from amenities_infos where amenities_infos.language_id = '.getAdminCurrentLang().' and amenities.id = amenities_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                $Amenities = Amenities::Leftjoin('amenities_infos','amenities_infos.id','=','amenities.id')
                    ->select(DB::raw('amenities.* ,amenities.id as cid'),'amenities_infos.*')
                    ->where("amenities_infos.language_id","=",getAdminCurrentLang())
                    ->whereRaw($query)
                    ->where('created_by',$vendor_id)
                    ->orderBy('amenities.id', 'desc')
                    ->get();
                    return Datatables::of($Amenities)->addColumn('action', function ($Amenities) {
                        if(has_staff_permission('vendor/amenities/edit')){
                            return '<div class="btn-group"><a href="'.URL::to("vendor/amenities/edit/".$Amenities->cid).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("vendor/amenities/delete/".$Amenities->cid).'" class="delete-'.$Amenities->cid.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                    </ul>
                                </div><script type="text/javascript">
                                $( document ).ready(function() {
                                $(".delete-'.$Amenities->cid.'").on("click", function(){
                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });});</script>';
                        }
                    })

                        ->addColumn('amenity_type', function ($Amenities) {

//  print_r($Amenities->amenity_type);
//  exit;

                     $prop = $Amenities->property;
                     $rt = $Amenities->room_type;
                    //  $val = explode("," , $Amenities->amenity_type);
 
                                    //  $ck = rtrim($s,',');

                                                             
                                //  print_r($prop);
                                //  print_r($rt);
                                //  exit;
/*                                  
                            if(in_array(01, $val)):
                                $data = trans("messages.Property & Room Type");

                            elseif(in_array(0, $val)):
                                $data = trans("messages.Property");
                      

                            elseif(in_array(1, $val)):
                                $data = trans("messages.Room Type");
                       

                            endif;
print_r($data);
exit;
                            return $data;

*/ 
                            if($Amenities->property==1 && $Amenities->room_type==2):
                                $data = trans("messages.Property & Room Type");
                  
                            elseif($Amenities->property==1):
                                $data = trans("messages.Property");
                       
                            elseif($Amenities->room_type==2):
                                $data = trans("messages.Room Type");
                            endif;
                            return $data;
                           
                        })
                        ->addColumn('default_status', function ($Amenities) {
                            if($Amenities->default_status==0):
                                $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                            elseif($Amenities->default_status==1):
                                $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                            endif;
                            return $data;
                        })
                        ->addColumn('amenity_name', function ($Amenities) {
                                $data = ucfirst($Amenities->amenity_name);
                            return $data;
                        })
                        ->make(true);
                }

}
