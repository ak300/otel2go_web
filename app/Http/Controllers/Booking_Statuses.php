<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\booking_status;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;


class Booking_Statuses extends Controller
{

                
                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function index()
                {
                    // print_r('0100110');exit;
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                     // print_r('0100110');exit;
                        $booking_status=DB::table('booking_status')
                         ->select('booking_status.*')
                        ->orderBy('booking_status.name', 'asc')
                        ->get();
                        // echo '<pre>';
                        //  print_r($booking_status);exit;
                        return view('admin.booking_status.list')->with('booking_status', $booking_status);
                    
                    
                }
               
                public function booking_create()
                {
                    // print_r('expression');exit;
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                        // if(!has_staff_permission('vendor/booking_status/create'))
                        // {
                        // return view('errors.405');
                        // } else
                        {
                        return view('admin.booking_status.create');
                    }
                }

                   public function booking_store(Request $data)
                {
                      if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    // echo "<pre>";
                    // print_r($data->all());exit;

                     // print_r('expression');exit;
                    $fields['name'] = Input::get('name');
                    $fields['color_code'] = Input::get('color_code');
                    $rules = array(
                        'name' => 'required|unique:booking_status,name',
                        'color_code' => 'required',
                    );
                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } 
                    else {
                        try{

                            $Booking_Statuses = new Booking_Status;
                            $Booking_Statuses->name =  $_POST['name'];
                            $Booking_Statuses->color_code =  $_POST['color_code'];
                            // echo "<pre>";
                            // print_r($Booking_Statuses);exit;
                            $Booking_Statuses->save();

                            Session::flash('message', trans('messages.Booking Status has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('admin/booking_status');
                    }
                }

                public function booking_status_edit($id)
                {
                   // print_r($id);
                   //  exit;
                   if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                            $Booking_Statuses = Booking_Status::find($id);

                            return view('admin.booking_status.edit')->with('data', $Booking_Statuses);
                            if(!count($Booking_Statuses))           
                   {
                           Session::flash('message', trans('messages.Invalid Booking Status details!'));     
                           return Redirect::to('admin/booking_status');
                    }
                }

                                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function booking_status_update(Request $data, $id)
                {

                     $fields['name'] = Input::get('name');
                     $fields['color_code'] = Input::get('color_code');
                     $rules = array(
                        'name' => 'required|unique:booking_status,name,'.$id,
                        'color_code' => 'required',
                     );
                    $validator = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        
                        try{
                            $Booking_Statuses = Booking_Status::find($id);
                            $Booking_Statuses->name =  $_POST['name'];
                            $Booking_Statuses->color_code =  $_POST['color_code'];
                            // echo "<pre>";
                            // print_r($Booking_Statuses);exit;SSS
                            $Booking_Statuses->save();

                            Session::flash('message', trans('messages.Booking Status has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('admin/booking_status');
                    }
                }

                public function booking_status_destroy($id)
                {
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                        $Booking_Statuses = DB::Table('booking_status')
                                    ->where('id','=',$id)
                                    ->count();
                                       
                        if($Booking_Statuses != 0) 
                        {
                        $data = Booking_Status::find($id);
                        $data->delete();
                        Session::flash('message', trans('messages.Booking Status has been deleted successfully!'));
                        return Redirect::to('admin/booking_status');
                        }
                        else
                        {
                               Session::flash('message', trans('messages.Invalid Accomodation details!'));     
                               return Redirect::to('admin/booking_status');
                        }
                } 

                public function anyAjaxBookingStatus()
                {
                  if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                        $Booking_Statuses = DB::table('booking_status')->select('*')->orderBy('id', 'desc');
                        return Datatables::of($Booking_Statuses)->addColumn('action', function ($Booking_Statuses) {
                                return '<div class="btn-group"><a href="'.URL::to("admin/booking_status/edit/".$Booking_Statuses->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a></div>';
                            })
                            ->addColumn('name', function ($Booking_Statuses) {
                                    $data = $Booking_Statuses->name;
                                return $data;
                            })
                            ->make(true);
                    }

}