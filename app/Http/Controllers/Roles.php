<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\role;
use App\Model\customers_view;
use App\Model\customers;
use App\Model\api_account;
use App\Model\roles_users;
use App\Model\api_resources;
use App\Model\role_tasks;
use App\Model\permission_menu;
use App\Http\Requests;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;
use App;
use App\Model\roles_permission;

class Roles extends Controller
{

                protected $_taskslist = array();
                public function __construct()
                {
                    $this->site_name = isset(getAppConfig()->site_name)?ucfirst(getAppConfig()->site_name):'';
                    SEOMeta::setTitle($this->site_name);
                    SEOMeta::setDescription($this->site_name);
                    SEOMeta::addKeyword($this->site_name);
                    OpenGraph::setTitle($this->site_name);
                    OpenGraph::setDescription($this->site_name);
                    OpenGraph::setUrl($this->site_name);
                    Twitter::setTitle($this->site_name);
                    Twitter::setSite('@'.$this->site_name);
                    App::setLocale('en');
                }
                /**
                 * Display a listing of the blogs.
                 *
                 * @return Response
                 */
                public function index()
                {
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    else{
                        if(!has_permission('system/permission')){
                            return view('errors.404');
                        }
                        return view('admin.roles.list');
                    }
                }
                /**
                 * Display a listing of the blogs.
                 *
                 * @return Response
                 */
                public function users()
                {
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    else{
                        if(!has_permission('permission/users')){
                            return view('errors.404');
                        }
                        // load the view and list the blogs
                        return view('admin.roles.users.list');
                    }
                }
                
                
                /**
                 * Show the form for creating a new blog.
                 *
                 * @return Response
                 */
                public function create()
                {
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    else{
                        if(!has_permission('system/permission/create'))
                        {
                            return view('errors.404');
                        }

                            $module=1;

                        $taskresource = $this->getPermissionHtmlTree($module);
                        $tasks = $this->getTaskList();
                        return view('admin.roles.create')->with('taskresource', $taskresource);
                    }
                }

                /**
                 * Show the form for creating a new blog.
                 *
                 * @return Response
                 */
                public function user_create()
                {
                    if (Auth::guest()){
                        return redirect()->guest('admin/login');
                    }else{

                        if(!has_permission('permission/usercreate')){
                            return view('errors.404');
                        }
                        $roles=DB::table('user_roles')
                                    ->select('user_roles.*')
                                    ->orderBy('user_roles.role_name', 'asc')
                                    ->where('created_by',Auth::id())
                                    ->get();

                        $users=DB::table('customers_view')
                                    ->select('customers_view.email','customers_view.id')
                                    ->orderBy('customers_view.email', 'asc')
                                    ->where('customers_view.user_type',"=",2)
                                    ->where('customers_view.status',"=",1)
                                    ->where('customers_view.active_status',"=",'A')
                                    ->where('customers_view.is_verified',"=",1)
                                    ->get();
                        return view('admin.roles.users.create')->with('roles', $roles)->with('users', $users);
                    }
                }

                /**
                 * Store a newly created blog in storage.
                 *
                 * @return Response
                 */
                public function user_store(Request $data)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                         
                    if(!has_permission('permission/userstore')){
                        return view('errors.404');
                    }
                    $fields['role_id'] = Input::get('role_id');
                    $fields['user_id'] = Input::get('user_id');
                    $rules = array(
                        'role_id' => 'required',
                        'user_id' => 'required',
                    );
                    $validator = Validator::make($fields, $rules);
                    // process the validation
                    if ($validator->fails()) {
                            return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        $roles=DB::table('roles_users')
                                 ->select('roles_users.role_id','roles_users.user_id')
                                 ->where('roles_users.role_id',"=",$fields['role_id'])
                                 ->where('roles_users.user_id',"=",$fields['user_id'])
                                 ->get();
                        if(count($roles)){
                            $validator->errors()->add('role_id', trans('messages.This role already assigned to this uesr'));
                            return Redirect::back()->withErrors($validator)->withInput();
                        }
                        // store
                        $roleusers = new Roles_users;
                        $roleusers->role_id =  $_POST['role_id'];
                        $roleusers->user_id =  $_POST['user_id'];
                        $roleusers->created_date = date("Y-m-d H:i:s");
                        $roleusers->created_by = Auth::id();
                        $roleusers->save();
                        // redirect
                        Session::flash('message', trans('messages.Role has been assgined to user successfully'));
                        return Redirect::to('permission/users');
                    }
                }
                
                /**
                 * Display the specified blog.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function show($id)
                {
                    if (Auth::guest()){
                        return redirect()->guest('admin/login');
                    }else{
                        // get the blog
                        $blog = Blogs::find($id);
                        $info = new blog_infos;
                        $type = 3;
                        $query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = '.getAdminCurrentLang().' and categories.id = categories_infos.category_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                            $category=DB::table('categories')
                                        ->select('categories.*','categories_infos.*')
                                        ->leftJoin('categories_infos','categories_infos.category_id','=','categories.id')
                                        ->whereRaw($query)
                                        ->where("categories.category_type","=",$type)
                                        ->orderBy('categories_infos.category_name', 'asc')
                                        ->get();
                        // show the view and pass the blog to it
                        return view('blog.show')->with('data', $blog)->with('category', $category)->with('infomodel', $info);
                    }
                }
                
                /**
                 * Show the form for editing the specified blog.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function edit($id)
                {
                    if (Auth::guest()){
                        return redirect()->guest('admin/login');
                    }else{
                        if(!has_permission('system/permission/edit/{id}')){
                            return view('errors.404');
                        }

                        $module=1;

                        $taskresource = $this->getPermissionHtmlTree($module,$id);
                        $tasks = $this->getTaskList();

                        $roles = Role::find($id);
                        if(!count($roles)){
                            Session::flash('message', 'Invalid Role');
                            return Redirect::to('system/permission');
                         }

                        $roles=DB::table('user_roles')
                         ->select('user_roles.*','roles_permission.*')
                        ->leftJoin('roles_permission','roles_permission.role_id','=','user_roles.id')
                        ->where('created_by',Auth::id())
                        ->where("user_roles.id","=",$id)
                        ->get();
                        return view('admin.roles.edit')->with('data', $roles[0])->with('taskresource', $taskresource);
                    }
                }

                /**
                 * Show the form for editing the specified blog.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function user_edit($id)
                {
                    // print_r(Auth::id());exit;

                    if (Auth::guest()){
                        return redirect()->guest('admin/login');
                    }else{
                        if(!has_permission('permission/users/edit/{id}')){
                            return view('errors.404');
                        }
                        // get the blog
                        $roles_users = Roles_users::find($id);
                        if(!count($roles_users))
                        {
                            Session::flash('message', 'Invalid Role');
                            return Redirect::to('permission/users');
                        }
                        $roles = DB::table('user_roles')
                                ->select('user_roles.*')
                                ->where('created_by',Auth::id())
                                ->orderBy('user_roles.role_name', 'asc')
                                ->get();
                        $users = DB::table('customers_view')
                                ->select('customers_view.email','customers_view.id')
                                ->orderBy('customers_view.email', 'asc')
                                ->where('customers_view.user_type',"=",2)
                                ->where('customers_view.status',"=",1)
                                ->where('customers_view.active_status',"=",'A')
                                ->where('customers_view.is_verified',"=",1)
                                ->get();
                        $roleusers=DB::table('roles_users')
                                ->select('roles_users.ruid','roles_users.user_id','user_roles.role_name','customers_view.email','customers_view.id','roles_users.role_id')
                                ->leftJoin("user_roles",'user_roles.id','=','roles_users.role_id')
                                ->leftJoin('customers_view','customers_view.id','=','roles_users.user_id')
                                ->orderBy('roles_users.ruid', 'desc')
                                ->where("roles_users.ruid","=",$id)
                                ->get();
                                // print_r($roleusers);exit;
                        return view('admin.roles.users.edit')->with('data', $roleusers[0])->with('roles', $roles)->with('users', $users);
                    }
                }
                
                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function user_update(Request $data, $id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                         
                    if(!has_permission('usersupdate/{id}')){
                            return view('errors.404');
                    }
                    $fields['role_id'] = Input::get('role_id');
                    $fields['user_id'] = Input::get('user_id');
                    $rules = array(
                        'role_id' => 'required',
                        'user_id' => 'required',
                    );    
                    $validator = Validator::make($fields, $rules);
                    // process the validation
                    if ($validator->fails()) {
                            return Redirect::back()->withErrors($validator)->withInput();
                    } else {

                        $roles=DB::table('roles_users')
                                 ->select('roles_users.role_id','roles_users.user_id')
                                 ->where('roles_users.role_id',"=",$fields['role_id'])
                                 ->where('roles_users.user_id',"=",$fields['user_id'])
                                 ->where('roles_users.ruid',"!=",$id)
                                 ->get();
                        
                        if(count($roles)){
                            $validator->errors()->add('role_id', trans('messages.This role already assigned to this uesr'));
                            return Redirect::back()->withErrors($validator)->withInput();
                        }
                        // store
                        $roles_users = Roles_users::find($id);
                        $roles_users->role_id =  $_POST['role_id'];
                        $roles_users->user_id =  $_POST['user_id'];
                        $roles_users->updated_date = date("Y-m-d H:i:s");
                        $roles_users->save();
                        // redirect
                        Session::flash('message', trans('messages.Role users has been updated successfully'));
                        return Redirect::to('permission/users');
                    }

                }
                
                /**
                 * Delete the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function destroy($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                         
                    if(!has_permission('system/permission/delete/{id}')){
                        return view('errors.404');
                    }
                    $data = Role::find($id);
                    $data->delete();
                    Session::flash('message', trans('messages.Role has been deleted successfully!'));
                    return Redirect::to('system/permission');
                }

                /**
                 * Delete the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function users_destroy($id)
                {
                    if(Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }                         
                    if(!has_permission('permission/users/delete/{id}'))
                    {
                        return view('errors.404');
                    }
                    $data = Roles_users::find($id);
                    $data->delete();
                    Session::flash('message', trans('messages.Role uesr has been deleted successfully!'));
                    return Redirect::to('permission/users');
                }
                
                
                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxrolelist(Request $request)
                {
                    if(Auth::guest())
                    {
                        return 404;
                    }                         

                    $roles = DB::table('user_roles')
                                ->select('user_roles.*')
                                ->where('user_roles.created_by','=',Auth::id())
                                ->orderBy('user_roles.id', 'desc');
                    return Datatables::of($roles)->addColumn('action', function ($roles) {
                        if(has_permission('system/permission/edit/{id}'))
                        {
                            $html ='<div class="btn-group"><a href="'.URL::to("system/permission/edit/".$roles->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("system/permission/delete/".$roles->id).'" class="delete-'.$roles->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                </ul>
                            </div>
                            <script type="text/javascript">
                                $( document ).ready(function() {
                                    $(".delete-'.$roles->id.'").on("click", function(){
                                        return confirm("'.trans("messages.Are you sure want to delete?").'");
                                    });
                                });
                            </script>';
                            return $html;
                        }
                    })
                    ->addColumn('active_status', function ($roles) {
                        if($roles->active_status==0):
                            $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                        elseif($roles->active_status==1):
                            $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                        endif;
                        return $data;
                    })
                    ->addColumn('role_name', function ($roles) {
                        return ucfirst($roles->role_name);
                    })
                    ->make(true);
                }

                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxroleuesrlist(Request $request)
                {
                    if(Auth::guest())
                    {
                        return 404;
                    }                     
                    $roleusers=DB::table('roles_users')
                                ->select('roles_users.ruid','roles_users.user_id','user_roles.role_name','customers_view.email','customers_view.id')
                                ->leftJoin("user_roles",'user_roles.id','=','roles_users.role_id')
                                ->leftJoin('customers_view','customers_view.id','=','roles_users.user_id')
                                ->where('roles_users.created_by','=',Auth::id())
                                ->orderBy('roles_users.ruid', 'desc');
                    return Datatables::of($roleusers)->addColumn('action', function ($roleusers) {
                        if(has_permission('permission/users/edit/{id}'))
                        {
                            $html ='<div class="btn-group"><a href="'.URL::to("permission/users/edit/".$roleusers->ruid).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("permission/users/delete/".$roleusers->ruid).'" class="delete-'.$roleusers->ruid.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                </ul>
                            </div>
                            <script type="text/javascript">
                                $( document ).ready(function() {
                                    $(".delete-'.$roleusers->ruid.'").on("click", function(){
                                        return confirm("'.trans("messages.Are you sure want to delete?").'");
                                    });
                                });
                            </script>'; 
                            return $html;
                        }
                    })
                    ->make(true);
                }

                public function tasks()
                {
                    return tasks();
                }

                public function optionTasks($tasks = null,$path = '', $level=0,$id='')
                {
                    if(is_null($tasks))
                    $tasks = $this->tasks();
                    $arr   = array();
                    $sortOrder = 0;
                    foreach ($tasks as $taskname =>  $task)
                    {
                        $options = array();
                        $options['id'] = $path.$taskname ;
                        if(in_array($options['id'],$this->getRoleTasks($id)))
                        {
                            $options['checked'] = true;
                        }
                        $options['level'] = $level;
                        $options['text']  = isset($task['title']) ? $task['title'] : '';
                        $options['task_note']    =  isset($task['task_note']) ? $task['task_note'] : false;
                        $options['sort_order']   = isset($task['sort']) ? (int)$task['sort'] : $sortOrder;
                        $options['apiresources'] = isset($task['apiresources']) ? $task['apiresources'] : array();
                        $options['index']        = false;
                        if(isset($task['task_index']))
                        {
                            $this->_taskslist[$options['id']] = $task['task_index'];
                            $options['index'] = $task['task_index'];
                        }
                        if(isset($task['children']))
                        {
                            $options['children'] = $this->optionTasks($task['children'], $path.$taskname."/",$level+1);
                        }
                        $arr[] = $options;
                    }

                    while (list($key, $value) = each($arr))
                    {
                        $last = $key;
                    }
                    if (isset($last))
                    {
                        $arr[$last]['last'] = true;
                    }
                    return $arr;
                }

                public function getRoleTasks($id='')
                {
                    if($id){
                        $roletasks=DB::table('role_tasks')
                                    ->select('role_tasks.task_definition')
                                    ->where('role_tasks.role_id', $id)
                                    ->get();
                        $tasks_def=array();
                        if(count($roletasks)){
                            foreach($roletasks as $key =>$value){
                                $tasks_def[]=$value->task_definition;
                            }
                        }
                        return $tasks_def;
                    }else { 
                        return array();
                    }
                }

                public function getApiResources($id='')
                {
                    if($id){
                        $resource_def=array();
                        $apiaccount=DB::table('api_account')
                                      ->select('api_account.account_id')
                                      ->where('api_account.role_id', $id)
                                      ->get();
                        if(count($apiaccount)){
                            $apiresources=DB::table('api_resources')
                            ->select('api_resources.method','api_resources.resources')
                            ->where('api_resources.account_id', $apiaccount[0]->account_id)
                            ->get();
                            if(count($apiresources)){
                                foreach($apiresources as $key =>$value){
                                    $resource_def[$value->resources][]=$value->method;
                                }
                            }
                        }
                        return $resource_def;
                    }else { 
                        return array();
                    }
                }

                public function getHtmlTree($nodes,$id='')
                {
                    $html = '';
                    $db = $this->getApiResources($id);
                    
                    $task_list=array();
                    foreach($nodes as $node) {
                        if(isset($node['children']) && !empty($node['children'])) { 
                            $html .= '<h5 class="lg-title mb5">'.$node['text'].'</h5>';
                            if(isset($node['task_note']) && $node['task_note']) {
                                $html .='<p class="mb10">'.$node['task_note'].'</p>';
                            } 
                            $html .= '<table class="table table-striped table-bordered dataTable roles" id="'.str_replace("/","_",$node['id']).'">';
                            $html .= '<col width="40%" />
                                        <col width="15%" />
                                        <col width="40%" />
                                        <thead>
                                            <tr>
                                                <th>'.trans('messages.Task Description').'</th>
                                                <th>'.trans('messages.Website Access').'&nbsp;<input type="checkbox" class="parentcheckbox" value="'.str_replace("/","_",$node['id']).'" /></th>
                                                <th>'.trans('messages.Api Resources').'</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                            $html.= $this->getHtmlTree($node['children'],$id);
                            $html.="</tbody></table>";
                        } else {

                            $html.="<tr>";
                            $selected = '';
                            if(in_array($node['id'],$this->getRoleTasks($id))) {
                                $selected = "checked"; 
                            }
                            $html.= '<td><div><label for="'.$node['id'].'">'.$node['text'];
                            if(isset($node['task_note']) && $node['task_note']) {
                                $html .='<p>'.$node['task_note'].'</p>';
                            }
                            $html .='</label></div></td>';
                            $html .='<td><input type="checkbox" class="toggle" name="nodes[]" '.$selected.' data-size="small" data-on-text='.trans('messages.Yes').' data-off-text='. trans('messages.No').' data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="'.$node['id'].'" id="'.$node['id'].'" /></td>';
                            if(isset($node['apiresources']) && count($node['apiresources'])) { 
                                $html .='<td><table border="0" width="100%"><tr>';
                                $html .= '<td></td>'; 
                                $resources = array('GET','POST','PUT','DELETE');
                                foreach($resources as $methods) { 
                                       $html .= '<td>'.$methods.'</td>'; 
                                }
                                $html .='</tr>';
                                
                                foreach($node['apiresources'] as $indexx => $res) {
                                    $index = array_get($res,'index','');
                                    
                                    //$dbresources = array_get($db,$index,array());
                                    $dbresources = $db;
                                    $resource =  array_get($res,'resource',array());
                                    $title =  array_get($res,'title',array());
                                    $html .= '<tr>';
                                    $html .= '<td>'.$title.'</td>';
                                    
                                    foreach($resources as $methods) {
                                        if(in_array($methods,$resource)){
                                            
                                            $selected = '';
                                             
                                            if(count($dbresources)){
                                                foreach ($dbresources as $keys => $item) {
                                                    if($keys==$indexx){
                                                        if(in_array($methods,$dbresources[$keys])) {
                                                            $selected = "checked='checked'";
                                                        }
                                                    }
                                                }
                                            }
                                             $html .= '<td><input type="checkbox" name="apires[resource]['.$index.']['.$methods.']"  '.$selected.' value="1"/></td>';
                                             $html .='<input type="hidden" name="apires[resource_index][]" value='.$indexx.'>';
                                        } else {
                                            $html .= '<td><input type="checkbox" name="apires[resource]['.$index.']['.$methods.']" disabled="disabled" value="1" /></td>';
                                            $html .='<input type="hidden" name="apires[resource_index][]" value='.$indexx.'>';
                                        }
                                    } 
                                    $html .='</tr>';
                                } 
                                $html .='</table></td>';
                            } else {
                                  $html .='<td>-</td>';
                            }
                            $html.="</tr>";
                        }

                    }
                    return $html;
                }

                public function getTaskList()
                {
                    return $this->_taskslist;
                }


                public function getPermissionMenu($module)
                {
                     $permissionMenu=DB::table('permission_menu')
                        ->select('permission_menu.*')
                        ->orderBy('permission_menu.id', 'asc')
                        ->where('module','=',1)
                        ->get();

                        return $permissionMenu;
                    
                }
                public function getPermissionMenuNodes($module)
                {
                     $getPermissionMenuNodes=DB::table('permission_menu')
                        ->select('permission_menu.menu_group','permission_menu.controller_group',  DB::raw('count( permission_menu.controller_group ) as controller_count'))
                        ->groupBy('permission_menu.menu_group','permission_menu.controller_group')
                        ->where('module','=',1)
                        ->get();

                        return $getPermissionMenuNodes;
                    
                }

                public function getPermissionMenus($module)
                {
                     $getPermissionMenuNodes=DB::table('permission_menu')
                        ->select('permission_menu.menu_group')
                        ->where('module','=',1)
                        ->get();

                        return $getPermissionMenuNodes;
                    
                }

                public function getPermissionHtmlTree($module,$id='')
                {

                   
                    $html = '';
                    $nodes = $this->getPermissionMenu($module);
                    $menu = $this->getPermissionMenuNodes($module);
                    $menuNodes=$this->getPermissionMenus($module);
                    $task_list=array();

                    foreach($menu as $item) {
                        if(isset($item->controller_count) && count($item->controller_count)>0) { 
                            $html .= '<h5 class="lg-title mb5">'.$item->menu_group.'</h5>';
                            if(isset($item->task_note) && $item->task_note) {
                                $html .='<p class="mb10">'.$item->task_note.'</p>';
                            } 
                            $html .= '<table class="table table-striped table-bordered dataTable roles" id="'.str_replace("/","_",$item->controller_group).'">';
                            $html .= '<col width="40%" />
                                        <col width="15%" />
                                        <col width="40%" />
                                        <thead>
                                            <tr>
                                                <th>'.$item->controller_group.'</th>
                                                <th>'.trans('messages.Website Access').'&nbsp;<input type="checkbox" class="parentcheckbox" value="'.str_replace("/","_",$item->controller_group).'" /></th>
                                                <th>'.trans('messages.Api Resources').'</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                        foreach ($nodes as $node) {
                                            # code...
                                            if($item->menu_group==$node->menu_group && $item->controller_group==$node->controller_group)
                                            {

                                                $html.= $this->getChildHtmlTree($node,$id);

                                            }
                                        }
                            $html.="</tbody></table>";
                        } 

                    }
                
                    return $html;
                }


                public function getRolePermissions($id)
                {


                    if($id)
                    {
                        $roletasks=DB::table('roles_permission')
                                    ->select('roles_permission.menu_id')
                                    ->where('roles_permission.role_id', $id)
                                    ->get();
                        
                        $tasks_def=array();
                        if(count($roletasks)){
                            foreach($roletasks as $key =>$value){
                                $tasks_def[]=$value->menu_id;
                            }
                        }
                        
                        return $tasks_def;
                    }else { 
                        return array();
                    }

                }


                public function getChildHtmlTree($item,$id='')
                {
                    
                    if(!$id=="")
                        {
                            $selected = '';

                            if(in_array($item->id,$this->getRolePermissions($id))) {
                                 $selected = "checked"; 
                             }


                        }
                    else
                        {
                            $selected = '';   
                        }

                            $html="<tr>";
                            $html.= '<td><div><label for="'.$item->id.'">'.$item->menu_key;
                            if(isset($item->task_note) && $item->task_note) {
                                $html .='<p>'.$item->task_note.'</p>';
                            }
                            $html .='</label></div></td>';
                            $html .='<td><input type="checkbox" class="toggle" name="nodes[]" '.$selected.' data-size="small" data-on-text='.trans('messages.Yes').' data-off-text='. trans('messages.No').' data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="'.$item->id.'" id="'.$item->id.'" /></td>';
                                  $html .='<td>-</td>';
                            
                            $html.="</tr>";

                            return $html;
                        
                }

}
