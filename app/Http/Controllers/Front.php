<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use DB;
use App\Model\contactus;
use App\Model\contactuses;
use App\Model\users;
use App\Model\settings;
use App\Model\settings_infos;
use App\Model\emailsettings;
use App\Model\cms;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use MetaTag;
use Mail;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Support\Facades\Input;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ServerErrorResponseException;
use App\Model\api;
use App\Model\Api_model;
use Socialite;
use App\Model\categories_infos;
use App\Model\weight_classes_infos;
use App\Model\cart_info;
use App\Model\vendors;
use App\Model\vendors_infos;
use App\Model\outlets;
use App\Model\outlet_infos;
use Illuminate\Pagination\Paginator;
use App\Model\subscription_transaction as Subscriptiontransaction;
use App\Model\subscriptions as Subscriptionrenewal;
use App\Model\subscription_payment;
use App\Model\admin_customers as Admincustomer;


class Front extends Controller
{

    const USERS_SIGNUP_EMAIL_TEMPLATE = 1;
    const USERS_WELCOME_EMAIL_TEMPLATE = 3;
    const COMMON_MAIL_TEMPLATE = 8;
    const ADMIN_MAIL_TEMPLATE_CONTACT = 23;
    const USER_MAIL_TEMPLATE_CONTACT = 15;
    const VENDORS_REGISTER_EMAIL_TEMPLATE = 4;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
         //~ print_r(getAppConfig()->site_name);exit;
        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => url('/'),
            // You can set any number of default request options.
            'timeout'  => 3000.0,
        ]);
        $this->theme = Session::get("general")->theme;
        $this->api = New Api;
    }

   
    public function check_login()
    { 
        //  print_r("Ak");exit;
        $user_details = array();
        $user_id = Session::get('user_id');
        $token = Session::get('token');
        if($user_id !="")
        {
            $user_array = array("user_id" => $user_id,"token"=>$token);
            $method = "POST";
            $data = array('form_params' => $user_array); 
            $response = $this->api->call_api($data,'api/user_detail',$method);
            $user_details = $response->response->user_data[0];
        }
        return $user_details;
    }
                
                
    public function home()
    {
        //$user_details = $this->check_login();

        $site_settings = getSettingsLists();

        //  dd($site_settings);

        SEOMeta::setTitle($site_settings->meta_title);
        SEOMeta::setDescription($site_settings->site_description);
        SEOMeta::addKeyword($site_settings->meta_keywords);
        OpenGraph::setTitle($site_settings->meta_title);
        OpenGraph::setDescription($site_settings->site_description);
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle($site_settings->meta_title);
        Twitter::setSite($site_settings->site_description);
        
        $method ='post';
        $data = array();
        $response = $this->api->call_api($data,'api/city_details',$method);

        //  dd($response);

        //  dd('Ak');
        
        $city = $response->response->cities;
        $category = $response->response->categories;
        $banners = $response->response->banners;

        return view('front.'.$this->theme.'.home')->with('city_details',$city)
                                                  ->with('categories',$category)
                                                  ->with('banners',$banners);
    }

    public function login_user(Request $data)
    {
        $api = New Api;
        $post_array = $data->all();
        $user_array = array("email" => $post_array['email'], "password" => $post_array['password'], "login_type" => 1,"language"=>getCurrentLang());
        $method = "POST";
        $data = array('form_params' => $user_array);
        $response = $api->call_api($data,'api/login_user',$method);
        // dd($response);
        if($response->response->httpCode =='200')
        {
            Session::put('user_id', $response->response->user_id);
            Session::put('email', $response->response->email);
            Session::put('first_name', $response->response->first_name);
            Session::put('last_name', $response->response->last_name);
            Session::put('name', $response->response->name);
            Session::put('social_title', $response->response->social_title);
            Session::put('profile_image', $response->response->image);
            Session::put('token', $response->response->token);
            return response()->json($response->response);
        }
        else
        {
            return response()->json($response->response);
        }
    }
    
    public function signup_user(Request $data)
    {
        $api = New Api;
        $user_array = $data->all();
        //  print_r($user_array);exit;
        $url = url("/api/signup_user");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);

        //dd($response);

        //  print_r($response);exit;
       
        if($response->response->httpCode =='200')
        {  
            return response()->json($response->response);
        }       
        else
        {
            return response()->json($response->response);
        } 
    }

    //  STORE GUEST [PAY NOW]
    
    public function signup_guest_now(Request $data)
    {
       
        $api = New Api;
        $user_array = $data->all();
        $url = url("/api/signup_guest_now");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);

        //  print_r($response);exit;
       
        if($response->response->httpCode =='200')
        {  
         
            return response()->json($response->response);
        }
        else
        {
            return response()->json($response->response);
        } 
    }


  //  CREATE SHA-HASH SIGNATURE
  
    public function iPay88_signature($source)
    {
      return hash('sha256', $source);
    }

    public function payment_details_page(Request $data){


            $post_data = $data->all();

            if(isset($post_data['new_user_id']))
            {
                  //    return 1;

                $user_id = $post_data['new_user_id'];
            }
            else
            {
                //  return 2;
                $user_id = $post_data['user_id'];
            }

           //   return $user_id;
            
            $outlet_id = $post_data['outlet_id'];
            $room_type_id = $post_data['room_type_id'];
            $check_in = $post_data['check_in'];
            $check_out = $post_data['check_out'];
            $room_count = $post_data['room_count'];
            $guest_count = $post_data['guest_count'];
            $no_of_nights = $post_data['no_of_nights'];
            $coupon_id = $post_data['coupon_id'];
            $coupon_amount = $post_data['coupon_amount'];
            $coupon_type = $post_data['coupon_type'];
            $original_price = $post_data['original_price'];
            $total_cost = $post_data['total_cost'];
            $vendor_id = $post_data['vendor_id'];


            
            $user = Admincustomer::find($user_id);

            //  return $user;

            $user_id = $user->id;
            $user_name =  $user->firstname;
            $user_email =  $user->email;
            $user_mobile =  $user->country_code."".$user->mobile_number;

            //  return array($user_mobile;

            //  PAYMENT GATEWAY DETAILS

            $payment_gateway_ipay88 = Subscriptionrenewal::get_payment_gateways(1,28);

            //  return $payment_gateway_ipay88;

            $payment_amount_price = $original_price;
            $payment_amount = $total_cost;
            $payment_title = 'Booking';
            $merchant_key = $payment_gateway_ipay88[0]->merchant_key;
            $merchant_code = $payment_gateway_ipay88[0]->account_id;
            $reference_no = getRandomNumber(12);

            //  $amount = str_replace('.', '', $payment_amount);  

            $amount = round($payment_amount);

            //  return $r;

            $currency = $payment_gateway_ipay88[0]->currency_code;
            $sign_src = $merchant_key.$merchant_code.$reference_no.$amount.$currency;

            //  return array($sign_src);

            $signature = $this->iPay88_signature($sign_src);
            //  return array($signature);

            if($payment_gateway_ipay88[0]->payment_mode==1){
              $action_url = 'https://www.mobile88.com/ePayment/entry.asp';
            } else {
              $action_url = 'https://www.mobile88.com/ePayment/entry.asp';
            }
            $response_url = env("APP_URL")."/api/booking_payment_response";
            $backend_url = env('APP_URL').'/api/response_booking_payment';
            //  echo $action_url; exit;

            //  return array($action_url);
            $html_pay = "<html xmlns='http://www.w3.org/1999/xhtml'>\n
            <head></head>\n
            <body>\n";
            $html_pay .=  "<form action='$action_url' method='post' name='frm'>\n";
            $html_pay .=   '<input type="hidden" name="MerchantCode" value="'.$merchant_code.'">';
            $html_pay .=   '<input type="hidden" name="RefNo" value="'.$reference_no.'">';
            $html_pay .=   '<input type="hidden" name="Amount" value="'.$amount.'">';
            $html_pay .=   '<input type="hidden" name="Currency" value="'.$currency.'">';
            $html_pay .=   '<input type="hidden" name="Lang" value="UTF-8">';
            $html_pay .=   '<input type="hidden" name="SignatureType" value="SHA256">';
            $html_pay .=   '<input type="hidden" name="Signature" value="'.$signature.'">';
            $html_pay .=   '<input type="hidden" name="ProdDesc" value="'.$payment_title.'">';
            $html_pay .=   '<input type="hidden" name="UserName" value="'.$user_name.'">';
            $html_pay .=   '<input type="hidden" name="UserEmail" value="'.$user_email.'">';
            $html_pay .=   '<input type="hidden" name="UserContact" value="'.$user_mobile.'">';
            $html_pay .=   '<input type="hidden" name="Remark" value="'.$user_id.'">';
            $html_pay .=   '<input type="hidden" name="ResponseURL" value="'.$response_url.'">';
            $html_pay .=   '<input type="hidden" name="BackendURL" value="'.$backend_url.'">';
            $html_pay .=   "</form>\n";
            $html_pay .=   "\t<script type='text/javascript'>\n";
            $html_pay .=   "\t\tdocument.frm.submit();\n";
            $html_pay .=   "\t</script>\n";
            $html_pay .= "</body>\n</html>";  
            echo $html_pay;exit;          
       
    }

    //  STORE GUEST [PAY AT HOTEL]

    public function signup_guest(Request $data)
    {
       
        $api = New Api;
        $user_array = $data->all();
        $url = url("/api/signup_guest");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);

        //  print_r($response);exit;
       
        if($response->response->httpCode =='200')
        {  
            if(isset($response->response->last_id))
            {
                //  NEW USER INSERT USER DETAILS SET INTO SESSION

                Session::put('user_id',$response->response->last_id);
                Session::put('user_name',$response->response->user_name);
                Session::put('user_email',$response->response->user_email);
                Session::put('user_mobile',$response->response->user_mobile);
                Session::put('country_code',$response->response->country_code);
                Session::put('token', $response->response->token);
                //  Session::put('profile_image', $response->response->image);
            }
            
            return response()->json($response->response);
        }
        else
        {
            return response()->json($response->response);
        } 
    }

    //  VERIFY GUEST OTP FOR BOOKING 

    public function verify_guest_otp(Request $data){

        $api = New Api;
        $user_array = $data->all();
        $url = url("/api/verify_guest_otp");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);

        //  print_r($response);exit;
        if($response->response->httpCode =='200')
        {  
            return response()->json($response->response);
        }
        else
        {
            return response()->json($response->response);
        } 
    }

    public function verify_user(Request $data){


        $api = New Api;
        $user_array = $data->all();
        $url = url("/api/verify_user");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);

        //   print_r($response);exit;
      
        if($response->response->httpCode =='200')
        {  
            return response()->json($response->response);
        }
        else
        {
            return response()->json($response->response);
        } 
    }

    public function resend_otp(Request $data){
        $api = New Api;
        $user_array = $data->all();
        // print_r($user_array);
        // exit;
        $url = url("/api/resend_otp");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);
        
        //   print_r($response); exit;
        if($response->response->httpCode =='200')
        {  
            return response()->json($response->response);
        }
        else
        {
            return response()->json($response->response);
        } 
    }

    public function update_user(Request $data)
    {
        $api = New Api;
        $user_array = $data->all();
        $url = url("/api/update_user");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);

        //  print_r($response);exit;
        if($response->response->httpCode =='200')
        {  
            Session::put('user_id',$response->response->user_id);
            Session::put('user_name',$response->response->user_name);
            Session::put('user_email',$response->response->user_email);
            Session::put('user_mobile',$response->response->user_mobile);
            Session::put('country_code',$response->response->country_code);
            Session::put('token', $response->response->token);
                //  Session::put('profile_image', $response->response->image);
                      
            return response()->json($response->response);
        }
        else
        {
            return response()->json($response->response);
        } 
    }

    public function logout()
    {
        if(Session::has('user_id')) {

            //  dd('AK');
            Session::flush(); 
            Session::flash('logout-message', trans("messages.Logout successfully"));
            return Redirect::to('/book-now')->send();
        }
        else
        {   //  IF SESSION CLEARED AUTOMATICALLY, IT REDIRECTS TO BOOK-NOW PAGE
            return Redirect::to('/book-now');          
        }
    }

    public function signin_user(Request $data)
    {
        $api = New Api;
        $user_array = $data->all();
        $url = url("/api/signin_user");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);

        //   print_r($response);exit;
        if($response->response->httpCode =='200') {  
            Session::put('user_id',$response->response->user_id);
            Session::put('user_name',$response->response->user_name);
            Session::put('user_email',$response->response->user_email);
            Session::put('user_mobile',$response->response->user_mobile);
            Session::put('country_code',$response->response->country_code);
            Session::put('token', $response->response->token);
            Session::put('profile_image', $response->response->image);
            return response()->json($response->response);
        } else {
            return response()->json($response->response);
        } 
    }

    public function forgot_password(Request $data)
    {
        $api = New Api;
        $user_array = $data->all();
        $url = url("/api/forgot_password");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);
        if($response->response->httpCode =='200')
        {  
            return response()->json($response->response);
        }
        else
        {
            return response()->json($response->response);
        } 
    }

    public function view_property($url_index,  Request $post_request){

        $req = $post_request->all();

        //  dd($req);
        
        if($url_index != ""){
            $api = New Api;
            $url = url("/api/view-hotel/".$url_index);
            $method = "POST";

            //  dd('Ak');

            if(Session::get('start_date') == "")
            {
                // echo 'sfk';exit;
                $sd = date('Y-m-d');
            }
            else
            {
                //  echo 'Ak';exit;
                $sd = Session::get('start_date');
            }
            if(Session::get('end_date') == "")
            {
                $ed = date('Y-m-d', strtotime(' +1 day'));
            }
            else
            {
                $ed = Session::get('end_date');
            }            
            if(Session::get('guest_count') == "")
            {
                $gc = 1;
            }
            else
            {
                $gc = Session::get('guest_count');
            }            
            if(Session::get('room_count') == "")
            {
                $rc = 1;
            }
            else
            {
                $rc = Session::get('room_count');
            }                        
            
      //    dd($ed);

            $user_array = array("sd" => $sd, "ed" => $ed, "gc" => $gc, "rc" => $rc);
            $data = array('form_params' => $user_array);

            //  $data = array();

            $response = $api->call_api($data,$url,$method);
            //   echo "<pre>";print_r($response);die;

            $outlets = $response->response->outlets;

            $outlet_id = $response->response->property_id;
            $vendor_id = $response->response->vendor_id;
            $amenities = $response->response->amenities;
            $roomtypecount = $response->response->roomtypecount;
            //  $places = $response->response->interest_places;

            $p1 = $response->response->p1;
            $p2 = $response->response->p2;
            $p3 = $response->response->p3;

            $rooc = $response->response->rooc;

            //  dd($p2);
            $recommended_properties = $response->response->recommended_properties;
            $room_type = $response->response->room_type;
            $price = $response->response->price;
            $avg_outlet_reviews = $response->response->avg_outlet_reviews;

            //$reviews = $response->response->reviews;

            $allreviews = $response->response->reviews;
            $avg_outlet_reviews_location = $response->response->avg_outlet_reviews_location;
            $avg_outlet_reviews_comfort = $response->response->avg_outlet_reviews_comfort;
            $avg_outlet_reviews_sleep_quality = $response->response->avg_outlet_reviews_sleep_quality;
            $avg_outlet_reviews_rooms = $response->response->avg_outlet_reviews_rooms;
            $avg_outlet_reviews_cleanliness = $response->response->avg_outlet_reviews_cleanliness;

            $total_rating = $response->response->total_rating;
            // print_r($avg_outlet_reviews_cleanliness);exit;

            $count_ratings5 = $response->response->count_ratings5;
            // echo $count_ratings5;die;
            $count_ratings4 = $response->response->count_ratings4;
            $count_ratings3 = $response->response->count_ratings3;
            $count_ratings2 = $response->response->count_ratings2;
            $count_ratings1 = $response->response->count_ratings1;
            $avg_outlet_reviews = $response->response->avg_outlet_reviews;

            $total_votes = ( $count_ratings1 + $count_ratings2 + $count_ratings3 + $count_ratings4 + $count_ratings5);
            
            if($total_votes != 0){
                $avg_ratings = (($count_ratings1 + $count_ratings2 * 2 + $count_ratings3 * 3 + $count_ratings4 * 4 + $count_ratings5 * 5) / $total_votes);

                    $avg_ratings5 = ((($count_ratings5) / $total_votes)*100);
                    $avg_ratings4 = ((($count_ratings4) / $total_votes)*100);
                    $avg_ratings3 = ((($count_ratings3) / $total_votes)*100);
                    $avg_ratings2 = ((($count_ratings2) / $total_votes)*100);
                    $avg_ratings1 = ((($count_ratings1) / $total_votes)*100);
            }
            else{

                    // print_r($avg_ratings);exit;
                    $avg_ratings5 = 0;
                    $avg_ratings4 = 0;
                    $avg_ratings3 = 0;
                    $avg_ratings2 = 0;
                    $avg_ratings1 = 0;
            }

            SEOMeta::setTitle(ucfirst($outlets[0]->outlet_name));
            SEOMeta::setDescription(ucfirst($outlets[0]->outlet_name));
            SEOMeta::addKeyword(ucfirst($outlets[0]->outlet_name));
            OpenGraph::setTitle(ucfirst($outlets[0]->outlet_name));
            OpenGraph::setDescription(ucfirst($outlets[0]->outlet_name));
            // OpenGraph::setUrl(URL::to('/'));
            Twitter::setTitle(ucfirst($outlets[0]->outlet_name));
            Twitter::setSite(ucfirst($outlets[0]->outlet_name));   

            $reviews   = DB::table('outlet_reviews')
                        ->select('outlet_reviews.comments', 'outlet_reviews.ratings', 'outlet_reviews.title', 'outlet_reviews.created_date', 'admin_customers.id', 'admin_customers.firstname', 'admin_customers.lastname','outlet_reviews.location', 'outlet_reviews.comfort', 'outlet_reviews.sleep_quality', 'outlet_reviews.rooms', 'cleanliness'/*, 'admin_customers.image'*/)
                        ->leftJoin('admin_customers','admin_customers.id','=','outlet_reviews.customer_id')
                        ->where('outlet_reviews.approval_status','1')
                        ->where('outlet_reviews.outlet_id', '=', $outlet_id)
                        ->orderBy('outlet_reviews.id', 'desc')
                        ->paginate(4);


            return view('front.'.$this->theme.'.view_property')->with('outlets',$outlets)
                    ->with('property_id',$outlet_id)->with('amenities',$amenities)
                    ->with('recommended_properties',$recommended_properties)
                    ->with('room_type',$room_type)->with('price',$price[0])
                    ->with('avg_outlet_reviews',$avg_outlet_reviews)->with('reviews',$reviews)
                    ->with('count_ratings5',$count_ratings5)->with('count_ratings4',$count_ratings4)
                    ->with('count_ratings3',$count_ratings3)->with('count_ratings2',$count_ratings2)
                    ->with('count_ratings1',$count_ratings1)->with('avg_ratings5',$avg_ratings5)
                    ->with('avg_ratings4',$avg_ratings4)
                    ->with('avg_outlet_reviews_location',$avg_outlet_reviews_location)
                    ->with('avg_outlet_reviews_comfort',$avg_outlet_reviews_comfort)
                    ->with('avg_outlet_reviews_sleep_quality',$avg_outlet_reviews_sleep_quality)
                    ->with('avg_outlet_reviews_rooms',$avg_outlet_reviews_rooms)
                    ->with('avg_outlet_reviews_cleanliness',$avg_outlet_reviews_cleanliness)
                    ->with('total_rating',$total_rating)->with('rooc',$rooc)
                    ->with('p1',$p1)->with('p2',$p2)->with('p3',$p3)
                    ->with('roomtypecount',$roomtypecount)
                    ->with('allreviews',$allreviews)->with('vendor_id',$vendor_id);
                    //  ->with('places',$places)
        }
                             
    }


            public function roomshow(Request $data)
            {
                $api = New Api;
                $user_array = $data->all();
                $url = url("/api/roomshow");
                $method = "POST";
                $data = array('form_params' => $user_array);

                $response = $api->call_api($data,$url,$method);

                //  print_r($response);exit;

                $room_type = $response->response->room_type;
                $outlets = $response->response->property_details;
                $vendor_id = $response->response->property_details[0]->vendor_id;
                $btn_val = $response->response->btn_val;

                //  print_r($room_type);exit;

                return view('front.'.$this->theme.'.view_roomtype', ['room_type' => $room_type,
                                                                     'outlets' => $outlets,
                                                                     'btn_val' => $btn_val,
                                                                     'vendor_id' => $vendor_id]);
            }


            public function roomsavailablitycheck(Request $data)
            {
                //  print_r($data->all());exit;

                $post_data = $data->all();

                //  print_r($post_data);exit;

                $sd = $post_data['sd'];

                $s = date_create($post_data['sd']);
                $e = date_create($post_data['ed']);

                $c = date_diff($e,$s);                

                //  print_r($c->days);exit;


                Session::put('start_date', $post_data['sd']);
                Session::put('end_date',   $post_data['ed']);
                Session::put('guest_count',$post_data['adult_count']);
                Session::put('room_count', $post_data['room_count']);
                Session::put('duration',   $c->days);

                $api = New Api;
                $user_array = $data->all();
                $url = url("/api/roomsavailablitycheck");
                $method = "POST";
                $data = array('form_params' => $user_array);

                $response = $api->call_api($data,$url,$method);

                //  print_r($response);exit;

                $room_type = $response->response->room_type;
                $outlets = $response->response->property_details;
                $vendor_id = $response->response->property_details[0]->vendor_id;



                //  print_r($room_type);exit;

                return view('front.'.$this->theme.'.view_roomtype', ['room_type' => $room_type,
                                                                     'outlets' => $outlets,
                                                                     'vendor_id' => $vendor_id]);
            }


            /**
             * Posts
             *
             * @return void
             */
            public function showPosts($url_index)
            {
                $reviews = Reviews::paginate(5);
                print_r($reviews);exit;
                if (Request::ajax()) {
                    return Response::json(View::make('reviews', array('reviews' => $reviews))->render());
                }
                return View::make('front.'.$this->theme.'.view_property', array('reviews' => $reviews));
            }


    public function cms($url_index){

        // print_r('expression');exit;
        $query = '"cms_infos"."language_id" = (case when (select count(*) as totalcount from cms_infos where cms_infos.language_id = ' . getCurrentLang() . ' and cms.id = cms_infos.cms_id) > 0 THEN ' . getCurrentLang() . ' ELSE 1 END)';
        $cms_info = DB::table('cms')
                ->select('cms.*', 'cms_infos.*')
                ->leftJoin('cms_infos', 'cms_infos.cms_id', '=', 'cms.id')
                ->whereRaw($query)
                ->where("url_index", "=", $url_index)
                ->limit(1)
                ->get();
        if (!count($cms_info)) {
            Session::flash('message', 'Invalid Page');
            Session::flash('alert-class', 'alert-danger');
            return Redirect::to('/');
        }
        SEOMeta::setTitle($cms_info[0]->title);
        SEOMeta::setDescription($cms_info[0]->title);
        SEOMeta::addKeyword($cms_info[0]->title);
        OpenGraph::setTitle($cms_info[0]->title);
        OpenGraph::setDescription($cms_info[0]->title);
        // OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle($cms_info[0]->title);
        Twitter::setSite($cms_info[0]->title);
        return view('front.' . $this->theme . '.cms')->with('cmsinfo', $cms_info);
    }

    public function hotel_list(Request $data)
    {
                //     dd($data->all());

        $place =  $data->place_city;

        if($data !="")
        {
            Session::put('place_city', $data->place_city);
            Session::put('start_date', $data->startDate);
            Session::put('end_date',   $data->endDate);
            Session::put('guest_count',$data->guest_count);
            Session::put('room_count', $data->room_count);
            Session::put('duration',   $data->duration);
        }

        $search_array = $data->all();

        $api = New Api;
        $url = url("/api/hotel_list");
        $method = "POST";
        $data = array('form_params' => $search_array);

        $response = $api->call_api($data,$url,$method);

        $outlets = $response->response->outlets;
        $pricing = $response->response->pricing;
        $accomodation = $response->response->accomodation;
        $amenities = $response->response->amenities;
        $cities = $response->response->cities;


        return view('front.' . $this->theme . '.list_property')->with('properties', $outlets)
                                                               ->with('pricing', $pricing)
                                                               ->with('amenities',$amenities)
                                                               ->with('accomodation',$accomodation)
                                                               ->with('city_details',$cities);
    }

    public function hotels_list($location,$lat,$lng,$sd,$ed,$gc,$rc,$user_lat,$user_lng,  Request $post_request)
    {

        // DATE CALCULATION 

        //  dd($user_lat);

        $s = date_create($sd);
        $e = date_create($ed);

        $c = date_diff($e,$s);

        $days = $c->days;

        if($location == "")
        {
            return view('front_errors.404');
        }

        if(!is_numeric($lat))
        {
            return view('front_errors.404');
        }

        if(!is_numeric($lng))
        {
            return view('front_errors.404');
        }  

        if($sd == "")
        {
            return view('front_errors.404');
        }

        if($ed == "")
        {
            return view('front_errors.404');
        }

        if(!is_numeric($gc))
        {
            return view('front_errors.404');
        }

        if(!is_numeric($rc))
        {
            return view('front_errors.404');
        }                           

        if(!is_numeric($user_lat))
        {
            return view('front_errors.404');
        }

        if(!is_numeric($user_lng))
        {
            return view('front_errors.404');
        }        

        //  dd($days);    

             
        $site_settings = getSettingsLists();

        SEOMeta::setTitle($site_settings->meta_title);
        SEOMeta::setDescription($site_settings->site_description);
        SEOMeta::addKeyword($site_settings->meta_keywords);
        OpenGraph::setTitle($site_settings->meta_title);
        OpenGraph::setDescription($site_settings->site_description);
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle($site_settings->meta_title);
        Twitter::setSite($site_settings->site_description);                            

        
        $user_array = array("lat" => $lat,"lng" => $lng,"sd" => $sd,"ed" => $ed,"rc" => $rc,"gc" => $gc);
          //    dd($user_array);
        $api = New Api;
        $url = url("/api/hotels_list");
        $method = "POST";        

        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);
        

              // dd($response);

        if($response->response->httpCode == 400)
        {
            return view('front_errors.404');
        }          

        $out = $response->response->outlets;


        //  dd($out);
            $outlets = DB::table('outlets')
                                ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                ->leftjoin('vendors_view','vendors_view.id','=','outlets.vendor_id')
                                ->leftjoin('cities_infos','cities_infos.id','=','outlets.city_id')
                                ->leftJoin('outlet_reviews','outlet_reviews.outlet_id','=','outlets.id')
                                ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
                                ->whereIN('outlets.id',$out)
                                ->select(DB::Raw('avg(outlet_reviews.ratings) as avg_out_rev'),
                                    'outlets.id','outlets.url_index','outlet_infos.outlet_name',
                                    'outlet_infos.contact_address','outlets.outlet_image',
                                    'vendors_view.id as vid',
                                    'cities_infos.city_name','zones_infos.zone_name')
                                ->groupBy('outlets.id','outlets.url_index','outlet_infos.outlet_name',
                                    'outlet_infos.contact_address','outlets.outlet_image',
                                    'cities_infos.city_name','zones_infos.zone_name',
                                    'vendors_view.featured_vendor','vendors_view.id')
                                ->orderBy('vendors_view.featured_vendor','desc')
                                ->orderByRaw(DB::RAW("earth_box(ll_to_earth(".$user_lat.",".$user_lng."), 50000) @> ll_to_earth(outlets.latitude, outlets.longitude)",'desc'))
                                ->orderBy('avg_out_rev','asc')
                                //  ->where('vendors_view.featured_vendor','=',1)
                                ->paginate(2);
                                //  ->get();

               //   dd($outlets);

            $orp = outlets::whereIN('id',$out)->pluck('rd')->toArray();

            if(count($orp) > 0)
            {
                $min_value = min($orp);
                $max_value = max($orp); 
            }
            else
            {
                $min_value = 0;
                $max_value = 1000;
            }

    

            //  dd($max_value);

            if ($post_request->ajax()) {
                $data = $post_request->all();

                  //    dd($data);
                        if(!empty($data['data']['cities'])){
                            $cities = explode(',',$data['data']['cities']);
                       }

                       if(!empty($data['data']['pricing'])){
                            $pricing = explode(',',$data['data']['pricing']);
                       }
                       
                       if(!empty($data['data']['amenities'])){
                            $amenities = explode(',',$data['data']['amenities']);
                       }

                       if(!empty($data['data']['accomodation'])){
                            $accomodation = explode(',',$data['data']['accomodation']);
                       }

                       if(!empty($data['data']['rating'])){
                            $rating = explode(',',$data['data']['rating']);
                       }       

                       if(!empty($data['data']['slider_val'])){
                            $slider_val = explode(',',$data['data']['slider_val']);
                       }                    

                       if(!empty($data['data']['radio_price'])){
                            $radio_price = $data['data']['radio_price'];
                       }

                       if(!empty($data['data']['radio_popular'])){
                            $radio_popular = $data['data']['radio_popular'];
                       }
                       
                       if(!empty($data['data']['radio_dist'])){
                            $radio_dist = $data['data']['radio_dist'];
                       }

                       if(!empty($data['data']['radio_rating'])){
                            $radio_rating = $data['data']['radio_rating'];
                       }

                       if(!empty($data['data']['user_lat'])){
                            $user_lat = $data['data']['user_lat'];
                       }

                       if(!empty($data['data']['user_lng'])){
                            $user_lng = $data['data']['user_lng'];
                       }                                              

               //   dd($data);

                    $category_property = DB::table('outlets')
                    ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                    ->Join('outlet_place_category_mapping','outlet_place_category_mapping.outlet_id','=','outlets.id');
                        
                          if(isset($pricing)){
                            $category_property->leftjoin('outlet_price_category_mapping','outlet_price_category_mapping.outlet_id','=','outlets.id');
                          }

                          if(isset($amenities)){
                            $category_property->leftjoin('outlet_amenity_mapping','outlet_amenity_mapping.outlet_id','=','outlets.id');
                          }
                          if(isset($accomodation)){
                            $category_property->leftjoin('outlet_accomodation_mapping','outlet_accomodation_mapping.outlet_id','=','outlets.id');
                          }
                          if(isset($cities)){
                            $category_property->leftjoin('cities','cities.id','=','outlets.city_id');
                          }
                          
                          if(isset($pricing)){
                            $category_property->whereIn('outlet_price_category_mapping.outlet_price_category',$pricing);
                          }
                          if(isset($radio_price)){
                                $category_property->orderby('outlets.rd','asc');
                            }

                          if(isset($radio_rating)){
                                $category_property->orderby('outlets.average_rating','desc');
                            }

                          if(isset($radio_dist)){

                                   //    dd($radio_dist);

                                //  print_r($lat);dd($lng);exit;

                                $category_property->orderByRaw(DB::RAW("earth_box(ll_to_earth(".$user_lat.",".$user_lng."), 50000) @> ll_to_earth(outlets.latitude, outlets.longitude)",'asc'));

                                //  dd('Ak');
                            
                                //  $category_property->orderby('outlets.average_rating','desc');
                            } 
                          if(isset($rating)){
                                $category_property->whereIn('outlets.average_rating',$rating);
                            }

                          if(isset($slider_val)){
                                $category_property->whereBetween('outlets.rd',$slider_val);
                            }

                          if(isset($amenities)){
                            $category_property->whereIn('outlet_amenity_mapping.amenity_id',$amenities);
                          }

                          if(isset($accomodation)){
                            $category_property->whereIn('outlet_accomodation_mapping.accomodation_id',$accomodation);
                          }

                          //    dd("Ak");
                          if(isset($cities)){
                            $category_property->whereIn('cities.id', $cities);
                          }


                          if($lat !="" && $lng !="")
                            {
                                $query_dist = "earth_box(ll_to_earth(".$lat.",".$lng."), 50000) @> ll_to_earth(outlets.latitude, outlets.longitude)";
                            }

                            if($gc !="")
                            {
                                 $grt = "room_type.adult_count >= $gc";
                            }                           

                    $outlets = $category_property->select(DB::Raw('avg(outlet_reviews.ratings) as or_rates'),
                                DB::Raw('min(room_type.discount_price) as discount_price'),
                                DB::Raw('min(room_type.normal_price) as normal_price'),
                                'outlets.url_index','outlet_infos.outlet_name','outlets.outlet_image',
                                'outlets.id','outlet_infos.contact_address','cities_infos.city_name',
                                'zones_infos.zone_name')
                            ->Join('room_type','room_type.property_id','=','outlets.id')
                            ->leftjoin('cities_infos','cities_infos.id','=','outlets.city_id')
                            ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
                            ->leftjoin('outlet_reviews','outlet_reviews.outlet_id','=','outlets.id')
                            ->leftjoin('vendors_view','vendors_view.id','=','outlets.vendor_id')
                            ->groupBy('outlet_infos.outlet_name')
                            ->whereRaw($query_dist)
                            ->groupBy('outlets.outlet_image')
                            ->groupBy('outlets.url_index','cities_infos.city_name','zones_infos.zone_name')
                            ->groupBy('outlet_infos.contact_address','outlets.id','vendors_view.featured_vendor')
                            ->orderBy('vendors_view.featured_vendor','desc')
                            ->orderBy('or_rates','asc')
                            ->paginate(2);
                            //  ->tosql();
                           //    ->get();
                    //  dd($outlets);
                    return view('front.'.$this->theme.'.list_ajax_property', ['properties' => $outlets])->render();               
                
            }        

        
        $pricing = $response->response->pricing;
        $accomodation = $response->response->accomodation;
        $amenities = $response->response->amenities;
        $cities = $response->response->cities;

        $sd = date('m/d/Y', strtotime($sd));
        $ed = date('m/d/Y', strtotime($ed));

        //  dd($sd);

        Session::put('start_date', $sd);
        Session::put('end_date',   $ed);
        Session::put('guest_count',$gc);
        Session::put('room_count', $rc);
        Session::put('duration',   $days);         

        return view('front.' . $this->theme . '.list_property')->with('properties', $outlets)
                                                               ->with('pricing', $pricing)
                                                               ->with('amenities',$amenities)
                                                               ->with('accomodation',$accomodation)
                                                               ->with('city_details',$cities)
                                                               ->with('min_value',$min_value)
                                                               ->with('max_value',$max_value)
                                                               ->with('sd',$sd)->with('ed',$ed)
                                                               ->render();       

     //    dd($response);
     
    }

    public function checkout($outlet_url_index,$room_url_index)
    {
            //  dd($oid);

            $old_date = date('01/01/1970');

            if(Session::get('start_date') == "")
            {
                
                $sd = date('m/d/Y');
                Session::put('start_date', $sd);
                //  print_r('1');



            }
            elseif(Session::get('start_date') == $old_date)
            {
                
                $sd = date('m/d/Y');
                Session::put('start_date',   $sd);
                //  print_r('2');
                //  dd($sd);
            }                                    
            else
            {
                
                $sd = Session::get('start_date');
                //  print_r('3  ');
                //  dd($sd);
            
            }

                $ss = Session::get('start_date');
               
        
            if(Session::get('end_date') == "")
            {
                $ed = date('m/d/Y', strtotime(' +1 day'));
                Session::put('end_date',   $ed);

                //  SMALL DATE MONTH ISSUE CREATED - IT WAS RECTIFIED
            }
            elseif(Session::get('end_date') == $old_date)
            {
                $ed = date('m/d/Y', strtotime(' +1 day'));
                Session::put('end_date',   $ed);
            }
            else
            {
                $ed = Session::get('end_date');
            }         

        //  echo "<pre>";print_r($sd);print_r($ed);exit;



             $s = date_create($sd);
             $e = date_create($ed);

             // print_r($s);print_r($e);exit;

             // dd($sd);

             $c = date_diff($e,$s);

             $days = $c->days;


            if(Session::get('guest_count') == "")
            {
                $gc = 1;
                Session::put('guest_count',$gc);
            }
            else
            {
                $gc = Session::get('guest_count');
            }            
            if(Session::get('room_count') == "")
            {
                $rc = 1;
                Session::put('room_count', $rc);
            }
            else
            {
                $rc = Session::get('room_count');
            }               

             // dd($days);

        $outlets = DB::table('outlets')
                     ->select('outlets.id','outlets.outlet_image','outlet_infos.outlet_name'
                            ,'outlet_infos.contact_address','outlets.url_index')
                     ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                     ->where('outlets.url_index',$outlet_url_index)
                     ->get();
        $outlet_id = $outlets[0]->id;
         // dd($outlet_id);
        $room_type = DB::table('room_type')
                     ->select('room_type.created_by','room_type.id','room_type_infos.room_type','room_type.discount_price')
                     ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                     ->where('room_type.url_index','=',$room_url_index)
                     ->where('property_id','=',$outlet_id)
                     ->get();

        $room_type_id = $room_type[0]->id;

        //  dd($room_type);

        $current_date = date('Y-m-d');

        $coupons = DB::table('coupons')
                        ->select('coupons.id as coupon_id', 'coupon_type', 'offer_amount', 'coupon_code', 'start_date', 'end_date', 'offer_type', 'minimum_order_amount', 'offer_percentage')
                        ->leftJoin('coupon_outlet','coupon_outlet.coupon_id','=','coupons.id')
                        //  ->where('coupon_code','=',$post_data['promo_code'])
                        ->where('coupon_outlet.outlet_id','=',$outlet_id)
                        ->where('coupons.start_date','<=',$current_date)
                        ->where('coupons.end_date','>=',$current_date)
                        ->first();

        //  dd($coupons);

        $user_array = array("sd" => $sd,"ed" => $ed,"rc" => $rc,"gc" => $gc,"outlet_id" => $outlet_id,
            "room_type_id" => $room_type_id);

        $api = New Api;
        $url = url("/api/roomtypeavailablitycheck");
        $method = "POST";        

        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);

        //  dd($response);

        SEOMeta::setTitle(ucfirst($outlets[0]->outlet_name));
        SEOMeta::setDescription(ucfirst($outlets[0]->outlet_name));
        SEOMeta::addKeyword(ucfirst($outlets[0]->outlet_name));
        OpenGraph::setTitle(ucfirst($outlets[0]->outlet_name));
        OpenGraph::setDescription(ucfirst($outlets[0]->outlet_name));
        // OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle(ucfirst($outlets[0]->outlet_name));
        Twitter::setSite(ucfirst($outlets[0]->outlet_name));        

        if($response->response->httpCode =='200')
        {  
            
            return view('front.' . $this->theme . '.checkout')->with('properties',$outlets[0])
                                                              ->with('room_type',$room_type[0])
                                                              ->with('coupons',$coupons)
                                                              ->with('startDate',$sd)
                                                              ->with('endDate',$ed)
                                                              ->with('days',$days);            
        }
        else
        {
            Session::flash('reorder-message', trans('messages.Rooms are Not Available for this Date!'));
            return Redirect::to('/view-hotel/'.$outlet_url_index)->send();
        }         



    }


    public function redirectToProvider() {
        // print_r('expression');exit;
        return Socialite::driver('facebook')->redirect();
    }

    
    public function handleProviderCallback() {
        try {
            $user = Socialite::driver('facebook')->stateless()->user();

            //  print_r($user);exit;
            $name = $user["name"];
            $fb_token = $user->token;
            $name_splitted = explode(" ", $name);
            $first_name = isset($name_splitted[0]) ? $name_splitted[0] : "";
            $last_name = isset($name_splitted[1]) ? $name_splitted[1] : "";
            $email = isset($user["email"]) ? $user["email"] : "";
            //$email = "";
            //  $gender = (isset($user->user['gender']) && ($user->user['gender'] == "male")) ? "M" : "F";
            $facebook_id = $user["id"];
            $api = New Api;
            $user_array = array("name" => $name, "first_name" => $first_name, "last_name" => $last_name, "email" => $email,/* "gender" => $gender, */ "login_type" => 1, "user_type" => 4, "facebook_id" => $facebook_id, "image_url" => $user->getAvatar(), "language" => getCurrentLang());

            //  print_r($user_array);exit;
            $url = url("/api/signup_fb_user");
            $method = "POST";
            $data = array('form_params' => $user_array);
            $response = $api->call_api($data, $url, $method);
              //    print_r($response);exit;
            if ($response->response->status == false) {
                Session::flash('message', $response->response->Message);
                return Redirect::to(URL::previous());
            }
            else if ($response->response->status == true) {

                Session::flash('message', $response->response->Message);
                Session::put('user_id',$response->response->user_id);
                Session::put('user_name',$response->response->first_name);
                Session::put('user_email',$response->response->email);
                Session::put('user_mobile',$response->response->mobile);
                //  Session::put('country_code',$response->response->country_code);
                Session::put('token', $user->token);
                Session::put('profile_image', $response->response->image);
                //  Session::put('name', $response->response->name);
                //  Session::put('social_title', $response->response->social_title);
                Session::put('facebook_id', $response->response->facebook_id);
                Session::put('fb_token', $fb_token);

                if ($response->response->email == "" || $response->response->mobile == "") {
                    return Redirect::to('/profile');
                }
                return Redirect::to(URL::previous());
            } else {
                Session::flash('message', $response->response->Message);
                return Redirect::to(URL::previous());
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            Session::flash('message', trans("messages.Some thing went wrong"));
           return Redirect::to(URL::previous());
        } catch (Exception $e) {

        }
    }    


    public function redirectToProviderGoogle()
    {
        // print_r('expression');exit;
        return Socialite::driver('google')->redirect();
    }


    public function handleProviderCallbackGoogle()
    {
        //  print_r('expression');exit;
        try {
            
            $user = Socialite::with('google')->user();
            //  $user = Socialite::driver('google')->stateless()->user();
             // echo '<pre>';print_r($user);exit;

            
            $name = $user->name;
            $google_token = $user->token;
            $name_splitted = explode(' ',$name);
            $first_name = isset($name_splitted[0]) ? $name_splitted[0] : "";
            $last_name = isset($name_splitted[1]) ? $name_splitted[1] : "";
            $email = isset($user->email) ? $user->email : "";
            $gender = (isset($user->user['gender']) && ($user->user['gender'] == "female")) ? "F" : "M";
            $googlepkus_id = $user->id;
            $api = New Api;
            $user_array = array("name" => $name, "first_name" => $first_name, "last_name" => $last_name, "email" => $email, "gender" => $gender, "login_type" => 1, "user_type" => 5, "gplus_id" => $googlepkus_id  , "image_url" => $user->getAvatar(), "language" => getCurrentLang(),'gplus_token'=>$user->token);
            $url = url("/api/signup_gplus_user");
            $method = "POST";
            $data = array('form_params' => $user_array);
            $response = $api->call_api($data, $url, $method);
                
            //  dd($response);

            if ($response->response->status == false) {
                Session::flash('gfmessage', $response->response->Message);
                return Redirect::to(URL::previous());
            } 
            else if ($response->response->status == true) {

                Session::flash('gsmessage', $response->response->Message);
                Session::put('user_id',$response->response->user_id);
                Session::put('user_name',$response->response->first_name);
                //  Session::put('last_name', $response->response->last_name);
                Session::put('user_email',$response->response->email);
                Session::put('user_mobile',$response->response->mobile);
                //  Session::put('country_code',$response->response->country_code);
                Session::put('token', $user->token);
                Session::put('profile_image', $response->response->image);
                //  Session::put('name', $response->response->name);
                //  Session::put('social_title', $response->response->social_title);
                Session::put('gplus_id', $response->response->gplus_id);
                Session::put('gplus_token', $google_token);

                if($response->response->mail == 1)
                {

                $string = $response->response->string;
                $email = $response->response->email;

                $template = DB::table('email_templates')
                        ->select('from_email', 'from', 'subject', 'template_id', 'content')
                        ->where('template_id', '=', self::USERS_WELCOME_EMAIL_TEMPLATE)
                        ->get();

                    if(count($template)){
                        $from = $template[0]->from_email;
                        $from_name=$template[0]->from;
                        $subject = $template[0]->subject;
                        if(!$template[0]->template_id)
                        {
                            $template = 'mail_template';
                            $from=getAppConfigEmail()->contact_email;
                            $subject = "Welcome to ".getAppConfig()->site_name;
                            $from_name="";
                        }
                        $user['password'] = $string;
                        $content =array("customer" => $response->response->first_name,
                            'u_password' => $string);
                        $email=smtp($from,$from_name,$email,$subject,$content,$template);
                    }   
                }

                if ($response->response->email == "" || $response->response->mobile == "") {
                    return Redirect::to('/profile');
                }
                return Redirect::to(URL::previous());
            } else {
                Session::flash('gfmessage', $response->response->Message);
                return Redirect::to(URL::previous());
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            Session::flash('message', trans("messages.Some thing went wrong"));
           return Redirect::to(URL::previous());
        } catch (Exception $e) {
            
        }

    }



    public function review_comments(Request $data)
    {
        // $post_data = $data->all();

        // print_r($post_data);exit;
        
        $api = New Api;
        $user_array = $data->all();
        // print_r($user_array);exit;
        // exit;
        $url = url("/api/viewhotel/review_comments");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);

                //  print_r($response); exit;

        $reviews = $response->response->reviews;


        // if($response->response->httpCode =='200')
        // {  
        //     return response()->json($response->response);
        // }
        // else
        // {
        //     return response()->json($response->response);
        // } 

        return view('front.'.$this->theme.'.view_pagination', ['reviews' => $reviews]);        
        
        // echo "<pre>";print_r($data);die;   
    }

     public function contactus() {

        $site_settings = getSettingsLists();       

        SEOMeta::setTitle('Contact us');
        SEOMeta::setDescription('Contact us');
        SEOMeta::addKeyword('Contact us');
        OpenGraph::setTitle('Contact us');
        OpenGraph::setDescription('Contact us');
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle('Contact us');
        Twitter::setSite($site_settings->site_name);
        
         $settings = Settings::find(1);
         $settings_infos = settings_infos::find(1);

        // Section description
        return view('front.' . $this->theme . '.contactus')->with('settings', $settings)->with('settings_infos', $settings_infos);
    }

    public function storecontact(Request $data)
    {
        //  print_r($data->all());
        $api = New Api;
        $user_array = $data->all();
        //print_r($user_array);exit;
        // exit;
        $url = url("/api/postcontactus");
        $method = "POST";
        $data = array('form_params' => $user_array);

        $response = $api->call_api($data,$url,$method);

          //    print_r($response); exit;
        if($response->response->httpCode =='200')
        {  
            return response()->json($response->response);
        }
        else
        {
            return response()->json($response->response);
        } 
    }
	
	
	public function landing()
    {

        $site_settings = getSettingsLists();

        //  dd($site_settings);

        SEOMeta::setTitle('Best hotel management system');
        SEOMeta::setDescription('Best hotel management system');
        SEOMeta::addKeyword('Best hotel management system');
        OpenGraph::setTitle('Best hotel management system');
        OpenGraph::setDescription('Best hotel management system');
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle('Best hotel management system');
        Twitter::setSite('Best hotel management system');

        return view('front.' . $this->theme . '.landing');
    }
	
	public function landing_signup()
    {
        $site_settings = getSettingsLists();

        SEOMeta::setTitle('Make the world your workplace');
        SEOMeta::setDescription('Make the world your workplace');
        SEOMeta::addKeyword('Make the world your workplace');
        OpenGraph::setTitle('Make the world your workplace');
        OpenGraph::setDescription('Make the world your workplace');
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle('Make the world your workplace');
        Twitter::setSite('Make the world your workplace');

        return view('front.' . $this->theme . '.landing_signup');
    }

    public function user_rating(Request $data) {
        $api = New Api;
        $user_array = $data->all();
        //  print_r($user_array);exit;
        $url = url("/api/user_rating");
        $method = "POST";
        $post_array = array_merge($user_array, array('user_id' => Session::get('user_id'),"language" => isset($user_array['language'])?$user_array['language']:'1'));
        $data = array('form_params' => $post_array);
        $response = $api->call_api($data, $url, $method);

        //  print_r($response);exit;
        if ($response->response->httpCode == '200') {
            return response()->json($response->response);
        } else {
            return response()->json($response->response);
        }
    }    

      public function blog($index = '') {
        $url_index = Input::get('filter');
        $keyword = Input::get('keyword');
        SEOMeta::setTitle('Blog');
        SEOMeta::setDescription('Blog');
        SEOMeta::addKeyword('Blog');
        OpenGraph::setTitle('Blog');
        OpenGraph::setDescription('Blog');
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle('Blog');
        Twitter::setSite('Blog');


       /* $type = 3;
        $category_id = '';
        if ($url_index) {

            if(is_numeric($url_index)){
                $category_id = DB::select("select id from  categories where category_type = $type and id ='" . $url_index . "' limit 1");
            }else{


            $category_id = DB::select("select id from  categories where category_type = $type and url_key ='" . $url_index . "' limit 1");
            }
        }
        $query = '"blog_infos"."language_id" = (case when (select count(*) as totalcount from blog_infos where blog_infos.language_id = ' . getCurrentLang() . ' and blogs.id = blog_infos.blog_id) > 0 THEN ' . getCurrentLang() . ' ELSE 1 END)';
        $condtion = 'blogs.status = 1';
        if ($category_id) {
            $c_ids = $category_id[0]->id;
            $condtion .= " and (regexp_split_to_array(category_ids,',')::integer[] @> '{" . $c_ids . "}'::integer[]  and category_ids !='')";
        }
        if ($keyword) {
            $condtion .= " and blog_infos.title ILIKE '%" . $keyword . "%'";
        }
        $blogs = DB::table('blogs')
                ->select('blogs.*', 'blog_infos.*')
                ->leftJoin('blog_infos', 'blog_infos.blog_id', '=', 'blogs.id')
                ->whereRaw($query)
                ->whereRaw($condtion)
               // ->where('blogs_infos.status', '=', '1')
                ->orderBy('blogs.id', 'desc')
                ->get();


        $query = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = ' . getCurrentLang() . ' and categories.id = categories_infos.category_id) > 0 THEN ' . getCurrentLang() . ' ELSE 1 END)';
        $category = DB::table('categories')
                ->select('categories.*', 'categories_infos.*')
                ->leftJoin('categories_infos', 'categories_infos.category_id', '=', 'categories.id')
                ->whereRaw($query)
                ->where("categories.category_type", "=", $type)
                ->where('categories.category_status','=',1)
                ->orderBy('categories_infos.category_name', 'asc')
                ->get();

        return view('front.' . $this->theme . '.blog')->with('blog', $blogs)->with('category', $category); */
         $query = '"blog_infos"."language_id" = (case when (select count(*) as totalcount from blog_infos where blog_infos.language_id = ' . getCurrentLang() . ' and blogs.id = blog_infos.blog_id) > 0 THEN ' . getCurrentLang() . ' ELSE 1 END)';
            $condtion = 'blogs.status = 1';
         $blogs = DB::table('blogs')
                ->select('blogs.*', 'blog_infos.*')
                ->leftJoin('blog_infos', 'blog_infos.blog_id', '=', 'blogs.id')
                ->whereRaw($query)
                ->whereRaw($condtion)
                ->where('blogs.status', '=', '1')
                ->orderBy('blogs.id', 'desc')
                ->get();
        return view('front.' . $this->theme . '.blog')->with('blog', $blogs);
    }

    public function blog_info($url_key)
    {
        $query = '"blog_infos"."language_id" = (case when (select count(*) as totalcount from blog_infos where blog_infos.language_id = ' . getCurrentLang() . ' and blogs.id = blog_infos.blog_id) > 0 THEN ' . getCurrentLang() . ' ELSE 1 END)';
            $condtion = 'blogs.status = 1';
         $blog_infos = DB::table('blogs')
                ->select('blogs.*', 'blog_infos.*')
                ->leftJoin('blog_infos', 'blog_infos.blog_id', '=', 'blogs.id')
                ->whereRaw($query)
                ->whereRaw($condtion)
                ->where('blogs.status', '=', '1')
                ->where('blogs.url_index',$url_key)
                ->orderBy('blogs.id', 'desc')
                ->first();

         $related_blogs = DB::table('blogs')
                ->select('blogs.*', 'blog_infos.*')
                ->leftJoin('blog_infos', 'blog_infos.blog_id', '=', 'blogs.id')
                ->whereRaw($query)
                ->whereRaw($condtion)
                ->where('blogs.status', '=', '1')
                //  ->orderBy('blogs.id', 'desc')
                ->take(3)
                ->get();
        //  dd($related_blogs);
                if(!count($blog_infos)){
                    Session::flash('message', 'Invalid Blog');
                    Session::flash('alert-class', 'alert-danger');
                    return Redirect::to('/');
                }               
        return view('front.' . $this->theme . '.blog_info')->with('blog', $blog_infos)
                                                           ->with('related_blogs',$related_blogs);
                    
    }

    public function user_referral()
    {
        SEOMeta::setTitle('Refer Friend');
        SEOMeta::setDescription('Refer Friend');
        SEOMeta::addKeyword('Refer Friend');
        //OpenGraph::setTitle('Hotel Management Website.');
        OpenGraph::setTitle('Refer Friend');
        //OpenGraph::setDescription(Session::get("general")->site_name . ' - ' . 'Blog');
        OpenGraph::setDescription('Best Hotel Management Website.');
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle('Refer Friend');
        Twitter::setSite(Session::get("general")->site_name);
        //$imgpath = base_path().'assets/front/otel2go/images/logo/159_81/logo.png';
        // OpenGraph::setImage($imgpath);
        $user_id = Session::get('user_id');
        $user_infos = DB::table('admin_customers')->where('id',$user_id)
                        ->select('id','customer_unique_id')->first();
        if(!count($user_infos)){
                    Session::flash('message', 'Invalid User Details');
                    Session::flash('alert-class', 'alert-danger');
                    return Redirect::to('/');
        }         
        return view('front.' . $this->theme . '.refer_earn')->with('data', $user_infos);
    }

    public function signup_referral_user($customer_unique_id)
    {
        $user_id = Session::has('user_id')?Session::get('user_id'):0;
         if($user_id != 0){
             Session::flash('message', 'User is already logged in.Please try other browsers.');
             Session::flash('alert-class', 'alert-danger');
             return Redirect::to('/');
         }
         //$customer_unique_id =  (String)base64_decode($referral_id);//die;  
         $user_details = DB::table('admin_customers')->where('customer_unique_id',$customer_unique_id)->first();
         if(!count($user_details)){
                Session::flash('message', 'Invalid Referral Id');
                Session::flash('alert-class', 'alert-danger');
                return Redirect::to('/');
         }
         return view('front.' . $this->theme . '.referral_signup')->with('referral_id',$customer_unique_id)->with('user_id',$user_details->id);
         //->with('data', $user_infos);
    }
}