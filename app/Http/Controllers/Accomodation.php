<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\accomodation_type;
use App\Model\accomodation_type_infos;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;


class Accomodation extends Controller
{

                
                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function index()
                {

                    if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        } 
                        if(!has_staff_permission('vendor/accomodation'))
                        {
                        return view('errors.405');
                        }
                    else{
                        $query = '"accomodation_type_infos"."language_id" = (case when (select count(*) as totalcount from accomodation_type_infos where accomodation_type_infos.language_id = '.getAdminCurrentLang().' and accomodation_type.id = accomodation_type_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $Accomodation_type=DB::table('accomodation_type')
                         ->select(DB::raw('accomodation_type.* ,accomodation_type.id as cid'),'accomodation_type_infos.*')
                        ->leftJoin('accomodation_type_infos','accomodation_type_infos.id','=','accomodation_type.id')
                        ->whereRaw($query)
                        ->orderBy('accomodation_type_name', 'asc')
                        ->get();
                        return view('vendors.accomodation.list')->with('accomodation_type', $Accomodation_type);
                    }
                    
                }
                
                public function accomodation_create()
                {
                    if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/accomodation/create'))
                        {
                        return view('errors.405');
                        } else{
                        return view('vendors.accomodation.create');
                    }
                }


                public function accomodation_edit($id)
                {
                   // print_r($id);
                   //  exit;
                   if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/accomodation/edit'))
                        {
                        return view('errors.405');
                        }

                        // $users = DB::table('users')->count();
                        $accomodation_type = DB::Table('accomodation_type')
                                    ->where('id','=',$id)
                                    ->count();            
                        if($accomodation_type != 0) 
                        {
                            $info = new Accomodation_type_infos;
                            // echo "<pre>";
                            // print_r($info);exit;
                            $Accomodation_type = Accomodation_type::find($id);

                            $ac = DB::table('accomodation_type_infos')
                                    ->select('accomodation_type_infos.*')
                                    ->where('id',$id)
                                    ->get();

                            // $cp = DB::table('accomodation_type_infos')
                            // ->pluck('accomodation_type_name');
                            return view('vendors.accomodation.edit')->with('data', $Accomodation_type)->with('infomodel', $info)->with('ac',$ac[0]);
                        }           
                        else
                        {
                           Session::flash('message', trans('messages.Invalid Accomodation details!'));     
                           return Redirect::to('vendor/accomodation');
                        }
                }
                
                public function accomodation_store(Request $data)
                {
                    // print_r($data->all());exit;
                    $accomodation_type_name = Input::get('accomodation_type_name');
                    foreach ($accomodation_type_name  as $key => $value) {
                        $fields['accomodation_type_name'.$key] = $value;
                        //  $rules['accomodation_type_name'.'1'] = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:accomodation_type_infos,accomodation_type_name';
                        $rules['accomodation_type_name'.'1'] = 'required|regex:/(^[A-Za-z\s ]+$)+/';
                        
                    }
                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{

                            $Accomodation_type = new Accomodation_type;
                            $Accomodation_type->url_index =  $_POST['accomodation_type_name'][1] ? str_slug($_POST['accomodation_type_name'][1]): str_slug($_POST['accomodation_type_name'][1]);
                            // Accessing the value from the session of store controller...
                            $vendor_id = Session::get('vendor_id');
                            // print_r($vendor_id);die;
                            $Accomodation_type->created_by = $vendor_id;

                            $Accomodation_type->created_date = date("Y-m-d H:i:s");          
                            $Accomodation_type->modified_date = date("Y-m-d H:i:s");
                            $Accomodation_type->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            //$this->country_save_before($Countries,$_POST);
                            // echo "<pre>";
                            // print_r($Accomodation_type);exit;

                            $Accomodation_type->save();

                   /*         $imageName = strtolower($Accomodation_type->id . '.' . $data->file('amenty_image')->getClientOriginalExtension());
                            $data->file('amenty_image')->move(
                                base_path() . '/public/assets/admin/base/images/Accomodation_type/', $imageName
                            );
                            $destinationPath2 = url('/assets/admin/base/images/Accomodation_type/'.$imageName.'');
                          
                            $size=getImageResize('VENDOR');
                            Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/Accomodation_type/list/'.$imageName)->destroy();
                            
                            
                            $Accomodation_type->amenty_image=$imageName;
                    
                            $Accomodation_type->save();
                    */

                            $this->accomodation_type_save_after($Accomodation_type,$_POST);
                            Session::flash('message', trans('messages.Accomodation has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('vendor/accomodation');
                    }
                }
              
                
                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function accomodation_update(Request $data, $id)
                {
/*                    
                    print_r($id);
                    print_r($data->all());
                    exit;
*/                    

                  //  $fields['zone_code'] = Input::get('zone_code');
                   // $fields['amenty_image'] = Input::file('amenty_image');
                    //$fields['country'] = Input::get('country');
                    $rules = array(
                       // 'accomodation_type_name' => 'required',
                        //'amenty_image' => 'required',
                        // 'country' => 'required',
                    );
                    $accomodation_type_name = Input::get('accomodation_type_name');
                    foreach ($accomodation_type_name  as $key => $value) {
                        $fields['accomodation_type_name'.$key] = $value;
                        //  $rules['accomodation_type_name'.'1'] = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:accomodation_type_infos,accomodation_type_name,'.$id.',id';
                        $rules['accomodation_type_name'.'1'] = 'required|regex:/(^[A-Za-z\s ]+$)+/';
                        
                    }


                    $validator = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        
                        try{
                            
                            $Accomodation_type = Accomodation_type::find($id); 
                            $Accomodation_type->url_index =  $_POST['accomodation_type_name'][1] ? str_slug($_POST['accomodation_type_name'][1]): str_slug($_POST['accomodation_type_name'][1]);
                         //   $Accomodation_type->zone_code = $_POST['zone_code'];
                            //$Accomodation_type->country_id = 1;      /*  1-> Malaysia    $_POST['country'];   */    

                            $vendor_id = Session::get('vendor_id');

                            $Accomodation_type->created_by = $vendor_id;
                            $Accomodation_type->modified_date = date("Y-m-d H:i:s");
                            $Accomodation_type->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            $Accomodation_type->save();
/*
                    if(isset($_FILES['amenty_image']['name']) && $_FILES['amenty_image']['name']!=''){
                                $imageName = strtolower($id . '.' . $data->file('amenty_image')->getClientOriginalExtension());
                                $data->file('amenty_image')->move(
                                    base_path() . '/public/assets/admin/base/images/Accomodation_type/', $imageName
                                );
                                $destinationPath2 = url('/assets/admin/base/images/Accomodation_type/'.$imageName.'');
                                $size=getImageResize('VENDOR');
                                Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/Accomodation_type/list/'.$imageName)->destroy();

                          
                               Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/detail/'.$imageName)->destroy();
                                Image::make( $destinationPath2 )->fit($size['THUMB_WIDTH'], $size['THUMB_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/'.$imageName)->destroy();
                                Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/detail/'.$imageName)->destroy();

                      
                        
                                $Accomodation_type->amenty_image = $imageName;

                                $Accomodation_type->save();
                            }
*/                            

                            $this->accomodation_type_save_after($Accomodation_type,$_POST);
                            Session::flash('message', trans('messages.Accomodation has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('vendor/accomodation');
                    }
                }

               
               
                /**
                 * add,edit datas  saved in main table 
                 * after inserted in sub tabel.
                 *
                 * @param  int  $id
                 * @return Response
                 */
               public static function accomodation_type_save_after($object,$post)
               {
                    $city = $object;
                    $post = $post;
                    if(isset($post['accomodation_type_name'])){
                        $accomodation_type_name = $post['accomodation_type_name'];
                        try{                
                            $affected = DB::table('accomodation_type_infos')->where('id', '=', $object->id)->delete();
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
                                if(isset($accomodation_type_name[$lang->id]) && $accomodation_type_name[$lang->id]!=""){
                                    $infomodel = new Accomodation_type_infos;
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->id = $object->id; 
                                    $infomodel->accomodation_type_name = $accomodation_type_name[$lang->id];
                                    $infomodel->save();
                                }
                            }
                            }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                    }
               }
                
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function accomodation_destroy($id)
                {
                    if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/accomodation/delete'))
                        {
                        return view('errors.405');
                        }
                        $accomodation_type = DB::Table('accomodation_type')
                                    ->where('id','=',$id)
                                    ->count();
                                       
                        if($accomodation_type != 0) 
                        {
                        $data = Accomodation_type::find($id);
                        $data->delete();
                        Session::flash('message', trans('messages.Accomodation has been deleted successfully!'));
                        return Redirect::to('vendor/accomodation');
                        }
                        else
                        {
                               Session::flash('message', trans('messages.Invalid Accomodation details!'));     
                               return Redirect::to('vendor/accomodation');
                        }
                }  
                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxAccomodation_type()
                {
                  $vendor_id = Session::get('vendor_id');
                        //  print_r($id); exit;

                        if(Session::get('vendor_type') == 2 )
                            {
                                $vendor_id = Session::get('created_vendor_id');
                                //  print_r($id); exit;
                            }                                        
                    //  $country=getCountryLists();
                $query = '"accomodation_type_infos"."language_id" = (case when (select count(*) as totalcount from accomodation_type_infos where accomodation_type_infos.language_id = '.getAdminCurrentLang().' and accomodation_type.id = accomodation_type_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                $Accomodation_type = Accomodation_type::Leftjoin('accomodation_type_infos','accomodation_type_infos.id','=','accomodation_type.id')
                ->select(DB::raw('accomodation_type.* ,accomodation_type.id as cid'),'accomodation_type_infos.*')
                    ->whereRaw($query)
                    ->where('created_by',$vendor_id)
                    ->orderBy('accomodation_type.id', 'desc')
                    ->get();

                    return Datatables::of($Accomodation_type)->addColumn('action', function ($Accomodation_type) {
                        if(has_staff_permission('vendor/accomodation/edit')){
                            return '<div class="btn-group"><a href="'.URL::to("vendor/accomodation/edit/".$Accomodation_type->cid).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("vendor/accomodation/delete/".$Accomodation_type->cid).'" class="delete-'.$Accomodation_type->cid.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                    </ul>
                                </div><script type="text/javascript">
                                $( document ).ready(function() {
                                $(".delete-'.$Accomodation_type->cid.'").on("click", function(){
                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });});</script>';
                        }
                        })
                        ->addColumn('default_status', function ($Accomodation_type) {
                            if($Accomodation_type->default_status==0):
                                $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                            elseif($Accomodation_type->default_status==1):
                                $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                            endif;
                            return $data;
                        })
                        ->addColumn('accomodation_type_name', function ($Accomodation_type) {
                                $data = ucfirst($Accomodation_type->accomodation_type_name);
                            return $data;
                        })
                        ->make(true);
                }

                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function manager_index()
                {

                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } 
                    else{
                        $query = '"accomodation_type_infos"."language_id" = (case when (select count(*) as totalcount from accomodation_type_infos where accomodation_type_infos.language_id = '.getAdminCurrentLang().' and accomodation_type.id = accomodation_type_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $Accomodation_type=DB::table('accomodation_type')
                         ->select(DB::raw('accomodation_type.* ,accomodation_type.id as cid'),'accomodation_type_infos.*')
                        ->leftJoin('accomodation_type_infos','accomodation_type_infos.id','=','accomodation_type.id')
                        ->whereRaw($query)
                        ->orderBy('accomodation_type_name', 'asc')
                        ->get();
                        return view('managers.accomodation.list')->with('accomodation_type', $Accomodation_type);
                    }
                    
                }
                
                public function manager_accomodation_create()
                {
                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    }else{
                        return view('managers.accomodation.create');
                    }
                }


                public function manager_accomodation_edit($id)
                {
                   // print_r($id);
                    //exit;
                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } else{
                        $info = new Accomodation_type_infos;
                       // print_r($info);exit;
                        $Accomodation_type = Accomodation_type::find($id);

                        $ac = Accomodation_type_infos::find($id);
                        //  print_r($ac);exit;
                        return view('managers.accomodation.edit')->with('data', $Accomodation_type)->with('infomodel', $info)->with('ac',$ac);
                    }
                }
                
                
                public function manager_accomodation_store(Request $data)
                {
// print_r($data->all());
// exit;
                    //$fields['zone_code'] = Input::get('zone_code');
                  //  $fields['amenty_image'] = Input::file('amenty_image');

                    //  $fields['country'] = Input::get('country');
                   /* $rules = array(
                        
                        'amenty_image' => 'required|mimes:png,jpg,jpeg,bmp|max:2024'
                        //  'country' => 'required',
                    );
                    */
                    $accomodation_type_name = Input::get('accomodation_type_name');
                    foreach ($accomodation_type_name  as $key => $value) {
                        $fields['accomodation_type_name'.$key] = $value;
                        //  $rules['accomodation_type_name'.'1'] = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:accomodation_type_infos,accomodation_type_name';
                        $rules['accomodation_type_name'.'1'] = 'required|regex:/(^[A-Za-z\s ]+$)+/';
                        
                    }
                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{
// print_r($Accomodation_type);exit;
                            $Accomodation_type = new Accomodation_type;
                            $Accomodation_type->url_index =  $_POST['accomodation_type_name'][1] ? str_slug($_POST['accomodation_type_name'][1]): str_slug($_POST['accomodation_type_name'][1]);
                            // Accessing the value from the session of store controller...
                            $vendor_id = Session::get('vendor_id');

                            // print_r($vendor_id);die;
                            $Accomodation_type->created_by = $vendor_id;

                            $Accomodation_type->created_date = date("Y-m-d H:i:s");          
                            $Accomodation_type->modified_date = date("Y-m-d H:i:s");
                            $Accomodation_type->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            //$this->country_save_before($Countries,$_POST);
                            // echo "<pre>";
                            // print_r($Accomodation_type);exit;
                            $Accomodation_type->save();

                   /*         $imageName = strtolower($Accomodation_type->id . '.' . $data->file('amenty_image')->getClientOriginalExtension());
                            $data->file('amenty_image')->move(
                                base_path() . '/public/assets/admin/base/images/Accomodation_type/', $imageName
                            );
                            $destinationPath2 = url('/assets/admin/base/images/Accomodation_type/'.$imageName.'');
                          
                            $size=getImageResize('VENDOR');
                            Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/Accomodation_type/list/'.$imageName)->destroy();
                            
                            
                            $Accomodation_type->amenty_image=$imageName;
                    
                            $Accomodation_type->save();
                    */
                        
                            $this->accomodation_type_save_after($Accomodation_type,$_POST);
                            Session::flash('message', trans('messages.Accomodation has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('managers/accomodation');
                    }
                }
              
                
                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function manager_accomodation_update(Request $data, $id)
                {
/*                    
                    print_r($id);
                    print_r($data->all());
                    exit;
*/                    

                  //  $fields['zone_code'] = Input::get('zone_code');
                   // $fields['amenty_image'] = Input::file('amenty_image');
                    //$fields['country'] = Input::get('country');
                    $rules = array(
                       // 'accomodation_type_name' => 'required',
                        //'amenty_image' => 'required',
                        // 'country' => 'required',
                    );
                    $accomodation_type_name = Input::get('accomodation_type_name');
                    foreach ($accomodation_type_name  as $key => $value) {
                        $fields['accomodation_type_name'.$key] = $value;
                        //  $rules['accomodation_type_name'.'1'] = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:accomodation_type_infos,accomodation_type_name,'.$id.',id';
                        $rules['accomodation_type_name'.'1'] = 'required|regex:/(^[A-Za-z\s ]+$)+/';
                        
                    }


                    $validator = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        
                        try{
                            
                            $Accomodation_type = Accomodation_type::find($id); 
                            $Accomodation_type->url_index =  $_POST['accomodation_type_name'][1] ? str_slug($_POST['accomodation_type_name'][1]): str_slug($_POST['accomodation_type_name'][1]);
                         //   $Accomodation_type->zone_code = $_POST['zone_code'];
                            //$Accomodation_type->country_id = 1;      /*  1-> Malaysia    $_POST['country'];   */    

                            $vendor_id = Session::get('vendor_id');

                            $Accomodation_type->created_by = $vendor_id;
                            $Accomodation_type->modified_date = date("Y-m-d H:i:s");
                            $Accomodation_type->default_status =  isset($_POST['status']) ? $_POST['status']: 0;
                            $Accomodation_type->save();
/*
                    if(isset($_FILES['amenty_image']['name']) && $_FILES['amenty_image']['name']!=''){
                                $imageName = strtolower($id . '.' . $data->file('amenty_image')->getClientOriginalExtension());
                                $data->file('amenty_image')->move(
                                    base_path() . '/public/assets/admin/base/images/Accomodation_type/', $imageName
                                );
                                $destinationPath2 = url('/assets/admin/base/images/Accomodation_type/'.$imageName.'');
                                $size=getImageResize('VENDOR');
                                Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/Accomodation_type/list/'.$imageName)->destroy();

                          
                               Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/detail/'.$imageName)->destroy();
                                Image::make( $destinationPath2 )->fit($size['THUMB_WIDTH'], $size['THUMB_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/'.$imageName)->destroy();
                                Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/city/thumb/detail/'.$imageName)->destroy();

                      
                        
                                $Accomodation_type->amenty_image = $imageName;

                                $Accomodation_type->save();
                            }
*/                            

                            $this->accomodation_type_save_after($Accomodation_type,$_POST);
                            Session::flash('message', trans('messages.Accomodation has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('managers/accomodation');
                    }
                }

               
               
                /**
                 * add,edit datas  saved in main table 
                 * after inserted in sub tabel.
                 *
                 * @param  int  $id
                 * @return Response
                 */
               public static function manager_accomodation_type_save_after($object,$post)
               {
                    $city = $object;
                    $post = $post;
                    if(isset($post['accomodation_type_name'])){
                        $accomodation_type_name = $post['accomodation_type_name'];
                        try{                
                            $affected = DB::table('accomodation_type_infos')->where('id', '=', $object->id)->delete();
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
                                if(isset($accomodation_type_name[$lang->id]) && $accomodation_type_name[$lang->id]!=""){
                                    $infomodel = new Accomodation_type_infos;
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->id = $object->id; 
                                    $infomodel->accomodation_type_name = $accomodation_type_name[$lang->id];
                                    $infomodel->save();
                                }
                            }
                            }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                    }
               }
                
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function manager_accomodation_destroy($id)
                {
                    if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    }
                    $data = Accomodation_type::find($id);
                    $data->delete();
                    Session::flash('message', trans('messages.Accomodation has been deleted successfully!'));
                    return Redirect::to('managers/accomodation');
                }

                
                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxManagerAccomodation_type()
                {
                                                          
                    //  $country=getCountryLists();
                $query = '"accomodation_type_infos"."language_id" = (case when (select count(*) as totalcount from accomodation_type_infos where accomodation_type_infos.language_id = '.getAdminCurrentLang().' and accomodation_type.id = accomodation_type_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                $Accomodation_type = Accomodation_type::Leftjoin('accomodation_type_infos','accomodation_type_infos.id','=','accomodation_type.id')
            ->select(DB::raw('accomodation_type.* ,accomodation_type.id as cid'),'accomodation_type_infos.*')
                    ->whereRaw($query)
                    ->orderBy('accomodation_type.id', 'desc')
                    ->get();

                    return Datatables::of($Accomodation_type)->addColumn('action', function ($Accomodation_type) {
                            return '<div class="btn-group"><a href="'.URL::to("managers/accomodation/edit/".$Accomodation_type->cid).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("managers/accomodation/delete/".$Accomodation_type->cid).'" class="delete-'.$Accomodation_type->cid.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                    </ul>
                                </div><script type="text/javascript">
                                $( document ).ready(function() {
                                $(".delete-'.$Accomodation_type->cid.'").on("click", function(){
                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                });});</script>';
                        })
                        ->addColumn('default_status', function ($Accomodation_type) {
                            if($Accomodation_type->default_status==0):
                                $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                            elseif($Accomodation_type->default_status==1):
                                $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                            endif;
                            return $data;
                        })
                        ->addColumn('accomodation_type_name', function ($Accomodation_type) {
                                $data = ucfirst($Accomodation_type->accomodation_type_name);
                            return $data;
                        })
                        ->make(true);
                }



}
