<?php 
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Model\contactus;
use App\Model\contactuses;
//use PushNotification;
use Illuminate\Support\Facades\Redirect;
use DB;
use Session;
use App;
use URL;
use Illuminate\Support\Contracts\ArrayableInterface;
use Illuminate\Support\Facades\Text;
use App\Model\users;
use App\Model\api;
use App\Model\admin_customers as Admincustomer;
use App\Model\zones;
use App\Model\zones_infos;
use App\Model\rooms;
use App\Model\booking_detail;
use App\Model\booked_room_details;
use App\Model\booking_charge;
use App\Model\payment;
use App\Model\booking_infos;
use App\Model\room_type;
use App\Model\room_type_infos;
use Hash;
use App\Model\subscription_transaction as Subscriptiontransaction;
use App\Model\subscriptions as Subscriptionrenewal;
use App\Model\subscription_payment;
use App\Model\outlet_reviews;
use App\Model\wallet_payment;
use App\Model\wallet_transaction;
use App\Model\user_payment;
use App\Model\user_transaction;
use App\Model\user_wallets;


class Common extends Controller
{
    const USER_FORGOT_PASSWORD_TEMPLATE = 36;
    const USERS_SIGNUP_EMAIL_TEMPLATE = 1;
    const USERS_WELCOME_EMAIL_TEMPLATE = 3;
    const COMMON_MAIL_TEMPLATE = 8;
    const ADMIN_MAIL_TEMPLATE_CONTACT = 23;
    const USER_MAIL_TEMPLATE_CONTACT = 15;
    const VENDORS_REGISTER_EMAIL_TEMPLATE = 4;
    const USER_BOOKING_VERIFICATION_TEMPLATE = 2;
    const USER_SIGNUP_VERIFICATION_TEMPLATE = 5;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct(Request $data) 
     {
        $post_data = $data->all();
        if(isset($post_data['language']) && $post_data['language']!='' && $post_data['language']==2) {
            App::setLocale('ar');
        }  else {
            App::setLocale('en');
        }
    }
    
    /*
     * login
     */
    public function login_user(Request $data)
    {
        //echo "asdfasdf";exit;
        $post_data = $data->all();
        //print_r($post_data);exit;
        $rules = [
            'email'    => ['required', 'email'],
            'password' => ['required'],
            'user_type'    => ['required'],
            'device_id'    => ['required_unless:user_type,1,2'],
            'device_token' => ['required_unless:user_type,1,2'],
        ];
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails()) 
        {
            $j = 0;
            foreach( $validator->errors()->messages() as $key => $value) 
            {
                $errors[] = is_array($value)?implode( ',',$value ):$value;
            }
            $errors = implode( ", \n ", $errors );
            $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => $errors));
            
        }
        else 
        {
            $user_data = Api::login_user($post_data);
           
            if(count($user_data) > 0)
            {
                $user_data = $user_data[0];
                /*if($_POST['user_type'] == 4 || $_POST['user_type'] == 5)
                {
                    $res = DB::table('users')
                            ->where('id', $students_data->id)
                            ->update(['device_id' => $_POST['device_id'],'device_token' => $_POST['device_token']]);
                }*/
                $token = JWTAuth::fromUser($user_data,array('exp' => 200000000000));
                $result = array("response" => array("httpCode" => 200, "status" => true, "Message" => trans("messages.User Logged-in Successfully"), "user_id" => $user_data->id, "token" => $token, "email" => $user_data->email, "social_title" => $user_data->social_title, "first_name" => $user_data->first_name, "last_name" => $user_data->last_name, "image" => $user_data->image));
            }
            else 
            {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.These credentials do not match our records")));
            }
        }
        return json_encode($result);
    }
    public function get_payment_gateways($language_id)
    {
        //echo $language_id;
        $query = '"payment_gateways_info"."language_id" = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $gateways = DB::table('payment_gateways')
                ->select('*','payment_gateways.id as payment_gateway_id')
                ->leftJoin('payment_gateways_info','payment_gateways_info.payment_id','=','payment_gateways.id')
                ->orderBy('payment_gateways.id', 'desc')
                 ->where('active_status',"=",1)
                ->whereRaw($query)
                ->get();
        //print_r($gateways);exit;
        return $gateways;
    }
    public function languages()
    {
        $module_settings_data = modules_list();
        $languages = DB::table('languages')->select('name','language_code','is_rtl','created_at','id')->where('status', 1)->orderby("languages.id","asc")->get();
        $language = getCurrentLang();
        $squery = '"settings_infos"."language_id" = (case when (select count(settings_infos.language_id) as totalcount from settings_infos where settings_infos.language_id = '.$language.' and settings.id = settings_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
        $general_settings = DB::table('settings')
                            ->select('settings_infos.site_name','default_language','default_country','contact_address','settings_infos.copyrights','site_owner','email','telephone','fax','geocode','settings_infos.site_description','default_currency','currency_side')
                            ->whereRaw($squery)
                            ->leftJoin('settings_infos','settings_infos.id','=','settings.id')
                            ->first();

                            
        $social_settings = DB::table('socialmediasettings')->select('facebook_page','twitter_page','linkedin_page')->first();
         $delivery_settings = DB::table('delivery_settings') ->first();
        $currency_list = DB::table('currencies')
                        ->select('id','currencies_infos.currency_name','currencies_infos.language_id','currencies_infos.currency_symbol')
                        ->join('currencies_infos','currencies_infos.currency_id','=','currencies.id')
                        ->get();
             $currency =array();           
                if(count($currency_list)>0)
                {    
                    foreach($currency_list as $cur)
                    {  if($cur->language_id == 1){
                        $currency['id'] = $cur->id; 
                        $currency['currency_symbol'] = $cur->currency_symbol; 
                        $currency['currency_name'] = $cur->currency_name; 
                        }
                        if($cur->language_id == 2){
                        $currency['id'] = $cur->id; 
                        $currency['currency_symbol_arabic'] = $cur->currency_symbol; 
                        $currency['currency_name_arabic'] = $cur->currency_name; 
                        }
                        
                    }   
                }
        $result = array("response" => array("httpCode" => 200, "status" => true, 'languages' => $languages, 'modules_list' => $module_settings_data, "general_settings" => $general_settings, "social_settings" => $social_settings,"currency_list"=> array($currency),"delivery_settings"=> $delivery_settings));
        return json_encode($result);
    }
    public function payment_gateway_list(Request $data)
    {
        //echo $language_id;
        $post_data = $data->all();
        $language_id = $post_data["language"];
        $query = '"payment_gateways_info"."language_id" = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $payment_detail = DB::table('payment_gateways')
                ->select('*','payment_gateways.id as payment_gateway_id')
                ->leftJoin('payment_gateways_info','payment_gateways_info.payment_id','=','payment_gateways.id')
                ->orderBy('payment_gateways.id', 'desc')
                 ->where('active_status',"=",1)
                ->whereRaw($query)
                ->get();
        //print_r($gateways);exit;
        if(count($payment_detail)>0)
        {
            foreach($payment_detail as $k=>$v)
            {
                $payment_detail[$k]->currency_id = ($payment_detail[$k]->currency_id!='')?$payment_detail[$k]->currency_id:'';
                $payment_detail[$k]->image = ($payment_detail[$k]->image!='')?$payment_detail[$k]->image:'';
            }
        }
        $result = array("response" => array("httpCode" => 200 , "Message" => trans("messages.Payment detail"), "payment_detail"=>$payment_detail));
        return json_encode($result,JSON_UNESCAPED_UNICODE);
    }
    public function currency_converter(Request $data)
    { 
        $data_all = $data->all();
        $rules = [
            'user_id'         => ['required'],
            'token'           => ['required'],
            'currency_amount' => ['required'],
            'from_currency'   => ['required'],
        ];
        $errors = $result = array();

        $validator = app('validator')->make($data->all(), $rules);
        if ($validator->fails()) 
        {
            foreach( $validator->errors()->messages() as $key => $value) 
            {
                $errors[] = is_array($value)?implode( ',',$value ):$value;
            }
            $errors = implode( ", \n ", $errors );
            $result = array("response" => array("httpCode" => 400, "Status" => "Failure", "Message" => $errors, "Error" => trans("messages.Error List")));
        }
        else {
            try {
                $check_auth      = JWTAuth::toUser($data_all['token']);
                $currency_amount = $data->currency_amount;
                $from_currency   = $data->from_currency;
                $to_currency     = 'USD';
                $amount          = urlencode($currency_amount);
                $from_currency   = urlencode($from_currency);
                $to_currency     = urlencode($to_currency);
                $get             = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from_currency&to=$to_currency");
                $get              = explode("<span class=bld>",$get);
                $converted_amount = $amount;
                if(isset($get[1]))
                {
                    $get = explode("</span>",$get[1]);
                    if(isset($get[0]))
                    {
                        $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
                    }
                    $result = array("response" => array("httpCode" => 200, "status" => "Success", "Message" => trans("messages.Currency converted successfully"), "converted_amount" => $converted_amount, "to_currency" => 'USD'));
                }
                else {
                    $result = array("response" => array("httpCode" => 400, "status" => "Failure", "Message" => trans("messages.Currency converted failed")));
                }
            }
            catch(JWTException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => "Failure", "Message" => trans("messages.Kindly check the user credentials")));
            }
            catch(TokenExpiredException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => "Failure", "Message" => trans("messages.Kindly check the user credentials")));
            }
        }
        return json_encode($result);
    }
    public function mob_faq($language)
    {
        
        $query = 'cms_infos.language_id = (case when (select count(cms_infos.language_id) as totalcount from cms_infos where cms_infos.language_id = '.$language.' and cms.id = cms_infos.cms_id) > 0 THEN '.$language.' ELSE 1 END)';
        $cms = DB::table('cms')->select('cms.id','cms.url_index','cms.sort_order','cms_infos.title')
            ->leftJoin('cms_infos','cms_infos.cms_id','=','cms.id')
            ->whereRaw($query)
            ->where('cms.cms_type','=',2)
            ->where('cms.cms_status','=',1)
            //->where('cms.url_index','=',$index)
            ->orderBy('cms.sort_order', 'asc')
            ->get();
            $cms_items=array();
            
                if(count($cms)){
                    $result = array("response" => array("httpCode" => 200, "status" => true,'data'=>$cms,'Message' => trans('messages.Faq list')));
                }
        
            return json_encode($result);
            
    }
    public function banners()
    { 
        
        $banners = DB::table('banner_settings')->select('banner_settings.banner_setting_id','banner_settings.banner_title','banner_settings.banner_subtitle','banner_settings.banner_image','banner_settings.banner_link')->where('banner_type', 2)->where('status', 1)->orderBy('default_banner', 'desc')->get();

        if(count($banners)>0)
        {   

            foreach($banners as $ban=>$items)
            {
            $banner_image = URL::asset('assets/admin/base/images/no_image.png');
            if(file_exists(base_path().'/public/assets/admin/base/images/banner/'.$items->banner_image) && $items->banner_image != '')
            {
            $banner_image = url('/assets/admin/base/images/banner/'.$items->banner_image);
            }
            $banners[$ban]->banner_image = $banner_image;
            }   
            $result = array("response" => array("httpCode" => 200 , "Message" => trans("messages.Banner details"),"banners"=> $banners));    
        }
        else 
        {
            $result = array("response" => array("httpCode" => 400 , "Message" => trans("messages.Invalid banner"), "banners"=>$banners));

        }
        return json_encode($result);    
    }

    public function getcity_categories()
    {

        $city_details = DB::table('cities')->Join('cities_infos','cities_infos.id','=','cities.id')
        ->leftjoin('outlets','outlets.city_id','=','cities.id')
        ->select('cities.id','cities.url_index','cities.city_image','cities_infos.city_name',DB::raw('count(outlets.id) as outlet_count'))
        //  ->leftjoin('room_type','room_type.property_id','=','outlets.id')
        ->groupBy('cities_infos.city_name')->groupBy('cities.id')
        //  ->orderby('outlet_count','desc')->groupBy('outlets.id')
        ->get();

         // return $city_details;

        $categories = DB::table('categories')->Join('categories_infos','categories_infos.category_id','=','categories.id')
        ->where('categories.category_type','=',7)
        ->select('categories.id','categories_infos.category_name','categories.image','categories.url_key')->get();

        $banners = DB::table('banner_settings')->where('status','=',1)->get();

        //  return $banners;
        
        $result = array("response" => array("httpCode" => 200 , "Message" => trans("messages.City and Category details"),"cities"=> $city_details,"categories"=> $categories,"banners" => $banners));

        return json_encode($result); 
    }

    public function signup_user(Request $data)
    {
        $post_data = $data->all();

        //  return $post_data;

        $user = DB::table('admin_customers')
                    ->where('email',$post_data['email'])
                    ->where('mobile_number',$post_data['mobile'])
                    ->where('country_code',$post_data['country_code'])
                    ->where('otp_verify_status',0)
                    ->first();

        //  return $user;

        if($user)
        {

                $book_user = Admincustomer::where('id','=',$user->id)->first();

                 // return $book_user;

                $otp = getOTPNumber(6);

                //  $mobile = $book_user->country_code.trim($book_user->mobile_number);
                $mobile = $post_data['country_code'].trim($post_data['mobile']);

                $msg = DB::table('sms_templates')
                            ->select('message')
                            ->where('id','=',self::USER_SIGNUP_VERIFICATION_TEMPLATE)
                            //  ->where('id','=',self::USER_BOOKING_VERIFICATION_TEMPLATE)
                            ->first();

                $result = $msg->message;
                $default_sitename = getAppConfig()->site_name;
                $message = str_replace('${otp_number}',$otp,$result);
                $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                $result = send_sms($mobile,$message,$result);

              

                if($result == 2){

                    $book_user->otp_number = $otp;
                    $book_user->save();

                   $result = array("response" => array("httpCode" => 700, "status" => true, "Message" => trans("messages.Please confirm your otp number for your account activation in ".getAppConfig()->site_name), "user_id" => $user->id));           
                }  

                                                 

            return json_encode($result);
        }


        $rules = [
            'email'      => ['required', 'email', 'max:250', 'unique:admin_customers,email'],
            'password'   => ['required','min:6'],
            'mobile'        => ['required','numeric', 'unique:admin_customers,mobile_number'],
            'login_type'   => ['required'],
            'terms_condition' => ['required_if:login_type,1'],
        ];
        $error = $result = array();
        $messages = array(
        'terms_condition.required_if' => 'The Terms and Condition field is required.',
        );
        $validator = app('validator')->make($post_data, $rules , $messages);

        if ($validator->fails()) {
            
            foreach ($validator->errors()->messages() as $key => $value)
            {
                $error[] = is_array($value) ? implode(',', $value) : $value;
            }
            $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));
        } else {
            //To get otp number
            $otp = getOTPNumber(6);

            $mobile = $post_data['country_code'].trim($post_data['mobile']);
            $msg = 'Your otp to verify your account in '.getAppConfig()->site_name.' is '.$otp;
            $result = send_sms($mobile,$msg);

            if($result == 2){
                
                $user = new Admincustomer();
                $user->email = $post_data['email'];
                $user->hash_password = md5($post_data['password']);
                $user->country_code = $post_data['country_code'];
                $user->mobile_number = $post_data['mobile'];
                $user->login_type = $post_data['login_type'];
                $user->otp_number = $otp;
                $user->customer_unique_id = getRandomBookingNumber(8);
                $user->referred_customer_id = isset($post_data['user_id'])?$post_data['user_id']:0;
                $user->save();

                $last_insert_id = $user->id;
                $result = array("response" => array("httpCode" => 200, "last_id" => $last_insert_id ,     "status" => true, 
                "Message" => trans("messages.Please confirm your otp number for your account activation in ".getAppConfig()->site_name))); 
            }  else {
                $result = array("response" => array("httpCode" => 300, "status" => false, 
                "Message" => $result)); 
            } 
        }
        return json_encode($result);
    } 


    //  CREATE SHA-HASH SIGNATURE
    public function iPay88_signature($source)
    {
      return hash('sha256', $source);
    }   


     //  STORE GUEST [PAY NOW]

    public function signup_guest_now(Request $data)
    {
        //  $post_data = $data->all();

        return $post_data;

            $token = $post_data['_token'];


        //  IF USER ALREADY EXISTS [LOGGED IN]

            if(isset($post_data['user_id']))
                {
                    //  return 1;

                    $user_id = $post_data['user_id'];

                    $mobile = $post_data['mobile'];

                    $country_code = $post_data['country_code'];

                    $book_user = Admincustomer::where('id','=',$user_id)->first();

                    //  return json_encode($book_user);

                    $result = array("response" => array("httpCode" => 200, "status" => true,"Message" => trans("messages.Please confirm your otp number for your Booking verification in ".getAppConfig()->site_name." ".$book_user->otp_number)));                     

                    return json_encode($result);                    
                                               
                }

            else 
            {

                    //  GUEST USER SIGNUP

                    $rules = [
                        'user_name'  => ['required', 'alpha', 'max:250', 'unique:admin_customers,firstname'],
                        'email'      => ['required', 'email', 'max:250', 'unique:admin_customers,email'],
                        'mobile'     => ['required','numeric', 'unique:admin_customers,mobile_number'],
                        'login_type' => ['required'],
                    ];
                    $error = $result = array();
                    $messages = array(
                        'terms_condition.required_if' => 'The Terms and Condition field is required.',
                    );

                    $validator = app('validator')->make($post_data, $rules , $messages);


                    if ($validator->fails()) {
                        
                        foreach ($validator->errors()->messages() as $key => $value)
                        {
                            $error[] = is_array($value) ? implode(',', $value) : $value;
                        }
                        $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));

                        //  return json_encode($result);

                    } else {                   
                            
                            $user = new Admincustomer();
                            $user->email = $post_data['email'];
                            //  $user->hash_password = md5($post_data['password']);
                            $user->country_code = $post_data['country_code'];
                            $user->mobile_number = $post_data['mobile'];
                            $user->login_type = $post_data['login_type'];
                            $user->firstname = $post_data['user_name'];
                            $user->otp_number = $otp;
                            $user->otp_verify_status = 1;   
                            $user->save();
        
                            $user_id = $user->id;

                            //  $user_id = 17;

                            $user = Admincustomer::where('id','=',$user_id)->first();

                            //  return $user;

                            $result = array("response" => array("httpCode" => 200, "last_id" => $user_id ,
                                "user_email" => $user->email , "user_mobile" => $user->mobile_number,
                                "user_name" => $user->firstname,"country_code" => $user->country_code,
                                "token" => $token,"status" => true, 
                            "Message" => trans("messages.Please confirm your otp number for your Booking verification in ".getAppConfig()->site_name)));                            
                            
                    }

                    return json_encode($result);

            }
    }

    public function payment_details_page(Request $data)
    {
            $post_data = $data->all();
      
                  //    return $post_data;

            $otp_id = $post_data['verify_otp_id'];

            if(isset($post_data['new_user_id']))
            {
                  //    return 1;

                $user_id = $post_data['new_user_id'];
            }
            else
            {
                //  return 2;
                $user_id = $post_data['user_id'];
            }

           //   return $user_id;
            
            $outlet_id = $post_data['outlet_id'];
            $room_type_id = $post_data['room_type_id'];
            $check_in = $post_data['check_in'];
            $check_out = $post_data['check_out'];
            $room_count = $post_data['room_count'];
            $guest_count = $post_data['guest_count'];
            $no_of_nights = $post_data['no_of_nights'];
            $coupon_id = $post_data['coupon_id'];
            $coupon_amount = $post_data['coupon_amount'];
            $coupon_type = $post_data['coupon_type'];
            $original_price = $post_data['original_price'];
            $total_cost = $post_data['total_cost'];
            $vendor_id = $post_data['vendor_id'];


            
            $user = Admincustomer::find($user_id);

            //  return $user;

            $user_id = $user->id;
            $user_name =  $user->firstname;
            $user_email =  $user->email;
            $user_mobile =  $user->country_code."".$user->mobile_number;

            //  return array($user_mobile;

            //  PAYMENT GATEWAY DETAILS

            $payment_gateway_ipay88 = Subscriptionrenewal::get_payment_gateways(1,28);

            //  return $payment_gateway_ipay88;

            $payment_amount_price = $original_price;
            $payment_amount = $total_cost;
            $payment_title = 'Booking';
            $merchant_key = $payment_gateway_ipay88[0]->merchant_key;
            $merchant_code = $payment_gateway_ipay88[0]->account_id;
            $reference_no = getRandomNumber(12);

            //  $amount = str_replace('.', '', $payment_amount);  

            $amount = round($payment_amount);

            //  return $r;

            $currency = $payment_gateway_ipay88[0]->currency_code;
            $sign_src = $merchant_key.$merchant_code.$reference_no.$amount.$currency;

            //  return array($sign_src);

            $signature = $this->iPay88_signature($sign_src);
            //  return array($signature);

            if($payment_gateway_ipay88[0]->payment_mode==1){
              $action_url = 'https://www.mobile88.com/ePayment/entry.asp';
            } else {
              $action_url = 'https://www.mobile88.com/ePayment/entry.asp';
            }
            $response_url = env("APP_URL")."/api/booking_payment_response";
            $backend_url = env('APP_URL').'/api/response_booking_payment';
            //  echo $action_url; exit;

            //  return array($action_url);
            $html_pay = "<html xmlns='http://www.w3.org/1999/xhtml'>\n
            <head></head>\n
            <body>\n";
            $html_pay .=  "<form action='$action_url' method='post' name='frm'>\n";
            $html_pay .=   '<input type="hidden" name="MerchantCode" value="'.$merchant_code.'">';
            $html_pay .=   '<input type="hidden" name="RefNo" value="'.$reference_no.'">';
            $html_pay .=   '<input type="hidden" name="Amount" value="'.$amount.'">';
            $html_pay .=   '<input type="hidden" name="Currency" value="'.$currency.'">';
            $html_pay .=   '<input type="hidden" name="Lang" value="UTF-8">';
            $html_pay .=   '<input type="hidden" name="SignatureType" value="SHA256">';
            $html_pay .=   '<input type="hidden" name="Signature" value="'.$signature.'">';
            $html_pay .=   '<input type="hidden" name="ProdDesc" value="'.$payment_title.'">';
            $html_pay .=   '<input type="hidden" name="UserName" value="'.$user_name.'">';
            $html_pay .=   '<input type="hidden" name="UserEmail" value="'.$user_email.'">';
            $html_pay .=   '<input type="hidden" name="UserContact" value="'.$user_mobile.'">';
            $html_pay .=   '<input type="hidden" name="Remark" value="'.$user_id.'">';
            $html_pay .=   '<input type="hidden" name="ResponseURL" value="'.$response_url.'">';
            $html_pay .=   '<input type="hidden" name="BackendURL" value="'.$backend_url.'">';
            $html_pay .=   "</form>\n";
            $html_pay .=   "\t<script type='text/javascript'>\n";
            $html_pay .=   "\t\tdocument.frm.submit();\n";
            $html_pay .=   "\t</script>\n";
            $html_pay .= "</body>\n</html>";  
            //  echo $html_pay;exit;            

                //  return array($room_count);


         return json_encode($html_pay);
    }     

    //  STORE GUEST [PAY AT HOTEL]

    public function signup_guest(Request $data)
    {
        $post_data = $data->all();

        $token = $post_data['_token'];

        // return $post_data;


        //  IF USER ALREADY EXISTS [LOGGED IN]

        if(isset($post_data['user_id']))
            {
                //  return 1;

                $user_id = $post_data['user_id'];

                $mobile = $post_data['mobile'];

                $country_code = $post_data['country_code'];

                $book_user = Admincustomer::where('id','=',$user_id)->first();

                 // return $book_user;

                $otp = getOTPNumber(6);

                //  $mobile = $book_user->country_code.trim($book_user->mobile_number);
                $mobile = $post_data['country_code'].trim($post_data['mobile']);
                //  $msg = 'Your otp to verify your Booking in '.getAppConfig()->site_name.' is '.$otp;

                $msg = DB::table('sms_templates')
                            ->select('message')
                            ->where('id','=',self::USER_BOOKING_VERIFICATION_TEMPLATE)
                            ->first();

                $result = $msg->message;
                $default_sitename = getAppConfig()->site_name;
                $message = str_replace('${otp_number}',$otp,$result);
                $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                $result = send_sms($mobile,$message,$result);
/*                
                 $msg = 'Your otp to verify your Booking in '.getAppConfig()->site_name.' is '.$otp;
                 $result = send_sms($mobile,$msg);                
*/
                if($result == 2){

                    $book_user->otp_number = $otp;
                    $book_user->save();

                    $result = array("response" => array("httpCode" => 200, "status" => true,"Message" => trans("messages.Please confirm your otp number for your Booking verification in ".getAppConfig()->site_name)));              
                }
                else {
                    $result = array("response" => array("httpCode" => 300, "status" => false, 
                    "Message" => $result)); 
                 } 

                 return json_encode($result);
                                           
            }

                //  IF USER NOT VERIFIED 

                $user = DB::table('admin_customers')
                            ->where('email',$post_data['email'])
                            ->where('mobile_number',$post_data['mobile'])
                            ->where('country_code',$post_data['country_code'])
                            ->where('otp_verify_status',0)
                            ->first();

                //  return array($user);

                if($user)
                {

                        $book_user = Admincustomer::where('id','=',$user->id)->first();

                         // return $book_user;

                        $otp = getOTPNumber(6);

                        //  $mobile = $book_user->country_code.trim($book_user->mobile_number);
                        $mobile = $post_data['country_code'].trim($post_data['mobile']);

                        $msg = DB::table('sms_templates')
                                    ->select('message')
                                    ->where('id','=',self::USER_SIGNUP_VERIFICATION_TEMPLATE)
                                    //  ->where('id','=',self::USER_BOOKING_VERIFICATION_TEMPLATE)
                                    ->first();

                        $result = $msg->message;
                        $default_sitename = getAppConfig()->site_name;
                        $message = str_replace('${otp_number}',$otp,$result);
                        $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                        $result = send_sms($mobile,$message,$result);

                      

                        if($result == 2){

                            $book_user->otp_number = $otp;
                            $book_user->save();

                           $result = array("response" => array("httpCode" => 700, "status" => true, "Message" => trans("messages.Please confirm your otp number for your account activation in ".getAppConfig()->site_name), "user_id" => $user->id));           
                        }  

                                                         

                    return json_encode($result);
                }            

                //  IF USER ALREADY THERE MEANS [ SEND SMS TO HIM & LOGGED HIM]

                $user = DB::table('admin_customers')
                            ->where('email',$post_data['email'])
                            ->where('mobile_number',$post_data['mobile'])
                            ->where('country_code',$post_data['country_code'])
                            ->first();

                if($user)
                {
                    //  return 1;

                    $book_user = Admincustomer::where('id','=',$user->id)->first();

                    //  return array($book_user);

                    $otp = getOTPNumber(6);

                    $mobile = $post_data['country_code'].trim($post_data['mobile']);

                    $msg = DB::table('sms_templates')
                                ->select('message')
                                ->where('id','=',self::USER_BOOKING_VERIFICATION_TEMPLATE)
                                ->first();

                    $result = $msg->message;
                    $default_sitename = getAppConfig()->site_name;
                    $message = str_replace('${otp_number}',$otp,$result);
                    $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                    $result = send_sms($mobile,$message,$result);

                    if($result == 2){

                        $book_user->otp_number = $otp;
                        $book_user->save();

                        $result = array("response" => array("httpCode" => 200, "status" => true, 
                        "last_id" => $user->id ,"user_email" => $user->email ,
                        "user_mobile" => $user->mobile_number,"token" => $token,
                        "user_name" => $user->firstname,"country_code" => $user->country_code,
                        "Message" => trans("messages.Please confirm your otp number for your Booking verification in ".getAppConfig()->site_name)));              
                    }
                    else {
                        $result = array("response" => array("httpCode" => 300, "status" => false, 
                        "Message" => $result)); 
                    } 

                     return json_encode($result);                                            
                }

            //  GUEST USER SIGNUP

            $rules = [
                'email'      => ['required', 'email', 'max:250', 'unique:admin_customers,email'],
                'mobile'        => ['required','numeric', 'unique:admin_customers,mobile_number'],
                'login_type'   => ['required'],
                
            ];
            $error = $result = array();
            $messages = array(
                'terms_condition.required_if' => 'The Terms and Condition field is required.',
            );

            $validator = app('validator')->make($post_data, $rules , $messages);


            if ($validator->fails()) {
                
                foreach ($validator->errors()->messages() as $key => $value)
                {
                    $error[] = is_array($value) ? implode(',', $value) : $value;
                }
                $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));

            } else {
                //To get otp number
             // return $post_data;

                //  return 1;

                $token = $post_data['_token'];

                $otp = getOTPNumber(6);

                $mobile = $post_data['country_code'].trim($post_data['mobile']);

                $msg = DB::table('sms_templates')
                            ->select('message')
                            ->where('id','=',self::USER_BOOKING_VERIFICATION_TEMPLATE)
                            ->first();

                $result = $msg->message;
                $default_sitename = getAppConfig()->site_name;
                $message = str_replace('${otp_number}',$otp,$result);
                $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                $result = send_sms($mobile,$message,$result);

                //  $result = 2;

                if($result == 2){
                    
                    $user = new Admincustomer();
                    $user->email = $post_data['email'];
                    //  $user->hash_password = md5($post_data['password']);
                    $user->country_code = $post_data['country_code'];
                    $user->mobile_number = $post_data['mobile'];
                    $user->login_type = $post_data['login_type'];
                    $user->firstname = $post_data['user_name'];
                    $user->otp_number = $otp;
                    $user->otp_verify_status = 1; 
                    $user->customer_unique_id = getRandomBookingNumber(8);  
                    $user->save();

                    $last_insert_id = $user->id;
                    $result = array("response" => array("httpCode" => 200, "last_id" => $last_insert_id ,
                        "user_email" => $user->email , "user_mobile" => $user->mobile_number,
                        "user_name" => $user->firstname,"country_code" => $user->country_code,
                        "token" => $token,"status" => true, 
                    "Message" => trans("messages.Please confirm your otp number for your Booking verification in ".getAppConfig()->site_name)));
                }  else {
                    $result = array("response" => array("httpCode" => 300, "status" => false, 
                    "Message" => $result)); 
                } 
            }
            return json_encode($result);
    } 



    public function verify_user(Request $data)
    {
            $post_data = $data->all();
      
            //To get otp number

            //   return $post_data;
            $current_otp = $post_data['otp_number'];
            $otp_id = $post_data['verify_otp_id'];

            if($post_data['verify_user_id'] != 1)
            {
                $otp_id = $post_data['verify_user_id'];
            }

            //  return $otp_id;
            if($otp_id > 0){
                $user = Admincustomer::find($otp_id);
                if($user){
                    $old_otp = $user->otp_number;  
                    if($current_otp == $old_otp){
                         $user->otp_verify_status = 1;   
                         $user->save();

                         $result = array("response" => array("httpCode" => 200, "last_id" => $otp_id,
                            "status" => true, 
                        "Message" => "The otp verification is success."));
                    } else {
                         $result = array("response" => array("httpCode" => 400, "status" => false, 
                        "Message" => "The otp you have entered is not matched with our records."));
                    }
                } else {
                     $result = array("response" => array("httpCode" => 400, "status" => false, 
                "Message" => "We don't have any matching records for your request."));
                }
            } else {
                 $result = array("response" => array("httpCode" => 400, "status" => false, 
                "Message" => "We don't have any matching records for your request."));
            }
        return json_encode($result);
    } 
/*
    public function verify_user_old(Request $data)
    {
            $post_data = $data->all();
      
            //To get otp number

            //  return $post_data;
            $current_otp = $post_data['otp_number'];
            $otp_id = $post_data['verify_otp_id'];
            if($otp_id > 0){
                $user = Admincustomer::find($otp_id);
                if($user){
                    $old_otp = $user->otp_number;  
                    if($current_otp == $old_otp){
                         $user->otp_verify_status = 1;   
                         $user->save();

                         $result = array("response" => array("httpCode" => 200, "last_id" => $otp_id,
                            "status" => true, 
                        "Message" => "The otp verification is success."));
                    } else {
                         $result = array("response" => array("httpCode" => 400, "status" => false, 
                        "Message" => "The otp you have entered is not matched with our records."));
                    }
                } else {
                     $result = array("response" => array("httpCode" => 400, "status" => false, 
                "Message" => "We don't have any matching records for your request."));
                }
            } else {
                 $result = array("response" => array("httpCode" => 400, "status" => false, 
                "Message" => "We don't have any matching records for your request."));
            }
        return json_encode($result);
    } 

*/    

    public function verify_guest(Request $data)
    {
            $post_data = $data->all();
      
            //To get otp number
            $current_otp = $post_data['otp_number'];
            $otp_id = $post_data['verify_otp_id'];
            if($otp_id > 0){
                $user = Admincustomer::find($otp_id);
                if($user){
                    $old_otp = $user->otp_number;  
                    if($current_otp == $old_otp){
                         $user->otp_verify_status = 1;   
                         $user->save();

                         $result = array("response" => array("httpCode" => 200, "last_id" => $otp_id,
                            "status" => true, 
                        "Message" => "The otp verification is success."));
                    } else {
                         $result = array("response" => array("httpCode" => 400, "status" => false, 
                        "Message" => "The otp you have entered is not matched with our records."));
                    }
                } else {
                     $result = array("response" => array("httpCode" => 400, "status" => false, 
                "Message" => "We don't have any matching records for your request."));
                }
            } else {
                 $result = array("response" => array("httpCode" => 400, "status" => false, 
                "Message" => "We don't have any matching records for your request."));
            }
        return json_encode($result);
    }    

    public function resend_otp(Request $data)
    {
            $post_data = $data->all();

              return $post_data;

            //  BOOKING RESEND OTP
        if(isset($post_data['user_id']))
            {
                $user_id = $post_data['user_id'];

                if($user_id > 0){
                    $user = Admincustomer::find($user_id);
                    //return json_encode($user);
                    if($user){
                        $old_otp = $user->otp_number; 

                        $mobile = $user->country_code.trim($user->mobile_number);
                        //  $msg = 'Your otp to verify your Booking in '.getAppConfig()->site_name.' is '.$old_otp;
                        //  $result = send_sms($mobile,$msg);


                        $msg = DB::table('sms_templates')
                                    ->select('message')
                                    //  ->where('id','=',self::USER_SIGNUP_VERIFICATION_TEMPLATE)
                                    ->where('id','=',self::USER_BOOKING_VERIFICATION_TEMPLATE)
                                    ->first();

                        //  $otp = $user_details[0]->otp_number;

                        $result = $msg->message;
                        $default_sitename = getAppConfig()->site_name;
                        $message = str_replace('${otp_number}',$old_otp,$result);
                        $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                        $result = send_sms($mobile,$message,$result);

                        //  $result = 2;    // TEMP SMS NOT WORKING DONE BY AK

                        if($result == 2){
                             $result = array("response" => array("httpCode" => 200, "status" => true, 
                            "Message" => trans("messages.Please confirm your otp number for your Booking activation in ".getAppConfig()->site_name))); 
                        } else {
                            $result = array("response" => array("httpCode" => 400, "status" => false, 
                            "Message" => $result)); 
                        }    
                    } else {
                         $result = array("response" => array("httpCode" => 400, "status" => false, 
                    "Message" => "We don't have any matching records for your request."));
                    }
                } else {
                     $result = array("response" => array("httpCode" => 400, "status" => false, 
                    "Message" => "We don't have any matching records for your request."));
                }
            return json_encode($result);
        }
            //To get otp number
            $otp_id = $post_data['verify_otp_id'];
            if($otp_id > 0){
                $user = Admincustomer::find($otp_id);
                //return json_encode($user);

                //  return $user;
                if($user){
                    $old_otp = $user->otp_number; 

                    //  return $old_otp;

                    $mobile = $user->country_code.trim($user->mobile_number);
                     // $msg = 'Your otp to verify your account in '.getAppConfig()->site_name.' is '.$old_otp;
                    //  $result = send_sms($mobile,$msg);

                    

                    $msg = DB::table('sms_templates')
                                ->select('message')
                                ->where('id','=',self::USER_SIGNUP_VERIFICATION_TEMPLATE)
                                //  ->where('id','=',self::USER_BOOKING_VERIFICATION_TEMPLATE)
                                ->first();

                    //  $otp = $user_details[0]->otp_number;

                    $result = $msg->message;
                    $default_sitename = getAppConfig()->site_name;
                    $message = str_replace('${otp_number}',$old_otp,$result);
                    $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                    $result = send_sms($mobile,$message,$result);                     

                    //  return $result;

                    if($result == 2){
                         $result = array("response" => array("httpCode" => 200, "status" => true, 
                        "Message" => trans("messages.Please confirm your otp number for your account activation in ".getAppConfig()->site_name))); 
                    } else {
                        $result = array("response" => array("httpCode" => 400, "status" => false, 
                        "Message" => $result)); 
                    }    
                } else {
                     $result = array("response" => array("httpCode" => 400, "status" => false, 
                "Message" => "We don't have any matching records for your request."));
                }
            } else {
                 $result = array("response" => array("httpCode" => 400, "status" => false, 
                "Message" => "We don't have any matching records for your request."));
            }
        return json_encode($result);
    } 

    public function update_user(Request $data)
    {
        $post_data = $data->all();

        //  return $post_data;
        $rules = [
            'firstname'      => ['required','regex:/[A-Za-z\s ]+/'],
            'lastname'   => ['required','regex:/[A-Za-z\s ]+/'],
            'gender'        => ['required'],
            'image'   => ['nullable','mimes:jpeg,jpg,png,gif','max:2048'],
        ];
        $error = $result = array();
        $validator = app('validator')->make($post_data, $rules);

         if ($validator->fails()) {

            foreach ($validator->errors()->messages() as $key => $value)
            {
                $error[] = is_array($value) ? implode(',', $value) : $value;
            }
            $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));
        } else {
            $user_id = $post_data['user_input_id'];
            if($user_id > 0){
                    $user = Admincustomer::find($user_id);
                    $user->firstname = $post_data['firstname'];
                    $user->lastname = $post_data['lastname'];
                    $user->gender = $post_data['gender'];
                    $user->save();

                    $token = $post_data['_token'];
                    $user_id = $user->id;
                    $user_name = $user->firstname;
                    $user_email = $user->email;
                    $user_mobile = $user->mobile_number;
                    $country_code = $user->country_code;
                         

                $result = array("response" => array("httpCode" => 200, "user_id" => $user_id,
                    "token" => $token,"user_name" => $user_name,"user_email" => $user_email
                    ,"user_mobile" => $user_mobile,"country_code" => $country_code,"status" => true, 
                "Message" => "User Details has been updated successfully."));
            } else {
                 $result = array("response" => array("httpCode" => 400, "status" => false, 
                "Message" => "We don't have any matching records for your request."));
            }
        }

        return json_encode($result);
    }

    public function storecontact(Request $data)
    {
        $post_data = $data->all();

        // return $post_data;

        $rules = [
            'name'      =>  ['required', 'regex:/(^[A-Za-z0-9 ]+$)+/', 'max:56'],
            'email'   => ['required', 'email', 'max:250'],
            'mobile'        => ['required', 'regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/'],
            'message'   => 'required',
        ];
        $error = $result = array();
        $validator = app('validator')->make($post_data, $rules);

        if ($validator->fails()) {

            foreach ($validator->errors()->messages() as $key => $value)
            {
                $error[] = is_array($value) ? implode(',', $value) : $value;
            }
            $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));
        } 
        else {
                    
                    $data_all  = $data->all();

                    // return $data_all;
                    $contactus = new Contactuses;
                    $contactus->name         = $data_all['name'];
                    $contactus->email        = $data_all['email'];
                    $contactus->country_code = $data_all['country_code'];
                    $contactus->phone_number = $data_all['mobile'];
                    $contactus->message      = $data_all['message'];
                    // $contactus->enquery_type = $data_all['enquery_type'];
                    //$contactus->city_id      = $data_all['city'];
                    $contactus->created_at   = date("Y-m-d H:i:s");
                    // print_r($contactus);exit;
                    $contactus->save();
                    
                    // return $data_all['message'];
                    //$city_details = get_city_details($data_all['city']);
                    $app_config   = getAppConfig();
                    $email_config = getAppConfigEmail();
                    //$city_name    = isset($city_details->city_name)?$city_details->city_name:'-';
                    //sending contact mail request to admin
                    $to       = $email_config->support_mail;
                    $email    = $data_all['email'];
                    $subject  = "Contact request from ". $app_config->site_name." by ".$data_all['name'];
                    $content  = $data_all['message'];
                    // return $content;
                    $reply_to = $data_all['email'];
                    $template = DB::table('email_templates')
                                ->select('from_email', 'from', 'subject', 'template_id','content')
                                ->where('template_id','=',self::ADMIN_MAIL_TEMPLATE_CONTACT)
                                ->get();
                                // return 1;
                                // return $template;
                    // $value = isset($array['my_index']) ? $array['my_index'] : '';
                    // $value = array_key_exists('template_id', $template) ? $template['template_id'] : '';
                    if(count($template))
                    {
                        $from = $template[0]->from_email;
                        $from_name = $template[0]->from;
                        //$subject = $template[0]->subject;
                        if(!$template[0]->template_id)
                        {
                            $template  = 'mail_template';
                            $from      = $email_config->contact_mail;
                            $subject   = "Welcome to ".$app_config->site_name;
                            $from_name = "";
                        }
                        // return $template;
                        $content = array('name' => $data_all['name'],'email' => $data_all['email'],'phone_number' => $data_all['mobile'],/*'enquery_type' => $enquery_type,*/'message' => $data_all['message']);
                        $email = smtp($from,$from_name,explode(',',$to),$subject,$content,$template,'',$reply_to);
                        // return $content;
                    }

                    
                    $to       = $data_all['email'];
                    $email    = $data_all['email'];
                    $subject  = "Contact request reply from ". $app_config->site_name;
                    $content  = $data_all['message'];
                    $template = DB::table('email_templates')
                                ->select('from_email', 'from', 'subject', 'template_id','content')
                                ->where('template_id','=',self::USER_MAIL_TEMPLATE_CONTACT)
                                ->get();
                
                    if(count($template))
                    {
                        $from      = $template[0]->from_email;
                        $from_name = $template[0]->from;
                        if(!$template[0]->template_id)
                        {
                            $template  = 'mail_template';
                            $from      = $email_config->contact_mail;
                            $subject   = "Welcome to ".$app_config->site_name;
                            $from_name = "";
                        }
                        $content = array("notification" => array('name' => $data_all['name'],'email' => $data_all['email'],'message' => $data_all['message'],'mobile' => $data_all['mobile']));
                        $email=smtp($from,$from_name,$to,$subject,$content,$template);
                    }

                    $result = array("response" => array("httpCode" => 200, "status" => true, 
                        'Message' => 'Your request has been posted successfully.'));
            }

        return json_encode($result);
    }


    public function signin_user(Request $data)
    {
            $post_data = $data->all();

            //   return $post_data;

            $rules = [
                'password'     => ['required'],
                'email_mobile' => ['required'],
            ];
            $error = $result = array();
            $messages = array(
                'terms_condition.required_if' => 'The Terms and Condition field is required.',
            );

            $validator = app('validator')->make($post_data, $rules , $messages);


            if ($validator->fails()) {
                
                foreach ($validator->errors()->messages() as $key => $value)
                {
                    $error[] = is_array($value) ? implode(',', $value) : $value;
                }
                $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));

                return json_encode($result);

            }
            else
            {
                
                $email_mobile = $post_data['email_mobile'];
                $password = md5($post_data['password']);

                //  return array($password);
                $token = $post_data['_token'];                
                $user_details = Admincustomer::where('email',$email_mobile)
                                ->where('hash_password',$password)
                                ->orWhere('mobile_number',$email_mobile)
                                ->get();

                //  return $user_details; 

                if(count($user_details) >0){
                    $otp = $user_details[0]->otp_verify_status;  
                    if($otp == 1){
                         $result = array("response" => array("httpCode" => 200, "status" => true,
                            "user_id" => $user_details[0]->id,"user_mobile" => $user_details[0]->mobile_number,
                            "user_name" => $user_details[0]->firstname, "user_email" => $user_details[0]->email,
                            "country_code" => $user_details[0]->country_code, "token" => $token,
                            "image" => $user_details[0]->image,
                            "Message" => "You have logged in successfully."));
                    } else {
                        
                        $mobile = $user_details[0]->country_code.trim($user_details[0]->mobile_number);
                        // $msg = 'Your otp to verify your account in '.getAppConfig()->site_name.' is '.$user_details[0]->otp_number;
                        // $result = send_sms($mobile,$msg);

                        $msg = DB::table('sms_templates')
                                    ->select('message')
                                    ->where('id','=',self::USER_SIGNUP_VERIFICATION_TEMPLATE)
                                    //  ->where('id','=',self::USER_BOOKING_VERIFICATION_TEMPLATE)
                                    ->first();

                        $otp = $user_details[0]->otp_number;

                        $result = $msg->message;
                        $default_sitename = getAppConfig()->site_name;
                        $message = str_replace('${otp_number}',$otp,$result);
                        $message = str_replace('${SITE_NAME}',$default_sitename,$message);
                        $result = send_sms($mobile,$message,$result);                        

                        if($result == 2){
                             $result = array("response" => array("httpCode" => 500,"user_id" => $user_details[0]->id,"status" => false, 
                            "Message" => trans("messages.Your account is not verified.Please confirm your otp number for your account activation in ".getAppConfig()->site_name))); 
                        } else {
                            $result = array("response" => array("httpCode" => 500, "status" => false, 
                                "Message" => "Your account is not verified.Try with different Login.")); 
                        }
                    }
                } else {
                     $result = array("response" => array("httpCode" => 300, "status" => false, 
                                     "Message" => "These credentials do not match our records."));
                }
                
            return json_encode($result);
          }
    } 

    public function forgot_password(Request $data)
    {
        $post_data = $data->all();

            $rules = [
                'email' => ['required', 'email']
            ];
            $error = $result = array();
            $messages = array(
                'terms_condition.required_if' => 'The Terms and Condition field is required.',
            );

            $validator = app('validator')->make($post_data, $rules , $messages);


            if ($validator->fails()) {
                
                foreach ($validator->errors()->messages() as $key => $value)
                {
                    $error[] = is_array($value) ? implode(',', $value) : $value;
                }
                $result = array("response" => array("httpCode" => 400, "status" => false, "Error" => trans("messages.Error List"), "Message" => $error));

                return json_encode($result);

            }
            else
            {

                $email = $post_data['email'];

                $user_details = Admincustomer::where('email',$email)->get(); 


                if(count($user_details) > 0){
                    $password = getRandomNumber(6);
                    $hash_password = md5($password);
                    $users = Admincustomer::find($user_details[0]['id']);
                    $users->hash_password = $hash_password;
                    $users->save();

                    $template = DB::table('email_templates')
                              ->select('*')
                              ->where('template_id','=',self::USER_FORGOT_PASSWORD_TEMPLATE)
                              ->get();
                  $template = iterator_to_array($template);                      

                   if(count($template))
                   {
                      $from = 'nextbrain.sender@gmail.com';
                      $from_name=$template[0]->from;
                      $subject = $template[0]->subject;
                      $to = 'gopalsrinivasan1995@gmail.com';
                      $login_link = URL::to('/');
                      $user_name = $user_details[0]['firstname'].' '.$user_details[0]['lastname'];
                      if(!$template[0]->template_id)
                      {
                          $template = 'mail_template';
                          $from=getAppConfigEmail()->contact_email;
                          $subject = "Welcome to ".getAppConfig()->site_name;
                          $from_name="";
                      }
                             
                     $content = array("user_name" => $user_name,"login_link" => $login_link,"new_password" => 
                        $password);  
                     $email=smtp($from,$from_name,$to,$subject,$content,$template); 
                           
                      if($email){
                        $result = array("response" => array("httpCode" => 200, "status" => true, 
                                    "Message" => "New password has been sent to your email."));
                      } else {
                        $result = array("response" => array("httpCode" => 400, "status" => false, 
                                    "Message" => "We can't able to send the email.Please Sign in with different login or try again later."));
                      }     
                    }        

                } else {
                    $result = array("response" => array("httpCode" => 400, "status" => false, 
                                    "Message" => "These email do not match our records."));
                }

                return json_encode($result);

            }
    }

    public function city_property_listing($url_index)
    {

        //To get the city name
        $city_name = DB::table('cities')
            ->Join('cities_infos','cities_infos.id','=','cities.id')
            ->where('cities.url_index',$url_index)
            ->select('cities.id','cities_infos.city_name')
            ->get();

        //To get the property details
         $outlets = DB::table('cities')
            ->join('outlets','outlets.city_id','=','cities.id')
            ->leftjoin('cities_infos','cities_infos.id','=','cities.id')  
            ->join('outlet_price_category_mapping','outlet_price_category_mapping.outlet_id','=','outlets.id')
            ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')  
            ->select('outlet_infos.outlet_name','cities_infos.city_name','outlets.outlet_image','outlet_infos.contact_address')
            ->where('cities.url_index',$url_index)
            ->groupBy('cities_infos.city_name')
            ->groupBy('outlet_infos.outlet_name')
            ->groupBy('outlets.outlet_image')
            ->groupBy('outlet_infos.contact_address')
            ->paginate(2);
        
        //To get the accomodation details
        $accommodation_types = DB::table('accomodation_type')
            ->Join('accomodation_type_infos','accomodation_type_infos.id','=','accomodation_type.id')
            ->select('accomodation_type.id','accomodation_type_infos.accomodation_type_name')
            ->get();

        //To get the amenity details
        $amenities = DB::table('amenities')
            ->Join('amenities_infos','amenities_infos.id','=','amenities.id')
            ->select('amenities.id','amenities_infos.amenity_name')
            ->get();

        //To get the pricing categories
        $pricing_category = DB::table('categories')
            ->leftjoin('categories_infos','categories_infos.category_id','=','categories.id')
            ->where('categories.category_type',6)
            ->select('categories.id','categories_infos.category_name','categories.url_key')
            ->get();
        //To get the area details for the current city                            
        $areas = DB::table('cities')
            ->join('zones','zones.city_id','=','cities.id')
            ->leftJoin('zones_infos','zones_infos.zone_id','=','zones.id')
            ->where('cities.url_index',$url_index)
            ->select('zones.id','zones_infos.zone_name')
            ->get();
            
        $avg_outlet_reviews = DB::table('outlet_reviews')->avg('ratings');
                        // ->select(DB::RAW('avg(outlet_reviews.ratings) as avg_reviews_rating'))
                        // ->where('outlet_reviews.outlet_id','=',$outlet_id)
                        // ->get();
                    // return $reviews;

        if(count($city_name) > 0)  {
            $result = array("response" => array("httpCode" => 200, "status" => true, "properties" => $outlets,"pricing" => $pricing_category, "accomodation" => $accommodation_types, "amenities" => $amenities, "areas" => $areas,"city_name" => $city_name, "avg_outlet_reviews" => $avg_outlet_reviews, "Message" => "City Properties List."));
        } else {
            $result = array("response" => array("httpCode" => 400, "status" => false, "properties" => $outlets,"pricing" => $pricing_category, "accomodation" => $accommodation_types, "amenities" => $amenities, "areas" => $areas,"city_name" => $city_name, "avg_outlet_reviews" => $avg_outlet_reviews, "Message" => "No Properties Found."));
        }                   
       
       return json_encode($result); 
    }

    public function category_property_listing($url_index)
    {
                 /*    $outlets =  DB::table('categories')
                      ->leftjoin('outlet_place_category_mapping','outlet_place_category_mapping.outlet_place_category','=','categories.id::text')
                     // ->Join('outlets','oultets.id','=','outlet_place_category_mapping.outlet_id')
                     // ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')  
                      ->where('categories.url_key','=',$url_index)
                      ->where('categories.category_type',7)
                      ->select('*')->get();
                   */
            //To get the properties list
                   
            $condition = "c.url_key = '".$url_index."'";

            $query = "select min(rt.discount_price) as discount_price, min(rt.normal_price) as normal_price, 
                count(orr.id) as or_count, avg(orr.ratings) as or_rates, cit.city_name,zo.zone_name,
                oi.outlet_name,o.outlet_image,oi.contact_address,o.url_index
                from categories c 
                join outlet_place_category_mapping opcm  on opcm.outlet_place_category = c.id::text 
                join outlets o on o.id = opcm.outlet_id left join outlet_infos oi on oi.id = o.id 
                left join outlet_reviews orr on orr.outlet_id = o.id 
                join room_type rt on rt.property_id = o.id 
                left join cities_infos cit on cit.id = o.city_id 
                left join zones_infos zo on zo.zone_id = o.location_id 
                where ".$condition." and c.category_type = 7 
                group by oi.outlet_name,o.outlet_image,oi.contact_address,cit.city_name,zo.zone_name,o.url_index";

            $outlets = DB::select($query);
            
            //To get the pricing categories

              //    return $outlets;


            $pricing_category = DB::table('categories')
                            ->leftjoin('categories_infos','categories_infos.category_id','=','categories.id')
                            ->where('categories.category_type',6)
                            ->select('categories.id','categories_infos.category_name','categories.url_key')
                            ->get();
          
          //To get the city details
          $cities = DB::table('cities')->Join('cities_infos','cities_infos.id','=','cities.id')
                     ->select('cities.id','cities_infos.city_name')->get();
           //To get the accomodation details
        $accommodation_types = DB::table('accomodation_type')->Join('accomodation_type_infos','accomodation_type_infos.id','=','accomodation_type.id')->select('accomodation_type.id','accomodation_type_infos.accomodation_type_name')->get();

        //To get the amenity details
        $amenities = DB::table('amenities')->Join('amenities_infos','amenities_infos.id','=','amenities.id')->select('amenities.id','amenities_infos.amenity_name')->get();

        $avg_outlet_reviews = DB::table('outlet_reviews')->avg('ratings');

        if(count($outlets) > 0)  {
            $result = array("response" => array("httpCode" => 200, "status" => true, 
                            "properties" => $outlets,"pricing" => $pricing_category,
                            "accomodation" => $accommodation_types,
                            "amenities" => $amenities, "city_details" => $cities, "avg_outlet_reviews" => $avg_outlet_reviews,
                            "Message" => "City Properties List."));
        } else {
            $result = array("response" => array("httpCode" => 400, "status" => false, 
                            "properties" => $outlets,"pricing" => $pricing_category,
                            "accomodation" => $accommodation_types,"city_details" => $cities,
                            "amenities" => $amenities, "avg_outlet_reviews" => $avg_outlet_reviews,
                            "Message" => "No Properties Found."));
        }                   
       
       return json_encode($result); 
    }

     public function view_property($url_index ,Request $data){
        
        if($url_index != ""){

            $post_data = $data->all();

            //   return $post_data;

            $sd = $post_data['sd'];
            $ed = $post_data['ed'];
            $gc = $post_data['gc'];
            $rc = $post_data['rc'];

            //   return $gc;

            //  return array($url_index);

            $property_details = DB::table('outlets')
                                ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                ->leftJoin('cities','cities.id','=','outlets.city_id')
                                ->leftjoin('cities_infos','cities_infos.id','=','cities.id')
                                ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
                                ->where('outlets.url_index',$url_index)
                                ->select('outlets.id','outlets.url_index','outlet_infos.outlet_name',
                                    'outlet_infos.contact_address','outlets.highlights','outlets.facilities',
                                    'outlets.near_by','outlets.hotel_rules','outlets.vendor_id',
                                    'outlets.outlet_image','cities_infos.city_name',
                                    'zones_infos.zone_name','cities.url_index as city_url_index')
                                ->get()->toArray();

            //  return $property_details;

            // if(!empty(($property_details)))
            // {
            //     $oid = 1;
            //     //  return $oid;
            // }
            // else
            // {
            //     $oid = 2;
            //     return $oid;               
            // }                       
                       //   return $property_details;
            $outlet_id = (count($property_details) > 0)? $property_details[0]->id:'';
            $vendor_id = (count($property_details) > 0)? $property_details[0]->vendor_id:'';



            $property_amenities = DB::table('outlet_amenity_mapping')
                                ->leftjoin('amenities',DB::raw('CAST("amenities"."id" AS TEXT)'),'=','outlet_amenity_mapping.amenity_id')
                                ->leftjoin('amenities_infos','amenities_infos.id','=','amenities.id')
                                ->select('amenities.id','amenities_infos.amenity_name')->get();
            
            $p1 = DB::table('place_of_interest')
                                ->select('place_name','distance')
                                ->where('property_id',$outlet_id)
                                ->where('place',1)
                                ->get();

            $p2 = DB::table('place_of_interest')
                                ->select('place_name','distance')
                                ->where('property_id',$outlet_id)
                                ->where('place',2)
                                ->get();

            $p3 = DB::table('place_of_interest')
                                ->select('place_name','distance')
                                ->where('property_id',$outlet_id)
                                ->where('place',3)
                                ->get();                                                                

            //  return $p2;

            $recommended_properties = DB::table('outlets')
                                    ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                    ->Join('room_type','room_type.property_id','=','outlets.id')
                                    ->leftjoin('cities_infos','cities_infos.id','=','outlets.city_id')  
                                    ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
                                    //  ->where('outlets.id','!=',$outlet_id)  
                                    ->where('outlets.vendor_id','=',$vendor_id)
                                    ->select(DB::RAW('min(room_type.discount_price) as discount_price'),
                                        DB::RAW('max(room_type.normal_price) as normal_price'),
                                        'outlets.outlet_image','outlet_infos.outlet_name',
                                        'outlet_infos.contact_address','outlets.url_index',
                                        'cities_infos.city_name','zones_infos.zone_name')
                                    ->groupBy('outlets.outlet_image','outlet_infos.outlet_name','outlet_infos.contact_address','outlets.url_index',
                                        'cities_infos.city_name','zones_infos.zone_name')
                                    ->limit(3)
                                    ->get();
                //  return $recommended_properties;
                                    
            $query = DB::table('rooms')
                        ->leftjoin('room_type','room_type.id','=','rooms.room_type_id')
                        ->leftjoin('room_type_infos','room_type_infos.id','=','room_type.id')
                        //  ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->where('rooms.property_id',$outlet_id)
                        ->where('room_type.adult_count','>=',$gc)
                    //  ->whereBetween('booked_room.date',array($s,$e))
                        ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id','room_type_infos.room_type','room_type.normal_price','room_type.discount_price','room_type.room_image','room_type.adult_count',
                            'room_type.child_count')
                        ->groupBy('room_type.id')
                        ->groupBy('room_type_infos.room_type')
                        ->groupBy('room_type.normal_price')
                        ->groupBy('room_type.discount_price')
                        ->groupBy('room_type.room_image')
                        ->groupBy('room_type.adult_count')
                        ->groupBy('room_type.child_count');
                        //  ->get();
           
            $room_type = $query->get();
                     // return $room_type;

            $rt = $room_type->pluck('id');             

            //  return array($cout);

            $rtr = rooms::whereIN('room_type_id',$rt)
                        ->pluck('id')->toArray();

            $rtrs = rooms::whereIN('room_type_id',$rt)
                        ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->whereBetween('booked_room.date',array($sd,$ed))
                        ->where('booked_room.status','=',1)
                        ->pluck('rooms.id')->toArray();  



               //   return $rtrs;

/*
            $all = DB::table('rooms')
                        //  ->select('rooms.id','room_type.id')
                        ->pluck('rooms.id')
                        ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                        //  ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->whereIN('room_type.id',$rt)
                        //  ->whereNOTBetween('booked_room.date',array($cin,$cout))
                        //  ->groupBy('room_type.id')
                        ->toArray(); 


            $a = DB::table('rooms')
                        //  ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id')
                        ->pluck('rooms.id')
                        ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                        ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->whereIN('room_type.id',$rt)
                        ->whereNOTBetween('booked_room.date',array($cin,$cout))
                        ->groupBy('room_type.id')
                        ->toArray(); 

              return $a;
*/
            $result=array_diff($rtr,$rtrs);

            

            //  HERE WE ARE GETTING ONLY 2 ROOM TYPE 

            $users = DB::table('rooms')
                                ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id',
                                    'room_type_infos.room_type','room_type.normal_price',
                                    'room_type.discount_price','room_type.room_image',
                                    'room_type.adult_count','room_type.child_count','room_type.url_index')
                                ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                ->leftJoin('room_type_infos','room_type_infos.id','room_type.id')
                                ->whereIn('rooms.id', $result)
                                ->groupBy('room_type.id')
                                ->groupBy('room_type_infos.room_type')
                                ->groupBy('room_type.normal_price')
                                ->groupBy('room_type.discount_price')
                                ->groupBy('room_type.room_image')
                                ->groupBy('room_type.adult_count')
                                ->groupBy('room_type.child_count')
                                ->take(2)
                                ->get();

            $roomtype = DB::table('rooms')
                                ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id',
                                    'room_type_infos.room_type','room_type.normal_price',
                                    'room_type.discount_price','room_type.room_image',
                                    'room_type.adult_count','room_type.child_count','room_type.url_index')
                                ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                ->leftJoin('room_type_infos','room_type_infos.id','room_type.id')
                                ->whereIn('rooms.id', $result)
                                ->groupBy('room_type.id')
                                ->groupBy('room_type_infos.room_type')
                                ->groupBy('room_type.normal_price')
                                ->groupBy('room_type.discount_price')
                                ->groupBy('room_type.room_image')
                                ->groupBy('room_type.adult_count')
                                ->groupBy('room_type.child_count')
                                ->get();                                
            //  return $roomtype;

            $rooc = $users->pluck('rooms_count')->toArray();

                //    return $rooc;
            $price = DB::table('room_type')
                        ->select(DB::RAW('min(room_type.discount_price) as disc_price'))
                        ->where('room_type.property_id','=',$outlet_id)
                        ->get();
                        //    return $price;

            $avg_outlet_reviews = DB::table('outlet_reviews')
                        ->where('outlet_reviews.approval_status',1)
                        ->where('outlet_reviews.outlet_id','=',$outlet_id)
                        ->avg('ratings');
                        //   return $avg_outlet_reviews;

            $rcondtion = '(outlet_reviews.approval_status = 1 and outlet_reviews.outlet_id = '.$outlet_id.')';

            $reviews   = DB::table('outlet_reviews')
                        ->select('outlet_reviews.comments', 'outlet_reviews.ratings', 'outlet_reviews.title',
                             'outlet_reviews.created_date', 'admin_customers.id', 'admin_customers.firstname',
                             'admin_customers.lastname', 'outlet_reviews.location', 'outlet_reviews.comfort', 
                             'outlet_reviews.sleep_quality', 'outlet_reviews.rooms', 'cleanliness'
                            /*, 'admin_customers.image'*/)
                        ->leftJoin('admin_customers','admin_customers.id','=','outlet_reviews.customer_id')
                        ->whereRaw($rcondtion)
                        ->orderBy('outlet_reviews.id', 'desc')
                        ->get();

            //  return $reviews;

            $avg_outlet_reviews_location = outlet_reviews::whereRaw($rcondtion)->avg('location');
            $avg_outlet_reviews_comfort  = outlet_reviews::whereRaw($rcondtion)->avg('comfort');
            $avg_outlet_reviews_sleep_quality = outlet_reviews::whereRaw($rcondtion)->avg('sleep_quality');
            $avg_outlet_reviews_rooms =  outlet_reviews::whereRaw($rcondtion)->avg('rooms');
            $avg_outlet_reviews_cleanliness =  outlet_reviews::whereRaw($rcondtion)->avg('cleanliness');

                        // return $reviews;

            $total_rating = outlet_reviews::where('outlet_id','=',$outlet_id)->count();
            //  return $total_rating;

            $count_ratings5 = outlet_reviews::whereRaw($rcondtion)
                                    ->where('outlet_reviews.ratings','=',5)->count();

            $count_ratings4 = outlet_reviews::whereRaw($rcondtion)
                                    ->where('outlet_reviews.ratings','=',4)->count();

            $count_ratings3 = outlet_reviews::whereRaw($rcondtion)
                                    ->where('outlet_reviews.ratings','=',3)->count();

            $count_ratings2 = outlet_reviews::whereRaw($rcondtion)
                                    ->where('outlet_reviews.ratings','=',2)->count();

            $count_ratings1 = outlet_reviews::whereRaw($rcondtion)
                                    ->where('outlet_reviews.ratings','=',1)->count();

            //  return $count_ratings4;           
               
            $result = array("response" => array("httpCode" => 200,"status" => true,
                "outlets" => $property_details,"property_id" => $outlet_id,"vendor_id" => $vendor_id,
                "amenities" => $property_amenities,"recommended_properties" => $recommended_properties,
                "room_type" => $users , "price" => $price, "avg_outlet_reviews" => $avg_outlet_reviews,
                "reviews" => $reviews, "count_ratings5" => $count_ratings5,"count_ratings4" => $count_ratings4,
                "count_ratings3" => $count_ratings3, "count_ratings2" => $count_ratings2,
                "count_ratings1" => $count_ratings1, "total_rating" => $total_rating,
                "avg_outlet_reviews_location" => $avg_outlet_reviews_location,
                "avg_outlet_reviews_comfort" => $avg_outlet_reviews_comfort,
                "avg_outlet_reviews_sleep_quality" => $avg_outlet_reviews_sleep_quality,
                "avg_outlet_reviews_rooms" => $avg_outlet_reviews_rooms,
                "avg_outlet_reviews_cleanliness" => $avg_outlet_reviews_cleanliness,
                "p1" => $p1,"p2" => $p2,"p3" => $p3, /*"interest_places" => $place_of_interest, */
                "rooc" => $rooc, "roomtypecount" => $roomtype
                ));          

            return json_encode($result); 
        }
                             
    }

    public function roomshow(Request $data)
    {
        $post_data = $data->all();

        //  return 1;

        //   return $post_data;

            $guest_count = $post_data['adult_count'];
            $room_count = $post_data['room_count'];
            $startDate = $post_data['sd'];
            $endDate = $post_data['ed'];
            $oid = $post_data['outlet_id'];
            $btn_val = $post_data['btn_val'];

            $property_details = DB::table('outlets')
                                ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                ->leftjoin('cities_infos','cities_infos.id','=','outlets.city_id')  
                                ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
                                ->where('outlets.id',$oid)
                                ->select('outlets.id','outlets.url_index','outlet_infos.outlet_name','outlet_infos.contact_address','outlets.highlights','outlets.facilities','outlets.near_by','outlets.hotel_rules','outlets.vendor_id','outlets.outlet_image','cities_infos.city_name','zones_infos.zone_name')
                                ->get()->toArray();

            //  return $property_details;               

            $query = DB::table('rooms')
                        ->leftjoin('room_type','room_type.id','=','rooms.room_type_id')
                        ->where('rooms.property_id',$oid)
                        ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id')
                        ->groupBy('room_type.id');
                      
            $room_type = $query->get();
                     // return $room_type;

            $rt = $room_type->pluck('id');

            //  return array($rt);

            $rtr = rooms::whereIN('room_type_id',$rt)
                        ->pluck('id')->toArray();

            $rtrs = rooms::whereIN('room_type_id',$rt)
                        ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->whereBetween('booked_room.date',array($startDate,$endDate))
                        ->where('booked_room.status','=',1)
                        ->pluck('rooms.id')->toArray();

            // return $rtrs;
            $result=array_diff($rtr,$rtrs);
            
            //  return $result;

            $users = DB::table('rooms')
                                ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id as rt')
                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                //  ->leftJoin('outlets','outlets.id','rooms.property_id')
                                ->whereIn('rooms.id', $result)
                                ->groupBy('room_type.id')
                                ->get();

            //   return $users;

            $romco = $users->where('rooms_count','>=',$room_count)->pluck('rt')->toArray();

                     // return $romco;


            if($btn_val == 1)
            {
                $rooms_type = DB::table('room_type')
                                    ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id',
                                        'room_type_infos.room_type','room_type.normal_price',
                                        'room_type.discount_price','room_type.room_image',
                                        'room_type.adult_count','room_type.child_count','room_type.url_index')
                                    ->leftJoin('rooms','rooms.room_type_id','room_type.id')
                                    ->leftJoin('room_type_infos','room_type_infos.id','room_type.id')
                                    ->whereIn('room_type.id', $romco)
                                    ->where('room_type.adult_count','>=',$guest_count)
                                    ->groupBy('room_type.id')
                                    ->groupBy('room_type_infos.room_type')
                                    ->groupBy('room_type.normal_price')
                                    ->groupBy('room_type.discount_price')
                                    ->groupBy('room_type.room_image')
                                    ->groupBy('room_type.adult_count')
                                    ->groupBy('room_type.child_count')
                                    ->get();                
            }
            elseif($btn_val == 2)
            {
                $rooms_type = DB::table('room_type')
                                    ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id',
                                        'room_type_infos.room_type','room_type.normal_price',
                                        'room_type.discount_price','room_type.room_image',
                                        'room_type.adult_count','room_type.child_count','room_type.url_index')
                                    ->leftJoin('rooms','rooms.room_type_id','room_type.id')
                                    ->leftJoin('room_type_infos','room_type_infos.id','room_type.id')
                                    ->whereIn('room_type.id', $romco)
                                    ->where('room_type.adult_count','>=',$guest_count)
                                    ->groupBy('room_type.id')
                                    ->groupBy('room_type_infos.room_type')
                                    ->groupBy('room_type.normal_price')
                                    ->groupBy('room_type.discount_price')
                                    ->groupBy('room_type.room_image')
                                    ->groupBy('room_type.adult_count')
                                    ->groupBy('room_type.child_count')
                                    ->take(2)
                                    ->get();          
            }

            //  return $btn_val;

            //  return $users;             

            if(count($rooms_type) > 0)  {
                    $result = array("response" => array("httpCode" => 200, "status" => true,
                                                        "room_type" => $rooms_type,
                                                        "property_details" => $property_details,
                                                        "btn_val" => $btn_val,
                                                        "Message" => "Rooms Found"));
            }
            else{
                    $result = array("response" => array("httpCode" => 400, "status" => false,
                                                        "room_type" => $rooms_type,
                                                        "property_details" => $property_details,
                                                        "btn_val" => $btn_val,
                                                        "Message" => "No Rooms Found"));
            }
            return json_encode($result);
    }   

    public function roomsavailablitycheck(Request $data)
    {
        $post_data = $data->all();

        //  return 1;

        //  return $post_data;

            $guest_count = $post_data['adult_count'];
            $room_count = $post_data['room_count'];
            $startDate = $post_data['sd'];
            $endDate = $post_data['ed'];
            $oid = $post_data['outlet_id'];
            
            $property_details = DB::table('outlets')
                                ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                ->leftjoin('cities_infos','cities_infos.id','=','outlets.city_id')  
                                ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
                                ->where('outlets.id',$oid)
                                ->select('outlets.id','outlets.url_index','outlet_infos.outlet_name','outlet_infos.contact_address','outlets.highlights','outlets.facilities','outlets.near_by','outlets.hotel_rules','outlets.vendor_id','outlets.outlet_image','cities_infos.city_name','zones_infos.zone_name')
                                ->get()->toArray();

            //  return $property_details;               

            $query = DB::table('rooms')
                        ->leftjoin('room_type','room_type.id','=','rooms.room_type_id')
                        ->where('rooms.property_id',$oid)
                        ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id')
                        ->groupBy('room_type.id');
                      
            $room_type = $query->get();
                     // return $room_type;

            $rt = $room_type->pluck('id');

            //  return array($rt);

            $rtr = rooms::whereIN('room_type_id',$rt)
                        ->pluck('id')->toArray();

            $rtrs = rooms::whereIN('room_type_id',$rt)
                        ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->whereBetween('booked_room.date',array($startDate,$endDate))
                        ->where('booked_room.status','=',1)
                        ->pluck('rooms.id')->toArray();

            // return $rtrs;
            $result=array_diff($rtr,$rtrs);
            
            //  return $result;

            $users = DB::table('rooms')
                                ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id as rt')
                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                //  ->leftJoin('outlets','outlets.id','rooms.property_id')
                                ->whereIn('rooms.id', $result)
                                ->groupBy('room_type.id')
                                ->get();

            //   return $users;

            $romco = $users->where('rooms_count','>=',$room_count)->pluck('rt')->toArray();

                     // return $romco;


            $rooms_type = DB::table('room_type')
                                ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id',
                                    'room_type_infos.room_type','room_type.normal_price',
                                    'room_type.discount_price','room_type.room_image',
                                    'room_type.adult_count','room_type.child_count','room_type.url_index')
                                ->leftJoin('rooms','rooms.room_type_id','room_type.id')
                                ->leftJoin('room_type_infos','room_type_infos.id','room_type.id')
                                ->whereIn('room_type.id', $romco)
                                ->where('room_type.adult_count','>=',$guest_count)

            //  COMMENTED HERE, ADULT COUNT WILL MAKES SOME PROBLEM IN FUNCTIONALITY

                                ->groupBy('room_type.id')
                                ->groupBy('room_type_infos.room_type')
                                ->groupBy('room_type.normal_price')
                                ->groupBy('room_type.discount_price')
                                ->groupBy('room_type.room_image')
                                ->groupBy('room_type.adult_count')
                                ->groupBy('room_type.child_count')
                                ->take(2)
                                ->get();          
          

            //  return $rooms_type;

            //  return $users;             

            if(count($rooms_type) > 0)  {
                    $result = array("response" => array("httpCode" => 200, "status" => true,
                                                        "room_type" => $rooms_type,
                                                        "property_details" => $property_details,
                                                        "Message" => "Rooms Found"));
            }
            else{
                    $result = array("response" => array("httpCode" => 400, "status" => false,
                                                        "room_type" => $rooms_type,
                                                        "property_details" => $property_details,
                                                        "Message" => "No Rooms Found"));
            }
            return json_encode($result);
    }

    public function roomtypeavailablitycheck(Request $data)
    {
        $post_data = $data->all();

        //  return 1;

        //  return $post_data;

            $guest_count = $post_data['gc'];
            $room_count = $post_data['rc'];
            $startDate = $post_data['sd'];
            $endDate = $post_data['ed'];
            $oid = $post_data['outlet_id'];
            $rt = $post_data['room_type_id'];
            
            $property_details = DB::table('outlets')
                                ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                ->leftjoin('cities_infos','cities_infos.id','=','outlets.city_id')  
                                ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
                                ->where('outlets.id',$oid)
                                ->select('outlets.id','outlets.url_index','outlet_infos.outlet_name','outlet_infos.contact_address','outlets.highlights','outlets.facilities','outlets.near_by','outlets.hotel_rules','outlets.vendor_id','outlets.outlet_image','cities_infos.city_name','zones_infos.zone_name')
                                ->get()->toArray();

            //  return $property_details;               


            $rtr = rooms::where('room_type_id',$rt)
                        ->pluck('id')->toArray();

            //  return $rtr;

            $rtrs = rooms::where('room_type_id',$rt)
                        ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->whereBetween('booked_room.date',array($startDate,$endDate))
                        ->where('booked_room.status','=',1)
                        ->pluck('rooms.id')->toArray();

            // return $rtrs;
            $result=array_diff($rtr,$rtrs);
            
            //  return $result;

            $users = DB::table('rooms')
                                ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id as rt')
                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                //  ->leftJoin('outlets','outlets.id','rooms.property_id')
                                ->whereIn('rooms.id', $result)
                                ->groupBy('room_type.id')
                                ->get();

            //   return $users;

            $rooms_count = $users->where('rooms_count','>=',$room_count)->pluck('rooms_count')->toArray();

            //  return $rooms_count;

            $room_type = DB::table('room_type')
                         ->select('room_type.created_by','room_type.id','room_type_infos.room_type','room_type.discount_price')
                         ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                         ->where('room_type.id','=',$rt)
                         ->get();  

            //  return $room_type;  

            if(count($rooms_count) > 0)  {
                    $result = array("response" => array("httpCode" => 200, "status" => true,
                                                        "room_type" => $room_type,
                                                        "property_details" => $property_details,
                                                        "Message" => "Rooms Found"));
            }
            else{
                    $result = array("response" => array("httpCode" => 400, "status" => false,
                                                        "room_type" => $room_type,
                                                        "property_details" => $property_details,
                                                        "Message" => "No Rooms Found"));
            }
            return json_encode($result);
    }    

    public function hotel_list(Request $data)
    {
        $post_data = $data->all();

              //    return $post_data;

        $city = $post_data['place_city'];

        $guest_count = $post_data['guest_count'];
        $room_count = $post_data['room_count'];
        $startDate = $post_data['startDate'];
        $endDate = $post_data['endDate'];
        $latitude = $post_data['latitude'];
        $longitude = $post_data['longitude'];

/*
        $query = DB::table('outlets')
                    ->leftJoin('outlet_infos','outlet_infos.id','outlets.id')
                    ->leftJoin('cities','cities.id','outlets.city_id')
                    ->leftJoin('cities_infos','cities_infos.id','=','cities.id')
                    ->leftJoin('zones','zones.id','outlets.location_id')
                    ->leftJoin('zones_infos','zones_infos.zone_id','zones.id')
                    ->Join('room_type','room_type.property_id','outlets.id')
                    ->select(DB::Raw('min(room_type.discount_price) as discount_price'),DB::Raw('min(room_type.normal_price) as normal_price'),'outlets.url_index','outlets.id as oid','outlet_infos.outlet_name','outlets.outlet_image','outlet_infos.contact_address')
                    ->groupBy('outlets.id','outlet_infos.outlet_name','outlets.url_index','outlet_infos.contact_address')
                    ->distinct();
            
/*
            ->orWhere('outlet_infos.outlet_name','like', '%'. $city . '%')
            ->orWhere('cities_infos.city_name','like', '%'. $city . '%')
*/


          

            //  return $query;
/*
            if ($city) {
                $query->where(function ($q) use ($city) {
                    $q->where('outlet_infos.outlet_name', 'like',  '%'. $city . '%')
                        ->orWhere('cities_infos.city_name', 'like','%'. $city . '%')
                        ->orWhere('zones_infos.zone_name', 'like', '%'. $city . '%');
                });
            }
*/
/*            
            if ($guest_count){
                $query->where(function ($q) use ($guest_count) {
                    $q->where('room_type.adult_count','>=',$guest_count)
                        ->orWhere('room_type.adult_count','>=',$guest_count);
                });
            }
            
            
        $outlets = $query->get();
        
          return $outlets;
*/       
            $query_dist="";
            if($latitude !="" && $longitude !="")
            {
                 $query_dist = "earth_box(ll_to_earth(".$latitude.",".$longitude."), 50000) @> ll_to_earth(latitude, longitude)";
            }
                    
            $outlets = DB::select('SELECT Distinct on (outlets.id) outlets.id, 
                                  (outlets.id) as hotel_id, 
                                  avg(outlet_reviews.ratings) as avg_rating,
                                  min(room_type.discount_price) as discount_price,
                                  min(room_type.normal_price) as normal_price,
                                  outlets.url_index,outlet_infos.outlet_name,
                                  outlets.outlet_image,outlet_infos.contact_address,
                                  cities_infos.city_name,zones_infos.zone_name
                                  FROM outlets
                                  left join outlet_infos on outlet_infos.id = outlets.id
                                  left join outlet_reviews on outlet_reviews.outlet_id = outlets.id
                                  left join cities_infos on cities_infos.id = outlets.city_id
                                  left join zones_infos on zones_infos.zone_id = outlets.location_id
                                  Join room_type on room_type.property_id = outlets.id
                                  WHERE '.$query_dist.'
                                  GROUP BY outlets.id,outlet_infos.outlet_name,outlet_infos.contact_address,
                                  cities_infos.city_name,zones_infos.zone_name');

              //    return $outlets;
/*
        $o = $outlets->pluck('oid');

        $s = date_create($startDate);
        $e = date_create($endDate);

        $c = date_diff($e,$s);

        $days = $c->days;

        //  return $days;

        $roo=DB::table('rooms')
                    ->select('rooms.id as rid')
                    ->leftJoin('rooms_infos','rooms_infos.id','=','rooms.id')
                    ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                    //  ->whereBetween('booked_room.date',array($s,$e))
                    ->get();        

          return $roo;
        
        $rooms = DB::table('rooms')
                    ->select('rooms.id as rid','room_type.id')
                    ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                    ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                    ->leftJoin('room_type_infos','room_type_infos.id','room_type.id')
                    ->whereIN('rooms.property_id',$o)
                    ->groupBy('room_type.id','rid')
                    ->get();
          return $rooms;

        $rt = room_type::whereIN('property_id',$o)
                    ->where('room_type.adult_count','>=',$guest_count)
                    ->pluck('id')->toArray();

        $all = DB::table('rooms')->whereIN('room_type.id',$rt)
                                    ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id')
                                    ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                    ->groupBy('room_type.id')
                                    ->get();
                                    
          //return $all;     

        $rc = $all->where('rooms_count','>=',2)->pluck('id');

        //  $room_t = $rc['id'];


        return $rc;
   
      $s = getRoomFrontBookingList($startDate,$endDate,$room_count,$rc,$guest_count,$days);

        return $s;
*/       

        $cities = DB::table('cities')->Join('cities_infos','cities_infos.id','=','cities.id')
                     ->select('cities.id','cities_infos.city_name')->get();
        //  return $cities; 
        $pricing_category = DB::table('categories')
                    ->leftjoin('categories_infos','categories_infos.category_id','=','categories.id')
                    ->where('categories.category_type',6)
                    ->select('categories.id','categories_infos.category_name','categories.url_key')
                    ->get();      

        //    return $pricing_category;

        //To get the accomodation details
        $accommodation_types = DB::table('accomodation_type')
            ->Join('accomodation_type_infos','accomodation_type_infos.id','=','accomodation_type.id')
            ->select('accomodation_type.id','accomodation_type_infos.accomodation_type_name')
            ->get();

        //To get the amenity details
        $amenities = DB::table('amenities')
            ->Join('amenities_infos','amenities_infos.id','=','amenities.id')
            ->select('amenities.id','amenities_infos.amenity_name')
            ->get();                    

        if(count($outlets) > 0)  {
                $result = array("response" => array("httpCode" => 200, "status" => true,
                                                    /* "rooms" => $rooms,*/ "outlets" => $outlets,
                                                    "pricing" => $pricing_category, "amenities" => $amenities,
                                                    "accomodation" => $accommodation_types,"cities"  => $cities,
                                                    "Message" => "Hotel Found"));
        }
        else{
                $result = array("response" => array("httpCode" => 400, "status" => false,
                                                    /* "rooms" => $rooms, */ "outlets" => $outlets,
                                                    "pricing" => $pricing_category, "amenities" => $amenities,
                                                    "accomodation" => $accommodation_types,"cities"  => $cities,
                                                    "Message" => "No Hotels Found"));
        }
        return json_encode($result);
    }

    public function hotels_list(Request $data)
    {
        $post_data = $data->all();

                 // return $post_data;

        //  $city = $post_data['place_city'];

            $guest_count = $post_data['gc'];
            $room_count = $post_data['rc'];
            $startDate = $post_data['sd'];
            $endDate = $post_data['ed'];
            $latitude = $post_data['lat'];
            $longitude = $post_data['lng'];
     
            $query_dist="";
            if($latitude !="" && $longitude !="")
            {
                 $query_dist = "earth_box(ll_to_earth(".$latitude.",".$longitude."), 50000) @> ll_to_earth(latitude, longitude)";
            }

            if($guest_count !="")
            {
                 $gt = "room_type.adult_count >= $guest_count";
            }            
            
            $outlets = DB::select('SELECT Distinct on (outlets.id) outlets.id, 
                                  (outlets.id) as hotel_id, 
                                  avg(outlet_reviews.ratings) as avg_rating,
                                  min(room_type.discount_price) as discount_price,
                                  min(room_type.normal_price) as normal_price,
                                  outlets.url_index,outlet_infos.outlet_name,
                                  outlets.outlet_image,outlet_infos.contact_address,
                                  cities_infos.city_name,zones_infos.zone_name
                                  FROM outlets
                                  left join outlet_infos on outlet_infos.id = outlets.id
                                  left join outlet_reviews on outlet_reviews.outlet_id = outlets.id
                                  left join cities_infos on cities_infos.id = outlets.city_id
                                  left join zones_infos on zones_infos.zone_id = outlets.location_id
                                  Join room_type on room_type.property_id = outlets.id
                                  WHERE '.$query_dist.' and '.$gt.'
                                  GROUP BY outlets.id,outlet_infos.outlet_name,outlet_infos.contact_address,
                                  cities_infos.city_name,zones_infos.zone_name');



         //   return $outlets;
             
          $oid = array();
          foreach($outlets as $out)
          {
                $oid[] = $out->hotel_id;
          }

          //    return $oid;


            $query = DB::table('rooms')
                        ->leftjoin('room_type','room_type.id','=','rooms.room_type_id')
                        ->whereIN('rooms.property_id',$oid)
                        ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id')
                        ->groupBy('room_type.id');
                      
           
            $room_type = $query->get();
                     // return $room_type;

            $rt = $room_type->pluck('id');

            //  return array($cout);

            $rtr = rooms::whereIN('room_type_id',$rt)
                        ->pluck('id')->toArray();

            $rtrs = rooms::whereIN('room_type_id',$rt)
                        ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->whereBetween('booked_room.date',array($startDate,$endDate))
                        ->where('booked_room.status','=',1)
                        ->pluck('rooms.id')->toArray();

            //  return $rtrs;
            $result=array_diff($rtr,$rtrs);
            

            $users = DB::table('rooms')
                                ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id as rt')
                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                //  ->leftJoin('outlets','outlets.id','rooms.property_id')
                                ->whereIn('rooms.id', $result)
                                ->groupBy('room_type.id')
                                ->get();

            //   return $users;

            $romco = $users->where('rooms_count','>=',$room_count)->pluck('rt')->toArray();

                  //    return $romco;

            $out =  DB::table('outlets')
                     ->whereIN('room_type.id',$romco)
                     ->leftjoin('room_type','room_type.property_id','=','outlets.id')
                     ->distinct('oid')
                     ->pluck('outlets.id as oid')
                     ->toArray();

              //    return $out;
/*
            $property_details = DB::table('outlets')
                                ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                ->leftjoin('cities_infos','cities_infos.id','=','outlets.city_id')  
                                ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
                                ->whereIN('outlets.id',$out)
                                ->select('outlets.id','outlets.url_index','outlet_infos.outlet_name','outlet_infos.contact_address','outlets.outlet_image','cities_infos.city_name','zones_infos.zone_name')
                                ->get()->toArray();  
*/
            //   return $property_details;
      

        $cities = DB::table('cities')->Join('cities_infos','cities_infos.id','=','cities.id')
                     ->select('cities.id','cities_infos.city_name')->get();
        //  return $cities; 
        $pricing_category = DB::table('categories')
                    ->leftjoin('categories_infos','categories_infos.category_id','=','categories.id')
                    ->where('categories.category_type',6)
                    ->select('categories.id','categories_infos.category_name','categories.url_key')
                    ->get();      

        //    return $pricing_category;

        //To get the accomodation details
        $accommodation_types = DB::table('accomodation_type')
            ->Join('accomodation_type_infos','accomodation_type_infos.id','=','accomodation_type.id')
            ->select('accomodation_type.id','accomodation_type_infos.accomodation_type_name')
            ->get();

        //To get the amenity details
        $amenities = DB::table('amenities')
            ->Join('amenities_infos','amenities_infos.id','=','amenities.id')
            ->select('amenities.id','amenities_infos.amenity_name')
            ->get();                    

        if(count($outlets) > 0)  {
                $result = array("response" => array("httpCode" => 200, "status" => true,
                                                    /* "rooms" => $rooms,*/ "outlets" => $out,
                                                    "pricing" => $pricing_category, "amenities" => $amenities,
                                                    "accomodation" => $accommodation_types,"cities"  => $cities,
                                                    "Message" => "Hotel Found"));
        }
        else{
                $result = array("response" => array("httpCode" => 400, "status" => false,
                                                    /* "rooms" => $rooms, */ "outlets" => $out,
                                                    "pricing" => $pricing_category, "amenities" => $amenities,
                                                    "accomodation" => $accommodation_types,"cities"  => $cities,
                                                    "Message" => "No Hotels Found"));
        }
        return json_encode($result);
    }
    //Return URL response
    public function payment_response(Request $data)
    {
        $response_data = $data->all();
        $RefNo = $response_data['RefNo'];
        $subscription_payments = subscription_payment::where('reference_no','=',$RefNo)->first();
        if(count($subscription_payments)>0){
            //echo '<pre>return';print_r($subscription_payments);
            $payments_id = $subscription_payments->id;
            $vendor_id = $subscription_payments->vendor_id;
            $plan_id = $subscription_payments->plan_id;
            $payment_type = $subscription_payments->payment_type;
            $plan_duration = $subscription_payments->plan_duration;
            $plan_price = $subscription_payments->plan_price;
            $plan_discount_price = $subscription_payments->plan_discount_price;
            $auth_id = $subscription_payments->auth_id;
            
            
            
            $spayments = subscription_payment::find($payments_id);
            $spayments->payment_status = $response_data['Status'];
            $spayments->transaction_id = $response_data['TransId'];
            $spayments->auth_id = $response_data['AuthCode'];
            $spayments->payer_status = $response_data['Remark'];
            $spayments->payer_id = $response_data['Remark'];    // $response_data['PaymentId'];
            $spayments->transaction_secret_id = $response_data['BankMID'];
            $spayments->transaction_date = $response_data['TranDate'];
            $spayments->total_amount = $response_data['Amount'];
            $spayments->c_country = $response_data['S_country'];
            $spayments->c_bank = $response_data['S_bankname'];
            $spayments->c_no = $response_data['CCNo'];
            $spayments->c_name = $response_data['CCName'];
            $spayments->correlation_id = $response_data['Signature'];
            $spayments->pending_reason = serialize($response_data);
            $spayments->reason_code = $response_data['ErrDesc'];
/*            
            $spayments->user_email = Session::get('vendor_email');
            $spayments->vendor_id = Session::get('vendor_id');
*/
            $spayments->save();


            if($response_data['Status']==1){ //Success
                $subscription_trans = new Subscriptiontransaction();
                $subscription_trans->vendor_id = $vendor_id;
                $subscription_trans->transaction_id = $response_data['TransId'];
                $subscription_trans->payment_status = $response_data['Status'];
                $subscription_trans->country_code = '+60';
                $subscription_trans->currency_code = $response_data['Currency'];
                $subscription_trans->payment_type = $payment_type;
                $subscription_trans->plan_id = $plan_id;
                $subscription_trans->auth_id = $auth_id;
                $subscription_trans->reference_no = $RefNo;
                $subscription_trans->plan_price = $plan_price;
                $subscription_trans->plan_discount_price =  $plan_discount_price;
                $subscription_trans->plan_duration =  $plan_duration;
                $subscription_trans->save();
                return Redirect::to('vendors/subs_payments/1');
            } else {
                                
                return Redirect::to('vendors/subs_payments/0');
            }
        }
    }
    //Backend URL response
    public function paymentResponse(Request $data)
    {
        $response_data = $data->all();
        $RefNo = $response_data['RefNo'];
        $subscription_payments = subscription_payment::where('reference_no','=',$RefNo)->first();
        if(count($subscription_payments)>0){
            //echo '<pre>return';print_r($subscription_payments);
            $payments_id = $subscription_payments->id;
            $vendor_id = $subscription_payments->vendor_id;
            $plan_id = $subscription_payments->plan_id;
            $payment_type = $subscription_payments->payment_type;
            $plan_duration = $subscription_payments->plan_duration;
            $plan_price = $subscription_payments->plan_price;
            $plan_discount_price = $subscription_payments->plan_discount_price;
            $auth_id = $subscription_payments->auth_id;
            
            
            
            $spayments = subscription_payment::find($payments_id);
            $spayments->payment_status = $response_data['Status'];
            $spayments->transaction_id = $response_data['TransId'];
            $spayments->auth_id = $response_data['AuthCode'];
            $spayments->payer_status = $response_data['Remark'];
            $spayments->payer_id = $response_data['Remark'];    // $response_data['PaymentId'];
            $spayments->transaction_secret_id = $response_data['BankMID'];
            $spayments->transaction_date = $response_data['TranDate'];
            $spayments->total_amount = $response_data['Amount'];
            $spayments->c_country = $response_data['S_country'];
            $spayments->c_bank = $response_data['S_bankname'];
            $spayments->c_no = $response_data['CCNo'];
            $spayments->c_name = $response_data['CCName'];
            $spayments->correlation_id = $response_data['Signature'];
            $spayments->pending_reason = serialize($response_data);
            $spayments->reason_code = $response_data['ErrDesc'];
/*            
            $spayments->user_email = Session::get('vendor_email');
            $spayments->vendor_id = Session::get('vendor_id');
*/
            $spayments->save();



            if($response_data['Status']==1){ //Success
                
                $subscription_trans = new Subscriptiontransaction();
                $subscription_trans->vendor_id = $vendor_id;
                $subscription_trans->transaction_id = $response_data['TransId'];
                $subscription_trans->payment_status = $response_data['Status'];
                $subscription_trans->country_code = '+60';
                $subscription_trans->currency_code = $response_data['Currency'];
                $subscription_trans->payment_type = $payment_type;
                $subscription_trans->plan_id = $plan_id;
                $subscription_trans->auth_id = $auth_id;
                $subscription_trans->reference_no = $RefNo;
                $subscription_trans->plan_price = $plan_price;
                $subscription_trans->plan_discount_price =  $plan_discount_price;
                $subscription_trans->plan_duration =  $plan_duration;
                $subscription_trans->save();
                return Redirect::to('vendors/subs_payments/1');
            } else {    //  FAIL
                                
                return Redirect::to('vendors/subs_payments/0');
            }
            
        }       
    }

    //Return URL response
    public function wallet_payment_response(Request $data)
    {
        $response_data = $data->all();
        $RefNo = $response_data['RefNo'];
        $wallet_payment = wallet_payment::where('reference_no','=',$RefNo)->first();
        if(count($wallet_payment)>0){
            //echo '<pre>return';print_r($wallet_payment);
            $payments_id = $wallet_payment->id;
            $customer_id = $wallet_payment->customer_id;
            $payment_type = $wallet_payment->payment_type;
            $total_amount = $wallet_payment->total_amount;
            $auth_id = $wallet_payment->auth_id;
            
            
            $wpayments = wallet_payment::find($payments_id);
            $wpayments->payment_status = $response_data['Status'];
            $wpayments->transaction_id = $response_data['TransId'];
            $wpayments->auth_id = $response_data['AuthCode'];
            $wpayments->payer_status = $response_data['Remark'];
            $wpayments->payer_id = $response_data['Remark'];    // $response_data['PaymentId'];
            $wpayments->transaction_secret_id = $response_data['BankMID'];
            $wpayments->transaction_date = $response_data['TranDate'];
            $wpayments->total_amount = $response_data['Amount'];
            $wpayments->c_country = $response_data['S_country'];
            $wpayments->c_bank = $response_data['S_bankname'];
            $wpayments->c_no = $response_data['CCNo'];
            $wpayments->c_name = $response_data['CCName'];
            $wpayments->correlation_id = $response_data['Signature'];
            $wpayments->pending_reason = serialize($response_data);
            $wpayments->reason_code = $response_data['ErrDesc'];
/*            
            $wpayments->user_email = Session::get('vendor_email');
            $wpayments->vendor_id = Session::get('vendor_id');
*/
            $wpayments->save();


            if($response_data['Status']==1){ //Success
                $wallet_trans = new wallet_transaction();
                $wallet_trans->customer_id = $customer_id;
                $wallet_trans->transaction_id = $response_data['TransId'];
                $wallet_trans->payment_status = $response_data['Status'];
                $wallet_trans->country_code = '+60';
                $wallet_trans->currency_code = $response_data['Currency'];
                $wallet_trans->payment_type = $payment_type;
                //  $wallet_trans->plan_id = $plan_id;
                $wallet_trans->auth_id = $auth_id;
                $wallet_trans->reference_no = $RefNo;
                $wallet_trans->total_amount = $total_amount;
                $wallet_trans->save();

                $user_wallets_logs = new user_wallets;
                $user_wallets_logs->customer_id   = $customer_id;
                $user_wallets_logs->wallet_id     = $wallet_transv->id;
                $user_wallets_logs->amount        = $total_amount;
                $user_wallets_logs->type          = 1;
                $user_wallets_logs->created_date  = date("Y-m-d H:i:s");
                //  $user_wallets_logs->modified_date = date("Y-m-d H:i:s");
                $user_wallets_logs->save();            

                Session::flash('message-success',trans("messages.Amount Added Successfully"));
                return Redirect::to('/wallet');
            } else {

                Session::flash('message-failure',trans("messages.Amount Failed to Add"));
                return Redirect::to('/wallet');
            }
        }
    }
    //Backend URL response
    public function payment_response_wallet(Request $data)
    {
        $response_data = $data->all();
        $RefNo = $response_data['RefNo'];
        $wallet_payment = wallet_payment::where('reference_no','=',$RefNo)->first();
        if(count($wallet_payment)>0){
            //echo '<pre>return';print_r($wallet_payment);
            $payments_id = $wallet_payment->id;
            $customer_id = $wallet_payment->customer_id;
            $payment_type = $wallet_payment->payment_type;
            $total_amount = $wallet_payment->total_amount;
            $auth_id = $wallet_payment->auth_id;
            
            
            $wpayments = wallet_payment::find($payments_id);
            $wpayments->payment_status = $response_data['Status'];
            $wpayments->transaction_id = $response_data['TransId'];
            $wpayments->auth_id = $response_data['AuthCode'];
            $wpayments->payer_status = $response_data['Remark'];
            $wpayments->payer_id = $response_data['Remark'];    // $response_data['PaymentId'];
            $wpayments->transaction_secret_id = $response_data['BankMID'];
            $wpayments->transaction_date = $response_data['TranDate'];
            $wpayments->total_amount = $response_data['Amount'];
            $wpayments->c_country = $response_data['S_country'];
            $wpayments->c_bank = $response_data['S_bankname'];
            $wpayments->c_no = $response_data['CCNo'];
            $wpayments->c_name = $response_data['CCName'];
            $wpayments->correlation_id = $response_data['Signature'];
            $wpayments->pending_reason = serialize($response_data);
            $wpayments->reason_code = $response_data['ErrDesc'];
/*            
            $wpayments->user_email = Session::get('vendor_email');
            $wpayments->vendor_id = Session::get('vendor_id');
*/
            $wpayments->save();


            if($response_data['Status']==1){ //Success
                $wallet_trans = new wallet_transaction();
                $wallet_trans->customer_id = $customer_id;
                $wallet_trans->transaction_id = $response_data['TransId'];
                $wallet_trans->payment_status = $response_data['Status'];
                $wallet_trans->country_code = '+60';
                $wallet_trans->currency_code = $response_data['Currency'];
                $wallet_trans->payment_type = $payment_type;
                //  $wallet_trans->plan_id = $plan_id;
                $wallet_trans->auth_id = $auth_id;
                $wallet_trans->reference_no = $RefNo;
                $wallet_trans->total_amount = $total_amount;
                $wallet_trans->save();
                
                Session::flash('message-success',trans("messages.Amount Added Successfully"));
                return Redirect::to('/wallet');
            } else {

                Session::flash('message-failure',trans("messages.Amount Failed to Add"));
                return Redirect::to('/wallet');
            }
        }
   
    }

    public function booking_payment_response(Request $data)
    {

        $response_data = $data->all();
        $RefNo = $response_data['RefNo'];
        $user_payment = user_payment::where('reference_no','=',$RefNo)->first();
        if(count($user_payment)>0){
            //echo '<pre>return';print_r($user_payment);
            $payments_id = $user_payment->id;
            $customer_id = $user_payment->customer_id;
            $payment_type = $user_payment->payment_type;
            $total_amount = $user_payment->total_amount;
            $auth_id = $user_payment->auth_id;
            
            
            $wpayments = user_payment::find($payments_id);
            $wpayments->payment_status = $response_data['Status'];
            $wpayments->transaction_id = $response_data['TransId'];
            $wpayments->auth_id = $response_data['AuthCode'];
            $wpayments->payer_status = $response_data['Remark'];
            $wpayments->payer_id = $response_data['Remark'];    // $response_data['PaymentId'];
            $wpayments->transaction_secret_id = $response_data['BankMID'];
            $wpayments->transaction_date = $response_data['TranDate'];
            $wpayments->total_amount = $response_data['Amount'];
            $wpayments->c_country = $response_data['S_country'];
            $wpayments->c_bank = $response_data['S_bankname'];
            $wpayments->c_no = $response_data['CCNo'];
            $wpayments->c_name = $response_data['CCName'];
            $wpayments->correlation_id = $response_data['Signature'];
            $wpayments->pending_reason = serialize($response_data);
            $wpayments->reason_code = $response_data['ErrDesc'];
/*            
            $wpayments->user_email = Session::get('vendor_email');
            $wpayments->vendor_id = Session::get('vendor_id');
*/
            $wpayments->save();


            if($response_data['Status']==1){ //Success
                $user_trans = new user_transaction();
                $user_trans->customer_id = $customer_id;
                $user_trans->transaction_id = $response_data['TransId'];
                $user_trans->payment_status = $response_data['Status'];
                $user_trans->country_code = '+60';
                $user_trans->currency_code = $response_data['Currency'];
                $user_trans->payment_type = $payment_type;
                //  $user_trans->plan_id = $plan_id;
                $user_trans->auth_id = $auth_id;
                $user_trans->reference_no = $RefNo;
                $user_trans->total_amount = $total_amount;
                $user_trans->save();
                
                Session::flash('message-success',trans("messages.Booking Created Successfully"));
                return Redirect::to('/');
            } else {

                Session::flash('message-failure',trans("messages.Booking Failed to Create"));
                return Redirect::to('/');
            }
        }
    }

    public function response_booking_payment(Request $data)
    {
        $response_data = $data->all();
        $RefNo = $response_data['RefNo'];
        $user_payment = user_payment::where('reference_no','=',$RefNo)->first();
        if(count($user_payment)>0){
            //echo '<pre>return';print_r($user_payment);
            $payments_id = $user_payment->id;
            $customer_id = $user_payment->customer_id;
            $payment_type = $user_payment->payment_type;
            $total_amount = $user_payment->total_amount;
            $auth_id = $user_payment->auth_id;
            
            
            $wpayments = user_payment::find($payments_id);
            $wpayments->payment_status = $response_data['Status'];
            $wpayments->transaction_id = $response_data['TransId'];
            $wpayments->auth_id = $response_data['AuthCode'];
            $wpayments->payer_status = $response_data['Remark'];
            $wpayments->payer_id = $response_data['Remark'];    // $response_data['PaymentId'];
            $wpayments->transaction_secret_id = $response_data['BankMID'];
            $wpayments->transaction_date = $response_data['TranDate'];
            $wpayments->total_amount = $response_data['Amount'];
            $wpayments->c_country = $response_data['S_country'];
            $wpayments->c_bank = $response_data['S_bankname'];
            $wpayments->c_no = $response_data['CCNo'];
            $wpayments->c_name = $response_data['CCName'];
            $wpayments->correlation_id = $response_data['Signature'];
            $wpayments->pending_reason = serialize($response_data);
            $wpayments->reason_code = $response_data['ErrDesc'];
/*            
            $wpayments->user_email = Session::get('vendor_email');
            $wpayments->vendor_id = Session::get('vendor_id');
*/
            $wpayments->save();


            if($response_data['Status']==1){ //Success
                $user_trans = new user_transaction();
                $user_trans->customer_id = $customer_id;
                $user_trans->transaction_id = $response_data['TransId'];
                $user_trans->payment_status = $response_data['Status'];
                $user_trans->country_code = '+60';
                $user_trans->currency_code = $response_data['Currency'];
                $user_trans->payment_type = $payment_type;
                //  $user_trans->plan_id = $plan_id;
                $user_trans->auth_id = $auth_id;
                $user_trans->reference_no = $RefNo;
                $user_trans->total_amount = $total_amount;
                $user_trans->save();
                
                Session::flash('message-success',trans("messages.Booking Created Successfully"));
                return Redirect::to('/');
            } else {

                Session::flash('message-failure',trans("messages.Booking Failed to Create"));
                return Redirect::to('/');
            }
        }
    }
    
    public function review_comments(Request $data)
    {

        $post_data = $data->all();

        //  return $post_data;

        $page_number = $post_data['page_id'];

         // return $page_number;

        $page_number = $page_number - 1;

        $start = $page_number * 4;

        $perpage = 4;


        $outlet_id = $post_data['outlet_id'];
        //  return $post_data;
         $reviews   = DB::table('outlet_reviews')
                        ->select('outlet_reviews.comments', 'outlet_reviews.ratings', 'outlet_reviews.title', 'outlet_reviews.created_date', 'admin_customers.id', 'admin_customers.firstname', 'admin_customers.lastname'/*,'outlet_reviews.location', 'outlet_reviews.comfort', 'outlet_reviews.sleep_quality', 'outlet_reviews.rooms', 'cleanliness', 'users.image'*/)
                        ->leftJoin('admin_customers','admin_customers.id','=','outlet_reviews.customer_id')
                        ->where('outlet_reviews.approval_status','1')
                        ->where('outlet_reviews.outlet_id', '=', $outlet_id)
                        ->orderBy('outlet_reviews.id', 'desc')
                        ->skip($start)
                        ->take($perpage)
                        //  ->paginate(3);
                        ->get();
        //  return $reviews;
 
        $total_reviews_count = DB::table('outlet_reviews')->where('outlet_reviews.outlet_id', '=', $outlet_id)->where('outlet_reviews.approval_status',1)->count();
        // return $total_reviews_count;

        if(count($reviews) > 0)  {
                $result = array("response" => array("httpCode" => 200, "status" => true,
                                                    "reviews"  => $reviews, /*"count"  => $total_reviews_count,*/
                                                    "Message" => "Reviews Details Found"));
        }
        else{
                $result = array("response" => array("httpCode" => 400, "status" => false,
                                                    "reviews"  => $reviews,
                                                    "Message" => "No Reviews Found"));
        }
        return json_encode($result);        
    }
/*
    public function wallet(Request $data)
    {
        $post_data = $data->all();

        //  return $post_data;

        $id = $post_data['user_id'];

        $wallet_history = DB::select('SELECT wl.amount,wl.type,created_date
                            FROM wallet_logs wl
                            where wl.customer_id = ? ORDER BY wl.id',array($id));

        return $wallet_history;



    }
*/    
}
