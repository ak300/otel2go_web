<?php 
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Contracts\ArrayableInterface;
use Illuminate\Support\Facades\Text;
use Illuminate\Support\Facades\Redirect;
use JWTAuth;
use DB;
use App;
use Image;
use URL;
use Hash;
use Twilio;
use Services_Twilio;
use PDF;
use PushNotification;
use App\Model\cart_info;
use App\Model\users;
use App\Model\Users\cards;
use App\Model\api_Model;
use App\Model\vendors;
use App\Model\favorite_vendors;
use App\Model\cart_model;
use App\Model\return_orders;
use App\Model\orders;
use App\Model\order;
use App\Model\return_orders_log;
use App\Model\return_reasons;
use App\Model\transaction;
use App\Model\Users\addresstype;
use App\Model\admin_customers as Admincustomer;
use App\Model\booking_detail;
use App\Model\booked_room_details;
use App\Model\rooms;
use App\Model\rooms_infos;
use App\Model\booking_charge;
use App\Model\payment;
use App\Model\booking_infos;
use App\Model\room_type;
use App\Model\room_type_infos;
use App\Model\vendors_view;
use Paypal;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Details;
use PayPal\Api\Capture;
//  use PayPal\Api\Payment;
use PayPal\Api\Amount;
use PayPal\Api\Refund;
use PayPal\Api\Authorization;
use Session;

class Ordercheckout extends Controller
{
    const USER_SIGNUP_EMAIL_TEMPLATE = 1;
    const USERS_WELCOME_EMAIL_TEMPLATE = 3;
    const USERS_FORGOT_PASSWORD_EMAIL_TEMPLATE = 6;
    const USER_CHANGE_PASSWORD_EMAIL_TEMPLATE = 13;
    const OTP_EMAIL_TEMPLATE = 14;
    const ORDER_MAIL_TEMPLATE = 5;
    const ORDER_MAIL_VENDOR_TEMPLATE = 16;
    const RETURN_STATUS_CUSTOMER_EMAIL_TEMPLATE = 17;
    const ORDER_STATUS_UPDATE_USER = 18;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $_apiContext;
    public function __construct(Request $data)
    {   
		$post_data = $data->all();
		if(isset($post_data['language']) && $post_data['language']!='' && $post_data['language']==2)
			   {
				   App::setLocale('ar');
			   }
			   else {
				   App::setLocale('en');
			   }
/*               
        $this->_apiContext = PayPal::ApiContext(getAppPaymentConfig()->merchant_key,getAppPaymentConfig()->merchant_secret_key);
        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName'   => storage_path('logs/paypal.log'),
            'log.LogLevel'   => 'FINE'
        ));
*/        
    }
    
    /*
     * order detail
     */
    public function index(Request $data)
	{

		$post_data = $data->all();
		$cart_items = $this->calculate_cart($post_data['language'],$post_data['user_id']);
		$address_list = $this->get_address($post_data['language'],$post_data['user_id']);
		$gateway_list = $this->get_payment_gateways($post_data['language']);
		$delivery_slots = $this->get_delivery_slots();
		$delivery_settings = $this->get_delivery_settings();
		$time_interval = $this->get_delivery_time_interval();
		$avaliable_slot_mob = $this->get_avaliable_slot_mobl();
		$outlet_detail = $this->get_outlet_detail($post_data['language'],$post_data['user_id']);
		$address_type = $this->address_type();
		$date = date('Y-m-d'); //today date
		$weekOfdays = array();
		$weeks = array();
		$uweek = array();
		$deliver_slot_array = array();
		$datetime = new \DateTime();
		//$datetime->modify('+1 day');
		$listItem = array('<li class="active">', '</li>');
		$i = 0;
		$weekarray = array();
		while (true)
		{
			if ($i === 7) break;
			if ($datetime->format('N') === '7' && $i === 0)
			{
				$datetime->add(new \DateInterval('P1D'));
				continue;
			}
			$weekarray[] = $datetime->format('N');
			$j = $datetime->format('N');
			$jj = $datetime->format('N');
			$datetime->add(new \DateInterval('P1D'));
			$wk_day = date('N', strtotime($date));
			$weekOfdays[$j] = date('d M', strtotime($date));
			$weekOfdays_mob[] = date('d-m-Y', strtotime($date));
			$weekday = date('l', strtotime($date));
			foreach($time_interval as $time)
			{
				
				
				$slot_id = $this->check_value_exist($delivery_slots,$time->id,$j,'time_interval_id','day');
				$deliver_slot_array[$weekOfdays[$j]][] = array(
				"time"=>date('g:i a', strtotime($time->start_time)).' - '.date('g:i a', strtotime($time->end_time)),
				"slot"=>$slot_id,
				"time_interval_id"=>$time->id,
				"key"=>$weekOfdays[$j],
				"day"=>$j,
				"weekday" =>$weekday,
				"date" => $date
				);
			}
			$uj=$j+1;
			if($uj==8){
				$uweek[1] = date('l',strtotime($date));
			}else {
				$uweek[$uj] = date('l',strtotime($date));
			}

			$weeks[$j] = date('l',strtotime($date));
			$week_mob[] = date('l',strtotime($date));
			$date = date('Y-m-d', strtotime('+1 day', strtotime($date)));		
			$i++;
		}


		/** avilable slot  mobile **/
		$date1 = date('Y-m-d');
		$format_slot = array();
		$iii =0;
		foreach($uweek as $ukey=>$week)
		{
			foreach($avaliable_slot_mob as $key=>$mobile_slot)
			{
				if($mobile_slot->day == $ukey)
				{
					
					$mobile_slot->week_mob_time = date('g:i a', strtotime($mobile_slot->start_time)).' - '.date('g:i a', strtotime($mobile_slot->end_time));
					$format_slot[$iii]['week_date'] = date('d-m-Y', strtotime($date1));
					$format_slot[$iii]['day'] = $week;

					/** current day past time slot restriction **/
					$start_time = explode('-',$mobile_slot->week_mob_time);
					$stime=date('H:i:s',strtotime($start_time[0]));
					$etime=date('H:i:s',strtotime($start_time[1]));
					$td=date('d-m-Y', strtotime($date1));
					$today=date('d-m-Y');
					if ((time() >= strtotime($stime)) && (time() >= strtotime($etime)) && ($td==$today)) {
						//$format_slot[$iii]['time'][] = array();
					}
					else if((time() > strtotime($stime)) && (time() < strtotime($etime)) && ($td==$today)){
						$format_slot[$iii]['time'][] = $mobile_slot;
					}
					else {
						$format_slot[$iii]['time'][] = $mobile_slot;
					}
					/** current day past time slot restriction end **/

					/** current day past time without slot restriction we have to enable this and hide above **/
						//$format_slot[$iii]['time'][] = $mobile_slot;
					/** current day past time without slot restriction we have to enable this and hide above end **/

				}else {
					$format_slot[$iii]['day'] = $week;
					$format_slot[$iii]['week_date'] = date('d-m-Y', strtotime($date1));
				}
				
			}
			$date1 = date('d-m-Y', strtotime('+1 day', strtotime($date1)));
			$iii++;
		}

		/** avilable slot  mobile **/
		

		/** avilable slot updated mobile **/
		$avilable_slot_updated = array();
		if(count($format_slot) > 0 ){
			foreach($format_slot as $akey => $avilbale){
				
				$avilable_slot_updated[$akey]['day']=$avilbale['day'];
				$avilable_slot_updated[$akey]['week_date']=$avilbale['week_date'];

				if(isset($avilbale['time']) && $avilbale['time']!=''){
				foreach($avilbale['time'] as $tkey => $utime){
					$day_of_week=date('N', strtotime($avilbale['day']));
						$slot_class = 1;
						$avilable_slot_updated[$akey]['time'][$tkey]['status'] = 	$slot_class;
						$avilable_slot_updated[$akey]['time'][$tkey]['week_mob_time'] = 	$utime->week_mob_time;
						$avilable_slot_updated[$akey]['time'][$tkey]['slot_id'] = 	$utime->slot_id;
						$avilable_slot_updated[$akey]['time'][$tkey]['time_interval_id'] = 	$utime->time_interval_id;
				}
			 }else {
				 $avilable_slot_updated[$akey]['time']=array(); 
				 
			 }
			}
		}
		/** avilable slot updated mobile end **/
		$result = array("response" => array("httpCode" => 400 , "Message" => trans("messages.No cart items found"),"cart_items"=>array()));
		if(count($cart_items['cart_items'])>0)
		{
			$result = array("response" => array("httpCode" => 200 , "Message" => "Cart details", "cart_items"=>$cart_items['cart_items'],"total"=>$cart_items['total'],"sub_total"=>$cart_items['sub_total'],"tax"=>$cart_items['tax'],"tax_amount"=>$cart_items['tax_amount'],"delivery_cost"=>$cart_items['delivery_cost'],"address_list"=>$address_list,"gateway_list"=>$gateway_list,'delivery_slots'=>$delivery_slots,'time_interval'=>$time_interval,'delivery_slot_array'=>$deliver_slot_array,'weekOfdays'=>$weekOfdays,'weekOfdays_mob'=>$weekOfdays_mob,'week_mob'=>$week_mob,'week'=>$weeks,'outlet_detail'=>$outlet_detail,'address_type'=>$address_type,'delivery_settings'=>$delivery_settings,'avaliable_slot_mob'=>$avilable_slot_updated));
		}
		return json_encode($result);
	}
    
    public function get_avaliable_slot_mobl()
    {
        $available_slots = DB::select('SELECT dts.day,dti.start_time,dti.end_time,dts.id AS slot_id,dts.time_interval_id
        FROM delivery_time_slots dts
        LEFT JOIN delivery_time_interval dti ON dti.id = dts.time_interval_id');
        return $available_slots;
    }
    
    public function get_delivery_settings()
    {
        $delivery_settings = DB::table('delivery_settings')
        ->first();
        return $delivery_settings;
    }
    public function address_type($language_id='')
    {
	       if($language_id == '')
			   {
				   $language_id = getAdminCurrentLang();
			   }
			   
         $query = '"address_infos"."language_id" = (case when (select count(*) as totalcount from address_infos where address_infos.language_id = '.$language_id.' and address_type.id = address_infos.address_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $address_type=Addresstype::Leftjoin('address_infos','address_infos.address_id','=','address_type.id')
                         ->select('address_type.*','address_infos.*') 
                         ->whereRaw($query)
                          ->where('active_status', 1)
                         ->orderBy('id', 'desc')
                         ->get();
                                         
        return $address_type;
    }
    
    /** public function check_value_exist($delivery_slots,$interval_id,$day,$key1,$key2)
    {
        foreach ($delivery_slots as $slots)
        {
            if (is_array($slots) && check_value_exist($delivery_slots, $interval_id,$day,$key1,$key2)) return $slots->id;
            if (isset($slots->$key1) && $slots->$key1 == $interval_id && isset($slots->$key2) &&$slots->$key2 == $day) return $slots->id;
        }
        return 0;
    }**/

    public function check_value_exist($delivery_slots,$interval_id,$day,$key1,$key2)
    {
		$di=$day+1;
		if($di==8){
			$di=1;
		}
        foreach ($delivery_slots as $slots)
        {
            if (is_array($slots) && check_value_exist($delivery_slots, $interval_id,$di,$key1,$key2)) return $slots->id;
            if (isset($slots->$key1) && $slots->$key1 == $interval_id && isset($slots->$key2) &&$slots->$key2 == $di) return $slots->id;
        }
        return 0;
    }

    
    public function get_outlet_detail($language,$user_id)
    {
		 $query1 = 'outlet_infos.language_id = (case when (select count(outlet_infos.id) as totalcount from outlet_infos where outlet_infos.language_id = ' . $language . ' and outlets.id = outlet_infos.id) > 0 THEN ' . $language . ' ELSE 1 END)';
    
        $outlet_detail = DB::table('cart')
                ->select('cart_detail.outlet_id','vendors.id as vendor_id','outlet_infos.contact_address','outlets.latitude','outlets.longitude','outlets.delivery_time')
                ->join('cart_detail', 'cart_detail.cart_id', '=', 'cart.cart_id')
                ->join('outlets','cart_detail.outlet_id','=','outlets.id')
                ->join('outlet_infos', 'outlets.id', '=', 'outlet_infos.id')
                ->join('vendors','vendors.id','=','outlets.vendor_id')
                 ->where('cart.user_id',"=",$user_id)
                 ->whereRaw($query1)
                ->first();
        //echo $outlet_detail->outlet_id;
        /*$outlet_slot_detail = DB::table('delivery_timings')
                            ->select('vendor_id','day_week','day_week','start_time','end_time')
                             ->where('vendor_id',"=",$outlet_detail->outlet_id)
                            ->get(); */
        //$outlet_info["address"] = $outlet_detail;
        //$outlet_info["slots"] = $outlet_slot_detail;
        //print_r($outlet_slot_detail);exit;
        return $outlet_detail;
    }
    public function get_delivery_slots()
    {
        $delivery_slots = DB::table('delivery_time_slots')
                ->select('*')
                ->get();
        return $delivery_slots;
    }
    
    public function get_delivery_time_interval()
    {
        $time_interval = DB::table('delivery_time_interval')
                ->select('*')
                ->orderBy('start_time', 'asc')
                ->get();
        return $time_interval;
    }
    public function calculate_cart($language,$user_id)
    {
        $cart_data = cart_model::cart_items($language,$user_id);
        $delivery_settings = $this->get_delivery_settings();
        $sub_total = 0;
        $tax = 0;
		$delivery_cost =0;
		$tax_amount=0;

        $minimum_order_amount = getSettings('min_order_amount');
        $minimum_order_amount = !(empty($minimum_order_amount ))? $minimum_order_amount : 0;
        $is_service_tax =  getSettings('is_service_tax_applicable');
        if($is_service_tax)
        {
            $tax = getSettings('service_tax');
        }else
        {
             $tax = 0;
        }

		foreach($cart_data as $key=>$items)
		{ 
            $sub_total += $items->quantity * $items->discount_price;
            //$tax += $items->service_tax;
            $product_image = URL::asset('assets/front/'.Session::get('general')->theme.'/images/no_image.png');
            if(file_exists(base_path().'/public/assets/admin/base/images/products/list/'.$items->product_image) && $items->product_image != '')
            {
                $product_image = url('/assets/admin/base/images/products/list/'.$items->product_image);
            }
            $cart_data[$key]->product_image = $product_image;
        }

        if($tax > 0)
        $tax_amount = $sub_total * $tax /100;
        else
        $tax_amount = 0;

        $total = $sub_total+$tax_amount;


        
        // if($sub_total > $minimum_order_amount)
        // {
        //     $delivery_cost = 0;
        // }else
        // {
        //         if($delivery_settings->on_off_status == 1)
        //         {
        //             if($delivery_settings->delivery_type == 1)
        //             {
        //                 $total = $total+$delivery_settings->delivery_cost_fixed;
        //                 $delivery_cost = $delivery_settings->delivery_cost_fixed;
        //             }
        //             if($delivery_settings->delivery_type == 2)
        //             {
        //                 $total = $total+$delivery_settings->flat_delivery_cost;
        //                 $delivery_cost = $delivery_settings->flat_delivery_cost;
        //             }
                    
        //         }
        // }


        if($sub_total > $minimum_order_amount)
        {
            $delivery_cost = 0;
        }else
        {
            $delivery_cost = getSettings('delivery_cost');
        }


        return array("cart_items"=>$cart_data,"total"=>$total+$delivery_cost,"sub_total"=>$sub_total,"tax"=>$tax,"delivery_cost"=>$delivery_cost,"tax_amount"=>$tax_amount, "minimum_order_amount" => $minimum_order_amount);
    }
    
    public function get_address($language_id,$user_id)
    {   
		
		$query = '"address_infos"."language_id" = (case when (select count(*) as totalcount from address_infos where address_infos.language_id = '.$language_id.' and address_type.id = address_infos.address_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $address = DB::table('user_address')
                    ->select('*','user_address.id as address_id','address_infos.name as address_type')
                    ->where('user_id','=',$user_id)
                    ->whereRaw($query)
                    ->leftJoin('address_type','address_type.id','=','user_address.address_type')
                     ->leftJoin('address_infos','address_infos.address_id','=','address_type.id')
                    ->orderBy('user_address.id', 'desc')
                    ->get();


        if(count($address) > 0)
        {
            foreach($address as $key=>$val)
            {
                $address[$key]->city_id = ($val->city_id != '')?$val->city_id:'';
                $address[$key]->country_id = ($val->country_id != '')?$val->country_id:'';
                $address[$key]->postal_code = ($val->postal_code != '')?$val->postal_code:'';
            }
        }
        return $address;
    }
    
    public function get_payment_gateways($language_id)
    {
        //echo $language_id;
        $query = '"payment_gateways_info"."language_id" = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $gateways = DB::table('payment_gateways')
                ->select('*','payment_gateways.id as payment_gateway_id')
                ->leftJoin('payment_gateways_info','payment_gateways_info.payment_id','=','payment_gateways.id')
                ->orderBy('payment_gateways.id', 'desc')
                 ->where('active_status',"=",1)
                ->whereRaw($query)
                ->get();
        //print_r($gateways);exit;
        return $gateways;
    }
    
    public function get_payment_gateway($payment_gateway_id,$language_id)
    {
        $query = '"payment_gateways_info"."language_id" = (case when (select count(payment_gateways_info.language_id) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $gateways = DB::table('payment_gateways')
                ->select('payment_gateways.id','payment_gateways.payment_type','payment_gateways.merchant_key','payment_gateways.account_id','payment_gateways.payment_mode','payment_gateways.commision','payment_gateways_info.name','payment_gateways.id as payment_gateway_id','currencies.currency_code')
                ->leftJoin('payment_gateways_info','payment_gateways_info.payment_id','=','payment_gateways.id')
                ->leftJoin('currencies','currencies.id','=','payment_gateways.currency_id')
                ->orderBy('payment_gateways.id', 'desc')
                ->where('payment_gateways.active_status',"=",1)
                ->where('payment_gateways.id',"=",$payment_gateway_id)
                ->whereRaw($query)
                ->first();
        return $gateways;
    }
    
    public function get_payment_details(Request $data)
    {
        $post_data = $data->all();
        $cart_items = $this->calculate_cart($post_data['language'],$post_data['user_id']);
        $payment_gateway_detail = $this->get_payment_gateway($post_data['payment_gateway_id'],$post_data['language']);
        $result = array("response"=>array("httpCode" => 400,"Message" => trans("messages.No cart items found"),"cart_items"=>array()));
        if(count($cart_items)>0)
        {
            $result = array("response" => array("httpCode" => 200,"Message" => "Cart details", "cart_items"=>$cart_items['cart_items'],"total"=>$cart_items['total'],"sub_total"=>$cart_items['sub_total'],"tax"=>$cart_items['tax'],"tax_amount"=>$cart_items['tax_amount'],"delivery_cost"=>$cart_items['delivery_cost'],"payment_gateway_detail"=>$payment_gateway_detail));
        }
        return json_encode($result);

    }
    
    public function offline_payment(Request $data)
    { 
		
        $post_data = $data->all();
        $current_date = strtotime(date('Y-m-d'));
        $payment_array =  json_decode($post_data['payment_array']);
        $payment_arrays =  json_decode($post_data['payment_array'],true);
      //print_r( $payment_array);exit;
        $rules = array();
        if($payment_array->order_type == 1)
        {
			
            $rules['delivery_address'] = 'required';
        }
        $validation = app('validator')->make($payment_arrays,$rules);
        // process the validation
        if ($validation->fails()) 
        { 
            foreach( $validation->errors()->messages() as $key => $value) 
            {
                $errors[] = is_array($value)?implode( ',',$value ):$value;
            }
            $errors = implode( ", \n ", $errors );
            $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => $errors, "Error" => trans("messages.Error List")));
        }
        else
        { 
            $total_amt = $payment_array->total;
           
            if($payment_array->coupon_id != 0)
            {
                $coupon_details = DB::table('coupons')
                                ->select('coupons.id as coupon_id', 'coupon_type', 'offer_amount', 'coupon_code', 'start_date', 'end_date')
                                ->leftJoin('coupon_outlet','coupon_outlet.coupon_id','=','coupons.id')
                                ->where('coupons.id','=',$payment_array->coupon_id)
                                ->where('coupon_outlet.outlet_id','=',$payment_array->outlet_id)
                                ->first();
                if(count($coupon_details) == 0)
                {
                    $result = array("response" => array("httpCode" => 400,"Message" => "No coupons found"));
                    return json_encode($result);
                }
                else if((strtotime($coupon_details->start_date) <= $current_date) && (strtotime($coupon_details->end_date) >= $current_date))
                {
                    $coupon_user_limit_details = DB::table('user_cart_limit')
                                                    ->select('cus_order_count','user_limit','total_order_count','coupon_limit')
                                                    ->where('customer_id','=',$post_data['user_id'])
                                                    ->where('coupon_id','=',$payment_array->coupon_id)
                                                    ->first();
                    if(count($coupon_user_limit_details)>0)
                    {   
                        if($coupon_user_limit_details->cus_order_count >= $coupon_user_limit_details->user_limit)
                        {
                            $result = array("response" => array("httpCode" => 400,"Message" => "Max user limit has been crossed"));
                            return json_encode($result);
                        }
                        if($coupon_user_limit_details->total_order_count >= $coupon_user_limit_details->coupon_limit)
                        {
                            $result = array("response" => array("httpCode" => 400,"Message" => "Max coupon limit has been crossed"));
                            return json_encode($result);
                        }
                    }
                }
                else {
                    $result = array("response" => array("httpCode" => 400,"Message" => trans("messages.No coupons found")));
                    return json_encode($result);
                }
                //$total_amt = $payment_array->total - $payment_array->coupon_amount;
            }
            //echo 'update users set current_balance = '.$payment_array->admin_commission.' where id = 1';exit;
            $order_id = DB::table('orders')->insertGetId(
                [
                'order_key' => str_random(32), 
                'customer_id' => $payment_array->user_id, 
                //'vendor_id' => $payment_array->store_id, 
                //'vendor_name' => $payment_array->vendor_name,
                'total_amount' => $total_amt,
                'created_date' => date("Y-m-d H:i:s"),
                'order_status' => $payment_array->order_status,
                'coupon_id' => $payment_array->coupon_id,
                'coupon_amount' => $payment_array->coupon_amount,
                'coupon_type' => $payment_array->coupon_type,
                'service_tax' => $payment_array->tax_amount,
                'payment_status' => $payment_array->payment_status,
                //'invoice_id' => str_random(32),
                //'payment_gateway_commission' => $payment_array->payment_gateway_commission,
                //'outlet_id' => $payment_array->outlet_id,
                'delivery_instructions' => $payment_array->delivery_instructions,
                'delivery_address' => isset($payment_array->delivery_address)?$payment_array->delivery_address:'',
                'payment_gateway_id' => $payment_array->payment_gateway_id,
                'delivery_slot' => isset($payment_array->delivery_slot)?$payment_array->delivery_slot:'',
                'delivery_date' => $payment_array->delivery_date,
                'delivery_charge' => isset($payment_array->delivery_cost)?$payment_array->delivery_cost:'',
                // 'admin_commission' => $payment_array->admin_commission,
                // 'vendor_commission' => $payment_array->vendor_commission,
                'order_type' => $payment_array->order_type,
                //'vendor_key' => $payment_array->vendor_key
                'delivery_slot_type' => $payment_array->delivery_slot_type,
                ]
            );

            $update_orders = Orders::find($order_id);
            $update_orders->invoice_id = 'INV'.str_pad($order_id,8,"0",STR_PAD_LEFT).time();
            $update_orders->order_key_formated = '#ORD'.$order_id;
            $update_orders->save();


            $order_outlet_id = array();
            $vendor_names_list = '';

            foreach($payment_array->items as $item)
            {

                $var_id = isset($item->variant_id)? $item->variant_id: 0;
                $values = array('item_id' =>$item->product_id,'item_cost'=>$item->discount_price,'item_unit'=>$item->quantity//,'item_offer'=>$item->item_offer
                    ,'order_id'=>$order_id, 'vendor_id'=>$item->store_id,'outlet_id'=>$item->outlet_id,'product_variant_id'=> $var_id);
                DB::table('orders_info')->insert($values);
            }



                    $values = array(
                            'cart_id' => $payment_array->cart_id,
                            'order_id'    => $order_id,
                            'customer_id' => $payment_array->user_id,
                            //'vendor_id'   => $payment_array->store_id,
                            
                            //'outlet_id'   => $payment_array->outlet_id,
                            'payment_status' => "SUCCESS",
                            'payment_type' => 'COD',
                            'payer_id'=>!empty($payment_params->payer_id)?$payment_params->payer_id:'',
                            'transaction_id'=>!empty($payment_params->transaction_id)?$payment_params->transaction_id:'',
                            'country_code'=>!empty($payment_params->country_code)?$payment_params->country_code:'',
                            'created_date' => date("Y-m-d H:i:s"),
                            'currency_code'=>$payment_array->currency_code
                    );


                //print_r($values);exit;            
            DB::table('transaction')->insert($values);
            DB::update('delete from cart where user_id = ?', array($payment_array->user_id));
            $result = array("response"=>array("httpCode" => 400,"Message" => "Something went wrong"));


            //DB::update('update orders set order_key_formated = ? where id = ?', array($order_key_formatted,$order_id));
            // DB::update('update users set current_balance = current_balance+? where id = ?', array($payment_array->admin_commission,1));
            // DB::update('update vendors set current_balance = current_balance+? where id = ?', array($payment_array->vendor_commission,$payment_array->store_id));

            // $items = $payment_array->items;
            // foreach($items as $item)
            // {
            //     $values = array('item_id' =>$item->product_id,'item_cost'=>$item->discount_price,'item_unit'=>$item->quantity,'item_offer'=>$item->item_offer,'order_id'=>$order_id);
            //     DB::table('orders_info')->insert($values);
            // }

 
            if($values)
            {
                $result = array("response" => array("httpCode" => 200,"Message" => "Order initated success","order_id"=>$order_id));
                //Email notification to customer
                $this->send_order_email($order_id,$payment_array->user_id,$post_data['language']);
                //Email notification to admin & vendor
                $this->send_order_email_admin_vendors($order_id,$payment_array->user_id,$post_data['language']);
                $users = Users::find($payment_array->user_id); 

                $order_key_formatted = "#ORD".$order_id;

                $order_title = 'Your order '.$order_key_formatted.'  has been placed';
                $notification_message = PushNotification::Message($order_title,array(
                    'badge' => 1,
                    'sound' => 'example.aiff',
                    'actionLocKey' => $order_title,
                    //'launchImage' => base_path().'/assets/admin/base/images/offers/'.$offer_image,
                    'id' => $order_id,
                    'type' => 2,
                    'title' => $order_title,
                    'custom' => array('id' => $order_id,'type' => 2,'title' => $order_title)//If type 1 means offers and 2 means orders
                ));


                if($users->android_device_token != '')
                {
                    $android_device_arr[0] = PushNotification::Device($users->android_device_token);
                    $android_devices = PushNotification::DeviceCollection($android_device_arr);
                    $collection = PushNotification::app('TijikAndroid')->to($android_devices);
                    //it was need to set 'sslverifypeer' parameter to false
                    $collection->adapter->setAdapterParameters(['sslverifypeer' => false]);
                    $collection->send($notification_message);
                    // get response for each device push
                    foreach ($collection->pushManager as $push)
                    {
                        $response = $push->getAdapter()->getResponse();
                    }
                }

                /* //Mobile Android & iOS Push notifications
                $order_title = "Order initated success";
                $notification_message = PushNotification::Message('Your order has been placed',array(
                    'badge' => 1,
                    'sound' => 'example.aiff',
                    'actionLocKey' => 'New Order Placed!',
                    //'launchImage' => base_path().'/assets/admin/base/images/offers/'.$offer_image,
                    'id' => $order_id,
                    'title' => $order_title,
                    'custom' => array('id' => $order_id,'title' =>$order_title)
                ));
                //Get user details if user using mobile devices or not
                $user_data = Users::find($payment_array->user_id);
                //Send notifications to Android User
                if(!empty($user_data->login_type) && $user_data->login_type==2){
                    $android_device_arr[] = PushNotification::Device($user_data->android_device_token);
                    $android_devices = PushNotification::DeviceCollection($android_device_arr);
                    //echo '<pre>';print_r($android_devices);print_r($ios_devices);exit;
                    $collection = PushNotification::app('TijikAndroid')
                    ->to($android_devices);
                    //it was need to set 'sslverifypeer' parameter to false
                    $collection->adapter->setAdapterParameters(['sslverifypeer' => false]);            
                    $collection->send($notification_message);
                    // get response for each device push
                    /*foreach ($collection->pushManager as $push) {
                        $response = $push->getAdapter()->getResponse();
        
                       dd($response);
                    }/
                }
                //Send notifications to iOS User
                if(!empty($user_data->login_type) && $user_data->login_type==3){
                    $ios_device_arr[] = PushNotification::Device($user_data->ios_device_token);
                    $ios_devices = PushNotification::DeviceCollection($ios_device_arr);
            
                    $ios_collection = PushNotification::app('TijikIOS')
                        ->to($ios_devices)
                        ->send($notification_message);
                    // get response for each device push
                    /*foreach ($ios_collection->pushManager as $push) {
                        $response = $push->getAdapter()->getResponse();
        
                       dd($response);
                    }/
                }*/
                //Internal Admin Notifications Storing with notifications
                $mess = "New Order Was Placed at ".$payment_array->vendor_name;
                $values = array('order_id' => $order_id,
                            'customer_id'=>$payment_array->user_id,
                            'vendor_id'=>$payment_array->store_id,
                            'outlet_id'=>$payment_array->outlet_id,
                            'message'=> $mess,
                            'read_status'=> 0,
                            'created_date'=> date('Y-m-d H:i:s'));
                DB::table('notifications')->insert($values);
            }
        }
        return json_encode($result);
    }

    
    
    public function online_payment(Request $data)
    {
        $post_data = $data->all();
		$payment_array  = json_decode($post_data['payment_array']);
		$payment_params = json_decode($post_data['payment_params']);
		$total_amt = $payment_array->total;
		//print_r($payment_params);die;
	//

			$order_id = DB::table('orders')->insertGetId(
				[
				'order_key' => str_random(32), 
				'customer_id' => $payment_array->user_id, 
				//'vendor_id' => $payment_array->store_id,
				'total_amount' => $total_amt,
				'created_date' => date("Y-m-d H:i:s"),
				'order_status' => $payment_array->order_status,
				'coupon_id' => $payment_array->coupon_id,
				'coupon_amount' => $payment_array->coupon_amount,
				'coupon_type' => $payment_array->coupon_type,
				'service_tax' => $payment_array->tax_amount,
				'payment_status' => $payment_array->payment_status,
				//'invoice_id' => $payment_array->invoice_id,
				//'payment_gateway_commission' => $payment_array->payment_gateway_commission,
				//'outlet_id' => $payment_array->outlet_id,
				'delivery_instructions' => $payment_array->delivery_instructions,
				'delivery_address' => ($payment_array->delivery_address != '')?$payment_array->delivery_address:0,
				'payment_gateway_id' => $payment_array->payment_gateway_id,
                'delivery_slot' => isset($payment_array->delivery_slot)?$payment_array->delivery_slot:0,
                'delivery_date' =>isset($payment_array->delivery_date)?$payment_array->delivery_date:date("Y-m-d H:i:s"),
				'delivery_charge' => $payment_array->delivery_cost,
				// 'admin_commission' => $payment_array->admin_commission,
				// 'vendor_commission' => $payment_array->vendor_commission,
				'order_type' => $payment_array->order_type,
				'delivery_slot_type' => $payment_array->delivery_slot_type,
                //'delivery_slot_name' => "ASAP"
				]
			);



			$update_orders = Orders::find($order_id);
			$update_orders->invoice_id = 'INV'.str_pad($order_id,8,"0",STR_PAD_LEFT).time();
            $update_orders->order_key_formated = '#ORD'.$order_id;
			$update_orders->save();



            // DB::update('update users set current_balance = current_balance+? where id = ?', array($payment_array->admin_commission,1));

            $order_outlet_id = array();
            $vendor_names_list = '';

            foreach($payment_array->items as $item)
            {

                $var_id = isset($item->variant_id)? $item->variant_id: 0;
                $values = array('item_id' =>$item->product_id,'item_cost'=>$item->discount_price,'item_unit'=>$item->quantity//,'item_offer'=>$item->item_offer
                    ,'order_id'=>$order_id, 'vendor_id'=>$item->store_id,'outlet_id'=>$item->outlet_id,'product_variant_id'=> $var_id);
                DB::table('orders_info')->insert($values);
            }

// insert from anotgher table

			// $payment_array->invoice_id = $update_orders->invoice_id;
			// $order_key_formatted = "#OR".$payment_array->vendor_key.$order_id;
			// DB::update('update orders set order_key_formated = ? where id = ?', array($order_key_formatted,$order_id));
			// DB::update('update users set current_balance = current_balance+? where id = ?', array($payment_array->admin_commission,1));
			// DB::update('update vendors set current_balance = current_balance+? where id = ?', array($payment_array->vendor_commission,$payment_array->store_id));
			// $items = $payment_array->items;
			// foreach($items as $item)
			// {
			// 	$values = array('item_id' =>$item->product_id,'item_cost'=>$item->discount_price,'item_unit'=>$item->quantity,'item_offer'=>$item->item_offer,'order_id'=>$order_id);
			// 	DB::table('orders_info')->insert($values);
			// }

			$values = array(
                            'cart_id' => $payment_array->cart_id,
                            'order_id'    => $order_id,
							'customer_id' => $payment_array->user_id,
							//'vendor_id'   => $payment_array->store_id,
							
							//'outlet_id'   => $payment_array->outlet_id,
							'payment_status' => "SUCCESS",
							'payment_type' => !empty($payment_params->payment_method)?$payment_params->payment_method:'',
							'payer_id'=>!empty($payment_params->payer_id)?$payment_params->payer_id:'',
							'transaction_id'=>!empty($payment_params->transaction_id)?$payment_params->transaction_id:'',
							'country_code'=>!empty($payment_params->country_code)?$payment_params->country_code:'',
							'created_date' => date("Y-m-d H:i:s"),
							'currency_code'=>$payment_array->currency_code);


				//print_r($values);exit;			
			DB::table('transaction')->insert($values);
			DB::update('delete from cart where user_id = ?', array($payment_array->user_id));
			$result = array("response"=>array("httpCode" => 400,"Message" => "Something went wrong"));


			if($values)
			{
				$result = array("response" => array("httpCode" => 200,"Message" => "Order initated success","order_id"=>$order_id));
				//Email notification to customer
                // echo 'order id : '  . $order_id . '<br>';
                // echo '<h2>payment array</h2>';
                // echo '<pre>';
                // print_r($payment_array);
                // echo 'lang :' . $post_data['language'];

                // exit;
				$this->send_order_email($order_id,$payment_array->user_id,$post_data['language']);
				//Email notification to admin & vendor
				//$this->send_order_email_admin_vendors($order_id,$payment_array->user_id,$post_data['language']);
				$users = Users::find($payment_array->user_id); 

                $order_key_formatted = "#ORD".$order_id;
				$order_title = 'Your order '.$order_key_formatted.'  has been placed';
				$notification_message = PushNotification::Message($order_title,array(
					'badge' => 1,
					'sound' => 'example.aiff',
					'actionLocKey' => $order_title,
					//'launchImage' => base_path().'/assets/admin/base/images/offers/'.$offer_image,
					'id' => $order_id,
					'type' => 2,
					'title' => $order_title,
					'custom' => array('id' => $order_id,'type' => 2,'title' => $order_title)//If type 1 means offers and 2 means orders
				));
				if($users->android_device_token != '')
				{
					$android_device_arr[0] = PushNotification::Device($users->android_device_token);
					//print_r($users->android_device_token);exit;
					$android_devices = PushNotification::DeviceCollection($android_device_arr);
					$collection = PushNotification::app('TijikAndroid')->to($android_devices);
					//it was need to set 'sslverifypeer' parameter to false
					$collection->adapter->setAdapterParameters(['sslverifypeer' => false]);
					$collection->send($notification_message);
					// get response for each device push
					foreach ($collection->pushManager as $push)
					{
						$response = $push->getAdapter()->getResponse();
					}
				}

				/* //Mobile Android & iOS Push notifications
				$order_title = "Order initated success";
				$notification_message = PushNotification::Message('Your order has been placed',array(
					'badge' => 1,
					'sound' => 'example.aiff',
					'actionLocKey' => 'New Order Placed!',
					//'launchImage' => base_path().'/assets/admin/base/images/offers/'.$offer_image,
					'id' => $order_id,
					'title' => $order_title,
					'custom' => array('id' => $order_id,'title' =>$order_title)
				));
				//Get user details if user using mobile devices or not
				$user_data = Users::find($payment_array->user_id);
				//Send notifications to Android User
				if(!empty($user_data->login_type) && $user_data->login_type==2){
					$android_device_arr[] = PushNotification::Device($user_data->android_device_token);
					$android_devices = PushNotification::DeviceCollection($android_device_arr);
					//echo '<pre>';print_r($android_devices);print_r($ios_devices);exit;
					$collection = PushNotification::app('Yo9lekAndroid')
					->to($android_devices);
					//it was need to set 'sslverifypeer' parameter to false
					$collection->adapter->setAdapterParameters(['sslverifypeer' => false]);			
					$collection->send($notification_message);
					// get response for each device push
					/*foreach ($collection->pushManager as $push) {
						$response = $push->getAdapter()->getResponse();
		
					   dd($response);
					}/
				}
				//Send notifications to iOS User
				if(!empty($user_data->login_type) && $user_data->login_type==3){
					$ios_device_arr[] = PushNotification::Device($user_data->ios_device_token);
					$ios_devices = PushNotification::DeviceCollection($ios_device_arr);
			
					$ios_collection = PushNotification::app('Yo9lekIOS')
						->to($ios_devices)
						->send($notification_message);
					// get response for each device push
					/*foreach ($ios_collection->pushManager as $push) {
						$response = $push->getAdapter()->getResponse();
		
					   dd($response);
					}/
				}*/
				//Internal Admin Notifications Storing with notifications
				$mess = "New Order Was Placed ";
				$values = array('order_id' => $order_id,
							'customer_id'=>$payment_array->user_id,
							//'vendor_id'=>$payment_array->store_id,
							//'outlet_id'=>$payment_array->outlet_id,
							'message'=> $mess,
							'read_status'=> 0,
							'created_date'=> date('Y-m-d H:i:s'));
				DB::table('notifications')->insert($values);
			}
		
		return json_encode($result);
    }
    
    
    
    public function send_order_email($id='',$uid='',$language='')
    {
        $order_id   = isset($_POST['order_id'])? $_POST['order_id']: $id;
        $user_id    = isset($_POST['user_id'])? $_POST['user_id']: $uid;
        $language   = isset($_POST['language'])? $_POST['language']: $language;

        //  $user_array = array("user_id" => $user_id,"language"=>$language,"order_id" =>$order_id);


        //  $response   = $this->booking_detail($user_array);

        $booking_details = DB::table('booking_details')
                            ->select('booking_details.charges as bc_charges','booking_details.created_date as bc_date','booking_details.charges as bc_charges','admin_customers.id as cust_id','booking_details.id as bid','booking_details.*','admin_customers.*','booking_status.*','booking_info.*')
                            ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                            ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                            ->leftJoin('rooms','rooms.id','booking_info.room_id')
                            ->leftJoin('room_type','room_type.id','booking_details.room_type')
                            ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                            ->where('booking_details.id','=',$order_id)
                            ->get();

        $outlet_id = (count($booking_details) > 0)? $booking_details[0]->outlet_id:'';

        //  $vendor_id = (count($booking_details) > 0)? $booking_details[0]->vendor_id:'';

        $vendor_id = $booking_details[0]->vendor_id;

        $outlets = DB::table('outlets')
                    ->select('outlets.*','outlet_infos.*')
                    ->leftJoin('outlet_infos','outlet_infos.id','outlets.id')
                    ->where('outlets.id','=',$outlet_id)
                    ->get();

        $rooms_count = DB::table('booking_details')
                    ->select(DB::RAW('count(booking_info.id) as rooms_count'),'rooms_infos.room_name')
                    ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                    ->leftJoin('rooms','rooms.id','booking_info.room_id')
                    ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                    ->where('booking_details.id','=',$order_id)
                    ->groupby('rooms_infos.room_name')
                    ->get();

        $room_count = $rooms_count->pluck('rooms_count')->sum();

        $room_name = $booking_details[0]->room_name;


        $logo = url('/assets/front/'.Session::get("general")->theme.'/images/logo/'.Session::get("general")->theme.'.png');

        //$logo = 'freshnexg.png';
/*
        $delivery_date    = date("d F, l", strtotime($delivery_details->delivery_date)); 
        $delivery_time    = date('g:i a', strtotime($delivery_details->start_time)).'-'.date('g:i a', strtotime($delivery_details->end_time));
        $sub_total = 0;
        $item      = '';
        $site_name = Session::get("general")->site_name;
        $currency_side   = getCurrencyPosition()->currency_side;
        $currency_symbol = getCurrency($language); 
*/
        //  SEND TO USER

        $attachment ="";

        $users = Admincustomer::find($booking_details[0]->customer_id);
        $to=$users->email;
        $subject = 'Order Confirmation - Your Order with '.getAppConfig()->site_name.' ['.$booking_details[0]->booking_random_id .'] has been successfully placed!';
        $template = DB::table('email_templates')
                        ->select('*')
                        ->where('template_id','=',self::ORDER_MAIL_TEMPLATE)
                        ->get();
        if(count($template)){
                $from = $template[0]->from_email;
                $from_name=$template[0]->from;
                //$subject = $template[0]->subject;
                if(!$template[0]->template_id){
                $template = 'mail_template';
                $from=getAppConfigEmail()->contact_mail;
                $subject = "Welcome to ".getAppConfig()->site_name;
                $from_name="";
                $attachment ="";
        }
        $content =array("order" => array('name' =>$users->name /*, 'rooms_count'=>$room_count, */
            ,'room_name'=>$room_name ));
        $email=smtp($from,$from_name,$to,$subject,$content,$template,$attachment);


        // send to ADMIN

        $users = Users::find(1);
        //  $admin_mail = $users->email;
        $admin_mail = 'ashokkumar.v@nextbrainitech.com';
        $subject    = getAppConfig()->site_name.' New Order Placed & Confirmation - ['.$booking_details[0]->booking_random_id .']';

        //  SAME TEMPLATE FOR ADMIN & VENDOR

        $template   = DB::table('email_templates')
                        ->select('*')
                        ->where('template_id','=',self::ORDER_MAIL_VENDOR_TEMPLATE)
                        ->get();

        if(count($template))
        {
            $from = $template[0]->from_email;
            $from_name=$template[0]->from;
            if(!$template[0]->template_id)
            {
                $template = 'mail_template';
                $from     = getAppConfigEmail()->contact_mail;
                $subject  = getAppConfig()->site_name.' New Order Placed & Confirmation - ['.$booking_details[0]->booking_random_id .']';
                $from_name = "";
                $attachment ="";
            }
            $content = array("order" => array('name' =>$users->first_name  ,'room_name'=>$room_name /*,
            'room_name'=>$room_name */ ));
            $mail = smtp($from,$from_name,$admin_mail,$subject,$content,$template);
        }

        // send to VENDOR

        $vendors = vendors_view::find($vendor_id);
        $vendor_mail = $vendors->email;

        if(count($template))
        {
            $from = $template[0]->from_email;
            $from_name=$template[0]->from;
            if(!$template[0]->template_id)
            {
                $template = 'mail_template';
                $from     = getAppConfigEmail()->contact_mail;
                $subject  = getAppConfig()->site_name.' New Order Placed & Confirmation - ['.$booking_details[0]->booking_random_id .']';
                $from_name = "";
                $attachment ="";
            }
            $content = array("order" => array('name' =>$vendors->first_name ,'room_name'=>$room_name /*, 
            'room_name'=>$room_name */ ));
            $mail = smtp($from,$from_name,$vendor_mail,$subject,$content,$template);
        }



            /** SMS **/
            /** $number  = '+918867953038';
            $message = 'Order Confirmation - Your Order with '.getAppConfig()->site_name.' ['.$vendor_info[0]->order_key_formated .'] has been successfully placed!';
            $twilo_sid    = "ACa0946c498b75106632928c87c2a1e70f";
            $twilio_token = "4a43fcf31ea349c14ce6e6255a096ae4";
            $from_number  = "+19729927910";
            $client = new Services_Twilio($twilo_sid, $twilio_token);
            // Create an authenticated client for the Twilio API
            try {
                $m = $client->account->messages->sendMessage(
                       $from_number, // the text will be sent from your Twilio number
                       $number, // the phone number the text will be sent to
                       $message // the body of the text message
                    );
                //echo '<pre>';print_r($client);die;
                $users->otp = $otp;
                $users->updated_date = date("Y-m-d H:i:s");
                $users->save();
                $result = array("response" => array("httpCode" => 200,"Message" => "Otp sent"));
            }
            catch(Services_Twilio_RestException $e) {
               $result = array("response" => array("httpCode" => 400,"Message" => "Something went wrong"));
            };**/
            /** SMS **/
            return true;
        }
        
    }
    public function send_order_email_admin_vendors($id='',$uid='',$language='')
    {

        $order_id   = $id;
        $user_id    = $uid;
        $language   = $language;

        $order_id   = isset($_POST['order_id'])? $_POST['order_id']: $id;
        $user_id    = isset($_POST['user_id'])? $_POST['user_id']: $uid;
        $language   = isset($_POST['language'])? $_POST['language']: $language;

        $user_array = array("user_id" => $user_id,"language"=>$language,"order_id" =>$order_id);
        $response   = $this->get_order_detail($user_array);


        // echo '<pre>';
        // print_r( $response );
        // exit;

        // $loc = array();

        // foreach ($response['order_detail'] as $key => $value) {
        //     # code...
        //     $loc[$value->vendor_id]['vendor_id'] = $value->vendor_id;
        //     $loc[$value->vendor_id]['vendor_name'] = $value->vendor_name;
        //     $loc[$value->vendor_id]['logo_image'] = $value->logo_image;
        //     $loc[$value->vendor_id]['contact_address'] = $value->contact_address;
        //     $loc[$value->vendor_id]['contact_email'] = $value->contact_email;
        //     $loc[$value->vendor_id]['total_amount'] = $value->total_amount;
        //     $loc[$value->vendor_id]['outlets'][$value->outlet_id]['outlet_name'] = $value->outlet_name;
        //     $loc[$value->vendor_id]['outlets'][$value->outlet_id]['item'][] = $value->item;
        //     $loc[$value->vendor_id]['outlets'][$value->outlet_id]['outlet_id'] = $value->outlet_id;

        // }

        // echo '<pre>'; print_r( $loc ); exit;

        // $order_detail     = $response["order_detail"];
        // $delivery_details = $response["delivery_details"];
        // $order_info      = $response["order_info"];

        // $logo             = url('/assets/front/'.Session::get("general")->theme.'/images/'.Session::get("general")->theme.'.png');
        
        // $delivery_date    = date("d F, l", strtotime($delivery_details->delivery_date)); 
        // $delivery_time    = date('g:i a', strtotime($delivery_details->start_time)).'-'.date('g:i a', strtotime($delivery_details->end_time));
        // $sub_total = 0;$item      = '';
        // $currency_side   = getCurrencyPosition()->currency_side;
        // $currency_symbol = getCurrency($language);


        // foreach( $loc as $indiVendor)
        // {
        //     foreach( $indiVendor['outlets'] as $out)
        //     {
        //         $item .= '<tr style="background:#d1d5d4;padding:0 9px;"><td align="center" style="font-size:15px;padding:10px 0; font-family:arial; font-weight:normal; border-bottom:1px solid #ccc;">'.$out['outlet_name'].'</td><td></td><td></td><td></td>';

        //         foreach($out['item'] as $items)
        //         {

        //             if($currency_side == 1)
        //             {
        //                 $item_cost = $currency_symbol.$items->item_cost;
        //                 $unit_cost = $currency_symbol.($items->item_cost*$items->item_unit);
        //             }
        //             else {
        //                 $item_cost = $items->item_cost.$currency_symbol;
        //                 $unit_cost = ($items->item_cost*$items->item_unit).$currency_symbol;
        //             }
        //             $item .= '<tr><td align="center" style="font-size:15px;padding:10px 0; font-family:arial; font-weight:normal; border-bottom:1px solid #ccc;">'.wordwrap(ucfirst(strtolower($items->product_name)),40,"<br>\n").'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:arial; font-weight:normal;">'.wordwrap(ucfirst(strtolower($items->description)),40,"<br>\n").'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:arial; font-weight:normal;">'.$items->item_unit.'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:arial; font-weight:normal;">'.$item_cost.'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:arial; font-weight:normal;">'.$unit_cost.'</td></tr>';
        //             $sub_total += $items->item_cost*$items->item_unit;
        //         }
        //     }

        //     // gen tax 

        //     $delivery_charge = $currency_symbol.'0';
        //     $service_tax     = $currency_symbol.'0';

         //============================= from send_order_detail =======================


        $order_detail     = $response["order_detail"];
        $delivery_details = $response["delivery_details"];
        $order_info      = $response["order_info"];

        $logo             = url('/assets/front/'.Session::get("general")->theme.'/images/'.Session::get("general")->theme.'.png');

        //$logo = 'freshnexg.png';

        $delivery_date    = date("d F, l", strtotime($delivery_details->delivery_date)); 
        $delivery_time    = date('g:i a', strtotime($delivery_details->start_time)).'-'.date('g:i a', strtotime($delivery_details->end_time));
        $sub_total = 0;
        $item      = '';
        $site_name = Session::get("general")->site_name;
        $currency_side   = getCurrencyPosition()->currency_side;
        $currency_symbol = getCurrency($language); 

        if($delivery_details->order_type == 1)
        {
            $delivery_type   = 'DELIVERY ADDRESS :';
            $delivery_address = ($delivery_details->contact_address != '')?ucfirst($delivery_details->contact_address):'-';
        }
        else {
            $delivery_type   = 'PICKUP ADDRESS :';
            $delivery_address = ($delivery_details->contact_address != '')?ucfirst($delivery_details->contact_address):'-';
        }

        $overall_sub_total = 0;
        $overall_item      = '';
        $overall_bill_form = '';
        $item      = '';



        if($currency_side == 1)
        {
            $total_amount = $currency_symbol.$order_info->total_amount;
        }
        else {
            $total_amount = $order_info->total_amount.$currency_symbol;
        }
        

        $vendor_html = '';


        foreach($order_detail as $k=>$out)
        {

            $vendor_html = ' <!DOCTYPE html>
            <html lang="en">

            <head>
                <meta charset="UTF-8">
                
                <title>Editable Invoice</title>
                
             
            </head>

            <body style="font: 12px/1.4 Sans-serif;">

                <div id="page-wrap" style="width: 700px; margin: 0 auto;">

            <table  style="width: 700px;">
                <!--
                <tr> 
                     <td width="20%">a</td>  <td width="20%">b</td> <td width="20%">c</td> <td width="20%">d</td> <td width="20%">e</td>
                </tr>
                <tr> 
                      <td>a</td>  <td>b</td> <td>c</td> <td>d</td> <td>e</td>
                </tr>

                -->
                
                <tr>      
                    <td width="100%" colspan="5" style="height: 10px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px;">        
                    INVOICE 
                    </td>  

                    <!-- <td>b</td> <td>c</td> <td>d</td> <td>e</td> -->
                </tr>
                    
                <table cellpadding="20">
                
                    <tr>
                        <td width="50%"> ' . 
                        getSettings("contact_address") . 
                         getSettings('telephone') .
                        ' </td>
                        
                        <td> &nbsp; </td>
                        
                        <td width="50%" align="right"> <img src="' . $logo . '"></td>
                    
                    </tr>
                    

                </table>

            </table>


        


             <table  id="geta" width="700px" >
                <tr>
               
                    <td align="left">
                        <table width="400px">
                            <tr>  
                                <td> 
                                <div><b>DELIVERY ADDRESS:</b></div>
                                <div >
                                   ' . $delivery_address . '    </div>
                                    
                                </td>
                                <td> &nbsp;</td>
                            </tr>
                        </table>
                   </td>
         
            

 
                    <td align="right" border="0" width="300px">
                            
                        <table id="meta"  style="  margin-top: 1px;   border-collapse: collapse; margin:30px; width:300px">
                            <tr>
                                <td class="meta-head" style="  text-align: right; text-align: left; background: #eee;">Invoice #</td>
                                <td style=" text-align: right; background: #eee; ">' . $order_info->invoice_id . '</td>
                            </tr>
                            <tr>

                                <td class="meta-head" style=" text-align: right; text-align: left; background: #eee;">Date</td>
                                <td style=" text-align: right; background: #eee;">' . date('d F Y', strtotime($order_info->created_date))  . '</td>
                            </tr>
                            <tr>
                                <td class="meta-head" style=" text-align: right; text-align: left; background: #eee;">Amount</td>
                                <td style=" text-align: right; background: #eee;"><div class="due"> ' 
                                . $total_amount . '</div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>';


            $sub_total = 0;
                
            if(file_exists(base_path().'/public/assets/admin/base/images/vendors/logos/'.$out->logo_image)) 
                $img = url('/assets/admin/base/images/vendors/logos/'.$out->logo_image.''); 
            else
                $img = URL::asset('assets/front/'.Session::get("general")->theme.'/images/image_preview.jpg');


    //     $overall_item .= '<table width="700px"><tr><td><img width="50" height="50" src="'. $img . '"></td><td  align="left" style="text-align:left; 
    // vertical-align:middle;" valign="middle" width="100%">' .ucfirst($out->vendor_name).wordwrap(ucfirst($out->contact_address),70,"<br>\n") . '</td></tr><tr>';
        $vendor_html .='<!-- vendor 1 -->
            <table width="100%" style="margin-top: 20px" padding="10">
                <tr>
                <td>
                <table width="100%">
                    <tr>
                        <td>
                            <img src="' .   $img . '" width="50" height="50"> 
                        </td>
                        <td width="100%">
                            <h3>' .$out->outlet_name. '</h3> 
                            <!-- (' . $out->contact_email . ') -->
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <td colspan="2">
                            <span> <em style="font-size:8px">' . wordwrap(ucfirst($out->contact_address),70,"<br>\n") .'</em></span> 
                        </td>
                    </tr>
                    -->
                </table>
                </td>  
                </tr>
                
                <tr>
                    <td width="100%">
                        <table id="items" style="  width: 100%;  border: 1px solid black; border-collapse: collapse;" >
                        
                        <!-- th for vendor items -->
                          <tr>
                              <th style="border: 1px solid black; padding: 5px;background: #EEEEEE;">ID</th>
                              <th style="border: 1px solid black; padding: 5px;background: #EEEEEE;">Item</th>
                              <th style="border: 1px solid black; padding: 5px;background: #EEEEEE;">Unit Cost</th>
                              <th style="border: 1px solid black; padding: 5px;background: #EEEEEE;">Quantity</th>
                              <th style="border: 1px solid black; padding: 5px;background: #EEEEEE;">Price</th>
                          </tr>

                        ';

            foreach( $out->item as $items ){
                if($currency_side == 1)
                {
                    $item_cost = $currency_symbol.$items->item_cost;
                    $unit_cost = $currency_symbol.($items->item_cost*$items->item_unit);
                }
                else {
                    $item_cost = $items->item_cost.$currency_symbol;
                    $unit_cost = ($items->item_cost*$items->item_unit).$currency_symbol;
                }


                    // $item = '<tr><td>'.wordwrap(ucfirst(strtolower($items->product_name)),40,"<br>\n").
                    // '</td><td>'.$items->item_unit.'</td><td>'.$item_cost.'</td><td>'.$unit_cost.'</td></tr>';

                if($currency_side == 1)
                {
                    $item_amt = $currency_symbol. ( $items->item_cost*$items->item_unit );
                }
                else {
                    $item_amt = ( $items->item_cost*$items->item_unit ) . $currency_symbol;
                }

                $vendor_html .= '<tr class="item-row" style="border: 1px solid black; padding: 5px;">

                   <td class="description" style="border: 1px solid black; padding: 5px; vertical-align: top;">#' . $items->product_id . '</td>

                <td class="item-name" style="border: 1px solid black; padding: 5px;width: 175px;vertical-align: top; width: 300px; ">
                <div class="delete-wpr"> ' . wordwrap(ucfirst(strtolower($items->product_name)),40,"<br>\n") .' </div></td>
             
                <td style="border: 1px solid black; padding: 5px;vertical-align: top;">' . $items->item_cost . '</td>
                <td style="border: 1px solid black; padding: 5px;text-align: center;vertical-align: top;">' . $items->item_unit .'</td>
                <td style="border: 1px solid black; padding: 5px;vertical-align: top;"><span class="price">' . $item_amt . '</span></td>
                </tr>';

                //$overall_item .= $item;
                $sub_total += $items->item_cost*$items->item_unit;
                $overall_sub_total += $items->item_cost*$items->item_unit;
            }

            //$vendor_html .= '</td></tbody></table>';


        if($currency_side == 1)
        {
            $sub_total = $currency_symbol.$sub_total;
        }
        else {
            $sub_total = $sub_total.$currency_symbol;
        }

            $vendor_html .='<!-- subtotal: open -->
                    
                          <tr>
                              <td colspan="2" class="blank" style="border: 1px solid black; padding: 5px;border: 0; "> </td>
                              <td colspan="2" class="total-line" style="border: 1px solid black; padding: 5px; border-right: 0; text-align: right;">Subtotal</td>
                              <td class="total-value" style="border: 1px solid black; padding: 5px;border-left: 0; padding: 10px;"><div id="subtotal">'. $sub_total .'</div></td>
                          </tr>

             
                        
                        <!-- subtotal: close -->

                        </table>
                    </td>
                </tr>
                
            </table>';


        

        if($currency_side == 1)
        {
            $delivery_charge = $currency_symbol.'0';
        }
        else {
            $delivery_charge = '0'.$currency_symbol;
        }
        if($delivery_details->order_type == 1)
        {    
            
            if($currency_side == 1)
            {
                $delivery_charge = $currency_symbol.$order_info->delivery_charge;
            }
            else {
                $delivery_charge = $order_info->delivery_charge.$currency_symbol;
            }
        }

        if($currency_side == 1)
        {
            $grand_total_amount    = $currency_symbol.$order_info->total_amount;
            $grand_sub_total       = $currency_symbol.$overall_sub_total;
            $service_tax     = $currency_symbol.$delivery_details->service_tax;
        }
        else {
            $grand_total_amount    = $order_info->total_amount.$currency_symbol;
            $grand_sub_total       = $overall_sub_total.$currency_symbol;
            $service_tax     = $delivery_details->service_tax.$currency_symbol;
        }

        $delivery_email   = $delivery_details->email;
        $delivery_address = ($delivery_details->contact_address != '')?ucfirst($delivery_details->contact_address):'-';

        if($delivery_details->order_type == 1)
        {
            $delivery_type   = 'DELIVERY ADDRESS :';
            $delivery_address = ($delivery_details->contact_address != '')?ucfirst($delivery_details->contact_address):'-';
        }
        else {
            $delivery_type   = 'PICKUP ADDRESS :';
            $delivery_address = ($delivery_details->contact_address != '')?ucfirst($delivery_details->contact_address):'-';
        }

        //


        $coupon_offer = '';

        if($currency_side == 1)
        {
            $total_amount    = $currency_symbol.$order_info->total_amount;
            $sub_total       = $currency_symbol.$overall_sub_total;
            if($order_info->coupon_amount > 0){
                $coupon_offer    = '<tr>
                                    <td style="font-size:15px; font-weight:bold; font-family:dejavu sans,arial; color:#000; line-height:28px;">Coupon Offer</td>
                                    <td width="10"></td>
                                    <td style="font-size:16px; font-weight:500; font-family:dejavu sans,arial; color:#666; line-height:28px;" align="right">'.$currency_symbol.$order_info->coupon_amount.'</td>
                                    </tr>';
            }
        }
        else {
            $total_amount    = $order_info->total_amount.$currency_symbol;
            $sub_total       = $overall_sub_total.$currency_symbol;
            if($order_info->coupon_amount > 0){
                $coupon_offer    = '<tr>
                                    <td style="font-size:15px; font-weight:bold; font-family:dejavu sans,arial; color:#000; line-height:28px;">Coupon Offer</td>
                                    <td width="10"></td>
                                    <td style="font-size:16px; font-weight:500; font-family:dejavu sans,arial; color:#666; line-height:28px;" align="right">'.$order_info->coupon_amount.$currency_symbol.'</td>
                                    </tr>';
            }
        }


        $site_name = Session::get("general")->site_name;

        $vendor_html .= '<!-- fine grand total -->
            
            <table width="100%" style="width: 100%;  border: 1px solid black; border-collapse: collapse;">
            
                <tr>
                    <td width="60%">&nbsp;</td><td width="15%">&nbsp;</td><td width="15%">&nbsp;</td>
                </tr>
                
                <tr>
                    <td width="60%">&nbsp;</td>
         
                    <td   class="total-line" style="border: 1px solid black; padding: 5px; border-right: 0; text-align: right;">Grand Total</td>
                    <td class="total-value" style="border: 1px solid black; padding: 5px;border-left: 0; padding: 10px;"><div id="total">' . $grand_sub_total .'</div></td>
                </tr>
                
                <tr>
                    <td width="60%">&nbsp;</td>
             
                    <td  class="total-line" style="border: 1px solid black; padding: 5px; border-right: 0; text-align: right;"> Delivery Charges </td>
                    <td class="total-value" style="border: 1px solid black; padding: 5px;border-left: 0; padding: 10px;">' . $delivery_charge. '</td>
                </tr>
                
                <tr>
                    <td width="60%">&nbsp;</td>
             
                    <td  class="total-line" style="border: 1px solid black; padding: 5px; border-right: 0; text-align: right;">Amount Paid</td>
                    <td class="total-value" style="border: 1px solid black; padding: 5px;border-left: 0; padding: 10px;">' . $grand_total_amount .'</td>
                </tr>
                          
            
            </table>';

//echo '<pre>'; print_r( $vendor_html);  ob_flush(); flush();exit;

            $pdf = App::make('dompdf.wrapper');

            $pdf->loadHTML($vendor_html)->save(base_path().'/public/assets/front/'.Session::get("general")->theme.'/images/invoice/'.$out->invoice_id. '_' . $out->outlet_id . '.pdf');

            $attachment[] = base_path().'/public/assets/front/'.Session::get("general")->theme.'/images/invoice/'.$out->invoice_id. '_' . $out->outlet_id . '.pdf';

            $vendor_mail = $out->contact_email;

            $vendor_mail = 'vinay.s@nextbrainitech.com';

            $subject     = getAppConfig()->site_name.' New Order Placed & Confirmation - ['.$order_info->invoice_id .']';
            $template    = DB::table('email_templates')
                            ->select('*')
                            ->where('template_id','=',self::ORDER_MAIL_VENDOR_TEMPLATE)
                            ->get();


            if(count($template))
            {
                $from = $template[0]->from_email;
                $from_name=$template[0]->from;
                //$subject = $template[0]->subject;
                if(!$template[0]->template_id)
                {
                    $template = 'mail_template';
                    $from     = getAppConfigEmail()->contact_mail;
                    $subject  = getAppConfig()->site_name.' New Order Placed & Confirmation - ['.$vendor_info[0]->invoice_id .']';
                    $from_name = "";
                }
                $content = array("order" => array('name' =>$out->vendor_name,'data'=>$vendor_html));
 
                $email=smtp($from,$from_name,$vendor_mail,$subject,$content,$template,$attachment);
                $attachment = array();
                // $users = Users::find(1);
                // $admin_mail = $users->email;
                // $content = array("order" => array('name' =>$users->name,'data'=>$html));
                // $mail = smtp($from,$from_name,$admin_mail,$subject,$content,$template);

            }










        }

            // EOF 

            // ============================================




            // $pdf = App::make('dompdf.wrapper');
            // $pdf->loadHTML($html)->save(base_path().'/public/assets/front/'.Session::get("general")->theme.'/images/invoice/vendors/'.$indiVendor['vendor_id'] . '_' . $out['outlet_id'] . '_' . $order_info->invoice_id . 'pdf');

            // $vendor_mail = $indiVendor['contact_email'];

            // $subject     = getAppConfig()->site_name.' New Order Placed & Confirmation - ['.$order_info->invoice_id .']';
            // $template    = DB::table('email_templates')
            //                 ->select('*')
            //                 ->where('template_id','=',self::ORDER_MAIL_VENDOR_TEMPLATE)
            //                 ->get();

            // if(count($template))
            // {
            //     $from = $template[0]->from_email;
            //     $from_name=$template[0]->from;
            //     //$subject = $template[0]->subject;
            //     if(!$template[0]->template_id)
            //     {
            //         $template = 'mail_template';
            //         $from     = getAppConfigEmail()->contact_mail;
            //         $subject  = getAppConfig()->site_name.' New Order Placed & Confirmation - ['.$vendor_info[0]->invoice_id .']';
            //         $from_name = "";
            //     }
            //     $content = array("order" => array('name' =>$indiVendor['vendor_name'],'data'=>$html));
            //     $email = smtp($from,$from_name,$vendor_mail,$subject,$content,$template);
            //     // $users = Users::find(1);
            //     // $admin_mail = $users->email;
            //     // $content = array("order" => array('name' =>$users->name,'data'=>$html));
            //     // $mail = smtp($from,$from_name,$admin_mail,$subject,$content,$template);
            //     return true;
            // }


            // if(count($template))
            // {
            //     $from = $template[0]->from_email;
            //     $from_name=$template[0]->from;
            //     //$subject = $template[0]->subject;
            //     if(!$template[0]->template_id)
            //     {
            //     $template = 'mail_template';
            //     $from     = getAppConfigEmail()->contact_mail;
            //     $subject  = getAppConfig()->site_name.' New Order Placed & Confirmation - ['.$vendor_info[0]->invoice_id .']';
            //     $from_name = "";
            //     }
            //     $content = array("order" => array('name' =>$vendor_info[0]->vendor_name,'data'=>$html));
            //     $email = smtp($from,$from_name,$vendor_mail,$subject,$content,$template);
            //     $users = Users::find(1);
            //     $admin_mail = $users->email;
            //     $content = array("order" => array('name' =>$users->name,'data'=>$html));
            //     $mail = smtp($from,$from_name,$admin_mail,$subject,$content,$template);
            //     return true;
            // }


       
        



        // ---- gen vendor invoice --

    /*
        $order_detail     = $response["order_detail"];
        $delivery_details = $response["delivery_details"];
        $order_info      = $response["order_info"];
        $logo             = url('/assets/front/'.Session::get("general")->theme.'/images/'.Session::get("general")->theme.'.png');

        $delivery_date    = date("d F, l", strtotime($delivery_details->delivery_date)); 
        $delivery_time    = date('g:i a', strtotime($delivery_details->start_time)).'-'.date('g:i a', strtotime($delivery_details->end_time));
        $sub_total = 0;$item      = '';
        $currency_side   = getCurrencyPosition()->currency_side;
        $currency_symbol = getCurrency($language);
        foreach($order_detail as $items)
        {
            if($currency_side == 1)
            {
                $item_cost = $currency_symbol.$items->item_cost;
                $unit_cost = $currency_symbol.($items->item_cost*$items->item_unit);
            }
            else {
                $item_cost = $items->item_cost.$currency_symbol;
                $unit_cost = ($items->item_cost*$items->item_unit).$currency_symbol;
            }
            $item .= '<tr><td align="center" style="font-size:15px;padding:10px 0; font-family:arial; font-weight:normal; border-bottom:1px solid #ccc;">'.wordwrap(ucfirst(strtolower($items->product_name)),40,"<br>\n").'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:arial; font-weight:normal;">'.wordwrap(ucfirst(strtolower($items->description)),40,"<br>\n").'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:arial; font-weight:normal;">'.$items->item_unit.'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:arial; font-weight:normal;">'.$item_cost.'</td><td align="center" style="font-size:15px;padding:10px 0;border-bottom:1px solid #ccc; font-family:arial; font-weight:normal;">'.$unit_cost.'</td></tr>';
            $sub_total += $items->item_cost*$items->item_unit;
        }
        if($currency_side == 1)
        {
            $delivery_charge = $currency_symbol.'0';
            $total_amount    = $currency_symbol.$delivery_details->total_amount;
            $sub_total       = $currency_symbol.$sub_total;
            $service_tax     = $currency_symbol.$delivery_details->service_tax;
        }
        else {
            $delivery_charge = '0'.$currency_symbol;
            $total_amount    = $delivery_details->total_amount.$currency_symbol;
            $sub_total       = $sub_total.$currency_symbol;
            $service_tax     = $delivery_details->service_tax.$currency_symbol;
        }
        if($delivery_details->order_type == 1)
        {
            if($currency_side == 1)
            {
                $delivery_charge = $currency_symbol.$delivery_details->delivery_charge;
            }
            else {
                $delivery_charge = $delivery_details->delivery_charge.$currency_symbol;
            }
        }
        $delivery_email   = $delivery_details->email;
        $delivery_address = ($delivery_details->contact_address != '')?ucfirst($delivery_details->contact_address):'-';
        if($delivery_details->order_type == 1)
        {
            $delivery_type   = 'DELIVERY ADDRESS :';
            $delivery_address = ($delivery_details->contact_address != '')?ucfirst($delivery_details->contact_address):'-';
        }
        else {
            $delivery_type   = 'PICKUP ADDRESS :';
            $delivery_address = ($delivery_details->contact_address != '')?ucfirst($delivery_details->contact_address):'-';
        }
        $site_name = Session::get("general")->site_name;
        $html = '<table width="700px" cellspacing="0" cellpadding="0" bgcolor="#fff" style="border:1px solid #ccc;">
        <tbody>
        <tr>
        <td style="border-bottom:1px solid #ccc;">
        <table style="padding-top: 25px; padding-bottom: 25px;" width="700px" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td width="20">&nbsp;</td>
        <td>
        <table>
        <tr>
        <td style="font-size:16px; font-weight:bold; font-family:Verdana; color:#000; padding-bottom:10px;">BILL FORM :</td>
        </tr>
        <tr>
        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.ucfirst($vendor_info[0]->vendor_name).','.wordwrap(ucfirst($vendor_info[0]->contact_address),70,"<br>\n").'<br/>'.ucfirst($vendor_info[0]->contact_email).'</td>
        </tr>
        </table>
        </td>
        <td align="right"><a title="'.$site_name.'" href="'.url('/').'"><img src="'.$logo.'" alt="'.$site_name.'" /></a></td>
        <td width="20">&nbsp;</td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <!-- end 1 tr -->
        <tr>
        <td>
        <table style="padding-top: 25px; padding-bottom: 25px;" width="700px" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td width="20">&nbsp;</td>
        <td colspan="4">
        <table>
        <tr>
        <td style="font-size:16px; font-weight:bold; font-family:Verdana; color:#000; padding-bottom:10px;">'.$delivery_type.'</td>
        </tr>
        <tr>
        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.wordwrap($delivery_address,70,"<br>\n").'
        <br/>'.$delivery_email.'</td>
        </tr>
        </table>
        </td>
        <td align="right">
        <table cellpadding="0" cellspacing="0">
        <tr>
        <td style="font-size:15px; font-weight:bold; font-family:Verdana; color:#000; line-height:28px;">Invoice</td>
        <td></td>
        <td align="left" style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.$vendor_info[0]->invoice_id.'</td>
        </tr>
        <tr>
        <td style="font-size:15px; font-weight:bold; font-family:Verdana; color:#000; line-height:28px;">Delivery date</td>
        <td></td>
        <td align="left" style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.date('F d, Y', strtotime($delivery_details->delivery_date)).'</td>
        </tr>
        <tr>
        <td style="font-size:15px; font-weight:bold; font-family:Verdana; color:#000; line-height:28px;">Invoice date</td>
        <td></td>
        <td align="left" style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">'.date('F d, Y', strtotime($vendor_info[0]->created_date)).'</td>
        </tr>
        <tr>
        <td style="font-size:11px; font-weight:bold; font-family:Verdana; color:#000; line-height:28px; background:#d1d5d4; padding:0 9px;">AMOUNT DUE</td>
        <td></td>
        <td align="left" style="font-size:16px; font-weight:500; font-family:dejavu sans,arial;  color:#666; line-height:28px;background:#d1d5d4;padding:0 9px;">'.$total_amount.'</td>
        </tr>
        </table>
        </td>
        <td width="20">&nbsp;</td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <!-- end 2 tr -->
        <tr>
        <td>
        <table cellpadding="0" cellspacing="0" width="100%">
        <tr style="background:#d1d5d4;padding:0 9px;">
        <td align="center" style=" padding:7px 0; font-size:17px; font-family:Verdana; font-weight:bold;">Item</th>
        <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Description</th>
        <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Quantity</th>
        <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Unit cost</th>
        <td align="center" style=" padding:7px 0;font-size:17px; font-family:Verdana; font-weight:bold;">Line total</th>
        </tr>'.$item.'
        </table>
        </td>
        </tr>
        <!-- end 3 tr -->
        <tr>
        <td>
        <table style="padding-top: 25px; padding-bottom: 25px;" width="787" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td width="20">&nbsp;</td>
        <td>
        <table>
        <tbody><tr>
        <td style="font-size:16px; font-weight:bold; font-family:Verdana; color:#000; padding-bottom:10px;">NOTES / MEMO :</td>
        </tr>
        <tr>
        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;">Free shipping with 30-day money-back guarntee </td>
        </tr>
        </tbody></table>
        </td>
        <td align="right">
        <table cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size:15px; font-weight:bold; font-family:arial; color:#000; line-height:28px;">SUBTOTAL</td>
        <td width="10"></td>
        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;" align="right">'.$sub_total.'</td>
        </tr>
        <tr>
        <td style="font-size:15px; font-weight:bold; font-family:arial; color:#000; line-height:28px;">Delivery fee</td>
        <td width="10"></td>
        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;" align="right">'.$delivery_charge.'</td>
        </tr>
        <tr>
        <td style="font-size:15px; font-weight:bold; font-family:arial; color:#000; line-height:28px;">Tax</td>
        <td width="10"></td>
        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;" align="right">'.$service_tax.'</td>
        </tr>
        <tr>
        <td style="font-size:15px; font-weight:bold; font-family:arial; color:#000; line-height:28px; background:#d1d5d4; padding:0 9px;">TOTAL</td>
        <td style="background:#d1d5d4;padding:0 9px;" width="10"></td>
        <td style="font-size:16px; font-weight:500; font-family:arial; color:#666; line-height:28px;background:#d1d5d4;padding:0 9px;" align="right">'.$total_amount.'</td>
        </tr>
        </tbody></table>
        </td>
        <td width="20">&nbsp;</td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td>
        <table>
        <tr>
        <td width="20">&nbsp;</td>
        <td style="font-size:12px; font-family:arial; color:#666;padding:10px 10px 0 0"><b style="font-family: arial; font-weight: bold;">'.trans('messages.Returns Policy: ').'</b>' .trans('messages.At FreshNexG we try to deliver perfectly each and every time. But in the off-chance that you need to return the item, please do so with the ').'<b style="font-family: arial; font-weight: bold;">'.trans('messages.original Brand').''.trans('messages.box/price tag, original packing and invoice').'</b> '.trans('messages.without which it will be really difficult for us to act on your request. Please help us in helping you. Terms and conditions apply').'</td>
        <td width="20">&nbsp;</td>
        </tr>
        </tbody>
        </table>';
        /*$logo = url('/assets/front/'.Session::get("general")->theme.'/images/'.Session::get("general")->theme.'.png');
        if(file_exists(base_path().'/public/assets/admin/base/images/vendors/list/'.$vendor_info[0]->logo_image)) { 
            $vendor_image ='<img width="100px" height="100px" src="'.URL::to("assets/admin/base/images/vendors/list/".$vendor_info[0]->logo_image).'") >';
        } else{  
            $vendor_image ='<img width="100px" height="100px" src="'.URL::to("assets/front/'.Session::get("general")->theme.'/images/blog_no_images.png").'") >';
        }
        $delivery_date = date("d F, l", strtotime($delivery_details[0]->delivery_date)); 
        $delivery_time = date('g:i a', strtotime($delivery_details[0]->start_time)).'-'.date('g:i a', strtotime($delivery_details[0]->end_time));
        $sub_total = 0;
        $item = '';
        foreach($order_detail as $items){
            $item .='<tr>
            <td style="width: 200px; padding: 15px 15px;" valign="middle"><a style="text-decoration: none; font-size: 16px; color: #333; font-family: arial;" title="" href="#"><img width="50px" height="50px" style="vertical-align: middle;" src='.url('/assets/admin/base/images/products/thumb/'.$items->product_image).' alt="del" /></a>
            <p style="margin: 10px 0 0 0;">'.str_limit(ucfirst(strtolower($items->product_name)),30).'</p>
            </td>
            <td style="font-size: 16px; color: #333; font-family: arial; text-align: right; padding: 0 15px;" width="100">'.$items->item_cost.getCurrency().'</td>
            <td style="font-size: 16px; color: #333; font-family: arial; text-align: right;" width="100">'.$items->item_unit.'</td>
            <td style="font-size: 16px; color: #333; font-family: arial; text-align: right; padding: 0 15px;" width="100">'.($items->item_cost*$items->item_unit).getCurrency().'</td>
            </tr>
            <tr>
            <td colspan="5" width="100%">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
            <td style="width: 100%; border-bottom: 1px solid #ccc;">&nbsp;</td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>';
            $sub_total += $items->item_cost*$items->item_unit;
        }
        if($delivery_details[0]->order_type == 1){
            $delivery_address ='<tr>
            <td style="font-size: 16px; color: #333; font-family: arial; text-align: left; padding: 15px 13px;">'.trans('messages.Delivery address').'</td>
            <td>:</td>
            <td style="font-size: 16px; color: #333; font-family: arial; text-align: right; padding: 15px 13px;">'.$delivery_details[0]->address.'</td>
            </tr>';
        }else {
            $delivery_address ='<tr>
            <td style="font-size: 16px; color: #333; font-family: arial; text-align: left; padding: 15px 13px;">'.trans('messages.Pickup address').'</td>
            <td>:</td>
            <td style="font-size: 16px; color: #333; font-family: arial; text-align: right; padding: 15px 13px;">'.$delivery_details[0]->address.'</td>
            </tr>';
        }
        $html ='<table style="border: 1px solid #ccc;" border="0" width="450" align="left" cellspacing="0" cellpadding="0" bgcolor="#fff">
        <tbody>
        <tr>
        <td style="border-bottom: 1px solid #ccc; padding: 15px 15px;">
        <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td><a title="" href="#"><img src='.$logo.' alt="'.Session::get("general")->theme.'" /></a></td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td style="border-bottom: 1px solid #ccc; padding: 15px 15px;">
        <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 20px; font-weight: normal; color: #333; font-family: arial;">Order Summary</td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td>
        <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="width: 250px; padding: 15px 15px;">'.$vendor_image.'
        <h3 style="font-size: 20px; font-family: arial; color: #888; font-weight: normal;">'.$vendor_info[0]->vendor_name.'</h3>
        </td>
        <td style="width: 350px;">
        <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="width: 50%; font-size: 20px; font-family: arial; color: #333; font-weight: normal; text-align: right; padding-bottom: 15px;">'.trans('messages.Order Id').'</td>
        <td width="150">&nbsp;</td>
        <td style="width: 50%; font-size: 20px; font-family: arial; color: #888; font-weight: normal; text-align: right; padding: 0 15px 15px;">'.$vendor_info[0]->order_key_formated.'</td>
        </tr>
        <tr>
        <td style="width: 50%; font-size: 20px; font-family: arial; color: #333; font-weight: normal; text-align: right; padding-bottom: 15px;">'.trans('messages.Date').'</td>
        <td width="150">&nbsp;</td>
        <td style="width: 50%; font-size: 20px; font-family: arial; color: #888; font-weight: normal; text-align: right; padding: 0 15px 15px;">'.date('d M, Y', strtotime($vendor_info[0]->created_date)).'</td>
        </tr>
        <tr>
        <td style="width: 50%; font-size: 20px; font-family: arial; color: #333; font-weight: normal; text-align: right;">'.trans('messages.Status').'</td>
        <td width="150">&nbsp;</td>
        <td style="width: 50%; font-size: 20px; font-family: arial; color: #888; font-weight: normal; text-align: right; padding: 0 15px;">'.$vendor_info[0]->name.'</td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td style="border-bottom: 1px solid #ccc; padding: 15px 15px;">
        <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 20px; font-weight: normal; color: #333; font-family: arial;">'.trans('messages.Bill details').'</td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td>
        <table border="0" cellspacing="0" cellpadding="0">
        <tbody>'.$item.'</tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td>
        <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        
        <td style="padding-bottom: 15px; padding-top: 15px;" width="300">&nbsp;</td>
        
        <td style="font-size: 16px; color: #333; padding-bottom: 15px; padding-top: 15px; font-family: arial; text-align: right;">'.trans('messages.Subtotal').'</td>
        <td style="font-size: 16px; color: #333; font-family: arial; text-align: right; padding: 15px 15px 15px;">'.$sub_total.getCurrency().'</td>
        </tr>
        <tr>
        
        <td style="padding-bottom: 15px;" width="300">&nbsp;</td>
        
        <td style="font-size: 16px; color: #e91e63; font-family: arial; text-align: right; padding-bottom: 15px;">'.trans('messages.Delivery fee').'</td>
        <td style="font-size: 20px; color: #e91e63; font-family: arial; text-align: right; padding: 0 15px 15px;">'.$delivery_details[0]->delivery_charge.getCurrency().'</td>
        </tr>
        <tr>
        <td style="padding-bottom: 15px;" width="300">&nbsp;</td>
        <td style="font-size: 16px; color: #333; font-family: arial; text-align: right; padding-bottom: 15px;">'.trans('messages.Tax').'</td>
        <td style="font-size: 16px; color: #333; font-family: arial; text-align: right; padding: 0 15px 15px;">'.$delivery_details[0]->service_tax.getCurrency().'</td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td style="border-top: 1px solid #ccc; border-bottom: 1px solid #ccc; padding: 15px 0;">
        <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 16px; color: #333; font-family: arial; text-align: right;" width="450">'.trans('messages.Total').'</td>
        <td style="font-size: 20px; color: #e91e63; font-family: arial; text-align: right; padding: 0 15px;">'.$delivery_details[0]->total_amount.getCurrency().'</td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <tr>
        <td>
        <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
        <tbody>'.$delivery_address.'<tr>
        <td style="font-size: 16px; color: #333; font-family: arial; text-align: left; padding: 15px 13px;">'.trans('messages.Delivery slot').'</td>
        <td>:</td>
        <td style="font-size: 16px; color: #333; font-family: arial; text-align: right; padding: 15px 13px;">'.$delivery_date ." : ". $delivery_time.'</td>
        </tr>
        <tr>
        <td style="font-size: 16px; color: #333; font-family: arial; text-align: left; padding: 15px 13px;">'.trans('messages.Payment mode').'</td>
        <td>:</td>
        <td style="font-size: 16px; color: #333; font-family: arial; text-align: right; padding: 15px 13px;">'.$vendor_info[0]->payment_gateway_name.'</td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table>'; 
        Send Email To vendor here  
                  

    */
    }
    
    public function order_detail(Request $data)
    {
        $post_data = $data->all();
    
        //  return $post_data;

        //  MULTIPLE LANGUAGES NO NEED NOW
/*        
         App::setLocale('en');
        if ($post_data['language'] == 2) {
            App::setLocale('ar');
        }
*/        
        //  $language_id = $post_data['language'];
        //  $user_id = $post_data['user_id'];
        $order_id = $post_data['order_id'];


        $booking_details = DB::table('booking_details')
                            ->select('booking_details.charges as bc_charges','booking_details.created_date as bc_date','booking_details.charges as bc_charges','admin_customers.id as cust_id','booking_details.id as bid','booking_details.*','admin_customers.*','booking_status.*','booking_info.*')
                            ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                            ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                            ->leftJoin('rooms','rooms.id','booking_info.room_id')
                            ->leftJoin('room_type','room_type.id','booking_details.room_type')
                            ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                            ->where('booking_details.id','=',$order_id)
                            ->get();

        //  return $booking_details;



        //  return $room;

        $outlet_id = (count($booking_details) > 0)? $booking_details[0]->outlet_id:'';

        $vendor_id = (count($booking_details) > 0)? $booking_details[0]->vendor_id:'';

        //  return $vendor_id;

        $outlets = DB::table('outlets')
                    ->select('outlets.*','outlet_infos.*')
                    ->leftJoin('outlet_infos','outlet_infos.id','outlets.id')
                    ->where('outlets.id','=',$outlet_id)
                    ->get();

        $vendors = DB::table('vendors_view')
                    ->select('vendors_view.*')
                    ->where('vendors_view.id','=',$vendor_id)
                    ->get();

        //  return $vendors;                

        $rooms_count = DB::table('booking_details')
                    ->select(DB::RAW('count(booking_info.id) as rooms_count'),'rooms_infos.room_name')
                    ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                    ->leftJoin('rooms','rooms.id','booking_info.room_id')
                    ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                    ->where('booking_details.id','=',$order_id)
                    ->groupby('rooms_infos.room_name')
                    ->get();

        //  return $rooms_count;

        $room_count = $rooms_count->pluck('rooms_count')->sum();

        $room_name = $rooms_count->pluck('room_name')->toArray();

        //  return $room_name;

        //$roooms = DB::table('')

             // return $rooms_count;

        


        if(count($outlets) > 0)  {
            $result = array("response" => array("httpCode" => 200, "status" => true,
                            "properties" => $outlets, "booking_details" => $booking_details,
                            "rooms_count" => $rooms_count, "outlet_id" => $outlet_id,
                            "room_count" => $room_count, "room_name" => $room_name,
                            "vendors" => $vendors,
                            "Message" => "Booking Details"));
        } else {
            $result = array("response" => array("httpCode" => 400, "status" => false,
                            "properties" => $outlets, "booking_details" => $booking_details,
                            "rooms_count" => $rooms_count, "outlet_id" => $outlet_id,
                            "room_count" => $room_count, "room_name" => $room_name,
                            "vendors" => $vendors,
                            "Message" =>  "No Booking Found."));
        }            

        return json_encode($result);
    }


    public function get_multi_order_detail($data)
    {
        $post_data = $data;
        $language_id = $post_data['language'];
        $order_info = DB::select('SELECT o.id as order_id, o.customer_id,o.created_date,o.total_amount,o.delivery_charge,o.payment_gateway_id,o.invoice_id,o.order_status,order_status.name, o.coupon_id,
           o.coupon_amount
            FROM orders o
            left join order_status order_status on order_status.id = o.order_status
            WHERE o.id = ? AND o.customer_id = ? ',array($post_data['order_id'],$post_data['user_id']));




        $query = 'pi.lang_id = (case when (select count(*) as totalcount from products_infos where products_infos.lang_id = '.$language_id.' and p.id = products_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $query3 = '"vendors_infos"."lang_id" = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = '.$language_id.' and vendors.id = vendors_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $query4 = '"payment_gateways_info"."language_id" = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $query5 = '"outlet_infos"."language_id" = (case when (select count(outlet_infos.language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.getCurrentLang().' and outlets.id = outlet_infos.id) > 0 THEN '.getCurrentLang().' ELSE 1 END)';
        $wquery = '"weight_classes_infos"."lang_id" = (case when (select count(weight_classes_infos.lang_id) as totalcount from weight_classes_infos where weight_classes_infos.lang_id = '.$language_id.' and weight_classes.id = weight_classes_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';

            $order_detail = DB::select('SELECT vendors_infos.vendor_name, vendors.id as vendor_id, vendors.logo_image, vendors.contact_address, vendors.contact_email, outlet_infos.outlet_name, outlets.id as outlet_id, payment_gateways_info.name as payment_gateway_name,o.id as order_id,orders_info.outlet_id,o.order_key_formated,o.coupon_amount,o.total_amount,o.delivery_charge,o.invoice_id,vendors.email,
                orders_info.item_id, orders_info.item_cost, orders_info.item_unit , orders_info.id as orders_info_id
                FROM 
                orders o 
                left join orders_info on o.id = orders_info.order_id
                left join vendors vendors on vendors.id = orders_info.vendor_id
                left join vendors_infos vendors_infos on vendors_infos.id = vendors.id
                left join outlets outlets on outlets.id = orders_info.outlet_id
                left join outlet_infos outlet_infos on outlet_infos.id = outlets.id
                left join order_status order_status on order_status.id = o.order_status
                left join payment_gateways payment_gateways on payment_gateways.id = o.payment_gateway_id
                left join payment_gateways_info payment_gateways_info on payment_gateways_info.payment_id = payment_gateways.id
                where '.$query3.' AND '.$query4.' AND '.$query5.' AND o.id = '. $post_data['order_id'] .' AND o.customer_id = ' . $post_data['user_id'] .  ' ORDER BY o.id, vendors.id, outlets.id');





            foreach($order_detail as $out){
                $out->item = DB::select('SELECT p.product_image,p.id AS product_id,oi.item_cost,oi.item_unit,oi.item_offer,pi.product_name,pi.description,p.weight,weight_classes_infos.title,weight_classes_infos.unit as unit_code
                    FROM orders_info oi
                    LEFT JOIN products p ON p.id = oi.item_id
                    LEFT JOIN products_infos pi ON pi.id = p.id
                    LEFT JOIN weight_classes ON weight_classes.id = p.weight_class_id
                    LEFT JOIN weight_classes_infos ON weight_classes_infos.id = weight_classes.id
                    where '.$query.' AND '.$wquery.' AND oi.id = '.$out->orders_info_id.' ORDER BY oi.id');
            }
    

        $query2 = 'pgi.language_id = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and pg.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $delivery_details = DB::select('SELECT o.delivery_instructions,o.delivery_slot_type,o.delivery_slot,o.delivery_slot_type,trans.currency_code,pg.id as payment_gateway_id,pgi.name,o.total_amount,o.delivery_charge,o.service_tax,dti.start_time,end_time,o.created_date,o.delivery_date,o.order_type,u.name as user_name,u.first_name,u.last_name,u.email,u.mobile,ua.address as contact_address

            FROM orders o
            LEFT JOIN user_address ua ON ua.id = o.delivery_address
            LEFT JOIN users u ON u.id = ua.user_id
            left join payment_gateways pg on pg.id = o.payment_gateway_id
            left join payment_gateways_info pgi on pgi.payment_id = pg.id
            left join delivery_time_slots dts on dts.id=o.delivery_slot
            left join delivery_time_interval dti on dti.id = dts.time_interval_id
            left join transaction trans on trans.order_id = o.id
            where '.$query2.'AND o.id = ? AND o.customer_id= ?',array($post_data['order_id'],$post_data['user_id']));

        $result = array("order_detail"=>$order_detail,"delivery_details"=>$delivery_details, 'order_info'=> $order_info[0]);

        return $result;
    }

    public function get_order_detail($data)
    {
        $post_data = $data;
        $language_id = $post_data['language'];
 
        $order_info = DB::select('SELECT o.id as order_id, o.customer_id,o.created_date,o.total_amount,o.delivery_charge,o.payment_gateway_id,o.invoice_id,o.order_status,order_status.name, o.coupon_id,
           o.coupon_amount
            FROM orders o
            left join order_status order_status on order_status.id = o.order_status
            WHERE o.id = ? AND o.customer_id = ? ',array($post_data['order_id'],$post_data['user_id']));



        $query = 'pi.lang_id = (case when (select count(*) as totalcount from products_infos where products_infos.lang_id = '.$language_id.' and p.id = products_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $query3 = '"vendors_infos"."lang_id" = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = '.$language_id.' and vendors.id = vendors_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $query4 = '"payment_gateways_info"."language_id" = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $query5 = '"outlet_infos"."language_id" = (case when (select count(outlet_infos.language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.$language_id.' and outlets.id = outlet_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $wquery = '"weight_classes_infos"."lang_id" = (case when (select count(weight_classes_infos.lang_id) as totalcount from weight_classes_infos where weight_classes_infos.lang_id = '.$language_id.' and weight_classes.id = weight_classes_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';

 



            $order_detail = DB::select('SELECT vendors_infos.vendor_name, vendors.id as vendor_id, vendors.logo_image, vendors.contact_address, vendors.contact_email, outlet_infos.outlet_name,
                outlet_infos.contact_address as outlet_address,payment_gateways_info.name as payment_gateway_name,o.id as order_id,orders_info.outlet_id,o.order_key_formated,o.coupon_amount,o.total_amount,o.delivery_charge,o.invoice_id,vendors.email
                FROM 
                orders o 
                left join orders_info on o.id = orders_info.order_id
                left join vendors vendors on vendors.id = orders_info.vendor_id
                left join vendors_infos vendors_infos on vendors_infos.id = vendors.id
                left join outlets outlets on outlets.id = orders_info.outlet_id
                left join outlet_infos outlet_infos on outlet_infos.id = outlets.id
                left join order_status order_status on order_status.id = o.order_status
                left join payment_gateways payment_gateways on payment_gateways.id = o.payment_gateway_id
                left join payment_gateways_info payment_gateways_info on payment_gateways_info.payment_id = payment_gateways.id
                where '.$query3.' AND '.$query4.' AND '.$query5.' AND o.id = '. $post_data['order_id'] .' AND o.customer_id = ' . $post_data['user_id'] .  '  group by outlets.id,vendors.id, vendors_infos.vendor_name,outlet_infos.outlet_name,payment_gateways_info.name,o.id,orders_info.outlet_id, outlet_infos.contact_address    ORDER BY vendor_id , outlets.id asc ');


 //        // get original model
 //        $fetchMode = DB::getFetchMode();
 //        // set mode to custom
 //        DB::setFetchMode(\PDO::FETCH_ASSOC);
 //        // get data
 // //echo '<pre>';


            foreach($order_detail as $key=>$out){

                $fixed_p = DB::select('SELECT p.product_image,p.id AS product_id,oi.item_cost,oi.item_unit,pi.product_name,pi.description,p.weight,weight_classes_infos.title,weight_classes_infos.unit as unit_code, oi.product_variant_id as variant_id, oi.id as order_info_id
                    FROM orders_info oi
                    LEFT JOIN products p ON p.id = oi.item_id
                    LEFT JOIN products_infos pi ON pi.id = p.id
                    LEFT JOIN weight_classes ON weight_classes.id = p.weight_class_id
                    LEFT JOIN weight_classes_infos ON weight_classes_infos.id = weight_classes.id
                    where '.$query.' AND '.$wquery.' AND oi.outlet_id = '.$out->outlet_id.' AND oi.order_id = ' . $post_data['order_id'] . ' ORDER BY oi.id');



                $var_p = DB::select('SELECT p.product_image,p.id AS product_id,oi.item_cost,oi.item_unit,pi.product_name,pi.description,pv.weight_value as weight,weight_classes_infos.title,weight_classes_infos.unit as unit_code, oi.product_variant_id as variant_id, oi.id as order_info_id
                FROM orders_info oi
                JOIN products p ON p.id = oi.item_id
                LEFT JOIN products_infos PI ON pi.id = p.id
                JOIN product_variants pv ON oi.product_variant_id = pv.variant_id


                Left join weight_classes ON weight_classes.id = pv.weight_class_id
                Left join weight_classes_infos   ON weight_classes_infos.id =weight_classes.id

                where  coalesce(oi.product_variant_id,0) != 0 AND  '.$query.' AND '.$wquery.' AND oi.outlet_id = '.$out->outlet_id.' AND oi.order_id = ' . $post_data['order_id'] . ' ORDER BY oi.id');
                
     


                       // // restore mode the original
                       //  DB::setFetchMode($fetchMode);

                        $output = array();

                        if( count($fixed_p) > 0 ){
                            foreach ($fixed_p as $key => $value) {
                                $output[] =  $value;
                            }
                        }


                        if( count($var_p) > 0 ){
                            foreach ($var_p as $key => $value) {
                                $value->product_name = $value->product_name . ' (' . $value->weight . ' ' . $value->unit_code . ')' ;
                                $output[] =  $value;
                            }
                        }






                       // array_multisort(array_column($output, 'order_info_id'), $output);


                        // if( count($output) > 0 ){
                        //     foreach ($output as $key => $value) {


                        //         if(!empty($value))
                        //         $output[$key] =  (object)$value;

            
                        //     }
                        // }

                       $out->item =  $output;


}
                foreach($order_detail as $key=>$out){

                    $outlet_total = 0;

                    foreach($out->item as $items)
                    {
                       $outlet_total += $items->item_cost * $items->item_unit;
                    }

                    $out->total_amount = $outlet_total;
                }


                // foreach($out->item as $niv)
                // {
                //     $niv->mup ='cup';
                //     if(!empty($niv->variant_id))
                //     {
                //         $wquery = '"weight_classes_infos"."lang_id" = (case when (select count(weight_classes_infos.lang_id) as totalcount from weight_classes_infos where weight_classes_infos.lang_id = ' . $language_id . ' and weight_classes.id = weight_classes_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';

                //         $actual_weight_unit = DB::table('product_variants')
                //         ->select('weight_classes_infos.unit as unit_code','product_variants.weight_value as weight','product_variants.quantity as product_qty')
                //         ->leftJoin('weight_classes', 'weight_classes.id', '=', 'product_variants.weight_class_id')
                //         ->leftJoin('weight_classes_infos', 'weight_classes_infos.id', '=', 'weight_classes.id')
                //         ->where('product_variants.variant_id', '=', $niv->variant_id)
                //         ->whereRaw($wquery)
                //         ->first();

                //         $niv->mip = $actual_weight_unit;
                //     }
                // }


        $query2 = 'pgi.language_id = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and pg.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        
        $delivery_details = DB::select('SELECT o.delivery_instructions,o.delivery_slot_type,o.delivery_slot,o.delivery_slot_type,trans.currency_code,pg.id as payment_gateway_id,pgi.name,o.total_amount,o.delivery_charge,o.service_tax,dti.start_time,end_time,o.created_date,o.delivery_date,o.order_type,u.name as user_name,u.first_name,u.last_name,u.email,u.mobile,ua.address as contact_address

            FROM orders o
            LEFT JOIN user_address ua ON ua.id = o.delivery_address
            LEFT JOIN users u ON u.id = ua.user_id
            left join payment_gateways pg on pg.id = o.payment_gateway_id
            left join payment_gateways_info pgi on pgi.payment_id = pg.id
            left join delivery_time_slots dts on dts.id=o.delivery_slot
            left join delivery_time_interval dti on dti.id = dts.time_interval_id
            left join transaction trans on trans.order_id = o.id
            where '.$query2.'AND o.id = ? AND o.customer_id= ?',array($post_data['order_id'],$post_data['user_id']));

        $result = array("order_detail"=>$order_detail,"delivery_details"=>$delivery_details[0], 'order_info'=> $order_info[0] );
// echo '<pre>';

// //
// print_r(  $result );
// exit;
        return $result;



        // --------------------------------------
 
    }


    public function get_ordser_detail($data)
    {
        $post_data = $data;
        //print_r($data);exit;
        $language_id = $post_data['language'];
        $query3 = '"vendors_infos"."lang_id" = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = '.$language_id.' and vendors.id = vendors_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $query4 = '"payment_gateways_info"."language_id" = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';

        $vendor_info = DB::select('SELECT vendors_infos.vendor_name, vendors.logo_image, vendors.contact_address, vendors.contact_email, o.id as order_id,o.created_date,o.order_status,order_status.name,payment_gateways_info.name as payment_gateway_name,oinf.outlet_id,vendors.id as vendor_id,o.order_key_formated,o.invoice_id, vendors.email FROM orders o
            left join orders_info oinf on o.id = oinf.order_id
        left join vendors vendors on vendors.id = oinf.vendor_id
        left join vendors_infos vendors_infos on vendors_infos.id = vendors.id
        left join order_status order_status on order_status.id = o.order_status
        left join payment_gateways payment_gateways on payment_gateways.id = o.payment_gateway_id
        left join payment_gateways_info payment_gateways_info on payment_gateways_info.payment_id = payment_gateways.id
        where '.$query3.' AND '.$query4.' AND o.id = ? AND o.customer_id= ? ORDER BY o.id',array($post_data['order_id'],$post_data['user_id']));

        $query = 'pi.lang_id = (case when (select count(*) as totalcount from products_infos where products_infos.lang_id = '.$language_id.' and p.id = products_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $order_items = DB::select('SELECT p.product_image,p.id AS product_id,oi.item_cost,oi.item_unit,oi.item_offer,o.total_amount,o.delivery_charge,o.service_tax,o.invoice_id,pi.product_name,pi.description,o.coupon_amount
        FROM orders o
        LEFT JOIN orders_info oi ON oi.order_id = o.id
        LEFT JOIN products p ON p.id = oi.item_id
        LEFT JOIN products_infos pi ON pi.id = p.id
        where '.$query.' AND o.id = ? AND o.customer_id= ? ORDER BY oi.id',array($post_data['order_id'],$post_data['user_id']));

        $query2 = 'pgi.language_id = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and pg.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
         $query5 = 'out_infos.language_id = (case when (select count(*) as totalcount from outlet_infos where outlet_infos.language_id = '.$language_id.' and out.id = outlet_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $delivery_details = DB::select('SELECT o.delivery_instructions,ua.address,pg.id as payment_gateway_id,pgi.name,o.total_amount,o.delivery_charge,o.service_tax,dti.start_time,end_time,o.created_date,o.delivery_date,o.order_type,out_infos.contact_address,o.coupon_amount,ua.address as user_contact_address, u.email FROM orders o
            left join orders_info oinf on o.id = oinf.order_id
                    LEFT JOIN user_address ua ON ua.id = o.delivery_address
                    LEFT JOIN users u ON u.id = ua.user_id
                    left join payment_gateways pg on pg.id = o.payment_gateway_id
                    left join payment_gateways_info pgi on pgi.payment_id = pg.id
                    left join delivery_time_slots dts on dts.id=o.delivery_slot
                    left join delivery_time_interval dti on dti.id = dts.time_interval_id
                    left join outlets out on out.id = oinf.outlet_id
                    left join outlet_infos out_infos on out_infos.id = out.id
                    where '.$query2.' AND '.$query5.'AND o.id = ? AND o.customer_id= ?',array($post_data['order_id'],$post_data['user_id']));
        $result = array("order_items"=>array(),"delivery_details"=>array(),"vendor_info"=>array());
        if(count($order_items)>0 && count($delivery_details)>0 && count($vendor_info)>0)
        {
            $result = array("order_items"=>$order_items,"delivery_details"=>$delivery_details,"vendor_info"=>$vendor_info);
        }
        return $result;
    }
    
    public function return_reason($language)
    {
        $return_reasons= array();
        $result = array("response" => array("httpCode" => 400, "status" => false, "return_reasons" =>$return_reasons));
        $query = '"return_reason"."lang_id" = (case when (select count(*) as totalcount from return_reason where return_reason.lang_id = '.$language.') > 0 THEN '.$language.' ELSE 1 END)';
        $return_reasons=DB::table('return_reason')
                ->whereRaw($query)
                ->orderBy('id', 'asc')
                ->get();
        $reasons_array = array();
        foreach($return_reasons as $reasons)
        {
            $reasons_array[$reasons->id] = ucfirst(strtolower($reasons->name));
        }
        return $reasons_array;
    }
    /* Mobile api return reason */
    public function mob_return_reason($language)
    {
        $return_reasons= array();
        $result = array("response" => array("httpCode" => 400, "status" => false, "return_reasons" =>$return_reasons));
        $query = '"return_reason"."lang_id" = (case when (select count(return_reason.lang_id) as totalcount from return_reason where return_reason.lang_id = '.$language.') > 0 THEN '.$language.' ELSE 1 END)';
        $return_reasons = DB::table('return_reason')
                            ->whereRaw($query)
                            ->orderBy('id', 'asc')
                            ->get();
        $reasons_array = array();
        $r = 0;
        foreach($return_reasons as $reasons)
        {
            $reasons_array[$r]['id']   = $reasons->id;
            $reasons_array[$r]['name'] = $reasons->name;
            $r++;
        }
        return $reasons_array;
    }
    
    public function update_promocode(Request $data)
    {

        
        $current_date = strtotime(date('Y-m-d'));
		$post_data = $data->all();

        //  return $post_data;

        $total_amount = $post_data['total'];

		$coupon_details = DB::table('coupons')
                        ->select('coupons.id as coupon_id', 'coupon_type', 'offer_amount', 'coupon_code', 'start_date', 'end_date', 'offer_type', 'minimum_order_amount', 'offer_percentage',
                            'user_limit','coupon_limit')
						->leftJoin('coupon_outlet','coupon_outlet.coupon_id','=','coupons.id')
						->where('coupon_code','=',$post_data['promo_code'])
						->where('coupon_outlet.outlet_id','=',$post_data['outlet_id'])
						->first();

        //  return array($coupon_details);
		if(count($coupon_details) == 0)
		{
			$result = array("response" => array("httpCode" => 400,"Message" => trans("messages.This coupon is not applicable for the current store.")));
			return json_encode($result);
		}
		else if((strtotime($coupon_details->start_date) <= $current_date) && (strtotime($coupon_details->end_date) >= $current_date) && (($coupon_details->minimum_order_amount) <= $post_data['total'] ) )
		{

            //  IF COUPON OFFER AMOUNT EXCEEDS TOTAL AMOUNT -> COUPON 

            if($coupon_details->offer_type == 1)
            {
                $offer_amount = $coupon_details->offer_amount;

                if($total_amount <= $offer_amount ){

                    $result = array("response" => array("httpCode" => 400,"Message" => "To apply this coupon code your booking amount should be minimum ".$offer_amount.""));
                    return json_encode($result);

                }
            }

            //  USER LIMIT  && COUPON LIMIT
              
            $coupon_user_limit_details = DB::table('booking_details')
                                            //  ->select('coupon_id')
                                            //  ->where('customer_id','=',$post_data['user_id'])
                                            ->leftJoin('coupons','coupons.id','=','booking_details.coupon_id')
                                            ->where('coupons.id',$coupon_details->coupon_id)
                                            ->count();



            if(count($coupon_user_limit_details)>0)
            {   
                if($coupon_user_limit_details >= $coupon_details->user_limit)
                {
                    $result = array("response" => array("httpCode" => 400,"Message" => "Max user limit has been crossed"));
                    return json_encode($result);
                }
                if($coupon_user_limit_details >= $coupon_details->coupon_limit)
                {
                    $result = array("response" => array("httpCode" => 400,"Message" => "Max coupon limit has been crossed"));
                    return json_encode($result);
                }
            }                                           


			$result = array("response" => array("httpCode" => 200,"status" => true, "Message" => trans("messages.Coupon applied Successfully"), "offer_type"=>$coupon_details->offer_type, "coupon_details"=>$coupon_details  /*     ,"coupon_user_limit_details"=>$coupon_user_limit_details */  ));
			return json_encode($result);
		}
		else {
			$result = array("response" => array("httpCode" => 400,"Message" => trans("messages.Invalid promocode.")));
			return json_encode($result);
		}
    }
    
    public function send_otp(Request $data)
    {
        if(isset($data['language']) && $data['language']==2)
		{
			App::setLocale('ar');
		}
		else 
		{
			App::setLocale('en');
		}
		$post_data  = $data->all();
		$user_id    = $post_data['user_id'];
		$otp_option = $post_data['otp_option'];
        $otp        = rand(100000,999999);
        $users      = Users::find($user_id);
        if($otp_option == 2 || $otp_option == 3)
        {  
            $number  = $users->mobile;
            $message = 'You have received OTP password for '.getAppConfig()->site_name.'. Your OTP code is '.$otp.'. This code can be used only once and dont share this code with anyone.';
            $twilo_sid    = "AC648e25ad93bdcbb9788baa76f1735e3c";
            $twilio_token = "cca34a4a0811d1d4161470812348bf5c";
            $from_number  = "+14055627423";
			$client = new Services_Twilio($twilo_sid, $twilio_token);
			//print_r ($client);exit;
            // Create an authenticated client for the Twilio API
            try {
				$m = $client->account->messages->sendMessage(
						$from_number, // the text will be sent from your Twilio number
						$number, // the phone number the text will be sent to
						$message // the body of the text message
                   );
				//echo $m->body;exit;
                $users->otp = $otp;
                $users->updated_date = date("Y-m-d H:i:s");
                $users->save();
                if($otp_option == 2)
                {
					$result = array("response" => array("httpCode" => 200,"Message" => trans("messages.we have sent verification code to your mobile.")));
					return json_encode($result);
				}
				
			
            }
            catch (Exception $e) {
				$result = array("response" => array("httpCode" => 400,"Message" => $e->getMessage()));
				return json_encode($result);
			}
			catch(\Services_Twilio_RestException $e) {
               $result = array("response" => array("httpCode" => 400,"Message" => trans("messages.Invalid Phone number.")));
               return json_encode($result);
            }
        }
        if($otp_option == 1 || $otp_option == 3)
        {
            $template = DB::table('email_templates')
                                ->select('from_email', 'from', 'subject', 'template_id','content')
                                ->where('template_id','=',self::OTP_EMAIL_TEMPLATE)
                                ->get();
            if(count($template))
      
            {
                $from      = $template[0]->from_email;
                $from_name = $template[0]->from;
                $subject   = $template[0]->subject;
                if(!$template[0]->template_id)
                {
                    $template  = 'mail_template';
                    $from      = getAppConfigEmail()->contact_email;
                    $subject   = getAppConfig()->site_name." OTP Password";
                    $from_name = "";
                }
                $content = array("name" => ucfirst($users->name), "otp_password" => "".$otp);
				$email   = smtp($from, $from_name, $users->email, $subject, $content, $template);
                $users   = Users::find($user_id);
                $users->otp = $otp;
                $users->updated_date = date("Y-m-d H:i:s");
                $users->save();
                if($otp_option == 3)
                {
					$result = array("response" => array("httpCode" => 200,"Message" => trans("messages.we have sent verification code to your mobile and to your email.")));
					return json_encode($result);
				}
				else
				{
					
                $result = array("response" => array("httpCode" => 200,"Message" => trans("messages.we have sent verification code to your email.")));
			    }
            }
        }
        return json_encode($result);
    }


        

   
    public function check_otp(Request $data)
    {
        $post_data = $data->all();
        $coupon_details = DB::table('users')
                        ->select('id')
                        ->where('id','=',$post_data['user_id'])
                        ->where('otp','=',$post_data['otp'])
                        ->first();
        $result = array("response"=>array("httpCode" => 400,"Message" => trans("messages.Order verification code is wrong"),"order_items"=>array()));
        if(count($coupon_details)>0)
        {
            $result = array("response" => array("httpCode" => 200,"Message" => "Otp verified success"));
        }
        return json_encode($result);
    }
    
    public function re_order(Request $data)
    {
        $post_data = $data->all();
        $query = 'SELECT oi.vendor_id,oi.outlet_id,oi.item_id AS product_id,oi.item_unit
        FROM orders o
        LEFT JOIN orders_info oi ON oi.order_id = o.id
        where o.id = ? AND o.customer_id= ? ORDER BY oi.id';
        //echo $post_data['user_id'].",".$post_data['order_id']."<br/>";
        $order_items = DB::select($query,array($post_data['order_id'],$post_data['user_id']));
        foreach($order_items as $order)
        {
            $re_order_data = array();
            $re_order_data['user_id'] = $post_data['user_id'];
            $re_order_data['vendors_id'] = $order->vendor_id;
            $re_order_data['outlet_id'] = $order->outlet_id;
            $re_order_data['product_id'] = $order->product_id;
            $re_order_data['qty'] = $order->item_unit;
            $cart_data = $this->reorder_add_cart($re_order_data);
        }
        $result = array("response" => array("httpCode" => 200,"Message" => trans("messages.Order has been added to your cart.")));
        return json_encode($result);
    }
    
    public function reorder_add_cart($data)
    {
        $post_data = $data;
        $ucdata = DB::table('cart')
                    ->select('cart.cart_id')
                    ->where("cart.user_id","=",$post_data['user_id'])
                    ->get();
        if(count($ucdata))
        {
                $uucdata = DB::table('cart')
                    ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                    ->select('cart.cart_id','cart_detail.product_id','cart_detail.quantity','cart_detail.cart_detail_id')
                    ->where("cart.user_id","=",$post_data['user_id'])
                    ->where("cart_detail.store_id","=",$post_data['vendors_id'])
                    ->where("cart_detail.outlet_id","=",$post_data['outlet_id'])
                    ->get();
                    if(count($uucdata))
                    {
                            $cdata = DB::table('cart')
                            ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                            ->select('cart.cart_id','cart_detail.product_id','cart_detail.quantity','cart_detail.cart_detail_id')
                            ->where("cart.user_id","=",$post_data['user_id'])
                            ->where("cart_detail.store_id","=",$post_data['vendors_id'])
                            ->where("cart_detail.outlet_id","=",$post_data['outlet_id'])
                            ->where("cart_detail.product_id","=",$post_data['product_id'])
                            ->get();
                            if(count($cdata))
                            {
                                $cart = Cart_model::find($cdata[0]->cart_id);
                                $cart->updated_at  = date("Y-m-d H:i:s");
                                $cart->save();
                                $cart_info = Cart_info::find($cdata[0]->cart_detail_id);
                                $quntiry = $cart_info->quantity + $post_data['qty'];
                                $cart_info->quantity  = $quntiry;
                                $cart_info->updated_at  = date("Y-m-d H:i:s");
                                $cart_info->save();
                                $cart_item = 0;
                                if($post_data['user_id'])
                                {
                                    $cdata = DB::table('cart')
                                            ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                                            ->select('cart_detail.cart_id',DB::raw('count(cart_detail.cart_detail_id) as cart_count'))
                                            ->where("cart.user_id","=",$post_data['user_id'])
                                            ->groupby('cart_detail.cart_id')
                                            ->get();
                                    if(count($cdata))
                                    {
                                        $cart_item = $cdata[0]->cart_count;
                                    }
                                }
                                return true;
                            }
                            else 
                            {
                                $ccdata = DB::table('cart')
                                ->leftJoin('cart_detail','cart_detail.cart_id', '=', 'cart.cart_id')
                                ->select('cart.cart_id')
                                ->where("cart.user_id","=",$post_data['user_id'])
                                ->where("cart_detail.store_id","=",$post_data['vendors_id'])
                                ->where("cart_detail.outlet_id","=",$post_data['outlet_id'])
                                ->get();
                                if(count($ccdata)){
                                    $cart = Cart_model::find($ccdata[0]->cart_id);
                                    $cart->updated_at  = date("Y-m-d H:i:s");
                                    $cart->save();
                                }        
                                else {        
                                    $cart     = new Cart_model;
                                    $cart->user_id          = $post_data['user_id'];
                                    //$cart->store_id          = $post_data['vendors_id'];
                                    //$cart->outlet_id          = $post_data['outlet_id'];
                                    $cart->cart_status          = 1;
                                    $cart->created_at  = date("Y-m-d H:i:s");
                                    $cart->updated_at  = date("Y-m-d H:i:s");
                                    $cart->save();
                                }
                                $cart_info   = new Cart_info;
                                $cart_info->cart_id          = $cart->cart_id;
                                $cart_info->product_id          = $post_data['product_id'];
                                $cart_info->quantity          = $post_data['qty'];
                                $cart_info->store_id          = $post_data['vendors_id'];
                                $cart_info->outlet_id          = $post_data['outlet_id'];
                                $cart_info->created_at  = date("Y-m-d H:i:s");
                                $cart_info->updated_at  = date("Y-m-d H:i:s");
                                $cart_info->save();
                                $cart_item = 0;
                                if($post_data['user_id'])
                                {
                                    $cdata = DB::table('cart')
                                            ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                                            ->select('cart_detail.cart_id',DB::raw('count(cart_detail.cart_detail_id) as cart_count'))
                                            ->where("cart.user_id","=",$post_data['user_id'])
                                            ->groupby('cart_detail.cart_id')
                                            ->get();
                                    if(count($cdata))
                                    {
                                        $cart_item = $cdata[0]->cart_count;
                                    }
                                }
                                //$result = array("response" => array("httpCode" => 200 , "Message" => "Cart has been added successfully!","type" => 1,"cart_count" => $cart_item));
                                return true;
                            }
                    }
                    else
                    {
                        $cart_item = 0;
                        if($post_data['user_id'])
                        {
                            $cdata = DB::table('cart')
                            ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                            ->select('cart_detail.cart_id',DB::raw('count(cart_detail.cart_detail_id) as cart_count'))
                            ->where("cart.user_id","=",$post_data['user_id'])
                            ->groupby('cart_detail.cart_id')
                            ->get();
                            if(count($cdata))
                            {
                                $cart_item = $cdata[0]->cart_count;
                            }
                        }
                        return false;
                    }
        }
        else 
        {
            $cart     = new Cart_model;
            $cart->user_id          = $post_data['user_id'];
            // $cart->store_id          = $post_data['vendors_id'];
            // $cart->outlet_id          = $post_data['outlet_id'];
            $cart->cart_status          = 1;
            $cart->created_at  = date("Y-m-d H:i:s");
            $cart->updated_at  = date("Y-m-d H:i:s");
            $cart->save();
            
            $cart_info   = new Cart_info;
            $cart_info->cart_id          = $cart->cart_id;
            $cart_info->product_id          = $post_data['product_id'];
            $cart_info->quantity          = $post_data['qty'];
            $cart_info->store_id          = $post_data['vendors_id'];
            $cart_info->outlet_id          = $post_data['outlet_id'];
            $cart_info->created_at  = date("Y-m-d H:i:s");
            $cart_info->updated_at  = date("Y-m-d H:i:s");
            $cart_info->save();
            $cart_item = 0;
            if($post_data['user_id']){
                $cdata = DB::table('cart')
                ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                ->select('cart_detail.cart_id',DB::raw('count(cart_detail.cart_detail_id) as cart_count'))
                ->where("cart.user_id","=",$post_data['user_id'])
                ->groupby('cart_detail.cart_id')
                ->get();
                if(count($cdata))
                {
                    $cart_item = $cdata[0]->cart_count;
                }
            }
            return true;
        }            
        return false;
    }
    
    public function return_order(Request $data)
    {
        $post_data = $data->all();
        $return_orders     = new return_orders;
        $return_orders->order_id          = $post_data['order_id'];
        $return_orders->return_reason          = $post_data['return_reason'];
        $return_orders->return_comments          = trim($post_data['comments']);
        $return_orders->return_action_id          = 1;
        $return_orders->return_status          = 1;
        $return_orders->created_by          = $post_data['user_id'];
        $return_orders->created_at  = date("Y-m-d H:i:s");
        $return_orders->modified_at  = date("Y-m-d H:i:s");
        $return_orders->modified_by  = $post_data['user_id'];
        $return_orders->save();

        //Get the order related data here and updated with log table
        $order_data = orders::find($post_data['order_id']);
        $Orders = new return_orders_log;
        // $Orders->vendor_id    = $order_data->vendor_id;
        // $Orders->outlet_id    = $order_data->outlet_id;
        $Orders->order_id    = $post_data['order_id'];
        $Orders->return_orders_id = $return_orders->id;
        $Orders->customer_id = $order_data->customer_id;
        $Orders->return_status = 1;
        $Orders->return_reason = $post_data['return_reason'];
        $Orders->return_action = 1;
        $Orders->order_status = 17;
        $Orders->modified_date = date("Y-m-d H:i:s");
        $Orders->created_date = date("Y-m-d H:i:s");
        $Orders->customer_notified = 1;
        $Orders->modified_by = $post_data['user_id'];
        $Orders->save();

        //Update return order status with orders table
        DB::update('update orders set order_status = ? where id = ?', array(17,$post_data['order_id']));
        $result = array("response" => array("httpCode" => 200,"Message" => "Order return initiated successfully"));

        /*Internal Admin Notifications Storing with notifications table
        * When order is returned by customer insert the notification logs for Admin & Vendor
        */
        $mess ="Your Order ".$order_data->order_key_formated." has been returned";
        $values = array('order_id' => $post_data['order_id'],
                    'customer_id' => $order_data->customer_id,
                    // 'vendor_id' => $order_data->vendor_id,
                    // 'outlet_id' => $order_data->outlet_id,
                    'message' => $mess,
                    'read_status' => 0,
                    'created_date' => date('Y-m-d H:i:s'));
        DB::table('notifications')->insert($values);

        //Send mail to admin regarding when order returned by customer here
        $template=DB::table('email_templates')
                ->select('*')
                ->where('template_id','=',self::RETURN_STATUS_CUSTOMER_EMAIL_TEMPLATE)
                ->get();
        if(count($template))
        {
            $from = $template[0]->from_email;
            $from_name = $template[0]->from;
            $subject = $template[0]->subject;
            if(!$template[0]->template_id)
            {
                $template = 'mail_template';
                $from = getAppConfigEmail()->contact_email;
                $subject = getAppConfig()->site_name." Return Order Status Information";
                $from_name = "";
            }
            $users = Users::find(1);
            $customers = Users::find($post_data['user_id']);
            $return_reason = return_reasons::find($post_data['return_reason']);
            $cont_replace = "Following user <b>".$customers->name."</b> was returned order <b>". $order_data->order_key_formated ."</b> with following store or outlet <b>".$post_data['vendor_name']."</b>";
            $cont_replace1 = "Kindly find the reason for returning the order <b>". $return_reason->name ."</b> and make it necessary arrangements for the order.Kindly find the below comments from a customer: <br/><b>".$post_data['comments']."</b>";
            $content = array("name" => $users->name,"email"=>$users->email,"replacement"=>$cont_replace,"replacement1"=>$cont_replace1);
            $email = smtp($from,$from_name,$users->email,$subject,$content,$template);
        }
        return json_encode($result);
    }
    
    public function paypal_payment(Request $data)
    {
        
        $post_data = $data->all();
        $payment_array =  json_decode($post_data['payment_array']);
        $payment_arrays =  json_decode($post_data['payment_array'],true);
        //print_r($payment_array);exit;
        $rules = array();
        if($payment_array->order_type == 1)
        {
            $rules['delivery_address'] = 'required';
        }
        $validation = app('validator')->make($payment_arrays,$rules);
        // process the validation
        if ($validation->fails()) 
        {
            foreach( $validation->errors()->messages() as $key => $value) 
            {
                $errors[] = is_array($value)?implode( ',',$value ):$value;
            }
            $errors = implode( ", \n ", $errors );
            $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => $errors, "Error" => trans("messages.Error List")));
        }
        else
        {
            $total_amt = $payment_array->total;
            if($payment_array->coupon_id != 0)
            {
                $coupon_details = DB::table('coupons')
                                ->select('*','coupons.id as coupons_id')
                                ->leftJoin('coupon_outlet','coupon_outlet.coupon_id','=','coupons.id')
                                ->where('coupons.id','=',$payment_array->coupon_id)
                                ->first();
                if(count($coupon_details) == 0)
                {
                    $result = array("response" => array("httpCode" => 400,"Message" => "No coupons found"));
                    return json_encode($result);
                }
                $coupon_user_limit_details = DB::table('user_cart_limit')
                        ->select('*')
                        ->where('user_id','=',$post_data['user_id'])
                        ->where('coupon_id','=',$payment_array->coupon_id)
                        ->first();
                if(count($coupon_user_limit_details)>0)
                {
                    if($coupon_user_limit_details->cus_order_count >= $coupon_user_limit_details->user_limit)
                    {
                        $result = array("response" => array("httpCode" => 400,"Message" => "Max order limit has been crossed", "order_items"=>$order_items,"delivery_details"=>$delivery_details));
                        return json_encode($result);
                    }
                    if($coupon_user_limit_details->total_order_count >= $coupon_user_limit_details->coupon_limit)
                    {
                        $result = array("response" => array("httpCode" => 400,"Message" => "Max order limit has been crossed", "order_items"=>$order_items,"delivery_details"=>$delivery_details));
                        return json_encode($result);
                    }
                }
            }
        }
        
        $paypal=Paypal::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);
        // process the validation
             //Save Details
            try    {

                $payer = PayPal    ::Payer();
                $payer->setPaymentMethod('paypal');
                //$details = new Details();
                //$details->setShipping($shipping)->setTax($_POST['service_tax']);
                $amount = PayPal:: Amount();
                $amount->setCurrency('USD');
                $amount->setTotal($total_amt);//->setDetails($details);
                //$item1 = new Item();
                //$item1->setName($data['doctor_first_name'])->setCurrency('USD')->setQuantity(1)->setPrice($pay);
                
                
                
                //print_r($payment_array->items);exit;
                $items = array();
                $index = 0;
                foreach ($payment_array as $_item) {
                   $index++;
                   $items[$index] = new Item();
                   $items[$index]->setName($_item['name_key'])
                                 ->setCurrency($_item['currency_key'])
                                 ->setQuantity($_item['quantity_key'])
                                 ->setPrice($_item['price_key']);
                }
                
                
                
                
                
                $itemList = new ItemList(); $itemList->setItems(array($item1));
                $transaction = PayPal::Transaction();
                $transaction->setAmount($amount);
                $info ='Book Appointment - '.$data['doctor_first_name'].' Pay on $ '.$pay;
                $transaction->setDescription($info)->setItemList($itemList);
                if(isset($_POST['location_visit_status']) && $_POST['location_visit_status']!=''){
                    //$transaction->setDescription('Visit Patient Location - '.$_POST['consulting_fees']);
                }
                $redirectUrls = PayPal:: RedirectUrls();
                $redirectUrls->setReturnUrl(route('getDone'));
                $redirectUrls->setCancelUrl(route('getCancel'));
                $payment = PayPal::Payment();
                $payment->setIntent('sale');
                $payment->setPayer($payer);
                $payment->setRedirectUrls($redirectUrls);
                $payment->setTransactions(array($transaction));
                $response = $payment->create($this->_apiContext);
                
                $redirectUrl = $response->links[1]->href;
                return redirect()->to($redirectUrl);
            }
            catch(Exception $ex) {
                 ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex); exit(1);
            }
            Session::flash('message', 'Error: Oops. Something went wrong. Please try again later.'); 
            return Redirect::to('/');
    }
    public function convert_currency(Request $data)
    {
        $post = $data->all();
        $from_currency = $data->from_currency;
        $to_currency = getCurrencycode();
        $amount = urlencode($total_amount);
        $from_currency = urlencode($from_currency);
        $to_currency = urlencode($to_currency);
        $get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=$from_currency&to=$to_currency");
        $get = explode("<span class=bld>",$get);
        $converted_amount = $total_amount;
        if(isset($get[1]))
        {
            $get = explode("</span>",$get[1]);
            if(isset($get[0]))
            {
                $converted_amount = preg_replace("/[^0-9\.]/", null, $get[0]);
            }
        }
    }

    public function cancel_booking(Request $data)
    {
        $post_data = $data->all();

        //  return $post_data;

        $order_id = $post_data['order_id'];

        $user_id = $post_data['user_id'];

        //  return $user_id;      
       

        $booking_detail = booking_detail::find($order_id);

         //refund amount calculation 

         if($booking_detail->payment_type == 0 || $booking_detail->payment_type == 1){

         }  else if($booking_detail->payment_type == 2) {
            $refund_amount = $booking_detail->payments;
            $vendor_id = $booking_detail->vendor_id;
            $vendor_details = DB::table('vendors')->where('id',$vendor_id)->select('refund_amount_deduction')
                             ->first();
            if(count($vendor_details)>0){
                $ref_percent = $vendor_details->refund_amount_deduction;
                $amount_reduction = ($ref_percent * $refund_amount) / 100;
                $total_refund_amount = $refund_amount - $amount_reduction;
            }                             
            //Payment Gateway to refund amount

            if($payment_status == 1){
                //change the refund status in the booking and 
            }
         } 
         


        $booking_detail->booking_status = 4;   //  CANCELLED
        $booking_detail->order_cancelled_by = $user_id; 
        $booking_detail->order_cancelled_user_type = 4; 

        $booking_detail->save();

        $brd = DB::table('booked_room')
                ->where('booking_id','=',$order_id)
                ->update(['booking_status' => 4, 'status' => 2]);

        //  return $room;               

        $booking_details = DB::table('booking_details')
                            ->select('booking_details.created_date as bc_date',
                                'booking_details.charges as bc_charges','admin_customers.id as cust_id',
                                'booking_details.id as bid','booking_status.name as status_name',
                                'booking_details.*','admin_customers.*','booking_status.*','booking_info.*')
                            ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                            ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                            ->leftJoin('rooms','rooms.id','booking_info.room_id')
                            ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                            ->where('booking_details.id','=',$order_id)
                            ->get();

        //  return $booking_details;                  

        $outlet_id = (count($booking_details) > 0)? $booking_details[0]->outlet_id:'';

        //  return $outlet_id;
        $vendor_id = (count($booking_details) > 0)? $booking_details[0]->vendor_id:'';

        //  return $vendor_id;

        $outlets = DB::table('outlets')
                    ->select('outlets.*','outlet_infos.*')
                    ->leftJoin('outlet_infos','outlet_infos.id','outlets.id')
                    ->where('outlets.id','=',$outlet_id)
                    ->get();

        $vendors = DB::table('vendors_view')
                    ->select('vendors_view.*')
                    ->where('vendors_view.id','=',$vendor_id)
                    ->get();

        $rooms_count = DB::table('booking_details')
                    ->select(DB::RAW('count(booking_info.id) as rooms_count'))
                    ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                    ->where('booking_details.id','=',$order_id)
                    ->get();

        //  CANCEL MAIL TO USER

        $users = Admincustomer::find($user_id);
            $to = $users->email;
/*
            $subject = 'Your Order with '.getAppConfig()->site_name.' ['.$booking_details[0]->booking_random_id .'] has been successfully '.$vendor_info[0]->status_name.'!';
*/
            $subject = 'Your Order with '.getAppConfig()->site_name.' ['.$booking_details[0]->booking_random_id .'] has been successfully '.$booking_details[0]->status_name.'!';

            $template=DB::table('email_templates')
                        ->select('*')
                        ->where('template_id','=',self::ORDER_STATUS_UPDATE_USER)
                        ->get();
            if(count($template))
            {
                $from = $template[0]->from_email;
                $from_name=$template[0]->from;
                if(!$template[0]->template_id)
                {
                    $template = 'mail_template';
                    $from=getAppConfigEmail()->contact_mail;
                }
                $orders_link ='<a href="'.URL::to("orders").'" title="'.trans("messages.View").'">'.trans("messages.View").'</a>';
                $content =array('name' =>"".$users->firstname,'order_key'=>"".$booking_details[0]->booking_random_id,'status_name'=>"".$booking_details[0]->status_name,'orders_link'=>"".$orders_link);
                $attachment = "";
                $email=smtp($from,$from_name,$to,$subject,$content,$template,$attachment);
            }                      

        if(count($outlets) > 0)  {
            $result = array("response" => array("httpCode" => 200, "status" => true,
                            "properties" => $outlets, "booking_details" => $booking_details,
                            "rooms_count" => $rooms_count, "outlet_id" => $outlet_id,
                            "vendors" => $vendors,
                            "Message" => "Booking Details"));
        } else {
            $result = array("response" => array("httpCode" => 400, "status" => false,
                            "properties" => $outlets, "booking_details" => $booking_details,
                            "rooms_count" => $rooms_count, "outlet_id" => $outlet_id,
                            "vendors" => $vendors,
                            "Message" =>  "No Booking Found."));
        }            

        return json_encode($result);

    }

   
    
    public function cancel_order(Request $data)
    {
        $post_data = $data->all();
        $orders=DB::table('transaction')
             ->select('*')
             ->where('order_id','=',$post_data['order_id'])
             ->where('payment_status','=','SUCCESS')
             ->get();
        if(!count($orders))
        {
            $result = array("response"=>array("httpCode" => 400,"Message" => "No orders found","order_items"=>array()));
            return json_encode($result);
        }
        if($orders[0]->payment_type == "paypal")
        {
            $transaction_id = $orders[0]->transaction_id;
            $capture_get = Capture::get($transaction_id, $this->_apiContext);
            //print_r($capture_get);exit;
            //$capture_get = Capture::get('8GK68812KY030334A', $this->_apiContext);    
            if($capture_get->getState()=="completed")
            {
                $refund = new Refund();
                $amt = new Amount();
                $amt->setCurrency("USD")
                ->setTotal($capture_get->getAmount()->getTotal());
                $refund->setAmount($amt);
                $refundstatus=false;
                 
                //$refund->setId($transaction_id);
                try{  
					   //print_r($refund);exit;
                        $captureRefund = $capture_get->refund($refund, $this->_apiContext);
                         print_r($captureRefund);exit;
                        $Refundget=Refund::get($captureRefund->getId(), $this->_apiContext);
                        //print_r($Refundget->getId());exit;
                        $refund_id = $Refundget->getId();
                        $refund_status = $Refundget->getState();
                        if($Refundget->getId()){
                            $transaction = Transaction::find($orders[0]->id);
                            //print_r($transaction);exit;
                            $transaction->refund_id = $refund_id;
                            $transaction->refund_status = $refund_status;
                            $transaction->captured = 4;
                            $transaction->refund_updated_date = date("Y-m-d H:i:s");
                            $transaction->save();
                            $refundstatus= true;
                        }
                }
                catch (PayPal\Exception\PayPalConnectionException $ex) {
                        echo $ex->getCode(); // Prints the Error Code
                        echo $ex->getData(); // Prints the detailed error message
                        //die($ex);
                        exit;
                }
                catch (Exception $ex) {
                        echo $ex->getMessage();
                        die($ex);
                        echo "Problem in order refund process"; exit;
                }
                
                if($refundstatus)
                {
                    $affected = DB::update('update orders set order_status = ? where id = ? AND order_status = ?', array(11,$post_data['order_id'],1));
                    $result = array("response" => array("httpCode" => 200,"Message" => trans("messages.Order has been cancelled ")));
                    $order_detail = $this->get_order_details($post_data['order_id']);
                    $order_details = $order_detail["order_items"];
                    $delivery_details = $order_detail["delivery_details"];
                    $vendor_info = $order_detail["vendor_info"];
                    $logo = url('/assets/front/'.Session::get("general")->theme.'/images/'.Session::get("general")->theme.'.png');
                    if(file_exists(base_path().'/public/assets/admin/base/images/vendors/list/'.$vendor_info[0]->logo_image)) { 
                        $vendor_image ='<img width="100px" height="100px" src="'.URL::to("assets/admin/base/images/vendors/list/".$vendor_info[0]->logo_image).'") >';
                    }
                    else
                    {  
                        $vendor_image ='<img width="100px" height="100px" src="'.URL::to("assets/front/".Session::get('general')->theme."/images/blog_no_images.png").'") >';
                    }
                    $delivery_date = date("d F, l", strtotime($delivery_details->delivery_date)); 
                    $delivery_time = date('g:i a', strtotime($delivery_details->start_time)).'-'.date('g:i a', strtotime($delivery_details->end_time));
                    $users=Users::find($delivery_details->customer_id); 
                    $to=$users->email;
                    $subject = 'Your Order with '.getAppConfig()->site_name.' ['.$vendor_info[0]->order_key_formated .'] has been successfully '.$vendor_info[0]->status_name.'!';
                    $template=DB::table('email_templates')
                    ->select('*')
                    ->where('template_id','=',self::ORDER_STATUS_UPDATE_USER)
                    ->get();
                    if(count($template))
                    {
                        $from = $template[0]->from_email;
                        $from_name=$template[0]->from;
                        if(!$template[0]->template_id)
                        {
                            $template = 'mail_template';
                            $from=getAppConfigEmail()->contact_mail;
                        }
                        $orders_link ='<a href="'.URL::to("orders").'" title="'.trans("messages.View").'">'.trans("messages.View").'</a>';
                        $content =array('name' =>"".$users->name,'order_key'=>"".$vendor_info[0]->order_key_formated,'status_name'=>"".$vendor_info[0]->status_name,'orders_link'=>"".$orders_link);
                        $attachment = "";
                        $email=smtp($from,$from_name,$to,$subject,$content,$template,$attachment);
                    }
                }
                else
                {
                    $result = array("response"=>array("httpCode" => 400,"Message" => "No orders found","order_items"=>array()));
                }
            }
            else
            {
                $result = array("response"=>array("httpCode" => 400,"Message" => "No orders found","order_items"=>array()));
            }
        }
        else
        {
            $affected = DB::update('update orders set order_status = ? where id = ? AND order_status = ?', array(11,$post_data['order_id'],1));
            $order_detail = $this->get_order_details($post_data['order_id']);
            $order_details = $order_detail["order_items"];
            $delivery_details = $order_detail["delivery_details"];
            $order_info = $order_detail["order_info"];
            $logo = url('/assets/front/'.Session::get("general")->theme.'/images/'.Session::get("general")->theme.'.png');
            // if(file_exists(base_path().'/public/assets/admin/base/images/vendors/list/'.$vendor_info[0]->logo_image)) { 
            //     $vendor_image ='<img width="100px" height="100px" src="'.URL::to("assets/admin/base/images/vendors/list/".$vendor_info[0]->logo_image).'") >';
            // }
            // else
            // {  
            //     $vendor_image ='<img width="100px" height="100px" src="'.URL::to("assets/front/".Session::get('general')->theme."/images/blog_no_images.png").'") >';
            // }
            $delivery_date = date("d F, l", strtotime($delivery_details->delivery_date)); 
            $delivery_time = date('g:i a', strtotime($delivery_details->start_time)).'-'.date('g:i a', strtotime($delivery_details->end_time));
            $users=Users::find($order_info->customer_id); 
            $to=$users->email;
            $subject = 'Your Order with '.getAppConfig()->site_name.' ['.$vendor_info[0]->order_key_formated .'] has been successfully '.$vendor_info[0]->status_name.'!';
            $template=DB::table('email_templates')
            ->select('*')
            ->where('template_id','=',self::ORDER_STATUS_UPDATE_USER)
            ->get();
            if(count($template))
            {
                $from = $template[0]->from_email;
                $from_name=$template[0]->from;
                if(!$template[0]->template_id)
                {
                    $template = 'mail_template';
                    $from=getAppConfigEmail()->contact_mail;
                }
                $orders_link ='<a href="'.URL::to("orders").'" title="'.trans("messages.View").'">'.trans("messages.View").'</a>';
                $content =array('name' =>"".$users->name,'order_key'=>"".$vendor_info[0]->order_key_formated,'status_name'=>"".$vendor_info[0]->status_name,'orders_link'=>"".$orders_link);
                $attachment = "";
                $email=smtp($from,$from_name,$to,$subject,$content,$template,$attachment);
            }            
            $result = array("response" => array("httpCode" => 200,"Message" => trans("messages.Order has been cancelled")));
        }
        return json_encode($result);
    }
    
    public function get_order_details($order_id)
    {
        $language_id = getCurrentLang();
        $query3 = '"vendors_infos"."lang_id" = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = '.$language_id.' and vendors.id = vendors_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $query4 = '"payment_gateways_info"."language_id" = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $vendor_info = DB::select('SELECT vendors_infos.vendor_name,vendors.email,vendors.logo_image,o.id as order_id,o.created_date,o.order_status,order_status.name as status_name,order_status.color_code as color_code,payment_gateways_info.name as payment_gateway_name,oinf.outlet_id,vendors.id as vendor_id,o.order_key_formated
        FROM orders o
        left join orders_info oinf on o.id = oinf.order_id
        left join vendors vendors on vendors.id = oinf.vendor_id
        left join vendors_infos vendors_infos on vendors_infos.id = vendors.id
        left join order_status order_status on order_status.id = o.order_status
        left join payment_gateways payment_gateways on payment_gateways.id = o.payment_gateway_id
        left join payment_gateways_info payment_gateways_info on payment_gateways_info.payment_id = payment_gateways.id
        where '.$query3.' AND '.$query4.' AND o.id = ? ORDER BY o.id',array($order_id));

        $query = 'pi.lang_id = (case when (select count(*) as totalcount from products_infos where products_infos.lang_id = '.$language_id.' and p.id = products_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $order_items = DB::select('SELECT p.product_image,p.id AS product_id,oi.item_cost,oi.item_unit,oi.item_offer,o.total_amount,o.delivery_charge,o.service_tax,o.invoice_id,pi.product_name,pi.description,o.coupon_amount
        FROM orders o
        LEFT JOIN orders_info oi ON oi.order_id = o.id
        LEFT JOIN products p ON p.id = oi.item_id
        LEFT JOIN products_infos pi ON pi.id = p.id
        where '.$query.' AND o.id = ? ORDER BY oi.id',array($order_id));
        $query5 = 'out_infos.language_id = (case when (select count(*) as totalcount from outlet_infos where outlet_infos.language_id = '.$language_id.' and out.id = outlet_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $query2 = 'pgi.language_id = (case when (select count(*) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and pg.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $delivery_details = DB::select('SELECT o.delivery_instructions,ua.address,pg.id as payment_gateway_id,pgi.name,o.total_amount,o.delivery_charge,o.service_tax,dti.start_time,end_time,o.created_date,o.delivery_date,o.order_type,out_infos.contact_address,o.coupon_amount,o.customer_id FROM orders o
            left join orders_info oinf on o.id = oinf.order_id
                    LEFT JOIN user_address ua ON ua.id = o.delivery_address
                    left join payment_gateways pg on pg.id = o.payment_gateway_id
                    left join payment_gateways_info pgi on pgi.payment_id = pg.id
                    left join delivery_time_slots dts on dts.id=o.delivery_slot
                    left join delivery_time_interval dti on dti.id = dts.time_interval_id
                    left join outlets out on out.id = oinf.outlet_id
                   left join outlet_infos out_infos on out_infos.id = out.id
                    where '.$query2.' AND '.$query5.'AND o.id = ?',array($order_id));
        if(count($order_items)>0 && count($delivery_details)>0 && count($vendor_info)>0)
        {
            $result = array("order_items"=>$order_items,"delivery_details"=>$delivery_details,"vendor_info"=>$vendor_info);
        }
        return $result;
    }
    public function delete_sms(Request $data)
    {
        $twilo_sid    = "AC648e25ad93bdcbb9788baa76f1735e3c";
        $twilio_token = "cca34a4a0811d1d4161470812348bf5c";
        $from_number  = "+14055627423";
        $client = new Services_Twilio($twilo_sid, $twilio_token);
        $call = $client->account->messages->getIterator(0, 50, array('DateCreated>' => '2017-01-10 08:00:00', 'DateCreated<' => '2016-12-01'));
        foreach($call as $c)
        {
            //~ $client->messages($c)->delete();
            //~ echo '<pre>';
            print_r($c);die;
        }echo 2;die;
    }


    public function order_driver_location(Request $data)
    {
        $post_data = $data->all();
        $rules = [
            'user_id'  => ['required'],
            'token'    => ['required'],
            'language' => ['required'],
            'order_id' => ['required'],
        ];
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails()) 
        {
            $j = 0;
            foreach( $validator->errors()->messages() as $key => $value) 
            {
                $errors[] = is_array($value)?implode( ',',$value ):$value;
            }
            $errors = implode( ", \n ", $errors );
            $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => $errors));
        }
        else 
        {
            try {
                if($post_data['language']==2)
                {
                    App::setLocale('ar');
                }
                else 
                {
                    App::setLocale('en');
                }
                $check_auth = JWTAuth::toUser($post_data['token']);
                $language   = $post_data['language'];
                $driver_details = Order::get_driver_current_location($post_data['order_id']);
                if(count($driver_details)>0)
                {

                    $imageName = url('/assets/admin/base/images/default_avatar_male.jpg');
                    if(file_exists(base_path().'/public/assets/admin/base/images/drivers/'.$driver_details->driver_profile_image)&& $driver_details->driver_profile_image!='' )
                    { 
                        $imageName =URL::to("/assets/admin/base/images/drivers/".$driver_details->driver_profile_image.'?'.time());
                    }
                    $driver_details->driver_profile_image = $imageName;
                    $driver_details->delivery_time = $driver_details->start_time.'-'.$driver_details->end_time;
					$language_id=$post_data['language'];
                    $query = 'pi.lang_id = (case when (select count(*) as totalcount from products_infos where products_infos.lang_id = '.$language_id.' and p.id = products_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
					$order_items = DB::select('SELECT p.product_image,p.id AS product_id,oi.item_cost,oi.item_unit,oi.item_offer,o.total_amount,o.delivery_charge,o.service_tax,o.invoice_id,pi.product_name,pi.description,o.coupon_amount
					FROM orders o
					LEFT JOIN orders_info oi ON oi.order_id = o.id
					LEFT JOIN products p ON p.id = oi.item_id
					LEFT JOIN products_infos pi ON pi.id = p.id
					where '.$query.' AND o.id = ? ORDER BY oi.id',array($driver_details->order_id));
					$driver_details->order_items = count($order_items);
                    $result = array("response" => array("httpCode" => 200, "status" => true, "Message" => trans("messages.Driver details"), "driver_details" => $driver_details));
                }
                else {
                    $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.No driver assigned to this order")));
                }
            }
            catch(JWTException $e) { 
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            }
            catch(TokenExpiredException $e) { 
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            }
        }
        return json_encode($result);
    }

    //  PAY AT HOTEL

    public function verify_guest_otp(Request $data)
    {
            $post_data = $data->all();
      
            //  return $post_data;
            // To get otp number
            $current_otp = $post_data['otp_number'];
            $otp_id = $post_data['verify_otp_id'];

            if(isset($post_data['new_user_id']))
            {
                  //    return 1;

                $user_id = $post_data['new_user_id'];
            }
            else
            {
                $user_id = $post_data['user_id'];
            }

           //    return $user_id;
            
            $outlet_id = $post_data['outlet_id'];
            $room_type_id = $post_data['room_type_id'];
            $check_in = $post_data['check_in'];
            $check_out = $post_data['check_out'];
            $room_count = $post_data['room_count'];
            $guest_count = $post_data['guest_count'];
            $no_of_nights = $post_data['no_of_nights'];
            $coupon_id = $post_data['coupon_id'];
            $coupon_amount = $post_data['coupon_amount'];
            $coupon_type = $post_data['coupon_type'];
            $original_price = $post_data['original_price'];
            $total_cost = $post_data['total_cost'];
            $vendor_id = $post_data['vendor_id'];

            //  GETTING OUTLET URL INDEX

            $property_details = DB::table('outlets')
                                ->select('outlets.url_index')
                                //->leftJoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                ->where('outlets.id','=',$outlet_id)
                                ->get();

            $outlet_url_index = $property_details[0]->url_index;

            //  return $outlet_url_index;

            if($otp_id > 0){
                $user = Admincustomer::find($user_id);

                //  return $user_id;

                //  $temp_order_id = 132;

                $room_type_cost = DB::table('room_type')->where('id','=',$room_type_id)
                                                        ->select('discount_price','normal_price')
                                                        ->get();

                //  return $room_type_cost;

                $rtr = rooms::where('room_type_id',$room_type_id)
                            ->pluck('id')->toArray();

                 // return $rtr;

                $rtrs = rooms::where('room_type_id',$room_type_id)
                            ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                            ->whereBetween('booked_room.date',array($check_in,$check_out))
                            ->where('booked_room.status','=',1)
                            ->pluck('rooms.id')
                            ->toArray();

                //  return $rtrs;
                $rt=array_diff($rtr,$rtrs);

                $free_rooms = rooms::whereIN('id',$rt)
                            ->pluck('id')->toArray();                

                 // return $free_rooms;

                $free_rooms_count = count($free_rooms);

                //  return $free_rooms_count;

                 // return $room_count;

                if($room_count <= $free_rooms_count)
                {
                    $free = 1;
                }
                else
                {
                    $free = 2;
                }

                $free_rooms = rooms::whereIN('id',$rt)
                            ->take($room_count)
                            ->pluck('id')->toArray();

                //  return $free_rooms;

                //  TOTAL ROOMS ID ARE STORING IN BOOKING DETAILS , FURTHER WE CAN EXPLODE & USE THAT

                $roo = implode(',', $free_rooms);

                //  return array($roo);

                $free_room_name = rooms_infos::whereIN('id',$free_rooms)->pluck('room_name')->toArray();

                //  return $free_room_name;  
                
                $roo_nm = implode(',', $free_room_name);

                //  return array($roo_nm);                              

                //  return $free_rooms;
                if($user && $free_rooms && $free == 1){
                    $old_otp = $user->otp_number;  
                    if($current_otp == $old_otp){

                        //  USER BOOKING

                            $cout_date = date('Y-m-d',strtotime($check_out. ' - 1 day'));

                            $cin = strtotime($check_in);

                            $cout =strtotime($cout_date);

                            //  STORE BOOKING 

                            $booking_detail = new booking_detail;

                            $booking_detail->booking_status =  5;   //  Initiated
                            $booking_detail->booking_source = 2;
                            $booking_detail->adult_count = $guest_count;
                            //  $booking_detail->child_count = Input::get('child_count');
                            $booking_detail->check_in_date = $check_in;
                            $booking_detail->check_out_date = $check_out;
                            //  $booking_detail->check_out_date = $cout_date;
                            $booking_detail->no_of_days = $no_of_nights;
                            $booking_detail->room_type = $room_type_id;
                            $booking_detail->rooms = $roo;
                            $booking_detail->room_name = $roo_nm;
                            //  $booking_detail->notes = Input::get('notes');
                            $booking_detail->booking_random_id = getRandomBookingNumber(6);
                            $booking_detail->created_date = date('Y-m-d H:i:s');
                            $booking_detail->room_booked_date = date('Y-m-d');
                            $booking_detail->charges = $total_cost;
                            $booking_detail->original_price = $original_price;
                            
                            $booking_detail->coupon_id = $coupon_id;
                            $booking_detail->coupon_amount = $coupon_amount;
                            $booking_detail->coupon_type = $coupon_type;
                            $booking_detail->payments = 0;
                            $booking_detail->outlet_id = $outlet_id;
                            $booking_detail->vendor_id = $vendor_id;
                            $booking_detail->customer_id = $user_id;
                            $booking_detail->booking_type = 1;  //  PAY AT HOTEL
                            $booking_detail->room_type_cost = $room_type_cost[0]->discount_price;

                            $booking_detail->save();              

                            //   STORE BOOKED ROOM IN BOOKING_INFO

                            foreach ($free_rooms as $key => $value) {
                                $infos = new booking_infos;

                                $infos->room_id = $value;
                                $infos->booking_id = $booking_detail->id;

                                $infos->save();

                            //  CHANGE STATUS OF ROOM

                                $users = DB::table('rooms')
                                                    ->where('rooms.id',$value)
                                                    ->update([
                                                        'room_clean_status' => 0,
                                                        'availability_status' => 1,
                                                        'check_in_date' => $check_in,
                                                        'check_out_date' => $check_out,
                                                        'customer_id' => $user_id,
                                                    ]);  

                            //  STORE BOOKED ROOM DETAILS                           

                            for ($seconds=$cin; $seconds<=$cout; $seconds+=86400)
                                {
                                    $brd = new booked_room_details;

                                    $brd->check_in_date  = $check_in;
                                    //  $brd->check_out_date = Input::get('check_out');
                                    $brd->check_out_date = $check_out;
                                    $brd->no_of_days     = $no_of_nights;
                                    $brd->room_id        = $value;
                                    $brd->booking_id     = $booking_detail->id;
                                    $brd->date = date("Y-m-d", $seconds);
                                    $brd->availablity_status = 1;
                                    $brd->booking_status = 5;   //  Initiated
                                    $brd->customer_id = $user_id;

                                    $brd->save();
                                }
                            }
                           
                           //      STORE ROOM CHARGES

                            $charges = new booking_charge;

                            //  $charges->room_id = $rid;  // $rt->id;
                            $charges->room_type = $room_type_id;
                            $charges->price = $total_cost;
                            $charges->charge_type = 1;
                            $charges->booking_id = $booking_detail->id;
                            $charges->customer_id = $user_id;
                            $charges->created_date = date('Y-m-d H:i:s');
                            //  $charges->notes = Input::get('notes');
                            $charges->active_status = 1;
                            $charges->property_id = $outlet_id;
                            $charges->save();


                            $booking_id = $booking_detail->id;

                            $booking_id_encrypted = encrypt($booking_id);

                            $this->send_order_email($booking_id,$user_id,1);

                
                         $result = array("response" => array("httpCode" => 200, "last_id" => $otp_id,
                            "status" => true, "booking_id" => $booking_id_encrypted,
                        "Message" => "The OTP Verification is Success."));
                    } else {
                         $result = array("response" => array("httpCode" => 400, "status" => false, 
                        "Message" => "The otp you have entered is not matched with our records."));
                    }
                } else {
                     $result = array("response" => array("httpCode" => 600, "status" => false, 
                "outlet_url_index" => $outlet_url_index, "Message" => "Rooms Not Available."));
                }
            } else {
                 $result = array("response" => array("httpCode" => 400, "status" => false, 
                "Message" => "Rooms Not Available & We don't have any matching records for your request."));
            }
        return json_encode($result);
    }

}
