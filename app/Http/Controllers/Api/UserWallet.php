<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Contracts\ArrayableInterface;
use Illuminate\Support\Facades\Text;
use Illuminate\Support\Facades\Redirect;
use JWTAuth;
use DB;
use App;
use Image;
use URL;
use Hash;
use Session;
use Twilio;
use Services_Twilio;
use PDF;
use PushNotification;
use App\Model\admin_customers as Admin_customer;
use App\Model\user_wallets;
use App\Model\user_wallets_logs;

class UserWallet extends Controller
{
    const WALLET_MAIL_TEMPLATE = 26;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    /* user wallet list*/
    public function user_wallet(Request $data)
    {
		$post_data = $data->all();

		//	return $post_data;
		// if ( isset($post_data['language']) && $post_data['language'] == 2)
		// {
		// 	App::setLocale('ar');
		// }
		// else {
		// 	App::setLocale('en');
		// }
        $rules = [
            'user_id'  => ['required'],
            'token'    => ['required'],
            'language' => ['required'],
        ];
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails())
        {
            $j = 0;
            foreach ($validator->errors()->messages() as $key => $value)
            {
                $errors[] = is_array($value) ? trans('messages.'.implode(',', $value)) : $value;
            }
            $errors = implode(", \n ", $errors);
            $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => $errors));
        } else {
			try {
                //	$check_auth  = JWTAuth::toUser($post_data['token']);
                $users       = Admin_customer::find($post_data['user_id']);

                //	return $users;
                if(count($users) > 0)
					$user_wallet_amount = !empty($users->wallet_amount)?(int)$users->wallet_amount:0;
				$user_credited_wallet = user_wallets::get_user_credited_wallet_list($post_data['user_id']);
				$user_debited_wallet  = user_wallets::get_user_debitted_wallet_list($post_data['user_id']);
				$result = array("response" => array("httpCode" => 200, "Message" => trans("messages.User wallet credit and debit list"), "credit_wallet_list" => $user_credited_wallet, "debit_wallet_list" => $user_debited_wallet, "user_wallet_amount" => $user_wallet_amount ));
			} catch (JWTException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            } catch (TokenExpiredException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            }
		}
		return json_encode($result,JSON_UNESCAPED_UNICODE);
    }
    /* To add the wallet */
    public function credit_wallet_amount(Request $data)
    {
		$post_data = $data->all();
		if ( isset($post_data['language']) && $post_data['language'] == 2)
		{
			App::setLocale('ar');
		}
		else {
			App::setLocale('en');
		}
        $rules = [
            'user_id'  => ['required'],
            'token'    => ['required'],
            'language' => ['required'],
            'payment_array' => ['required'],
        ];
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails())
        {
            $j = 0;
            foreach ($validator->errors()->messages() as $key => $value)
            {
                $errors[] = is_array($value) ? trans('messages.'.implode(',', $value)) : $value;
            }
            $errors = implode(", \n ", $errors);
            $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => $errors));
        } else {
			try {
				$payment_array = json_decode($post_data['payment_array']);
                $check_auth    = JWTAuth::toUser($post_data['token']);
                $wallet_amount = $payment_array->amount/100;
                /* To add the user wallet table */
                $user_wallets = new user_wallets;
				$user_wallets->customer_id     = $post_data['user_id'];
				$user_wallets->payment_status  = $payment_array->response_message;
				$user_wallets->payment_type    = $payment_array->payment_method;
				$user_wallets->payer_id        = $payment_array->authorization_code;
				$user_wallets->transaction_id  = $payment_array->fort_id;
				$user_wallets->recipt_id       = $payment_array->merchant_reference;
				$user_wallets->reason_code     = $payment_array->response_code;
				$user_wallets->paypal_email    = $payment_array->customer_email;
				$user_wallets->captured        = $payment_array->status;
				$user_wallets->total_amount    = $wallet_amount;
				$user_wallets->signature       = $payment_array->signature;
				$user_wallets->access_code     = $payment_array->access_code;
				$user_wallets->language        = $payment_array->language;
				$user_wallets->merchant_identifier      = $payment_array->merchant_identifier;
				$user_wallets->authorization_links      = $payment_array->card_number;
				$user_wallets->payment_transaction_fees = $payment_array->expiry_date;
				$user_wallets->transaction_type         = $payment_array->payment_option;
				$user_wallets->captured_valid_until     = $payment_array->customer_ip;
				$user_wallets->captured_pending_reason  = $payment_array->customer_ip;
				$user_wallets->captured_valid_until     = $payment_array->customer_ip;
				$user_wallets->captured_pending_reason  = $payment_array->language;
				$user_wallets->payment_mode             = $payment_array->eci;
				$user_wallets->captured_payment_type    = $payment_array->command;
				$user_wallets->captured_correlation_id  = $payment_array->sdk_token;
				$user_wallets->captured_transaction_id  = $payment_array->merchant_reference;
				$user_wallets->transaction_token        = $payment_array->token_name;
				$user_wallets->currency_code            = $payment_array->currency;
				$user_wallets->created_date             = date("Y-m-d H:i:s");
				$user_wallets->payfort_serialize        = serialize($post_data['payment_array']);
				$user_wallets->save();
				/* To add the user wallet logs table */
				$user_wallets_logs = new user_wallets_logs;
				$user_wallets_logs->customer_id   = $post_data['user_id'];
				$user_wallets_logs->wallet_id     = $user_wallets->id;
				$user_wallets_logs->amount        = $wallet_amount;
				$user_wallets_logs->type          = 1;
				$user_wallets_logs->created_date  = date("Y-m-d H:i:s");
				$user_wallets_logs->modified_date = date("Y-m-d H:i:s");
				$user_wallets_logs->save();
				/* To update the users table in wallet_amount */
				$users = Users::find($post_data['user_id']);
				$users->wallet_amount = $users->wallet_amount+$wallet_amount;
				$users->save();
				/* To send the mail to customer */
				$to      = $users->email;
				$subject = 'Your amount '.$wallet_amount.' added in wallet successfully';
				$template = DB::table('email_templates')
							->select('from_email', 'from', 'subject', 'template_id','content')
							->where('template_id','=',self::WALLET_MAIL_TEMPLATE)
							->get();
				$contact_email = getAppConfigEmail()->contact_mail;
				if(count($template)) {
					$from = $template[0]->from_email;
					$from_name = $template[0]->from;

					if(!$template[0]->template_id){
						$template = 'mail_template';
						$from     = $contact_email;
						$subject  = "Welcome to ".$site_name;
						$from_name="";
					}
					$content = array('name' => $users->name, 'wallet_amount' => !empty($users->wallet_amount)?$users->wallet_amount:0);
					$email   = smtp($from,$from_name,$to,$subject,$content,$template);
				}
				$result = array("response" => array("httpCode" => 200, "status" => true, "Message" => trans("messages.Amount added in wallet"), "user_wallet_amount" => $users->wallet_amount));
			} catch (JWTException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            } catch (TokenExpiredException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            }
		}
		return json_encode($result,JSON_UNESCAPED_UNICODE);
	}
}
