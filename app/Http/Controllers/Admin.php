<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use DB;
use App\Model\users;
use App\Model\customers_view;
use App\Model\customers;
use App\Model\vendors_view;
use App\Model\settings;
use App\Model\settings_infos;
use App\Model\emailsettings;
use App\Model\smssettings;
use App\Model\socialmediasettings;
use App\Model\imageresizesettings;

use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Hash;
use Illuminate\Support\Facades\Text;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class Admin extends Controller
{
                const COMMON_MAIL_TEMPLATE = 8;
                const ADMIN_CHANGE_PASSWORD_EMAIL_TEMPLATE = 21;

                
                use HasRoles;
            	protected $guard_name = 'web'; // or whatever guard you want to use

                    /**
                     * Create a new controller instance.
                     *
                     * @return void
                     */
                public function __construct()
                {
            		$this->site_name = isset(getAppConfig()->site_name)?ucfirst(getAppConfig()->site_name):'';
                    $this->middleware('auth');
                    SEOMeta::setTitle($this->site_name);
                    SEOMeta::setDescription($this->site_name);
                    SEOMeta::addKeyword($this->site_name);
                    OpenGraph::setTitle($this->site_name);
                    OpenGraph::setDescription($this->site_name);
                    OpenGraph::setUrl($this->site_name);
                    Twitter::setTitle($this->site_name);
                    Twitter::setSite('@'.$this->site_name);
                    App::setLocale('en');
                }

                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function index()
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }

                    $id    = Auth::id();
                    $users = Customers_view::find($id);

                    $this->loginactivity($users);
                    Session::flash('message', 'Logged in successfully');
                    return Redirect::to('admin/dashboard');
                }

                public function edit_profile($id)
                {
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    $users = Customers_view::find($id);
                    return view('admin.edit_profile')->with('data', $users);
                }


                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function update_profile(Request $data, $id)
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    $validation = Validator::make($data->all(), array(
                        //'title' => 'required',
                        'name' => 'required|alpha_num',
                        'designation' => 'required|alpha_num',
                        'image'       => 'mimes:png,jpeg,bmp|max:2024',
            			'mobile' => 'max:12|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
                        //mimes:jpeg,bmp,png and for max size max:10000
                    ));
                    // process the validation
                    if ($validation->fails()) {
                           return Redirect::back()->withErrors($validation);
                    } else {
                        
                        // store datas in to database
                        $users = Users::find($id);
                        $users->name          = $_POST['name'];
                        $users->mobile_number = $_POST['mobile'];
                        $users->updated_date  = date("Y-m-d H:i:s");
                        $users->save();


                        $customers = Customers_view::find($id);

                        $usertoken = sha1(uniqid(Text::random('alnum', 32), TRUE));
                        if(!$users->user_token){
                            $customers = customers::where('user_id',$id)->update(['user_token'=> $usertoken]);
                        }

                        $customers = customers::where('user_id','=',$id)
                                        ->update([
                                                    'designation'   => $_POST['designation'],
                                                    'gender'        => $_POST['gender'],
                                                    'date_of_birth' => $_POST['date_of_birth'],
                                                ]);

                        //$users->social_title      = $_POST['social_title'];

                        if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=''){ 
                            $destinationPath = base_path() .'/public/assets/admin/base/images/admin/profile/'; // upload path
                            $imageName = $users->id . '.' .
                            $data->file('image')->getClientOriginalExtension();
                            $data->file('image')->move($destinationPath, $imageName);
                            $destinationPath1 = url('/assets/admin/base/images/admin/profile/'.$imageName.'');
                            Image::make( $destinationPath1 )->fit(75, 75)->save(base_path() .'/public/assets/admin/base/images/admin/profile/thumb/'.$imageName)->destroy();

                            $customers = customers::where('user_id',$id)->update(['image'=> $imageName]);
                        }
                        
                        // redirect
                        Session::flash('message', trans('messages.Profile has been successfully updated'));
                        return Redirect::to('admin/editprofile/'.$id);
                    }
                }

                public function dashboard()
                {    
            		
            	 if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
            		$user = Users::find(Auth::id());
                
                    $users_query = "SELECT  
                    (
                    SELECT COUNT(1)
                    FROM customers_view
                    WHERE user_type !=1 and date_trunc('day', created_date) = date_trunc('day', CURRENT_DATE)) AS day_count,

                    (SELECT COUNT(1)
                    FROM customers_view
                    WHERE user_type !=1 and date_trunc('WEEK', created_date) = date_trunc('WEEK', CURRENT_DATE)) AS week_count,

                    (SELECT COUNT(1)
                    FROM customers_view
                    WHERE user_type !=1 and date_trunc('month', created_date) = date_trunc('month', CURRENT_DATE)) AS month_count,

                    (SELECT COUNT(1)
                    FROM customers_view
                    WHERE user_type !=1 and date_trunc('year', created_date) = date_trunc('year', CURRENT_DATE)) AS year_count,
                    COUNT(1) AS total_count
                    FROM customers_view WHERE user_type !=1";
                    $users_period_count = DB::select($users_query);
                    
                    $vendors_query = "SELECT  
                    (
                    SELECT COUNT(1)
                    FROM vendors_view
                    WHERE vendor_type = 1 and active_status = 1 and is_verified = 1 and date_trunc('day', created_date) = date_trunc('day', CURRENT_DATE)) AS day_count,

                    (SELECT COUNT(1)
                    FROM vendors_view
                    WHERE vendor_type = 1 and active_status = 1 and is_verified = 1 and date_trunc('WEEK', created_date) = date_trunc('WEEK', CURRENT_DATE)) AS week_count,

                    (SELECT COUNT(1)
                    FROM vendors_view
                    WHERE vendor_type = 1 and active_status = 1 and is_verified = 1 and date_trunc('month', created_date) = date_trunc('month', CURRENT_DATE)) AS month_count,

                    (SELECT COUNT(1)
                    FROM vendors_view
                    WHERE vendor_type = 1 and active_status = 1 and is_verified = 1 and date_trunc('year', created_date) = date_trunc('year', CURRENT_DATE)) AS year_count,
                    COUNT(1) AS total_count
                    FROM vendors where vendor_type = 1 and active_status = 1";
                    $vendors_period_count = DB::select($vendors_query);
                    
                    
                    
                    $outlets_query = "SELECT  
                    (
                    SELECT COUNT(1)
                    FROM outlets
                    WHERE date_trunc('day', created_date) = date_trunc('day', CURRENT_DATE)) AS day_count,

                    (SELECT COUNT(1)
                    FROM outlets
                    WHERE date_trunc('WEEK', created_date) = date_trunc('WEEK', CURRENT_DATE)) AS week_count,

                    (SELECT COUNT(1)
                    FROM outlets
                    WHERE date_trunc('month', created_date) = date_trunc('month', CURRENT_DATE)) AS month_count,

                    (SELECT COUNT(1)
                    FROM outlets
                    WHERE date_trunc('year', created_date) = date_trunc('year', CURRENT_DATE)) AS year_count,
                    COUNT(1) AS total_count
                    FROM outlets";
                    $outlets_period_count = DB::select($outlets_query);
                    
                    
                    $newsletter_subscribers_query = "SELECT  
                    (
                    SELECT COUNT(1)
                    FROM newsletter_subscribers
                    WHERE date_trunc('day', created_date) = date_trunc('day', CURRENT_DATE)) AS day_count,

                    (SELECT COUNT(1)
                    FROM newsletter_subscribers
                    WHERE date_trunc('WEEK', created_date) = date_trunc('WEEK', CURRENT_DATE)) AS week_count,

                    (SELECT COUNT(1)
                    FROM newsletter_subscribers
                    WHERE date_trunc('month', created_date) = date_trunc('month', CURRENT_DATE)) AS month_count,

                    (SELECT COUNT(1)
                    FROM newsletter_subscribers
                    WHERE date_trunc('year', created_date) = date_trunc('year', CURRENT_DATE)) AS year_count,
                    COUNT(1) AS total_count
                    FROM newsletter_subscribers";
                    $newsletter_subscribers_period_count = DB::select($newsletter_subscribers_query);
                    
                    
                    
                    $outlet_reviews_query = "SELECT  
                    (SELECT COUNT(1)
                    FROM outlet_reviews
                    WHERE date_trunc('day', created_date) = date_trunc('day', CURRENT_DATE)) AS day_count,

                    (SELECT COUNT(1)
                    FROM outlet_reviews
                    WHERE date_trunc('WEEK', created_date) = date_trunc('WEEK', CURRENT_DATE)) AS week_count,

                    (SELECT COUNT(1)
                    FROM outlet_reviews
                    WHERE date_trunc('month', created_date) = date_trunc('month', CURRENT_DATE)) AS month_count,

                    (SELECT COUNT(1)
                    FROM outlet_reviews
                    WHERE date_trunc('year', created_date) = date_trunc('year', CURRENT_DATE)) AS year_count,
                    COUNT(1) AS total_count
                    FROM outlet_reviews";
                    $outlet_reviews_query = DB::select($outlet_reviews_query);
                    
                    $blogs_query = "SELECT  
                    (
                    SELECT COUNT(1)
                    FROM blogs
                    WHERE date_trunc('day', created_at) = date_trunc('day', CURRENT_DATE)) AS day_count,

                    (SELECT COUNT(1)
                    FROM blogs
                    WHERE date_trunc('WEEK', created_at) = date_trunc('WEEK', CURRENT_DATE)) AS week_count,

                    (SELECT COUNT(1)
                    FROM blogs
                    WHERE date_trunc('month', created_at) = date_trunc('month', CURRENT_DATE)) AS month_count,

                    (SELECT COUNT(1)
                    FROM blogs
                    WHERE date_trunc('year', created_at) = date_trunc('year', CURRENT_DATE)) AS year_count,
                    COUNT(1) AS total_count
                    FROM blogs";
                    $blogs_count = DB::select($blogs_query);
                    
                    
                    
                    $outlets = DB::table('outlets')->select('outlets.id')->get();
                    $coupons  = DB::table('coupons')->where('active_status',1)->select('coupons.id')->get();
                    $newsletter_subscribers = DB::table('newsletter_subscribers')->where('active_status',1)->select('newsletter_subscribers.id')->get();


                    $web_user_query = "SELECT to_char(i, 'YYYY') as year_data, to_char(i, 'MM') as month_data, to_char(i, 'Month') as month_string, count(id) as web_total_count FROM generate_series(now() - INTERVAL '1 year', now(), '1 month') as i left join customers_view on (to_char(i, 'YYYY') = to_char(created_date, 'YYYY') and to_char(i, 'MM') = to_char(created_date, 'MM') and login_type = 1 and user_type != 1) GROUP BY 1,2,3 order by year_data desc, month_data desc limit 12";
                    $web_user_count = DB::select($web_user_query);
                    $android_user_query = "SELECT to_char(i, 'YYYY') as year_data, to_char(i, 'MM') as month_data, to_char(i, 'Month') as month_string, count(id) as android_total_count FROM generate_series(now() - INTERVAL '1 year', now(), '1 month') as i left join customers_view on (to_char(i, 'YYYY') = to_char(created_date, 'YYYY') and to_char(i, 'MM') = to_char(created_date, 'MM') and login_type = 2) GROUP BY 1,2,3 order by year_data desc, month_data desc limit 12";
                    $android_user_count = DB::select($android_user_query);
                    $ios_user_query = "SELECT to_char(i, 'YYYY') as year_data, to_char(i, 'MM') as month_data, to_char(i, 'Month') as month_string, count(id) as ios_total_count FROM generate_series(now() - INTERVAL '1 year', now(), '1 month') as i left join customers_view on (to_char(i, 'YYYY') = to_char(created_date, 'YYYY') and to_char(i, 'MM') = to_char(created_date, 'MM') and login_type = 3) GROUP BY 1,2,3 order by year_data desc, month_data desc limit 12";
                    $ios_user_count = DB::select($ios_user_query);

                    $language_id = getAdminCurrentLang();
                    $vendor_language_query = '"vendors_infos"."lang_id" = (case when (select count(id) as totalcount from vendors_infos where vendors_infos.lang_id = '.$language_id.' and vendors.user_id = vendors_infos.vendors_view_id) > 0 THEN '.$language_id.' ELSE 1 END)';
            /*        
                    $store_transaction_query = "SELECT vendors_infos.vendor_name, SUM (total_amount) AS total FROM orders JOIN orders_info on orders.id = orders_info.order_id join vendors on vendors.id = orders_info.vendor_id JOIN vendors_infos on vendors_infos.id = vendors.id where ".$vendor_language_query." GROUP BY vendor_name ORDER BY total DESC";
                    $store_transaction_count = DB::select($store_transaction_query);
            */
                    return view('admin.home')->with('outlets', $outlets)->with('coupons', $coupons)->with('newsletter_subscribers', $newsletter_subscribers)->with('web_user_count', $web_user_count)->with('android_user_count', $android_user_count)->with('ios_user_count', $ios_user_count)->with('users_period_count', $users_period_count)->with('vendors_period_count', $vendors_period_count)->with('newsletter_subscribers_period_count', $newsletter_subscribers_period_count)->with('blogs_count', $blogs_count)->with('outlets_period_count', $outlets_period_count)->with('outlet_reviews_query', $outlet_reviews_query);
                }

                public function adminlogout()
                { 
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    $id=Auth::id();
                    $users = Users::find($id);
                    $this->logoutactivity($users);
                    Auth::logout();
                    //$locale=Session::get('locale'); 
                    //App::setLocale($locale);
                    //Session::flush();
                    return Redirect::to('admin/login');
                }
                
                public function logoutactivity($obj)
                {
                    $user = $obj;
                    $message = "Logged out";
                    if($user->id) {
                        userlog($message,$user->id);
                    }
                }
                 public function loginactivity($obj)
                {
                    $user = $obj;
                    $message = "Logged in as administrator in ".ucfirst(Session::get("general")->site_name);
                    if($user->id) {
                        userlog($message,$user->id);
                    }
                }

                public function general_settings()
                { 
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    if(!has_permission('admin/settings/general'))
                    {
                        return view('errors.404');
                    }
                    $id = 1;
                    $settings  = Settings::find($id);
                    $info      = new Settings_infos;
                    $languages = DB::table('languages')->where('status','=',1)->get();
                    $countries = DB::select('select c.*, ci.* FROM "countries" AS "c" LEFT JOIN "countries_infos" AS "ci" ON ("ci"."id" = "c"."id" AND "ci"."language_id" = (case when (select count(*) as totalcount from countries_infos as cinfo where cinfo.language_id = 1 and id = ci.id) > 0 THEN 1 ELSE 1 END)) where country_status = 1 order by country_name asc');
                    return view('admin.settings.general_settings')->with('settings', $settings)->with('countries', $countries)->with('languages', $languages)->with('infomodel', $info);
                }
                /**
                 * Store updated settings in storage.
                 *
                 * @return Response
                 */
                public function update_general_settings(Request $data, $id)
                { 

                    //  dd($data->all());
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
            		if(!has_permission('admin/settings/general'))
                    {
                        return view('errors.404');
                    }
       

          
                    $validation = Validator::make($data->all(), array(
                        //'title' => 'required',
                        //'site_name' => 'required',
                        'site_owner' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3|max:32',
                        'email'=>'required|email',
                        //'telephone'=>'required', 
                         'telephone'=>'required|regex:/\(?\+?([0-9]{2,3})\)?([ .-]?)([0-9]{3,4})\2([0-9]{4})/',
                        'fax'=>'required',
                        'geocode'=>'required',
                        //  'min_fund_request' => 'required|regex:/^\d*(\.\d{1,2})?$/',
                        //  'max_fund_request' => 'required|regex:/^\d*(\.\d{1,2})?$/',
                        //'meta_title' => 'required',
                        //'meta_keywords' => 'required',
                        //'meta_description' => 'required',
                        'contact_address' => 'required',
                        //'default_language' => 'required',
                        //'default_country' => 'required',
                       // 'copyrights' => 'required',
                        'logo'       => 'mimes:png,jpeg,bmp|max:2024',
                        'favicon'    => 'mimes:png,jpeg,bmp,ico|max:2024',
                       // 'footer_text' => 'required',
                        'theme' => 'required',
                        //mimes:jpeg,bmp,png and for max size max:10000
                        //  'min_order_amount' => 'required',
                        //  'call_an_vendor'  => 'required',
                        //  'name_the_outlet' => 'required',
                        'delivery_cost' => 'required',
                    )); 


                    if(Input::has('is_service_tax_applicable'))
                        $rules['service_tax'] = 'required|numeric';

                    $site_name = Input::get('site_name');
                    foreach ($site_name  as $key => $value) {
                        $fields['site_name'.$key] = $value;
                        $rules['site_name'.'1'] = 'required';
                    }
                     $copyrights = Input::get('copyrights');
                    foreach ($copyrights  as $key => $value) {
                        $fields['copyrights'.$key] = $value;
                        $rules['copyrights'.'1'] = 'required';
                    }
                    $meta_title = Input::get('meta_title');
                    foreach ($meta_title  as $key => $value) {
                        $fields['meta_title'.$key] = $value;
                        $rules['meta_title'.'1'] = 'required';
                    }
                    $meta_keywords = Input::get('meta_keywords');
                    foreach ($meta_keywords  as $key => $value) {
                        $fields['meta_keywords'.$key] = $value;
                        $rules['meta_keywords'.'1'] = 'required';
                    }
                     $meta_description = Input::get('meta_description');
                    foreach ($meta_description  as $key => $value) {
                        $fields['meta_description'.$key] = $value;
                        $rules['meta_description'.'1'] = 'required';
                    }
                    $footer_text = Input::get('footer_text');
                    foreach ($footer_text  as $key => $value) {
                        $fields['footer_text'.$key] = $value;
                        $rules['footer_text'.'1'] = 'required';
                    }
                    $site_description = Input::get('site_description');
                    foreach ($site_description  as $key => $value) {
                        $fields['site_description'.$key] = $value;
                        $rules['site_description'.'1'] = 'required';
                    }
                    // process the validation
                    if ($validation->fails()) {
                           return Redirect::back()->withErrors($validation)->withInput();
                    } else {//echo '<pre>';print_r($_POST);die;
                        // store datas in to database
                        $settings = Settings::find($id);
                       // $settings->site_name      = $_POST['site_name'];
                        $settings->site_owner      = $_POST['site_owner'];
                        $settings->email      = $_POST['email'];  
                        $settings->telephone      = $_POST['telephone'];
                        $settings->fax      = $_POST['fax'];
                        //  $settings->min_fund_request      = $_POST['min_fund_request'];
                        //  $settings->max_fund_request      = $_POST['max_fund_request'];
                        $settings->geocode      = $_POST['geocode'];//print_r($settings->geocode);exit;
                        //$settings->meta_title    = $_POST['meta_title'];
                        //$settings->meta_keywords    = $_POST['meta_keywords'];
                        //$settings->site_description    = $_POST['site_description'];
                        //$settings->meta_description    = $_POST['meta_description'];
                        $settings->contact_address    = $_POST['contact_address'];
                        //$settings->default_language    = $_POST['default_language'];
                       // $settings->default_country    = $_POST['default_country'];
                        //$settings->copyrights    = $_POST['copyrights'];
                        $settings->updated_at = date("Y-m-d H:i:s");
                       // $settings->footer_text    = $_POST['footer_text'];
                        $settings->theme    = $_POST['theme'];
                       //echo '<pre>'; print_r( $settings);exit;
                        //  $settings->min_order_amount = $_POST['min_order_amount'];
                        $settings->delivery_cost = $_POST['delivery_cost'];
                        $settings->is_service_tax_applicable = Input::has('is_service_tax_applicable')? true: false;
/*                        
                        $settings->call_an_vendor= $_POST['call_an_vendor'];
                        $settings->name_the_outlet= $_POST['name_the_outlet'];
*/                        
                        $settings->vendor_commision= $_POST['vendor_commision'];
                        $settings->referral_commision= $_POST['referral_commision'];
                        $settings->cancellation_time = $_POST['cancellation_time'];
                        
                        if(Input::has('is_service_tax_applicable'))
                            $settings->service_tax = Input::get('service_tax');
                        else
                            $settings->service_tax = 0;

                        $settings->save();
                        
                        if(isset($_FILES['logo']['name']) && $_FILES['logo']['name']!=''){ 
                            $destinationPath = base_path() .'/public/assets/front/'.Session::get("general")->theme.'/images/logo/'; // upload path
                            $logoName = 'logo' . '.' . 
                            $data->file('logo')->getClientOriginalExtension();
                            $data->file('logo')->move(base_path() . '/public/assets/front/'.Session::get("general")->theme.'/images/logo/', $logoName);
                            $destinationPath1 = url('/assets/front/'.Session::get("general")->theme.'/images/logo/'.$logoName.'');
                            
                            
                            Image::make( $destinationPath1 )    /*->fit(getImageResize('LOGO')['WIDTH'],getImageResize('LOGO')['HEIGHT'])   */->save(base_path() .'/public/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.$logoName)->destroy();
                            $settings->logo = $logoName;
                            $settings->save();
                        }
                        if(isset($_FILES['front_logo']['name']) && $_FILES['front_logo']['name']!=''){ 
                            $destinationPath = base_path() .'/public/assets/front/'.Session::get("general")->theme.'/images/logo/'; // upload path
                            $frontlogoName = 'front_logo' . '.' . 
                            $data->file('front_logo')->getClientOriginalExtension();
                            $data->file('front_logo')->move(base_path() . '/public/assets/front/'.Session::get("general")->theme.'/images/logo/', $frontlogoName);
                            $destinationPath1 = url('/assets/front/'.Session::get("general")->theme.'/images/logo/'.$frontlogoName.'');
                             Image::make( $destinationPath1 )->fit(212,60)->save(base_path() .'/public/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.$frontlogoName)->destroy();
                            $settings->front_logo = $frontlogoName;
                            
                            $settings->save();
                        }
                        if(isset($_FILES['arabic_logo']['name']) && $_FILES['arabic_logo']['name']!=''){ 
                            $destinationPath = base_path() .'/public/assets/front/'.Session::get("general")->theme.'/images/logo/'; // upload path
                            $arabiclogoName = 'arabic_logo' . '.' . 
                            $data->file('arabic_logo')->getClientOriginalExtension();
                            $data->file('arabic_logo')->move(base_path() . '/public/assets/front/'.Session::get("general")->theme.'/images/logo/', $arabiclogoName);
                            $destinationPath1 = url('/assets/front/'.Session::get("general")->theme.'/images/logo/'.$arabiclogoName.'');
                            Image::make( $destinationPath1 )->fit(199,133)->save(base_path() .'/public/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.$arabiclogoName)->destroy();
                            $settings->arabic_logo = $arabiclogoName;
                            $settings->save();
                        }
                        if(isset($_FILES['favicon']['name']) && $_FILES['favicon']['name']!=''){ 
                            $destinationPath = base_path() .'/public/assets/front/'.Session::get("general")->theme.'/images/favicon/'; // upload path
                            $faviconName = 'favicon' . '.' . 
                            $data->file('favicon')->getClientOriginalExtension();
                            $data->file('favicon')->move(base_path() . '/public/assets/front/'.Session::get("general")->theme.'/images/favicon/', $faviconName);
                            $destinationPathfavicon = url('/assets/front/'.Session::get("general")->theme.'/images/favicon/'.$faviconName.'');
                            Image::make( $destinationPathfavicon )->fit(getImageResize('FAVICON')['WIDTH'], getImageResize('FAVICON')['HEIGHT'])->save(base_path() .'/public/assets/front/'.Session::get("general")->theme.'/images/favicon/16_16/'.$faviconName)->destroy();
                            $settings->favicon = $faviconName;
                            $settings->save();
                        }
                        $this->settings_save_after($settings,$_POST);
                        Session::flash('message', trans('messages.General settings has been successfully updated'));
                        return Redirect::to('admin/settings/general');
                    }
                }

                public static function settings_save_after($object,$post)
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    if(isset($post['site_name'])) {
                       
                        $site_name = $post['site_name'];
                        $meta_title = $post['meta_title'];
                        $meta_keywords = $post['meta_keywords'];
                        $meta_description = $post['meta_description'];
                        $site_description = $post['site_description'];
                        $copyrights = $post['copyrights']; 
                        $footer_text = $post['footer_text'];
                        try{ 
                            $data = Settings_infos::find($object->id);
                         
                            if(count($data)>0){
                                $data->delete();
                            } 
                            $languages = DB::table('languages')->where('status', 1)->get();$s = 0;
                            foreach($languages as $key => $lang){  
                                if((isset($copyrights[$lang->id]) && $copyrights[$lang->id]!="")){
                                    $infomodel = new Settings_infos; 
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->id = $object->id; 
                                    $infomodel->site_name = $site_name[$lang->id]; 
                                    $infomodel->site_description = $site_description[$lang->id]; //echo '<pre>';print_r( $infomodel);exit; 
                                    $infomodel->meta_title = $meta_title[$lang->id]; 
                                    $infomodel->meta_keywords = $meta_keywords[$lang->id];
                                    $infomodel->meta_description = $meta_description[$lang->id]; 
                                    $infomodel->footer_text = $footer_text[$lang->id];
                                    $infomodel->copyrights = $copyrights[$lang->id];//if($s==1){echo '<pre>' ; print_r($infomodel);exit;}
                                    $infomodel->save(); //echo"in";exit;
                                    $s++;
                                }
                            }
                        } catch(Exception $e) {
                            Log::Instance()->add(Log::ERROR, $e);
                        }
                    }
                }

                public function email_settings()
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    if (!has_permission('admin/settings/email'))
                    {
                        return view('errors.404');
                    }
                    $id=1;
                    $settings = Emailsettings::find($id);
                    return view('admin.settings.email_settings')->with('settings', $settings);
                }

                    /**
                 * Store updated email settings in storage.
                 *
                 * @return Response
                 */
                public function update_email_settings(Request $data, $id)
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
            		if (!has_permission('admin/settings/email'))
                    {
                        return view('errors.404');
                    }
                    // validate
                    // read more on validation at http://laravel.com/docs/validation
                    $validation = Validator::make($data->all(), array(
                        //'title' => 'required',
                        'contact_mail' => 'required|email',
                        'support_mail' => 'required|email',
                        'mobile_number' => 'required',
                        'smtp_host_name' => 'required',
                        'smtp_username' => 'required',
                        'smtp_password' => 'required',
                        'smtp_port' => 'required',
                        'smtp_encryption' => 'required',
                        'mail_driver' => 'required',
                    ));
                    // process the validation
                    if ($validation->fails()) {
                           return Redirect::back()->withErrors($validation)->withInput();
                    } else {
                        // store datas in to database
                        $settings = Emailsettings::find($id);
                        $settings->contact_mail      = $_POST['contact_mail'];
                        $settings->support_mail    = $_POST['support_mail'];
                        $settings->mobile_number    = $_POST['mobile_number'];
                        $settings->skype    = $_POST['skype'];
                        $settings->smtp_host_name    = $_POST['smtp_host_name'];
                        $settings->smtp_username    = $_POST['smtp_username'];
                        $settings->smtp_password    = $_POST['smtp_password'];
                        $settings->smtp_port    = $_POST['smtp_port'];
                        $settings->smtp_encryption    = $_POST['smtp_encryption'];
                        $settings->smtp_enable    = $_POST['smtp_enable'];
                        $settings->mail_driver    = $_POST['mail_driver'];           
                        $settings->updated_at = date("Y-m-d H:i:s");
                        $settings->save();
                        // redirect
                        Session::flash('message', trans('messages.Email settings has been successfully updated'));
                        return Redirect::to('admin/settings/email');
                    }
                }

                public function social_media_settings()
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
            		if (!has_permission('admin/settings/socialmedia'))
                    {
                        return view('errors.404');
                    }
                    $id=1;
                    $settings = Socialmediasettings::find($id);
                    return view('admin.settings.social_media_settings')->with('settings', $settings);
                }

                /**
                 * Store updated social media settings in storage.
                 *
                 * @return Response
                 */
                public function update_media_settings(Request $data, $id)
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
            		if (!has_permission('admin/settings/socialmedia'))
                    {
                        return view('errors.404');
                    }
                    // validate
                    // read more on validation at http://laravel.com/docs/validation
                    $validation = Validator::make($data->all(), array(
                        'facebook_page' => 'required',
                        'twitter_page' => 'required',
                         'instagram_page' => 'required',
                        'linkedin_page' => 'required',
                        'google_plus_page' => 'required',
                        'tumblr_page' => 'required',
                        'youtube_url' => 'required',
                        'android_page' => 'required',
                        'iphone_page' => 'required',
                        'facebook_app_id' => 'required',
                        'facebook_secret_key' => 'required',
                        'twitter_api_key' => 'required',
                        'twitter_secret_key' => 'required',
                        'gmap_api_key' => 'required',
                        'analytics_code' => 'required',
                    ));
                    // process the validation
                    if ($validation->fails()) {
                           return Redirect::back()->withErrors($validation)->withInput();
                    } else {
                        // store datas in to database
                        $settings = Socialmediasettings::find($id);
                        $settings->facebook_page      = $_POST['facebook_page'];
                        $settings->instagram_page      = $_POST['instagram_page'];
                        $settings->twitter_page    = $_POST['twitter_page'];
                        $settings->linkedin_page    = $_POST['linkedin_page'];
                        $settings->google_plus_page    = $_POST['google_plus_page'];
                        $settings->tumblr_page    = $_POST['tumblr_page'];
                        $settings->youtube_url    = $_POST['youtube_url'];
                        $settings->android_page    = $_POST['android_page'];
                        $settings->iphone_page    = $_POST['iphone_page'];
                        $settings->facebook_app_id    = $_POST['facebook_app_id'];
                        $settings->facebook_secret_key    = $_POST['facebook_secret_key'];
                        $settings->twitter_api_key    = $_POST['twitter_api_key'];
                        $settings->twitter_secret_key    = $_POST['twitter_secret_key'];
                        $settings->gmap_api_key    = $_POST['gmap_api_key'];
                        $settings->analytics_code    = $_POST['analytics_code'];      
                        $settings->updated_at = date("Y-m-d H:i:s");
                        $settings->save();
                        // redirect
                        Session::flash('message', trans('messages.Social media settings has been successfully updated'));
                        return Redirect::to('admin/settings/socialmedia');
                    }
                }
                
                public function local()
                { 
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    if(!has_permission('admin/settings/local'))
                    {
                        return view('errors.404');
                    }
                    $settings = Settings::find(1);
                    return view('admin.settings.local_settings')->with('settings', $settings);
                }
                
                        /**
                 * Store updated email settings in storage.
                 *
                 * @return Response
                 */
                public function update_local(Request $data, $id)
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
            		if(!has_permission('admin/settings/local'))
                    {
                        return view('errors.404');
                    }
                    // validate
                    // read more on validation at http://laravel.com/docs/validation
                    $validation = Validator::make($data->all(), array(
                        //'title' => 'required',
                        'default_country'  => 'required|integer',
                        'default_city'     => 'required|integer',
                        'default_language' => 'required|integer',
                        'default_currency' => 'required|integer',
                        'currency_side'    => 'required|integer',
                        //  'default_weight_class' => 'required|integer',
                    ));
                    // process the validation
                    if ($validation->fails()) {
                           return Redirect::back()->withErrors($validation)->withInput();
                    } else {
                        // store datas in to database
                        $settings = Settings::find($id);
                        $settings->default_country     = $_POST['default_country'];
                        $settings->default_city        = $_POST['default_city'];
                        $settings->default_language    = $_POST['default_language'];
                        $settings->default_currency    = $_POST['default_currency'];
                        $settings->currency_side       = $_POST['currency_side'];
                        //  $settings->default_weight_class= $_POST['default_weight_class'];
                        $settings->save();
                        // redirect
                        Session::flash('message', trans('messages.Local settings has been successfully updated'));
                        return Redirect::to('admin/settings/local');
                    }
                }
                
                public function image_settings()
                { 
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    if (!has_permission('admin/settings/image'))
                    {
                        return view('errors.404');
                    }
                    $id=1;
                    $common = Imageresizesettings::find(1);
                    $store = Imageresizesettings::find(2);
                    $product = Imageresizesettings::find(3);
                    $banner = Imageresizesettings::find(4);
                    $vendor = Imageresizesettings::find(5);
                    return view('admin.settings.image_settings')->with('common', $common)->with('store', $store)->with('product', $product)->with('banner', $banner)->with('vendor', $vendor);
                }
                
                 /**
                 * Store updated email settings in storage.
                 *
                 * @return Response
                 */
                public function update_image_settings(Request $data, $id)
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
            		if (!has_permission('admin/settings/image'))
                    {
                        return view('errors.404');
                    }
                    // validate
                    // read more on validation at http://laravel.com/docs/validation
                    $validation = Validator::make($data->all(), array(
                        //'title' => 'required',
                        'logo_width' => 'required|numeric',
                        'logo_height' => 'required|numeric',
                        'favicon_width' => 'required|numeric',
                        'favicon_height' => 'required|numeric',
                        'category_width' => 'required|numeric',
                        'category_height' => 'required|numeric',
                        'store_list_width' => 'required|numeric',
                        'store_list_height' => 'required|numeric',
                        'store_detail_width' => 'required|numeric',
                        'store_detail_height' => 'required|numeric',
                        'store_thumb_width' => 'required|numeric',
                        'store_thumb_height' => 'required|numeric',
                        'product_list_width' => 'required|numeric',
                        'product_list_height' => 'required|numeric',
                        'product_detail_width' => 'required|numeric',
                        'product_detail_height' => 'required|numeric',
                        'product_thumb_width' => 'required|numeric',
                        'product_thumb_height' => 'required|numeric',
                        'banner_list_width' => 'required|numeric',
                        'banner_list_height' => 'required|numeric',
                    ));
                    // process the validation
                    if ($validation->fails()) {
                           return Redirect::back()->withErrors($validation)->withInput();
                    } else {
                    

                            if($_POST['common']){
                                
                                $common = Imageresizesettings::find(1);
                                $common->list_width      = $_POST['logo_width'];
                                $common->list_height      = $_POST['logo_height'];
                                $common->detail_width      = $_POST['favicon_width'];
                                $common->detail_height      = $_POST['favicon_height'];
                                $common->thumb_width      = $_POST['category_width'];
                                $common->thumb_height      = $_POST['category_height'];
                                $common->type      = $_POST['common'];
                                $common->updated_at = date("Y-m-d H:i:s");
                                $common->save();
                            }
                            
                            if($_POST['store']){
                                $store = Imageresizesettings::find(2);
                                $store->list_width      = $_POST['store_list_width'];
                                $store->list_height      = $_POST['store_list_height'];
                                $store->detail_width      = $_POST['store_detail_width'];
                                $store->detail_height      = $_POST['store_detail_height'];
                                $store->thumb_width      = $_POST['store_thumb_width'];
                                $store->thumb_height      = $_POST['store_thumb_height'];
                                $store->type      = $_POST['store'];
                                $store->updated_at = date("Y-m-d H:i:s");
                                $store->save();
                            }

                            if($_POST['product']){
                                $product = Imageresizesettings::find(3);
                                $product->list_width      = $_POST['product_list_width'];
                                $product->list_height      = $_POST['product_list_height'];
                                $product->detail_width      = $_POST['product_detail_width'];
                                $product->detail_height      = $_POST['product_detail_height'];
                                $product->thumb_width      = $_POST['product_thumb_width'];
                                $product->thumb_height      = $_POST['product_thumb_height'];
                                $product->type      = $_POST['product'];
                                $product->updated_at = date("Y-m-d H:i:s");
                                $product->save();
                            }
                            
                            if($_POST['banner']){
                                $banner = Imageresizesettings::find(4);
                                $banner->list_width      = $_POST['banner_list_width'];
                                $banner->list_height      = $_POST['banner_list_height'];
                                $banner->type      = $_POST['banner'];
                                $banner->updated_at = date("Y-m-d H:i:s");
                                $banner->save();
                            }
                            if($_POST['vendor']){
                                $vendor = Imageresizesettings::find(5);
                                $vendor->list_width      = $_POST['vendor_list_width'];
                                $vendor->list_height      = $_POST['vendor_list_height'];
                                $vendor->detail_width      = $_POST['vendor_detail_width'];
                                $vendor->detail_height      = $_POST['vendor_detail_height'];
                                $vendor->thumb_width      = $_POST['vendor_thumb_width'];
                                $vendor->thumb_height      = $_POST['vendor_thumb_height'];
                                $vendor->type      = $_POST['vendor'];
                                $vendor->updated_at = date("Y-m-d H:i:s");
                                $vendor->save();
                            }
                        // redirect
                        Session::flash('message', trans('messages.Image settings has been successfully updated'));
                        return Redirect::to('admin/settings/image');
                    }
                }
                /*
                 * Render the change password view
                */
                public function change_password() 
                {
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    else {
                        return view('admin.reset');
                    }
                }
                
                /*
                 * Vendor change password request goes here
                */
                public function change_details(Request $data) 
                {
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    $datas = Input::all();
                    // validate
                    // read more on validation at http://laravel.com/docs/validation
                    $validation = Validator::make($data->all(), array(
                        'old_password' => 'required|min:5|max:16|regex:/(^[A-Za-z0-9 !@#$%]+$)+/',
                        'password' => 'required|min:5|max:16|confirmed|regex:/(^[A-Za-z0-9 !@#$%]+$)+/',
                        'password_confirmation' => 'required|min:5|max:16|regex:/(^[A-Za-z0-9 !@#$%]+$)+/'
                    ));
                    // process the validation
                    if ($validation->fails())
                    {
                        //return redirect('create')->withInput($datas)->withErrors($validation);
                        return Redirect::back()->withErrors($validation)->withInput();
                    }
                    else {


                        //Get new password details from posts
                        $old_password   = Input::get('old_password');
                        $string         = Input::get('password');
                        $pass_string    = Hash::make($string);
                        $old_pass_string= Hash::make($old_password);
                        $session_userid = Auth::id();
                    
                        $users_data     = DB::table('customers_view')
                                            ->select('id','name','email','password')
                                            ->where('id',$session_userid)
                                            ->Where(function($query) {
                                                $query->orWhere('user_type', 1)
                                                    ->orWhere('user_type', 2);
                                            })
                                            ->first();

                        if(count($users_data) > 0 && Hash::check($old_password,$users_data->password) == 1)
                        {
                            //Sending the mail to vendors
                            $template = DB::table('email_templates')
                                                ->select('from_email','from','subject','template_id','content')
                                                ->where('template_id','=',self::ADMIN_CHANGE_PASSWORD_EMAIL_TEMPLATE)
                                                ->get();

                            //print_r($template);
                            //exit;
                            if(count($template))
                            {
                                $from      = $template[0]->from_email;
                                $from_name = $template[0]->from;
                                $subject   = $template[0]->subject;
                                if(!$template[0]->template_id)
                                {
                                    $template  = 'mail_template';
                                    $from      = getAppConfigEmail()->contact_email;
                                    $subject   = getAppConfig()->site_name." New Password Request Updated";
                                    $from_name = "";
                                }
                                $content = array("name" => ''.$users_data->name, "email" => ''.$users_data->email, "password" => ''.$string);
                                $email = smtp($from,$from_name,$users_data->email,$subject,$content,$template);
                            }
                            //Update random password to users table to coreesponding admin
                            $res = DB::table('users')
                                    ->where('id', $session_userid)
                                    ->update(['password' => $pass_string]);
                            //After updating new password details logout the session and redirects to login page
                            Session::flash('message', trans('messages.Your Password Changed Successfully.'));
                            return Redirect::to('admin/dashboard');
                        }
                        else {
                            $validation->errors()->add('old_password', 'Old password is incorrect.');
                            return Redirect::back()->withErrors($validation)->withInput();
                        }
                    }
                }

                /* Newsletter Management Start */
                public function newsletter()
                {
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    else {
                        if(!has_permission('admin/newsletter')){
                            return view('errors.404');
                        }
                        SEOMeta::setTitle('Newsletter - '.$this->site_name);
                        SEOMeta::setDescription('Newsletter - '.$this->site_name);
                        return view('admin.newsletter.send');
                    }
                }
                public function send_newsletter(Request $data)
                {
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    if(!has_permission('send_newsletter')){
                        return view('errors.404');
                    }
                    $entity_type       = $_POST['entity_type'];
                    $fields['users']   = isset($_POST['users'])?$_POST['users']:'';
                    $fields['subject'] = $_POST['subject'];
                    $fields['message'] = $_POST['message'];
                    $rules = array(
                        'users'   => $_POST['mail_type']==1 ? 'required' : '',
                        'subject' => 'required',
                        'message' => 'required',
                    );
                    $validator = Validator::make($fields, $rules);
                    // process the validation
                    if ($validator->fails())
                    {
                        return Redirect::back()->withErrors($validator)->withInput();
                    }
                    else {
                        try{
                            if( $_POST['mail_type']==2 )
                            {
                                $from_email = getAppConfigEmail()->smtp_username;
                                $from_name = getAppConfig()->site_name;
                                mailchimpCampaign($from_email,$from_name,$_POST['subject'],$_POST['message']);
                            }
                            else if( $_POST['mail_type']==1 )
                            {
                                if( $entity_type == 3 )
                                {
                                    $groups     = $_POST['users'];
                                    $users_list = all_customers_list($groups);
                                    $user_email = array();
                                    if(count($users_list) > 0 )
                                    {
                                        $u = 0;
                                        foreach($users_list as $u_l)
                                        {
                                            $user_email[$u] = $u_l->email;
                                            $u++;
                                        }
                                        $email    = $user_email;
                                        $subject  = $_POST['subject'];
                                        $content  = $_POST['message'];
                                        $template = DB::table('email_templates')
                                                    ->select('from_email', 'from', 'subject', 'template_id','content')
                                                    ->where('template_id','=',self::COMMON_MAIL_TEMPLATE)
                                                    ->get();
                                        if(count($template))
                                        {
                                            $from      = $template[0]->from_email;
                                            $from_name = $template[0]->from;
                                            //$subject = $template[0]->subject;
                                            if(!$template[0]->template_id)
                                            {
                                                $template  = 'mail_template';
                                                $from      = getAppConfigEmail()->contact_email;
                                                $subject   = "Welcome to ".getAppConfig()->site_name;
                                                $from_name = "";
                                            }
                                            $content = array("notification" => array('MAIL' => $content));
                                            $email   = smtp($from,$from_name,$email,$subject,$content,$template);
                                        }
                                    }
                                }
                                else {
                                    $email    = $_POST['users'];
                                    $subject  = $_POST['subject'];
                                    $content  = $_POST['message'];
                                    $template = DB::table('email_templates')
                                                ->select('from_email', 'from', 'subject', 'template_id','content')
                                                ->where('template_id','=',self::COMMON_MAIL_TEMPLATE)
                                                ->get();
                                    if(count($template))
                                    {
                                        $from      = $template[0]->from_email;
                                        $from_name = $template[0]->from;
                                        //$subject = $template[0]->subject;
                                        if(!$template[0]->template_id)
                                        {
                                            $template  = 'mail_template';
                                            $from      = getAppConfigEmail()->contact_email;
                                            $subject   = "Welcome to ".getAppConfig()->site_name;
                                            $from_name = "";
                                        }
                                        $content = array("notification" => array('MAIL' => $content));
                                        $email   = smtp($from,$from_name,$email,$subject,$content,$template);
                                    }
                                }
                            }
                            Session::flash('message', trans('messages.The Newsletter has been sent successfully'));
                        } catch(Exception $e) {
                            Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('admin/newsletter');
                    }
                }
                /* To get the all customers list */
                public function getAllCustomersData(Request $request)
                {
                     if (Auth::guest())
                    {
                        return 404;
                    }
                    if($request->ajax())
                    {
                        $customers_list = all_customers_list();
                        return response()->json([
                            'data' => $customers_list
                        ]);
                    }
                }
                /* To get the all newsletter subscribers list */
                public function getAllSubscribersData(Request $request)
                {
                    if (Auth::guest())
                    {
                        return 404;
                    }

                    if($request->ajax())
                    {
                        $newsletter_subs_list = all_newsletter_subscribers_list();
                        return response()->json([
                            'data' => $newsletter_subs_list
                        ]);
                    }
                }
                /* To get the all customers groups list */
                public function getAllCustomersGroupData(Request $request)
                {
                    if (Auth::guest())
                    {
                        return 404;
                    }
                    if($request->ajax())
                    {
                        $customers_groups_list = all_customers_groups_list();
                        return response()->json([
                            'data' => $customers_groups_list
                        ]);
                    }
                }  

                		//Get city list for ajax request
            	public function getUserData(Request $request)
            	{
                    if (Auth::guest())
                    {
                        return 404;
                    }
            		if($request->ajax()){
            			$entity_type = $request->input('entity_type');
            			$users_list = getUserList($entity_type);
            			return response()->json([
            				'data' => $users_list
            			]);
            		}
            	}
                /* Newsletter Management End */

                public function  sms_settings(){
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    if (!has_permission('admin/settings/sms'))
                    {
                        return view('errors.404');
                    }
                    $id=1;
                    $settings = Smssettings::find($id);
                    return view('admin.settings.sms_settings')->with('settings', $settings);
                }

                  /**
                 * Store updated social media settings in storage.
                 *
                 * @return Response
                 */
                public function update_sms_settings(Request $data, $id)
                {
                    // echo "<pre>";
                    // print_r($data->all());exit;
                     if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
                    if (!has_permission('admin/settings/sms'))
                    {
                        return view('errors.404');
                    }
                    // validate
                    // read more on validation at http://laravel.com/docs/validation
                    $validation = Validator::make($data->all(), array(
                        'sms_account_id' => 'required',
                        'sms_account_token' => 'required',
                         'sms_sender_number' => 'required',
                    ));
                    // process the validation
                    if ($validation->fails()) {
                           return Redirect::back()->withErrors($validation)->withInput();
                    } else {
                        // store datas in to database
                        $settings = Smssettings::find($id);
                        $settings->sms_account_id      = $_POST['sms_account_id'];
                        $settings->sms_account_token      = $_POST['sms_account_token'];
                        $settings->sms_sender_number    = $_POST['sms_sender_number'];
                        $settings->save();
                        // redirect
                        Session::flash('message', trans('messages.Sms settings has been successfully updated'));
                        return Redirect::to('admin/settings/sms');
                    }
                }

}
