<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;
use App\Model\housekeeping_staffs as HouseKeepingModel;
use App\Model\housekeeping_tasks as HouseKeepingTasks;
use App\Model\rooms as Rooms;

class HouseKeeping extends Controller
{
    
     public function __construct(){

     }

     /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyAjaxHousekeeping()
    { 
        $vendor_id = Session::get('vendor_id');
          //  print_r($id); exit;

          if(Session::get('vendor_type') == 2 )
              {
                  $vendor_id = Session::get('created_vendor_id');
                  //  print_r($id); exit;
              }                   
        $vendor_id = Session::get('vendor_id');
        $property_id = Session::get('property_id');
        $outlet_staffs=DB::table('housekeeping_staffs')
         ->leftjoin('outlet_infos','outlet_infos.id','=','housekeeping_staffs.outlet_id')
        ->where('housekeeping_staffs.vendor_id','=',$vendor_id)
        ->where('housekeeping_staffs.outlet_id','=',$property_id)
        ->select('housekeeping_staffs.id','outlet_infos.outlet_name','housekeeping_staffs.firstname','housekeeping_staffs.lastname','housekeeping_staffs.mobile_number','housekeeping_staffs.status','housekeeping_staffs.created_at','housekeeping_staffs.updated_at')
        ->orderBy('outlet_infos.outlet_name', 'asc')
        ->get();
        return Datatables::of($outlet_staffs)->addColumn('action', function ($outlet_staffs) {
          if(has_staff_permission('vendor/house_keeping/edit')){
                return '<div class="btn-group"><a href="'.URL::to("vendor/house_keeping/edit/".$outlet_staffs->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                        <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu xs pull-right" role="menu">
                        <li><a href="'.URL::to("vendor/house_keeping/view/".$outlet_staffs->id).'"  title="'.trans("messages.View").'"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;'.@trans("messages.View").'</a></li>
                        </li>
                        <li><a href="'.URL::to("vendor/house_keeping/delete/".$outlet_staffs->id).'" class="delete-'.$outlet_staffs->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                        </ul>
                    </div><script type="text/javascript">
                    $( document ).ready(function() {
                    $(".delete-'.$outlet_staffs->id.'").on("click", function(){
                         return confirm("'.trans("messages.Are you sure want to delete?").'");
                    });});</script>';
              }
            })
            ->editColumn('firstname', function ($outlet_staffs) {
                $data = '-';
                if($outlet_staffs->firstname != null && $outlet_staffs->lastname != null):
              $data = $outlet_staffs->firstname.' '.$outlet_staffs->lastname;
                else:
              $data = $outlet_staffs->firstname;
                endif;
                return $data;
            })
            ->editColumn('status', function ($outlet_staffs) {
                    $data = '-';
                    if($outlet_staffs->status != null)
                    {
                      if($outlet_staffs->status == 0){ 
                        $data = "<span class='label label-danger'>Inactive</span>";
                       }elseif($outlet_staffs->status == 1){ 
                        $data = "<span class='label label-success'>Active</span>";
                       }
                    } 
                    return $data;
            })
            ->make(true);
    }

    /**
     * Show the House Keeper List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Session::get('vendor_id')) {
                return redirect()->guest('vendors/login');
        } 
        if (!Session::get('property_id'))
            {
                Session::flash('no-property','Add Property First');
                return redirect::to('vendor/outlets');
            }          
        else {
            return view('vendors.house_keeping.list');
        }
        
    }

    /**
     * Show the House Keeper Add Form.
     *
     */
    public function create()
    {
        if (!Session::get('vendor_id'))
        {
            return redirect()->guest('vendors/login');
        } else {
            return view('vendors.house_keeping.create');
        }
    }

    /**
     * Storing the House Keeper Details.
     *
     */
    public function store(Request $data)
    {
        if (!Session::get('vendor_id')){
            return redirect()->guest('vendors/login');
        } else {
               
              $validation = Validator::make($data->all(),array(
                  'first_name' => 'required|alpha',
                  'last_name' => 'required|alpha',
                  'mobile' => 'required|numeric|digits_between:6,16',
                  'date_of_birth' => 'required',
                  'gender' => 'required',
                  'outlet_name' => 'required', 
                  'address' => 'required|alpha',
                  'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:2048',
              ));   
                      // process the validation
              if ($validation->fails()) {
                  return Redirect::back()->withErrors($validation)->withInput();
              } else {

                  $vendor_session_id = Session::get('vendor_id');
                  $outlet_id = Input::get('outlet_name');
                  $total_staffs = getHousekeepingStaffLimit(1,$outlet_id);
                  $staffs_limit = getHousekeepingStaffLimit(2,$vendor_session_id);

                  if($total_staffs >= $staffs_limit){
                      $errors = array('You have reached your maximum housekeeping staffs limit.');
                      return Redirect::back()->withErrors($errors)->withInput();
                  }

                  $housekeeping_staffs = new HouseKeepingModel();
                  $housekeeping_staffs->firstname = Input::get('first_name');
                  $housekeeping_staffs->lastname = Input::get('last_name');
                  $housekeeping_staffs->mobile_number = Input::get('mobile');
                  $housekeeping_staffs->date_of_birth = Input::get('date_of_birth');
                  $housekeeping_staffs->gender = Input::get('gender');
                  $housekeeping_staffs->outlet_id = Input::get('outlet_name');
                  $housekeeping_staffs->vendor_id = Session::get('vendor_id');
                  $housekeeping_staffs->address = Input::get('address');
                  $housekeeping_staffs->status = (int)Input::get('status');
                  $housekeeping_staffs->save();

                  //For Housekeeper Image
                  if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=''){ 
                      $destinationPath = base_path().'/public/assets/admin/base/images/house_keepers';
                      $imageName = $_FILES['image']['name'];
                      $data->file('image')->move($destinationPath, $imageName);

                      $housekeeping_staffs->image = $imageName;
                      $housekeeping_staffs->save();
                  }
                  Session::flash('message', trans('messages.Housekeeper Details has been submitted successfully!'));
                  return Redirect::to('vendor/house_keeping');
              }
        }
    }

    /**
     * House Keeper Details For Editing.
     *
     */
    public function edit($id){
        if (!Session::get('vendor_id'))
        {
            return redirect()->guest('vendors/login');
        } else {
            $property_id = Session::get('property_id');
            //echo $property_id;die;
            $housekeepers = HouseKeepingModel::where('id','=',$id)
                            ->where('outlet_id','=',$property_id)->get()->toArray();
            //$housekeepers = iterator_to_array($housekeepers);
            if(count($housekeepers) > 0){
             // echo "<pre>";print_r($housekeepers);die;
              return view('vendors.house_keeping.edit')->with('data',$housekeepers);
            } else {
              Session::flash('message', trans('messages.Invalid Housekeeper Details!'));
              return Redirect::to('vendor/house_keeping');
            }
            
        }
    }

     /**
     * Updating House Keeper Details.
     *
     */
    public function update(Request $data,$id){
      if (!Session::get('vendor_id'))
      {
          return redirect()->guest('vendors/login');
      } else {
         $validation = Validator::make($data->all(),array(
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha',
            'mobile' => 'required|numeric|digits_between:6,16',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'outlet_name' => 'required', 
            'address' => 'required|alpha',
            'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:2048',
        ));   
                // process the validation
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } else {
            $housekeeping_staffs = HouseKeepingModel::find($id);
            $housekeeping_staffs->firstname = Input::get('first_name');
            $housekeeping_staffs->lastname = Input::get('last_name');
            $housekeeping_staffs->mobile_number = Input::get('mobile');
            $housekeeping_staffs->date_of_birth = Input::get('date_of_birth');
            $housekeeping_staffs->gender = Input::get('gender');
            $housekeeping_staffs->outlet_id = Input::get('outlet_name');
            $housekeeping_staffs->vendor_id = Session::get('vendor_id');
            $housekeeping_staffs->address = Input::get('address');
            $housekeeping_staffs->status = (int)Input::get('status');
            $housekeeping_staffs->save();

            //For Housekeeper Image
            if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=''){ 
                $destinationPath = base_path().'/public/assets/admin/base/images/house_keepers';
                $imageName = $_FILES['image']['name'];
                $data->file('image')->move($destinationPath, $imageName);

                $housekeeping_staffs->image = $imageName;
                $housekeeping_staffs->save();
            }
            Session::flash('message', trans('messages.Housekeeper Details has been updated successfully!'));
            return Redirect::to('vendor/house_keeping');
        }
      }
    }

     /**
     * Deleting House Keeper Details.
     *
     */
    public function destroy($id){
      if (!Session::get('vendor_id'))
      {
          return redirect()->guest('vendors/login');
      } else {
        $property_id = Session::get('property_id');
        $housekeepers = HouseKeepingModel::where('id','=',$id)
                        ->where('outlet_id','=',$property_id)->first();
        if(count($housekeepers) > 0){
            $housekeepers->delete();
            Session::flash('message', trans('messages.Housekeeper Details has been deleted successfully!'));
            return Redirect::to('vendor/house_keeping');
        } else {
             Session::flash('message', trans('messages.Invalid Housekeeper Details!'));
            return Redirect::to('vendor/house_keeping');
        }      
      }
    }

    /**
     * View the House Keeper Details.
     *
     */
    public function show($id){
      if (!Session::get('vendor_id'))
      {
          return redirect()->guest('vendors/login');
      } else {
          $property_id = Session::get('property_id');
          $housekeepers = HouseKeepingModel::where('id','=',$id)->where('outlet_id','=',$property_id)->get()->toArray();
          if(count($housekeepers) > 0){
            $housekeepers = HouseKeepingModel::find($id)->toArray();
            return view('vendors.house_keeping.show')->with('data',$housekeepers);
          } else {
            Session::flash('message', trans('messages.Invalid Housekeeper Details!'));
            return Redirect::to('vendor/house_keeping');
          }
          
      }
    }

    
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyAjaxHousekeepingTasks()
    {                          
        $vendor_id = Session::get('vendor_id');
        $property_id = Session::get('property_id');
        $outlet_tasks=DB::table('housekeeping_tasks')
         ->leftjoin('outlet_infos','outlet_infos.id','=','housekeeping_tasks.outlet_id')
         ->leftjoin('rooms_infos','rooms_infos.id','=','housekeeping_tasks.room_id')
         ->leftjoin('housekeeping_staffs','housekeeping_staffs.id','=','housekeeping_tasks.housekeeper_id')
        ->where('housekeeping_tasks.vendor_id','=',$vendor_id)
        ->where('housekeeping_tasks.outlet_id','=',$property_id)
        ->select('housekeeping_tasks.id','outlet_infos.outlet_name','housekeeping_tasks.taskname','housekeeping_staffs.firstname','housekeeping_staffs.lastname','rooms_infos.room_name','housekeeping_tasks.task_date','housekeeping_tasks.task_time','housekeeping_tasks.created_at','housekeeping_tasks.updated_at','housekeeping_tasks.room_clean_status')
        ->orderBy('housekeeping_tasks.id', 'desc')
        ->get();
         
        return Datatables::of($outlet_tasks)->addColumn('action', function ($outlet_tasks) {
              if($outlet_tasks->room_clean_status == 2){
                    $data ='<a href="'.URL::to("vendor/housekeeping_tasks/view/".$outlet_tasks->id).'" class="btn btn-xs btn-white" title="'.trans("messages.View").'"><i class="fa fa-eye"></i>&nbsp;'.trans("messages.View").'</a>';
                    return $data;
                } else {
                return '<div class="btn-group"><a href="'.URL::to("vendor/housekeeping_tasks/edit/".$outlet_tasks->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                        <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu xs pull-right" role="menu">
                        <li><a href="'.URL::to("vendor/housekeeping_tasks/delete/".$outlet_tasks->id).'" class="delete-'.$outlet_tasks->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                        </ul>
                    </div><script type="text/javascript">
                    $( document ).ready(function() {
                    $(".delete-'.$outlet_tasks->id.'").on("click", function(){
                         return confirm("'.trans("messages.Are you sure want to delete?").'");
                    });});</script>';
                  }
            })
            ->addColumn('room_cleaning_status',function($outlet_tasks){
                $room_cleans = getRoomCleanStatus();
                if($outlet_tasks->room_clean_status == 2){
                    $data ='<span class="label label-success">Cleaning Completed</span>';
                } else {
                $data = "<select name='clean_status' id='clean_status' class='clean_status form-control'>";
                    if(count($room_cleans)>0){
                        foreach($room_cleans as $key => $val){

                            if($key == $outlet_tasks->room_clean_status){
                              $data .= "<option data-id='".$outlet_tasks->id."' value='".$key."' selected>".$val."</option>";
                            }
                            else{
                              $data .= "<option data-id='".$outlet_tasks->id."' value='".$key."'>".$val."</option>";
                            }
                          
                        }
                      } 
                $data .= "</select>";
                }      
                return $data;                       
              })
            ->editColumn('firstname', function ($outlet_tasks) {
                $data = '-';
                if($outlet_tasks->firstname != null && $outlet_tasks->lastname != null):
              $data = $outlet_tasks->firstname.' '.$outlet_tasks->lastname;
                else:
              $data = $outlet_tasks->firstname;
                endif;
                return $data;
            })
            ->make(true);
    }


    /**
     * Show the House Keeper Tasks List.
     *
     * @return \Illuminate\Http\Response
     */
    public function housekeepingTaskIndex(){
        if (!Session::get('vendor_id')) {
                return redirect()->guest('vendors/login');
        }
        if (!Session::get('property_id'))
            {
                Session::flash('no-property','Add Property First');
                return redirect::to('vendor/outlets');
            }  
         else {
            return view('vendors.house_keeping.house_keeping_tasks.list');
        }
    }

    /**
     * Show the House Keeper Task Add Form.
     *
     */
    public function housekeepingTaskCreate()
    {
        if (!Session::get('vendor_id'))
        {
            return redirect()->guest('vendors/login');
        } else {
 
            $outlet_id = Session::get('property_id');  
            $rooms = DB::table('rooms')->leftjoin('rooms_infos','rooms_infos.id','=','rooms.id')
                 ->where('rooms.property_id','=',$outlet_id)
                 ->where('rooms.room_clean_status','=','0')
                ->select('rooms.id','rooms_infos.room_name')
                ->get(); 
            $rooms = iterator_to_array($rooms);
            
            $Housekeepers = DB::table('housekeeping_staffs')
                   ->where('outlet_id','=',$outlet_id)
                  ->select('id','firstname','lastname')
                  ->get();    
            $Housekeepers = iterator_to_array($Housekeepers);                                 
            return view('vendors.house_keeping.house_keeping_tasks.create')->with('room_details',$rooms)->with('housekeeper_details',$Housekeepers);
        }
    }

    public function housekeepingTaskStore(Request $data){
      if (!Session::get('vendor_id'))
      {
          return redirect()->guest('vendors/login');
      } else {
         $validation = Validator::make($data->all(),array(
            'outlet_name' => 'required',
            'room_name' => 'required',
            'housekeeper_name' => 'required',
            'task_name' => 'required|string',
            'date' => 'required',
            'time' => 'required',
        ));   
         //Incase of validation failure send the room and housekeeper details to the create page
          $outlet_id = Input::get('outlet_name');
          $rooms = DB::table('rooms')->leftjoin('rooms_infos','rooms_infos.id','=','rooms.id')
                   ->where('rooms.property_id','=',$outlet_id)
                  ->select('rooms.id','rooms_infos.room_name')
                  ->get();    
          $rooms = iterator_to_array($rooms);

          $Housekeepers = DB::table('housekeeping_staffs')
                        ->where('outlet_id','=',$outlet_id)
                        ->select('id','firstname','lastname')
                        ->get();  
          $Housekeepers = iterator_to_array($Housekeepers);        
        // process the validation
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput()->with('room_details',$rooms)->with('housekeeper_details',$Housekeepers);
        } else {
            $housekeeping_tasks = new HouseKeepingTasks();
            $housekeeping_tasks->vendor_id = Session::get('vendor_id');
            $housekeeping_tasks->outlet_id = Input::get('outlet_name');
            $housekeeping_tasks->room_id = Input::get('room_name');
            $housekeeping_tasks->housekeeper_id = Input::get('housekeeper_name');
            $housekeeping_tasks->taskname = Input::get('task_name');
            $housekeeping_tasks->task_date = Input::get('date');
            $housekeeping_tasks->task_time = Input::get('time');
            $housekeeping_tasks->save();

            Session::flash('message', trans('messages.Housekeeper Task Details has been submitted successfully!'));
            return Redirect::to('vendor/housekeeping_tasks');
        }
      }
    }

    public function housekeepingTaskEdit($id){
      if (!Session::get('vendor_id'))
      {
          return redirect()->guest('vendors/login');
      } else {
            $property_id = Session::get('property_id');
            $task_details = DB::table('housekeeping_tasks')
                            ->where('id','=',$id)->where('outlet_id','=',$property_id)
                            ->where('room_clean_status','!=',2)
                            ->get();
            $task_details = iterator_to_array($task_details);
            if(count($task_details) > 0){
                        $task_details = HouseKeepingTasks::find($id)->toArray();
              $property_id = $task_details['outlet_id'];
              $rooms = DB::table('rooms')->leftjoin('rooms_infos','rooms_infos.id','=','rooms.id')
                     ->where('rooms.property_id','=',$property_id)
                    ->select('rooms.id','rooms_infos.room_name')
                    ->get();    
              $rooms = iterator_to_array($rooms);

              $Housekeepers = DB::table('housekeeping_staffs')
                     ->where('outlet_id','=',$property_id)
                    ->select('id','firstname','lastname')
                    ->get();    
              $Housekeepers = iterator_to_array($Housekeepers);

              return view('vendors.house_keeping.house_keeping_tasks.edit')->with('data',$task_details)->with('room_details',$rooms)->with('housekeeper_details',$Housekeepers);
            } else {
              Session::flash('message', trans('messages.Invalid Housekeeper Task Details!'));
              return Redirect::to('vendor/housekeeping_tasks');
          }
      }      
    }

     /**
     * Updating House Keeper Details.
     *
     */
    public function housekeepingTaskUpdate(Request $data,$id){
      if (!Session::get('vendor_id'))
      {
          return redirect()->guest('vendors/login');
      } else {
         $validation = Validator::make($data->all(),array(
            'outlet_name' => 'required',
            'room_name' => 'required',
            'housekeeper_name' => 'required',
            'task_name' => 'required|string',
            'date' => 'required',
            'time' => 'required',
        ));    
                // process the validation
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } else {
            $housekeeping_tasks = HouseKeepingTasks::find($id);
            $housekeeping_tasks->vendor_id = Session::get('vendor_id');
            $housekeeping_tasks->outlet_id = Input::get('outlet_name');
            $housekeeping_tasks->room_id = Input::get('room_name');
            $housekeeping_tasks->housekeeper_id = Input::get('housekeeper_name');
            $housekeeping_tasks->taskname = Input::get('task_name');
            $housekeeping_tasks->task_date = Input::get('date');
            $housekeeping_tasks->task_time = Input::get('time');
            $housekeeping_tasks->save();

            Session::flash('message', trans('messages.Housekeeper Task Details has been updated successfully!'));
            return Redirect::to('vendor/housekeeping_tasks');
        }
      }
    }

    public function housekeepingTaskDestroy($id){
      if (!Session::get('vendor_id'))
      {
          return redirect()->guest('vendors/login');
      } else {
        $property_id = Session::get('property_id');
        $housekeepers = DB::table('housekeeping_tasks')->where('id','=',$id)
                        ->where('outlet_id','=',$property_id)
                        ->where('room_clean_status','!=',2)->get();
        $housekeepers = iterator_to_array($housekeepers);
        if(count($housekeepers) > 0){
            $housekeepers->delete();
          Session::flash('message', trans('messages.Housekeeper Task Details has been deleted successfully!'));
          return Redirect::to('vendor/housekeeping_tasks');
        } else {
             Session::flash('message', trans('messages.Invalid Housekeeper Task Details!'));
            return Redirect::to('vendor/housekeeping_tasks');
        }
        
      }  
    }


    public function housekeepingTaskShow($id){
       if (!Session::get('vendor_id'))
      {
          return redirect()->guest('vendors/login');
      } else { 
          $housekeepers = DB::table('housekeeping_tasks')->where('id','=',$id)->where('room_clean_status','=',2)->get();
          $housekeepers = iterator_to_array($housekeepers);
           if(count($housekeepers) > 0){
               $outlet_tasks=DB::table('housekeeping_tasks')
                           ->leftjoin('outlet_infos','outlet_infos.id','=','housekeeping_tasks.outlet_id')
                           ->leftjoin('rooms_infos','rooms_infos.id','=','housekeeping_tasks.room_id')
                           ->leftjoin('housekeeping_staffs','housekeeping_staffs.id','=','housekeeping_tasks.housekeeper_id')
                           ->where('housekeeping_tasks.id','=',$id)
                          ->select('housekeeping_tasks.id','outlet_infos.outlet_name','housekeeping_tasks.taskname','housekeeping_staffs.firstname','housekeeping_staffs.lastname','rooms_infos.room_name','housekeeping_tasks.task_date','housekeeping_tasks.task_time','housekeeping_tasks.created_at','housekeeping_tasks.updated_at','housekeeping_tasks.room_clean_status')
                          ->orderBy('housekeeping_tasks.id', 'desc')
                          ->get();
                $outlet_tasks = iterator_to_array($outlet_tasks);
                return view('vendors.house_keeping.house_keeping_tasks.show')->with('data',$outlet_tasks);
           } else {
               Session::flash('message', trans('messages.Invalid Housekeeper Task Details!'));
              return Redirect::to('vendor/housekeeping_tasks');
           }
            

         
      }
    }
    /**
     *Selecting All Rooms based on Property id.
     */
    public function allRoomsByProperty(Request $request){
        $result = '';
        if($request){
          $property = $request['property_id'];
          $rooms = DB::table('rooms')->leftjoin('rooms_infos','rooms_infos.id','=','rooms.id')
                   ->where('rooms.property_id','=',$property)
                  ->select('rooms.id','rooms_infos.room_name')
                  ->get();    
          $rooms = iterator_to_array($rooms);
          $result = (count($rooms) > 0)?$rooms:array();
        }
        $json_data = array(
          "data"            => $result,
        );
        echo json_encode($json_data);
        }

    /**
     *Selecting All Housekeepers based on Property id.
     */    
    public function getAllHousekeepers(Request $request){
        $result = '';
        if($request){
          $property = $request['property_id'];
          $Housekeepers = DB::table('housekeeping_staffs')
                   ->where('outlet_id','=',$property)
                  ->select('id','firstname','lastname')
                  ->get();    
          $Housekeepers = iterator_to_array($Housekeepers);
          $result = (count($Housekeepers) > 0)?$Housekeepers:array();
        }
        $json_data = array(
          "data"            => $result,
        );
        echo json_encode($json_data);
      
    } 

    //Update room clean status in housekeeping tasks table and rooms table
    public function updateTaskStatus(Request $request){
      $result = '';
      if($request){
          $task_status = $request['status'];
          $task_id = $request['task_id'];
          //Update the room clean status in housekeeping tasks table
          $managetask = HouseKeepingTasks::find($task_id);
          $managetask->room_clean_status = $task_status;
          $managetask->save();

          //Update the room clean status in rooms table
          $room_id = $managetask->room_id;
          $roomstatus = Rooms::find($room_id);
          $roomstatus->room_clean_status = $task_status;
          $roomstatus->save();

          $json_data = array(
          "success" => true,
        );
        echo json_encode($json_data);
      }
    }   
}
