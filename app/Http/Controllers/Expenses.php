<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\expense;
use App\Model\expense_infos;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;


class Expenses extends Controller
{

                
                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function index()
                {
                    $id = Session::get('vendor_id');
                    if (!$id)
                        {
                            return redirect()->guest('vendors/login');
                        } 
                        if(!has_staff_permission('vendor/expenses'))
                        {
                        return view('errors.405');
                        }
                    if (!Session::get('property_id'))
                        {
                            Session::flash('no-property','Add Property First');
                            return redirect::to('vendor/outlets');
                        }                          
                    else{

                        
/*                        
                        $query = '"amenities_infos"."language_id" = (case when (select count(*) as totalcount from amenities_infos where amenities_infos.language_id = '.getAdminCurrentLang().' and amenities.id = amenities_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                        $Amenities=DB::table('amenities')
                         ->select(DB::raw('amenities.* ,amenities.id as cid'),'amenities_infos.*')
                        ->leftJoin('amenities_infos','amenities_infos.id','=','amenities.id')
                        ->whereRaw($query)
                        ->orderBy('amenity_name', 'asc')
                        ->get();
*/                        
                        return view('vendors.expenses.list')/*->with('amenities', $Amenities)*/;
                    }
                    
                }
                
                public function expenses_create()
                {

                    if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/expenses/create'))
                        {
                        return view('errors.405');
                        } else{
                        return view('vendors.expenses.create');
                    }
                }


                public function expenses_edit($id)
                {
                   if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/expenses/edit'))
                        {
                            return view('errors.405');
                        }

                        $expenses = DB::table('expenses')
                        ->where('id','=',$id)->get();
                        //  print_r(count($expenses));exit;
                         if (count($expenses) > 0)
                         {
                            $info = new expense_infos;
                            $expense = expense::find($id);

                            //  dd($expense);
                            return view('vendors.expenses.edit')->with('data', $expense)->with('infomodel', $info);
                         }
                         else{
                            Session::flash('message', trans('messages.Invalid expenses details!'));
                           return Redirect::to('vendor/expenses');
                    }
                }
                
                
                public function expenses_store(Request $data)
                {

  //dd($data->all());
                    $fields['exp_price']  = Input::get('exp_price');
                    $fields['date'] = Input::get('date');
                    $rules = array(
                        
                        'exp_price' => 'required|numeric',
                        'date' => 'required',
                    );
                    $expense_name = Input::get('expense_name');
                    foreach ($expense_name  as $key => $value) {
                        $fields['expense_name'.$key] = $value;
                        $rules['expense_name'.'1'] = 'required|regex:/(^[A-Za-z\s ]+$)+/|unique:expenses_infos,expense_name';
                        
                    }
                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{


                            $Expense = new expense;

                            $Expense->created_at = date("Y-m-d H:i:s");          
                            $Expense->expense_date = Input::get('date');
                            $Expense->expense_cost = Input::get('exp_price');
                            //  $Expense->default_status =  isset($_POST['status']) ? $_POST['status']: 0;

                            $Expense->url_index =  $_POST['expense_name'][1] ? str_slug($_POST['expense_name'][1]): str_slug($_POST['expense_name'][1]);

                            $vendor_id = Session::get('vendor_id');
                            $property_id = Session::get('property_id');
                            
                            $Expense->created_by = $vendor_id;
                            $Expense->property_id = $property_id;
                            $Expense->save();

/*                            

                            $imageName = strtolower($Expense->id . '.' . $data->file('amenty_image')->getClientOriginalExtension());
                            $data->file('amenty_image')->move(
                                base_path() . '/public/assets/admin/base/images/Expense/', $imageName
                            );
                            $destinationPath2 = url('/assets/admin/base/images/Expense/'.$imageName.'');
                          
                            $size=getImageResize('VENDOR');
                            Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/Expense/list/'.$imageName)->destroy();
                            
                            
                            $Expense->amenty_image=$imageName;

                            $Expense->save();

*/
                            $this->expenses_save_after($Expense,$_POST);
                            Session::flash('message', trans('messages.Expense has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('vendor/expenses');
                    }
                }
              
                
                /**
                 * Update the specified blog in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function expenses_update(Request $data, $id)
                {
//  dd($data->all());                    
               
                    $fields['exp_price']  = Input::get('exp_price');
                    $fields['date'] = Input::get('date');
                    $rules = array(
                        
                        'exp_price' => 'required|numeric',
                        'date' => 'required',
                    );                    
                    $expense_name = Input::get('expense_name');
                    foreach ($expense_name  as $key => $value) {
                        $fields['expense_name'.$key] = $value;
                        $rules['expense_name'.'1'] = 'required|regex:/(^[A-Za-z0-9 ]+$)+/|unique:expenses_infos,expense_name,'.$id.',id';
                        
                    }


                    $validator = Validator::make($fields, $rules);    
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        
                        try{

                            //  dd('Ak');
                            
                            $Expense = expense::find($id); 


                            $Expense->expense_date = Input::get('date');
                            $Expense->expense_cost = Input::get('exp_price');
                            //  $Expense->default_status =  isset($_POST['status']) ? $_POST['status']: 0;

                            $Expense->url_index =  $_POST['expense_name'][1] ? str_slug($_POST['expense_name'][1]): str_slug($_POST['expense_name'][1]);

                            $Expense->updated_at = date("Y-m-d H:i:s");
                            $Expense->save();
/*
                            if(isset($_FILES['amenty_image']['name']) && $_FILES['amenty_image']['name']!=''){
                                        $imageName = strtolower($id . '.' . $data->file('amenty_image')->getClientOriginalExtension());
                                        $data->file('amenty_image')->move(
                                            base_path() . '/public/assets/admin/base/images/Expense/', $imageName
                                        );
                                        $destinationPath2 = url('/assets/admin/base/images/Expense/'.$imageName.'');
                                        $size=getImageResize('VENDOR');
                                        Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/Expense/list/'.$imageName)->destroy();

                                        $Expense->amenty_image = $imageName;

                                        $Expense->save();
                                    }
*/
                            $this->expenses_save_after($Expense,$_POST);
                            Session::flash('message', trans('messages.Expense has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('vendor/expenses');
                    }
                }

               
               
                /**
                 * add,edit datas  saved in main table 
                 * after inserted in sub tabel.
                 *
                 * @param  int  $id
                 * @return Response
                 */
               public static function expenses_save_after($object,$post)
               {
                    $city = $object;
                    $post = $post;
                    if(isset($post['expense_name'])){
                        $expense_name = $post['expense_name'];
                        try{                
                            $affected = DB::table('expenses_infos')->where('id', '=', $object->id)->delete();
                            $languages = DB::table('languages')->where('status', 1)->get();
                            foreach($languages as $key => $lang){
                                if(isset($expense_name[$lang->id]) && $expense_name[$lang->id]!=""){
                                    $infomodel = new expense_infos;
                                    $infomodel->language_id = $lang->id;
                                    $infomodel->id = $object->id; 
                                    $infomodel->expense_name = $expense_name[$lang->id];
                                    $infomodel->save();
                                }
                            }
                            }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                            }
                    }
               }
                
                /**
                 * Delete the specified country in storage.
                 *
                 * @param  int  $id
                 * @return Response
                 */
                public function expenses_destroy($id)
                {
                    if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendor/expenses/delete'))
                        {
                            return view('errors.405');
                        }
                        $expenses = DB::table('expenses')
                                        ->where('id','=',$id)->get();

                        //  dd($expenses);

                        if (count($expenses) > 0)
                        {
                            $data = expense::find($id);
                            $data->default_status = 2;
                            $data->save();
                            Session::flash('message', trans('messages.Expenses has been deleted successfully!'));
                            return Redirect::to('vendor/expenses');
                        }
                        else
                        {
                            Session::flash('message', trans('messages.Invalid expenses details!'));
                            return Redirect::to('vendor/expenses');
                        }
                        
                }

                
                /**
                 * Process datatables ajax request.
                 *
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxExpenses()
                {


                 $vendor_id = Session::get('vendor_id');
                        //  print_r($id); exit;

                        if(Session::get('vendor_type') == 2 )
                            {
                                $vendor_id = Session::get('created_vendor_id');
                                //  print_r($id); exit;
                            } 
                $property_id = Session::get('property_id');
                    
                $query = '"expenses_infos"."language_id" = (case when (select count(*) as totalcount from expenses_infos where expenses_infos.language_id = '.getAdminCurrentLang().' and expenses.id = expenses_infos.id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                $expenses = Expense::Leftjoin('expenses_infos','expenses_infos.id','=','expenses.id')
                    ->select(DB::raw('expenses.* ,expenses.id as cid'),'expenses_infos.*')
                    ->where("expenses_infos.language_id","=",getAdminCurrentLang())
                    ->whereRaw($query)
                    ->where('expenses.created_by','=',$vendor_id)
                    ->where('expenses.property_id','=',$property_id)
                    ->orderBy('expenses.id', 'desc')
                    ->get();
                                        //  print_r($expenses);exit;
                    return Datatables::of($expenses)->addColumn('action', function ($expenses) {
                        if(has_staff_permission('vendor/expenses/edit')){
                            return '<div><a href="'.URL::to("vendor/expenses/edit/".$expenses->cid).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a></div>';
                        }
                    })
                        ->addColumn('default_status', function ($expenses) {
                            if($expenses->default_status==0):
                                $data = '<span class="label label-danger">'.trans("messages.Inactive").'</span>';
                            elseif($expenses->default_status==1):
                                $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                            elseif($expenses->default_status==2):
                                $data = '<span class="label label-danger">'.trans("messages.Delete").'</span>';
                            endif;
                            return $data;
                        })
                        ->addColumn('expense_cost', function ($expenses) {
                            if(getCurrencyPosition()->currency_side == 1)
                            {
                                return getCurrency()." ".$expenses->expense_cost;
                            }
                            else {
                                return $expenses->expense_cost." ".getCurrency();
                            }
                        })                        
                        ->addColumn('expense_name', function ($expenses) {
                                $data = ucfirst($expenses->expense_name);
                            return $data;
                        })
                        ->make(true);
                }

                public function accounts()
                {

                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }              
                else
                    {

                    $vendor_id = Session::get('vendor_id');

                    $property_id = Session::get('property_id');                        

                    $date_data = DB::table('booking_details')
                                    ->select(DB::Raw('MIN(check_in_date) as min_date, 
                                                      MAX(check_out_date) as max_date'))
                                    ->get();  

                    $sum_data = DB::table('booking_details')
                                    ->select(DB::Raw('sum(charges) as cp, 
                                                      sum(payments) as pp'))
                                    ->where('booking_details.vendor_id','=',$vendor_id)
                                    ->where('booking_details.outlet_id','=',$property_id)
                                    ->get(); 
                    $order_status = DB::table('booking_status')
                                            ->select('id','name')->orderBy('id', 'asc')->get();                                                         
                        return view('vendors.expenses.accounts.list')->with('date_data',$date_data[0])
                                                                     ->with('sum_data',$sum_data)
                                                                     ->with('order_status',$order_status);
                    }
                }

            public function anyAjaxAccountList(Request $request)
            {
                $post_data = $request->all();

                //  print_r('Ak'); exit;

                $vendor_id = Session::get('vendor_id');

                $property_id = Session::get('property_id');

                $orders  = DB::table('booking_details')
                                    ->select(
                                            'booking_details.id',
                                            'booking_details.charges',
                                            'booking_details.payments',
                                            'booking_details.booking_status',
                                            'booking_details.booking_random_id',
                                            'booking_details.check_out_date',
                                            'booking_details.check_in_date',
                                            'admin_customers.firstname',
                                            'booking_status.name'
                                            )
                                    ->leftJoin('admin_customers','admin_customers.id'
                                        ,'booking_details.customer_id')
                                    ->leftJoin('booked_room','booked_room.booking_id','booking_details.id')
                                    ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                                    //  ->where('booking_details.vendor_id','=',$vendor_id)
                                    ->where('booking_details.outlet_id','=',$property_id)
                                    ->groupBy('booking_details.id','admin_customers.firstname','booking_status.name');
                                    //  ->orderBy('booking_details.id','desc');
                                    //  ->get();
/*                                    
                print_r($orders);
                exit;
*/
                return Datatables::of($orders)->addColumn('date_start', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_in_date));
                    return $data;
                })

                ->addColumn('action', function ($orders) {
                                    
                            $html='<div><a href="'.URL::to("vendor/showbooking/".$orders->id).'" class="btn btn-xs btn-white" title="'.trans("messages.View").'"><i class="fa fa-eye"></i>&nbsp;'.trans("messages.View").'</a><div>';
                                    
                            return $html;
                                        
                })
                                   

                ->addColumn('date_end', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_out_date));
                    return $data;
                })

                ->addColumn('booking_status', function ($booking_details) {
            
                        if($booking_details->booking_status == 1):
                            $data = $booking_details->name;
                        elseif($booking_details->booking_status == 2):
                            $data = $booking_details->name; 
                        elseif($booking_details->booking_status == 3):
                            $data = $booking_details->name;
                        elseif($booking_details->booking_status == 4):
                            $data = $booking_details->name;
                        elseif($booking_details->booking_status == 5):
                            $data = $booking_details->name;   
                        elseif($booking_details->booking_status == 6):
                            $data = $booking_details->name;                                                         
                        endif;
                        return $data;
                })                 
/*                
                ->addColumn('tax_total', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->tax_total;
                    }
                    else {
                        return $orders->tax_total.getCurrency();
                    }
                })
*/                
                ->addColumn('total_charges', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency()." ".$orders->charges;
                    }
                    else {
                        return $orders->charges." ".getCurrency();
                    }
                })
                ->addColumn('total_payments', function ($orders) {
                    if($orders->payments == 0)
                    {
                        return '-';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency()." ".$orders->payments;
                    }
                    else {
                        return $orders->payments." ".getCurrency();
                    }
                })  
                ->addColumn('due_amount', function ($orders) {

                    $bcharges = $orders->charges;

                    $bpayments = $orders->payments;



                    if($bcharges >= $bpayments)
                    {
                        //  print_r("if"); exit;
                       $due_amount = $bcharges - $bpayments;

                    }
                    else 
                    {
                        $due_amount = "";
                    }
                    
                    if($due_amount == 0)
                    {
                        return '-';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency()." ".$due_amount;
                    }
                    else {
                        return $due_amount." ".getCurrency();
                    }
                })    
                ->addColumn('balance', function ($orders) {

                    $bcharges = $orders->charges;

                    $bpayments = $orders->payments;


                    if($bcharges >= $bpayments)
                    {
                        //  print_r("if"); exit;
                       $due_amount = "";

                    }
                    if($bcharges < $bpayments)
                    {
                        $due_amount = $bpayments - $bcharges;
                    }

                    if($due_amount == 0)
                    {
                        return '-';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency()." ".$due_amount;
                    }
                    else {
                        return $due_amount." ".getCurrency();
                    }
                })                                             
                ->filter(function ($query) use ($request){
                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 
                    $order_status =  $request->order_status;
                    $group_by =  $request->group_by;

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and booked_room.date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                    if(!empty($order_status))                    
                    {
                        $order_status = Input::get('order_status');
                        $condition2   = $condition." and booking_details.booking_status = ".$order_status;

                        $query->whereRaw($condition2);
                    }                    
                    if(!empty($group_by))
                    {
                        $group_by = ($request->get('group_by') != '')?$request->get('group_by'):1;
                        if($group_by == 1)
                            $start_date = " date_trunc('day', booking_details.created_date) AS date_start, ";
                        else if($group_by == 2)
                            $start_date = " date_trunc('week', booking_details.created_date) AS date_start, ";
                        else if($group_by == 3)
                            $start_date = " date_trunc('month', booking_details.created_date) AS date_start, ";
                        else if($group_by == 4)
                            $start_date = " date_trunc('year', booking_details.created_date) AS date_start, ";
                        $query->selectRaw($start_date.' MAX(booking_details.created_date) AS date_end')->groupBy('date_start');
                    }                    
                })
                ->make(true);
            }


}
